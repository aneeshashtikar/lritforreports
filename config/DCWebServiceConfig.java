
package in.gov.lrit.portal.webservice.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean; 
import org.springframework.context.annotation.Configuration; 
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import in.gov.lrit.portal.webservice.config.DCSoapConnector;

@Configuration 
public class DCWebServiceConfig {
	@Value("${lrit.dc_url}")
	private String dcUrl;
	@Bean 
	public Jaxb2Marshaller marshaller() { 
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller(); 
		marshaller.setContextPaths("org.imo.gisis.xml.lrit._2008","org.imo.gisis.xml.lrit.positionrequest._2008","org.imo.gisis.xml.lrit.ddprequest._2008"
				,"org.imo.gisis.xml.lrit.coastalstatestandingorderupdate._2014","org.imo.gisis.xml.lrit.geographicalareaupdate._2014","org.imo.gisis.xml.lrit.surpicrequest._2014");
		return	marshaller; 
	}

	@Bean
	public DCSoapConnector dcSoapConnector(Jaxb2Marshaller marshaller) {
		DCSoapConnector client = new DCSoapConnector();
		client.setDefaultUri(dcUrl);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller); 
		return client; 
	}
}
