package in.gov.lrit.portal.webservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class ASPWebServiceConfig {

	  @Bean
	    public Jaxb2Marshaller marshallerClientAsp() {
	        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
	        // this is the package name specified in the <generatePackage> specified in
	        // pom.xml
	        marshaller.setContextPaths("org.imo.gisis.xml.lrit.dnidrequest._2008", "org.imo.gisis.xml.lrit._2008");
	        return marshaller;
	    }
	 
	    @Bean
	    public ASPSoapConnector aspsoapConnector(Jaxb2Marshaller marshaller) {
	    	ASPSoapConnector client = new ASPSoapConnector();
	        client.setDefaultUri("http://10.210.8.140:8181/cxf/LRITASP");
	        client.setMarshaller(marshaller);
	        client.setUnmarshaller(marshaller);
	        return client;
	    }
}
