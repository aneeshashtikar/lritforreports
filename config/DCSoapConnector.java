package in.gov.lrit.portal.webservice.config;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;

import org.imo.gisis.xml.lrit._2008.Response;
import org.imo.gisis.xml.lrit.ddprequest._2008.DDPRequestType;
import org.imo.gisis.xml.lrit.geographicalareaupdate._2014.GeographicalAreaUpdate;
import org.springframework.context.annotation.Configuration;
import org.imo.gisis.xml.lrit.positionrequest._2008.ShipPositionRequestType;
import org.imo.gisis.xml.lrit.surpicrequest._2014.SURPICRequest;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;


@Configuration
public class DCSoapConnector extends WebServiceGatewaySupport {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DCSoapConnector.class);
	
	
	public Response callWebService(String url, DDPRequestType request) throws SOAPException{
		//String res = (String) getWebServiceTemplate().marshalSendAndReceive(url, request);
		System.out.println("in call web service=");
		
		Response resp=null;

		try {
			MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		    SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		    WebServiceTemplate wsTemplate = getWebServiceTemplate();
		    wsTemplate.setMessageFactory(saajSoapMessageFactory);
	     resp =(Response) wsTemplate.marshalSendAndReceive(url, request);
		System.out.println("response="+resp.getResponse().toString());
		 if (resp!=null)
		    {
				System.out.println("not");

		    }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	   
	   
		System.out.println("after  call web service");
		//System.out.println("response="+resp.getResponse().toString());

		return resp;
	}
	
	public Response callWebService(String url, GeographicalAreaUpdate request) throws SOAPException{
		//String res = (String) getWebServiceTemplate().marshalSendAndReceive(url, request);
		logger.info("in call web service=");
		 Response resp=null;
		try {
		MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
	    SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
	    WebServiceTemplate wsTemplate = getWebServiceTemplate();
	    wsTemplate.setMessageFactory(saajSoapMessageFactory);
		
	   resp =(Response) wsTemplate.marshalSendAndReceive(url, request);
	    logger.info("after  call web service");
	    logger.info("response="+resp.getResponse());
	    
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return resp;
	}
	
	
	public Response callWebService(String url, ShipPositionRequestType request) throws SOAPException{
		//String res = (String) getWebServiceTemplate().marshalSendAndReceive(url, request);
		System.out.println("in flagggggcall web service=");
		
		Response resp=null;
		try {
		MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
	    SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
	    WebServiceTemplate wsTemplate = getWebServiceTemplate();
	    wsTemplate.setMessageFactory(saajSoapMessageFactory);

	    System.out.println("in shipposition web service22=");
	     resp =(Response) wsTemplate.marshalSendAndReceive(url, request);
	    
	    
	    if (resp!=null)
	    {
			System.out.println("shipposition");

	    }
	    else
	    {
	    	System.out.println("shipposition not");
	    }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("after  shipposition web service");
		System.out.println("response="+resp.getResponse().toString());

		return resp;
	}
	
	public Response callWebService(String url, SURPICRequest request) throws SOAPException{
		//String res = (String) getWebServiceTemplate().marshalSendAndReceive(url, request);
		logger.info("in call web service=");
		 Response resp=null;
		try {
		MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
	    SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
	    WebServiceTemplate wsTemplate = getWebServiceTemplate();
	    wsTemplate.setMessageFactory(saajSoapMessageFactory);
		
	   resp =(Response) wsTemplate.marshalSendAndReceive(url, request);
	    logger.info("after  call web service");
	    logger.info("response="+resp.getResponse());
	    
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return resp;
	}
	
	
	
	
	
}



