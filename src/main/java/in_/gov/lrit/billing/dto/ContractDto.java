/**
 * @ContractDto.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Nov 7, 2019
 */
package in_.gov.lrit.billing.dto;

import java.util.Date;

/**
 * Description :- ContractDto is use for showing the list of various contracts
 * @author lrit-billing
 *
 */
public class ContractDto {

	private final String contractNo;
	private final String contractType;
	private final Date validFrom;
	private final Date validTo;
	private final String agency2Name;
	private final Boolean validity;
	private final Integer contractId;
	private final String action;


	public ContractDto(String contractNumber, String contractType, Date validFrom, Date validTo, Boolean validity,
			String agency2Name,Integer contractId) {
		super();
		this.contractNo = contractNumber;
		this.contractType = contractType;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.agency2Name = agency2Name;
		this.validity = validity;
		this.contractId = contractId;
		String terminate = "<a href='/lrit/contract/terminate/"+ contractId +  "' target=\"_blank\">Terminate Contract</a></br>";
		String edit = "<a href='/lrit/contract/getContractByContractId/" + contractId + "'  target=\"_blank\">Edit Contract</a></br>";
		String view = "<a href='/lrit/contract/viewContractByContractId/" + contractId + "'  target=\"_blank\">View Contract</a></br>";

			this.action = "";
		
 
	}


	/**
	 * @return the contractNo
	 */
	public String getContractNo() {
		return contractNo;
	}


	/**
	 * @return the contractType
	 */
	public String getContractType() {
		return contractType;
	}


	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}


	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}



	/**
	 * @return the validity
	 */
	public Boolean getValidity() {
		return validity;
	}


	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}


	/**
	 * @return the agency2Name
	 */
	public String getAgency2Name() {
		return agency2Name;
	}


	/**
	 * @return the contractId
	 */
	public Integer getContractId() {
		return contractId;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((agency2Name == null) ? 0 : agency2Name.hashCode());
		result = prime * result + ((contractId == null) ? 0 : contractId.hashCode());
		result = prime * result + ((contractNo == null) ? 0 : contractNo.hashCode());
		result = prime * result + ((contractType == null) ? 0 : contractType.hashCode());
		result = prime * result + ((validFrom == null) ? 0 : validFrom.hashCode());
		result = prime * result + ((validTo == null) ? 0 : validTo.hashCode());
		result = prime * result + ((validity == null) ? 0 : validity.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractDto other = (ContractDto) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (agency2Name == null) {
			if (other.agency2Name != null)
				return false;
		} else if (!agency2Name.equals(other.agency2Name))
			return false;
		if (contractId == null) {
			if (other.contractId != null)
				return false;
		} else if (!contractId.equals(other.contractId))
			return false;
		if (contractNo == null) {
			if (other.contractNo != null)
				return false;
		} else if (!contractNo.equals(other.contractNo))
			return false;
		if (contractType == null) {
			if (other.contractType != null)
				return false;
		} else if (!contractType.equals(other.contractType))
			return false;
		if (validFrom == null) {
			if (other.validFrom != null)
				return false;
		} else if (!validFrom.equals(other.validFrom))
			return false;
		if (validTo == null) {
			if (other.validTo != null)
				return false;
		} else if (!validTo.equals(other.validTo))
			return false;
		if (validity == null) {
			if (other.validity != null)
				return false;
		} else if (!validity.equals(other.validity))
			return false;
		return true;
	}




	






}
