/**
 * @InvoiceDto.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 30-Aug-2019
 */
package in_.gov.lrit.billing.dto;

import java.util.Date;

import in.gov.lrit.billing.model.billingservice.Status;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public class InvoiceDto {
	private final String invoiceBillNo;
	private final Date fromDate;
	private final Date toDate;
	private final Date date;
	private final String status;
	private final Integer billingServiceId;
	private final Integer lastStatus;
	private final String action;

	/**
	 * @param invoiceBillNo
	 * @param fromDate
	 * @param toDate
	 * @param date
	 * @param status
	 */
	public InvoiceDto(String invoiceBillNo, Date fromDate, Date toDate, Date date) {
		super();
		this.invoiceBillNo = invoiceBillNo;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.date = date;
		this.status = "";
		this.billingServiceId = 0;
		this.lastStatus = 0;
		this.action = "";
	}

	/**
	 * @param invoiceBillNo
	 * @param fromDate
	 * @param toDate
	 * @param date
	 * @param status
	 */
	public InvoiceDto(String invoiceBillNo, Date fromDate, Date toDate, Date date, String status) {
		super();
		this.invoiceBillNo = invoiceBillNo;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.date = date;
		this.status = status;
		this.billingServiceId = 0;
		this.lastStatus = 0;
		this.action = "";
	}

	public InvoiceDto(String invoiceBillNo, Date fromDate, Date toDate, Date date, Status status,
			Integer billingServiceId) {
		super();
		this.invoiceBillNo = invoiceBillNo;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.date = date;
		this.status = status.getStatusName();
		this.billingServiceId = billingServiceId;
		this.lastStatus = status.getStatusId();

		String sendInvoiceDG = "<a href='/lrit/invoice/sendInvoice/" + billingServiceId
				+ "' target='_blank'>Send_To_DG</a>";
		String uploadSignedInvoice = "<a href='/lrit/invoice/uploadSignedInvoice/" + billingServiceId
				+ "' target='_blank'>Upload_Signed_Invoice</a>";
		String sendMailToAgency = "<a href='/lrit/invoice/mailInvoice/" + billingServiceId
				+ "' target='_blank'>Send Mail</a>";
		String updateCount = "<a href='/lrit/invoice/updateCount/" + billingServiceId
				+ "' target='_blank'>Update Count</a>";
		String updatePayment = "<a href='/lrit/invoice/updatePayment/" + billingServiceId
				+ "' target='_blank'>Update Payment</a>";
		String sendReminder = "<a href='/lrit/invoice/sendReminder/" + billingServiceId
				+ "' target='_blank'>Send Reminder</a>";

//		if (status.getStatusId() == 1) {
//			this.action = sendInvoiceDG;
//		} else if (status.getStatusId() == 2) {
//			this.action = sendInvoiceDG;
//		} else if (status.getStatusId() == 3) {
//			this.action = uploadSignedInvoice;
//		} else if (status.getStatusId() == 4) {
//			this.action = sendMailToAgency;
//		} else if (status.getStatusId() == 5) {
//			this.action = updateCount + "<br>" + updatePayment + "<br>" + sendReminder;
//		} else if (status.getStatusId() == 6) {
//			this.action = "";
//		} else if (status.getStatusId() == 7) {
//			this.action = updatePayment + "<br>" + sendReminder;
//		} else
		this.action = "";

	}

	public String getAction() {
		return action;
	}

	public String getInvoiceBillNo() {
		return invoiceBillNo;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public Date getDate() {
		return date;
	}

	public String getStatus() {
		return status;
	}

	public Integer getBillingServiceId() {
		return billingServiceId;
	}

	public Integer getLastStatus() {
		return lastStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((billingServiceId == null) ? 0 : billingServiceId.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((invoiceBillNo == null) ? 0 : invoiceBillNo.hashCode());
		result = prime * result + ((lastStatus == null) ? 0 : lastStatus.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceDto other = (InvoiceDto) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (billingServiceId == null) {
			if (other.billingServiceId != null)
				return false;
		} else if (!billingServiceId.equals(other.billingServiceId))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (fromDate == null) {
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate))
			return false;
		if (invoiceBillNo == null) {
			if (other.invoiceBillNo != null)
				return false;
		} else if (!invoiceBillNo.equals(other.invoiceBillNo))
			return false;
		if (lastStatus == null) {
			if (other.lastStatus != null)
				return false;
		} else if (!lastStatus.equals(other.lastStatus))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (toDate == null) {
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate))
			return false;
		return true;
	}

}
