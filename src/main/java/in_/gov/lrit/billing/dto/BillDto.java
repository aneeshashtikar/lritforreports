/**
 * @BillDto.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 30-Aug-2019
 */
package in_.gov.lrit.billing.dto;

import java.util.Date;

import in.gov.lrit.billing.model.billingservice.Status;

/** Description :- BillDto is use for showing the list of various bills
 * @author lrit-billing
 *
 */
public class BillDto {
	private final String billNo;
	private final Date fromDate;
	private final Date toDate;
	private final Date date;
	private final String status;
	private final String action;
	private final Integer statusId;
	private final Integer billingServiceId;
	private final Integer contractType;
	
	/**
	 * @param billNo
	 * @param fromDate
	 * @param toDate
	 * @param date
	 */
	public BillDto(String billNo, Date fromDate, Date toDate, Date date) {
		super();
		this.billNo = billNo;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.date = date;
		this.status = "";
		this.action = "";
		this.statusId = 0;
		this.contractType = 0;
		this.billingServiceId = 0;
		
	}

	/**
	 * @param billNo
	 * @param fromDate
	 * @param toDate
	 * @param date
	 * @param status
	 */
	public BillDto(String billNo, Date fromDate, Date toDate, Date date, String status) {
		super();
		this.billNo = billNo;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.date = date;
		this.status = status;
		this.action = "";
		this.statusId = 0;
		this.contractType = 0;
		this.billingServiceId = 0;
	}
	
	public BillDto(String billNo, Date fromDate, Date toDate, Date date, Status status,
			Integer billingServiceId) {
		super();
		this.billNo = billNo;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.date = date;
		this.status = status.getStatusName();
		this.statusId = 0;
		this.contractType = 0;
		this.billingServiceId = 0;
		this.action = "<a href='/lrit/bill/sendVerifiedBillCG/" + billingServiceId
				+ "'  target=\"_blank\">Send_To_DG</a></br>"
				+ "<a href='/lrit/bill/verifyBill/" + billingServiceId
				+ "' target=\"_blank\">Verify Bill</a></br>"
				+ "<a href='/lrit/bill/updateCSPBill/" + billingServiceId
				+ "'  target=\"_blank\">Update Bill</a></br>"
				+ "<a href='/lrit/bill/updatePayment/" + billingServiceId
				+ "'  target=\"_blank\">Update Payment</a></br>";
	}
	
	public BillDto(String billNo, Date fromDate, Date toDate, Date date, Status status,
			Integer billingServiceId, Integer contractType) {
		super();
		this.billNo = billNo;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.date = date;
		this.status = status.getStatusName();
		this.statusId = status.getStatusId();
		this.contractType = contractType;
		this.billingServiceId = billingServiceId;
		String updateBillByContractType = contractType == 3 ? "<a href='/lrit/bill/updateCSPBill/" : "<a href='/lrit/bill/updateBill/" ;
		String viewBillByContractType = contractType == 3 ? "<a href='/lrit/bill/viewCSPBill/" : "<a href='/lrit/bill/viewBill/" ;
		
		String sentToDG = "<a href='/lrit/bill/sendVerifiedBillCG/" + billingServiceId + "'  target=\"_blank\">Send To DG</a></br>";
		
		String verifyBill =  "<a href='/lrit/bill/verifyBill/" + billingServiceId + "' target=\"_blank\">Verify Bill</a></br>";
		
		String updateBill = updateBillByContractType + billingServiceId + "'  target=\"_blank\">Update Bill</a></br>";
		
		String viewBill = viewBillByContractType + billingServiceId + "'  target=\"_blank\">View Bill</a></br>";
		
		String updatePayment = "<a href='/lrit/bill/updatePayment/" + billingServiceId + "'  target=\"_blank\">Update Payment</a></br>";
		
		this.action="";
			
		
		
		
	}
	

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	public String getBillNo() {
		return billNo;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public Date getDate() {
		return date;
	}

	public String getStatus() {
		return status;
	}
	
	

	/**
	 * @return the billingServiceId
	 */
	public Integer getBillingServiceId() {
		return billingServiceId;
	}

	/**
	 * @return the contractType
	 */
	public Integer getContractType() {
		return contractType;
	}

	/**
	 * @return the statusId
	 */
	public Integer getStatusId() {
		return statusId;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((billNo == null) ? 0 : billNo.hashCode());
		result = prime * result + ((billingServiceId == null) ? 0 : billingServiceId.hashCode());
		result = prime * result + ((contractType == null) ? 0 : contractType.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((statusId == null) ? 0 : statusId.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillDto other = (BillDto) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (billNo == null) {
			if (other.billNo != null)
				return false;
		} else if (!billNo.equals(other.billNo))
			return false;
		if (billingServiceId == null) {
			if (other.billingServiceId != null)
				return false;
		} else if (!billingServiceId.equals(other.billingServiceId))
			return false;
		if (contractType == null) {
			if (other.contractType != null)
				return false;
		} else if (!contractType.equals(other.contractType))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (fromDate == null) {
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (statusId == null) {
			if (other.statusId != null)
				return false;
		} else if (!statusId.equals(other.statusId))
			return false;
		if (toDate == null) {
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate))
			return false;
		return true;
	}

}
