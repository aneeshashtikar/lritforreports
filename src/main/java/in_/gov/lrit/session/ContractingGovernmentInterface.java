package in_.gov.lrit.session;

import java.io.Serializable;

/*public interface ContractingGovernmentInterface extends Serializable {

	public String getLritId();
	public String getCountryName();
}
*/

public class ContractingGovernmentInterface implements Serializable {
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String lritId;
    private String countryName;

    public String getLritId() {
		return lritId;
	}

	public void setLritId(String lritId) {
		this.lritId = lritId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public ContractingGovernmentInterface(String lritId, String countryName) {
        this.lritId = lritId;
        this.countryName = countryName;
      
    }
}