package in_.gov.lrit.session;

import java.io.Serializable;

import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;

/*public interface UserSessionInterface extends Serializable {

	public PortalLritIdMaster getRequestorsLritId();
	public String getName();
	public String getEmailid();
	public String getCategory();
	public String getLoginId();
	
}
*/

public class UserSessionInterface implements Serializable {
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private String requestorsLritId;
    private String name;
    private String emailid;
    private String category;
    private String loginId;
	private PortalLritIdMaster requestorsLritId;

    
    public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmailid() {
		return emailid;
	}


	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getLoginId() {
		return loginId;
	}


	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}


	public PortalLritIdMaster getRequestorsLritId() {
		return requestorsLritId;
	}


	public void setRequestorsLritId(PortalLritIdMaster requestorsLritId) {
		this.requestorsLritId = requestorsLritId;
	}


	public UserSessionInterface(String name,String emailid,String category,String loginId,PortalLritIdMaster requestorsLritId) {
       // this.requestorsLritId = requestorsLritId;
        this.name = name;
        this.emailid = emailid;
        this.category = category;
        this.loginId = loginId;
        this.requestorsLritId=requestorsLritId;
      
    }
    
}