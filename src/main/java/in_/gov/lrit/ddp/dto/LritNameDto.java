/**
 * @ASPDto.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 23, 2019
 */
package in_.gov.lrit.ddp.dto;

/**
 * Description :- LritNameDto is use to get LritName in query
 * @author lrit-billing
 *
 */
public class LritNameDto {
	private final String lritid;
	private final String name;

	/**
	 * @return the lritid
	 */
	public String getLritid() {
		return lritid;
	}

	public LritNameDto(String lritid) {
		super();
		this.lritid = lritid;
		this.name = "";
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public LritNameDto(String lritid, String name) {
		super();
		this.lritid = lritid;
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lritid == null) ? 0 : lritid.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LritNameDto other = (LritNameDto) obj;
		if (lritid == null) {
			if (other.lritid != null)
				return false;
		} else if (!lritid.equals(other.lritid))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return lritid + "-" + name;
	}
}
