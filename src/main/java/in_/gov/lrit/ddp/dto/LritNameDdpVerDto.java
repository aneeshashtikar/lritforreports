/**
 * @ASPDto.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 23, 2019
 */
package in_.gov.lrit.ddp.dto;

/**
 * Description :- LritNameDdpVerDto is use to get LritName and DDP Version in query
 * @author lrit-billing
 *
 */
public class LritNameDdpVerDto {
	private final String lritid;
	private final String name;
	private final String ddpVer; 
	/**
	 * @return the lritid
	 */
	public String getLritid() {
		return lritid;
	}

	public LritNameDdpVerDto(String lritid, String name, String ddpVer) {
		super();
		this.lritid = lritid;
		this.name = name;
		this.ddpVer = ddpVer;
	}

	
	public String getDdpVer() {
		return ddpVer;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/*
	 * public LritNameDdpVerDto(String lritid, String name) { super(); this.lritid =
	 * lritid; this.name = name; }
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lritid == null) ? 0 : lritid.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LritNameDdpVerDto other = (LritNameDdpVerDto) obj;
		if (lritid == null) {
			if (other.lritid != null)
				return false;
		} else if (!lritid.equals(other.lritid))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ddpVer == null) {
			if (other.ddpVer != null)
				return false;
		} else if (!ddpVer.equals(other.ddpVer))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return lritid + "-" + name + "-" + ddpVer;
	}
}
