/**
 * @LritNameDtoInterface.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 24, 2019
 */
package in_.gov.lrit.ddp.dto;

/**
 * Description :- LritNameDto is use to get LritName in query
 * @author lrit-billing
 *
 */
public interface LritNameDtoInterface {
	
	public String lritid();
	public String name();
}
