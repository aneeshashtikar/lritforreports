package in_.gov.lrit.portal.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class VwShipStatusLatestPositionDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String imoNo;
	private String mmsiNo;
	private String shipName;
	private String callSign;	
	private String latitude;	
	private String longitude;	
	private Date timestamp1;	
	private String status;
	private String ownerCompany;
	
	
	public VwShipStatusLatestPositionDto(String imoNo, String mmsiNo, String shipName, String callSign, String latitude,
			String longitude, Date timestamp1, String status, String ownerCompany) {
		super();
		this.imoNo = imoNo;
		this.mmsiNo = mmsiNo;
		this.shipName = shipName;
		this.callSign = callSign;
		this.latitude = latitude;
		this.longitude = longitude;
		this.timestamp1 = timestamp1;
		this.status = status;
		this.ownerCompany = ownerCompany;
	}


	public VwShipStatusLatestPositionDto() {
		super();
	}


	public String getImoNo() {
		return imoNo;
	}


	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}


	public String getMmsiNo() {
		return mmsiNo;
	}


	public void setMmsiNo(String mmsiNo) {
		this.mmsiNo = mmsiNo;
	}


	public String getShipName() {
		return shipName;
	}


	public void setShipName(String shipName) {
		this.shipName = shipName;
	}


	public String getCallSign() {
		return callSign;
	}


	public void setCallSign(String callSign) {
		this.callSign = callSign;
	}


	public String getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	public Date getTimestamp1() {
		return timestamp1;
	}


	public void setTimestamp1(Date timestamp1) {
		this.timestamp1 = timestamp1;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getOwnerCompany() {
		return ownerCompany;
	}


	public void setOwnerCompany(String ownerCompany) {
		this.ownerCompany = ownerCompany;
	} 
	
	
}
