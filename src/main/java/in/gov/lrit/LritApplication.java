/**
 * @LritApplication.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author   Kavita Sharma
 * @version 1.0
 */
package in.gov.lrit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.bedatadriven.jackson.datatype.jts.JtsModule;

import in.gov.lrit.config.FileStorageProperties;

/**
 * This class provides the entry method for the application .
 * 
 * @author Kavita Sharma
 */
@SpringBootApplication
@ComponentScan(basePackages = "in.gov.lrit")
@EnableConfigurationProperties({ FileStorageProperties.class })
public class LritApplication extends SpringBootServletInitializer {

	private static Class<LritApplication> applicationClass = LritApplication.class;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

	public static void main(String[] args) {
		SpringApplication.run(applicationClass, args);
	}

	@Bean
	public JtsModule jtsmodule() {
		return new JtsModule();
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public LocalValidatorFactoryBean validator(MessageSource messageSource) {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource);
		return bean;
	}
}
