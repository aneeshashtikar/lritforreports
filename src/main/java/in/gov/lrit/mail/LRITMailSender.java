package in.gov.lrit.mail;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class LRITMailSender {

	@Autowired
	private JavaMailSender javaMailSender;

	public void sendEmail(String from, String to, String subject, String mailBody) {

		SimpleMailMessage msg = new SimpleMailMessage();

		msg.setFrom(from);
		msg.setTo(to);

		msg.setSubject(subject);
		msg.setText(mailBody);

		javaMailSender.send(msg);

	}

	public void sendEmailWithAttachment(String from, String to, String subject, String mailBody, String resource)
			throws MessagingException {

		MimeMessage msg = javaMailSender.createMimeMessage();

		// true = multipart message
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);

		helper.setFrom(from);
		helper.setTo(to);

		helper.setSubject(subject);

		// default = text/plain
		// helper.setText("Check attachment for image!");

		// true = text/html
		helper.setText(mailBody, true);

		// Add resource from resources folder as attachment
		helper.addAttachment(resource, new ClassPathResource(resource));

		javaMailSender.send(msg);

	}

	public void sendEmailWithAttachment(String from, String to, String subject, String mailBody, DataSource datasource,
			String attachmentName) throws MessagingException {

		MimeMessage msg = javaMailSender.createMimeMessage();

		// true = multipart message
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);

		helper.setFrom(from);
		helper.setTo(to);

		helper.setSubject(subject);

		// default = text/plain
		// helper.setText("Check attachment for image!");

		// true = text/html
		helper.setText(mailBody, true);

		// Add resource from resources folder as attachment
		helper.addAttachment(attachmentName, datasource);

		javaMailSender.send(msg);

	}

	public void sendEmailWithAttachment(String from, String to, String subject, String mailBody, DataSource datasource,
			String attachmentName, String cc) throws MessagingException {

		MimeMessage msg = javaMailSender.createMimeMessage();

		// true = multipart message
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);

		helper.setFrom(from);
		helper.setTo(to);
		helper.setCc(InternetAddress.parse(cc));

		helper.setSubject(subject);

		// default = text/plain
		// helper.setText("Check attachment for image!");

		// true = text/html
		helper.setText(mailBody, true);

		// Add resource from resources folder as attachment
		helper.addAttachment(attachmentName, datasource);

		javaMailSender.send(msg);

	}
}
