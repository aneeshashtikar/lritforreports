package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.gis.ddp.model.LritPort;
import in.gov.lrit.gis.ddp.model.PortModel;

public interface PortDetailsRepository extends JpaRepository<LritPort, String>{	
	
		
	 @Query(value = "Select * from lrit_ports where cg_lritid = :cglritid and regularversion_no= :regularversion", nativeQuery = true)
	  List<LritPort> findBycgLritidAndRegularversionNo(String cglritid,String regularversion);	
	

}

