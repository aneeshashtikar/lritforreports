package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the lrit_ports database table.
 * 
 */
@Embeddable
public class LritPortPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String locode;

	@Column(name="regularversion_no")
	private String regularversionNo;

	public LritPortPK() {
	}
	public String getLocode() {
		return locode;
	}
	public void setLocode(String locode) {
		this.locode = locode;
	}
	public String getRegularversionNo() {
		return this.regularversionNo;
	}
	public void setRegularversionNo(String regularversionNo) {
		this.regularversionNo = regularversionNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof LritPortPK)) {
			return false;
		}
		LritPortPK castOther = (LritPortPK)other;
		return 
			this.locode.equals(castOther.locode)
			&& this.regularversionNo.equals(castOther.regularversionNo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.locode.hashCode();
		hash = hash * prime + this.regularversionNo.hashCode();
		
		return hash;
	}
}