package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.gis.ddp.model.LritContractGovtMst;


public interface ContractingGovtMasterRepository extends JpaRepository<LritContractGovtMst, String>{	
	
		
	 @Query(value = "Select * from lrit_contract_govt_mst where regularversion_no= :regularversion ", nativeQuery = true)
	List<LritContractGovtMst> findByCgName(String regularversion);	
	
	 @Query(value = "Select cg_lritid from lrit_contract_govt_mst where cg_name= :country limit 1 ", nativeQuery = true)
	 String findByCgLritid(String country);

}

