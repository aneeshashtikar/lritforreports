package in.gov.lrit.gis.ddp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.vividsolutions.jts.geom.Geometry;

@Entity
//@Table(name = "lrit_ports")
public class PortModel {
	
	   @Id
	    private String locode;   
	   
	   @Column(name="regularversion_no")
		private String regularversionno;
	   
		private String name;
		private Geometry position;
		@Column(name="cg_lritid")
		private String cglritid;
		
		
			
	/*
	 * @Column(name="cg_name") private String cgName;
	 */
		
		public PortModel() {
			
		}


		public String getLocode() {
			return locode;
		}



		public String getRegularversionno() {
			return regularversionno;
		}


		public void setRegularversionno(String regularversionno) {
			this.regularversionno = regularversionno;
		}


		public void setLocode(String locode) {
			this.locode = locode;
		}



		

		public String getName() {
			return name;
		}



		public void setName(String name) {
			this.name = name;
		}



		public Geometry getPosition() {
			return position;
		}



		public void setPosition(Geometry position) {
			this.position = position;
		}



		public String getCglritid() {
			return cglritid;
		}



		public void setCglritid(String cglritid) {
			this.cglritid = cglritid;
		}


}
