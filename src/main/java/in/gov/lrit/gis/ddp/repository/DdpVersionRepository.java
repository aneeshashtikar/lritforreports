package in.gov.lrit.gis.ddp.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import in.gov.lrit.gis.ddp.model.DdpVersion;

public interface DdpVersionRepository extends JpaRepository<DdpVersion, String>{	
	
	List<DdpVersion> findAll();	
}

