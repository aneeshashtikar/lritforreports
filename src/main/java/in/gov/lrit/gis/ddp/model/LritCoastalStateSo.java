package in.gov.lrit.gis.ddp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "lrit_coastal_state_so")
@IdClass(LritCoastalStateSoPK.class)
public class LritCoastalStateSo {
	
	@Id	
	private String immediateversion_no;
	
	@Id		
	private String cg_lritid;

	private String cg_regularversion_no;

	public String getImmediateversion_no() {
		return immediateversion_no;
	}

	public String getCg_lritid() {
		return cg_lritid;
	}

	public void setCg_lritid(String cg_lritid) {
		this.cg_lritid = cg_lritid;
	}

	public void setImmediateversion_no(String immediateversion_no) {
		this.immediateversion_no = immediateversion_no;
	}

	

	public String getCg_regularversion_no() {
		return cg_regularversion_no;
	}

	public void setCg_regularversion_no(String cg_regularversion_no) {
		this.cg_regularversion_no = cg_regularversion_no;
	}
	
	
}
