/**
 * @(#)countryWaterPolygonAvailRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */


package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.gis.ddp.model.CountrywaterPolygonsAvail;
import in.gov.lrit.gis.ddp.model.SOFetchModel;
import in.gov.lrit.gis.ddp.model.lritIdMapping;

@Repository
public interface countryWaterPolygonAvailRepository extends CrudRepository<CountrywaterPolygonsAvail, Long> {

	/*@Query(value = "Select a.cg_lritid, a.type , lrit_contract_govt_mst.cg_name from lrit_contract_govt_mst  RIGHT JOIN \r\n"
			+ "(Select lrit_polygons.cg_lritid, lrit_polygons.type from lrit_polygons\r\n"
			+ "GROUP BY lrit_polygons.cg_lritid, lrit_polygons.type \r\n"
			+ "ORDER BY lrit_polygons.cg_lritid ) a \r\n"
			+ "ON lrit_contract_govt_mst.cg_lritid = a.cg_lritid ORDER BY a.cg_lritid; ", nativeQuery = true)*/
	
	@Query(value = "Select cg_lritid, type from lrit_polygons where regularversion_no = (Select fn_getapplicableregularversion()) GROUP BY cg_lritid, type ORDER BY cg_lritid;", nativeQuery = true)		
	List<CountrywaterPolygonsAvail> findallGroupBy();
	
	

}
