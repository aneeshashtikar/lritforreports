package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the lrit_contract_govt_mst database table.
 * 
 */
@Embeddable
public class LritContractGovtMstPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="cg_lritid")
	private String cgLritid;

	@Column(name="regularversion_no")
	private String regularversionNo;

	public LritContractGovtMstPK() {
	}
	public String getCgLritid() {
		return this.cgLritid;
	}
	public void setCgLritid(String cgLritid) {
		this.cgLritid = cgLritid;
	}
	public String getRegularversionNo() {
		return this.regularversionNo;
	}
	public void setRegularversionNo(String regularversionNo) {
		this.regularversionNo = regularversionNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof LritContractGovtMstPK)) {
			return false;
		}
		LritContractGovtMstPK castOther = (LritContractGovtMstPK)other;
		return 
			this.cgLritid.equals(castOther.cgLritid)
			&& this.regularversionNo.equals(castOther.regularversionNo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.cgLritid.hashCode();
		hash = hash * prime + this.regularversionNo.hashCode();
		
		return hash;
	}
}