package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;

import com.vividsolutions.jts.geom.Geometry;


/**
 * The persistent class for the lrit_polygons database table.
 * 
 */
@Entity
@Table(name="lrit_polygons")
//@NamedQuery(name="LritPolygon.findAll", query="SELECT l FROM LritPolygon l")
public class LritPolygon implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LritPolygonPK id;

	private String caption;

	@Column(name="cg_lritid")
	private String cgLritid;

	@Column(name="cg_regularversion_no")
	private String cgRegularversionNo;

	private Geometry poslist;

	private String type;

	public LritPolygon() {
	}

	public LritPolygonPK getId() {
		return this.id;
	}

	public void setId(LritPolygonPK id) {
		this.id = id;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getCgLritid() {
		return this.cgLritid;
	}

	public void setCgLritid(String cgLritid) {
		this.cgLritid = cgLritid;
	}

	public String getCgRegularversionNo() {
		return this.cgRegularversionNo;
	}

	public void setCgRegularversionNo(String cgRegularversionNo) {
		this.cgRegularversionNo = cgRegularversionNo;
	}

	public Geometry getPoslist() {
		return this.poslist;
	}

	public void setPoslist(Geometry poslist) {
		this.poslist = poslist;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}