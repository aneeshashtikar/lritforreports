/**
 * @(#)SOFetchRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */


package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.gis.ddp.model.LritCoastalStateSo;
import in.gov.lrit.gis.ddp.model.SOFetchModel;

@Repository
public interface SOFetchRepository extends CrudRepository<SOFetchModel, Long> {

	@Query(value = "SELECT * FROM lrit_polygons where cg_lritid = ?1 and type='CoastalSea' and  regularversion_no = (Select fn_getapplicableregularversion()); ", nativeQuery = true)
	List<SOFetchModel> findBycglritid(String cg_lritid);

	@Query(value = "SELECT * FROM lrit_polygons where cg_lritid = ?1 and type in ?2 and  regularversion_no = (Select fn_getapplicableregularversion()); ", nativeQuery = true)
	List<SOFetchModel> findBycglritidWaterPolygons(String string, List<String> waterPolygonForSQL);

	@Query(value = "SELECT DISTINCT ON (cg_lritid) * FROM lrit_polygons where type='CoastalSea' and  regularversion_no = (Select fn_getapplicableregularversion()); ", nativeQuery = true)
	List<SOFetchModel> findCountryBoundary();
	
	@Query(value="SELECT max(cast(substring(area_id,10) as integer)) FROM lrit_polygons where area_id like ?1% and  regularversion_no = (Select fn_getapplicableregularversion());", nativeQuery = true)
	int getNextAreacodeFromDDP(String watertype);
	
	
	/*@Query(value = "SELECT regularversion_no, area_id, cg_lritid, caption, poslist, type, cg_regularversion_no FROM public.lrit_polygons where"
			+ " area_id in (Select area_id from lrit_coastal_mapping where cg_lritid in ?1 and immediateversion_no =  fn_getApplicableImmediateVersion())"
			+ " and regularversion_no = fn_getapplicableregularversion()) ;", nativeQuery = true)*/
	
	@Query(value = "SELECT regularversion_no, area_id, cg_lritid, caption, poslist, type, cg_regularversion_no "
			+ "FROM public.lrit_polygons where area_id IN (Select area_id from lrit_coastal_mapping where cg_lritid = ?1"
			+ " and immediateversion_no =  fn_getApplicableImmediateVersion()) and regularversion_no =  fn_getApplicableRegularVersion();", nativeQuery = true)
	 List<SOFetchModel> getOpenSO(String requiredOpenSoLritId);
	
}
