package in.gov.lrit.gis.ddp.repository;


import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.gis.ddp.model.LritDdpHst;


public interface DdpHistoryRepository extends JpaRepository<LritDdpHst, String>{	
	
		
	 @Query(value = "Select implemented_at from lrit_ddp_hst where updated_version = :ddpversion limit 1", nativeQuery = true)
	 Timestamp findByUpdatedVersion(String ddpversion);	
	

}

