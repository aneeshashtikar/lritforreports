package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.gov.lrit.gis.ddp.model.PortModel;

public interface PortRepository extends CrudRepository<PortModel, Long> {

	@Query(value = "Select * from lrit_ports where cg_lritid = ?1 and regularversion_no = (Select fn_getapplicableregularversion());", nativeQuery = true)
	List<PortModel> findByCglritid(String cglritid);

	@Query(value = "Select locode, regularversion_no, name, position, cg_lritid from lrit_ports\r\n"
			+ "where lrit_ports.locode in ?1 and regularversion_no = (Select fn_getapplicableregularversion()) ORDER BY name;", nativeQuery = true)
	List<PortModel> findPortDetailsForDisplay(List<String> locode);
	

}
