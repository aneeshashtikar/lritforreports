package in.gov.lrit.gis.ddp.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SARCgLritIdModel {
	
	
	private String cglritID;
	@Id
	private String sar_asp_dc_CglritID;
	
	public String getCglritID() {
		return cglritID;
	}
	public void setCglritID(String cglritID) {
		this.cglritID = cglritID;
	}
	public String getSar_asp_dc_CglritID() {
		return sar_asp_dc_CglritID;
	}
	public void setSar_asp_dc_CglritID(String sar_asp_dc_CglritID) {
		this.sar_asp_dc_CglritID = sar_asp_dc_CglritID;
	}
	@Override
	public String toString() {
		return "SARCgLritIdModel [cglritID=" + cglritID + ", sar_asp_dc_CglritID=" + sar_asp_dc_CglritID + "]";
	} 
	
	
		
}
