package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the lrit_contract_govt_mst database table.
 * 
 */
@Entity
@Table(name="lrit_contract_govt_mst")
//@NamedQuery(name="LritContractGovtMst.findAll", query="SELECT l FROM LritContractGovtMst l")
public class LritContractGovtMst implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LritContractGovtMstPK id;

	@Column(name="cg_name")
	private String cgName;

	@Column(name="iso_code")
	private String isoCode;

	@Column(name="parent_lritid")
	private String parentLritid;
	
	@Transient
	private boolean Flag;

	public LritContractGovtMst() {
	}

	public LritContractGovtMstPK getId() {
		return this.id;
	}

	public void setId(LritContractGovtMstPK id) {
		this.id = id;
	}

	public String getCgName() {
		return this.cgName;
	}

	public void setCgName(String cgName) {
		this.cgName = cgName;
	}

	public String getIsoCode() {
		return this.isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	public String getParentLritid() {
		return this.parentLritid;
	}

	public void setParentLritid(String parentLritid) {
		this.parentLritid = parentLritid;
	}
	

	public boolean isFlag() {
		return Flag;
	}

	public void setFlag(boolean flag) {
		Flag = flag;
	}

	@Override
	public String toString() {
		return "LritContractGovtMst [id=" + id + ", cgName=" + cgName + ", isoCode=" + isoCode + ", parentLritid="
				+ parentLritid + "]";
	}

	
}