package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ddp_version database table.
 * 
 */
@Entity
@Table(name="ddp_version")
//@NamedQuery(name="DdpVersion.findAll", query="SELECT d FROM DdpVersion d")
public class DdpVersion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	
	private String applicableimmediateversion;

	private String applicableregularversion;

	private String ddpversion;
	
	private String ddpimplementationtime;

	public DdpVersion() {
	}

	public String getApplicableimmediateversion() {
		return this.applicableimmediateversion;
	}

	public void setApplicableimmediateversion(String applicableimmediateversion) {
		this.applicableimmediateversion = applicableimmediateversion;
	}

	public String getApplicableregularversion() {
		return this.applicableregularversion;
	}

	public void setApplicableregularversion(String applicableregularversion) {
		this.applicableregularversion = applicableregularversion;
	}

	public String getDdpversion() {
		return this.ddpversion;
	}

	public void setDdpversion(String ddpversion) {
		this.ddpversion = ddpversion;
	}

	public String getDdpimplementationtime() {
		return ddpimplementationtime;
	}

	public void setDdpimplementationtime(String ddpimplementationtime) {
		this.ddpimplementationtime = ddpimplementationtime;
	}
	

}