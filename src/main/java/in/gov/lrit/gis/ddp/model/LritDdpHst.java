package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the lrit_ddp_hst database table.
 * 
 */
@Entity
@Table(name="lrit_ddp_hst")
@NamedQuery(name="LritDdpHst.findAll", query="SELECT l FROM LritDdpHst l")
public class LritDdpHst implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_ddp",sequenceName="seq_ddp_hst")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="seq_ddp")
	@Column(name="id")
	private Integer id;

	@Column(name="base_version")
	private String baseVersion;

	//@Column(name="file")
	private String file;

	
	@Column(name="implemented_at")
	private Timestamp implementedAt;

	@Column(name="updated_version")
	private String updatedVersion;

	public LritDdpHst() {
	}

	public String getBaseVersion() {
		return this.baseVersion;
	}

	public void setBaseVersion(String baseVersion) {
		this.baseVersion = baseVersion;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getImplementedAt() {
		return this.implementedAt;
	}

	public void setImplementedAt(Timestamp implementedAt) {
		this.implementedAt = implementedAt;
	}

	public String getUpdatedVersion() {
		return this.updatedVersion;
	}

	public void setUpdatedVersion(String updatedVersion) {
		this.updatedVersion = updatedVersion;
	}

}