package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.gov.lrit.gis.ddp.model.LritCoastalStateSo;



public interface LritCoastalStateSoRepository extends CrudRepository<LritCoastalStateSo, Long>{
	
	//@Query(value="SELECT cg_lritid, immediateversion_no, cg_regularversion_no FROM  lrit_coastal_state_so where immediateversion_no = fn_getApplicableImmediateVersion() and cg_regularversion_no = fn_getApplicableRegularVersion();", nativeQuery = true)
	
	@Query(value="SELECT css.cg_lritid, css.immediateversion_no, css.cg_regularversion_no FROM  lrit_coastal_state_so css, lrit_ddp_immediate_mst im \r\n" + 
			" where css.immediateversion_no = fn_getApplicableImmediateVersion() and css.immediateversion_no = im.immediateversion_no \r\n" + 
			" and css.cg_regularversion_no = fn_getApplicableRegularVersion(im.immediateversionpublishedat\\:\\:timestamp);", nativeQuery = true)
	List<LritCoastalStateSo> getOpenStandingOrder();

}
