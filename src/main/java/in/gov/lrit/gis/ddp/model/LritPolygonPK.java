package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the lrit_polygons database table.
 * 
 */
@Embeddable
public class LritPolygonPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="area_id")
	private String areaId;

	@Column(name="regularversion_no")
	private String regularversionNo;

	public LritPolygonPK() {
	}
	public String getAreaId() {
		return this.areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getRegularversionNo() {
		return this.regularversionNo;
	}
	public void setRegularversionNo(String regularversionNo) {
		this.regularversionNo = regularversionNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof LritPolygonPK)) {
			return false;
		}
		LritPolygonPK castOther = (LritPolygonPK)other;
		return 
			this.areaId.equals(castOther.areaId)
			&& this.regularversionNo.equals(castOther.regularversionNo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.areaId.hashCode();
		hash = hash * prime + this.regularversionNo.hashCode();
		
		return hash;
	}
}