package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;

import com.vividsolutions.jts.geom.Geometry;


/**
 * The persistent class for the lrit_port_facility_type database table.
 * 
 */
@Entity
@Table(name="lrit_port_facility_type")
@NamedQuery(name="LritPortFacilityType.findAll", query="SELECT l FROM LritPortFacilityType l")
public class LritPortFacilityType implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LritPortFacilityTypePK id;

	@Column(name="cg_lritid")
	private String cgLritid;

	@Column(name="cg_regularversion_no")
	private String cgRegularversionNo;

	private String name;

	private Geometry position;

	public LritPortFacilityType() {
	}

	public LritPortFacilityTypePK getId() {
		return this.id;
	}

	public void setId(LritPortFacilityTypePK id) {
		this.id = id;
	}

	public String getCgLritid() {
		return this.cgLritid;
	}

	public void setCgLritid(String cgLritid) {
		this.cgLritid = cgLritid;
	}

	public String getCgRegularversionNo() {
		return this.cgRegularversionNo;
	}

	public void setCgRegularversionNo(String cgRegularversionNo) {
		this.cgRegularversionNo = cgRegularversionNo;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Geometry getPosition() {
		return this.position;
	}

	public void setPosition(Geometry position) {
		this.position = position;
	}

}