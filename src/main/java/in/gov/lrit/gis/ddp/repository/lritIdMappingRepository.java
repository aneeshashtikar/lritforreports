package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.gis.ddp.model.lritIdMapping;


@Repository
public interface lritIdMappingRepository extends CrudRepository<lritIdMapping, Long> {
	
	//@Query(value = "SELECT DISTINCT ON (cg_lritid) lrit_contract_govt_mst.cg_lritid, lrit_contract_govt_mst.cg_name FROM lrit_polygons,  lrit_contract_govt_mst where "
	//		+ "lrit_contract_govt_mst.cg_lritid = lrit_polygons.cg_lritid lrit_contract_govt_mst.regularversion_no = (Select fn_getapplicableregularversion(); ", nativeQuery = true)
	
	@Query(value = "SELECT DISTINCT ON (cg_lritid) lrit_contract_govt_mst.cg_lritid, lrit_contract_govt_mst.cg_name FROM lrit_contract_govt_mst where "
			+ "lrit_contract_govt_mst.regularversion_no = (Select fn_getapplicableregularversion()); ", nativeQuery = true)
	List<lritIdMapping> findAllLritIds();
	
	@Query(value = "SELECT cg_lritid FROM public.lrit_datacentre_info where dc_lritid='3065' and cg_regularversion_no = (Select fn_getapplicableregularversion());", nativeQuery = true)
	List<String> findUserLritIdsByDC();

}
