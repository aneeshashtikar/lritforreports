package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.gis.ddp.model.SARCgLritIdModel;

public interface SARCgLritIdRepository  extends CrudRepository<SARCgLritIdModel, Long>{
	
	
		@Query(value = "Select sarservice_lritid as sar_asp_dc_CglritID, cg_lritId as cglritID from lrit_sarservice where cg_lritId in :cg_lritid and  regularversion_no = (Select fn_getapplicableregularversion())",  nativeQuery = true)
		List<SARCgLritIdModel> findSarServiceLritId(@Param("cg_lritid") List<String> cg_lritId);	
		
		@Query(value = "Select dc_lritid as sar_asp_dc_CglritID, cg_lritId as cglritID from lrit_datacentre_info where cg_lritId in :cg_lritid and  regularversion_no = (Select fn_getapplicableregularversion())",  nativeQuery = true)
		List<SARCgLritIdModel> findDcLritId(@Param("cg_lritid") List<String> cg_lritId);
		
		@Query(value = "Select asp_lritid as sar_asp_dc_CglritID, cg_lritId as cglritID from lrit_aspinfo where cg_lritId in :cg_lritid and  regularversion_no = (Select fn_getapplicableregularversion())",  nativeQuery = true)
		List<SARCgLritIdModel> findAspLritId(@Param("cg_lritid") List<String> cg_lritId);
		
		
		@Query(value = "Select sarservice_lritid as sar_asp_dc_CglritID, cg_lritId as cglritID from lrit_sarservice where sarservice_lritid = :cg_lritid and  regularversion_no = (Select fn_getapplicableregularversion())",  nativeQuery = true)
		SARCgLritIdModel findLritIdFromSarServiceID(@Param("cg_lritid") String cg_lritId);	
		
		@Query(value = "Select dc_lritid as sar_asp_dc_CglritID, cg_lritId as cglritID from lrit_datacentre_info where dc_lritid = :cg_lritid and  regularversion_no = (Select fn_getapplicableregularversion())",  nativeQuery = true)
		SARCgLritIdModel findLritIdFromDcId(@Param("cg_lritid") String cg_lritId);
		
		@Query(value = "Select asp_lritid as sar_asp_dc_CglritID, cg_lritId as cglritID from lrit_aspinfo where asp_lritid = :cg_lritid and  regularversion_no = (Select fn_getapplicableregularversion())",  nativeQuery = true)
		SARCgLritIdModel findLritIdFromAspId(@Param("cg_lritid") String cg_lritId);
		
		
}
