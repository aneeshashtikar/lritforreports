/**
 * @(#)lritCGMasterModel.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.gis.ddp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class lritIdMapping {

	@Id
	@Column(name = "cg_lritid")
	private String cglritID;
		
	@Column(name = "cg_name")
	private String cglritName;
		
	public lritIdMapping() {
		
	}

	public lritIdMapping(String cglritID, String cglritName) {
		
		this.cglritID = cglritID;
		this.cglritName = cglritName;
	}

	public String getCglritID() {
		return cglritID;
	}

	public void setCglritID(String cglritID) {
		this.cglritID = cglritID;
	}

	public String getCglritName() {
		return cglritName;
	}

	public void setCglritName(String cglritName) {
		this.cglritName = cglritName;
	}

	
	
}
