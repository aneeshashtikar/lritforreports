package in.gov.lrit.gis.ddp.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import in.gov.lrit.gis.ddp.model.LritPolygon;


public interface PolygonRepository extends JpaRepository<LritPolygon, String>{	
	 @Query(value = "Select * from lrit_polygons where cg_lritid = :cglritid and type='CoastalSea' and regularversion_no= :regularvesrion and cg_regularversion_no= :regularvesrion limit 1", nativeQuery = true)
	  LritPolygon findBycgLritidAndRegularversionNo(String cglritid,String regularvesrion);	
	 
	 @Query(value = "Select area_id from lrit_polygons where cg_lritid = :cglritid and type=:areaType and regularversion_no= :regularvesrion ", nativeQuery = true)
	  List<String> getCountrypolygons(String cglritid,String regularvesrion, String areaType);
}

