/**
 * @(#)SOFetchModel.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.gis.ddp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Geometry;

@Entity
@Table(name = "lrit_polygons")
@IdClass(SOFetchPrimaryKey.class)
public class SOFetchModel {

	@Id
	@Column(name = "regularversion_no")
	private String regularversion_no;
	@Id
	private String area_id;
	@Column(name = "cg_lritid")
	private String cglritid;

	private String caption;
	
	@Type(type = "com.vividsolutions.jts.geom.Geometry")
	private Geometry poslist;
	private String type;

	private String cg_regularversion_no;
	
	public SOFetchModel() {

	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public Geometry getPoslist() {
		return poslist;
	}

	public void setPoslist(Geometry poslist) {

		this.poslist = poslist;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCg_lritid() {
		return cglritid;
	}

	public void setCg_lritid(String cg_lritid) {
		this.cglritid = cg_lritid;
	}

	
	
	public String getRegularversion_no() {
		return regularversion_no;
	}

	public void setRegularversion_no(String regularversion_no) {
		this.regularversion_no = regularversion_no;
	}

	public String getCglritid() {
		return cglritid;
	}

	public void setCglritid(String cglritid) {
		this.cglritid = cglritid;
	}

	public String getCg_regularversion_no() {
		return cg_regularversion_no;
	}

	public void setCg_regularversion_no(String cg_regularversion_no) {
		this.cg_regularversion_no = cg_regularversion_no;
	}

	public String getArea_id() {
		return area_id;
	}
	public void setArea_id(String area_id) {
		this.area_id = area_id;
	}

	@Override
	public String toString() {
		return "SOFetchModel [regularversion_no=" + regularversion_no + ", area_id=" + area_id + ", cglritid=" + cglritid
				+ ", caption=" + caption + ", poslist=" + poslist + ", type=" + type + "]";
	} 
	
	
}
