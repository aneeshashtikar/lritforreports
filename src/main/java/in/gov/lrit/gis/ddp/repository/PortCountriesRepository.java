package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.gov.lrit.gis.ddp.model.PortCountries;
import in.gov.lrit.gis.ddp.model.PortModel;

public interface PortCountriesRepository extends CrudRepository<PortCountries, Long>{
	

	@Query(value = "Select DISTINCT(cg_lritid)  from lrit_ports where regularversion_no = (Select fn_getapplicableregularversion()) ORDER BY cg_lritid; ", nativeQuery = true)
	List<PortCountries> findPortCountries();
	

}
