package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the lrit_port_facility_type database table.
 * 
 */
@Embeddable
public class LritPortFacilityTypePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="regularversion_no")
	private String regularversionNo;

	private String imoportfacilitynumber;

	public LritPortFacilityTypePK() {
	}
	public String getRegularversionNo() {
		return this.regularversionNo;
	}
	public void setRegularversionNo(String regularversionNo) {
		this.regularversionNo = regularversionNo;
	}
	public String getImoportfacilitynumber() {
		return this.imoportfacilitynumber;
	}
	public void setImoportfacilitynumber(String imoportfacilitynumber) {
		this.imoportfacilitynumber = imoportfacilitynumber;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof LritPortFacilityTypePK)) {
			return false;
		}
		LritPortFacilityTypePK castOther = (LritPortFacilityTypePK)other;
		return 
			this.regularversionNo.equals(castOther.regularversionNo)
			&& this.imoportfacilitynumber.equals(castOther.imoportfacilitynumber);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.regularversionNo.hashCode();
		hash = hash * prime + this.imoportfacilitynumber.hashCode();
		
		return hash;
	}
}