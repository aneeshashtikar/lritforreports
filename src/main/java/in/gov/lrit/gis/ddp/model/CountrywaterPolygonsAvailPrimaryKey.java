/**
 * @(#)CountrywaterPolygonsAvailPrimaryKey.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;

public class CountrywaterPolygonsAvailPrimaryKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cglritID;
	private String type;
	

}
