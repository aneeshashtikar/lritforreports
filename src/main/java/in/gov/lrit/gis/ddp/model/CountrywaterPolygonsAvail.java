/**
 * @(#)CountrywaterPolygonsAvail.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.gis.ddp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OrderBy;



@Entity
@IdClass(CountrywaterPolygonsAvailPrimaryKey.class)
public class CountrywaterPolygonsAvail {

	@Id
	@Column(name = "cg_lritid")
	private String cglritID;
	@Id
	private String type;
	
	
	public CountrywaterPolygonsAvail() {

	}	
	
	public String getCglritID() {
		return cglritID;
	}
	public void setCglritID(String cglritID) {
		this.cglritID = cglritID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	} 
}
