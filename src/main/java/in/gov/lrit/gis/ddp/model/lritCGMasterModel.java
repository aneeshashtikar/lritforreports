/**
 * @(#)lritCGMasterModel.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.gis.ddp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lrit_contract_govt_mst")

public class lritCGMasterModel {

	@Id
	@Column(name = "cg_lritid")
	private String cglritid;
	@Column(name = "ddpversion_no")
	private String ddpversionNo;
	@Column(name = "iso_code")
	private String isoCode;
	@Column(name = "cg_name")
	private String cgName;
	@Column(name = "parent_lritid")
	private String parentLritid;

	public lritCGMasterModel(String cglritid, String ddpversionNo, String isoCode, String cgName, String parentLritid) {

		this.cglritid = cglritid;
		this.ddpversionNo = ddpversionNo;
		this.isoCode = isoCode;
		this.cgName = cgName;
		this.parentLritid = parentLritid;
	}

	public lritCGMasterModel() {

	}

	public String getCglritid() {
		return cglritid;
	}

	public void setCglritid(String cglritid) {
		this.cglritid = cglritid;
	}

	public String getDdpversionNo() {
		return ddpversionNo;
	}

	public void setDdpversionNo(String ddpversionNo) {
		this.ddpversionNo = ddpversionNo;
	}

	public String getIsoCode() {
		return isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	public String getCgName() {
		return cgName;
	}

	public void setCgName(String cgName) {
		this.cgName = cgName;
	}

	public String getParentLritid() {
		return parentLritid;
	}

	public void setParentLritid(String parentLritid) {
		this.parentLritid = parentLritid;
	}

}
