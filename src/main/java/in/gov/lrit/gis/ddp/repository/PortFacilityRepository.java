package in.gov.lrit.gis.ddp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.gis.ddp.model.LritPort;
import in.gov.lrit.gis.ddp.model.LritPortFacilityType;
import in.gov.lrit.gis.ddp.model.PortModel;

public interface PortFacilityRepository extends JpaRepository<LritPortFacilityType, String>{	
	
		
	 @Query(value = "Select * from lrit_port_facility_type where cg_lritid = :cglritid and regularversion_no= :regularversion", nativeQuery = true)
	  List<LritPortFacilityType> findBycgLritidAndRegularversionNo(String cglritid,String regularversion);	
	

}

