package in.gov.lrit.gis.ddp.model;

import java.io.Serializable;
import javax.persistence.*;

import com.vividsolutions.jts.geom.Geometry;


/**
 * The persistent class for the lrit_ports database table.
 * 
 */
@Entity
@Table(name="lrit_ports")
//@NamedQuery(name="LritPort.findAll", query="SELECT l FROM LritPort l")
public class LritPort implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LritPortPK portid;

	@Column(name="cg_lritid")
	private String cgLritid;

	@Column(name="cg_regularversion_no")
	private String cgRegularversionNo;

	private String name;

	private Geometry position;

	public LritPort() {
	}

	/*
	 * public LritPortPK getId() { return id; }
	 * 
	 * public void setId(LritPortPK id) { this.id = id; }
	 */

	
	public String getCgLritid() {
		return this.cgLritid;
	}

	public LritPortPK getPortid() {
		return portid;
	}

	public void setPortid(LritPortPK portid) {
		this.portid = portid;
	}

	public void setCgLritid(String cgLritid) {
		this.cgLritid = cgLritid;
	}

	public String getCgRegularversionNo() {
		return this.cgRegularversionNo;
	}

	public void setCgRegularversionNo(String cgRegularversionNo) {
		this.cgRegularversionNo = cgRegularversionNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Geometry getPosition() {
		return this.position;
	}

	public void setPosition(Geometry position) {
		this.position = position;
	}

}