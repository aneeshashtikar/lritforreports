/**
 * @BillableItemServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 29-Aug-2019
 */
package in.gov.lrit.billing.service.payment.billableitem;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.billing.dao.payment.billableitem.BillableItemRepository;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;

/**
 * @author lrit-billing
 *
 */
@Service
public class BillableItemServiceImpl implements BillableItemService {

	@Autowired
	BillableItemRepository billableItemRepository;

	@Override
	public List<BillableItem> getBillableItemsOfService(Integer billingServiceId) {
		return billableItemRepository.findAllByBillingServiceId(billingServiceId);
	}

	@Override
	public BillableItem getBillableItem(Integer billableItemId) {
		return billableItemRepository.findById(billableItemId).get();
	}
}
