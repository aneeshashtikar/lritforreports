/**
 * @InvoiceAbstractServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 30-Oct-2019
 */
package in.gov.lrit.billing.service.billingservice.invoice;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.billing.dao.billingservice.ServiceRepository;
import in.gov.lrit.billing.dao.billingservice.invoice.ApprovedInvoiceRepository;
import in.gov.lrit.billing.dao.billingservice.invoice.GeneratedInvoiceRepository;
import in.gov.lrit.billing.dao.billingservice.invoice.InvoiceRepository;
import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryLogRepository;
import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.invoice.ApprovedInvoice;
import in.gov.lrit.billing.model.billingservice.invoice.GeneratedInvoice;
import in.gov.lrit.billing.model.billingservice.invoice.Invoice;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.billing.service.ServiceAbstractServiceImpl;
import in.gov.lrit.mail.LRITMailSender;
import in_.gov.lrit.billing.dto.InvoiceDto;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * @author lrit-billing
 *
 */
@org.springframework.stereotype.Service("invoiceAbstractServiceImpl")
public class InvoiceAbstractServiceImpl extends ServiceAbstractServiceImpl implements InvoiceAbstractService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(InvoiceAbstractServiceImpl.class);

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private GeneratedInvoiceRepository generatedInvoiceRepository;

	@Autowired
	private ApprovedInvoiceRepository approvedInvoiceRepository;

	@Autowired
	private LRITMailSender lritMailSender;

	@Autowired
	private PaymentBillableItemCategoryLogRepository paymentBillableItemCategoryLogRepo;
	
	@Autowired
	@Qualifier("portalDataSource")
	private javax.sql.DataSource portalDataSource;

	@Value("${ndc.emailid}")
	private String ndcEmailId;

	@Value("${ddg.emailid}")
	private String ddgEmailId;

	// FOR JRXML PATH OF INVOICE TO GENERATE PDF
	@Value("${invoice.jrxml.path}")
	private String invoiceJrxmlPath;

	/**
	 * RETURN INVOICE FROM DATABASE
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @return
	 */
	@Override
	public Invoice getInvoice(Integer invoiceId) {
		logger.info("INSIDE getInvoice()");


		Invoice invoice = null;
		if (serviceRepository.findById(invoiceId).isPresent()) {
			invoice = (Invoice) serviceRepository.findById(invoiceId).get();
			
			List<BillableItem> billableItems = invoice.getBillingBillableItems(); 
			for ( BillableItem billableItem : billableItems) {
				
				int bic = billableItem.getBillingPaymentBillableItemCategory().getBillableItemCategoryId();
				Date fromDate = billableItem.getDateRangeFrom(); 
				Date toDate = billableItem.getDateRangeTo(); 
				Double rate = paymentBillableItemCategoryLogRepo.getBillableItemCategoryRate(bic, fromDate, toDate) ;
				
				
				
				billableItem.setRate(rate);
			}
			
			logger.info("invoice.getBillableItems"+invoice.getBillingBillableItems());
			logger.info("FETCHED INVOICE : " + invoice);
			Contract contract = invoice.getBillingContract();
			Integer contractTypeId = contract.getBillingContractType().getContractTypeId();
			Agency agency2 = contract.getAgency2();
			String agency2Code = agency2.getAgencyCode();
			String agencyName = contractService.getAgencyName(contractTypeId, agency2Code);
			invoice.getBillingContract().setAgency2Name(agencyName);
		}


		return invoice;
	}

	/**
	 * METHOD RETURNS INVOICE NUMER FORMAT AS MENTOINED BY DGS EX(DGS/LRIT/1/2020)
	 * 
	 * @author lrit-billing
	 * @param year
	 * @return
	 */
	@Override
	public String getInvoiceNumber(int year) {
		logger.info("INSIDE GET INVOICE NUMBER METHOD");
		logger.info("year : " + year);

		int totalInvoices = invoiceRepository.getInvoiceCount(year);

		return "DGS/LRIT/" + (totalInvoices + 1) + "/" + year;
	}

	/**
	 * SAVE INVOICE TO DB
	 * 
	 * @author lrit-billing
	 * @param invoice
	 * @return
	 * @throws SQLException
	 * @throws JRException
	 * @throws IOException
	 */
//	@Transactional(value = "portalTransactionManager")
	@Override
	public Invoice generateInvoice(Invoice invoice) throws SQLException, JRException, IOException {
		logger.info("INSIDE generateInvoice()");
		logger.info("invoice : " + invoice.toString());

		// ****************
		// Still Unknown status regarding year from where to fetch contract based year
		// or current year based on GENERATION YEAR
		Integer year = Calendar.getInstance().get(Calendar.YEAR);
		invoice.setInvoiceNo(getInvoiceNumber(year));

		Invoice persistedInvoice = (Invoice) saveService(invoice);

		// save GeneratedInvoice
		GeneratedInvoice generatedInvoice = saveGeneratedInvoicePDF(persistedInvoice);
		return persistedInvoice;
	}

	/**
	 * SAVE GENERATED PDF OF INVOICE
	 * 
	 * @author lrit-billing
	 * @param invoice
	 * @return
	 * @throws SQLException
	 * @throws JRException
	 * @throws IOException
	 */
	@Override
	public GeneratedInvoice saveGeneratedInvoicePDF(Invoice invoice) throws SQLException, JRException, IOException {
		logger.info("INSIDE saveGeneratedInvoicePDF()");
		logger.info("invoice " + invoice);

		GeneratedInvoice generatedInvoicePDF = new GeneratedInvoice();
		byte[] invoicePDF = exportPdfFile(invoice);
		generatedInvoicePDF.setGeneratedCopy(invoicePDF);
		generatedInvoicePDF.setService(invoice);

		logger.info("Before Persist generatedInvoice : " + generatedInvoicePDF);
		generatedInvoicePDF = generatedInvoiceRepository.save(generatedInvoicePDF);

		logger.info("generatedInvoice : " + generatedInvoicePDF);
		return generatedInvoicePDF;
	}

	/**
	 * RETURNS ALL INVOICES
	 * 
	 * @author lrit-billing
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Override
	public List<InvoiceDto> fetchInvoices(Integer contractType, Date fromDate, Date toDate) {
		logger.info("INSIDE fetchInvoices");
		logger.info("contractType : " + contractType);
		logger.info("fromDate : " + fromDate);
		logger.info("toDate : " + toDate);

		List<InvoiceDto> invoices = null;
		if (fromDate == null || toDate == null) {
			invoices = invoiceRepository.findAllInvoiceDtoByContractType(contractType);
		} else {
			invoices = invoiceRepository.findAllInvoiceDtoByContractType(contractType, fromDate, toDate);
		}
		return invoices;
	}

	/**
	 * METHOD TO PROVIDE LATEST GENERATED PDF OF INVOICE
	 * 
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @return
	 */
//	public GeneratedInvoice getLastGeneratedInvoiceCopy(Integer invoiceId) {
//		GeneratedInvoice generatedInvoice = new GeneratedInvoice();
//		return generatedInvoice;
//
//	}

	/**
	 * METHOD TO RETURN LATEST APPROVED GENERATED COPY OF INVOICE
	 * 
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @return
	 */
//	public ApprovedInvoice getLastApprovedInvoiceCopy(Integer invoiceId) {
//		ApprovedInvoice approvedInvoice = new ApprovedInvoice();
//		return approvedInvoice;
//	}

	/**
	 * SEND MAIL TO DG WITH INVOICE AS PDF
	 *
	 * @author lrit-billing
	 * @param invoicePara
	 * @param invoiceId
	 * @return
	 * @throws MessagingException
	 */
	@Transactional(value = "portalTransactionManager")
	@Override
	public Invoice sendInvoiceToDG(Invoice invoicePara, Integer invoiceId) throws MessagingException {

		logger.info("INSIDE sendInvoiceToDG");
		logger.info("invoicePara : " + invoicePara);
		logger.info("invoiceId : " + invoiceId);

		Integer statusId = 3;
		Invoice invoice = getInvoice(invoiceId);

		invoice = (Invoice) setServiceStatus(invoice, statusId,
				invoicePara.getBillingServiceStatuses().get(0).getRemarks());

		// ****************
		// DETAILS TO BE SEND TO DG IS UNKNOWN

		String subject = "Kindly approve the Invoice";
		String mailBody = "Dear sir, <br> Kindly approve and sign the attached invoice.<br><br>";

		GeneratedInvoice latestGeneratedInvoice = getLatestGeneratedInvoice(invoiceId);
		byte[] generatedCopy = latestGeneratedInvoice.getGeneratedCopy();

		DataSource datasource = new ByteArrayDataSource(generatedCopy, "application/pdf");
		String attachmentName = "Invoice " + invoice.getInvoiceNo();

//		lritMailSender.sendEmailWithAttachment(from, to, subject, mailBody, datasource, attachmentName);
		lritMailSender.sendEmailWithAttachment(ndcEmailId, ddgEmailId, subject, mailBody, datasource, attachmentName);

//			lritMailSender.sendEmail(from, to, subject, mailBody);
		invoice = invoiceRepository.save(invoice);

		return invoice;
	}

	/**
	 * MAIL APPROVED INVOICE TO AGENCY FOR PAYMENT
	 *
	 * @author lrit-billing
	 * @param mailBody
	 * @param remarks
	 * @param invoiceId
	 * @throws MessagingException
	 */
	@Transactional(value = "portalTransactionManager")
	@Override
	public void mailInvoiceToAgency(String mailBody, String remarks, Integer invoiceId) throws MessagingException {
		logger.info("INSIDE mailInvoiceToAgency");
		logger.info("mailBody : " + mailBody);
		logger.info("remarks : " + remarks);
		logger.info("invoiceId : " + invoiceId);

		Integer statusId = 5;
		Invoice invoice = getInvoice(invoiceId);

		invoice = (Invoice) setServiceStatus(invoice, statusId, remarks);

		// EMAIL ID OF THE AGENCY TO WHOM MAIL TO BE SEND
//		String agencyMailId = invoice.getBillingContract().getAgency2().getGeneralEmail();
		String agencyMailId = invoice.getBillingContract().getAgency2().getFinancialEmail();
//		System.out.println(mailBody + "mailBody");
//		System.out.println(remarks + "mailBody");
//		System.out.println(agencyMailId + "mailBody");

		// ****************
		// DETAILS TO BE SEND TO DG IS UNKNOWN
		// String senderMailId = "yogendra@cdac.in";
		// String receiverMailId = "yogendra@cdac.in";
		// String subject = "Regarding payment";
		// String mailBody = "";
//		DataSource datasource = new ByteArrayDataSource(invoice.getApprovedCopy(), "application/pdf");
//		String attachmentName = "Invoice " + invoice.getInvoiceNo();
//		try {
//			lritMailSender.sendEmailWithAttachment(from, to, subject, mailBody, datasource, attachmentName);
//		} catch (MessagingException e) {
//			e.printStackTrace();
//		}

		String subject = "Regarding payment against invoice";

		ApprovedInvoice latestApprovedInvoice = getLatestApprovedInvoice(invoiceId);
		byte[] approvedCopy = latestApprovedInvoice.getApprovedCopy();

//		DataSource datasource = new ByteArrayDataSource(invoice.getApprovedCopy(), "application/pdf");
		DataSource datasource = new ByteArrayDataSource(approvedCopy, "application/pdf");
		String attachmentName = "Invoice-" + invoice.getInvoiceNo();

		logger.info("agencyMailId " + agencyMailId + " attachmentName " + attachmentName);

		mailBody += "<br> <br>";

//		lritMailSender.sendEmailWithAttachment(from, agencyMailId, subject, mailBody, datasource, attachmentName);

		lritMailSender.sendEmailWithAttachment(ndcEmailId, agencyMailId, subject, mailBody, datasource, attachmentName);

		// DataSource datasource = new ByteArrayDataSource(invoice.getGeneratedCopy(),
		// "application/pdf");
		// String attachmentName = "Invoice " + invoice.getInvoiceNo();
//		try {
		// lritMailSender.sendEmailWithAttachment(senderMailId, receiverMailId, subject,
		// mailBody, datasource,
		// attachmentName);
//			lritMailSender.sendEmail(from, to, subject, mailBody);
//		}
		/*
		 * catch (MessagingException e) { e.printStackTrace(); }
		 */
//		catch (Exception e) {
//			e.printStackTrace();
//		}

		invoice = invoiceRepository.save(invoice);
	}

	/**
	 * TO SEND MAIL TO AGENCY ALONG WITH CC
	 * 
	 * @author lrit-billing
	 * @param mailBody
	 * @param remarks
	 * @param invoiceId
	 * @param cc
	 * @throws MessagingException
	 */
	@Override
	public void mailInvoiceToAgency(String mailBody, String remarks, Integer invoiceId, String cc)
			throws MessagingException {
		logger.info("INSIDE mailInvoiceToAgency");
		logger.info("mailBody : " + mailBody);
		logger.info("remarks : " + remarks);
		logger.info("invoiceId : " + invoiceId);
		logger.info("cc : " + cc);

		Integer statusId = 5;
		Invoice invoice = getInvoice(invoiceId);

		invoice = (Invoice) setServiceStatus(invoice, statusId, remarks);

		// EMAIL ID OF THE AGENCY TO WHOM MAIL TO BE SEND
		String agencyMailId = invoice.getBillingContract().getAgency2().getFinancialEmail();

		String subject = "Regarding payment against invoice";

		ApprovedInvoice latestApprovedInvoice = getLatestApprovedInvoice(invoiceId);
		byte[] approvedCopy = latestApprovedInvoice.getApprovedCopy();

		DataSource datasource = new ByteArrayDataSource(approvedCopy, "application/pdf");
		String attachmentName = "Invoice-" + invoice.getInvoiceNo();

		logger.info("agencyMailId " + agencyMailId + " attachmentName " + attachmentName);

		mailBody += "<br> <br>";

		lritMailSender.sendEmailWithAttachment(ndcEmailId, agencyMailId, subject, mailBody, datasource, attachmentName,
				cc);

		invoice = invoiceRepository.save(invoice);
	}

	/**
	 * UPDATE COUNT OF MESSAGES
	 *
	 * @author lrit-billing
	 * @param service
	 * @param billingServiceId
	 * @return
	 * @throws SQLException
	 * @throws JRException
	 * @throws IOException
	 */
	@Override
	public Service updateMessageCount(Service service, Integer billingServiceId)
			throws SQLException, JRException, IOException {
		logger.info("INSIDE updateMessageCount");
		logger.info("service " + service);

		Invoice invoice = (Invoice) super.updateMessageCount(service, billingServiceId);
		GeneratedInvoice generatedInvoice = saveGeneratedInvoicePDF(invoice);

		return invoice;
	}

	/**
	 * UPDATE INVOICE PAYMENT AGAINST AGENCY
	 *
	 * @author lrit-billing
	 * @param formInvoice
	 * @param consumers
	 * @param invoiceId
	 * @return
	 */
	@Override
	public Invoice updateInvoicePayment(Invoice formInvoice, List<String> consumers, Integer invoiceId) {

		logger.info("INSIDE updateInvoicePayment");
		logger.info("formInvoice : " + formInvoice);
		logger.info("consumers : " + consumers);
		logger.info("invoiceId : " + invoiceId);

		Integer statusId = 7;
		Invoice fetchedInvoice = getInvoice(invoiceId);

		fetchedInvoice = (Invoice) updatePayment(fetchedInvoice, formInvoice, consumers);

		// TO CHECK FULL PAYMENT OR PARTIAL PAYMENT
		if (getRemainingAmountService(fetchedInvoice) == 0)
			statusId = 6;

		fetchedInvoice = (Invoice) setServiceStatus(fetchedInvoice, statusId,
				formInvoice.getBillingServiceStatuses().get(0).getRemarks());

		fetchedInvoice = invoiceRepository.save(fetchedInvoice);

		return fetchedInvoice;
	}

	/**
	 * OVERRIDED METHOD TO FETCH MESSAGE COUNT PER BILLABLE ITEM CATEGORY
	 *
	 * @author lrit-billing
	 * @param provider
	 * @param consumer
	 * @param paymentBillableItemCategory
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Override
	public BillableItem getBillableItem(String provider, String consumer,
			PaymentBillableItemCategory paymentBillableItemCategory, Date fromDate, Date toDate) {

		BillableItem billableItem = super.getBillableItem(provider, consumer, paymentBillableItemCategory, fromDate,
				toDate);

		// get contractTypeId
		Integer contractTypeId = paymentBillableItemCategory.getBillingContractType().getContractTypeId();

		// Get billable item category ID
		int billableItemCategoryId = paymentBillableItemCategory.getBillableItemCategoryId();

		// Get system count
		int systemCount = getBillableItemSystemCount(provider, consumer, fromDate, toDate, billableItemCategoryId,
				contractTypeId);
		billableItem.setSystemCount(systemCount);

		billableItem.setManualCount(systemCount);
		// Rate to be fetched from billing billable item category log on 16/01/2020
		double rate = paymentBillableItemCategory.getRate();
		billableItem.setCost(systemCount * rate);
		billableItem.setRate(rate);
		/*
		 * billableItem.setProvider(provider); billableItem.setConsumer(consumer);
		 * String consumerName = contractService.getStakeholderName(consumer,
		 * contractTypeId); billableItem.setConsumerName(consumerName); String
		 * providerName = contractService.getStakeholderName(provider, contractTypeId);
		 * billableItem.setProviderName(providerName);
		 * billableItem.setBillingPaymentBillableItemCategory(
		 * paymentBillableItemCategory);
		 */
		return billableItem;
	}

	/**
	 * THIS METHOD RETURNS HASHED BILLABLE ITEMS BASED ON GIVEN CONTRACTID AND GIVEN
	 * DATE RANGE
	 * 
	 * @author lrit-billing
	 * @param contractId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Override
	public HashMap<String, HashMap<String, List<BillableItem>>> getHashedBillableItems(Integer contractId,
			Date fromDate, Date toDate) {

		logger.info("INSIDE getHashedBillableItems");
		logger.info("contractId : " + contractId);
		logger.info("fromDate : " + fromDate);
		logger.info("toDate : " + toDate);

		Contract contract = contractService.getContract(contractId);
		// System.out.println("Contract " + contract );

		HashMap<String, HashMap<String, List<BillableItem>>> billableItems = new HashMap<String, HashMap<String, List<BillableItem>>>();

		java.sql.Date billableItemFromDate = null;
		java.sql.Date billableItemToDate = null;

		java.sql.Date providerFromDate = null;
		java.sql.Date providerToDate = null;

		java.sql.Date consumerFromDate = null;
		java.sql.Date consumerToDate = null;

		java.sql.Date fromDateSql = new java.sql.Date(fromDate.getTime());
		java.sql.Date toDateSql = new java.sql.Date(toDate.getTime());

		List<ContractStakeholder> providers = contractService.getValidContractStakeholderListByDates(contractId, true,
				fromDateSql, toDateSql);
		List<ContractStakeholder> consumers = contractService.getValidContractStakeholderListByDates(contractId, false,
				fromDateSql, toDateSql);

		logger.info("PROVIDERS : " + providers);
		logger.info("CONSUMERS : " + consumers);

		ContractType contractType = contract.getBillingContractType();

		List<PaymentBillableItemCategory> paymentBillableItemCategorys = paymentBillableItemCategoryLogRepo
				.findPbicByBillingContractTypeAndValidFromAndValidTill(contractType, fromDate, toDate);
		String consumerName = null;
		String providerName = null;
		BillableItem billableItem = null;
		for (ContractStakeholder providerCS : providers) {
			HashMap<String, List<BillableItem>> consumerHash = new HashMap<String, List<BillableItem>>();
			String provider = providerCS.getMemberId();
			providerFromDate = providerCS.getValidFrom();
			providerToDate = providerCS.getValidTo();
			for (ContractStakeholder consumerCS : consumers) {
				String consumer = consumerCS.getMemberId();
				consumerFromDate = consumerCS.getValidFrom();
				consumerToDate = consumerCS.getValidTo();
				List<BillableItem> messages = new ArrayList<BillableItem>();

				for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategorys) {

					billableItemFromDate = highestDate(providerFromDate, consumerFromDate, fromDateSql);
					billableItemToDate = lowestDate(providerToDate, consumerToDate, toDateSql);
					billableItem = getBillableItem(provider, consumer, paymentBillableItemCategory,
							billableItemFromDate, billableItemToDate);
					// if (billableItem.getSystemCount() > 0) {
					messages.add(billableItem);
					// }
					int bic = paymentBillableItemCategory.getBillableItemCategoryId();
					Double rate = paymentBillableItemCategoryLogRepo.getBillableItemCategoryRate(bic, billableItemFromDate, billableItemToDate) ;
					billableItem.setRate(rate);
					
//					logger.info("billableItem " + billableItem);
					consumerName = billableItem.getConsumerName();
					providerName = billableItem.getProviderName();
				}

				// Getting null pointer exception while accessing billableItem
				/*
				 * consumer = consumer.concat(" - " + billableItem.getConsumerName());
				 */
				consumer = consumer.concat(" - " + consumerName);
				consumerHash.put(consumer, messages);
			}
			// Getting null pointer exception while accessing billableItem
			/*
			 * billableItems.put(provider.concat(" - " + billableItem.getProviderName()),
			 * consumerHash);
			 */
			billableItems.put(provider.concat(" - " + providerName), consumerHash);
		}
		return billableItems;
	}

	@Override
	public List<BillableItem> getBillableItems(Integer contractId, Date fromDate, Date toDate) {

		Contract contract = contractService.getContract(contractId);
		List<BillableItem> billableItems = new ArrayList<BillableItem>();

		java.sql.Date fromDateSql = new java.sql.Date(fromDate.getTime());
		java.sql.Date toDateSql = new java.sql.Date(toDate.getTime());

		List<ContractStakeholder> providers = contractService.getValidContractStakeholderListByDates(contractId, true,
				fromDateSql, toDateSql);
		List<ContractStakeholder> consumers = contractService.getValidContractStakeholderListByDates(contractId, false,
				fromDateSql, toDateSql);
		java.sql.Date billableItemFromDate = null;
		java.sql.Date billableItemToDate = null;

		java.sql.Date providerFromDate = null;
		java.sql.Date providerToDate = null;

		java.sql.Date consumerFromDate = null;
		java.sql.Date consumerToDate = null;

		ContractType contractType = contract.getBillingContractType();

		List<PaymentBillableItemCategory> paymentBillableItemCategorys = paymentBillableItemCategoryLogRepo
				.findPbicByBillingContractTypeAndValidFromAndValidTill(contractType, fromDate, toDate);

		for (ContractStakeholder providerCS : providers) {
			String provider = providerCS.getMemberId();
			providerFromDate = providerCS.getValidFrom();
			providerToDate = providerCS.getValidTo();
			for (ContractStakeholder consumerCS : consumers) {
				String consumer = consumerCS.getMemberId();
				consumerFromDate = consumerCS.getValidFrom();
				consumerToDate = consumerCS.getValidTo();
				for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategorys) {

					billableItemFromDate = highestDate(providerFromDate, consumerFromDate,
							new java.sql.Date(fromDate.getTime()));
					billableItemToDate = lowestDate(providerToDate, consumerToDate,
							new java.sql.Date(toDate.getTime()));
					BillableItem billableItem = getBillableItem(provider, consumer, paymentBillableItemCategory,
							billableItemFromDate, billableItemToDate);

					// if (billableItem.getSystemCount() > 0) {

					billableItems.add(billableItem);
					// }
				}

			}
			// billableItems.put(provider.concat(" - " + getCGName(provider)),
			// consumerHash);
		}
		return billableItems;
	}

	/**
	 * RETURNS BYTE ARRAY FROM JASPER GENERATED PDF
	 *
	 * @author lrit-billing
	 * @param invoice
	 * @return
	 * @throws SQLException
	 * @throws JRException
	 * @throws IOException
	 */
	@Override
	public byte[] exportPdfFile(Invoice invoice) throws SQLException, JRException, IOException {
		logger.info("INSIDE exportPdfFile()");
		logger.info("invoice : " + invoice);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		invoice = getInvoice(invoice.getBillingServiceId());

		Connection conn = portalDataSource.getConnection();

		if (invoice != null) {
			logger.info("IF INVOICE EXISTS JASPER REPORT GENERATION");

			String agency2Name = invoice.getBillingContract().getAgency2().getName();
			logger.info("agency2Name " + agency2Name);

			String subject = "Invoice for LRIT services provided by Indian NDC to countries listed under " + agency2Name
					+ " as service provider, for the Period " + invoice.getFromDate() + " to " + invoice.getToDate()
					+ ".";

			// authorize person name must fetched from portal user
//			PortalUser portal
			String authorizedPerson = "Capt. S. S. Darokar";

			Map<String, Object> parameters = new HashMap<String, Object>();

			parameters.put("AuthorizedPerson", authorizedPerson);
			parameters.put("InvoiceNo", invoice.getBillingServiceId());
			parameters.put("Subject", subject);

			JasperReport jasperReport = JasperCompileManager.compileReport(invoiceJrxmlPath);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
			exporter.exportReport();
		}

		return out.toByteArray();
	}

	/**
	 * RETURNS LATEST GENERATED PDF OF INVOICE
	 *
	 * @author lrit-billing
	 * @param generatedInvoiceId
	 * @return
	 */
	@Override
	public GeneratedInvoice getLatestGeneratedInvoice(Integer generatedInvoiceId) {
		GeneratedInvoice generatedInvoice = null;
		generatedInvoice = generatedInvoiceRepository.getLatestGeneratedInvoice(generatedInvoiceId);
		return generatedInvoice;
	}

	/**
	 * RETURNS LATEST APPROVED INVOICE
	 *
	 * @author lrit-billing
	 * @param invoiceId
	 * @return
	 */
	@Override
	public ApprovedInvoice getLatestApprovedInvoice(Integer invoiceId) {
		ApprovedInvoice approvedInvoice = null;
		approvedInvoice = approvedInvoiceRepository.getLatestApprovedInvoice(invoiceId);
		return approvedInvoice;
	}

	/**
	 * FETCH GENERATEDPDF INVOICE
	 *
	 * @author lrit-billing
	 * @param generatedInvoiceId
	 * @return
	 */
	@Override
	public GeneratedInvoice fetchGeneratedInvoice(Integer generatedInvoiceId) {
		GeneratedInvoice generatedInvoice = null;
		if (generatedInvoiceRepository.findById(generatedInvoiceId).isPresent())
			generatedInvoice = generatedInvoiceRepository.findById(generatedInvoiceId).get();
		return generatedInvoice;
	}

	/**
	 * FETCH APPROVEDPDF INVOICE
	 *
	 * @author lrit-billing
	 * @param approvedInvoiceId
	 * @return
	 */
	@Override
	public ApprovedInvoice fetchApprovedInvoice(Integer approvedInvoiceId) {
		ApprovedInvoice approvedInvoice = null;
		if (approvedInvoiceRepository.findById(approvedInvoiceId).isPresent())
			approvedInvoice = approvedInvoiceRepository.findById(approvedInvoiceId).get();
		return approvedInvoice;
	}

	/**
	 * RETURNS ALL GENERATEDPDF FOR INVOICE
	 *
	 * @author lrit-billing
	 * @param billingServiceId
	 * @return
	 */
	@Override
	public List<GeneratedInvoice> fetchAllGeneratedInvoicePDF(Integer billingServiceId) {
		logger.info("INSIDE fetchAllGeneratedInvoicePDF");
		logger.info("billingServiceId : " + billingServiceId);

		List<GeneratedInvoice> generatedInvoices = null;
		generatedInvoices = generatedInvoiceRepository.findAllByService(billingServiceId);

		return generatedInvoices;
	}

	/**
	 * RETURNS ALL APPROVEDPDF FOR INVOICE
	 *
	 * @author lrit-billing
	 * @param billingServiceId
	 * @return
	 */
	@Override
	public List<ApprovedInvoice> fetchAllApprovedInvoicePDF(Integer billingServiceId) {
		logger.info("INSIDE fetchAllApprovedInvoicePDF");
		logger.info("billingServiceId : " + billingServiceId);

		List<ApprovedInvoice> approvedInvoices = null;
		approvedInvoices = approvedInvoiceRepository.findAllByService(billingServiceId);

		return approvedInvoices;
	}
}
