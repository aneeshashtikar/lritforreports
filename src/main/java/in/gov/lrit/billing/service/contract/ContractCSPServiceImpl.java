package in.gov.lrit.billing.service.contract;



import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.billing.controller.contract.ContractCSPController;
import in.gov.lrit.billing.dao.contract.ContractCSPRepository;
import in.gov.lrit.billing.dao.contract.ContractStakeholderRepository;
import in.gov.lrit.billing.dao.contract.agency.AgencyRepository;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;

/**
 * Service layer implementation to handle ContractCSP entity related activities
 * 
 * @author lrit_billing
 * 
 */
@Service
public class ContractCSPServiceImpl implements ContractCSPService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractCSPServiceImpl.class);
	
	@Autowired
	private ContractCSPRepository contractCSPRepository;

	@Autowired
	private AgencyRepository agencyRepo;

	@Autowired
	private ContractStakeholderRepository contractStakeholderRepo;

	@Autowired
	private ContractService contractService;

	/**
	 * To get Save CSP Contract
	 * @author lrit-billing
	 * 
	 */
	@Transactional(value = "portalTransactionManager")
	@Override
	public void save(ContractCSP contractCSP) {
		// TODO Auto-generated method stub
		Agency agency1 = contractCSP.getAgency1();
		Agency agency2 = contractCSP.getAgency2();
			
		String agency1Name = contractService.getAgencyName(contractCSP.getBillingContractType().getContractTypeId(), agency1.getAgencyCode());
		logger.info("agencyCode"+agency1.getAgencyCode());
	
		agency1.setName(agency1Name);
		logger.info("Agency1Name "+agency1);
		String agency2Code = agency2.getAgencyCode();

		ContractType contractType = contractCSP.getBillingContractType();
		int contractTypeId = contractType.getContractTypeId();
		String agency2Name = contractService.getAgencyName(contractTypeId, agency2Code);
		agency2.setName(agency2Name);
		agency2.setContractType(contractType);
		Agency savedAgency1 = agencyRepo.save(agency1);
		Agency savedAgency2 = agencyRepo.save(agency2);

		contractCSP.setAgency1(savedAgency1);
		contractCSP.setAgency2(savedAgency2);
		ContractCSP savedContractCSP = contractCSPRepository.saveAndFlush(contractCSP);

		List<ContractStakeholder> contractStakeholders = contractCSP.getContractStakeholders();
		//int contractStakeholderId = 491; 
		for (int i = 0; i < contractStakeholders.size(); i++) { 
			ContractStakeholder contractStakeholder = contractStakeholders.get(i);
			contractStakeholder.setBillingContract(savedContractCSP);
			contractStakeholderRepo.save(contractStakeholder);
		}
	}

	/**
	 * To get Contract Type By Id
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractType findById(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * To get List CSP Contracts
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<ContractCSP> getListCspContract() {
		logger.info("getListCspContract()");
		return contractCSPRepository.findAll();
	}

	/**
	 * To get CSP agency
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Agency getAgencyCSP() {
		logger.info("getAgencyCSP()");
		return contractCSPRepository.getAgencyCSP();
	}



}
