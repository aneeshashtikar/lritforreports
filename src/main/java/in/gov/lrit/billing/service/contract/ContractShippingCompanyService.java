/**
 * @ContractShippingCompanyService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Nov 5, 2019
 */
package in.gov.lrit.billing.service.contract;

import java.util.List;

import javax.validation.Valid;

import in.gov.lrit.billing.model.contract.ContractShippingCompany;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Service layer implementation to handle ContractShippingCompany entity related activities
 * @author lrit_billing
 *
 */

public interface ContractShippingCompanyService {

	/**
	 * @return
	 */
	List<LritNameDto> getAgencyShippingCompany();

	/**
	 * @param contractShippingCompany
	 */
	void save(@Valid ContractShippingCompany contractShippingCompany);

	/**
	 * @param agencyCode
	 * @return 
	 */
	List<Integer> getShippingCompanyVessels(String agencyCode);

}
