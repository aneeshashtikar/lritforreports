package in.gov.lrit.billing.service.billingservice.bill;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.billing.dao.billingservice.bill.BillCspRepository;

/**
 * Service layer implementation to handle ServiceBillCsp entity related activities 
 * @author lrit_billing
 * 
 */
@Service
public class BillCspServiceImpl implements BillCspService {

	@Autowired
	private BillCspRepository serviceBillCspRepository; 
}
