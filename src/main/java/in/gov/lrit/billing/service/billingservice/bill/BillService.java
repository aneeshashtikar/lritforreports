package in.gov.lrit.billing.service.billingservice.bill;

import java.util.Date;
import java.util.List;

import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.bill.Bill;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.billing.service.ServiceAbstractService;
import in_.gov.lrit.billing.dto.BillDto;

/**
 * Service layer interface to handle ServiceBill entity related activities
 * 
 * @author lrit-billing
 * 
 */
public interface BillService extends ServiceAbstractService {

	List<BillDto> findAllBillDto(Integer contractType, String fromDate, String toDate);

	Bill getBillById(Integer billingServiceId);

	Bill verifyBill(Integer billingServiceId);

	Bill getBill(Integer billingServiceId);

	@Override
	String getAgencyName(String agencyCode);

	ContractCSP getCSPContractByContractId(Integer contractId);

	void verifyBillWithSystemCount(Service bill, String remarks);

	void setStatusSendVerifiedBill(Integer billingServiceId);

	BillableItem getBillableItem(String provider, String consumer,
			PaymentBillableItemCategory paymentBillableItemCategory, Date fromDate, Date toDate, double conversionRate);

}
