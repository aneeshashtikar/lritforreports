/**
 * @ContractTypeService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 21-Aug-2019
 */
package in.gov.lrit.billing.service.contract;

import java.util.Collection;

import in.gov.lrit.billing.model.contract.ContractType;

/**
 * Service layer implementation to handle ContractType entity related activities
 * @author lrit_billing
 *
 */
public interface ContractTypeService {

	public Collection<ContractType> findAllContractType();

	public ContractType findByContractId(int contractTypeId);
}
