/**
 * @ContractShippingCompanyServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Nov 5, 2019
 */
package in.gov.lrit.billing.service.contract;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import in.gov.lrit.billing.dao.contract.ContractShippingCompanyRepository;
import in.gov.lrit.billing.dao.contract.ContractStakeholderRepository;
import in.gov.lrit.billing.dao.contract.agency.AgencyRepository;
import in.gov.lrit.billing.model.contract.ContractShippingCompany;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.portal.repository.vessel.ShippingCompanyRepository;
import in.gov.lrit.portal.repository.vessel.VesselUserRelationRepository;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Service layer implementation to handle ContractShippingCompany entity related activities
 * @author lrit_billing
 *
 */
@Service
public class ContractShippingCompanyServiceImpl implements ContractShippingCompanyService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractShippingCompanyServiceImpl.class);
	@Autowired
//	private PortalShippingCompanyRepository portalShippingCompanyRepository;
	private ShippingCompanyRepository portalShippingCompanyRepository;

	@Autowired
//	private PortalShippingcompanyVesselRelationRepository portalShippingcompanyVesselRelationRepository;
	private VesselUserRelationRepository portalShippingcompanyVesselRelationRepository;

	@Autowired
	private AgencyRepository agencyRepo;

	@Autowired
	private ContractShippingCompanyRepository contractShippingCompanyRepo;

	@Autowired
	private ContractStakeholderRepository contractStakeholderRepo;

	@Autowired
	private ContractService contractService;

	@Override
	public List<LritNameDto> getAgencyShippingCompany() {

		return portalShippingCompanyRepository.findAllShippingCompanyDto();
	}


	/**
	 * To Save Contract Shipping Company
	 * @author lrit-billing
	 * 
	 */

	@Override
	public void save(@Valid ContractShippingCompany contractShippingCompany) {
		logger.info("save");
		logger.info("ContractShippingComapnyContractNo "+contractShippingCompany.getContractNumber());
		Agency agency1 = contractShippingCompany.getAgency1();
		Agency agency2 = contractShippingCompany.getAgency2();
		logger.info("Agency1Code"+agency1.getAgencyCode());
		logger.info("agency2Code"+agency2.getAgencyCode());
		String agency1Name = contractService.getAgencyName(contractShippingCompany.getBillingContractType().getContractTypeId(), agency1.getAgencyCode());
		agency1.setName(agency1Name);
		Agency savedAgency1 = agencyRepo.save(agency1);
		contractShippingCompany.setAgency1(savedAgency1);
		String agency2Code = agency2.getAgencyCode();
		ContractType contractType = contractShippingCompany.getBillingContractType();
		int contractTypeId = contractType.getContractTypeId();
		String agency2Name = contractService.getAgencyName(contractTypeId, agency2Code);
		agency2.setName(agency2Name);
		agency2.setContractType(contractType);
		Agency savedAgency2 = agencyRepo.save(agency2);
		contractShippingCompany.setAgency2(savedAgency2);
		ContractShippingCompany savedContractShippingCompany = contractShippingCompanyRepo
				.saveAndFlush(contractShippingCompany);
		List<ContractStakeholder> contractStakeholders = contractShippingCompany.getContractStakeholders();

		for (int i = 0; i < contractStakeholders.size(); i++) {
			ContractStakeholder contractStakeholder = contractStakeholders.get(i);
			contractStakeholder.setBillingContract(savedContractShippingCompany);
			contractStakeholderRepo.save(contractStakeholder);
		}

	}

	/**
	 * To List of Shipping Comapny Vessels By Company Code
	 * @author lrit-billing
	 * 
	 */

	@Override
	public List<Integer> getShippingCompanyVessels(String companyCode) {
		// TODO Auto-generated method stub
		logger.info("getShippingCompanyVessels()");
		logger.info("companyCode"+companyCode);
		return portalShippingcompanyVesselRelationRepository.findVesselsIdsbyCompanyCode(companyCode);
	}

}
