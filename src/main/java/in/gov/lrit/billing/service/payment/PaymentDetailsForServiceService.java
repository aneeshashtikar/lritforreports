package in.gov.lrit.billing.service.payment;

import java.util.Optional;

import in.gov.lrit.billing.model.billingservice.bill.Bill;
import in.gov.lrit.billing.model.payment.PaymentDetailsForService;

public interface PaymentDetailsForServiceService {

	Optional<PaymentDetailsForService> findById(Integer pmtDetServiceId);

	PaymentDetailsForService getPaymentDetailsByPaymentServiceId(Integer pmtDetServiceId);

}
