package in.gov.lrit.billing.service.contract;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

//import com.gov.lrit.billing.dto.ContractDto;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAnnexure;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.ContractShippingCompany;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.ContractUsa;
import in.gov.lrit.billing.model.contract.CurrencyType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in_.gov.lrit.billing.dto.ContractDto;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Service layer interface to handle Contract entity related activities
 * 
 * @author lrit_billing
 * 
 */
@Service
public interface ContractService {
	/**
	 * @return
	 */
	public List<ContractType> getContractTypes();

	public Collection<Contract> getContractList();

	public Collection<Contract> getContractList(Date fromDate, Date toDate);

	/**
	 * @return
	 */
	public List<String> getLritAspInfoList();

	/**
	 * @return
	 */
	public List<String> getLritDatacentreInfoList();

	public List<LritNameDto> getLritNameDto();

	/**
	 * @param contractType
	 * @return
	 */
	public List<Contract> getListContracts(Integer contractType);

	/**
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public List<Contract> getListContracts(Integer contractType, java.util.Date fromDate, java.util.Date toDate);

	/**
	 * @param contractId
	 * @return
	 */
	public Contract getContractByContractId(Integer contractId);

	/**
	 * @param contractId
	 * @return
	 */
	public void terminateContractByContractId(Integer contractId);

	/**
	 * @param contractId
	 * @return
	 */
	List<String> getAgency1ContractStakeholders(Integer contractId);

	/**
	 * @param contractId
	 * @return
	 */
	List<String> getAgency2ContractStakeholders(Integer contractId);

	/**
	 * @param agencyCode
	 * @return
	 */
	public List<LritContractingGovtMst> getAgency1ContractingGovt(String agencyCode);

	/**
	 * @param agencyCode
	 * @return
	 */
	public List<LritContractingGovtMst> getAgency2ContractingGovt(String agencyCode);

	/**
	 * @param agencyCode
	 * @param agency1ContractStakeholders
	 * @return
	 */
	List<LritContractingGovtMst> getNonStakeholder(String agencyCode, List<String> agencyContractStakeholders);

	/**
	 * @param agencyCode
	 * @param agency1ContractStakeholders
	 * @return
	 */
	List<LritContractingGovtMst> getStakeholder(String agencyCode, List<String> agencyContractStakeholders);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractId
	 * @return
	 */
	Contract getContract(Integer contractId);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractID
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	List<PortalShippingCompany> getShippingCompany(Integer contractID, String validFrom, String validTo);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param companyCode
	 * @param contractID
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	List<Contract> getShippingContractList(String companyCode, Integer contractID, String validFrom, String validTo);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractTypeId
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	List<Contract> getContractList(Integer contractTypeId, String validFrom, String validTo);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	List<Contract> getContractList(String validFrom, String validTo);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param agencyCode
	 * @param contractTypeID
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	List<Contract> getAgencyWiseContracts(String agencyCode, Integer contractTypeID, String validFrom, String validTo);

	/**
	 * @param agencyCode
	 * @return
	 */

	public Agency getAgencyByAgencyCode(String agencyCode);

//	  public List<LritContractingGovtMst> getNonStakeholder(String agencyCode,
//	  List<String> contractStakeholder);

	public Agency getAgencyIndia();

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @return
	 */
	public List<LritNameDto> getAgencyContractTypeWise(Integer contractType);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param agencyCode
	 * @param contractTypeID
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	List<Contract> getAgencyWiseContracts(String agencyCode, Integer contractTypeID, Date validFrom, Date validTo);

	/**
	 * @param contract
	 * @return
	 */
	Contract save(Contract contract);

	/**
	 * @return
	 */
	List<LritNameDto> getLritNameDtoForAgency2();

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contract
	 * @return
	 */
	Contract saveContract(Contract contract);

	// Get list of contract stakeholders
	List<String> getContractStakeholders(Integer contractId, boolean isAgency1);

	// Get stakeholder name based on contract type id
	String getStakeholderName(String memberId, Integer contractTypeId);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @return
	 */
	ContractType getContractType(Integer contractType);

	/**
	 * @param contractType
	 * @return
	 */
	// public List<ContractDto> getListContractsDto(Integer contractType);

	// get name of agency based on contract type
	String getAgencyName(Integer contractTypeId, String agencyCode);

	List<LritNameDdpVerDto> getLritNameDdpVerDtoForAgency2(java.sql.Date date);

	List<LritContractingGovtMst> getStakeholderByDate( List<String> agencyContractStakeholders,
			java.sql.Date date);

	List<LritContractingGovtMst> getNonStakeholderByDate(String agencyCode, List<String> agencyContractStakeholders,
			java.sql.Date date);

	List<ContractStakeholder> getContractStakeholderList(Integer contractId, boolean isAgency1);

	public ContractStakeholder getContractStakeholder(int contractId, String memberId);


	/**
	 * 
	 * @param agencyCode
	 * @return
	 * @author Yogendra Yadav
	 */
	LritNameDdpVerDto getLritNameDdpVerDtofromAgencyCode(String agencyCode);

	/**
	 * 
	 * @param contractId
	 * @param isAgency1
	 * @param fromDate
	 * @param toDate
	 * @return Get list of contractstakholders whose valid from and valid to fields
	 *         don't lie outside fromDate and toDate
	 */
	List<ContractStakeholder> getValidContractStakeholderListByDates(Integer contractId, boolean isAgency1,
			java.sql.Date fromDate, java.sql.Date toDate);

	/**
	 * 
	 * @param contractId
	 * @param memberId
	 * @return Get latest(in case of more than one row against a particular member
	 *         id, member with validTo set to contract's valid to will be latest)
	 *         contract stakholder
	 */
	ContractStakeholder getLatestContractStakeholder(int contractId, String memberId);

	String getStakeholderName(String memberId, Integer contractTypeId, Integer contractId);

	/**
	 * @param contractType
	 * @return
	 */
	public List<ContractDto> getAllContractsByContractType(Integer contractType);

	/**
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public List<ContractDto> getAllContractsByContractType(Integer contractType, Date fromDate, Date toDate);



	/**
	 * @param contractId
	 * @return
	 */
	public List<ContractAnnexure> getAllContractsAnnexureById(Integer contractId);
	
	/**
	 * @param annexureId
	 * @return
	 */
	public ContractAnnexure getContractAnnexureById(Integer annexureId);

	/**
	 * @return
	 */
	public List<LritContractingGovtMst> getAllContractingGovts();


	/**
	 * @param contract
	 * @param contractingGov1
	 * @param contractingGov2
	 * @param contractAnnex
	 * @param remarks
	 * @param contractAnnexureDate
	 * @param agency2ContractGovValidTo 
	 * @param agency2ContractGovValidFrom 
	 * @param agency1ContractGovValidTo 
	 * @param agency1ContractGovValidFrom 
	 * @param toDates 
	 * @param fromDates 
	 * @param contractGovs 
	 * @throws IOException 
	 */
	public Contract updateContract(Contract contract, List<String> contractingGov1,
			List<String> contractingGov2, MultipartFile contractAnnex, String remarks, String contractAnnexureDate) throws IOException;
	
	/**
	 * @param contract
	 * @param contractingGov1
	 * @param contractAnnex
	 * @param remarks
	 * @param contractAnnexureDate
	 * @param agency1ContractGovValidTo 
	 * @param agency1ContractGovValidFrom 
	 * @throws IOException 
	 */
	
	public Contract updateContractCSP(ContractCSP contract, List<String> contractingGov1, MultipartFile contractAnnex, String remarks, String contractAnnexureDate, List<String> agency1ContractGovValidFrom, List<String> agency1ContractGovValidTo) throws IOException;
	

	/**
	 * @param contract
	 * @param contractingGov1
	 * @param contractAnnex
	 * @param remarks
	 * @param contractAnnexureDate
	 * @param agency1ContractGovValidTo 
	 * @param agency1ContractGovValidFrom 
	 * @throws IOException 
	 */
	public Contract updateContractShippingCompany(ContractShippingCompany contract, List<String> contractingGov1, MultipartFile contractAnnex, String remarks, String contractAnnexureDate, List<String> agency1ContractGovValidFrom, List<String> agency1ContractGovValidTo) throws IOException;

	/**
	 * @param contractAspDc
	 * @param contractingGov1
	 * @param contractingGov2
	 * @param contractAnnex
	 * @param remarks
	 * @param contractAnnexureDateString
	 * @param contractGovs
	 * @param fromDates
	 * @param toDates
	 * @param agency1ContractGovValidFrom
	 * @param agency1ContractGovValidTo
	 * @param agency2ContractGovValidFrom
	 * @param agency2ContractGovValidTo
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	ContractAspDc updateContract(@Valid ContractAspDc contractAspDc, List<String> contractingGov1,
			List<String> contractingGov2, MultipartFile contractAnnex, String remarks,
			String contractAnnexureDateString, List<String> contractGovs, List<java.sql.Date> fromDates, List<java.sql.Date> toDates,
			List<String> agency1ContractGovValidFrom, List<String> agency1ContractGovValidTo,
			List<String> agency2ContractGovValidFrom, List<String> agency2ContractGovValidTo)
			throws IOException, ParseException;

	/**
	 * @param contractUsa
	 * @param contractingGov1
	 * @param contractingGov2
	 * @param contractAnnex
	 * @param remarks
	 * @param contractAnnexureDateString
	 * @param agency1ContractGovValidFrom
	 * @param agency1ContractGovValidTo
	 * @param agency2ContractGovValidFrom
	 * @param agency2ContractGovValidTo
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	ContractUsa updateContract(@Valid ContractUsa contractUsa, List<String> contractingGov1,
			List<String> contractingGov2, MultipartFile contractAnnex, String remarks,
			String contractAnnexureDateString, List<String> agency1ContractGovValidFrom,
			List<String> agency1ContractGovValidTo, List<String> agency2ContractGovValidFrom,
			List<String> agency2ContractGovValidTo) throws IOException, ParseException;

	/**
	 * @return
	 */
	public List<CurrencyType> getAllCurrencyTypes();
	
	

}
