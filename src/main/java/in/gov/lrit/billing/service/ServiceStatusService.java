/**
 * @ServiceStatusService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 18-Oct-2019
 */
package in.gov.lrit.billing.service;

import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.Status;
import in.gov.lrit.billing.model.billingservice.invoice.ApprovedInvoice;

/**
 * @author lrit-billing
 *
 */
public interface ServiceStatusService {

	ServiceStatus saveServiceStatus(ServiceStatus serviceStatus);

	ServiceStatus saveUploadedInvoice(ServiceStatus serviceStatus);

	Status getLatestStatus(Integer billingServiceId);

	ServiceStatus saveUploadedInvoice(ServiceStatus serviceStatus, ApprovedInvoice approvedInvoice);

}
