/**
 * @ServiceServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 26-Aug-2019
 */
package in.gov.lrit.billing.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;

import in.gov.lrit.billing.dao.billingservice.ServiceRepository;
import in.gov.lrit.billing.dao.billingservice.StatusRepository;
import in.gov.lrit.billing.dao.contract.ContractStakeholderRepository;
import in.gov.lrit.billing.dao.payment.billableitem.BillableItemRepository;
import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryLogRepository;
import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryRepository;
import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.Status;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.payment.PaymentDetailsForService;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.payment.billableitem.BillableItemService;
import in.gov.lrit.billing.service.payment.billableitem.PaymentBillableItemCategoryService;
import in.gov.lrit.ddp.dao.LritContractingGovtMstRepository;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in.gov.lrit.portal.repository.vessel.ShippingCompanyRepository;
import in.gov.lrit.portal.repository.vessel.VesselRepository;
import in.gov.lrit.portal.repository.vessel.VesselUserRelationRepository;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * @author Yogendra Yadav (YY)
 *
 */
@org.springframework.stereotype.Service("serviceServiceImpl")
public class ServiceServiceImpl implements ServiceService {

	@Autowired
	protected ServiceRepository serviceRepository;

	@Autowired
	protected ContractStakeholderRepository contractStakeholderRepository;

	@Autowired
	protected ContractService contractService;

	@Autowired
	private PaymentBillableItemCategoryService paymentBillableItemCategoryService;

	@Autowired
	protected PaymentBillableItemCategoryLogRepository paymentBillableItemCategoryLogRepo; 
	
	@Autowired
	private BillableItemService billableItemService;

	@Autowired
	private StatusRepository statusRepostiory;

	@Autowired
//	private PortalShippingcompanyVesselRelationRepository portalShippingcompanyVesselRelationRepository;
	private VesselUserRelationRepository portalShippingcompanyVesselRelationRepository;

	@Autowired
	private LritContractingGovtMstRepository lritContractingGovtMstRepository;

//	@Autowired
//	private PortalShippingCompanyRepository portalShippingCompanyRepository;

	@Autowired
	private SimpleDateFormat simpleDateFormat;

//	@Autowired
//	private PortalVesselDetailRepository portalVesselDetailRepository;

	@Autowired
//	private PortalShippingCompanyRepository portalShippingCompanyRepository;
	private ShippingCompanyRepository portalShippingCompanyRepository;

	@Autowired
//	private PortalVesselDetailRepository portalVesselDetailRepository;
	private VesselRepository portalVesselDetailRepository;

	@Autowired
	protected BillableItemRepository billableItemRepository;

	@Autowired
	PaymentBillableItemCategoryRepository paymentBillableItemCategoryRepo;

	@Override
	public BillableItem getBillableItem(String provider, String consumer,
			PaymentBillableItemCategory paymentBillableItemCategory, Date fromDate, Date toDate) {
		Random r = new Random();
		BillableItem billableItem = new BillableItem();

		int min = 0;
		int max = 100;
		int systemCount = 0;

		int bic = paymentBillableItemCategory.getBillableItemCategoryId();
		int contractTypeId = paymentBillableItemCategory.getBillingContractType().getContractTypeId();

		System.out.println("Provider " + provider + " Consumer " + consumer + " fromDate " + fromDate + " toDate "
				+ toDate + " bic " + bic);
		if (contractTypeId == 1 || contractTypeId == 2) {
			systemCount = billableItemRepository.getSystemCountForAspDc(provider, consumer, fromDate, toDate, bic);
		} else if (contractTypeId == 3) {
			systemCount = billableItemRepository.getSystemCountForCSP(provider, consumer, fromDate, toDate, bic);
		} else if (contractTypeId == 4) {
			systemCount = r.nextInt((max - min) + 1) + min;
		}
		billableItem.setSystemCount(systemCount);
		billableItem.setManualCount(billableItem.getSystemCount());
		billableItem.setCost(billableItem.getManualCount() * paymentBillableItemCategory.getRate());
		billableItem.setProvider(provider);
		billableItem.setConsumer(consumer);
		billableItem.setBillingPaymentBillableItemCategory(paymentBillableItemCategory);

		return billableItem;
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public List<String> getProducers(Integer contractId) {
		return contractStakeholderRepository.getProviders(contractId);
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public List<String> getConsumers(Integer contractId) {
		return contractStakeholderRepository.getConsumers(contractId);
	}

	public List<Integer> getShippingConsumers(String agencyCode) {
		return portalShippingcompanyVesselRelationRepository.findVesselsByCompanyCode(agencyCode);
		// return null;
	}

	public List<String> getStringShippingConsumers(String agencyCode) {
		List<String> consumers = new ArrayList<>();
		for (Integer consumer : portalShippingcompanyVesselRelationRepository.findVesselsByCompanyCode(agencyCode)) {
			consumers.add(consumer.toString());
		}
		return consumers;
	}

	@Override
	public List<String> getConsumers(Contract contract) {
		List<String> consumers = new ArrayList<>();
		if (contract.getBillingContractType().getContractTypeId() == 4) {
			for (Integer consumer : portalShippingcompanyVesselRelationRepository
					.findVesselsByCompanyCode(contract.getAgency2().getAgencyCode())) {
				consumers.add(consumer.toString());
			}
		} else {
			consumers = contractStakeholderRepository.getConsumers(contract.getContractId());
		}
		return consumers;
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public List<BillableItem> getCGBillableItems(Integer contractId) {

		List<BillableItem> cgBillableItem = new ArrayList<BillableItem>();

		System.out.println("Contract ID : " + contractId);
		Contract contract = contractService.getContractByContractId(contractId);

		int contractTypeId = contract.getBillingContractType().getContractTypeId();

		List<String> providers = contractService.getContractStakeholders(contractId, false);

		System.out.println("providers " + providers);

		List<String> consumers = contractService.getContractStakeholders(contractId, true);
		System.out.println("consumers " + consumers);

		List<PaymentBillableItemCategory> paymentBillableItemCategoryList = getPaymentBillableItemCategoryList(
				contractId);
		System.out.println("paymentBillableItemCategoryList " + paymentBillableItemCategoryList);
		System.out.println("paymentBillableItemCategoryList.size() " + paymentBillableItemCategoryList.size());
		for (String provider : providers) {
			for (String consumer : consumers) {
				for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategoryList) {
					Date fromDate = null;
					Date toDate = null;
					BillableItem billableItem = new BillableItem();
					billableItem.setConsumer(consumer);
					billableItem.setProvider(provider);
					billableItem.setBillingPaymentBillableItemCategory(paymentBillableItemCategory);
					billableItem.setCost(0);
					billableItem.setManualCount(0);
					billableItem.setSystemCount(0);
					cgBillableItem.add(billableItem);
					String stakeHolderName = contractService.getStakeholderName(consumer, contractTypeId);
					billableItem.setConsumerName(stakeHolderName);
					stakeHolderName = contractService.getStakeholderName(provider, contractTypeId);
					billableItem.setProviderName(stakeHolderName);
				}
			}
		}
		return cgBillableItem;
	}

	/*
	 * @Override public List<BillableItem> getCGBillableItemsForBill(Integer
	 * contractId) {
	 * 
	 * List<BillableItem> cgBillableItem = new ArrayList<BillableItem>();
	 * 
	 * System.out.println("Contract ID : " + contractId); Contract contract =
	 * contractService.getContractByContractId(contractId);
	 * 
	 * List<String> consumers = getProducers(contractId);
	 * 
	 * // System.out.println("providers " + providers); List<String> providers =
	 * getConsumers(contractId);
	 * 
	 * // System.out.println("consumers " + consumers);
	 * List<PaymentBillableItemCategory> paymentBillableItemCategorys =
	 * (List<PaymentBillableItemCategory>) paymentBillableItemCategoryService
	 * .findByBillingContractType(contract.getBillingContractType().
	 * getContractTypeId()); // System.out.println("paymentBillableItemCategorys " +
	 * // paymentBillableItemCategorys); for (String provider : providers) { for
	 * (String consumer : consumers) { for (PaymentBillableItemCategory
	 * paymentBillableItemCategory : paymentBillableItemCategorys) { Date fromDate =
	 * null; Date toDate = null; BillableItem billableItem =
	 * getBillableItem(provider, consumer, paymentBillableItemCategory, fromDate,
	 * toDate); if (billableItem.getSystemCount() > 0) {
	 * cgBillableItem.add(billableItem); } } } } //
	 * System.out.println("cgBillableItem " + cgBillableItem); return
	 * cgBillableItem; }
	 */
	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public List<BillableItem> getVesselsBillableItems(Integer contractId) {
		List<BillableItem> vesselsBillableItems = new ArrayList<>();
		Contract contract = contractService.getContract(contractId);

		List<String> providers = getProducers(contractId);
		List<Integer> consumers = getShippingConsumers(contract.getAgency2().getAgencyCode());

		List<PaymentBillableItemCategory> paymentBillableItemCategorys = (List<PaymentBillableItemCategory>) paymentBillableItemCategoryService
				.findByBillingContractType(contract.getBillingContractType().getContractTypeId());

		for (String provider : providers) {
			for (Integer consumer : consumers) {
				for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategorys) {
					Date fromDate = null;
					Date toDate = null;
					BillableItem billableItem = getBillableItem(provider, consumer.toString(),
							paymentBillableItemCategory, fromDate, toDate);
					if (billableItem.getSystemCount() > 0) {
						vesselsBillableItems.add(billableItem);
					}
				}
			}
		}

		return vesselsBillableItems;
	}

	@Override
	public List<BillableItem> getCGBillableItemForService(Integer billingServiceId) {
		Service service = serviceRepository.getOne(billingServiceId);

		List<BillableItem> billableItems = new ArrayList<>();

		if (service.getBillingBillableItems() != null) {
			BillableItem billableItem = null;
			for (int index = 0; index < service.getBillingBillableItems().size(); index++) {
				billableItem = service.getBillingBillableItems().get(index);
				billableItem
						.setProviderName(billableItem.getProvider() + " - " + getCGName(billableItem.getProvider()));
				billableItem.setConsumerName(
						billableItem.getConsumer() + " - " + getNameContractTypeBased(billableItem.getConsumer(),
								service.getBillingContract().getBillingContractType().getContractTypeId()));
				billableItems.add(index, billableItem);
			}
		}

		// return billableItemService.getBillableItemsOfService(billingServiceId);
		return billableItems;
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public List<ServiceStatus> getServiceStatuses(Service service, Integer statusId, String remarks) {
		Status billingStatus = statusRepostiory.findById(statusId).get();

		ServiceStatus serviceStatus = new ServiceStatus();

		serviceStatus.setRemarks(remarks);
		serviceStatus.setBillingService(service);
		serviceStatus.setBillingStatus(billingStatus);

		List<ServiceStatus> billingServiceStatuses = new ArrayList<ServiceStatus>();
		if (service.getBillingServiceStatuses() != null)
			billingServiceStatuses = service.getBillingServiceStatuses();
		billingServiceStatuses.add(serviceStatus);

		return billingServiceStatuses;

	}

	@Override
	public Service setServiceStatuses(Service servicePara, Integer statusId, String remarks) {
		Status billingStatus = statusRepostiory.findById(statusId).get();
		ServiceStatus serviceStatus = new ServiceStatus();

		serviceStatus.setRemarks(remarks);
		serviceStatus.setBillingService(servicePara);
		serviceStatus.setBillingStatus(billingStatus);

		List<ServiceStatus> billingServiceStatuses = new ArrayList<ServiceStatus>();
		if (servicePara.getBillingServiceStatuses() != null)
			billingServiceStatuses = servicePara.getBillingServiceStatuses();
		billingServiceStatuses.add(serviceStatus);

		servicePara.setBillingServiceStatuses(billingServiceStatuses);

		return servicePara;
	}

	@Override
	public List<BillableItem> getUpdatedBillableItems(List<BillableItem> billableItemsUpdate,
			List<BillableItem> billableItemsData) {

		for (int index = 0; index < billableItemsUpdate.size(); index++) {
			BillableItem billableItem = billableItemsUpdate.get(index);
			for (BillableItem billableItemInvoice : billableItemsData) {
				System.out.println("index1 " + index + "__" + billableItem.getBillableItemId());
				System.out.println("index2 " + index + "__" + billableItemInvoice.getBillableItemId());
				if (billableItem.getBillableItemId().equals(billableItemInvoice.getBillableItemId())) {
					System.out.println("index1 " + index + "__" + billableItem.getBillableItemId());
					System.out.println("index2" + index + "__" + billableItemInvoice.getBillableItemId());
					billableItem.setManualCount(billableItemInvoice.getManualCount());
					billableItem.setCost(billableItemInvoice.getCost());
					break;
				}
			}
			billableItemsUpdate.set(index, billableItem);
		}
		return billableItemsUpdate;
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public Service saveService(Service service) {
		System.out.println("Service " + service);
		Service returnService = null;
		// System.out.println("Service " + service);
		Integer statusId = 1;
		String remarks = "";

		System.out.println("service.getBillingBillableItems().size() " + service.getBillingBillableItems().size());

		List<BillableItem> billingBillableItems = (service.getBillingBillableItems() == null)
				? new ArrayList<BillableItem>()
				: service.getBillingBillableItems();
		System.out.println("In serviceserviceimpl " + billingBillableItems);
		// TO SET EVERY BILLABLEITEM INVOICE AS REFERENCE
		for (int i = 0; i < billingBillableItems.size(); i++) {
			billingBillableItems.get(i).setBillingService(service);
			billingBillableItems.get(i).setDateRangeFrom(service.getFromDate());
			billingBillableItems.get(i).setDateRangeTo(service.getToDate());
		}
		System.out.println("In serviceserviceimpl after for");
		service.setBillingBillableItems(billingBillableItems);
		service.setBillingServiceStatuses(getServiceStatuses(service, statusId, remarks));
		System.out.println("In serviceserviceimpl after sert");
		returnService = serviceRepository.save(service);
		return returnService;
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public Service saveService(Service service, List<BillableItem> BillableItems) {

		Service returnService = null;

		Integer statusId = 1;
		String remarks = "";

		List<BillableItem> billingBillableItems = BillableItems;

		// TO SET EVERY BILLABLEITEM INVOICE AS REFERENCE
		for (int i = 0; i < billingBillableItems.size(); i++) {
			billingBillableItems.get(i).setBillingService(service);
			billingBillableItems.get(i).setDateRangeFrom(service.getFromDate());
			billingBillableItems.get(i).setDateRangeTo(service.getToDate());
		}

		service.setBillingBillableItems(billingBillableItems);
		service.setBillingServiceStatuses(getServiceStatuses(service, statusId, remarks));

		returnService = serviceRepository.save(service);

		return returnService;
	}

	@Override
	public String getCGName(String consumer) {
		// System.out.println("consumer "+consumer);
		return lritContractingGovtMstRepository.getCGName(consumer);
	}

	@Override
	public String getVesselName(String vesselId) {
		return portalVesselDetailRepository.findByVesselId(Integer.valueOf(vesselId)).getVesselName();
	}

	@Override
	public String getNameContractTypeBased(String id, Integer contractType) {
		if (contractType == 4)
			return portalVesselDetailRepository.findByVesselId(Integer.valueOf(id)).getVesselName();
		else
			return lritContractingGovtMstRepository.getCGName(id);
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public String getAgencyName(String agencyCode) {
		String agencyName = "";
		for (LritNameDto agency : getAllAgency()) {
			if (agency.getLritid().equals(agencyCode)) {
				agencyName = agency.getName();
				break;
			}
		}
		return agencyName;
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public String getAgencyName(Service service) {
		String agencyName = "";
		for (LritNameDto agency : getAllAgency()) {
			if (agency.getLritid().equals(service.getBillingContract().getAgency2().getAgencyCode())) {
				agencyName = agency.getName();
				break;
			}
		}
		return agencyName;
	}

	/**
	 * 
	 * @author Yogendra Yadav
	 */
	@Override
	public List<PortalShippingCompany> getShippingCompanys() {
		// return portalShippingCompanyRepository.findAll();
		return null;
	}

	/**
	 * @author Yogendra Yadav
	 */
	@Override
	public HashMap<String, HashMap<String, List<BillableItem>>> getHashedBillableItems(Integer contractId,
			String fromDateString, String toDateString) {

		Contract contract = contractService.getContract(contractId);
		HashMap<String, HashMap<String, List<BillableItem>>> billableItems = new HashMap<String, HashMap<String, List<BillableItem>>>();
		Date fromDate = null;
		Date toDate = null;

		try {
			fromDate = simpleDateFormat.parse(fromDateString);
			toDate = simpleDateFormat.parse(toDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<String> providers = getProducers(contractId);
		List<String> consumers = getConsumers(contract);

		if (contract.getBillingContractType().getContractTypeId() == 4) {
			consumers = getStringShippingConsumers(contract.getAgency2().getAgencyCode());
		} else {
			consumers = getConsumers(contractId);
		}

		int contractTypeId = contract.getBillingContractType().getContractTypeId();

		List<PaymentBillableItemCategory> paymentBillableItemCategorys = null;
		if (contractTypeId == 1 || contractTypeId == 2) {
			paymentBillableItemCategorys = (List<PaymentBillableItemCategory>) paymentBillableItemCategoryService
					.findByBillingContractType(1);

		} else {
			paymentBillableItemCategorys = (List<PaymentBillableItemCategory>) paymentBillableItemCategoryService
					.findByBillingContractType(contractTypeId);
		}

		for (String provider : providers) {
			HashMap<String, List<BillableItem>> consumerHash = new HashMap<String, List<BillableItem>>();

			for (String consumer : consumers) {
				List<BillableItem> messages = new ArrayList<BillableItem>();

				for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategorys) {
					BillableItem billableItem = getBillableItem(provider, consumer, paymentBillableItemCategory,
							fromDate, toDate);
					if (billableItem.getSystemCount() > 0) {
						messages.add(billableItem);
					}
				}
				if (contract.getBillingContractType().getContractTypeId() == 4)
					consumer = consumer.concat(" - " + getVesselName(consumer));
				else
					consumer = consumer.concat(" - " + getCGName(consumer));
				consumerHash.put(consumer, messages);
			}
			billableItems.put(provider.concat(" - " + getCGName(provider)), consumerHash);
		}
		return billableItems;
	}

	@Override
	public HashMap<String, HashMap<String, List<BillableItem>>> getCGHashedBillableItems(Integer billingServiceId) {
		Service service = serviceRepository.getOne(billingServiceId);
		HashMap<String, HashMap<String, List<BillableItem>>> provider = new HashMap<String, HashMap<String, List<BillableItem>>>();

		if (service.getBillingBillableItems() != null) {
			service.getBillingBillableItems().forEach((billableItem) -> {
				List<BillableItem> billableItems = null;
				HashMap<String, List<BillableItem>> consumer = null;

				if (provider.get(billableItem.getProvider()) != null) {
					consumer = provider.get(billableItem.getProvider());
					if (consumer.get(billableItem.getConsumer()) == null) {
						billableItems = new ArrayList<BillableItem>();
					} else
						billableItems = consumer.get(billableItem.getConsumer());

					billableItems.add(billableItem);
					consumer.put(billableItem.getConsumer(), billableItems);
					provider.put(billableItem.getProvider(), consumer);

				} else {
					consumer = new HashMap<String, List<BillableItem>>();
					billableItems = new ArrayList<BillableItem>();

					billableItems.add(billableItem);
					consumer.put(billableItem.getConsumer(), billableItems);
					provider.put(billableItem.getProvider(), consumer);
				}
			});
		}

		return provider;

	}

	@Override
	public List<LritNameDto> getAllAgency() {
		List<LritNameDto> lritNameDtos = contractService.getLritNameDto();
		lritNameDtos.addAll(portalShippingCompanyRepository.findAllShippingCompanyDto());
		return lritNameDtos;
	}

	@Override
	public HashMap<String, PaymentDetailsPerMember> getPaymentMembers(Integer billingServiceId) {
		HashMap<String, PaymentDetailsPerMember> consumersCost = new HashMap<>();

		List<BillableItem> consumerBillableItem = serviceRepository.getPaymentDetailMember(billingServiceId);

		PaymentDetailsPerMember paymentDetailsPerMember = new PaymentDetailsPerMember();
		for (BillableItem billableItem : consumerBillableItem) {
			paymentDetailsPerMember = null;
			if (consumersCost.get(billableItem.getConsumer()) == null) {

				paymentDetailsPerMember = new PaymentDetailsPerMember();
				paymentDetailsPerMember.setConsumer(billableItem.getConsumer());
				paymentDetailsPerMember.setCost(0.0);

				consumersCost.put(billableItem.getConsumer(), paymentDetailsPerMember);
			}

			paymentDetailsPerMember = consumersCost.get(billableItem.getConsumer());
			paymentDetailsPerMember.setCost(paymentDetailsPerMember.getCost() + billableItem.getCost());

			consumersCost.put(billableItem.getConsumer(), paymentDetailsPerMember);
		}
		return consumersCost;
	}

	@Override
	public HashMap<String, PaymentDetailsPerMember> getPaymentMembers(List<BillableItem> billableItems) {
		HashMap<String, PaymentDetailsPerMember> consumersCost = new HashMap<>();

		List<BillableItem> consumerBillableItem = billableItems;

		PaymentDetailsPerMember paymentDetailsPerMember = new PaymentDetailsPerMember();
		for (BillableItem billableItem : consumerBillableItem) {
			paymentDetailsPerMember = null;
			if (consumersCost.get(billableItem.getConsumer()) == null) {

				paymentDetailsPerMember = new PaymentDetailsPerMember();
				paymentDetailsPerMember.setConsumer(billableItem.getConsumer());
				// paymentDetailsPerMember.setConsumerName(getCGName(billableItem.getProvider()));
				paymentDetailsPerMember.setCost(0.0);

				consumersCost.put(billableItem.getConsumer(), paymentDetailsPerMember);
			}

			paymentDetailsPerMember = consumersCost.get(billableItem.getConsumer());
			paymentDetailsPerMember.setCost(paymentDetailsPerMember.getCost() + billableItem.getCost());
			System.out.println("billavle" + billableItem.getConsumer());
			if (billableItem.getConsumer().equals("0000")) {
				paymentDetailsPerMember.setConsumerName("CSP");
				System.out.println("payment " + paymentDetailsPerMember.getConsumerName());
			} else {
				paymentDetailsPerMember.setConsumerName(getCGName(billableItem.getConsumer()));
			}
			consumersCost.put(billableItem.getConsumer(), paymentDetailsPerMember);

		}
		return consumersCost;
	}

	@Override
	public HashMap<String, PaymentDetailsPerMember> getUncheckedMembers(Service service) {

		System.out
				.println(service.getBillingContract().getBillingContractType().getContractTypeId() + "ContractTYpeID");

		System.out.println("Hello " + service);
		HashMap<String, PaymentDetailsPerMember> unCheckedMembers = getPaymentMembers(
				service.getBillingBillableItems());

		System.out.println("Hello1 " + unCheckedMembers);
//		
//		if(unCheckedMembers != null) {
//			for(HashMap.Entry<String, PaymentDetailsPerMember> hfg : unCheckedMembers.entrySet()) {
//				System.out.println("Key "+hfg.getKey());
//				List<BillableItem> billableItems = service.getBillingBillableItems();
//				for(BillableItem billableItem : billableItems) {
//					billableItem.setConsumerName(getCGName(hfg.getKey()));
//					System.out.println(getCGName(hfg.getKey()));
//					
//				}
//				
//			}
//		}
//		
		List<PaymentDetailsForService> paymentDetailsForServices = service.getBillingPaymentDetailsForServices();
		System.out.println("paymentdetails " + paymentDetailsForServices.size());
		for (PaymentDetailsForService paymentDetailsForService : paymentDetailsForServices) {
			System.out.println("55555" + paymentDetailsForService.getBillingPaymentDetailsPerMembers());

			System.out.println(
					service.getBillingContract().getBillingContractType().getContractTypeId() + "ContractTYpeID");

			if (paymentDetailsForService.getBillingPaymentDetailsPerMembers() != null) {
				for (PaymentDetailsPerMember paymentDetailsPerMember : paymentDetailsForService
						.getBillingPaymentDetailsPerMembers()) {
					if (paymentDetailsPerMember.getConsumer().equals("0000")) {
						paymentDetailsPerMember.setConsumerName("CSP");
					} else
						paymentDetailsPerMember.setConsumerName(getCGName(paymentDetailsPerMember.getConsumer()));

					unCheckedMembers.remove(paymentDetailsPerMember.getConsumer());
				}
			}

		}
		return unCheckedMembers;
	}

	@Override
	public HashSet<String> getMembers(Service service) {
		HashSet<String> memebers = new HashSet<String>();
		List<BillableItem> billableItems = service.getBillingBillableItems();
		for (BillableItem billableItem : billableItems) {

			memebers.add(billableItem.getConsumer());

		}
		return null;

	}

	@Override
	public Double getRemainingAmountService(Service service) {
		Double remainingCost = 0.0;
		for (PaymentDetailsForService paymentDetailsForService : service.getBillingPaymentDetailsForServices())
			remainingCost += paymentDetailsForService.getPaidAmount();

		return (service.getGrandTotal() - remainingCost);
	}

	@Override
	public Service setBillableItems(Service mainService, Service dataCareerService) {
		System.out.println("Service " + mainService.getBillingBillableItems());
		System.out.println("dataCareerService " + dataCareerService.getBillingBillableItems());
		mainService.setBillingBillableItems(getUpdatedBillableItems(mainService.getBillingBillableItems(),
				dataCareerService.getBillingBillableItems()));
		mainService.setGrandTotal(dataCareerService.getGrandTotal());
		mainService.setSubTotal(dataCareerService.getSubTotal());

		return mainService;
	}

	@Override
	public Service updatePayment(Service paymentService, Service service, List<String> consumers) {

		if (consumers != null || !consumers.isEmpty()) {
			List<PaymentDetailsPerMember> selectedMembers = new ArrayList<PaymentDetailsPerMember>();

			PaymentDetailsForService paymentDetailsForService = service.getBillingPaymentDetailsForServices().get(0);

			for (String consumer : consumers) {
				for (PaymentDetailsPerMember paymentDetailsPerMember : paymentDetailsForService
						.getBillingPaymentDetailsPerMembers()) {

					if (consumer.equals(paymentDetailsPerMember.getConsumer())) {
						paymentDetailsPerMember.setBillingPaymentDetailsForService(paymentDetailsForService);
						selectedMembers.add(paymentDetailsPerMember);
					}
				}
			}
			paymentDetailsForService.setBillingPaymentDetailsPerMembers(selectedMembers);
			paymentDetailsForService.setBillingService(paymentService);

			List<PaymentDetailsForService> paymentDetailsForServices = (null == paymentService
					.getBillingPaymentDetailsForServices()) ? new ArrayList<PaymentDetailsForService>()
							: paymentService.getBillingPaymentDetailsForServices();

			paymentDetailsForServices.add(paymentDetailsForService);
			paymentService.setBillingPaymentDetailsForServices(paymentDetailsForServices);

		}

		return paymentService;
	}

	@Override
	public Integer getBillableItemSystemCount(String provider, String consumer, Date fromDate, Date toDate, Integer bic,
			Integer contractTypeId) {
		// TODO Auto-generated method stub

		int systemCount = 0;
		Random r = new Random();
		int min = 0;
		int max = 100;

		System.out.println("Provider " + provider + " Consumer " + consumer + " fromDate " + fromDate + " toDate "
				+ toDate + " bic " + bic);
		if (contractTypeId == 1 || contractTypeId == 2) {
			systemCount = billableItemRepository.getSystemCountForAspDc(provider, consumer, fromDate, toDate, bic);
		} else if (contractTypeId == 3) {
			systemCount = billableItemRepository.getSystemCountForCSP(provider, consumer, fromDate, toDate, bic);
		} else if (contractTypeId == 4) {
			systemCount = r.nextInt((max - min) + 1) + min;
		}
		return systemCount;
	}

	@Override
	public List<PaymentBillableItemCategory> getPaymentBillableItemCategoryList(Integer contractId) {
		// TODO Auto-generated method stub
		Contract contract = contractService.getContract(contractId);
		ContractType contractType = contract.getBillingContractType();
		Date validFrom = contract.getValidFrom();
		Date validTo = contract.getValidTo();
		int contractTypeId = contractType.getContractTypeId();

		List<PaymentBillableItemCategory> paymentBillableItemCategoryList = null;
		if (contractTypeId == 1 || contractTypeId == 2) {
			contractType.setContractTypeId(1);
			paymentBillableItemCategoryList = paymentBillableItemCategoryLogRepo
					.findPbicByBillingContractTypeAndValidFromAndValidTill(contractType, validFrom, validTo);

		} else {
			paymentBillableItemCategoryList = paymentBillableItemCategoryLogRepo
					.findPbicByBillingContractTypeAndValidFromAndValidTill(contractType, validFrom, validTo);
		}

		return paymentBillableItemCategoryList;
	}

	// public String getCGName(String lritId) {
	//
	// }

}
