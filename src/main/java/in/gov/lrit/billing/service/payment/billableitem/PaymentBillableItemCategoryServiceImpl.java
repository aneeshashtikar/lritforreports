/**
 * @BillableItemCategoryServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 21-Aug-2019
 */
package in.gov.lrit.billing.service.payment.billableitem;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryLogRepository;
import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryRepository;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategoryLog;
import in.gov.lrit.billing.service.contract.ContractService;

/**
 * @author Yogendra Yadav (YY)
 *
 */
@Service
public class PaymentBillableItemCategoryServiceImpl implements PaymentBillableItemCategoryService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
			.getLogger(PaymentBillableItemCategoryServiceImpl.class);

	@Autowired
	PaymentBillableItemCategoryRepository paymentBillableItemCategoryRepository;

	@Autowired
	private PaymentBillableItemCategoryLogRepository paymentBillableItemCategoryLogRepository;

	@Autowired
	private ContractService contractService;

	/**
	 * TO SET CATEGORY AS FOREIGN KEY IN CATEGORYLOGS
	 * 
	 * @param paymentBillableItemCategory
	 */
//	public void setPaymentBillableItemCategory(PaymentBillableItemCategory paymentBillableItemCategory) {
//		logger.info("INSIDE setPaymentBillableItemCategory");
//
//		for (int index = 0; index < paymentBillableItemCategory.getPaymentBillableItemCategoryLogs().size(); index++) {
//			paymentBillableItemCategory.getPaymentBillableItemCategoryLogs().get(index)
//					.setPaymentBillableItemCategory(paymentBillableItemCategory);
//		}
//	}

	/**
	 * TO SAVE NEWLY ADDED BILLABLE ITEM CATEGORY
	 * 
	 * @author lrit-billing
	 * @param paymentBillableItemCategory
	 * @return
	 */
	@Override
	public PaymentBillableItemCategory save(PaymentBillableItemCategory paymentBillableItemCategory) {
		logger.info("INSIDE save");
		logger.info("paymentBillableItemCategory : " + paymentBillableItemCategory);

		PaymentBillableItemCategoryLog paymentBillableItemCategoryLog = paymentBillableItemCategory
				.getPaymentBillableItemCategoryLog();
		paymentBillableItemCategoryLog.setPaymentBillableItemCategory(paymentBillableItemCategory);

		paymentBillableItemCategory = paymentBillableItemCategoryRepository.save(paymentBillableItemCategory);
		paymentBillableItemCategoryLog = paymentBillableItemCategoryLogRepository.save(paymentBillableItemCategoryLog);

		return paymentBillableItemCategory;
	}

	/**
	 * TO UPDATE BILLABLE ITEM CATEGORY, ENTERS NEW RECORD IN LOG TABLE
	 * 
	 * @author lrit-billing
	 * @param paymentBillableItemCategory
	 * @return
	 */
	@Override
	public PaymentBillableItemCategory update(PaymentBillableItemCategory paymentBillableItemCategory) {
		logger.info("INSIDE update");
		logger.info("paymentBillableItemCategory : " + paymentBillableItemCategory);

		Integer billableItemCategoryId = paymentBillableItemCategory.getBillableItemCategoryId();

		if (paymentBillableItemCategoryRepository.findById(billableItemCategoryId).isPresent()) {

			PaymentBillableItemCategory fetchedPaymentBillableItemCategory = paymentBillableItemCategoryRepository
					.findById(billableItemCategoryId).get();

			PaymentBillableItemCategoryLog paymentBillableItemCategoryLog = null;
			paymentBillableItemCategoryLog = paymentBillableItemCategory.getPaymentBillableItemCategoryLog();
			paymentBillableItemCategoryLog.setPaymentBillableItemCategory(fetchedPaymentBillableItemCategory);

			if (validateBillableLogEntry(paymentBillableItemCategoryLog)) {
				logger.info("VAlIDATED TRUE READY TO PERSIST");
				paymentBillableItemCategoryLog = paymentBillableItemCategoryLogRepository
						.save(paymentBillableItemCategoryLog);
				return fetchedPaymentBillableItemCategory;
			}
		}

		return null;
	}

	/**
	 * RETURN ALL PAYMENT BILLABLE ITEM CATEGORY BASED ON CONTRACT TYPE ID
	 * 
	 * @author lrit-billing
	 * @param contractTypeId
	 * @return
	 */
	@Override
	public Collection<PaymentBillableItemCategory> findByBillingContractType(Integer contractTypeId) {
		logger.info("INSIDE findByBillingContractType");
		logger.info("contractTypeId : " + contractTypeId);

		Collection<PaymentBillableItemCategory> paymentBillableItemCategories = paymentBillableItemCategoryRepository
				.findByBillingContractTypeId(contractTypeId);

		return paymentBillableItemCategories;

	}

	@Override
	public PaymentBillableItemCategory getBillableItemCategory(int billableItemCategoryId) {
		logger.info("INSIDE getBillableItemCategory");
		logger.info("billableItemCategoryId : " + billableItemCategoryId);

		PaymentBillableItemCategory paymentBillableItemCategory = null;

		if (paymentBillableItemCategoryRepository.findById(billableItemCategoryId).isPresent())
			paymentBillableItemCategory = paymentBillableItemCategoryRepository.findById(billableItemCategoryId).get();

		return paymentBillableItemCategory;
	}

	/**
	 * 
	 * @author lrit-billing
	 * @param billableItemCategoryLogId
	 * @return
	 */
	@Override
	public PaymentBillableItemCategoryLog getPaymentBillableItemCategoryLog(int billableItemCategoryLogId) {
		logger.info("INSIDE getPaymentBillableItemCategoryLog");
		logger.info("billableItemCategoryLogId : " + billableItemCategoryLogId);

		PaymentBillableItemCategoryLog paymentBillableItemCategoryLog = null;

		if (paymentBillableItemCategoryLogRepository.findById(billableItemCategoryLogId).isPresent())
			paymentBillableItemCategoryLog = paymentBillableItemCategoryLogRepository
					.findById(billableItemCategoryLogId).get();

		return paymentBillableItemCategoryLog;
	}

	/**
	 * RETURN LATEST UPDATED LOG PRICE ENTRY OF BILLABLE ITEM
	 * 
	 * @author lrit-billing
	 * @param billableItemCategoryId
	 * @return
	 */
	@Override
	public PaymentBillableItemCategoryLog getLatestBillableItemCategoryLog(int billableItemCategoryId) {
		logger.info("INSIDE getBillableItemCategory");
		logger.info("billableItemCategoryId : " + billableItemCategoryId);

		PaymentBillableItemCategoryLog paymentBillableItemCategoryLog = null;

		if (paymentBillableItemCategoryLogRepository.getLatestUpdatedCategoryLog(billableItemCategoryId) != null)
			paymentBillableItemCategoryLog = paymentBillableItemCategoryLogRepository
					.getLatestUpdatedCategoryLog(billableItemCategoryId);

		return paymentBillableItemCategoryLog;
	}

	/**
	 * THIS METHOD RETURN LOG AGAINST PASSED BILLABLE ITEM CATEGORY MULTIPLE LOG
	 * GETS AS OUTPUT
	 * 
	 * @author lrit-billing
	 * @param billableItemCategoryId
	 * @return
	 */
	@Override
	public List<PaymentBillableItemCategoryLog> getPaymentBillableItemCategoryLogs(int billableItemCategoryId) {
		logger.info("INSIDE getPaymentBillableItemCategoryLogs");
		logger.info("billableItemCategoryId : " + billableItemCategoryId);

		List<PaymentBillableItemCategoryLog> paymentBillableItemCategoryLogs = null;

		if (paymentBillableItemCategoryLogRepository.getLatestUpdatedCategoryLog(billableItemCategoryId) != null)
			paymentBillableItemCategoryLogs = paymentBillableItemCategoryLogRepository
					.findAllByPaymentBillableItemCategory(billableItemCategoryId);

		return paymentBillableItemCategoryLogs;
	}

	@Override
	public PaymentBillableItemCategoryLog saveBillableCategoryLog(
			PaymentBillableItemCategoryLog paymentBillableItemCategoryLog) {
		logger.info("INSIDE saveBillableCategoryLog");
		logger.info("paymentBillableItemCategoryLog : " + paymentBillableItemCategoryLog);

		Integer paymentCategoryLogId = paymentBillableItemCategoryLog.getPaymentBillableItemCategoryLogId();

		PaymentBillableItemCategoryLog fetchedBillableItemCategoryLog = getPaymentBillableItemCategoryLog(
				paymentCategoryLogId);
		paymentBillableItemCategoryLog
				.setPaymentBillableItemCategory(fetchedBillableItemCategoryLog.getPaymentBillableItemCategory());

		if (validateBillableLogEntry(paymentBillableItemCategoryLog)) {

			Date validFrom = paymentBillableItemCategoryLog.getValidFrom();
			Date validTill = paymentBillableItemCategoryLog.getValidTill();

			// PROPERTY TO UPDATE
			fetchedBillableItemCategoryLog.setRate(paymentBillableItemCategoryLog.getRate());
			fetchedBillableItemCategoryLog.setValidFrom(validFrom);
			fetchedBillableItemCategoryLog.setValidTill(validTill);
			fetchedBillableItemCategoryLog.setRemark(paymentBillableItemCategoryLog.getRemark());

			fetchedBillableItemCategoryLog = paymentBillableItemCategoryLogRepository
					.save(fetchedBillableItemCategoryLog);

			return fetchedBillableItemCategoryLog;
		}

		return null;
	}

	public Boolean validateBillableLogEntry(PaymentBillableItemCategoryLog paymentBillableItemCategoryLog) {
		logger.info("INSIDE validateBillableLogEntry");
		logger.info("paymentBillableItemCategoryLog : " + paymentBillableItemCategoryLog);

		if (paymentBillableItemCategoryLog.getPaymentBillableItemCategory() != null) {

			Date validFrom = paymentBillableItemCategoryLog.getValidFrom();
			Date validTill = paymentBillableItemCategoryLog.getValidTill();
			Integer paymentBillableItemCategoryId = paymentBillableItemCategoryLog.getPaymentBillableItemCategory()
					.getBillableItemCategoryId();

			List<PaymentBillableItemCategoryLog> paymentBillableItemCategoryLogs = paymentBillableItemCategoryLogRepository
					.fetchValidatedBillableLogs(validFrom, validTill, paymentBillableItemCategoryId);

			if (paymentBillableItemCategoryLogs != null)
				logger.info("paymentBillableItemCategoryLogs : " + paymentBillableItemCategoryLogs);

			if (paymentBillableItemCategoryLogs.isEmpty())
				return true;

			if (paymentBillableItemCategoryLogs.size() > 1)
				return false;
			else
				return true;

		}

		return false;
	}

	/*
	 * @Override public Collection<PaymentBillableItemCategory>
	 * findByBillingContractType(Integer contractTypeId) { // TODO Auto-generated
	 * method stub return null; }
	 */

}
