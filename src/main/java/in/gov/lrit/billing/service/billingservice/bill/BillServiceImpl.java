package in.gov.lrit.billing.service.billingservice.bill;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import in.gov.lrit.billing.controller.billingservice.bill.BillController;
import in.gov.lrit.billing.dao.billingservice.ServiceRepository;
import in.gov.lrit.billing.dao.billingservice.StatusRepository;
import in.gov.lrit.billing.dao.billingservice.bill.BillRepository;
import in.gov.lrit.billing.dao.payment.billableitem.BillableItemRepository;
import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryLogRepository;
import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.Status;
import in.gov.lrit.billing.model.billingservice.bill.Bill;
import in.gov.lrit.billing.model.contract.Contract;

import in.gov.lrit.billing.model.contract.ContractStakeholder;

import in.gov.lrit.billing.model.contract.ContractCSP;

import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.billing.service.ServiceAbstractServiceImpl;


import in.gov.lrit.billing.service.ServiceService;

import in.gov.lrit.billing.service.billingservice.invoice.InvoiceAbstractService;


import in.gov.lrit.billing.service.ServiceServiceImpl;


import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.ddp.dao.LritAspMstRepository;
import in.gov.lrit.ddp.dao.LritDatacentreMstRepository;
import in_.gov.lrit.billing.dto.BillDto;
import in_.gov.lrit.billing.dto.BillInvoiceListDto;
import in_.gov.lrit.ddp.dto.LritNameDto;


/**
 * Service layer implementation to handle ServiceBill entity related activities
 * 
 * @author lrit-billing
 * 
 */
@org.springframework.stereotype.Service("billServiceImpl")
public class BillServiceImpl extends ServiceAbstractServiceImpl implements BillService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BillServiceImpl.class);
	@Autowired
	private BillRepository billRepository;

	@Autowired
	private ServiceRepository serviceRepository; 

	@Autowired
	private ContractService contractService;

	@Autowired
	private LritAspMstRepository lritAspMstRepository;

	@Autowired
	private LritDatacentreMstRepository lritDAtacentreMstRepository;

	@Autowired
	private InvoiceAbstractService invoiceAbstractService;

	@Autowired
	private StatusRepository statusRepository;

	@Autowired
	private BillableItemRepository billableItemRepository; 
	
	@Autowired
	private PaymentBillableItemCategoryLogRepository paymentBillableItemCategoryLogRepo;
	@Value("${lrit.cgid}")
	private String indianCGLritId;

	/**
	 * For saving the service(bill) in database
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Service saveService(Service service) {
		Integer statusId = 9;
		String remarks = "Bill Received";

		logger.info("ServiceId-"+service.getBillingServiceId());
		List<BillableItem> billingBillableItems = service.getBillingBillableItems();

		// TO SET EVERY BILLABLEITEM INVOICE AS REFERENCE
		for (int i = 0; i < billingBillableItems.size(); i++) {
			billingBillableItems.get(i).setBillingService(service);
			billingBillableItems.get(i).setDateRangeFrom(service.getFromDate());
			billingBillableItems.get(i).setDateRangeTo(service.getToDate());
		}

		service.setBillingBillableItems(billingBillableItems);
		service.setBillingServiceStatuses(getServiceStatuses(service, statusId, remarks));


		service = serviceRepository.save(service);

		return service;
	}


	/**
	 * For Verified Bill with system Count
	 * @author lrit-billing
	 * 
	 */
	@Override
	public void verifyBillWithSystemCount(Service bill,String remarks) {
		logger.info("VerifyBillWithSystemCount()");
		logger.info("BillingserviceId-"+bill.getBillingServiceId());
		Integer statusId = 9;
		String	remarks1 = "Bill Verified";

		Bill bill_1 = getBill(bill.getBillingServiceId());
		bill_1 = (Bill) setServiceStatus(bill_1, statusId, remarks1);
		bill = serviceRepository.save(bill_1);

	}

	/**
	 * To Set the Status of Bill to SENDVERIFIEDBILLDG
	 * @author lrit-billing
	 * 
	 */
	@Override
	public void setStatusSendVerifiedBill(Integer billingServiceId) {
		logger.info("setStatusSendVerifiedBill()");
		logger.info("BillingserviceId-"+billingServiceId);
		Integer statusId = 10;
		String	remarks1 = "Bill Send";
		Bill bill = getBill(billingServiceId);
		bill = (Bill) setServiceStatus(bill, statusId, remarks1);
		bill = serviceRepository.save(bill);
	}

	/**
	 * To Set Status as Verified
	 * @author lrit-billing
	 * 
	 */
	private void setStatusAsVerified(Service bill, Integer statusId,String remarks) {
		logger.info("setStatusAsVerified()");
		logger.info("BillingserviceId-"+bill.getBillingServiceId());
		Status billingStatus = statusRepository.findById(statusId).get();
		ServiceStatus serviceStatus = new ServiceStatus();

		serviceStatus.setRemarks(remarks);
		serviceStatus.setBillingService(bill);
		serviceStatus.setBillingStatus(billingStatus);

		List<ServiceStatus> billingServiceStatuses = (bill.getBillingServiceStatuses() != null)? bill.getBillingServiceStatuses(): new ArrayList<ServiceStatus>();

		billingServiceStatuses.add(serviceStatus);
		bill.setBillingServiceStatuses(getServiceStatuses(bill, statusId, remarks));
		bill = serviceRepository.save(bill);
	}



	/**
	 * To get All BillDto by Contract Type,From Date and To Date
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<BillDto> findAllBillDto(Integer contractType, String fromDate, String toDate) {
		List<BillDto> listServiceBill = null;
		logger.info("findAllBillDto()");
		logger.info("contractType-"+contractType);
		logger.info("fromDate-"+fromDate);
		logger.info("toDate-"+toDate);
		if (fromDate.trim().equals("") && toDate.trim().equals("")) {
			listServiceBill = billRepository.findAllBillDtoByContractType(contractType);

		} else {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				listServiceBill = billRepository.findAllBillDtoByContractTypeDate(contractType,
						formatter.parse(fromDate), formatter.parse(toDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		return listServiceBill;
	}

	/**
	 * To get bill By Id
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Bill getBillById(Integer billingServiceId) {
		logger.info("getBillById()");
		logger.info("billingServiceId-"+billingServiceId);
		Bill bill = billRepository.findById(billingServiceId).get();
		Contract contract = contractService.getContractByContractId(bill.getBillingContract().getContractId());
		Double conversionRate = contract.getBillingCurrencyType().getConversionRate();
		List<BillableItem> billableItems = bill.getBillingBillableItems(); 
		 
		//billableItems = billableItemRepository.findAllByBillingServiceId(billingServiceId);
		for ( BillableItem billableItem : billableItems) {
			PaymentBillableItemCategory paymentBillableItemCategory = billableItem.getBillingPaymentBillableItemCategory();
			
			Double rate = paymentBillableItemCategory.getRate(); 
			logger.info("rate " + rate );
			logger.info("rate " + rate + " " + paymentBillableItemCategory.hashCode() + " " + billableItem.hashCode());
		}
		for ( BillableItem billableItem : billableItems) {
			int bic = billableItem.getBillingPaymentBillableItemCategory().getBillableItemCategoryId();
			Date fromDate = billableItem.getDateRangeFrom(); 
			Date toDate = billableItem.getDateRangeTo(); 
			Double rate = paymentBillableItemCategoryLogRepo.getBillableItemCategoryRate(bic, fromDate, toDate) ;
			Double convertedRate = rate * conversionRate;
			convertedRate =  Math.round(convertedRate * 100.0) / 100.0;
			billableItem.setRate(convertedRate);
			//billableItemCategory.setItemName(paymentBillableItemCategory.getItemName());
			
			//logger.info("rate " + rate + " convertedRate " + convertedRate + " " + paymentBillableItemCategory.hashCode() + " " + billableItem.hashCode());
			//paymentBillableItemCategory.setRate(convertedRate);
			
			
		}

		bill.setBillingBillableItems(billableItems);
		return bill;

	}

	/**
	 * To get Bill by Id and Set Agency 2
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Bill getBill(Integer billingServiceId) {
		logger.info("getBill()");
		logger.info("billingServiceId-"+billingServiceId);
		Bill bill = billRepository.findById(billingServiceId).get();
		Agency agency2 = contractService.getAgencyByAgencyCode(bill.getBillingContract().getAgency2().getAgencyCode());
		bill.getBillingContract().setAgency2Name(agency2.getName());
		return bill;
	}

	/**
	 * To get agency Name by Agency Code
	 * @author lrit-billing
	 * 
	 */
	@Override
	public String getAgencyName(String agencyCode) {
		logger.info("getAgencyName()");
		logger.info("agencyCode-"+agencyCode);
		if(agencyCode.equals("0000")) {
			String	agencyName = "CSP";
			return agencyName;
		}

		String agencyName = lritAspMstRepository.getAgencyName(agencyCode);
		if(agencyName == null)
			agencyName = lritDAtacentreMstRepository.getNameByDCLritid(agencyCode);

		logger.info("agencyName-"+agencyName);
		return agencyName;
	}



	/**
	 * To Verify Bill by BillId
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Bill verifyBill(Integer billingServiceId) {
		// TODO Auto-generated method stub
		logger.info("verifyBill()");
		logger.info("billingServiceId-"+billingServiceId);
		Bill bill = (Bill) serviceRepository.findById(billingServiceId).get();
		List<BillableItem> billableItems = bill.getBillingBillableItems();
		int contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
		logger.info("contractTypeId-"+contractTypeId);
		for (BillableItem billableItem : billableItems) {

			String consumer = billableItem.getConsumer();
			String provider = billableItem.getProvider(); 
			String stakeholderName = contractService.getStakeholderName(consumer, contractTypeId);
			billableItem.setConsumerName(stakeholderName);
			// billableItem.setProviderName(serviceService.getCGName(billableItem.getConsumer()));
			stakeholderName = contractService.getStakeholderName(provider, contractTypeId);
			billableItem.setProviderName(stakeholderName);
			PaymentBillableItemCategory paymentBillableItemCategory = billableItem.getBillingPaymentBillableItemCategory();
			int bic = paymentBillableItemCategory.getBillableItemCategoryId(); 

			Date fromDate = bill.getFromDate(); 
			Date toDate = bill.getToDate(); 
			int systemCount = getBillableItemSystemCount(provider, consumer, fromDate, toDate, bic, contractTypeId); 
			billableItem.setSystemCount(systemCount);
		}

		bill.setBillingBillableItems(billableItems);

		return bill;
	}

	/**
	 * Get list of Billable Items by Contract ID, From Date and To Date
	 * @author lrit-billing
	 * 
	 */
	public List<BillableItem> getBillableItems(Integer contractId,
			Date fromDate, Date toDate) {
		logger.info("getBillableItems()");
		logger.info("contractId-"+contractId);
		logger.info("fromDate-"+fromDate);
		logger.info("toDate-"+toDate);
		Contract contract = contractService.getContract(contractId);
		List<BillableItem> billableItems = new ArrayList<BillableItem>();

		java.sql.Date fromDateSql = new java.sql.Date(fromDate.getTime());
		java.sql.Date toDateSql = new java.sql.Date(toDate.getTime());


		List<ContractStakeholder> providers = contractService.getValidContractStakeholderListByDates(contractId, false, fromDateSql, toDateSql);
		List<ContractStakeholder> consumers = contractService.getValidContractStakeholderListByDates(contractId, true, fromDateSql, toDateSql);

		java.sql.Date billableItemFromDate = null; 
		java.sql.Date billableItemToDate = null;

		java.sql.Date providerFromDate = null; 
		java.sql.Date providerToDate = null;

		java.sql.Date consumerFromDate = null; 
		java.sql.Date consumerToDate = null;

		ContractType contractType = contract.getBillingContractType();
		logger.info("contractType " + contractType + " fromDate " + fromDate + " toDate " + toDate);
		List<PaymentBillableItemCategory> paymentBillableItemCategorys = (List<PaymentBillableItemCategory>) paymentBillableItemCategoryLogRepo
				.findPbicByBillingContractTypeAndValidFromAndValidTill(contractType, fromDate, toDate);


		java.sql.Date indianCGFromDate = null;
		java.sql.Date indianCGToDate = null; 

		for (ContractStakeholder consumerCS : consumers) {

			String indianCG = consumerCS.getMemberId();
			if (indianCG.equals(indianCGLritId)) {
				indianCGFromDate = consumerCS.getValidFrom();
				indianCGToDate = consumerCS.getValidTo();
			}
		}
		Double conversionRate = contract.getBillingCurrencyType().getConversionRate();
//		for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategorys) {
//			
//			Double rate = paymentBillableItemCategory.getRate(); 
//			Double convertedRate = rate * conversionRate;
//			convertedRate =  Math.round(convertedRate * 100.0) / 100.0;
//			logger.info("rate " + rate + " convertedRate " + convertedRate + " conversionRate " + conversionRate);
//			paymentBillableItemCategory.setRate(convertedRate);
//		}
		
		
		
		
		
		logger.info("Providers size " + providers.size());
		logger.info("Providers " + providers);
		logger.info("Consumers size " + consumers.size());
		logger.info("Consumers  " + consumers);
		logger.info("Payment billableItem category size " + paymentBillableItemCategorys.size());
		logger.info("Payment billableItem categorys " + paymentBillableItemCategorys);

		for (ContractStakeholder providerCS : providers) {

			String provider = providerCS.getMemberId();
			providerFromDate = providerCS.getValidFrom(); 
			providerToDate = providerCS.getValidTo();
			for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategorys) {

				BillableItem billableItemNew = new BillableItem();

				int contractTypeId = paymentBillableItemCategory.getBillingContractType().getContractTypeId();
				
				
				billableItemNew.setBillingPaymentBillableItemCategory(paymentBillableItemCategory);
				billableItemNew.setConsumer(indianCGLritId);
				String consumerName = contractService.getStakeholderName(indianCGLritId, contractTypeId);
				billableItemNew.setConsumerName(consumerName);

				billableItemNew.setProvider(provider);
				String providerName = contractService.getStakeholderName(provider, contractTypeId);
				billableItemNew.setProviderName(providerName);
				int bic = paymentBillableItemCategory.getBillableItemCategoryId();
				Date billableItemNewFromDate =  highestDate(providerFromDate, indianCGFromDate, fromDateSql); 
				Date billableItemNewToDate = lowestDate(providerToDate, indianCGToDate, toDateSql);
				billableItemNew.setDateRangeFrom(billableItemNewFromDate); 
				billableItemNew.setDateRangeTo(billableItemNewToDate);
				
				Double rate = paymentBillableItemCategoryLogRepo.getBillableItemCategoryRate(bic, billableItemNewFromDate, billableItemNewToDate) ;
				Double convertedRate = rate * conversionRate;
				convertedRate =  Math.round(convertedRate * 100.0) / 100.0;
				billableItemNew.setRate(convertedRate);
				int systemCount = 0; 
				for (ContractStakeholder consumerCS : consumers) {

					String consumer = consumerCS.getMemberId();
					consumerFromDate = consumerCS.getValidFrom();
					consumerToDate = consumerCS.getValidTo();

					billableItemFromDate =  highestDate(providerFromDate, consumerFromDate, fromDateSql); 
					billableItemToDate = lowestDate(providerToDate, consumerToDate, toDateSql);
					//BillableItem billableItem = getBillableItem(provider, consumer, paymentBillableItemCategory,
							//billableItemFromDate, billableItemToDate, conversionRate);
					//systemCount += billableItem.getSystemCount();
					
					systemCount += getBillableItemSystemCount(provider, consumer, billableItemFromDate, billableItemToDate, bic, contractTypeId); 
				}
				billableItemNew.setSystemCount(systemCount);
				billableItemNew.setManualCount(systemCount);
				
			
				double cost = convertedRate * systemCount;
				cost = Math.round(cost * 100.0) / 100.0;
				billableItemNew.setCost(cost);
				
				logger.info("Provider " + provider + " Consumer " + consumerName + " systemCount " + systemCount);
				billableItems.add(billableItemNew);

			}
		}
		return billableItems;
	}

	/**
	 * To get Billable Item by provider, consumer, paymentBillableItemCategory, from Date, to Date
	 * @author lrit-billing
	 * 
	 */
	@Override
	public BillableItem getBillableItem(String provider, String consumer,
			PaymentBillableItemCategory paymentBillableItemCategory, Date fromDate, Date toDate) {

		logger.info("getBillableItem()");
		logger.info("provider " + provider);
		logger.info("consumer " + consumer);
		logger.info("paymentBillableItemCategoryID " + paymentBillableItemCategory.getBillableItemCategoryId());
		logger.info("fromDate " + fromDate);
		logger.info("toDate " + toDate);

		BillableItem billableItem = super.getBillableItem(provider, consumer, paymentBillableItemCategory, fromDate, toDate);

		int bic = paymentBillableItemCategory.getBillableItemCategoryId(); 
		int contractTypeId = paymentBillableItemCategory.getBillingContractType().getContractTypeId();
		
		int systemCount = getBillableItemSystemCount(provider, consumer, fromDate, toDate, bic, contractTypeId);
		billableItem.setSystemCount(systemCount);
		


		int manualCount = systemCount;
		billableItem.setManualCount(manualCount);
		
	
		
		double rate = paymentBillableItemCategory.getRate(); 
		billableItem.setCost(manualCount * rate);
		return billableItem;

	}

	
	/**
	 * To get Billable Item by provider, consumer, paymentBillableItemCategory, from Date, to Date,conversion rate
	 * @author lrit-billing
	 * 
	 */
	@Override
	public BillableItem getBillableItem(String provider, String consumer,
			PaymentBillableItemCategory paymentBillableItemCategory, Date fromDate, Date toDate,double conversionRate) {

		logger.info("getBillableItem()");
		logger.info("provider " + provider);
		logger.info("consumer " + consumer);
		logger.info("paymentBillableItemCategoryID " + paymentBillableItemCategory.getBillableItemCategoryId());
		logger.info("fromDate " + fromDate);
		logger.info("toDate " + toDate);

		BillableItem billableItem = super.getBillableItem(provider, consumer, paymentBillableItemCategory, fromDate, toDate);

		int bic = paymentBillableItemCategory.getBillableItemCategoryId(); 
		int contractTypeId = paymentBillableItemCategory.getBillingContractType().getContractTypeId();
		
		int systemCount = getBillableItemSystemCount(provider, consumer, fromDate, toDate, bic, contractTypeId);
		billableItem.setSystemCount(systemCount);
		


		int manualCount = systemCount;
		billableItem.setManualCount(manualCount);

		Double rate = paymentBillableItemCategory.getRate(); 
		Double convertedRate = rate * conversionRate;
		convertedRate =  Math.round(convertedRate * 100.0) / 100.0;
		logger.info("rate " + rate + " convertedRate " + convertedRate + " conversionRate " + conversionRate);
		paymentBillableItemCategory.setRate(convertedRate);
		
		//double rate = paymentBillableItemCategory.getRate(); 
		billableItem.setCost(manualCount * convertedRate);
		billableItem.setRate(convertedRate);
		return billableItem;

	}
	
	/**
	 * To get CSP Contract By Contract ID
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractCSP getCSPContractByContractId(Integer contractId) {
		logger.info("getCSPContractByContractId()");
		logger.info("contractId " + contractId);
		return billRepository.getCSPContractByContractId(contractId);
	}







}
