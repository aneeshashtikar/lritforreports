package in.gov.lrit.billing.service.contract;

import java.util.List;

import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.agency.Agency;

/**
 * Service layer interface to handle ContractCSP entity related activities 
 * @author lrit_billing
 * 
 */
public interface ContractCSPService {
	public void save(ContractCSP contractCSP);

	/**
	 * @param i
	 * @return
	 */
	public ContractType findById(int i);

	/**
	 * @return
	 */
	public List<ContractCSP> getListCspContract();

	/**
	 * @return
	 */
	public Agency getAgencyCSP(); 
	
}
