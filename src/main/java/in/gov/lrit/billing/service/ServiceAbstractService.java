/**
 * @MainService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 30-Oct-2019
 */
package in.gov.lrit.billing.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.Status;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in_.gov.lrit.ddp.dto.LritNameDto;
import net.sf.jasperreports.engine.JRException;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public interface ServiceAbstractService {

	Service saveService(Service service);

	Service setServiceStatus(Service service, Integer statusId, String remarks);

	String getAgencyName(String agencyCode);

	List<LritNameDto> getAllAgency();

	HashMap<String, HashMap<String, List<BillableItem>>> getHashedBillableItems(Integer billingServiceId);

	Service updateMessageCount(Service service, Integer billingServiceId) throws SQLException, JRException, IOException;

	String getCGName(String consumer);

	String getNameContractTypeBased(String id, Integer contractType);

	HashMap<String, PaymentDetailsPerMember> getUncheckedMembers(Service service);

	Double getRemainingAmountService(Service service);

	Service updatePayment(Service paymentService, Service service, List<String> consumers);

	HashMap<String, HashMap<String, List<BillableItem>>> getHashedBillableItems(Integer contractId, Date fromDate,
			Date toDate);

	List<BillableItem> getCGBillableItemForService(Integer billingServiceId);

	// Get billable Item system count
	Integer getBillableItemSystemCount(String provider, String consumer, Date fromDate, Date toDate, Integer bic,
			Integer contractTypeId);

	// Get payment billable Item category
	List<PaymentBillableItemCategory> getPaymentBillableItemCategoryList(Integer contractId);

	BillableItem getBillableItem(String provider, String consumer,
			PaymentBillableItemCategory paymentBillableItemCategory, Date fromDate, Date toDate);
	
	

	List<BillableItem> getBillableItems(Integer contractId, Date fromDate, Date toDate);

	Service setProviderConsumerName(Service service);

	Service setPaymentConsumerName(Service service);

	Service updatePayment(Service service, List<String> consumers, Integer billingServiceId,byte[] paymentDocument,String paymentDate);

	Service setPaymentMembersName(Service service);

	Status getLatestStatus(Integer serviceId);

}
