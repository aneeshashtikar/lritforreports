/**
 * @ReminderLogServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 22-Aug-2019
 */
package in.gov.lrit.billing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.billing.dao.ReminderLogRepository;
import in.gov.lrit.billing.model.billingservice.ReminderLog;

/**
 * 
 * 
 * @author lrit-billing
 *
 */
@Service
public class ReminderLogServiceImpl implements ReminderLogService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ReminderLogServiceImpl.class);

	@Autowired
	ReminderLogRepository reminderLogRepository;

	/**
	 * PERSIST REMINDERLOG IN DATABASE
	 * 
	 * @author lrit-billing
	 * @param reminderLog
	 * @return
	 */
	@Transactional(value = "portalTransactionManager")
	@Override
	public ReminderLog save(ReminderLog reminderLog) {
		logger.info("INSIDE save ");
		logger.info("reminderLog : " + reminderLog);

		return reminderLogRepository.save(reminderLog);
	}

}
