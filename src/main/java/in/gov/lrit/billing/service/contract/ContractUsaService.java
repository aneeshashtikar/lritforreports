package in.gov.lrit.billing.service.contract;

import java.util.List;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.ContractUsa;

/**
 * Service layer interface to handle ContractUsa entity related activities 
 * @author lrit_billing
 * 
 */
public interface ContractUsaService {

	

	
	/**
	 * @param contractTypeId
	 * @return
	 */
	ContractType findById(Integer contractTypeId);

	
	/**
	 * @return
	 */
	List<ContractUsa> getListUsaContract();


	/**
	 * @param contractUSA
	 */
	void save(ContractUsa contractUSA);



}
