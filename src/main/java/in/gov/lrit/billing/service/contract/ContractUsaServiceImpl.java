package in.gov.lrit.billing.service.contract;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import in.gov.lrit.billing.dao.contract.ContractStakeholderRepository;
import in.gov.lrit.billing.dao.contract.ContractUsaRepository;
import in.gov.lrit.billing.dao.contract.agency.AgencyRepository;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.ContractUsa;
import in.gov.lrit.billing.model.contract.agency.Agency;

/**
 * Service layer implementation to handle ContractUsa entity related activities
 * 
 * @author lrit_billing
 * 
 */
@Service
public class ContractUsaServiceImpl implements ContractUsaService {


	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractUsaServiceImpl.class);
		
	@Autowired
	private ContractUsaRepository contractUsaRepository;
	
	@Autowired
	private AgencyRepository agencyRepo;

	@Autowired
	private ContractStakeholderRepository contractStakeholderRepo;
	
	@Autowired
	private ContractService contractService;


	/**
	 * To Save Contract USA
	 * @author lrit-billing
	 * 
	 */

	@Override
	public void save(ContractUsa contractUSA) {
		
		logger.info("save()");
		logger.info("ContractUsaContractNo."+contractUSA.getContractNumber());
		
		Agency agency1 = contractUSA.getAgency1();
		Agency agency2 = contractUSA.getAgency2();
		
		logger.info("agecny1"+agency1.getAgencyCode());
		logger.info("agency2"+agency2.getAgencyCode());
		
		String agency2Code = agency2.getAgencyCode();
		
		ContractType contractType = contractUSA.getBillingContractType();
		int contractTypeId = contractType.getContractTypeId();
		String agency2Name = contractService.getAgencyName(contractTypeId, agency2Code);

		String agency1Name = contractService.getAgencyName(contractUSA.getBillingContractType().getContractTypeId(), agency1.getAgencyCode());

		agency1.setName(agency1Name);
		agency2.setName(agency2Name);
		agency2.setContractType(contractType);
		Agency savedAgency1 = agencyRepo.save(agency1);
		Agency savedAgency2 = agencyRepo.save(agency2);
		
	
		contractUSA.setAgency1(savedAgency1);
		contractUSA.setAgency2(savedAgency2);
		
		ContractUsa savedContractUSA = contractUsaRepository.saveAndFlush(contractUSA);
		
		List<ContractStakeholder> contractStakeholders = contractUSA.getContractStakeholders();

		for (int i = 0; i < contractStakeholders.size(); i++) { 
			ContractStakeholder contractStakeholder = contractStakeholders.get(i);
			contractStakeholder.setBillingContract(savedContractUSA);
			contractStakeholderRepo.save(contractStakeholder);
		}
		 
		
	}


	/**
	 * To Get Contract Typpe by ID
	 * @author lrit-billing
	 * 
	 */

	@Override
	public ContractType findById(Integer contractTypeId) {
		// TODO Auto-generated method stub
		return null;
	}
	

	/**
	 * To Get List of USA Contract
	 * @author lrit-billing
	 * 
	 */

	public List<ContractUsa> getListUsaContract() {
		logger.info("getListUsaContract()");
		return contractUsaRepository.findAll();
	}



}
