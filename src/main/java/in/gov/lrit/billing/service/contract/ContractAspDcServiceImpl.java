package in.gov.lrit.billing.service.contract;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.billing.controller.contract.ContractController;
import in.gov.lrit.billing.dao.contract.ContractAspDcRepository;
import in.gov.lrit.billing.dao.contract.ContractRepository;
import in.gov.lrit.billing.dao.contract.ContractStakeholderRepository;
import in.gov.lrit.billing.dao.contract.agency.AgencyRepository;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.ddp.dao.LritAspMstRepository;
import in.gov.lrit.ddp.dao.LritDatacentreMstRepository;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;

/**
 * Service layer implementation to handle ContractAspDC entity related
 * activities
 * 
 * @author lrit_billing
 * 
 */
@Service
public class ContractAspDcServiceImpl implements ContractAspDcService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractAspDcServiceImpl.class);
	
	@Autowired
	private ContractRepository contractRepo;

	@Autowired 
	private ContractService contractService;
	
	@Autowired
	private ContractAspDcRepository contractAspDcRepository;

	@Autowired
	private AgencyRepository agencyRepo;

	@Autowired
	private ContractStakeholderRepository contractStakeholderRepo;
	
	@Autowired
	private LritAspMstRepository lritAspMstRepository;

	@Autowired
	private LritDatacentreMstRepository lritDAtacentreMstRepository;

	/**
	 * To save the ASP-DC Contract
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractAspDc save(ContractAspDc contractAspDc) {
		logger.info("save()");
		Agency agency1 = contractAspDc.getAgency1();
		Agency agency2 = contractAspDc.getAgency2(); 
		logger.info("Agency1Code-"+agency1.getAgencyCode());
		logger.info("Agency2Code-"+agency2.getAgencyCode());
		String agency2Code = agency2.getAgencyCode();
		String agency1Name = contractService.getAgencyName(contractAspDc.getBillingContractType().getContractTypeId(), agency1.getAgencyCode());
		
		agency1.setName(agency1Name);
		Agency savedAgency1 = agencyRepo.save(agency1);
		// agency2Code currently contain lritid-name-ddpver
		// We need to retrieve an object of type LritNameDdpVerDto from the agency2Code
		// After that we can set agencyCode of agency2 to lritid of LritNameDdpVerDto and name of agency2 to name of LritNameDdpVerDto
		LritNameDdpVerDto lritNameDdpVerDto = contractService.getLritNameDdpVerDtofromAgencyCode(agency2Code);
		agency2.setAgencyCode(lritNameDdpVerDto.getLritid());
		
		ContractType contractType = contractAspDc.getBillingContractType();
		agency2.setAgencyCode(lritNameDdpVerDto.getLritid());
		
		
		if(agency2.getName().equals("")) {
			agency2.setName(lritNameDdpVerDto.getName());
		}

		contractAspDc.setAgency1(savedAgency1);
		
		agency2.setContractType(contractType);
		Agency savedAgency2 = agencyRepo.save(agency2);

		contractAspDc.setAgency2(savedAgency2);
		ContractAspDc savedContractAspDc = contractAspDcRepository.saveAndFlush(contractAspDc);
		List<ContractStakeholder> contractStakeholders = contractAspDc.getContractStakeholders();

		for (int i = 0; i < contractStakeholders.size(); i++) { 
			ContractStakeholder contractStakeholder = contractStakeholders.get(i);
			contractStakeholder.setBillingContract(savedContractAspDc);
			contractStakeholderRepo.save(contractStakeholder);
		}
		return savedContractAspDc;

	}
	
	/**
	 * To get List of all Asp-Dc Contract
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<ContractAspDc> getListAspDcContract() {
		logger.info("getListAspDcContract()");
		return contractAspDcRepository.findAll();
	}

	/**
	 * To get Asp-Dc Contract by Contract Id
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractAspDc getContractDocument(Integer contractId) {
		logger.info("getContractDocument()");
		logger.info("contractId"+contractId);
		
		return contractAspDcRepository.getOne(contractId);
	}

	/**
	 * To Get Asp-DCs by Date
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritNameDdpVerDto> getAspDcsByDate(Date date) {
		// TODO Auto-generated method stub
		logger.info("getAspDcsByDate()");
		logger.info("date"+date);
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		List<LritNameDdpVerDto> lritNameDdpVerDtos = lritAspMstRepository.getLritNameDdpVerDtoForAgency2ByDate(sqlDate);
		lritNameDdpVerDtos.addAll(lritDAtacentreMstRepository.getLritNameDdpVerDtoForAgency2ByDate(sqlDate));
		return lritNameDdpVerDtos;
	}

	
}
