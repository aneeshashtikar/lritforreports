/**
 * @ContractTypeServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 21-Aug-2019
 */
package in.gov.lrit.billing.service.contract;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.billing.dao.contract.ContractTypeRepository;
import in.gov.lrit.billing.model.contract.ContractType;

/**
 * 
 * Service layer implementation to handle ContractType entity related activities
 * @author lrit_billing
 *
 */
@Service
public class ContractTypeServiceImpl implements ContractTypeService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractTypeServiceImpl.class);
	
	@Autowired
	ContractTypeRepository contractTypeRepository;


	/**
	 * To Get All Contract Types
	 * @author lrit-billing
	 * 
	 */

	@Override
	public Collection<ContractType> findAllContractType() {
		logger.info("findAllContractType()");
		return contractTypeRepository.findAll();
	}


	/**
	 * To get Contract Type by Contract Type ID
	 * @author lrit-billing
	 * 
	 */

	@Override
	public ContractType findByContractId(int contractTypeId) {
		// TODO Auto-generated method stub
		logger.info("findByContractId()");
		logger.info("contractTypeId"+contractTypeId);
		return contractTypeRepository.findById(contractTypeId).get();
	}

}
