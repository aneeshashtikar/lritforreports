/**
 * @ReminderLogService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 22-Aug-2019
 */
package in.gov.lrit.billing.service;

import in.gov.lrit.billing.model.billingservice.ReminderLog;

/**
 * 
 * @author lrit-billing
 *
 */
public interface ReminderLogService {

	public ReminderLog save(ReminderLog reminderLog);
}
