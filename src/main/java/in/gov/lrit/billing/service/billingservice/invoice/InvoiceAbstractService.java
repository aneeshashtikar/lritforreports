/**
 * @InvoiceAbstractService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 30-Oct-2019
 */
package in.gov.lrit.billing.service.billingservice.invoice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import in.gov.lrit.billing.model.billingservice.invoice.ApprovedInvoice;
import in.gov.lrit.billing.model.billingservice.invoice.GeneratedInvoice;
import in.gov.lrit.billing.model.billingservice.invoice.Invoice;
import in.gov.lrit.billing.service.ServiceAbstractService;
import in_.gov.lrit.billing.dto.InvoiceDto;
import net.sf.jasperreports.engine.JRException;

/**
 * @author lrit-billing
 *
 */
public interface InvoiceAbstractService extends ServiceAbstractService {

	Invoice getInvoice(Integer invoiceId);

	String getInvoiceNumber(int year);

	Invoice generateInvoice(Invoice invoice) throws SQLException, JRException, IOException;

	GeneratedInvoice saveGeneratedInvoicePDF(Invoice invoice) throws SQLException, JRException, IOException;

	List<InvoiceDto> fetchInvoices(Integer contractType, Date fromDate, Date toDate);

	Invoice sendInvoiceToDG(Invoice invoicePara, Integer invoiceId) throws MessagingException;

	void mailInvoiceToAgency(String mailBody, String remarks, Integer invoiceId) throws MessagingException;

	Invoice updateInvoicePayment(Invoice service, List<String> consumers, Integer invoiceId);

	byte[] exportPdfFile(Invoice invoice) throws SQLException, JRException, IOException;

	GeneratedInvoice getLatestGeneratedInvoice(Integer generatedInvoiceId);

	ApprovedInvoice getLatestApprovedInvoice(Integer invoiceId);

	GeneratedInvoice fetchGeneratedInvoice(Integer generatedInvoiceId);

	ApprovedInvoice fetchApprovedInvoice(Integer approvedInvoiceId);

	List<GeneratedInvoice> fetchAllGeneratedInvoicePDF(Integer billingServiceId);

	List<ApprovedInvoice> fetchAllApprovedInvoicePDF(Integer billingServiceId);

	void mailInvoiceToAgency(String mailBody, String remarks, Integer invoiceId, String cc) throws MessagingException;

}
