package in.gov.lrit.billing.service.billingservice.invoice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import in.gov.lrit.billing.dao.billingservice.StatusRepository;
import in.gov.lrit.billing.dao.billingservice.invoice.InvoiceRepository;
import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.invoice.ApprovedInvoice;
import in.gov.lrit.billing.model.billingservice.invoice.GeneratedInvoice;
import in.gov.lrit.billing.model.billingservice.invoice.Invoice;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.service.ServiceService;
import in.gov.lrit.mail.LRITMailSender;
import in_.gov.lrit.billing.dto.BillInvoiceListDto;
import in_.gov.lrit.billing.dto.InvoiceDto;

/**
 * Service layer implementation to handle Invoice entity related activities
 * 
 * @author Shivraj
 * 
 */
@org.springframework.stereotype.Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	@Qualifier("serviceServiceImpl")
	private ServiceService serviceService;

	@Autowired
	private StatusRepository statusRepostiory;

	@Autowired
	private SimpleDateFormat simpleDateFormat;

	@Autowired
	private LRITMailSender lritMailSender;

	@Override
	public Invoice saveInvoice(Invoice invoice) {

		Invoice returnInvoice = null;
		String remarks = "";
//		1;"Generated"
//		2;"ReGenerated"
//		3;"SentToDGSForApproval"
//		4;"ApprovedByDGS"
//		5;"SentToCGForPayment"
//		6;"Settled"
//		7;"PartiallySettled"
//		8;"Received"
//		9;"Verified"
//		10;"SentToDGShippingForPayment"

		Integer statusId = 1;
		List<BillableItem> billingBillableItems = serviceService
				.getCGBillableItems(invoice.getBillingContract().getContractId());

		// TO SET EVERY BILLABLEITEM INVOICE AS REFERENCE
		for (int i = 0; i < billingBillableItems.size(); i++) {
			billingBillableItems.get(i).setBillingService(invoice);
		}

		invoice.setBillingBillableItems(billingBillableItems);
		invoice.setBillingServiceStatuses(serviceService.getServiceStatuses(invoice, statusId, remarks));

		returnInvoice = invoiceRepository.save(invoice);
		return returnInvoice;
	}

	@Override
	public Invoice saveShippingInvoice(Invoice invoice) {
		Invoice returnInvoice = null;
		String remarks = "";

		Integer statusId = 1;
		List<BillableItem> billingBillableItems = serviceService
				.getVesselsBillableItems(invoice.getBillingContract().getContractId());

		// TO SET EVERY BILLABLEITEM INVOICE AS REFERENCE
		for (int i = 0; i < billingBillableItems.size(); i++) {
			billingBillableItems.get(i).setBillingService(invoice);
		}

		invoice.setBillingBillableItems(billingBillableItems);
		invoice.setBillingServiceStatuses(serviceService.getServiceStatuses(invoice, statusId, remarks));

		returnInvoice = invoiceRepository.save(invoice);
		return returnInvoice;
	}

	@Override
	public Invoice saveInvoiceService(Service invoice) {
		return (Invoice) serviceService.saveService(invoice,
				serviceService.getCGBillableItems(invoice.getBillingContract().getContractId()));
	}

	@Override
	public Invoice saveShippingInvoiceService(Service invoice) {
		return (Invoice) serviceService.saveService(invoice,
				serviceService.getVesselsBillableItems(invoice.getBillingContract().getContractId()));
	}

	@Override
	public Collection<Invoice> findAllInvoice(Integer contractType, String fromDate, String toDate) {
		Collection<Invoice> listServiceInvoice = null;

		if (fromDate.trim().equals("") && toDate.trim().equals("")) {
			listServiceInvoice = invoiceRepository.findAllByContractType(contractType);
		} else {
			try {
				listServiceInvoice = invoiceRepository.findAllByContractTypeDate(contractType,
						simpleDateFormat.parse(fromDate), simpleDateFormat.parse(toDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		return listServiceInvoice;
	}

	@Override
	public List<BillInvoiceListDto> findAllInvoiceBillDto(Integer contractType, String fromDate, String toDate) {
		List<BillInvoiceListDto> listServiceInvoice = null;

		if (fromDate.trim().equals("") && toDate.trim().equals("")) {
			listServiceInvoice = invoiceRepository.findAllBillInvoiceDtoByContractType(contractType);
		} else {
			try {
				listServiceInvoice = invoiceRepository.findAllBillInvoiceDtoByContractType(contractType,
						simpleDateFormat.parse(fromDate), simpleDateFormat.parse(toDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return listServiceInvoice;
	}

	@Override
	public List<InvoiceDto> findAllInvoiceDto(Integer contractType, String fromDate, String toDate) {
		List<InvoiceDto> invoices = null;

		if (fromDate.trim().equals("") && toDate.trim().equals("")) {
			invoices = invoiceRepository.findAllInvoiceDtoByContractType(contractType);
//			return null;
		} else {
			try {
				invoices = invoiceRepository.findAllInvoiceDtoByContractType(contractType,
						simpleDateFormat.parse(fromDate), simpleDateFormat.parse(toDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		return invoices;
	}

	@Override
	public Invoice getInvoice(Integer invoiceId) {
//		System.out.println("getINVOICE()");
		Invoice invoice = invoiceRepository.findById(invoiceId).get();
//		System.out.println("getINVOICE() 2");
		invoice.getBillingContract().setAgency2Name(
				serviceService.getAgencyName(invoice.getBillingContract().getAgency2().getAgencyCode()));
		return invoice;
	}

	@Override
	public Invoice updateToDG(Invoice invoicePara, Integer invoiceId) {
		Integer statusId = 3;
		Invoice invoice = getStatusInvoice(invoiceId, statusId,
				invoicePara.getBillingServiceStatuses().get(0).getRemarks());

		// SEND TO DG THROUGH MAIL
		// lritMailSender.sendEmailWithAttachment(from, to, subject, mailBody,
		// datasource, attachmentName);

		invoice = invoiceRepository.save(invoice);
		return invoice;
	}

	@Override
	public Invoice uploadApprovedInvoice(String remarks, byte[] approvedCopyByte, Integer invoiceId) {
		Integer statusId = 4;
		Invoice invoice = getStatusInvoice(invoiceId, statusId, remarks);
//		invoice.setApprovedCopy(approvedCopyByte);
		invoice = invoiceRepository.save(invoice);
		return invoice;
	}

	@Override
	public Invoice uploadApprovedInvoice(Invoice invoice, Integer invoiceId) {
		Integer statusId = 4;
		Invoice service = getInvoice(invoiceId);

		service = (Invoice) serviceService.getServiceStatuses(service, statusId,
				invoice.getBillingServiceStatuses().get(0).getRemarks());

		service = invoiceRepository.save(service);

		return service;
	}

	/**
	 * METHOD TO PROVIDE LATEST GENERATED PDF OF INVOICE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoiceId
	 * @return
	 */
	public GeneratedInvoice getLastGeneratedInvoiceCopy(Integer invoiceId) {
		GeneratedInvoice generatedInvoice = new GeneratedInvoice();
		return generatedInvoice;

	}

	/**
	 * METHOD TO RETURN LATEST APPROVED GENERATED COPY OF INVOICE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoiceId
	 * @return
	 */
	public ApprovedInvoice getLastApprovedInvoiceCopy(Integer invoiceId) {
		ApprovedInvoice approvedInvoice = new ApprovedInvoice();
		return approvedInvoice;
	}

	@Override
	public void sendMail(String mailBody, String remarks, Integer invoiceId) {
		Integer statusId = 5;
		Invoice invoice = getStatusInvoice(invoiceId, statusId, remarks);
		sendMailToAspDc(mailBody);
		DataSource datasource = new ByteArrayDataSource(getLastApprovedInvoiceCopy(invoiceId).getApprovedCopy(),
				"application/pdf");
		try {
			lritMailSender.sendEmailWithAttachment("yogendra@cdac.in", "yogendra@cdac.in", "Regarding payment",
					mailBody, datasource, "Invoice " + invoice.getInvoiceNo());
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		invoice = invoiceRepository.save(invoice);
	}

	@Override
	public Invoice getStatusInvoice(Integer invoiceId, Integer statusId, String remarks) {
		Invoice invoice = getInvoice(invoiceId);
		// invoice.setBillingServiceStatuses(serviceService.getServiceStatuses(invoice,
		// statusId, remarks));
		invoice = (Invoice) serviceService.setServiceStatuses(invoice, statusId, remarks);
		return invoice;
	}

	@Override
	public boolean sendMailToAspDc(String mailBody) {
		lritMailSender.sendEmail("yogendra@cdac.in", "yogendra@cdac.in", "Regarding payment", mailBody);
		return true;
	}

	@Override
	public Invoice updateMessageCount(Service service, String remarks, Integer invoiceId) {

		// regenerated due to update count
		Integer statusId = 2;
		Invoice updatedInvoice = getStatusInvoice(invoiceId, statusId, remarks);

		updatedInvoice.setBillingBillableItems(serviceService
				.getUpdatedBillableItems(updatedInvoice.getBillingBillableItems(), service.getBillingBillableItems()));
		updatedInvoice.setSubTotal(service.getSubTotal());
		updatedInvoice.setGrandTotal(service.getGrandTotal());

		updatedInvoice = invoiceRepository.save(updatedInvoice);
		return updatedInvoice;
	}

	@Override
	public Invoice updateMessageCount(Service service, Integer invoiceId) {

		Invoice invoice = getInvoice(invoiceId);
		// regenerated due to update count
		Integer statusId = 2;

		invoice = (Invoice) serviceService.setServiceStatuses(invoice, statusId,
				service.getBillingServiceStatuses().get(0).getRemarks());

		invoice = (Invoice) serviceService.setBillableItems(invoice, service);

		invoice = invoiceRepository.save(invoice);

		return invoice;
	}

	@Override
	public Invoice updatePayment(Invoice service, List<String> consumers, Integer invoiceId) {
		// regenerated due to update count
		Integer statusId = 7;
		Invoice paymentService = getInvoice(invoiceId);

		paymentService = (Invoice) serviceService.setServiceStatuses(paymentService, statusId,
				service.getBillingServiceStatuses().get(0).getRemarks());

		paymentService = (Invoice) serviceService.updatePayment(paymentService, service, consumers);

		paymentService = invoiceRepository.save(paymentService);

		return paymentService;
	}

	@Override
	public String getInvoiceNumber(String dateString) {

		String invoiceNumber = null;
		Date dateUtil;
		Calendar cal = Calendar.getInstance();
		try {
			dateUtil = simpleDateFormat.parse(dateString);
			cal.setTime(dateUtil);
			invoiceNumber = "DGS/LRIT/" + (invoiceRepository.getInvoiceCount(cal.get(Calendar.YEAR)) + 1) + "/"
					+ cal.get(Calendar.YEAR);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return invoiceNumber;
	}

	@Override
	public String getInvoiceNumber(int year) {
		String invoiceNumber = "";
		invoiceNumber = "DGS/LRIT/" + (invoiceRepository.getInvoiceCount(year) + 1) + "/" + year;
		return invoiceNumber;

	}

}
