/**
 * @ServiceStatusServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 18-Oct-2019
 */
package in.gov.lrit.billing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.billing.dao.billingservice.ServiceRepository;
import in.gov.lrit.billing.dao.billingservice.ServiceStatusRepository;
import in.gov.lrit.billing.dao.billingservice.StatusRepository;
import in.gov.lrit.billing.dao.billingservice.invoice.ApprovedInvoiceRepository;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.Status;
import in.gov.lrit.billing.model.billingservice.invoice.ApprovedInvoice;

/**
 * @author lrit-billing
 *
 */
@org.springframework.stereotype.Service
public class ServiceStatusServiceImpl implements ServiceStatusService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ServiceStatusServiceImpl.class);

	@Autowired
	private ServiceStatusRepository serviceStatusRepository;

	@Autowired
	private StatusRepository statusRepository;

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private ApprovedInvoiceRepository approvedInvoiceRepository;

	@Override
	public ServiceStatus saveServiceStatus(ServiceStatus serviceStatus) {
		serviceStatus = serviceStatusRepository.save(serviceStatus);
		return serviceStatus;
	}

	/**
	 * 
	 * @author lrit-billing
	 * @param serviceStatus
	 * @return
	 */
	@Override
	public ServiceStatus saveUploadedInvoice(ServiceStatus serviceStatus) {

		// FOR SETTING STATUS
		Integer statusId = 4;
		Status status = statusRepository.getOne(statusId);
		serviceStatus.setBillingStatus(status);

		// PERSISTING INVOICE
		serviceRepository.save(serviceStatus.getBillingService());

		// PERSISTING SERVICESTATUS
		serviceStatus = serviceStatusRepository.save(serviceStatus);
		return serviceStatus;
	}

	/**
	 * 
	 * @author lrit-billing
	 * @param billingServiceId
	 * @return
	 */
	@Override
	public Status getLatestStatus(Integer billingServiceId) {
		List<ServiceStatus> serviceStatuses = serviceStatusRepository.getLatestStatuses(billingServiceId);
		if (serviceStatuses != null)
			return serviceStatuses.get(0).getBillingStatus();

		return null;
	}

	/**
	 * TO SAVE APPROVED INVOICE PDF IN DB WITH STATUS
	 * 
	 * @author lrit-billing
	 * @param serviceStatus
	 * @param approvedInvoice
	 * @return
	 */
	@Transactional(value = "portalTransactionManager")
	@Override
	public ServiceStatus saveUploadedInvoice(ServiceStatus serviceStatus, ApprovedInvoice approvedInvoice) {

		logger.info("INSIDE saveUploadedInvoice");
		logger.info("serviceStatus : " + serviceStatus);
		logger.info("approvedInvoice : " + approvedInvoice);

		// FOR SETTING STATUS
		Integer statusId = 4;
		Status status = statusRepository.getOne(statusId);
		serviceStatus.setBillingStatus(status);

		// PERSISTING INVOICE
		serviceRepository.save(serviceStatus.getBillingService());

		// Save approved invoice
		approvedInvoiceRepository.save(approvedInvoice);

		// PERSISTING SERVICESTATUS
		serviceStatus = serviceStatusRepository.save(serviceStatus);
		return serviceStatus;
	}
}
