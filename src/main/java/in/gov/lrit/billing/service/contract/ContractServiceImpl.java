package in.gov.lrit.billing.service.contract;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import in.gov.lrit.billing.dao.contract.ContractRepository;
import in.gov.lrit.billing.dao.contract.ContractStakeholderRepository;
import in.gov.lrit.billing.dao.contract.ContractTypeRepository;
import in.gov.lrit.billing.dao.contract.agency.AgencyRepository;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAnnexure;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.ContractShippingCompany;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.ContractUsa;
import in.gov.lrit.billing.model.contract.CurrencyType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.ddp.dao.LritAspInfoRepository;
import in.gov.lrit.ddp.dao.LritAspMstRepository;
import in.gov.lrit.ddp.dao.LritContractingGovtMstRepository;
import in.gov.lrit.ddp.dao.LritDatacentreInfoRepository;
import in.gov.lrit.ddp.dao.LritDatacentreMstRepository;
import in.gov.lrit.ddp.dao.LritDdpImmediateMstRepository;
import in.gov.lrit.ddp.dao.LritDdpRegularMstRepository;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.model.LritDatacentreInfo;
import in.gov.lrit.ddp.service.LritAspInfoService;
import in.gov.lrit.ddp.service.LritDatacentreInfoService;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in.gov.lrit.portal.repository.vessel.ShippingCompanyRepository;
import in.gov.lrit.portal.repository.vessel.VesselRepository;
import in_.gov.lrit.billing.dto.ContractDto;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Service layer implementation to handle Contract entity related activities
 * 
 * @author lrit_billing
 * 
 */
@Service
public class ContractServiceImpl implements ContractService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractServiceImpl.class);

	@Autowired
	private ContractRepository contractRepository;

	@Autowired
	private ContractTypeRepository contractTypeRepository;

	@Autowired
	private AgencyRepository agencyRepo;

	@Autowired
	private ContractStakeholderRepository contractStakeholderRepo;

	@Autowired
	private LritAspInfoRepository lritAspInfoRepository;

	@Autowired
	private LritDatacentreInfoRepository lritDatacentreInfoRepository;

	@Autowired
	private LritAspMstRepository lritAspMstRepository;

	@Autowired
	private LritDatacentreMstRepository lritDAtacentreMstRepository;



	@Autowired
	private SimpleDateFormat simpleDateFormat;

	@Value("${dc.india.lritid}")
	private String indianDcLritid;

	@Autowired
	LritContractingGovtMstRepository lritContractingGovtMstRepository;

	@Autowired
	private ShippingCompanyRepository portalShippingCompanyRepository;

	@Autowired
	private ContractShippingCompanyService contractShippingCompanyService;

	@Autowired
	private VesselRepository portalVesselDetailRepository;

	@Autowired
	private LritDdpRegularMstRepository lritDdpRegularMstRepo;

	@Autowired
	private LritDdpImmediateMstRepository lritDdpImmediateMstRepo;

	@Autowired
	private LritAspInfoService lritAspInfoService;

	@Autowired
	private LritDatacentreInfoService lritDatacentreInfoService;	

	/**
	 * To get all Contract Types 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<ContractType> getContractTypes() {
		logger.info("INSIDE getContractTypes()");
		List<ContractType> contractTypes = contractTypeRepository.findAll();
		logger.info("contractTypes : " + contractTypes);
		return contractTypes;
	}

	/**
	 * To get Contract Type by ContractTypeID
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractType getContractType(Integer contractType) {
		logger.info("INSIDE getContractType()");
		logger.info("contractType : " + contractType);
		ContractType contractType2 = contractTypeRepository.getOne(contractType);
		logger.info("contractType2 : " + contractType2);
		return contractType2;
	}

	/**
	 * To get contract By Contract ID
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Contract getContract(Integer contractId) {
		logger.info("INSIDE getContract()");
		logger.info("contractId : " + contractId);
		Contract contract = null;
		contract = contractRepository.findByContractId(contractId);
		logger.info("contract : " + contract);
		return contract;
	}

	/**
	 * To get All Contract List
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<Contract> getContractList() {
		logger.info("getContractList()");
		return contractRepository.findAll();
	}

	/**
	 * To get Contract List By Valid From and Valid To
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<Contract> getContractList(String validFrom, String validTo) {
		logger.info("getContractList()");
		logger.info("validFrom"+validFrom);
		logger.info("validTo"+validTo);
		try {
			return contractRepository.findAllByValidDates(simpleDateFormat.parse(validFrom),
					simpleDateFormat.parse(validTo));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * To get Contract List By Contract Type ID,Valid From ,Valid To
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<Contract> getContractList(Integer contractTypeId, String validFrom, String validTo) {

		logger.info("getContractList()");
		logger.info("contractTypeId"+contractTypeId);
		logger.info("validFrom"+validFrom);
		logger.info("validTo"+validTo);
		List<Contract> listContracts = null;

		try {
			if (contractTypeId != null) {
				listContracts = contractRepository.findAllByContractTypeAndDate(contractTypeId,
						simpleDateFormat.parse(validFrom), simpleDateFormat.parse(validTo));
			} else
				listContracts = contractRepository.findAllCGContractByDate(simpleDateFormat.parse(validFrom),
						simpleDateFormat.parse(validTo));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return listContracts;
	}

	/**
	 * To get Agency Wise Contracts By agency Code,ContracttypeID,Valid From, Valid To
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<Contract> getAgencyWiseContracts(String agencyCode, Integer contractTypeID, String validFrom,
			String validTo) {
		logger.info("getAgencyWiseContracts()");
		logger.info("agencyCode"+agencyCode);
		logger.info("contractTypeId"+contractTypeID);
		logger.info("validFrom"+validFrom);
		logger.info("validTo"+validTo);
		List<Contract> contracts = null;
		// ContractType contractType =
		// contractTypeRepository.findById(contractTypeID).get();
		try {
			contracts = contractRepository.findAllContractsOnAgencyCode(contractTypeID, agencyCode,
					simpleDateFormat.parse(validFrom), simpleDateFormat.parse(validTo));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return contracts;

	}
	/**
	 * To get agency wise Contracts by agency code, Contract Type ID, Valid From and Valid To
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<Contract> getAgencyWiseContracts(String agencyCode, Integer contractTypeID, Date validFrom,
			Date validTo) {
		logger.info("getAgencyWiseContracts()");
		logger.info("agencyCode"+agencyCode);
		logger.info("contractTypeId"+contractTypeID);
		logger.info("validFrom"+validFrom);
		logger.info("validTo"+validTo);
		List<Contract> contracts = null;
		contracts = contractRepository.findAllContractsOnAgencyCode(contractTypeID, agencyCode, validFrom, validTo);
		return contracts;

	}


	/**
	 * To get Shipping Company by Contract ID, Valid From, Valid To
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<PortalShippingCompany> getShippingCompany(Integer contractID, String validFrom, String validTo) {
		logger.info("getShippingCompany()");
		logger.info("contractID"+contractID);

		logger.info("validFrom"+validFrom);
		logger.info("validTo"+validTo);
		List<PortalShippingCompany> shippingCompanyList = new ArrayList<>();
		ContractType contractType = contractTypeRepository.findById(contractID).get();
		try {
			List<String> companyCodeList = contractRepository.findAllCompanyCodeBetweenDates(
					contractType.getContractTypeId(), simpleDateFormat.parse(validFrom),
					simpleDateFormat.parse(validTo));
			if (!companyCodeList.isEmpty()) {
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return shippingCompanyList;
	}


	/**
	 * To get Shipping Company Contract List by Company Code,ContractTypeID and Date
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<Contract> getShippingContractList(String companyCode, Integer contractTypeID, String validFrom,
			String validTo) {
		logger.info("getShippingContractList()");
		logger.info("contractTypeID"+contractTypeID);
		logger.info("companyCode"+companyCode);
		logger.info("validFrom"+validFrom);
		logger.info("validTo"+validTo);
		List<Contract> contractList = null;
		// ContractType contractType =
		// contractTypeRepository.findById(contractTypeID).get();

		try {
			contractList = contractRepository.findAllContractsOnAgencyCode(companyCode,
					simpleDateFormat.parse(validFrom), simpleDateFormat.parse(validTo));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return contractList;

	}

	/**
	 * To get ASP lritId
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<String> getLritAspInfoList() {
		logger.info("getLritAspInfoList()");
		return lritAspInfoRepository.getAspLritId();
	}

	/**
	 * To get Dc lritId
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<String> getLritDatacentreInfoList() {
		logger.info("getLritDatacentreInfoList()");
		return lritDatacentreInfoRepository.getDcLritId();
	}

	/**
	 * To get Asp-Dc Dto(LritNameDto)
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritNameDto> getLritNameDto() {
		logger.info("getLritNameDto()");
		List<LritNameDto> lritNameDtos = lritAspMstRepository.getLritNameDto();
		lritNameDtos.addAll(lritDAtacentreMstRepository.getLritNameDto());
		return lritNameDtos;
	}

	/**
	 * To get LritNameDto(LritId,Name) for Agency2 in Contract
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritNameDto> getLritNameDtoForAgency2() {
		logger.info("getLritNameDtoForAgency2()");
		List<LritNameDto> lritNameDtos = lritAspMstRepository.getLritNameDtoForAgency2();
		lritNameDtos.addAll(lritDAtacentreMstRepository.getLritNameDtoForAgency2());
		return lritNameDtos;
	}

	/**
	 * To get LritName by Date for agency 2
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritNameDdpVerDto> getLritNameDdpVerDtoForAgency2(java.sql.Date date) {
		logger.info("getLritNameDdpVerDtoForAgency2()");
		logger.info("date"+date);
		// java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		List<LritNameDdpVerDto> lritNameDdpVerDtos = lritAspMstRepository.getLritNameDdpVerDtoForAgency2ByDate(date);
		lritNameDdpVerDtos.addAll(lritDAtacentreMstRepository.getLritNameDdpVerDtoForAgency2ByDate(date));
		return lritNameDdpVerDtos;

	}

	/**
	 * To get list of Contract By ContractType
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<Contract> getListContracts(Integer contractType) {
		logger.info("getListContracts()");
		logger.info("contractType"+contractType);
		List<Contract> contracts = contractRepository.getListContracts(contractType);
		return contracts;
	}

	/**
	 * To get list of Contract By Contract Type and Dates
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<Contract> getListContracts(Integer contractType, Date fromDate, Date toDate) {
		logger.info("getListContracts()");
		logger.info("contractType"+contractType);
		logger.info("fromDate"+fromDate);
		logger.info("toDate"+toDate);
		List<Contract> contracts = null;
		contracts = contractRepository.getListContracts(contractType, fromDate, toDate);
		return contracts;
	}

	/**
	 * To get Contract By Contract ID
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Contract getContractByContractId(Integer contractId) {
		logger.info("getContractByContractId()");
		logger.info("contractId"+contractId);
		return contractRepository.getContractByContractId(contractId);
	}

	/**
	 * To Terminate the Contract By Contract ID
	 * @author lrit-billing
	 * 
	 */
	@Override
	public void terminateContractByContractId(Integer contractId) {
		logger.info("terminateContractByContractId()");
		logger.info("contractId"+contractId);
		contractRepository.terminateContractByContractId(contractId);

	}

	/**
	 * To fetch Agency 1 Contract Stakeholders by Contract id 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<String> getAgency1ContractStakeholders(Integer contractId) {
		logger.info("getAgency1ContractStakeholders()");
		logger.info("contractId"+contractId);
		return contractRepository.getAgency1ContractStakeholders(contractId);
	}


	/**
	 * To fetch Agency 2 Contract Stakeholders by Contract id 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<String> getAgency2ContractStakeholders(Integer contractId) {
		logger.info("getAgency2ContractStakeholders()");
		logger.info("contractId"+contractId);
		return contractRepository.getAgency2ContractStakeholders(contractId);
	}


	/**
	 * To fetch Agency 1 Contracting Govts. By agency Code 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> getAgency1ContractingGovt(String agencyCode) {
		logger.info("getAgency1ContractingGovt()");
		logger.info("agencyCode"+agencyCode);
		List<LritContractingGovtMst> agency1ContractingGovt = lritAspInfoRepository.getAspCgList(agencyCode);
		agency1ContractingGovt.addAll(lritDatacentreInfoRepository.findCGDetails(agencyCode));
		return agency1ContractingGovt;
	}


	/**
	 * To fetch Agency 2 Contracting Govts. By agency Code 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> getAgency2ContractingGovt(String agencyCode) {
		logger.info("getAgency2ContractingGovt()");
		logger.info("agencyCode"+agencyCode);
		List<LritContractingGovtMst> agency2ContractingGovt = lritAspInfoRepository.getAspCgList(agencyCode);
		agency2ContractingGovt.addAll(lritDatacentreInfoRepository.findCGDetails(agencyCode));
		return agency2ContractingGovt;
	}


	/**
	 * To fetch Non Stakeholders by agency Code and Agency Contract Stakeholders 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> getNonStakeholder(String agencyCode, List<String> agencyContractStakeholders) {
		logger.info("getNonStakeholder()");
		logger.info("agencyCode"+agencyCode);
		logger.info("agencyContractStakeholdersList"+agencyContractStakeholders);
		List<LritContractingGovtMst> nonStakeholders = new ArrayList<LritContractingGovtMst>();
		nonStakeholders.addAll(lritAspInfoRepository.findNonStakeholder(agencyCode, agencyContractStakeholders));
		nonStakeholders.addAll(lritDatacentreInfoRepository.findNonStakeholder(agencyCode, agencyContractStakeholders));
		logger.info("nonStakeholders"+nonStakeholders);
		return nonStakeholders;

	}


	/**
	 * To fetch Non Stakeholders with in date by agency Code and Agency Contract Stakeholders and Date 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> getNonStakeholderByDate(String agencyCode,
			List<String> agencyContractStakeholders, java.sql.Date date) {
		logger.info("getNonStakeholderByDate()");
		logger.info("agencyCode"+agencyCode);
		logger.info("agencyContractStakeholdersList"+agencyContractStakeholders);
		logger.info("date"+date);

		String ddpVer = null;
		List<LritContractingGovtMst> nonStakeholders = null;
		ddpVer = lritDdpRegularMstRepo.getApplicableRegularDdpVersionByDate(date);
		if (agencyCode.startsWith("4")) {

			nonStakeholders = lritAspInfoRepository.findNonStakeholderByAgencyCodeAndDdpVersion(agencyCode, ddpVer,
					agencyContractStakeholders);
		} else if (agencyCode.startsWith("3")) {

			nonStakeholders = lritDatacentreInfoRepository.findNonStakeholderByAgencyCodeAndDdpVersion(agencyCode,
					ddpVer, agencyContractStakeholders);
		}

		return nonStakeholders;
	}


	/**
	 * To fetch Stakeholders by agency Code and Agency Contract Stakeholders 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> getStakeholder(String agencyCode, List<String> agencyContractStakeholders) {
		logger.info("getStakeholder()");
		logger.info("agencyCode"+agencyCode);
		logger.info("agencyContractStakeholdersList"+agencyContractStakeholders);

		List<LritContractingGovtMst> stakeholders = lritAspInfoRepository.findStakeholder(agencyCode,
				agencyContractStakeholders);
		stakeholders.addAll(lritDatacentreInfoRepository.findStakeholder(agencyCode, agencyContractStakeholders));

		return stakeholders;
	}

	/**
	 * To fetch  Stakeholders by agency Code and Agency Contract Stakeholders 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> getStakeholderByDate(List<String> agencyContractStakeholders,
			java.sql.Date date) {
		logger.info("getStakeholder()");
		logger.info("agencyContractStakeholdersList"+agencyContractStakeholders);
		logger.info("date"+date);
		String ddpVer = null;
		List<LritContractingGovtMst> stakeholders = null;
		ddpVer = lritDdpRegularMstRepo.getApplicableRegularDdpVersionByDate(date);
		logger.info("ddpVer"+ddpVer);
		stakeholders = lritContractingGovtMstRepository.findCGsByDDPVerAndLritids(ddpVer, agencyContractStakeholders);
		return stakeholders;
	}

	/**
	 * To fetch agency by agency Code 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Agency getAgencyByAgencyCode(String agencyCode) {
		logger.info("getStakeholder()");
		logger.info("agencyCode"+agencyCode);
		return contractRepository.getAgencyByAgencyCode(agencyCode);
	}

	/**
	 * To fetch India as Agency 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Agency getAgencyIndia() {
		logger.info("getAgencyIndia()");
		return contractRepository.getAgencyIndia();
	}

	/**
	 * To Contract List 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Collection<Contract> getContractList(Date fromDate, Date toDate) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * To fetch agency Contract type wise 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritNameDto> getAgencyContractTypeWise(Integer contractType) {
		logger.info("INSIDE getAgencyContractTypeWise()");
		logger.info("contractType : " + contractType);
		List<LritNameDto> agencys = new ArrayList<>();
		agencys = agencyRepo.getAgencyListByContractType(contractType);

		logger.info("agencys : " + agencys);
		return agencys;
	}

	/**
	 * To fetch Saved the Contract 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Contract save(Contract contract) {
		logger.info("save");
		logger.info("ContractNo-"+contract.getContractNumber());
		Agency agency1 = contract.getAgency1();
		Agency agency2 = contract.getAgency2();
		Agency savedAgency1 = agencyRepo.save(agency1);
		String agency2Code = agency2.getAgencyCode();

		ContractType contractType = contract.getBillingContractType();
		int contractTypeId = contractType.getContractTypeId();
		String agency2Name = getAgencyName(contractTypeId, agency2Code);

		agency2.setName(agency2Name);
		contract.setAgency1(savedAgency1);

		agency2.setContractType(contractType);
		Agency savedAgency2 = agencyRepo.save(agency2);
		contract.setAgency2(savedAgency2);

		Contract savedContract = contractRepository.saveAndFlush(contract);

		List<ContractStakeholder> contractStakeholders = contract.getContractStakeholders();

		for (int i = 0; i < contractStakeholders.size(); i++) {
			ContractStakeholder contractStakeholder = contractStakeholders.get(i);
			contractStakeholder.setBillingContract(savedContract);
			contractStakeholderRepo.save(contractStakeholder);
		}

		return savedContract;

	}

	/**
	 * To Saved the Contract 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Contract saveContract(Contract contract) {
		logger.info("saveContract()");
		contractRepository.save(contract);
		return contract;
	}

	/**
	 * To get Contract Stakeholders 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<String> getContractStakeholders(Integer contractId, boolean isAgency1) {
		// TODO Auto-generated method stub
		logger.info("getContractStakeholders()");
		logger.info("contractId-"+contractId);
		return contractStakeholderRepo.getContractStakeholders(contractId, isAgency1);
	}

	/**
	 * To get Contract Stakeholders List by Contract Id 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<ContractStakeholder> getContractStakeholderList(Integer contractId, boolean isAgency1) {
		// TODO Auto-generated method stub
		logger.info("getContractStakeholders()");
		logger.info("contractId-"+contractId);
		return contractStakeholderRepo.getContractStakeholderList(contractId, isAgency1);
	}

	/**
	 * To get Stakeholder Name by member id, Contract Type Id and Contract Id  
	 * @author lrit-billing
	 * 
	 */
	@Override
	public String getStakeholderName(String memberId, Integer contractTypeId, Integer contractId) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub

		logger.info("getStakeholderName()");
		logger.info("memberId-"+memberId);
		logger.info("contractTypeId-"+contractTypeId);
		logger.info("contractId-"+contractId);
		ContractStakeholder contractStakeholder = contractStakeholderRepo.getContractStakeholder(contractId, memberId);
		java.sql.Date date = contractStakeholder.getValidFrom();
		String ddpVer = lritDdpRegularMstRepo.getApplicableRegularDdpVersionByDate(date);
		logger.info("ddpVersion"+ddpVer);
		String memberName = null;
		// Hardcoding LRIT ID of india and srilanka
		if (contractTypeId == 1 || contractTypeId == 2 || memberId.equals("1065") || memberId.equals("1136")) {
			// memberName = lritContractingGovtMstRepository.getCGName(memberId);
			memberName = lritContractingGovtMstRepository.getCGNameByDdpVersion(memberId, ddpVer);
		} else if (contractTypeId == 3) {
			memberName = "CSP";
		} else if (contractTypeId == 4) {
			memberName = portalVesselDetailRepository.findByVesselId(Integer.valueOf(memberId)).getVesselName();
		}

		return memberName;
	}

	/**
	 * To get Agency Name by Contract Type Id and Agency Code 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public String getAgencyName(Integer contractTypeId, String agencyCode) {
		// TODO Auto-generated method stub
		logger.info("getAgencyName()");
		logger.info("contractTypeId-"+contractTypeId);
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());


		logger.info("agencyCode-"+agencyCode);
		String name = null;

		if (contractTypeId == 1 || contractTypeId == 2 || agencyCode.equals(indianDcLritid)) {
			if (agencyCode.startsWith("3")) {
				String ddpVer = lritDatacentreInfoService.getApplicableImmediateDdpVersionByDate(date);
				logger.info("ddpVersion"+ddpVer);
				//name = lritDAtacentreMstRepository.getNameByDCLritid(agencyCode);
				name = lritDAtacentreMstRepository.getNameByDCLritidByDdpVersion(agencyCode,ddpVer);
			} else if (agencyCode.startsWith("4")) {
				String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date); 
				//name = lritAspMstRepository.getAgencyName(agencyCode);
				name = lritAspMstRepository.getAgencyNameByAgencyCodeAndDddpVersion(agencyCode,ddpVer);
				logger.info("ddpVersion"+ddpVer);
			}
		} else if (contractTypeId == 3) {
			name = "CSP";
		} else if (contractTypeId == 4) {
			name = portalShippingCompanyRepository.getShippingCompanyName(agencyCode);
		}

		return name;
	}

	/**
	 * To get Contract Stakeholder by Contract ID and Member Id 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractStakeholder getContractStakeholder(int contractId, String memberId) {
		// TODO Auto-generated method stub
		logger.info("getContractStakeholder()");
		logger.info("contractId-"+contractId);
		logger.info("memberId-"+memberId);
		return contractStakeholderRepo.getContractStakeholder(contractId, memberId);
	}


	/**
	 * To get Contract Stakeholder By Contract Id and Memeber ID 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractStakeholder getLatestContractStakeholder(int contractId, String memberId) {
		// TODO Auto-generated method stub
		logger.info("getLatestContractStakeholder()");
		logger.info("contractId()"+contractId);
		logger.info("memberId()"+memberId);

		return contractStakeholderRepo.getLatestContractStakeholder(contractId, memberId);
	}

	/**
	 * To get All Contracts By Contract Type id
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<ContractDto> getAllContractsByContractType(Integer contractType) {
		logger.info("getAllContractsByContractType()");
		logger.info("contractType()"+contractType);

		return contractRepository.getAllContractsByContractType(contractType);
	}

	/**
	 * To get All Contracts By Contract Type id and From and to Dates
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<ContractDto> getAllContractsByContractType(Integer contractType, Date fromDate, Date toDate) {
		return contractRepository.getAllContractsByContractType(contractType, fromDate, toDate);
	}

	/**
	 * To check two lists are equal to not
	 * @author lrit-billing
	 * 
	 */
	// Following function needs to be moved to some Util type class
	// Check whether two lists are equal or not
	public static <T> boolean listEqualsIgnoreOrder(List<T> list1, List<T> list2) {
		logger.info("listEqualsIgnoreOrder()");
		logger.info("list1"+list1);
		logger.info("list2"+list2);
		return new HashSet<>(list1).equals(new HashSet<>(list2));
	}

	/**
	 * To get Common member list 
	 * @author lrit-billing
	 * 
	 */
	// Intersection of sets
	public static Set<String> intersectionOfList(List<String> contractStakeholderMemberIdList,
			List<String> selectedMemberIdList) {
		logger.info("intersectionOfList()");
		logger.info("contractStakeholderMemberIdList "+contractStakeholderMemberIdList);
		logger.info("selectedMemberIdList "+selectedMemberIdList);
		Set<String> intersection = new HashSet<String>(contractStakeholderMemberIdList);
		intersection.retainAll(new HashSet<String>(selectedMemberIdList));
		return intersection;
	}

	/**
	 * To remove difference
	 * @author lrit-billing
	 * 
	 */
	// Intersection of sets
	public static Set<String> differenceOfList(List<String> list1, Set<String> list2) {
		logger.info("differenceOfList()");
		logger.info("list1"+list1);
		logger.info("list2"+list2);
		Set<String> difference = new HashSet<String>(list1);
		logger.info("difference"+difference);
		difference.removeAll(new HashSet<String>(list2));
		return difference;
	}


	/**
	 * To Get Lrit Name By agency Code
	 * @author lrit-billing
	 * 
	 */
	@Override
	public LritNameDdpVerDto getLritNameDdpVerDtofromAgencyCode(String agencyCode) {
		logger.info("agencyCode " + agencyCode);
		// "-" USED AS DELIMITER TO SPLIT LRIT ID, AGENCY NAME AND DDP VERSION
		String agencyData[] = agencyCode.split("-");
		logger.info("agencyData[0]" + agencyData[0]);
		logger.info("agencyData[1]" + agencyData[1]);
		logger.info("agencyData[2]" + agencyData[2]);
		return new LritNameDdpVerDto(agencyData[0], agencyData[1], agencyData[2]);
	}


	/**
	 * To get valid Contract Stakeholders by Contract Id and DAtes
	 * @author lrit-billing
	 * 
	 */

	@Override
	public List<ContractStakeholder> getValidContractStakeholderListByDates(Integer contractId, boolean isAgency1,
			java.sql.Date fromDate, java.sql.Date toDate) {
		return contractStakeholderRepo.getValidContractStakeholderListByDates(contractId, isAgency1, fromDate, toDate);
	}

	/**
	 * To Get Stakeholder Name
	 * @author lrit-billing
	 * 
	 */
	@Override
	public String getStakeholderName(String memberId, Integer contractTypeId) {
		// TODO Auto-generated method stub

		logger.info("getStakeholderName()");
		logger.info("memberId " + memberId);
		logger.info("contractTypeId : " + contractTypeId);
		String memberName = null;
		if (contractTypeId == 1 || contractTypeId == 2 || memberId.equals("1065") || memberId.equals("1136")) {
			memberName = lritContractingGovtMstRepository.getCGName(memberId);
		} else if (contractTypeId == 3) {
			memberName = "CSP";
		} else if (contractTypeId == 4) {
			memberName = portalVesselDetailRepository.findByVesselId(Integer.valueOf(memberId)).getVesselName();
		}

		return memberName;
	}


	/**
	 * To get All Annexures By Contract Id
	 * @author lrit-billing
	 * 
	 */

	@Override
	public List<ContractAnnexure> getAllContractsAnnexureById(Integer contractId) {
		logger.info("getAllContractsAnnexureById()");
		logger.info("contractId " + contractId);
		return contractRepository.getAllContractsAnnexureById(contractId);
	}

	/**
	 * To get Annexure By Annexure ID
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractAnnexure getContractAnnexureById(Integer annexureId) {
		// TODO Auto-generated method stub
		logger.info("getContractAnnexureById()");
		logger.info("annexureId " + annexureId);
		return contractRepository.getContractAnnexureById(annexureId);
	}


	/**
	 * To get All Contracting Govts.
	 * @author lrit-billing
	 * 
	 */

	@Override
	public List<LritContractingGovtMst> getAllContractingGovts() {
		// TODO Auto-generated method stub
		logger.info("getAllContractingGovts()");
		return lritContractingGovtMstRepository.getAllContractingGovts();	
	}

	/**
	 * To Update or extend Contract
	 * @author lrit-billing
	 * 
	 */

	@Override
	public Contract updateContract(Contract contract, List<String> contractingGov1, List<String> contractingGov2,
			MultipartFile contractAnnex, String remarks, String contractAnnexureDateString) throws IOException {

		logger.info("updateContract()");
		logger.info("contractingGov1 " + contractingGov1);
		logger.info("contractingGov2 " + contractingGov2);
		logger.info("contractAnnexureDateString " + contractAnnexureDateString);

		// TODO Auto-generated method stub

		// contract -> contract received from the form
		int contractId = contract.getContractId();
		logger.info("contractId " + contractId);
		// persistedContract -> contract persisted in database
		Contract persistedContract = getContract(contractId);

		java.sql.Date persistedContractValidTo = persistedContract.getValidTo();
		java.sql.Date formContractValidFrom = contract.getValidFrom();
		java.sql.Date formContractValidTo = contract.getValidTo();

		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		List<ContractAnnexure> contractAnnexures = new ArrayList<ContractAnnexure>();
		List<ContractAnnexure> persistedContractAnnexures = persistedContract.getBillingContractAnnexures();
		contractAnnexures.addAll(persistedContractAnnexures);
		/**
		 * Find out purpose of the edit 1. Extension of Contract 2. Editing of other
		 * fields
		 */


		if (formContractValidFrom.compareTo(persistedContractValidTo) > 0) {
			// Extension of contract
			logger.info("Extension of contract");
			for (int i = 0; i < contractingGov1.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				contractStakeholder.setMemberId(contractingGov1.get(i));
				contractStakeholder.setIsagency1(true);
				contractStakeholder.setValidFrom(formContractValidFrom);
				contractStakeholder.setValidTo(formContractValidTo);
				System.out.println("Contract Stakeholder member ID : " + contractStakeholder.getMemberId());
				contractStakeholders.add(contractStakeholder);
			}

			for (int i = 0; i < contractingGov2.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				contractStakeholder.setMemberId(contractingGov2.get(i));
				contractStakeholder.setIsagency1(false);
				contractStakeholder.setValidFrom(formContractValidFrom);
				contractStakeholder.setValidTo(formContractValidTo);
				contractStakeholders.add(contractStakeholder);
			}

			// System.out.println("Helloo"+contractAspDc.getContractNumber());
			contract.setContractDocument(persistedContract.getContractDocument());
			contract.setContractId(null);
		} else {
			logger.info("User wants to edit the contract");

			// User wants to edit the contract
			String contentType = contractAnnex.getContentType();
			String pdf = "application/pdf";
			boolean isAnnexValid = !contractAnnex.isEmpty() && contentType.equals(pdf);
			boolean isRemarksValid = remarks != null && !remarks.equals("");
			boolean isDateValid = contractAnnexureDateString != null && !contractAnnexureDateString.equals("");
			java.sql.Date contractAnnexureDate = null;
			// boolean isAnnexureUploaded = true;

			if (isAnnexValid && isRemarksValid && isDateValid) {
				byte[] contractAnnexureDoc = contractAnnex.getBytes();
				// System.out.println("contractAnnex "+contractAnnexureDoc);
				ContractAnnexure contractAnnexure = new ContractAnnexure();
				contractAnnexure.setAnnexureDocument(contractAnnexureDoc);
				contractAnnexure.setRemarks(remarks);
				contractAnnexureDate = java.sql.Date.valueOf(contractAnnexureDateString);
				contractAnnexure.setDate(contractAnnexureDate);
				contractAnnexure.setBillingContract(contract);

				contractAnnexures.add(contractAnnexure);

				System.out.println("contractAnnexures.size() " + contractAnnexures.size());

			}
			// Set contract annexures
			contract.setBillingContractAnnexures(contractAnnexures);
		}

		// contractAspDc.setContractId(null);
		contract.setContractStakeholders(contractStakeholders);
		// System.out.println("Contract "+contractAspDc);
		Contract savedContract = save(contract);
		return savedContract;

	}

	/**
	 * To Update or Extend CSP Contract
	 * @author lrit-billing
	 * 
	 */

	@Override
	public Contract updateContractCSP(ContractCSP contract, List<String> contractingGov1, MultipartFile contractAnnex,
			String remarks, String contractAnnexureDateString,List<String> agency1ContractGovValidFrom,
			List<String> agency1ContractGovValidTo) throws IOException {

		int contractId = contract.getContractId();
		logger.info("updateContractCSP()");
		logger.info("contractingGov1"+contractingGov1);
		Contract persistedContract = getContract(contractId);
		logger.info("ContractId"+contractId);
		java.sql.Date persistedContractValidTo = persistedContract.getValidTo();
		java.sql.Date formContractValidFrom = contract.getValidFrom();
		java.sql.Date formContractValidTo = contract.getValidTo();
		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		List<ContractAnnexure> contractAnnexures = new ArrayList<ContractAnnexure>();
		List<ContractAnnexure> persistedContractAnnexures = persistedContract.getBillingContractAnnexures();
		contractAnnexures.addAll(persistedContractAnnexures);
		logger.info("agency1ContractGovValidFrom"+agency1ContractGovValidFrom);
		logger.info("agency1ContractGovValidTo"+agency1ContractGovValidTo);
		
		
	
		logger.info("Contract Stakeholders"+contractStakeholders);
		/**
		 * Find out purpose of the edit 1. Extension of Contract 2. Editing of other
		 * fields
		 */
		contract.setContractDocument(persistedContract.getContractDocument());
		if (formContractValidFrom.compareTo(persistedContractValidTo) > 0) {
			// Extension of contract
			logger.info("Extension of Contract");
			for(int i = 0; i < contractingGov1.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				String memberId = contractingGov1.get(i);
				contractStakeholder.setMemberId(memberId);
				contractStakeholder.setValidFrom(formContractValidFrom);
				contractStakeholder.setValidTo(formContractValidTo);
				contractStakeholder.setIsagency1(true);

				/*
				 * ContractStakeholder persistedContractStakeholder =
				 * getContractStakeholder(contractId, memberId); if (
				 * persistedContractStakeholder != null) { Integer id =
				 * persistedContractStakeholder.getId(); contractStakeholder.setId(id); }
				 */ 
//=======
//				contractStakeholder.setValidFrom(formContractValidFrom);
//				contractStakeholder.setValidTo(formContractValidTo);
//
//>>>>>>> .r684
				contractStakeholders.add(contractStakeholder);
			}
			String agency2Code = contract.getAgency2().getAgencyCode();
			ContractStakeholder contractStakeholder = new ContractStakeholder();
			contractStakeholder.setMemberId(agency2Code);
			contractStakeholder.setIsagency1(false);
			contractStakeholder.setValidFrom(formContractValidFrom);
			contractStakeholder.setValidTo(formContractValidTo);
			contractStakeholders.add(contractStakeholder);
		
			logger.info("Contract Stakeholders"+contractStakeholders);
			
			contract.setContractId(null);
		} else {
			
			for(int i = 0; i < contractingGov1.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				String memberId = contractingGov1.get(i);
				contractStakeholder.setMemberId(memberId);
				contractStakeholder.setValidFrom(java.sql.Date.valueOf(agency1ContractGovValidFrom.get(i)));
				contractStakeholder.setValidTo(java.sql.Date.valueOf(agency1ContractGovValidTo.get(i)));
				contractStakeholder.setIsagency1(true);
				ContractStakeholder persistedContractStakeholder = getContractStakeholder(contractId, memberId);
				if ( persistedContractStakeholder != null) {
					Integer id = persistedContractStakeholder.getId();
					contractStakeholder.setId(id);
				} 
				contractStakeholders.add(contractStakeholder);
			}
			/*
			 * String agency2Code = contract.getAgency2().getAgencyCode();
			 * ContractStakeholder contractStakeholder1 = new ContractStakeholder();
			 * contractStakeholder1.setMemberId(agency2Code);
			 * contractStakeholder1.setIsagency1(false);
			 * contractStakeholder1.setValidFrom(formContractValidFrom);
			 * contractStakeholder1.setValidTo(formContractValidTo);
			 * contractStakeholders.add(contractStakeholder1);
			 */
			logger.info("Edit the contract");
			// User wants to edit the contract
			String contentType = contractAnnex.getContentType();
			String pdf = "application/pdf";
			boolean isAnnexValid = !contractAnnex.isEmpty() && contentType.equals(pdf);
			boolean isRemarksValid = remarks != null && !remarks.equals("");
			boolean isDateValid = contractAnnexureDateString != null && !contractAnnexureDateString.equals("");
			java.sql.Date contractAnnexureDate = null;
			// boolean isAnnexureUploaded = true;

			if (isAnnexValid && isRemarksValid && isDateValid) {
				byte[] contractAnnexureDoc = contractAnnex.getBytes();
				// System.out.println("contractAnnex "+contractAnnexureDoc);
				ContractAnnexure contractAnnexure = new ContractAnnexure();
				contractAnnexure.setAnnexureDocument(contractAnnexureDoc);
				contractAnnexure.setRemarks(remarks);
				contractAnnexureDate = java.sql.Date.valueOf(contractAnnexureDateString);
				contractAnnexure.setDate(contractAnnexureDate);
				contractAnnexure.setBillingContract(contract);
				contractAnnexures.add(contractAnnexure);
				System.out.println("contractAnnexures.size() " + contractAnnexures.size());
			}
			// Set contract annexures
			contract.setBillingContractAnnexures(contractAnnexures);
		}

		// contractAspDc.setContractId(null);
		contract.setContractStakeholders(contractStakeholders);
		// System.out.println("Contract "+contractAspDc);
		Contract savedContract = save(contract);
		return savedContract;
	}


	
	
	/**
	 * To Update or Extend Shipping Company Contract
	 * @author lrit-billing
	 * 
	 */

	@Override
	public Contract updateContractShippingCompany(ContractShippingCompany contract, List<String> contractingGov1,
			MultipartFile contractAnnex, String remarks, String contractAnnexureDateString,List<String> agency1ContractGovValidFrom,
			List<String> agency1ContractGovValidTo) throws IOException {
		logger.info("updateContractShippingCompany()");
		int contractId = contract.getContractId();
		logger.info("contractingGov1"+contractingGov1);
		Contract persistedContract = getContract(contractId);

		java.sql.Date persistedContractValidTo = persistedContract.getValidTo();
		java.sql.Date formContractValidFrom = contract.getValidFrom();
		java.sql.Date formContractValidTo = contract.getValidTo();

		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		List<ContractAnnexure> contractAnnexures = new ArrayList<ContractAnnexure>();
		List<ContractAnnexure> persistedContractAnnexures = persistedContract.getBillingContractAnnexures();
		contractAnnexures.addAll(persistedContractAnnexures);
		
		/**
		 * Find out purpose of the edit 1. Extension of Contract 2. Editing of other
		 * fields
		 */

		if (formContractValidFrom.compareTo(persistedContractValidTo) > 0) {
			// Extension of contract
			logger.info("Extension of Contract");
			for (int i = 0; i < contractingGov1.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				contractStakeholder.setMemberId(contractingGov1.get(i));
				contractStakeholder.setIsagency1(true);
				contractStakeholder.setValidFrom(formContractValidFrom);
				contractStakeholder.setValidTo(formContractValidTo);
				contractStakeholders.add(contractStakeholder);
			}
			String agency2Code = contract.getAgency2().getAgencyCode();
			List<Integer> shipppingCompanyVessels = contractShippingCompanyService.getShippingCompanyVessels(agency2Code);

			for (int i = 0; i < shipppingCompanyVessels.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				contractStakeholder.setMemberId(shipppingCompanyVessels.get(i).toString());
				contractStakeholder.setValidFrom(formContractValidFrom);
				contractStakeholder.setValidTo(formContractValidTo);
				contractStakeholder.setIsagency1(false);
				contractStakeholders.add(contractStakeholder);
			}
			contract.setContractDocument(persistedContract.getContractDocument());
			contract.setContractId(null);
		} else {
			logger.info("Edit the Contract");
			// User wants to edit the contract
			String contentType = contractAnnex.getContentType();
			String pdf = "application/pdf";
			boolean isAnnexValid = !contractAnnex.isEmpty() && contentType.equals(pdf);
			boolean isRemarksValid = remarks != null && !remarks.equals("");
			boolean isDateValid = contractAnnexureDateString != null && !contractAnnexureDateString.equals("");
			java.sql.Date contractAnnexureDate = null;
			// boolean isAnnexureUploaded = true;

			if (isAnnexValid && isRemarksValid && isDateValid) {
				byte[] contractAnnexureDoc = contractAnnex.getBytes();
				// System.out.println("contractAnnex "+contractAnnexureDoc);
				ContractAnnexure contractAnnexure = new ContractAnnexure();
				contractAnnexure.setAnnexureDocument(contractAnnexureDoc);
				contractAnnexure.setRemarks(remarks);
				contractAnnexureDate = java.sql.Date.valueOf(contractAnnexureDateString);
				contractAnnexure.setDate(contractAnnexureDate);
				contractAnnexure.setBillingContract(contract);
				contractAnnexures.add(contractAnnexure);
			}
			for(int i = 0; i < contractingGov1.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				String memberId = contractingGov1.get(i);
				contractStakeholder.setMemberId(memberId);
				contractStakeholder.setValidFrom(java.sql.Date.valueOf(agency1ContractGovValidFrom.get(i)));
				contractStakeholder.setValidTo(java.sql.Date.valueOf(agency1ContractGovValidTo.get(i)));
				contractStakeholder.setIsagency1(true);
				ContractStakeholder persistedContractStakeholder = getContractStakeholder(contractId, memberId);
				if ( persistedContractStakeholder != null) {
					Integer id = persistedContractStakeholder.getId();
					contractStakeholder.setId(id);
				} 
				contractStakeholders.add(contractStakeholder);
			}
			// Set contract annexures
			contract.setBillingContractAnnexures(contractAnnexures);
		}

		contract.setContractStakeholders(contractStakeholders);

		Contract savedContract = save(contract);
		return savedContract;
	}
	

	/**
	 * To Update Asp-Dc Contract 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public ContractAspDc updateContract(@Valid ContractAspDc contractAspDc, List<String> contractingGov1,
			List<String> contractingGov2, MultipartFile contractAnnex, String remarks,
			String contractAnnexureDateString,List<String> contractGovs,List<java.sql.Date> fromDates,List<java.sql.Date> toDates,List<String> agency1ContractGovValidFrom,
			List<String> agency1ContractGovValidTo,List<String> agency2ContractGovValidFrom,List<String> agency2ContractGovValidTo) throws IOException, ParseException {
		// TODO Auto-generated method stub
		logger.info("updateContract()");
		logger.info("contractAspDc-"+contractAspDc.getContractId());
		logger.info("contractingGov1-"+contractingGov1);
		logger.info("contractingGov2-"+contractingGov2);
		logger.info("date"+contractAnnexureDateString);
		int contractId = contractAspDc.getContractId();
		Contract persistedContract = getContract(contractId);

		java.sql.Date persistedContractValidTo = persistedContract.getValidTo();
		java.sql.Date formContractValidFrom = contractAspDc.getValidFrom();
		java.sql.Date formContractValidTo = contractAspDc.getValidTo();
		
		List<ContractStakeholder> additionalContractStakholders = new ArrayList<ContractStakeholder>();
		for(int i = 0 ; i< contractGovs.size(); i++) {
			ContractStakeholder contractStakholder = new ContractStakeholder();
			contractStakholder.setMemberId(contractGovs.get(i));
			if (formContractValidFrom.compareTo(persistedContractValidTo) > 0) {
				contractStakholder.setValidFrom(formContractValidFrom);
				contractStakholder.setValidTo(formContractValidTo);
			} else { 
				contractStakholder.setValidFrom(fromDates.get(i));
				contractStakholder.setValidTo(toDates.get(i));
			}
			contractStakholder.setIsagency1(false);
			additionalContractStakholders.add(contractStakholder);
		}
		logger.info("contractgov1"+contractingGov1.size());
		
		logger.info("agency1ContractGovValidFrom "+agency1ContractGovValidFrom.size());
		logger.info("agency1ContractGovValidTo "+agency1ContractGovValidTo.size());
		logger.info("contractgov2 "+contractingGov2.size());
		logger.info("agency2ContractGovValidFrom "+agency2ContractGovValidFrom.size());
		logger.info("agency2ContractGovValidTo "+agency2ContractGovValidTo.size());
		
		logger.info("additionalContractStakeholders"+additionalContractStakholders);
	
		List<ContractStakeholder> agency1ContractStakeholders = new ArrayList<ContractStakeholder>();
		for(int i = 0; i < contractingGov1.size(); i++) {
			ContractStakeholder contractStakeholder = new ContractStakeholder();
			String memberId = contractingGov1.get(i);
			contractStakeholder.setMemberId(memberId);
			if (formContractValidFrom.compareTo(persistedContractValidTo) > 0) {
				contractStakeholder.setValidFrom(formContractValidFrom);
				contractStakeholder.setValidTo(formContractValidTo);
			} else {
				contractStakeholder.setValidFrom(java.sql.Date.valueOf(agency1ContractGovValidFrom.get(i)));
				contractStakeholder.setValidTo(java.sql.Date.valueOf(agency1ContractGovValidTo.get(i)));
			
				ContractStakeholder persistedContractStakeholder = getContractStakeholder(contractId, memberId);
				if ( persistedContractStakeholder != null) {
					Integer id = persistedContractStakeholder.getId();
					contractStakeholder.setId(id);
				}
			}
			contractStakeholder.setIsagency1(true);
			agency1ContractStakeholders.add(contractStakeholder);
		}
		logger.info("Agency 1 Contract Stakeholders"+agency1ContractStakeholders);
		List<String> fromDateList2 = new ArrayList<String>(); 
		List<String> toDateList2 = new ArrayList<String>();
		
		logger.info(agency2ContractGovValidFrom.toString());
		for ( int i = 0; i < agency2ContractGovValidFrom.size(); i++ ) {
			String fromDate = agency2ContractGovValidFrom.get(i);
			//logger.info("fromDate " + fromDate + " " + i);
			if ( ! fromDate.equals("") ) {
				fromDateList2.add(fromDate); 
				
			}
		}
		logger.info(agency2ContractGovValidFrom.toString());
		logger.info(agency2ContractGovValidTo.toString());
		for ( int i = 0; i < agency2ContractGovValidTo.size(); i++ ) {
			String toDate = agency2ContractGovValidTo.get(i);
			
			if ( ! toDate.equals("") ) {
				toDateList2.add(toDate);
			}
		}
		logger.info(agency2ContractGovValidTo.toString());
		logger.info("From list size " + fromDateList2.size());
		logger.info("To list size " + toDateList2.size());
		logger.info("CG list size " + contractingGov2.size());
		
		List<ContractStakeholder> agency2ContractStakeholders = new ArrayList<ContractStakeholder>();
		for(int i = 0; i < contractingGov2.size(); i++) {
			ContractStakeholder contractStakeholder = new ContractStakeholder();
			String memberId = contractingGov2.get(i);
			contractStakeholder.setMemberId(memberId);
			if (formContractValidFrom.compareTo(persistedContractValidTo) > 0) {
				contractStakeholder.setValidFrom(formContractValidFrom);
				contractStakeholder.setValidTo(formContractValidTo);
			} else {
				contractStakeholder.setValidFrom(java.sql.Date.valueOf(fromDateList2.get(i)));
				contractStakeholder.setValidTo(java.sql.Date.valueOf(toDateList2.get(i)));
			
				ContractStakeholder persistedContractStakeholder = getContractStakeholder(contractId, memberId);
				if ( persistedContractStakeholder != null) {
					Integer id = persistedContractStakeholder.getId();
					contractStakeholder.setId(id);
				}
			}
			contractStakeholder.setIsagency1(false);
			agency2ContractStakeholders.add(contractStakeholder);
		}
		logger.info("Agency 2 Contract Stakeholders"+agency2ContractStakeholders);
		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		List<ContractAnnexure> contractAnnexures = new ArrayList<ContractAnnexure>();
		List<ContractAnnexure> persistedContractAnnexures = persistedContract.getBillingContractAnnexures();
		contractAnnexures.addAll(persistedContractAnnexures);
		ContractAspDc AspDccontract = (ContractAspDc) getContractByContractId(contractAspDc.getContractId());
		contractAspDc.setContractDocument(AspDccontract.getContractDocument());
		/**
		 * Find out purpose of the edit 1. Extension of Contract 2. Addition/Deletion of
		 * CG's a. Due to wrong input b. As part of amendment/annexure
		 */
		if (formContractValidFrom.compareTo(persistedContractValidTo) > 0) {
			// Extension of contract
			logger.info("in if");
			logger.info("update for wrong input");
			
			
			contractAspDc.setContractId(null);
		} else {
			logger.info("in else");
			logger.info("update for annexure");
			// User wants to edit the contract
			String contentType = contractAnnex.getContentType();
			logger.info("contentType"+contentType);
			String pdf = "application/pdf";
			boolean isAnnexValid = !contractAnnex.isEmpty() && contentType.equals(pdf);
			boolean isRemarksValid = remarks != null && !remarks.equals("");
			boolean isDateValid = contractAnnexureDateString != null && !contractAnnexureDateString.equals("");
			java.sql.Date contractAnnexureDate = null;
			boolean isAnnexureUploaded = true;

			if (isAnnexValid && isRemarksValid && isDateValid) {
				byte[] contractAnnexureDoc = contractAnnex.getBytes();
				ContractAnnexure contractAnnexure = new ContractAnnexure();
				contractAnnexure.setAnnexureDocument(contractAnnexureDoc);
				contractAnnexure.setRemarks(remarks);
				contractAnnexureDate = java.sql.Date.valueOf(contractAnnexureDateString);
				contractAnnexure.setDate(contractAnnexureDate);
				contractAnnexure.setBillingContract(contractAspDc);
				logger.info("contractAnnuxure"+contractAnnexure.getAnnexureDocument());
				contractAnnexures.add(contractAnnexure);
			}
			contractAspDc.setBillingContractAnnexures(contractAnnexures);
		}
		contractStakeholders.addAll(agency1ContractStakeholders);
		contractStakeholders.addAll(agency2ContractStakeholders);
		contractStakeholders.addAll(additionalContractStakholders);
		contractAspDc.setContractStakeholders(contractStakeholders);
		ContractAspDc savedContractAspDc = (ContractAspDc) save(contractAspDc);
		return savedContractAspDc;
	}
	
	@Override
	public ContractUsa updateContract(@Valid ContractUsa contractUsa, List<String> contractingGov1,
			List<String> contractingGov2, MultipartFile contractAnnex, String remarks,
			String contractAnnexureDateString,List<String> agency1ContractGovValidFrom,
			List<String> agency1ContractGovValidTo,List<String> agency2ContractGovValidFrom,List<String> agency2ContractGovValidTo) throws IOException, ParseException {
		// TODO Auto-generated method stub
		logger.info("updateContract()");
		logger.info("contractAspDc-"+contractUsa.getContractId());
		logger.info("contractingGov1-"+contractingGov1);
		logger.info("contractingGov2-"+contractingGov2);
		logger.info("date"+contractAnnexureDateString);
		int contractId = contractUsa.getContractId();
		Contract persistedContract = getContract(contractId);

		java.sql.Date persistedContractValidTo = persistedContract.getValidTo();
		java.sql.Date formContractValidFrom = contractUsa.getValidFrom();
		java.sql.Date formContractValidTo = contractUsa.getValidTo();

		
	
		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		List<ContractAnnexure> contractAnnexures = new ArrayList<ContractAnnexure>();
		List<ContractAnnexure> persistedContractAnnexures = persistedContract.getBillingContractAnnexures();
		contractAnnexures.addAll(persistedContractAnnexures);
		/**
		 * Find out purpose of the edit 1. Extension of Contract 2. Addition/Deletion of
		 * CG's a. Due to wrong input b. As part of amendment/annexure
		 */
		ContractUsa usaContract = (ContractUsa) getContractByContractId(contractUsa.getContractId());
		contractUsa.setContractDocument(usaContract.getContractDocument());
		if (formContractValidFrom.compareTo(persistedContractValidTo) > 0) {
			// Extension of contract
			logger.info("in if");
			logger.info("update for wrong input");
			for (int i = 0; i < contractingGov1.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				String memberId = contractingGov1.get(i);
				contractStakeholder.setMemberId(contractingGov1.get(i));
				contractStakeholder.setIsagency1(true);
				contractStakeholder.setValidFrom(contractUsa.getValidFrom());
				contractStakeholder.setValidTo(contractUsa.getValidTo());
				
				contractStakeholders.add(contractStakeholder);
			}
			for (int i = 0; i < contractingGov2.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				String memberId = contractingGov2.get(i);
				contractStakeholder.setMemberId(contractingGov2.get(i));
				contractStakeholder.setIsagency1(false);
				contractStakeholder.setValidFrom(contractUsa.getValidFrom());
				contractStakeholder.setValidTo(contractUsa.getValidTo());
				
				contractStakeholders.add(contractStakeholder);
			}
			
			contractUsa.setContractId(null);
		} else {
			logger.info("in else");
			List<ContractStakeholder> agency1ContractStakeholders = new ArrayList<ContractStakeholder>();
			for(int i = 0; i < contractingGov1.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				String memberId = contractingGov1.get(i);
				contractStakeholder.setMemberId(memberId);
				contractStakeholder.setValidFrom(java.sql.Date.valueOf(agency1ContractGovValidFrom.get(i)));
				contractStakeholder.setValidTo(java.sql.Date.valueOf(agency1ContractGovValidTo.get(i)));
				contractStakeholder.setIsagency1(true);
				ContractStakeholder persistedContractStakeholder = getContractStakeholder(contractId, memberId);
				if ( persistedContractStakeholder != null) {
					Integer id = persistedContractStakeholder.getId();
					contractStakeholder.setId(id);
				} 
				agency1ContractStakeholders.add(contractStakeholder);
			}
			logger.info("Agency 1 Contract Stakeholders"+agency1ContractStakeholders);

			List<ContractStakeholder> agency2ContractStakeholders = new ArrayList<ContractStakeholder>();
			for(int i = 0; i < contractingGov2.size(); i++) {
				ContractStakeholder contractStakeholder = new ContractStakeholder();
				String memberId = contractingGov2.get(i);
				contractStakeholder.setMemberId(memberId);
				contractStakeholder.setValidFrom(java.sql.Date.valueOf(agency2ContractGovValidFrom.get(i)));
				contractStakeholder.setValidTo(java.sql.Date.valueOf(agency2ContractGovValidTo.get(i)));
				contractStakeholder.setIsagency1(false);
				ContractStakeholder persistedContractStakeholder = getContractStakeholder(contractId, memberId);
				if ( persistedContractStakeholder != null) {
					Integer id = persistedContractStakeholder.getId();
					contractStakeholder.setId(id);
				} 
				agency2ContractStakeholders.add(contractStakeholder);
			}
			logger.info("Agency 2 Contract Stakeholders"+agency2ContractStakeholders);
			
			logger.info("update for annexure");
			// User wants to edit the contract
			String contentType = contractAnnex.getContentType();
			logger.info("contentType"+contentType);
			String pdf = "application/pdf";
			boolean isAnnexValid = !contractAnnex.isEmpty() && contentType.equals(pdf);
			boolean isRemarksValid = remarks != null && !remarks.equals("");
			boolean isDateValid = contractAnnexureDateString != null && !contractAnnexureDateString.equals("");
			java.sql.Date contractAnnexureDate = null;
			boolean isAnnexureUploaded = true;
			if (isAnnexValid && isRemarksValid && isDateValid) {
				byte[] contractAnnexureDoc = contractAnnex.getBytes();
				ContractAnnexure contractAnnexure = new ContractAnnexure();
				contractAnnexure.setAnnexureDocument(contractAnnexureDoc);
				contractAnnexure.setRemarks(remarks);
				contractAnnexureDate = java.sql.Date.valueOf(contractAnnexureDateString);
				contractAnnexure.setDate(contractAnnexureDate);
				contractAnnexure.setBillingContract(contractUsa);
				logger.info("contractAnnuxure"+contractAnnexure.getAnnexureDocument());
				contractAnnexures.add(contractAnnexure);
			}
			contractStakeholders.addAll(agency1ContractStakeholders);
			contractStakeholders.addAll(agency2ContractStakeholders);
			contractUsa.setBillingContractAnnexures(contractAnnexures);
		}
		
		contractUsa.setContractStakeholders(contractStakeholders);
		ContractUsa savedContractUsa= (ContractUsa) save(contractUsa);
		return savedContractUsa;
	}

	@Override
	public List<CurrencyType> getAllCurrencyTypes() {
		logger.info("getAllCurrencyTypes()");
		return contractRepository.getAllCurrencyTypes();
	}



}
