/**
 * @BillableItemService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 29-Aug-2019
 */
package in.gov.lrit.billing.service.payment.billableitem;

import java.util.List;

import in.gov.lrit.billing.model.payment.billableitem.BillableItem;

/**
 * @author lrit-billing
 *
 */
public interface BillableItemService {

	List<BillableItem> getBillableItemsOfService(Integer billingServiceId);

	BillableItem getBillableItem(Integer billableItemId);

}
