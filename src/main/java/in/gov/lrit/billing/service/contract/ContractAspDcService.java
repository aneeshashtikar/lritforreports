package in.gov.lrit.billing.service.contract;

import java.util.Date;
import java.util.List;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;

/**
 * Service layer interface to handle ContractAspDC entity related activities 
 * @author lrit_billing
 * 
 */
public interface ContractAspDcService {

	/**
	 * @param contractAspDc
	 */
	ContractAspDc save(ContractAspDc contractAspDc);
	
	

	/**
	 * @return
	 */
	List<ContractAspDc> getListAspDcContract();



	/**
	 * @param i
	 * @return
	 */
	ContractAspDc getContractDocument(Integer i);



	/**
	 * @param date
	 * @return List of LritNameDDpVerDto
	 * 
	 */
	List<LritNameDdpVerDto> getAspDcsByDate(Date date);
	
	
	
	
}
