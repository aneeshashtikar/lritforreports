/**
 * @MainServiceAbstractImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 30-Oct-2019
 */
package in.gov.lrit.billing.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.billing.dao.billingservice.ServiceRepository;
import in.gov.lrit.billing.dao.billingservice.ServiceStatusRepository;
import in.gov.lrit.billing.dao.billingservice.StatusRepository;
import in.gov.lrit.billing.dao.contract.ContractRepository;
import in.gov.lrit.billing.dao.contract.ContractStakeholderRepository;
import in.gov.lrit.billing.dao.payment.billableitem.BillableItemRepository;
import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryLogRepository;
import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryRepository;
import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.Status;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.payment.PaymentDetailsForService;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.ddp.dao.LritContractingGovtMstRepository;
import in.gov.lrit.portal.repository.vessel.ShippingCompanyRepository;
import in.gov.lrit.portal.repository.vessel.VesselRepository;
import in_.gov.lrit.ddp.dto.LritNameDto;
import net.sf.jasperreports.engine.JRException;

/**
 * @author lrit-billing
 *
 */
@org.springframework.stereotype.Service("serviceAbstractServiceImpl")
public class ServiceAbstractServiceImpl implements ServiceAbstractService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ServiceAbstractServiceImpl.class);

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private ContractStakeholderRepository contractStakeholderRepository;

	@Autowired
	protected ContractService contractService;

	@Autowired
	private BillableItemRepository billableItemRepo;

	@Autowired
	protected PaymentBillableItemCategoryRepository paymentBillableItemCategoryRepo;

	@Autowired
	protected PaymentBillableItemCategoryLogRepository paymentBillableItemCategoryLogRepo; 
	
	@Autowired
	private StatusRepository statusRepository;

	@Autowired
	private LritContractingGovtMstRepository lritContractingGovtMstRepository;

	@Autowired
	private ShippingCompanyRepository portalShippingCompanyRepository;

	@Autowired
	private VesselRepository portalVesselDetailRepository;

	@Autowired
	private ContractRepository contractRepository;

	@Autowired
	private ServiceStatusRepository serviceStatusRepository;

	/**
	 * FETCH THE SERVICE(INVOICE/BILL)
	 *
	 * @author lrit-billing
	 * @param billingServiceId
	 * @return
	 */
	public Service getBillingService(Integer billingServiceId) {
		logger.info("INSIDE getBillingService");
		logger.info("billingServiceId : " + billingServiceId);

		Service service = null;
		if (serviceRepository.findById(billingServiceId).isPresent()) {
			service = serviceRepository.findById(billingServiceId).get();
			Integer contractTypeId = service.getBillingContract().getBillingContractType().getContractTypeId();
			String agencyCode = service.getBillingContract().getAgency2().getAgencyCode(); 
			String agencyName = contractService.getAgencyName(contractTypeId, agencyCode);
			service.getBillingContract()
					.setAgency2Name(agencyName);
		}
		return service;
	}

	/**
	 * SET SERVICE STATUS AND RETURN LIST OF SERVICE STATUSES
	 *
	 * @author lrit-billing
	 * @param service
	 * @param statusId
	 * @param remarks
	 * @return
	 */
	public List<ServiceStatus> getServiceStatuses(Service service, Integer statusId, String remarks) {
		logger.info("INSIDE getServiceStatuses");
		logger.info("service : " + service);
		logger.info("statusId : " + statusId);
		logger.info("remarks : " + remarks);

		Status billingStatus = statusRepository.findById(statusId).get();
		ServiceStatus serviceStatus = new ServiceStatus();

		serviceStatus.setRemarks(remarks);
		serviceStatus.setBillingService(service);
		serviceStatus.setBillingStatus(billingStatus);

		List<ServiceStatus> billingServiceStatuses = new ArrayList<ServiceStatus>();
		if (service.getBillingServiceStatuses() != null)
			billingServiceStatuses = service.getBillingServiceStatuses();

		billingServiceStatuses.add(serviceStatus);

		return billingServiceStatuses;
	}

	/**
	 * GET CONTRACTING GOVERNMENT NAME
	 *
	 * @author lrit-billing
	 * @param contractId
	 * @return
	 */
	public List<String> getProducers(Integer contractId) {
		return contractStakeholderRepository.getProviders(contractId);
	}

	/**
	 * GET CONTRACTING GOVERNMENT NAME OF CGS,VESSEL
	 *
	 * @author lrit-billing
	 * @param contract
	 * @return
	 */
	public List<String> getConsumers(Contract contract) {
//		List<String> consumers = new ArrayList<>();
//		if (contract.getBillingContractType().getContractTypeId() == 4) {
//			for (Integer consumer : portalShippingcompanyVesselRelationRepository
//					.findVesselsByCompanyCode(contract.getAgency2().getAgencyCode())) {
//				consumers.add(consumer.toString());
//			}
//		} else {
//			consumers = contractStakeholderRepository.getConsumers(contract.getContractId());
//		}
//		return consumers;

		return contractStakeholderRepository.getConsumers(contract.getContractId());
	}

	/**
	 * RETURN BILLABLE ITEM
	 *
	 * @author lrit-billing
	 * @param provider
	 * @param consumer
	 * @param paymentBillableItemCategory
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Override
	public BillableItem getBillableItem(String provider, String consumer,
			PaymentBillableItemCategory paymentBillableItemCategory, Date fromDate, Date toDate) {
		Random r = new Random();
		BillableItem billableItem = new BillableItem();
		int max = 100;
		int min = 0;
		Integer contractTypeId = paymentBillableItemCategory.getBillingContractType().getContractTypeId();
		billableItem.setSystemCount(r.nextInt((max - min) + 1) + min);
		billableItem.setManualCount(billableItem.getSystemCount());
		billableItem.setCost(billableItem.getManualCount() * paymentBillableItemCategory.getRate());
		billableItem.setProvider(provider);
		billableItem.setConsumer(consumer);
		String consumerName = contractService.getStakeholderName(consumer, contractTypeId);
		billableItem.setConsumerName(consumerName);
		String providerName = contractService.getStakeholderName(provider, contractTypeId);
		billableItem.setProviderName(providerName);
		billableItem.setBillingPaymentBillableItemCategory(paymentBillableItemCategory);

		return billableItem;
	}

	/**
	 * RETURN BILLABLE ITEMS
	 *
	 * @author lrit-billing
	 * @param contractId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Override
	public List<BillableItem> getBillableItems(Integer contractId, Date fromDate, Date toDate) {

		List<BillableItem> billableItems = new ArrayList<BillableItem>();

		Contract contract = contractRepository.findById(contractId).get();

		List<String> providers = getProducers(contractId);
		List<String> consumers = getConsumers(contract);
		ContractType contractType = contract.getBillingContractType();

		List<PaymentBillableItemCategory> paymentBillableItemCategorys = paymentBillableItemCategoryLogRepo
				.findPbicByBillingContractTypeAndValidFromAndValidTill(contractType, fromDate, toDate);

		for (String provider : providers) {
			for (String consumer : consumers) {
				for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategorys) {
					BillableItem billableItem = getBillableItem(provider, consumer, paymentBillableItemCategory,
							fromDate, toDate);
					if (billableItem.getSystemCount() > 0) {
						billableItems.add(billableItem);
					}
				}
			}
		}
		return billableItems;
	}

	/**
	 * PERSIST THE SEND SERVICE(INVOICE/BILL)
	 *
	 * @author lrit-billing
	 * @param service
	 * @return
	 */
	@Override
	public Service saveService(Service service) {

		logger.info("INSIDE saveService");
		logger.info("service : " + service);

		Integer statusId = 1;
		String remarks = "";

//		List<BillableItem> billingBillableItems = (service.getBillingBillableItems() == null) ? getBillableItems(
//				service.getBillingContract().getContractId(), service.getFromDate(), service.getToDate())
//				: service.getBillingBillableItems();

		List<BillableItem> billingBillableItems = service.getBillingBillableItems();

		// TO SET EVERY BILLABLEITEM INVOICE AS REFERENCE
		for (int i = 0; i < billingBillableItems.size(); i++) {
			billingBillableItems.get(i).setBillingService(service);
			billingBillableItems.get(i).setDateRangeFrom(service.getFromDate());
			billingBillableItems.get(i).setDateRangeTo(service.getToDate());
		}

		service.setBillingBillableItems(billingBillableItems);
		service.setBillingServiceStatuses(getServiceStatuses(service, statusId, remarks));

//		logger.info("service.getBillingBillableItems().size() " + service.getBillingBillableItems().size());
//		logger.info("service.getBillingBillableItems() " + service.getBillingBillableItems());

		service = serviceRepository.save(service);

		return service;
	}

	/**
	 * SET CONSUMER AND PROVIDER NAME OF SERVIE(INVOICE/BILL)
	 *
	 * @author lrit-billing
	 * @param service
	 * @return
	 */
	@Override
	public Service setProviderConsumerName(Service service) {

		logger.info("INSIDE setProviderConsumerName()");
		logger.info("service : " + service);

		int contractTypeId = service.getBillingContract().getBillingContractType().getContractTypeId();
		int contractId = service.getBillingContract().getContractId();
		service.getBillingBillableItems().forEach((billableItem) -> {

			String stakeHolder = billableItem.getProvider();
			String stakeHolderName = contractService.getStakeholderName(stakeHolder, contractTypeId);
			billableItem.setProviderName(stakeHolderName);

			stakeHolder = billableItem.getConsumer();
			stakeHolderName = contractService.getStakeholderName(stakeHolder, contractTypeId);
			billableItem.setConsumerName(stakeHolderName);

		});
		return service;
	}

	/**
	 * AMOUNT PAID MEMBER CGS AND VESSELS
	 *
	 * @author lrit-billing
	 * @param service
	 * @return
	 */
	@Override
	public Service setPaymentMembersName(Service service) {
		logger.info("INSIDE setPaymentMembersName()");

		int contractType = service.getBillingContract().getBillingContractType().getContractTypeId();
		logger.info("service : " + service);
		service.getBillingPaymentDetailsForServices().forEach((paymentService) -> {
			paymentService.getBillingPaymentDetailsPerMembers().forEach(paymentMember -> {
				paymentMember
						.setConsumerName(contractService.getStakeholderName(paymentMember.getConsumer(), contractType));
			});
		});

		return service;
	}

	/**
	 * ATTACH SERVICE STATUS TO SERVICE(INVOICE/BILL)
	 *
	 * @author lrit-billing
	 * @param service
	 * @param statusId
	 * @param remarks
	 * @return
	 */
	@Override
	public Service setServiceStatus(Service service, Integer statusId, String remarks) {

		logger.info("INSIDE setServiceStatus ");
		logger.info("service " + service);
		logger.info("statusId : " + statusId);
		logger.info("remarks : " + remarks);

		Status billingStatus = statusRepository.findById(statusId).get();
		logger.info("fetched status : " + billingStatus);

		ServiceStatus serviceStatus = new ServiceStatus();

		serviceStatus.setRemarks(remarks);
		serviceStatus.setBillingService(service);
		serviceStatus.setBillingStatus(billingStatus);

		List<ServiceStatus> billingServiceStatuses = (service.getBillingServiceStatuses() != null)
				? service.getBillingServiceStatuses()
				: new ArrayList<ServiceStatus>();

		billingServiceStatuses.add(serviceStatus);

		service.setBillingServiceStatuses(billingServiceStatuses);

		logger.info("EXIT setServiceStatus");
		return service;
	}

	/**
	 * ALL AGENCY DTOS
	 *
	 * @author lrit-billing
	 * @return
	 */
	@Override
	public List<LritNameDto> getAllAgency() {
		List<LritNameDto> lritNameDtos = contractService.getLritNameDto();
		lritNameDtos.addAll(portalShippingCompanyRepository.findAllShippingCompanyDto());
		return lritNameDtos;
	}

	/**
	 * RETURNS AGENCY NAME BASED ON AGENCYCODE
	 *
	 * @author lrit-billing
	 * @param agencyCode
	 * @return
	 */
	@Override
	public String getAgencyName(String agencyCode) {
		String agencyName = "";
		for (LritNameDto agency : getAllAgency()) {
			if (agency.getLritid().equals(agencyCode)) {
				agencyName = agency.getName();
				break;
			}
		}
		return agencyName;
	}

	/**
	 * RETURNS HASHED BILLABLE ITEMS PROVIDER->CONSUMER->BILLABLEITEM
	 *
	 * @author lrit-billing
	 * @param billingServiceId
	 * @return
	 */
	@Override
	public HashMap<String, HashMap<String, List<BillableItem>>> getHashedBillableItems(Integer billingServiceId) {
		Service service = serviceRepository.getOne(billingServiceId);
		HashMap<String, HashMap<String, List<BillableItem>>> provider = new HashMap<String, HashMap<String, List<BillableItem>>>();

		if (service.getBillingBillableItems() != null) {
			service.getBillingBillableItems().forEach((billableItem) -> {
				List<BillableItem> billableItems = null;
				HashMap<String, List<BillableItem>> consumer = null;

				if (provider.get(billableItem.getProvider()) != null) {
					consumer = provider.get(billableItem.getProvider());
					if (consumer.get(billableItem.getConsumer()) == null) {
						billableItems = new ArrayList<BillableItem>();
					} else
						billableItems = consumer.get(billableItem.getConsumer());

					billableItems.add(billableItem);
					consumer.put(billableItem.getConsumer(), billableItems);
					provider.put(billableItem.getProvider(), consumer);

				} else {
					consumer = new HashMap<String, List<BillableItem>>();
					billableItems = new ArrayList<BillableItem>();

					billableItems.add(billableItem);
					consumer.put(billableItem.getConsumer(), billableItems);
					provider.put(billableItem.getProvider(), consumer);
				}
			});
		}

		return provider;

	}

	/**
	 * UPDATES BILLABLE ITEM OF SERVICE
	 *
	 * @author lrit-billing
	 * @param mainService
	 * @param dataCareerService
	 * @return
	 */
	public Service setBillableItems(Service mainService, Service dataCareerService) {

		List<BillableItem> billableItemsToUpdate = mainService.getBillingBillableItems();

		for (int index = 0; index < billableItemsToUpdate.size(); index++) {
			BillableItem billableItem = billableItemsToUpdate.get(index);

			for (BillableItem billableItemInvoice : dataCareerService.getBillingBillableItems()) {
				if (billableItem.getBillableItemId().equals(billableItemInvoice.getBillableItemId())) {
					billableItem.setManualCount(billableItemInvoice.getManualCount());
					billableItem.setCost(billableItemInvoice.getCost());
					break;
				}
			}
			billableItemsToUpdate.set(index, billableItem);
		}

		mainService.setBillingBillableItems(billableItemsToUpdate);
		mainService.setGrandTotal(dataCareerService.getGrandTotal());
		mainService.setSubTotal(dataCareerService.getSubTotal());

		return mainService;
	}

	/**
	 * NAME OF PAYMENT MEMBER
	 *
	 * @author lrit-billing
	 * @param service
	 * @return
	 */
	@Override
	public Service setPaymentConsumerName(Service service) {

		int contractTypeId = service.getBillingContract().getBillingContractType().getContractTypeId();

		service.getBillingPaymentDetailsForServices().forEach((paymentService) -> {
			paymentService.getBillingPaymentDetailsPerMembers().forEach((paymentMember) -> {

				String stakeHolder = paymentMember.getConsumer();
				String stakeHolderName = contractService.getStakeholderName(stakeHolder, contractTypeId);
				paymentMember.setConsumerName(stakeHolderName);

			});
		});
		return service;
	}

	/**
	 * UPDATE MESSAGE COUNT
	 *
	 * @author lrit-billing
	 * @param service
	 * @param billingServiceId
	 * @return
	 * @throws SQLException
	 * @throws JRException
	 * @throws IOException
	 */
	@Override
	public Service updateMessageCount(Service service, Integer billingServiceId)
			throws SQLException, JRException, IOException {

		logger.info("INSIDE updateMessageCount");
		logger.info("service : " + service);
		logger.info("billingServiceId : " + billingServiceId);

		// regenerated due to update count
		Integer statusId = 2;
		Service serviceToPersist = null;

		// service.setBillingServiceId(billingServiceId);

//		if (serviceRepository.findById(billingServiceId).isPresent())
//			serviceToPersist = serviceRepository.findById(billingServiceId).get();

		serviceToPersist = getBillingService(billingServiceId);

		serviceToPersist = setServiceStatus(serviceToPersist, statusId,
				service.getBillingServiceStatuses().get(0).getRemarks());

		serviceToPersist = setBillableItems(serviceToPersist, service);

		serviceToPersist = serviceRepository.save(serviceToPersist);
		return serviceToPersist;
	}

	@Override
	public String getCGName(String consumer) {
		return lritContractingGovtMstRepository.getCGName(consumer);
	}

	@Override
	public String getNameContractTypeBased(String id, Integer contractType) {
		if (contractType == 4)
			return portalVesselDetailRepository.findByVesselId(Integer.valueOf(id)).getVesselName();
		else
			return lritContractingGovtMstRepository.getCGName(id);
	}

	@Override
	public HashMap<String, PaymentDetailsPerMember> getUncheckedMembers(Service service) {

		HashMap<String, PaymentDetailsPerMember> unCheckedMembers = new HashMap<>();

		for (BillableItem billableItem : service.getBillingBillableItems()) {
			PaymentDetailsPerMember paymentDetailsPerMember = null;
			if (unCheckedMembers.get(billableItem.getConsumer()) == null) {

				paymentDetailsPerMember = new PaymentDetailsPerMember();
				paymentDetailsPerMember.setConsumer(billableItem.getConsumer());
				paymentDetailsPerMember.setConsumerName(getNameContractTypeBased(billableItem.getConsumer(),
						service.getBillingContract().getBillingContractType().getContractTypeId()));
				paymentDetailsPerMember.setCost(0.0);

				unCheckedMembers.put(billableItem.getConsumer(), paymentDetailsPerMember);
			}

			paymentDetailsPerMember = unCheckedMembers.get(billableItem.getConsumer());
			paymentDetailsPerMember.setCost(paymentDetailsPerMember.getCost() + billableItem.getCost());

			unCheckedMembers.put(billableItem.getConsumer(), paymentDetailsPerMember);
		}

		for (PaymentDetailsForService paymentDetailsForService : service.getBillingPaymentDetailsForServices()) {
			if (paymentDetailsForService.getBillingPaymentDetailsPerMembers() != null) {
				for (PaymentDetailsPerMember paymentDetailsPerMember : paymentDetailsForService
						.getBillingPaymentDetailsPerMembers()) {
					unCheckedMembers.remove(paymentDetailsPerMember.getConsumer());
				}
			}
		}

		return unCheckedMembers;
	}

	@Override
	public Double getRemainingAmountService(Service service) {
		logger.info("INSIDE getRemainingAmountService()");
		logger.info("service : " + service);

		Double remainingCost = 0.0;
		for (PaymentDetailsForService paymentDetailsForService : service.getBillingPaymentDetailsForServices())
			remainingCost += paymentDetailsForService.getPaidAmount();

		Double getTotalRemainedAmount = service.getGrandTotal() - remainingCost;
		logger.info("Total Remained Amount : " + getTotalRemainedAmount);

		return getTotalRemainedAmount;
	}

	@Transactional(value = "portalTransactionManager")
	@Override
	public Service updatePayment(Service service, List<String> consumers, Integer billingServiceId, byte[] paymentDocument, String paymentDate) {
		// regenerated due to update count

		logger.info("INSIDE updatePayment()");
		logger.info("Service " + service);
		logger.info("consumers : " + consumers);
		logger.info("billingServiceId : " + billingServiceId);

		Service paymentService = serviceRepository.findById(billingServiceId).get();

		PaymentDetailsForService paymentDetailsForService = service.getBillingPaymentDetailsForServices().get(0);

		if (consumers != null && !consumers.isEmpty()) {
			logger.info("If consumers is null");
			List<PaymentDetailsPerMember> selectedMembers = new ArrayList<PaymentDetailsPerMember>();

//			PaymentDetailsForService paymentDetailsForService = service.getBillingPaymentDetailsForServices().get(0);

			for (String consumer : consumers) {
				for (PaymentDetailsPerMember paymentDetailsPerMember : paymentDetailsForService
						.getBillingPaymentDetailsPerMembers()) {

					if (consumer.equals(paymentDetailsPerMember.getConsumer())) {
						paymentDetailsPerMember.setBillingPaymentDetailsForService(paymentDetailsForService);
						selectedMembers.add(paymentDetailsPerMember);
					}
				}
			}
			paymentDetailsForService.setBillingPaymentDetailsPerMembers(selectedMembers);
//			paymentDetailsForService.setBillingService(paymentService);
//
//			List<PaymentDetailsForService> paymentDetailsForServices = (null == paymentService
//					.getBillingPaymentDetailsForServices()) ? new ArrayList<PaymentDetailsForService>()
//							: paymentService.getBillingPaymentDetailsForServices();
//
//			paymentDetailsForServices.add(paymentDetailsForService);
//			paymentService.setBillingPaymentDetailsForServices(paymentDetailsForServices);

		}

		paymentDetailsForService.setBillingService(paymentService);
		paymentDetailsForService.setPaymentAdviceDocument(paymentDocument);
		
		
		paymentDetailsForService.setPaymentDate(java.sql.Date.valueOf(paymentDate));
		List<PaymentDetailsForService> paymentDetailsForServices = (null == paymentService
				.getBillingPaymentDetailsForServices()) ? new ArrayList<PaymentDetailsForService>()
						: paymentService.getBillingPaymentDetailsForServices();

		paymentDetailsForServices.add(paymentDetailsForService);
		paymentService.setBillingPaymentDetailsForServices(paymentDetailsForServices);

		// paymentService = serviceRepository.save(paymentService);

		double remainingAmount = getRemainingAmountService(paymentService);
		Integer statusId = null;

		if (remainingAmount == 0) {
			logger.info("statusID set to fully paid");
			statusId = 6;
		} else {
			logger.info("statusID set to partially paid");
			statusId = 7;
		}

		String remarks = service.getBillingServiceStatuses().get(0).getRemarks();

		paymentService = setServiceStatus(paymentService, statusId, remarks);
		
		paymentService = serviceRepository.save(paymentService);

		logger.info("EXIT updatePayment");
		return paymentService;
	}

	/**
	 * UPDATE PAYMENT OF SERVICE
	 *
	 * @author lrit-billing
	 * @param paymentService
	 * @param service
	 * @param consumers
	 * @return
	 */
	@Override
	public Service updatePayment(Service paymentService, Service service, List<String> consumers) {

		if (consumers != null && !consumers.isEmpty()) {
			List<PaymentDetailsPerMember> selectedMembers = new ArrayList<PaymentDetailsPerMember>();

			PaymentDetailsForService paymentDetailsForService = service.getBillingPaymentDetailsForServices().get(0);

			for (String consumer : consumers) {
				for (PaymentDetailsPerMember paymentDetailsPerMember : paymentDetailsForService
						.getBillingPaymentDetailsPerMembers()) {

					if (consumer.equals(paymentDetailsPerMember.getConsumer())) {
						paymentDetailsPerMember.setBillingPaymentDetailsForService(paymentDetailsForService);
						selectedMembers.add(paymentDetailsPerMember);
					}
				}
			}
			paymentDetailsForService.setBillingPaymentDetailsPerMembers(selectedMembers);
			paymentDetailsForService.setBillingService(paymentService);

			List<PaymentDetailsForService> paymentDetailsForServices = (null == paymentService
					.getBillingPaymentDetailsForServices()) ? new ArrayList<PaymentDetailsForService>()
							: paymentService.getBillingPaymentDetailsForServices();

			paymentDetailsForServices.add(paymentDetailsForService);
			paymentService.setBillingPaymentDetailsForServices(paymentDetailsForServices);

		}

		return paymentService;
	}

	/**
	 * HASHED BILLABLE ITEM PROVIDER->CONSUMER->BILLABLEITEM
	 *
	 * @author lrit-billing
	 * @param contractId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Override
	public HashMap<String, HashMap<String, List<BillableItem>>> getHashedBillableItems(Integer contractId,
			Date fromDate, Date toDate) {

		Contract contract = contractService.getContract(contractId);
		HashMap<String, HashMap<String, List<BillableItem>>> billableItems = new HashMap<String, HashMap<String, List<BillableItem>>>();

		List<String> providers = getProducers(contractId);
		List<String> consumers = getConsumers(contract);

		ContractType contractType = contract.getBillingContractType();

		List<PaymentBillableItemCategory> paymentBillableItemCategorys = paymentBillableItemCategoryLogRepo
				.findPbicByBillingContractTypeAndValidFromAndValidTill(contractType, fromDate, toDate);

		for (String provider : providers) {
			HashMap<String, List<BillableItem>> consumerHash = new HashMap<String, List<BillableItem>>();

			for (String consumer : consumers) {
				List<BillableItem> messages = new ArrayList<BillableItem>();

				for (PaymentBillableItemCategory paymentBillableItemCategory : paymentBillableItemCategorys) {
					BillableItem billableItem = getBillableItem(provider, consumer, paymentBillableItemCategory,
							fromDate, toDate);
					if (billableItem.getSystemCount() > 0) {
						messages.add(billableItem);
					}
				}
				consumer = consumer.concat(" - "
						+ getNameContractTypeBased(consumer, contract.getBillingContractType().getContractTypeId()));
				consumerHash.put(consumer, messages);
			}
			billableItems.put(provider.concat(" - " + getCGName(provider)), consumerHash);
		}
		return billableItems;
	}

	/**
	 * CONTRACTING GOVERNMENT BILLABLE ITEM
	 *
	 * @author lrit-billing
	 * @param billingServiceId
	 * @return
	 */
	@Override
	public List<BillableItem> getCGBillableItemForService(Integer billingServiceId) {
		Service service = serviceRepository.getOne(billingServiceId);

		List<BillableItem> billableItems = new ArrayList<>();

		if (service.getBillingBillableItems() != null) {
			BillableItem billableItem = null;
			for (int index = 0; index < service.getBillingBillableItems().size(); index++) {
				billableItem = service.getBillingBillableItems().get(index);
				billableItem
						.setProviderName(billableItem.getProvider() + " - " + getCGName(billableItem.getProvider()));
				billableItem.setConsumerName(
						billableItem.getConsumer() + " - " + getNameContractTypeBased(billableItem.getConsumer(),
								service.getBillingContract().getBillingContractType().getContractTypeId()));
				billableItems.add(index, billableItem);
			}
		}

		return billableItems;
	}

	/**
	 * RETURN SYSTEM COUNT BASED ON PARAMETERS
	 *
	 * @author lrit-billing
	 * @param provider
	 * @param consumer
	 * @param fromDate
	 * @param toDate
	 * @param bic
	 * @param contractTypeId
	 * @return
	 */
	@Override
	public Integer getBillableItemSystemCount(String provider, String consumer, Date fromDate, Date toDate, Integer bic,
			Integer contractTypeId) {
		// TODO Auto-generated method stub

		int systemCount = 0;
		Random r = new Random();
		int min = 0;
		int max = 100;

		logger.info("Provider " + provider + " Consumer " + consumer + " fromDate " + fromDate + " toDate " + toDate
				+ " bic " + bic);
		if (contractTypeId == 1 || contractTypeId == 2) {
			systemCount = billableItemRepo.getSystemCountForAspDc(provider, consumer, fromDate, toDate, bic);
		} else if (contractTypeId == 3) {
			systemCount = billableItemRepo.getSystemCountForCSP(provider, consumer, fromDate, toDate, bic);
		} else if (contractTypeId == 4) {
			systemCount = r.nextInt((max - min) + 1) + min;
		}
		return systemCount;
	}

	/**
	 * RETURNS ONLY VALID PAYMENT BILLABLE ITEM CATEGORY
	 *
	 * @author lrit-billing
	 * @param contractId
	 * @return
	 */
	@Override
	public List<PaymentBillableItemCategory> getPaymentBillableItemCategoryList(Integer contractId) {
		// TODO Auto-generated method stub
		Contract contract = contractService.getContract(contractId);
		ContractType contractType = contract.getBillingContractType();
		Date validFrom = contract.getValidFrom();
		Date validTo = contract.getValidTo();
		int contractTypeId = contractType.getContractTypeId();

		List<PaymentBillableItemCategory> paymentBillableItemCategoryList = null;
		/*
		 * if ( contractTypeId == 1 || contractTypeId == 2 ) {
		 * contractType.setContractTypeId(1); }
		 */
		paymentBillableItemCategoryList = paymentBillableItemCategoryLogRepo
				.findPbicByBillingContractTypeAndValidFromAndValidTill(contractType, validFrom, validTo);

		return paymentBillableItemCategoryList;
	}

	/**
	 * 
	 *
	 * @author lrit-billing
	 * @param date1
	 * @param date2
	 * @param date3
	 * @return
	 */
	public java.sql.Date highestDate(java.sql.Date date1, java.sql.Date date2, java.sql.Date date3) {

		logger.info("highestDate date1 " + date1 + " date2 " + date2 + " date3 " + date3);
		if (date1.after(date2) || date1.equals(date2)) {
			if (date1.after(date3) || date1.equals(date3)) {
				return date1;
			} else
				return date3;
		} else if (date2.after(date1) || date2.equals(date1)) {
			if (date2.after(date3) || date2.equals(date3)) {
				return date2;
			} else
				return date3;
		}
		return null;
	}

	/**
	 * 
	 *
	 * @author lrit-billing
	 * @param date1
	 * @param date2
	 * @param date3
	 * @return
	 */
	public java.sql.Date lowestDate(java.sql.Date date1, java.sql.Date date2, java.sql.Date date3) {
		logger.info("lowestDate date1 " + date1 + " date2 " + date2 + " date3 " + date3);
		if (date1.before(date2) || date1.equals(date2)) {
			if (date1.before(date3) || date1.equals(date3)) {
				return date1;
			} else
				return date3;
		} else if (date2.before(date1) || date2.equals(date1)) {
			if (date2.before(date3) || date2.equals(date3)) {
				return date2;
			} else
				return date3;
		}
		return null;
	}

	/**
	 * RETURNS LATEST STATUS OF SERVICE
	 *
	 * @author lrit-billing
	 * @param serviceId
	 * @return
	 */
	@Override
	public Status getLatestStatus(Integer serviceId) {
		logger.info("INSIDE getLatestStatus");
		ServiceStatus serviceStatus = serviceStatusRepository.getLatestStatus(serviceId);
		return serviceStatus.getBillingStatus();
	}

}
