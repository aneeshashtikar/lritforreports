/**
 * @BillableItemService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 21-Aug-2019
 */
package in.gov.lrit.billing.service.payment.billableitem;

import java.util.Collection;
import java.util.List;

import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategoryLog;

/**
 * @author lrit-billing
 *
 */
public interface PaymentBillableItemCategoryService {

	public PaymentBillableItemCategory save(PaymentBillableItemCategory paymentBillableItemCategory);

	public PaymentBillableItemCategory update(PaymentBillableItemCategory paymentBillableItemCategory);

	public Collection<PaymentBillableItemCategory> findByBillingContractType(Integer contractTypeId);

	public PaymentBillableItemCategory getBillableItemCategory(int billableItemCategoryId);

	PaymentBillableItemCategoryLog getLatestBillableItemCategoryLog(int billableItemCategoryId);

	List<PaymentBillableItemCategoryLog> getPaymentBillableItemCategoryLogs(int billableItemCategoryId);

	PaymentBillableItemCategoryLog getPaymentBillableItemCategoryLog(int billableItemCategoryLogId);

	PaymentBillableItemCategoryLog saveBillableCategoryLog(
			PaymentBillableItemCategoryLog paymentBillableItemCategoryLog);
}
