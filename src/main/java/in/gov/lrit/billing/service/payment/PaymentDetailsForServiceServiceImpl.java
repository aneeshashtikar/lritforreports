package in.gov.lrit.billing.service.payment;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.billing.dao.payment.PaymentDetailsForServiceRepository;
import in.gov.lrit.billing.model.payment.PaymentDetailsForService;

/**
 * @author lrit-billing
 *
 */

@Service
public class PaymentDetailsForServiceServiceImpl implements PaymentDetailsForServiceService {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
			.getLogger(PaymentDetailsForServiceServiceImpl.class);

	@Autowired
	private PaymentDetailsForServiceRepository paymentDetailsForServiceRepo;

	@Override
	public Optional<PaymentDetailsForService> findById(Integer pmtDetServiceId) {

		return paymentDetailsForServiceRepo.findById(pmtDetServiceId);
	}

	/**
	 * To get Payment Details of bill
	 * 
	 * @author lrit-billing
	 *
	 */
	@Override
	public PaymentDetailsForService getPaymentDetailsByPaymentServiceId(Integer pmtDetServiceId) {
		logger.info("getPaymentDetailsByPaymentServiceId()");
		logger.info("pmtDetServiceID" + pmtDetServiceId);
		return paymentDetailsForServiceRepo.getPaymentDetailsByPaymentServiceId(pmtDetServiceId);
	}

}
