/**
 * @ServiceService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 26-Aug-2019
 */
package in.gov.lrit.billing.service;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * @author lrit-billing
 *
 */
public interface ServiceService {

	BillableItem getBillableItem(String provider, String consumer,
			PaymentBillableItemCategory paymentBillableItemCategory, Date fromDate, Date toDate);

	List<String> getProducers(Integer contractId);

	List<String> getConsumers(Integer contractId);

	List<BillableItem> getCGBillableItems(Integer contractId);

	List<BillableItem> getCGBillableItemForService(Integer billingServiceId);

	List<ServiceStatus> getServiceStatuses(Service service, Integer statusId, String remarks);

	List<BillableItem> getUpdatedBillableItems(List<BillableItem> billableItemsUpdate,
			List<BillableItem> billableItemsData);

	List<BillableItem> getVesselsBillableItems(Integer contractId);

	Service saveService(Service service);

	Service saveService(Service service, List<BillableItem> billingBillableItems);

	String getCGName(String consumer);

	String getAgencyName(String agencyCode);

	String getAgencyName(Service service);

	List<PortalShippingCompany> getShippingCompanys();

	HashMap<String, HashMap<String, List<BillableItem>>> getHashedBillableItems(Integer contractId, String fromDate,
			String toDate);

	List<LritNameDto> getAllAgency();

	HashMap<String, PaymentDetailsPerMember> getPaymentMembers(Integer billingServiceId);

	HashMap<String, PaymentDetailsPerMember> getPaymentMembers(List<BillableItem> billableItems);

	Double getRemainingAmountService(Service service);

	HashMap<String, PaymentDetailsPerMember> getUncheckedMembers(Service service);

	Service setServiceStatuses(Service servicePara, Integer statusId, String remarks);

	Service setBillableItems(Service mainService, Service dataCareerService);

	Service updatePayment(Service paymentService, Service service, List<String> consumers);

	HashSet<String> getMembers(Service service);

	/* List<BillableItem> getCGBillableItemsForBill(Integer contractId); */

	List<String> getConsumers(Contract contract);

	String getVesselName(String vesselId);

	String getNameContractTypeBased(String id, Integer contractType);

	HashMap<String, HashMap<String, List<BillableItem>>> getCGHashedBillableItems(Integer billingServiceId);

	// Get billable Item system count
	Integer getBillableItemSystemCount(String provider, String consumer, Date fromDate, Date toDate, Integer bic,
			Integer contractTypeId);

	// Get payment billable Item category
	List<PaymentBillableItemCategory> getPaymentBillableItemCategoryList(Integer contractId);

}
