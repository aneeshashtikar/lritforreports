package in.gov.lrit.billing.service.billingservice.invoice;

import java.util.Collection;
import java.util.List;

import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.billingservice.invoice.Invoice;
import in_.gov.lrit.billing.dto.BillInvoiceListDto;
import in_.gov.lrit.billing.dto.InvoiceDto;

/**
 * Service layer interface to handle Invoice entity related activities
 * 
 * @author Shivraj
 * 
 */
public interface InvoiceService {

	/**
	 * TO SAVE THE GENERATED INVOICE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @return
	 */
	public Invoice saveInvoice(Invoice invoice);

	/**
	 * RETURNS ALL INVOICE FALLING BETWEEN FROMDATE AND TODATE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public Collection<Invoice> findAllInvoice(Integer contractType, String fromDate, String toDate);

	/**
	 * RETURNS INVOICE BASED ON SERVICEID
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param billingServiceId
	 * @return
	 */
	public Invoice getInvoice(Integer invoiceId);

	/**
	 * TO SAVE UPLOAD INVOICE APPROVED BY DG IN DATABASE ALONG WITH STATUS
	 * 
	 * @author Yogendra Yadav
	 * @param serviceStatus
	 * @param approvedCopyByte
	 * @param invoiceId
	 * @return
	 */
	public Invoice uploadApprovedInvoice(String remarks, byte[] approvedCopyByte, Integer invoiceId);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param mailBody
	 * @param remarks
	 * @param invoiceId
	 */
	public void sendMail(String mailBody, String remarks, Integer invoiceId);

	/**
	 * THIS METHOD SEND MAIL TO ASP/DC/USAS FOR PAYMENT
	 * 
	 * @author Yogendra Yadav
	 * @param mailBody
	 * @return
	 */
	boolean sendMailToAspDc(String mailBody);

	/**
	 * RETURN INVOICE FETHED USING billingServiceId AND ALSO SETS THE STATUS OF THAT
	 * INVOICE
	 * 
	 * @author Yogendra Yadav
	 * @param billingServiceId
	 * @param statusId
	 * @param remarks
	 * @return INVOICE
	 */
	Invoice getStatusInvoice(Integer invoiceId, Integer statusId, String remarks);

	/**
	 * TO UPDATE THE DG REMARKS AND STATUS
	 * 
	 * @author Yogendra Yadav
	 * @param remarks
	 * @param invoiceId
	 * @return
	 */
	public Invoice updateToDG(Invoice invoicePara, Integer invoiceId);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	List<BillInvoiceListDto> findAllInvoiceBillDto(Integer contractType, String fromDate, String toDate);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @param remarks
	 * @param invoiceId
	 * @return
	 */
	Invoice updateMessageCount(Service invoice, String remarks, Integer invoiceId);

	/**
	 * TO PERSIST SHIPPPING INVOICE
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @return
	 */
	Invoice saveShippingInvoice(Invoice invoice);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @return
	 */
	Invoice saveInvoiceService(Service invoice);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @return
	 */
	Invoice saveShippingInvoiceService(Service invoice);

	/**
	 * RETURN ALL INVOICE BASED ON TIME PROVIDED AND THE CONTRACT TYPE
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	List<InvoiceDto> findAllInvoiceDto(Integer contractType, String fromDate, String toDate);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @param consumers
	 * @param invoiceId
	 * @return
	 */
	Invoice updatePayment(Invoice invoice, List<String> consumers, Integer invoiceId);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param dateString
	 * @return
	 */
	String getInvoiceNumber(String dateString);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param year
	 * @return
	 */
	String getInvoiceNumber(int year);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param service
	 * @param invoiceId
	 * @return
	 */
	Invoice updateMessageCount(Service service, Integer invoiceId);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @param invoiceId
	 * @return
	 */
	Invoice uploadApprovedInvoice(Invoice invoice, Integer invoiceId);

}
