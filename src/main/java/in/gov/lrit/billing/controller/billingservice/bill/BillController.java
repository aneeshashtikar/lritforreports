/**
 * @BillController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 08-Aug-2019
 */
package in.gov.lrit.billing.controller.billingservice.bill;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import in.gov.lrit.billing.dao.billingservice.bill.BillRepository;
import in.gov.lrit.billing.dao.payment.PaymentDetailsForServiceRepository;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.bill.Bill;
import in.gov.lrit.billing.model.billingservice.bill.BillCsp;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.model.payment.PaymentDetailsForService;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.service.ServiceService;
import in.gov.lrit.billing.service.billingservice.bill.BillService;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.payment.PaymentDetailsForServiceService;
import in.gov.lrit.mail.LRITMailSender;
import in_.gov.lrit.billing.dto.BillDto;
import in_.gov.lrit.ddp.dto.LritNameDto;
import net.sf.jasperreports.engine.JRException;

/**Description :- BillController is for Bill functionalities
 * @author lrit-billing
 *
 */
@Controller
@RequestMapping(path = "/bill")
public class BillController {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BillController.class);
	private static final long SerialVersionUID = 10l; 
	 

	
	@Autowired
	private PaymentDetailsForServiceService paymentDetailsForServiceService;


	@Autowired
	private BillService billService;

	@Autowired
	private ContractService contractService;

	@Autowired
	@Qualifier("serviceServiceImpl")
	private ServiceService serviceService;

	@Autowired
	private LRITMailSender lritMailSender;
	
	@Value("${ndc.emailid}")
	private String ndcEmailId;
	
	@Value("${ddg.emailid}")
	private String ddgEmailId;


	/**  
	 * to convert document in byte[]
	 * @author lrit-billing
	 */
	public byte[] getFileByte(MultipartFile billdoc) {
		byte[] billDocument = null;
		try {
			billDocument = billdoc.getBytes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return billDocument;
	}
	
	
	/**  
	 * to get bill document
	 * @author lrit-billing
	 */

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/uploaded/{serviceId}", method = RequestMethod.GET)
	public void getFile(@PathVariable("serviceId") Integer serviceId, HttpServletResponse response) throws IOException {
		logger.info("getFile()");
		response.setContentType("application/pdf");
		Bill bill = billService.getBill(serviceId);
		response.setHeader("Content-Disposition", String.format("attachment;filename=\"Bill_Document_From_\\"
				+ bill.getFromDate() + "To_" + bill.getToDate() + ".pdf" + ""));
		BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
		fos1.write(bill.getBilldoc());
		fos1.flush();
		fos1.close();
	}

	/**  
	 * to show bill list 
	 * @author lrit-billing
	 */

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/listBill", method = RequestMethod.GET)
	public ModelAndView listBill() {
		logger.info("listBill()");
		Bill bill = new Bill();
		List<ContractType> contractType = contractService.getContractTypes();
		for (int index = 0; index < contractType.size(); index++) {
			if (contractType.get(index).getContractTypeId() == 2 || contractType.get(index).getContractTypeId() == 4)
				contractType.remove(index);
		}
		logger.info("ContractTypes()"+contractType);
		ModelAndView model = new ModelAndView("billing/bill/listBill");
		model.addObject("contractType", contractType);
		model.addObject("bill", bill);
		return model;
	}
	

	/**  
	 * to show bill list 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/listBill", method = RequestMethod.POST)
	@ResponseBody
	public List<BillDto> listBillJson(@RequestParam("contractType") Integer contractType,
			@RequestParam(name = "fromDate", required = false) String fromDate,
			@RequestParam(name = "toDate", required = false) String toDate) {
		logger.info(contractType.toString());
		logger.info(fromDate);
		logger.info(toDate);
		List<BillDto> billList = billService.findAllBillDto(contractType, fromDate, toDate);
		return billList;
	}


	/**  
	 * to add bill GET method
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/addBill", method = RequestMethod.GET)
	public String saveBillGET(Model model) {
		Bill bill = new Bill();
		logger.info("addBillGET()");
		
		List<LritNameDto> agencys = contractService.getAgencyContractTypeWise(1);
		model.addAttribute("agencys", agencys);
		model.addAttribute("bill", bill);
		return "billing/bill/addBill";
	}


	/**  
	 * For Save bill
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/addBill", method = RequestMethod.POST)
	public String saveBillPOST(@ModelAttribute("bill") Bill bill, @RequestParam("billdocument") MultipartFile billdoc,
			Model model) {
		logger.info(billdoc.getContentType() + billdoc.getName() + "//" + billdoc.getOriginalFilename() + "//"
				+ billdoc.getSize() + "//");
	
		byte[] byteArr = getFileByte(billdoc);
		bill.setBilldoc(byteArr);
		logger.info("billNo"+bill.getBillNo());
		billService.saveService(bill);
		List<ContractType> contractType = contractService.getContractTypes();
		for (int index = 0; index < contractType.size(); index++) {
			if (contractType.get(index).getContractTypeId() == 2 || contractType.get(index).getContractTypeId() == 4)
				contractType.remove(index);
		}
		// Fetch list of agencys contract type 1/ASPDC
		List<LritNameDto> agencys = contractService.getAgencyContractTypeWise(1);
		model.addAttribute("agencys", agencys);
		model.addAttribute("bill", new Bill());
		model.addAttribute("message", "Bill is added succesfully.");
		model.addAttribute("alt", "2");
		model.addAttribute("contractType", contractType);
		return "billing/bill/listBill";
	}
	
	/**  
	 * to get values of bill for verification 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/verifyBill/{billingServiceId}", method = RequestMethod.GET)
	public ModelAndView showVerifyBill(@PathVariable("billingServiceId") Integer billingServiceId) {
		logger.info("showVerifyBill()");
		ModelAndView model = new ModelAndView("billing/bill/verifyBill");
		Bill bill = billService.verifyBill(billingServiceId);
		// bill = (Bill) billService.setProviderConsumerName(bill);
		model.addObject("bill", bill);
		return model;
	}


	/**  
	 * Save verification report 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/verifyBill/{billingServiceId}", method = RequestMethod.POST)
	public ModelAndView showVerifyBillPOST(@PathVariable("billingServiceId") Integer billingServiceId, Bill bill,
			@RequestParam(value = "remarks", required = false) String remarks) {
		logger.info("showVerifyBillPOST()");
		ModelAndView model = new ModelAndView("billing/bill/listBill");
		billService.verifyBillWithSystemCount(bill, remarks);
		List<ContractType> contractType = contractService.getContractTypes();
		for (int index = 0; index < contractType.size(); index++) {
			if (contractType.get(index).getContractTypeId() == 2 || contractType.get(index).getContractTypeId() == 4)
				contractType.remove(index);
		}
		model.addObject("bill", bill);
		model.addObject("contractType", contractType);
		return model;
	}
	/**  
	 * Get Bill to send Verified bill to DG 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/sendVerifiedBillCG/{billingServiceId}", method = RequestMethod.GET)
	public ModelAndView showVerifiedBillCG(@PathVariable("billingServiceId") Integer billingServiceId) {
		logger.info("showVerifiedBillCG()");
		ModelAndView model = new ModelAndView("billing/bill/sendVerifiedBillDG");
		logger.info("BillingServiceId"+billingServiceId);
		Bill bill = billService.getBillById(billingServiceId);
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("contractType", contractType);
		model.addObject("bill", bill);
		model.addObject("bill", bill);
		return model;
	}

	/**  
	 * To Send Verified Bill to DG
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/sendVerifiedBillCG/{billingServiceId}", method = RequestMethod.POST)
	public ModelAndView sendVerifiedBillCG(@PathVariable("billingServiceId") Integer billingServiceId,
			HttpServletResponse response) throws IOException, MessagingException {
		logger.info("sendVerifiedBillCG()");
		ModelAndView model = new ModelAndView("billing/bill/listBill");
		logger.info("billingServiceId"+billingServiceId);
		Bill bill = billService.getBillById(billingServiceId);
		String currency = "$";
		int contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
		if (contractTypeId == 3)
			currency = "₹";
		byte[] billDoc = bill.getBilldoc();

		java.sql.Date fromDate = bill.getFromDate();
		java.sql.Date toDate = bill.getToDate();
		double grandTotal = bill.getGrandTotal();

		DataSource datasource = new ByteArrayDataSource(billDoc, "application/pdf");

		lritMailSender.sendEmailWithAttachment(ndcEmailId, ddgEmailId, "Bill Payment",
				"Payment of bill received by us " + " for Period " + fromDate + " to " + toDate + " of " + currency
						+ ". " + grandTotal + " is pending. <br> Please ignore this mail if bill is paid.<br><br>",

				datasource, "Bill No.-" + bill.getBillingServiceId());

		// Set Status after sending DG
		billService.setStatusSendVerifiedBill(billingServiceId);
		model.addObject("bill", bill);
		model.addObject("message", "Mail sent successfully");
		model.addObject("alt", "2");
		List<ContractType> contractType = contractService.getContractTypes();
		for (int index = 0; index < contractType.size(); index++) {
			if (contractType.get(index).getContractTypeId() == 2 || contractType.get(index).getContractTypeId() == 4)
				contractType.remove(index);
		}
		model.addObject("contractType", contractType);
		return model;
	}
	/**  
	 * to get values of bill for updating Count 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateCount", method = RequestMethod.GET)
	public String showUpdateCount(Model model) {
		logger.info("showUpdateCount()");
		BillCsp bill = new BillCsp();
		model.addAttribute("bill", bill);
		return "billing/bill/updateCount";
	}

	/**  
	 * to get values for update count 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateBill/{billingServiceId}", method = RequestMethod.GET)
	public ModelAndView showUpdateBill(@PathVariable("billingServiceId") Integer billingServiceId) {
		logger.info("showUpdateBill()");
		logger.info("BillingServiceId-"+billingServiceId);
		Bill bill = billService.getBillById(billingServiceId);
		String agencyCode = bill.getBillingContract().getAgency2().getAgencyCode();
		Integer contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
		logger.info("agencyCode-"+agencyCode);
		String agencyName = contractService.getAgencyName(contractTypeId, agencyCode);
		logger.info("AgencyName-"+agencyName);
		ModelAndView model = new ModelAndView("billing/bill/updateBill");
		bill = (Bill) billService.setProviderConsumerName(bill);
		model.addObject("bill", bill);
		model.addObject("agencyName", agencyName);
		return model;
	}


	/**  
	 * to Save values with updated count 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateBill/{billingServiceId}", method = RequestMethod.POST)
	public ModelAndView updateBill(@PathVariable("billingServiceId") Integer billingServiceId,
			@ModelAttribute("bill") Bill bill, BindingResult bindingResult) {
		logger.info("updateBill()");
		logger.info("BillingServiceId-"+billingServiceId);

		ModelAndView model = new ModelAndView("billing/bill/listBill");
		try {
			bill = (Bill) billService.updateMessageCount(bill, billingServiceId);
		} catch (SQLException | JRException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (bill != null) {
			model.addObject("message", "Success!! Message Count has been Updated");
			String agencyCode = bill.getBillingContract().getAgency2().getAgencyCode();
			Integer contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
			logger.info("contractTypeId"+contractTypeId);
			String agencyName = contractService.getAgencyName(contractTypeId, agencyCode);
			logger.info("agencyName"+agencyName);
			bill = (Bill) billService.setProviderConsumerName(bill);
			model.addObject("bill", bill);
			model.addObject("agencyName", agencyName);
			model.addObject("message", "Success!! Message Count has been Updated");
			List<ContractType> contractType = contractService.getContractTypes();
			for (int index = 0; index < contractType.size(); index++) {
				if (contractType.get(index).getContractTypeId() == 2 || contractType.get(index).getContractTypeId() == 4)
					contractType.remove(index);
			}
			model.addObject("contractType", contractType);
			return model;
		}

		else {
			logger.info("In else");
			model.addObject("error", "Sorry!! Failed To Update Message Count");
			model.addObject("alt", "1");
			return model;
		}
	}

	/**  
	 * to get values of bill for updating Payment 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(path = "/updatePayment/{billingServiceId}", method = RequestMethod.GET)
	public ModelAndView updatePaymentGET(@PathVariable("billingServiceId") Integer billingServiceId) {

		logger.info("updatePaymentGet");
		Bill bill = billService.getBill(billingServiceId);
		logger.info("BillingServiceId-"+billingServiceId);
		bill = (Bill) billService.setPaymentConsumerName(bill);
		String agencyCode = bill.getBillingContract().getAgency2().getAgencyCode();
		Integer contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
		
		String agencyName = contractService.getAgencyName(contractTypeId, agencyCode);
		double remainedAmount =billService.getRemainingAmountService(bill);
         remainedAmount = Math.round(remainedAmount * 100.0) / 100.0;
		
		ModelAndView model = new ModelAndView("billing/bill/updatePayment");
		model.addObject("bill", bill);
		model.addObject("agencyName", agencyName);
		model.addObject("unCheckedMembers", billService.getUncheckedMembers(bill));
		model.addObject("remainedAmount",remainedAmount);
		return model;
	}

	/**  
	 * to Save the bill with payment details 
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(path = "/updatePayment/{billingServiceId}", method = RequestMethod.POST)
	public String updatePaymentPOST(@PathVariable("billingServiceId") Integer billingServiceId,
			@RequestParam(name = "consumers", required = false) List<String> consumers,
			@RequestParam(name="paymentDate") String paymentDate,
			@ModelAttribute("bill") Bill billFromUser,@RequestParam(name="paymentAdviceDocument") MultipartFile paymentAdviceDocument,
			BindingResult bindingResult, Model model) {
		logger.info("updatePaymentPOST()");
		String errorMessage = "";
		logger.info("BillingServiceId-"+billingServiceId);
		logger.info("Payment Date "+paymentDate);
		byte[] paymentDocument = getFileByte(paymentAdviceDocument);
		
		Bill fetchedBill = billService.getBill(billingServiceId);
		String agencyCode = fetchedBill.getBillingContract().getAgency2().getAgencyCode();
		Integer contractTypeId = fetchedBill.getBillingContract().getBillingContractType().getContractTypeId();
		String agencyName = contractService.getAgencyName(contractTypeId, agencyCode);
		fetchedBill = (Bill) billService.setPaymentConsumerName(fetchedBill);

		Double remainingAmount = billService.getRemainingAmountService(fetchedBill);
		logger.info("RemainingAmount "+remainingAmount);
		Double paidAmount = billFromUser.getBillingPaymentDetailsForServices().get(0).getPaidAmount();
		logger.info("Paid Amount "+paidAmount);
	
		if (consumers != null) {
			logger.info("IF CONSUMERS is selected");
			Double selectedAgencyAmount = 0.0;

			PaymentDetailsForService paymentDetailsForService = billFromUser.getBillingPaymentDetailsForServices().get(0);
			logger.info("PaymentDetailsForService-"+paymentDetailsForService);
			for (String consumer : consumers) {
				for (PaymentDetailsPerMember paymentDetailsPerMember : paymentDetailsForService
						.getBillingPaymentDetailsPerMembers()) {
					if (consumer.equals(paymentDetailsPerMember.getConsumer())) {
						selectedAgencyAmount += paymentDetailsPerMember.getCost();
					}
				}
			}

			if (selectedAgencyAmount > paidAmount) {
				// PAID AMOUNT LESS THAN SELECTED AGENCY
				bindingResult.rejectValue("billingPaymentDetailsForServices[0].paidAmount", "upload.invalid.file.type");
				logger.info("PAID AMOUNT LESS THAN SELECTED AGENCY");
				errorMessage = "Paid Amount is Less than selected Agency.";
			}

		} else {
			if (paidAmount <= 0) {
				bindingResult.rejectValue("grandTotal", "upload.invalid.file.type");
				logger.error("PAID AMOUNT SHOULD NOT BE ZERO");
				errorMessage = "Paid Amount should not be Zero.";
			}
		}
			if (paidAmount > remainingAmount) {
				// PAID AMOUNT GREATER THAN REMAINIG AMOUNT
				bindingResult.rejectValue("grandTotal", "upload.invalid.file.type");
				logger.info("PAID AMOUNT GREATER THAN REMAINING AMOUNT");
				errorMessage = "Paid Amount is greater than Remaining amount.";
			}

		if (bindingResult.hasErrors()) {
			logger.info("IF Error bindingResult Error");
			logger.info(bindingResult.getAllErrors().toString());
			model.addAttribute("error", errorMessage);
			// TO SET CONSUMER NAME OF CG OR VESSEL INSTEAD OF LRITID OR VESSELID FETECHED
			// FOR PAID CONSUMERS FROM DATABASE
			model.addAttribute("agencyName", agencyName);
			model.addAttribute("bill", fetchedBill);
			model.addAttribute("unCheckedMembers", billService.getUncheckedMembers(fetchedBill));
			model.addAttribute("remainedAmount", billService.getRemainingAmountService(fetchedBill));

			logger.info("Returning updatePayment.jsp page");
			return "billing/bill/updatePayment";


		} else {
			logger.info("No Error Persisiting Bill Payment");
			billFromUser = (Bill) billService.updatePayment(billFromUser, consumers, billingServiceId,paymentDocument,paymentDate);
			if (billFromUser != null) {
				billFromUser = billService.getBill(billingServiceId);
				model.addAttribute("message", "Success!! Payment has been Made");
			} else
				model.addAttribute("error", "Sorry!! Payment Failed");
		}

		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 2 || contractTypes.get(index).getContractTypeId() == 4)
				contractTypes.remove(index);
		}
		logger.info("ContractTypes" + contractTypes);
		model.addAttribute("contractType", contractTypes);

		logger.info("Returning listBill.jsp page");
		return "billing/bill/listBill";

	}

	/**  
	 * to get billable Items
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getBillableItems")
	@ResponseBody
	public List<BillableItem> getBillableItems(@RequestParam("contractId") Integer contractId,
			@RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
			@RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date toDate, Model model) {
		logger.info("getBillableItems()");
		List<BillableItem> billableItems = billService.getBillableItems(contractId, fromDate, toDate);

		return billableItems;
	}

	/** 
	 * to get billable items for bill
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGBillableItemsForBill")
	@ResponseBody
	public List<BillableItem> getCGBillableItemsForBill(@RequestParam("contractId") Integer contractId,
			@RequestParam("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
			@RequestParam("toDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date toDate, Model model) {
		
		logger.info("getCGBillableItemsForBill()");
		List<BillableItem> billableItems = billService.getBillableItems(contractId, fromDate, toDate);
		logger.info("BillableItem"+billableItems);
		return billableItems;

	}
	
	/** 
	 * to get Annual Management Charges
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getAnnualManagementCharges")
	@ResponseBody 
	public Double getAnnualManagementCharges(@RequestParam("contractId") Integer contractId,   Model model) {
		logger.info("getAnnualManagementCharges()");
		ContractCSP contractCSP = billService.getCSPContractByContractId(contractId);
		Double annualManagementCharges = (contractCSP.getManagementCharges()) / 12;
		return annualManagementCharges;
	}

	
	/** 
	 * to show bill details
	 * @author lrit-billing
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/viewBill/{billingServiceId}", method = RequestMethod.GET)
	public ModelAndView viewBill(@PathVariable("billingServiceId") Integer billingServiceId) {
		logger.info("viewBill()");
		logger.info("billingServiceId-"+billingServiceId);
		Bill bill = billService.getBillById(billingServiceId);
		String agencyCode = bill.getBillingContract().getAgency1().getAgencyCode();
		Integer contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
		String agencyName = contractService.getAgencyName(contractTypeId, agencyCode);
		ModelAndView model = new ModelAndView("billing/bill/viewBill");
		bill = (Bill) billService.setProviderConsumerName(bill);
		List<PaymentDetailsForService> paymentDetailsForServices = bill.getBillingPaymentDetailsForServices();
		for (PaymentDetailsForService paymentDetailsForService : paymentDetailsForServices) {
			List<PaymentDetailsPerMember> paymentDetailsForMembers = paymentDetailsForService
					.getBillingPaymentDetailsPerMembers();
			for (PaymentDetailsPerMember paymentDetailsForMember : paymentDetailsForMembers) {
				paymentDetailsForMember.setConsumerName(
						contractService.getStakeholderName(paymentDetailsForMember.getConsumer(), contractTypeId));
			}
		}
		List<ServiceStatus> billingServiceStatuses = bill.getBillingServiceStatuses();
		for(ServiceStatus billingServiceStatus : billingServiceStatuses) {
			String billingStatusName = billingServiceStatus.getBillingStatus().getStatusName();
			billingServiceStatus.setStatusName(billingStatusName);
		}
		logger.info("BillStatus"+bill.getBillingServiceStatuses());
		model.addObject("bill", bill);
		model.addObject("agencyName", agencyName);
		return model;
	}
	
	/**  
	 * to get payment advice document
	 * @author lrit-billing
	 */

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/adviceDocument/{pmtDetServiceId}", method = RequestMethod.GET)
	public void getAdviceDocument(@PathVariable("pmtDetServiceId") Integer pmtDetServiceId, HttpServletResponse response) throws IOException {
		logger.info("getFile()");
		response.setContentType("application/pdf");
		PaymentDetailsForService paymentDetailsForService= paymentDetailsForServiceService.getPaymentDetailsByPaymentServiceId(pmtDetServiceId);
		response.setHeader("Content-Disposition", String.format("attachment;filename=\"Payment_Advice_Document_\\"
				+ paymentDetailsForService.getPaymentDate()+".pdf" + ""));
		BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
		fos1.write(paymentDetailsForService.getPaymentAdviceDocument());
		fos1.flush();
		fos1.close();
	}


}