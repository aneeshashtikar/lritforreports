package in.gov.lrit.billing.controller.billingservice.bill;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import in.gov.lrit.billing.dao.billingservice.bill.BillRepository;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.bill.Bill;
import in.gov.lrit.billing.model.billingservice.bill.BillCsp;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.model.payment.PaymentDetailsForService;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.service.ServiceService;
import in.gov.lrit.billing.service.billingservice.bill.BillCspService;
import in.gov.lrit.billing.service.billingservice.bill.BillService;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.mail.LRITMailSender;
import in_.gov.lrit.ddp.dto.LritNameDto;
import net.sf.jasperreports.engine.JRException;


/**
 * Description :- BillCspController is used for CSP bill functionalities
 * @author lrit-billing
 * 
 */
@Controller
@RequestMapping(path = "/billCSP")
public class BillCspController {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BillCspController.class);
	private static final long SerialVersionUID = 10l; 

	@Autowired
	private BillService billService;

	@Autowired
	private ContractService contractService;

	@Autowired
	@Qualifier("serviceServiceImpl")
	private ServiceService serviceService;

	@Autowired
	private LRITMailSender lritMailSender;
	

	/**  
	 * to convert document in byte[]
	 * @author lrit-billing
	 */
	public byte[] getFileByte(MultipartFile billdoc) {
		byte[] billDocument = null;
		try {
			billDocument = billdoc.getBytes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return billDocument;
	}
	
	/**  
	 * to add CSP bill GET
	 * @author lrit-billing
	 */
	@RequestMapping(value = "/addCSPBill", method = RequestMethod.GET)
	public String addCSPBill(Model model) {
		logger.info("addCSPBill()");
		BillCsp bill = new BillCsp();
		List<LritNameDto> agencys = contractService.getAgencyContractTypeWise(3);
		model.addAttribute("agencys", agencys);
		model.addAttribute("bill", bill);
		return "billing/bill/addCSPBill";
	}

	/**  
	 * to add CSP bill POST
	 * @author lrit-billing
	 */
	@RequestMapping(value = "/addCSPBill", method = RequestMethod.POST)
	public String saveCSPBillPOST(@ModelAttribute("bill") BillCsp bill,
			@RequestParam("billdocument") MultipartFile billdoc, Model model) {
		logger.info(billdoc.getContentType() + billdoc.getName() + "//" + billdoc.getOriginalFilename() + "//"
				+ billdoc.getSize() + "//");
		byte[] byteArr = getFileByte(billdoc);
		bill.setBilldoc(byteArr);
		logger.info("billNo"+bill.getBillNo());
		billService.saveService(bill);
		BillCsp newBill = new BillCsp();
		List<ContractType> contractType = contractService.getContractTypes();
		for (int index = 0; index < contractType.size(); index++) {
			if (contractType.get(index).getContractTypeId() == 2 || contractType.get(index).getContractTypeId() == 4 )
				contractType.remove(index);
		}
		// Hard coding to get agency list for contract type 1 and 3 
		List<LritNameDto> agencys = contractService.getAgencyContractTypeWise(3);
		model.addAttribute("agencys", agencys);
		model.addAttribute("bill", newBill);
		model.addAttribute("message", "Bill is added succesfully.");
		model.addAttribute("contractType", contractType);
		return "billing/bill/listBill";
	}
	
	
	/**  
	 * to get values of bill for verification
	 * @author lrit-billing
	 */
	@RequestMapping(value = "/verifyCSPBill", method = RequestMethod.GET)
	public String showVerifyCSPBill(Model model) {
		logger.info("showUpdateCount()");
		Bill bill = new Bill();
		model.addAttribute("bill", bill);
		return "billing/bill/verifyCSPBill";
	}
	
	
	/**  
	 * to get values of CSP bill for update 
	 * @author lrit-billing
	 */
	@RequestMapping(value = "/updateCSPBill/{billingServiceId}", method = RequestMethod.GET)
	public ModelAndView showUpdateCSPBill(@PathVariable("billingServiceId") Integer billingServiceId) {
		logger.info("showUpdateCSPBill()");
		logger.info("billingServiceId-"+billingServiceId);
		Bill bill = billService.getBillById(billingServiceId);
		String agencyCode = bill.getBillingContract().getAgency2().getAgencyCode();
		Integer contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
		logger.info("contractTypeId-"+contractTypeId);
		ContractCSP contractCSP = billService.getCSPContractByContractId(bill.getBillingContract().getContractId());
		
		//Double annualManagementCharges = (contractCSP.getManagementCharges()) / 12;

		double annualManagementCharges =(contractCSP.getManagementCharges()) / 12;
		annualManagementCharges = Math.round(annualManagementCharges * 100.0) / 100.0;
		String agencyName = contractService.getAgencyName(contractTypeId, agencyCode);
		logger.info("AgencyName-"+agencyName);
		ModelAndView model = new ModelAndView("billing/bill/updateCSPBill");
		bill = (BillCsp) billService.setProviderConsumerName(bill);
		model.addObject("bill", bill);
		model.addObject("agencyName", agencyName);
		model.addObject("annualManagementCharges", annualManagementCharges);
		return model;

	}

	/**  
	 * to get Billable Items for CSP Bill
	 * @author lrit-billing
	 */
	@RequestMapping(value = "/getCGBillableItemsForCSPBill")
	@ResponseBody
	public List<BillableItem> getCGBillableItemsForCSPBill(@RequestParam("contractId") Integer contractId, Model model) {
		
		logger.info("getCGBillableItemsForCSPBill()");
		Contract contract = contractService.getContractByContractId(contractId);
		List<BillableItem> billableItems = serviceService.getCGBillableItems(contractId);
		double subtotal = 0;
		for (BillableItem billableItem : billableItems) {
			subtotal += billableItem.getCost();
			//For Bill Provider is Consumer and Consumer is provider
			billableItem.setConsumerName(serviceService.getCGName(billableItem.getProvider()));
			if (billableItem.getConsumer().equals(contract.getAgency2().getAgencyCode())) {
				billableItem.setProviderName(contract.getAgency2().getNameOfCompany());

			}else
			billableItem.setProviderName(serviceService.getCGName(billableItem.getConsumer()));
		}
		logger.info("Subtotal of billable Items " + subtotal);
		return billableItems;
	}
	
	
	/**  
	 * to get values of CSP Bill for view 
	 * @author lrit-billing
	 */
	@RequestMapping(value = "/viewCSPBill/{billingServiceId}", method = RequestMethod.GET)
	public ModelAndView viewCSPBill(@PathVariable("billingServiceId") Integer billingServiceId) {
		logger.info("viewCSPBill()");
		logger.info("billingServiceId-"+billingServiceId);
		Bill bill = billService.getBillById(billingServiceId);
		String agencyCode = bill.getBillingContract().getAgency1().getAgencyCode();
		Integer contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
		ContractCSP contractCSP = billService.getCSPContractByContractId(bill.getBillingContract().getContractId());
	
		double annualManagementCharges =(contractCSP.getManagementCharges()) / 12;
		annualManagementCharges = Math.round(annualManagementCharges * 100.0) / 100.0;
		List<PaymentDetailsForService> paymentDetailsForServices = bill.getBillingPaymentDetailsForServices();
		for(PaymentDetailsForService paymentDetailsForService : paymentDetailsForServices) {
			List<PaymentDetailsPerMember> paymentDetailsForMembers =	paymentDetailsForService.getBillingPaymentDetailsPerMembers();
			for(PaymentDetailsPerMember paymentDetailsForMember :paymentDetailsForMembers ) {
				paymentDetailsForMember.setConsumerName(contractService.getStakeholderName(paymentDetailsForMember.getConsumer(), contractTypeId));
			}
		}
		
		List<ServiceStatus> billingServiceStatuses = bill.getBillingServiceStatuses();
		
		for(ServiceStatus billingServiceStatus : billingServiceStatuses) {
			String billingStatusName = billingServiceStatus.getBillingStatus().getStatusName();
			billingServiceStatus.setStatusName(billingStatusName);
		}
		
		Agency agency = contractService.getAgencyByAgencyCode(agencyCode);
		ModelAndView model = new ModelAndView("billing/bill/viewCSPBill");
		bill = (BillCsp) billService.setProviderConsumerName(bill);
		model.addObject("bill", bill);
		model.addObject("agencyName", agency.getName());
		model.addObject("annualManagementCharges", annualManagementCharges);
		return model;

	}
	
	/**  
	 * to update the CSP Bill
	 * @author lrit-billing
	 * @throws IOException 
	 * @throws JRException 
	 * @throws SQLException 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateCSPBill/{billingServiceId}", method = RequestMethod.POST)
	public ModelAndView updateCSPBill(@PathVariable("billingServiceId") Integer billingServiceId,@ModelAttribute("bill") BillCsp bill,
			BindingResult bindingResult) throws SQLException, JRException, IOException {
		logger.info("updateCSPBill()");
		logger.info("BillingserviceId-"+billingServiceId);
		logger.info("BillCSPNO-"+bill.getBillNo());
		ModelAndView model = new ModelAndView("billing/bill/listBill");
		
		bill = (BillCsp) billService.updateMessageCount(bill,billingServiceId);

		if (bill != null) {
			model.addObject("message", "Success!! Message Count has been Updated");
			String agencyCode = bill.getBillingContract().getAgency2().getAgencyCode();
			Integer contractTypeId = bill.getBillingContract().getBillingContractType().getContractTypeId();
			logger.info("contractTypeId"+contractTypeId);
			String agencyName = contractService.getAgencyName(contractTypeId, agencyCode);
			logger.info("AgencyName-"+agencyName);
			bill = (BillCsp) billService.setProviderConsumerName(bill);
			
			List<ContractType> contractType = contractService.getContractTypes();
			for (int index = 0; index < contractType.size(); index++) {
				if (contractType.get(index).getContractTypeId() == 2 || contractType.get(index).getContractTypeId() == 4 )
					contractType.remove(index);
			}
			model.addObject("contractType", contractType);
			model.addObject("bill", bill);
			model.addObject("agencyName", agencyName);
			model.addObject("message", "Success!! Message Count has been Updated");
			return model;
		}

		else {
			logger.info("In Else");
			model.addObject("error", "Sorry!! Failed To Update Message Count");
			model.addObject("alt", "1");
			return model;
		}
	}
	




}
