/**
 * @ServiceController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing 
 * @version 1.0 26-Aug-2019
 */
package in.gov.lrit.billing.controller.billingservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.service.ServiceAbstractService;

/**
 * @author lrit-billing
 *
 */
@Controller
@RequestMapping(value = "/service")
public class ServiceAbstractController {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ServiceAbstractController.class);

	@Autowired
	@Qualifier("serviceAbstractServiceImpl")
	private ServiceAbstractService serviceAbstractServiceImpl;

	/**
	 * RETURNS BILLABLE ITEM FOR FOR GIVEN SERVICE ID (INVOICE/BILL)
	 * 
	 * 
	 * @author lrit-billing
	 * @param billingServiceId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGBillableItemsService")
	@ResponseBody
	public List<BillableItem> getCGBillableItemsService(@RequestParam("billingServiceId") Integer billingServiceId,
			Model model) {
		logger.info("INSIDE getCGBillableItemsService");
		logger.info("Contract " + billingServiceId);

		List<BillableItem> billableItems = serviceAbstractServiceImpl.getCGBillableItemForService(billingServiceId);

		return billableItems;
	}

}
