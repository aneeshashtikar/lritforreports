
package in.gov.lrit.billing.controller.contract;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAnnexure;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.CurrencyType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.service.contract.ContractAspDcService;
import in.gov.lrit.billing.service.contract.ContractCSPService;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.contract.ContractUsaService;
import in.gov.lrit.billing.validator.contract.ContractValidator;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.service.LritAspInfoService;
import in.gov.lrit.ddp.service.LritDatacentreInfoService;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in_.gov.lrit.billing.dto.ContractDto;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Controller to handle Contract related views
 * 
 * @author lrit-billing
 * 
 */
@Controller
@RequestMapping("/contract")
public class ContractController {

	// logger
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractController.class);

	@Autowired
	private ContractService contractService;

	@Autowired
	private ContractCSPService contractCSPService;

	@Autowired
	private ContractUsaService contractUsaService;

	@Autowired
	private ContractValidator contractValidator;

	@Autowired
	private ContractAspDcService contractAspDcService;


	@Autowired
	private LritAspInfoService lritAspInfoService;

	@Autowired
	private LritDatacentreInfoService lritDatacentreInfoService;
	/**
	 * TO GET CONTRACT FALLS BETWEEN GIVEN DATES
	 * 
	 * 
	 * @author lrit-billing
	 * 
	 */
	@RequestMapping(path = "/getCGContractList", method = RequestMethod.GET)
	@ResponseBody
	public List<Contract> getContractsList(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate,
			@RequestParam(name = "contractType", required = false) Integer contractType) {
		logger.info("getContractsList()");

		return contractService.getContractList(contractType, fromDate, toDate);
	}

	/**
	 * 
	 * To get Shipping Company Contract
	 * 
	 * @author lrit-billing
	 */
	@RequestMapping(path = "/getShippingCompany", method = RequestMethod.GET)
	@ResponseBody
	public List<PortalShippingCompany> getShippingBasedOnContract(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate,
			@RequestParam(name = "contractType", required = false) Integer contractType) {

		logger.info("getShippingBasedOnContract()");
		if (contractType == null)
			contractType = 4;
		return contractService.getShippingCompany(contractType, fromDate, toDate);
	}

	/**
	 * 
	 * To get Shipping Company Contract List
	 * 
	 * @author lrit-billing
	 */
	@RequestMapping(path = "/getShippingContractList", method = RequestMethod.GET)
	@ResponseBody
	public List<Contract> getShippingContractsList(@RequestParam("agencyCode") String agencyCode,
			@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
			@RequestParam(name = "contractType", required = false) Integer contractType) {
		logger.info("getContractsList()");
		if (contractType == null)
			contractType = 4;
		return contractService.getShippingContractList(agencyCode, contractType, fromDate, toDate);
	}

	/**
	 * To get Agencys by Contract Type Wise
	 * @author lrit-billing 
	 */
	@RequestMapping(path = "/getAgencys", method = RequestMethod.GET)
	@ResponseBody
	public List<LritNameDto> getAgencys(@RequestParam(name = "contractType") Integer contractType) {
		logger.info("getAgencys()");
		logger.info("contractType-"+contractType);
		return contractService.getAgencyContractTypeWise(contractType);
	}

	/**
	 * To get Agency Names by passing Dates and Contract type
	 * @author lrit-billing
	 */
	@RequestMapping(path = "/getAgencyNames", method = RequestMethod.GET)
	@ResponseBody
	public Set<LritNameDto> getAgencyNames(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate,
			@RequestParam(name = "contractType", required = false) Integer contractType) {
		logger.info("getAgencyNames()");
	
		logger.info("contractType-"+contractType);
		Set<LritNameDto> agencyNames = new HashSet<LritNameDto>();

		List<Contract> contracts = contractService.getContractList(contractType, fromDate, toDate);
		List<LritNameDto> agencys = contractService.getLritNameDto();
		for (Contract contract : contracts) {
			for (LritNameDto agency : agencys) {
				if (contract.getAgency2().getAgencyCode().equals(agency.getLritid())) {
					agencyNames.add(agency);
					break;
				}
			}
		}
		return agencyNames;
	}

	/**
	 * To get Contract with agency,contract-type,from date and to Date Wise
	 * @author lrit-billing
	 */
	@RequestMapping(path = "/getAgencyContracts", method = RequestMethod.GET)
	@ResponseBody
	public List<Contract> getAgencyContracts(@RequestParam("agencyCode") String agencyCode,
			@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
			@RequestParam(name = "contractType", required = false) Integer contractType) {
		logger.info("getAgencyContracts()");
		logger.info("agencyCode-"+agencyCode);
		List<Contract> contracts = new ArrayList<Contract>();
		if (contractType == null)
			contractType = 1;
		contracts = contractService.getAgencyWiseContracts(agencyCode, contractType, fromDate, toDate);
		return contracts;
	}

	/**
	 * To get agency wise contracts
	 * @author lrit-billing
	 */
	@RequestMapping(path = "/getAgencyWiseContracts", method = RequestMethod.GET)
	@ResponseBody
	public List<Contract> getAgencyWiseContracts(@RequestParam("agencyCode") String agencyCode,
			@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
			@RequestParam(name = "contractType", required = false) Integer contractType) {
		logger.info("getAgencyWiseContracts()");
		logger.info("agencyCode-"+agencyCode);
		List<Contract> contracts = new ArrayList<Contract>();
		contracts = contractService.getAgencyWiseContracts(agencyCode, contractType, fromDate, toDate);


		return contracts;
	}


	/**
	 * To get Contract by ContractId
	 * @author lrit-billing
	 */
	@RequestMapping(path = "/getContractOnContractId", method = RequestMethod.GET)
	@ResponseBody
	public Object getContractOnContractId(@RequestParam("contractId") Integer contractId) {
		logger.info("getContractOnContractId()");
		logger.info("contractId-"+contractId);
		return contractService.getContract(contractId);
	}


	/**
	 * To View the list of Contracts
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/viewcontract", method = RequestMethod.POST)
	public ModelAndView viewContract() {
		logger.info("viewContract()");

		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("contractType", contractType);
		return model;
	}

	/**
	 * To add billable items
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping("/addbillableitem")
	public ModelAndView addBillableItem() {
		logger.info("addBillableItem()");
		ModelAndView model = new ModelAndView("/billing/contract/addbillableitemcategory");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("contractType", contractType);
		return model;
	}
	
	/**
	 * To get List of Contract as per Contract type and Dates
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getContract", method = RequestMethod.GET)
	public List<Contract> getContract(@RequestParam(name = "contractType") Integer contractType,
			@RequestParam(name = "fromDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
			@RequestParam(name = "toDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date toDate) {

		logger.info("getContract()");
		logger.info("contractId-"+contractType);
		logger.info("fromDate-"+fromDate);
		logger.info("toDate-"+toDate);
		
		if (contractType != null && fromDate != null && toDate != null) {
			List<Contract> listContracts = contractService.getListContracts(contractType, fromDate, toDate);	
			return listContracts;

		} else if (contractType != null && fromDate == null || toDate == null || fromDate == null || toDate == null) {
			List<Contract> listContracts = contractService.getListContracts(contractType);
			return listContracts;
		}
		return null;
	}

	/**
	 * To get List of Contract as per Contract type and Dates POSt
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getContract", method = RequestMethod.POST)
	@ResponseBody
	public List<ContractDto> getContractPost(@RequestParam(name = "contractType") Integer contractType,
			@RequestParam(name = "fromDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
			@RequestParam(name = "toDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date toDate) {
		logger.info("getContractPost() method");
		logger.info("contractId-"+contractType);
		logger.info("fromDate-"+fromDate);
		logger.info("toDate-"+toDate);
		if (contractType != null && fromDate != null && toDate != null) {
			List<ContractDto> listContractDtos = contractService.getAllContractsByContractType(contractType, fromDate,
					toDate);
			
			return listContractDtos;

		} else if (contractType != null && fromDate == null || toDate == null || fromDate == null || toDate == null) {
			List<ContractDto> listContractDtos = contractService.getAllContractsByContractType(contractType);
			
			return listContractDtos;

		}
		return null;
	}
	
	/**
	 * To view the Contract Document
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/upload/{contractId}", method = RequestMethod.GET)
	public void getFile(@PathVariable("contractId") Integer contractId, HttpServletResponse response)
			throws IOException {
		logger.info("getFile()");
		Contract contract = contractService.getContractByContractId(contractId);
		response.setContentType("application/pdf");

		response.setHeader("Content-Disposition",
				String.format("attachment;filename=\"Contract Document \\" + contract.getContractNumber() + "\\_From-"
						+ contract.getValidFrom() + "\\-To-" + contract.getValidTo() + ".pdf"));
		BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
		fos1.write(contract.getContractDocument());
		fos1.flush();
		fos1.close();
	}
	
	/**
	 * To view the Contract Annexure Document
	 * @author lrit-billing
	 * 
	 */
	@RequestMapping(value = "/uploadAnnexure/{contractAnnexureId}", method = RequestMethod.GET)
	public void getAnnexureFile(@PathVariable("contractAnnexureId") Integer contractAnnecureId, HttpServletResponse response)
			throws IOException {
		logger.info("getFile()");
		ContractAnnexure contractAnnexure = contractService.getContractAnnexureById(contractAnnecureId);
		response.setContentType("application/pdf");

		response.setHeader("Content-Disposition",
				String.format("attachment;filename=\"Contract Annexure Document \\" + contractAnnexure.getDate() + "\\_.pdf"));
		BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
		fos1.write(contractAnnexure.getAnnexureDocument());
		fos1.flush();
		fos1.close();
	}

	/**
	 * To get Contract by Contract Id
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getContractByContractId/{id}", method = RequestMethod.GET)
	public ModelAndView getContractByContractId(@PathVariable(name = "id") Integer contractId) {
		logger.info("getContractByContractId()");
		logger.info("contractID"+contractId);
		Contract contract = contractService.getContractByContractId(contractId);

		List<String> agency1ContractStakeholders = contractService.getAgency1ContractStakeholders(contractId);
		List<String> agency2ContractStakeholders = contractService.getAgency2ContractStakeholders(contractId);
		
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		List<LritContractingGovtMst> listCGInfoAgency1D = contractService.getStakeholderByDate(agency1ContractStakeholders, date);
		
		List<LritContractingGovtMst> listCGInfoAgency2D = contractService.getStakeholderByDate( agency2ContractStakeholders, date);
		
		List<LritContractingGovtMst> listCGInfoAgency1 = contractService.getNonStakeholderByDate(contract.getAgency1().getAgencyCode(), agency1ContractStakeholders, date);
		
		List<LritContractingGovtMst> listCGInfoAgency2 = contractService.getNonStakeholderByDate(contract.getAgency2().getAgencyCode(), agency2ContractStakeholders, date);
		
		
		
		List<ContractStakeholder> contractStakeholders1 = new ArrayList<ContractStakeholder>();
		
		for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1D) {
			String lritId = lritContractingGovt.getCgLritid();
			logger.info("LRITID"+lritId);
			ContractStakeholder contractStakeholder = contractService.getContractStakeholder(contractId, lritId);
			contractStakeholder.setName(lritContractingGovt.getCgName());
			contractStakeholders1.add(contractStakeholder);
		}
		

		List<ContractStakeholder> contractNonStakeholders1 = new ArrayList<ContractStakeholder>();
		for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1) {
			ContractStakeholder contractStakeholder = new ContractStakeholder(); 
			String lritId = lritContractingGovt.getCgLritid();
			String name = lritContractingGovt.getCgName(); 
			contractStakeholder.setName(name);
			contractStakeholder.setMemberId(lritId);
			contractNonStakeholders1.add(contractStakeholder);
		}
		


		List<ContractStakeholder> contractStakeholdersForAdditionalCGs = new ArrayList<>(); 
		List<ContractStakeholder> contractStakeholdersForAdditionalCGList = new ArrayList<ContractStakeholder>(); 
		ContractStakeholder contractStakeholders = new ContractStakeholder();
		contractStakeholdersForAdditionalCGs.add(contractStakeholders);
		
	
		List<LritContractingGovtMst> contractingGovtList = contractService.getAllContractingGovts();
		List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
		List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
		//logger.info("Contracting Govt List"+contractingGovtList);
		/*
		 * If Contract is ASP/DC
		 */
		if ((contract.getBillingContractType().getContractTypeId() == 1 && contract instanceof ContractAspDc) || contract.getBillingContractType().getContractTypeId() == 2) {
			ModelAndView model = new ModelAndView();
			if(contract.getBillingContractType().getContractTypeId() == 2) {
				for (int index = 0; index < currencyTypes.size(); index++) {
					CurrencyType newCurrencyType = currencyTypes.get(index);
					if (currencyTypes.get(index).getCurrencyType().equals("USD"))
						newCurrencyTypes.add(newCurrencyType);
				}
				model.addObject("currencyTypes",newCurrencyTypes);
				
				model = new ModelAndView("/billing/contract/updateusacontract");
			}
			else {
				model = new ModelAndView("/billing/contract/updateaspcontract");
			}
			
			List<ContractStakeholder> contractNonStakeholders2 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency2) {
				ContractStakeholder contractStakeholder = new ContractStakeholder(); 
				String lritId = lritContractingGovt.getCgLritid();
				String name = lritContractingGovt.getCgName(); 
				contractStakeholder.setName(name);
				contractStakeholder.setMemberId(lritId);
				contractNonStakeholders2.add(contractStakeholder);
			}
			
			List<ContractStakeholder> contractStakeholders2 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency2D) {
				String lritId = lritContractingGovt.getCgLritid();
				ContractStakeholder contractStakeholder = contractService.getContractStakeholder(contractId, lritId);
				contractStakeholder.setName(lritContractingGovt.getCgName());
				contractStakeholders2.add(contractStakeholder);
			}
			
			
			model.addObject("currencyTypes",currencyTypes);
			contract.setCurrentContractStakeholders(contractStakeholdersForAdditionalCGs);
			model.addObject("contract", contract);		
			model.addObject("agency1ContractStakeholders", contractStakeholders1);
			model.addObject("agency2ContractStakeholders", contractStakeholders2);
			model.addObject("agency1CG", contractNonStakeholders1);
			model.addObject("agency2CG", contractNonStakeholders2);
			model.addObject("contractStakeholdersForAdditionalCGs", contractStakeholdersForAdditionalCGs);
			model.addObject("contractStakeholdersForAdditionalCGList", contractStakeholdersForAdditionalCGList);
			model.addObject("contractingGovts", contractingGovtList);
			return model;

			/*
			 * If Contract is CSP
			 * 
			 */
		}  else if (contract.getBillingContractType().getContractTypeId() == 3) {
			Agency agency2 = contractCSPService.getAgencyCSP();
			contract.setAgency2(agency2);
			ModelAndView model = new ModelAndView("/billing/contract/updatecspcontract");
			
			for (int index = 0; index < currencyTypes.size(); index++) {
				CurrencyType newCurrencyType = currencyTypes.get(index);
				if (currencyTypes.get(index).getCurrencyType().equals("RS"))
					newCurrencyTypes.add(newCurrencyType);
			}
			model.addObject("currencyTypes",newCurrencyTypes);
			model.addObject("currencyTypes",currencyTypes);
			model.addObject("contract", contract);
			model.addObject("agency1ContractStakeholders", contractStakeholders1);
			model.addObject("agency1CG", contractNonStakeholders1);
			return model;

		}

		else if (contract.getBillingContractType().getContractTypeId() == 4) {
			ModelAndView model = new ModelAndView("/billing/contract/updateshippingcompanycontract");
			
			
			for (int index = 0; index < currencyTypes.size(); index++) {
				CurrencyType newCurrencyType = currencyTypes.get(index);
				if (currencyTypes.get(index).getCurrencyType().equals("RS"))
					newCurrencyTypes.add(newCurrencyType);
			}
			model.addObject("currencyTypes",newCurrencyTypes);
			model.addObject("currencyTypes",currencyTypes);
			model.addObject("contract", contract);
			model.addObject("currencyTypes",currencyTypes);
			model.addObject("agency1ContractStakeholders", contractStakeholders1);
			model.addObject("agency1CG", contractNonStakeholders1);
			return model;

		}

		return null;

	}
	
	/**
	 * To terminate the Contract
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/terminate/{id}", method = RequestMethod.GET)
	public ModelAndView terminateContractByContractId(@PathVariable(name = "id") Integer contractId) {
		logger.info("terminateContractByContractId()");
		logger.info("contractId-"+contractId);
		
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		contractService.terminateContractByContractId(contractId);
		model.addObject("message", "Contract terminated.");
		//model.addObject("alt", "1");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("contractType", contractType);
		return model;
	}

	/**
	 * To Terminate contract Post
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/terminate/{id}", method = RequestMethod.POST)
	public ModelAndView terminateContractByContractIdPOST(@PathVariable(name = "id") Integer contractId) {
		logger.info("terminateContractByContractIdPOST()");
		logger.info("contractId-"+contractId);
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		contractService.terminateContractByContractId(contractId);
		model.addObject("message", "Contract terminated");
		//model.addObject("alt", "1");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("contractType", contractType);
		return model;
	}

	/**
	 * To get all the Contracts
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping("/getAllContracts")
	public ModelAndView getAllContracts() {
		logger.info("getAllContracts()");
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		return model;
	}

	/**
	 * To get Agency Details by Agency Code
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getAgencyDetails", method = RequestMethod.POST)
	public @ResponseBody Agency getAgencyDetails(@RequestParam("agencyCode") String agencyCode) {
		logger.info("getAgencyDetails()");
		logger.info("agencyCode-"+agencyCode);
		Agency agency = contractService.getAgencyByAgencyCode(agencyCode);
		return agency;
	}

	/**
	 * To get Contracting Govt. by Agency Code
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getContractingGovernments", method = RequestMethod.POST)
	public @ResponseBody List<LritContractingGovtMst> getContractingGovernments(
			@RequestParam("agencyCode") String agencyCode) {
		logger.info("getContractingGovernments()");
		logger.info("agencyCode-"+agencyCode);
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
		List<LritContractingGovtMst> listCGInfo = lritAspInfoService.getAspCgListByAgencyCodeAndDdpVer(agencyCode,
				ddpVer);
		// listCGInfo.addAll(lritDatacentreInfoService.findCGDetails(agencyCode));
		// Above Line is replaced by below code for testing with new ddb db
		// ddpVer = "1891";
		listCGInfo.addAll(lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agencyCode, ddpVer));
		return listCGInfo;
	}

	/**
	 * To get DDP Based Contracting Govts.
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getDDPBasedCGs", method = RequestMethod.POST)
	public @ResponseBody List<LritContractingGovtMst> getContractingGovernmentsDDP(	
			@RequestParam("agencyCode") String lritNameDdpVerDtoString) {
		LritNameDdpVerDto lritNameDdpVerDto = contractService.getLritNameDdpVerDtofromAgencyCode(lritNameDdpVerDtoString);
		logger.info(lritNameDdpVerDto.toString());
		String agencyCode = lritNameDdpVerDto.getLritid();
		String ddpVer = lritNameDdpVerDto.getDdpVer();
		List<LritContractingGovtMst> listCGInfo = new ArrayList<LritContractingGovtMst>(); 
		if ( agencyCode.startsWith("4")) {
			List<LritContractingGovtMst> aspsCgList = lritAspInfoService.getAspCgListByAgencyCodeAndDdpVer(agencyCode,
					ddpVer);
			if ( aspsCgList != null )
				listCGInfo.addAll(aspsCgList); 
		}

		// Get regular version number implemented at when a particular immediate version number is implemented 
		else if ( agencyCode.startsWith("3")) {
			java.sql.Date date = lritDatacentreInfoService.getImmediateDdpVersionImplementationDate(ddpVer);
			logger.info("Immediate DDP Version Implementation Date: " + date);
			if ( date != null ) {
				
				String regularDdpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
				logger.info("Regular DDP Version Number: " + regularDdpVer);
				List<LritContractingGovtMst> datacentreCgList = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agencyCode, regularDdpVer);
				if ( datacentreCgList != null )
					listCGInfo.addAll(datacentreCgList);
			}
		}
		return listCGInfo;
	}

	/**
	 * To get Contracting Govts. by agency Code and Ddp Version
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getContractingGovernmentsByAgencyCodeAnDdpVersion", method = RequestMethod.POST)
	public @ResponseBody List<LritContractingGovtMst> getContractingGovernmentsByAgencyCodeAnDdpVersion(
			@RequestParam("agencyCode") String agencyCode, @RequestParam("ddpVer") String ddpVer) {
		List<LritContractingGovtMst> listCGInfo = lritAspInfoService.getAspCgListByAgencyCodeAndDdpVer(agencyCode,
				ddpVer);
		logger.info("getContractingGovernmentsByAgencyCodeAnDdpVersion()");
		listCGInfo.addAll(lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agencyCode, ddpVer));
		return listCGInfo;
	}

	/**
	 * To view the list of Contract GET
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/viewcontract", method = RequestMethod.GET)
	public ModelAndView view() {
		logger.info("view()");

		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("contractType", contractType);
		return model;
	}

	/**
	 * To view the Contract by Contract Id
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/viewContractByContractId/{id}", method = RequestMethod.GET)
	public ModelAndView viewContractByContractId(@PathVariable(name = "id") Integer contractId) {
		logger.info("viewContractByContractId()");
		logger.info("contractId-"+contractId);
		Contract contract = contractService.getContractByContractId(contractId);
		List<String> agency1ContractStakeholders = contractService.getAgency1ContractStakeholders(contractId);

		List<String> agency2ContractStakeholders = contractService.getAgency2ContractStakeholders(contractId);
		//java.sql.Date date = contract.getValidFrom();

		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		List<LritContractingGovtMst> listCGInfoAgency1D = contractService.getStakeholderByDate( agency1ContractStakeholders, date);
		List<LritContractingGovtMst> listCGInfoAgency2D = contractService.getStakeholderByDate( agency2ContractStakeholders, date);
		List<LritContractingGovtMst> listCGInfoAgency1 = contractService.getNonStakeholderByDate(contract.getAgency1().getAgencyCode(), agency1ContractStakeholders, date);
		List<LritContractingGovtMst> listCGInfoAgency2 = contractService.getNonStakeholderByDate(contract.getAgency2().getAgencyCode(), agency2ContractStakeholders, date);
		List<ContractAnnexure> contractAnnexures = contractService.getAllContractsAnnexureById(contractId);
		
		Boolean annexureEmpty;
		if(!contractAnnexures.isEmpty()) {
			annexureEmpty = true;
		}else
			annexureEmpty=false;


		/*
		 * If Contract is ASP/DC
		 */
		if (contract.getBillingContractType().getContractTypeId() == 1 && contract instanceof ContractAspDc) {
			logger.info("in if ContractAspDc");
			ModelAndView model = new ModelAndView("/billing/contract/viewaspcontract");
			model.addObject("contract",contract);
			model.addObject("agency1ContractStakeholders", listCGInfoAgency1D);
			model.addObject("agency2ContractStakeholders", listCGInfoAgency2D);
			model.addObject("agency1CG", listCGInfoAgency1);
			model.addObject("agency2CG", listCGInfoAgency2);
			model.addObject("contractAnnexures",contractAnnexures);
			model.addObject("annexureEmpty",annexureEmpty);

			return model;

			/*
			 * If Contract is USA
			 * 
			 */
		} else if (contract.getBillingContractType().getContractTypeId() == 2) {
			logger.info("in if USA");
			ModelAndView model = new ModelAndView("/billing/contract/viewusacontract");
			model.addObject("contract", contract);
			model.addObject("agency1ContractStakeholders", listCGInfoAgency1D);
			model.addObject("agency2ContractStakeholders", listCGInfoAgency2D);
			model.addObject("agency1CG", listCGInfoAgency1);
			model.addObject("agency2CG", listCGInfoAgency2);
			model.addObject("contractAnnexures",contractAnnexures);
			model.addObject("annexureEmpty",annexureEmpty);
			return model;

			/*
			 * If Contract is CSP
			 */

		} else if (contract.getBillingContractType().getContractTypeId() == 3) {
			logger.info("in if CSP");
			Agency agency2 = contractCSPService.getAgencyCSP();
			contract.setAgency2(agency2);
			ModelAndView model = new ModelAndView("/billing/contract/viewcspcontract");
			model.addObject("contract", contract);
			model.addObject("agency1ContractStakeholders", listCGInfoAgency1D);
			model.addObject("agency1CG", listCGInfoAgency1);
			model.addObject("contractAnnexures",contractAnnexures);
			model.addObject("annexureEmpty",annexureEmpty);
			return model;

		}
		else if (contract.getBillingContractType().getContractTypeId() == 4) {
			logger.info("in if ShippingCompany");
			ModelAndView model = new ModelAndView("/billing/contract/viewshippingcompanycontract");
			model.addObject("contract", contract);
			model.addObject("agency1ContractStakeholders", listCGInfoAgency1D);
			model.addObject("agency1CG", listCGInfoAgency1);
			model.addObject("contractAnnexures",contractAnnexures);
			model.addObject("annexureEmpty",annexureEmpty);
			return model;

		}
		return null;

	}

	/**
	 * To get Contracting Govts. of Agency
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getAgencyCGs") 
	@ResponseBody
	public List<LritContractingGovtMst> getAgencyCGs(@RequestParam("agencyCode") String agencyCode, @RequestParam("date") java.sql.Date date) {
		logger.info("AgencyCode"+agencyCode);
		logger.info("getAgencyCGs()");
		
		List<LritContractingGovtMst> cgList = new ArrayList<LritContractingGovtMst>();
		String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
		if ( agencyCode.startsWith("3")) { 

			List<LritContractingGovtMst> datacentreCgList = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agencyCode, ddpVer);
			if ( datacentreCgList != null )
				cgList.addAll(datacentreCgList); 
		} else if (agencyCode.startsWith("4")) {

			List<LritContractingGovtMst> aspCgList = lritAspInfoService.getAspCgListByAgencyCodeAndDdpVer(agencyCode, ddpVer);
			if ( aspCgList != null )
				cgList.addAll(aspCgList); 
		}

		return cgList;

	}
	
	
	/**
	 * To get All Contracting Govts 
	 * @author lrit-billing
	 * 
	 */
	@RequestMapping(value = "/getContractingGovts") 
	@ResponseBody
	public List<LritContractingGovtMst> getContractingGovts() {
		logger.info("getContractingGovts()");
		List<LritContractingGovtMst> contractingGovtList = contractService.getAllContractingGovts();
		return contractingGovtList;

	}
}
