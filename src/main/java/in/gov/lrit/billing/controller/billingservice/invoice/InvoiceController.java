/**
 * @InvoiceController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 07-Aug-2019
 */
package in.gov.lrit.billing.controller.billingservice.invoice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import in.gov.lrit.billing.dao.contract.agency.AgencyRepository;
import in.gov.lrit.billing.model.billingservice.ReminderLog;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.invoice.Invoice;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.service.ReminderLogService;
import in.gov.lrit.billing.service.ServiceService;
import in.gov.lrit.billing.service.ServiceStatusService;
import in.gov.lrit.billing.service.billingservice.invoice.InvoiceAbstractServiceImpl;
import in.gov.lrit.billing.service.billingservice.invoice.InvoiceService;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.validator.service.ServiceValidator;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in_.gov.lrit.billing.dto.InvoiceDto;
import net.sf.jasperreports.engine.JRException;

/**
 * @author Yogendra Yadav (YY)
 *
 */
//@Controller
//@RequestMapping(path = "/invoice")
public class InvoiceController {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(InvoiceController.class);

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private ReminderLogService reminderLogService;

	@Autowired
	private ContractService contractService;

	@Autowired
	@Qualifier("serviceServiceImpl")
	private ServiceService serviceService;

	@Autowired
	private ServiceValidator serviceValidator;

	@Autowired
	private AgencyRepository agencyRepository;

	@Autowired
	private InvoiceAbstractServiceImpl invoiceAbstractServiceImpl;

	@Autowired
	private ServiceStatusService serviceStatusService;

	@Value("${file.upload.size}")
	private long FILE_UPLOAD_SIZE;

	@Value("${file.upload.type}")
	private String PDF_TYPE;

	@RequestMapping(value = "/listInvoice", method = RequestMethod.GET)
	public String listInvoice(Model model) {

		logger.info("listInvoice()");
		model.addAttribute("contractTypes", contractService.getContractTypes());
		return "billing/invoice/listInvoice";
	}

	/**
	 * To populate invoice based on time period and contract Type.
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @return
	 */
//	@RequestMapping(value = "/listInvoice", method = RequestMethod.POST)
//	@ResponseBody
//	public Collection<InvoiceDto> listInvoiceJson(@RequestParam("contractType") Integer contractType,
//			@RequestParam(name = "fromDate", required = false) String fromDate,
//			@RequestParam(name = "toDate", required = false) String toDate) {
//		logger.info(contractType.toString());
//		logger.info(fromDate);
//		logger.info(toDate);
//		List<InvoiceDto> invoiceList = invoiceService.findAllInvoiceDto(contractType, fromDate, toDate);
//		return invoiceList;
//	}

	@RequestMapping(value = "/listInvoice", method = RequestMethod.POST)
	@ResponseBody
	public List<InvoiceDto> listInvoiceJson(@RequestParam("contractType") Integer contractType,
			@RequestParam(name = "fromDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
			@RequestParam(name = "toDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date toDate) {

		return invoiceAbstractServiceImpl.fetchInvoices(contractType, fromDate, toDate);
	}

	/**
	 * TO FILL INVOICE DETAILS
	 * 
	 * @author Yogendra Yadav
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addInvoice", method = RequestMethod.GET)
	public String addInvoice(Model model) {

//		Agency agency1 = agencyRepository.getOne("3065");
//		Agency agency2 = agencyRepository.findById("0029554").get();
//		ContractType contractType = contractService.getContractType(4);
//
//		ContractShippingCompany contractShippingCompany = new ContractShippingCompany();
//		contractShippingCompany.setContractNumber("Ship1");
//		contractShippingCompany.setAgency1(agency1);
//		contractShippingCompany.setAgency2(agency2);
//		contractShippingCompany.setBillingContractType(contractType);
//
//		ContractStakeholder contractStakeholder = new ContractStakeholder();
//		contractStakeholder.setIsagency1(true);
//		contractStakeholder.setMemberId("3065");
//		contractStakeholder.setBillingContract(contractShippingCompany);
//
//		ContractStakeholder contractStakeholder1 = new ContractStakeholder();
//		contractStakeholder1.setIsagency1(false);
//		contractStakeholder1.setMemberId("0029554");
//		contractStakeholder1.setBillingContract(contractShippingCompany);
//
//		List<ContractStakeholder> contractStakeholders = new ArrayList<>();
//		contractStakeholders.add(contractStakeholder);
//		contractStakeholders.add(contractStakeholder1);
//
//		contractShippingCompany.setContractStakeholders(contractStakeholders);

		// contractService.saveContract(contractShippingCompany);

		logger.info("addInvoice()");
		Invoice invoice = new Invoice();
		List<ContractType> contractTypes = contractService.getContractTypes();
		model.addAttribute("contractTypes", contractTypes);
//		model.addAttribute("agencys", serviceService.getAllAgency());
		model.addAttribute("agencys", serviceService.getAllAgency());
		model.addAttribute("service", invoice);
		return "billing/invoice/addInvoice";
	}

	/**
	 * TO SAVE THE INVOICE DETAILS IN DATABASE AND ALSO CONTRACTING GOVERNMENT
	 * DETAILS WITH MESSAGE COUNT AND COST
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @param model
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/addInvoice", method = RequestMethod.POST)
	public String saveInvoice(@Valid @ModelAttribute("service") Invoice invoice, BindingResult bindingResult,
			Model model) {
		logger.info("SaveInvoice()");
		model.addAttribute("contractTypes", contractService.getContractTypes());
		model.addAttribute("agencys", serviceService.getAllAgency());

//		if (contract instanceof ContractAspDc) {
//			ContractAspDc contractAspDc = (ContractAspDc) contractService
//					.getContract(invoice.getBillingContract().getContractId());
//			invoice.setBillingContract(contractAspDc);
//			logger.info(contractAspDc.getThresholdAmount() + " Thres ");
//			if (invoice.getSubTotal() < contractAspDc.getThresholdAmount()) {
//				bindingResult.rejectValue("subTotal", "service.threshold.amount");
//			}
//			logger.info("ASP DC Contract");
//		} else if (contract instanceof ContractShippingCompany) {
//			logger.info("SHipping Company");
//		}

		// logger.info(invoice.getFile().getContentType());
		serviceValidator.validate(invoice, bindingResult);

		if (bindingResult.hasErrors()) {
			logger.info(bindingResult.getAllErrors().toString());
			if (invoice.getBillingContract() != null) {
				Contract contract = contractService.getContract(invoice.getBillingContract().getContractId());
				invoice.setBillingContract(contract);
				List<Contract> contracts = new ArrayList<Contract>();
				contracts = contractService.getAgencyWiseContracts(
						invoice.getBillingContract().getAgency2().getAgencyCode(),
						invoice.getBillingContract().getBillingContractType().getContractTypeId(),
						invoice.getFromDate(), invoice.getToDate());
				model.addAttribute("contracts", contracts);
			}
			model.addAttribute("error", "Sorry!! Unable To Save Invoice");
		} else {

			// YEAR CAN BE CHANGED AS PER PERIOD OF MESSAGE COUNTS
			// invoice.setInvoiceNo(invoiceService.getInvoiceNumber(Calendar.getInstance().get(Calendar.YEAR)));
			// invoice = invoiceService.saveInvoiceService(invoice);
			try {
				invoice = invoiceAbstractServiceImpl.generateInvoice(invoice);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JRException e) {
				// TODO Auto-generated catch block
				logger.error("EXCEPTION DUE TO JRXML FILE OF INVOICE");
				model.addAttribute("error", "Sorry!! Unable To Save Invoice, Error in PDF GENERATION");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error("EXCEPTION DUE TO ByteArrayOutputStream");
				model.addAttribute("error", "Sorry!! Unable To Save Invoice");
				e.printStackTrace();
			}
		}
		model.addAttribute("service", invoice);
		return "billing/invoice/addInvoice";
	}

	/**
	 * TO FETCH THE INVOICE WHICH WAS NEEDED TO SEND TO DG
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoiceId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/sendInvoice/{invoiceId}", method = RequestMethod.GET)
	public String showSendInvoiceDG(@PathVariable("invoiceId") Integer invoiceId, Model model) {

		logger.info("showSendInvoiceDG()");
//		Invoice invoice = invoiceService.getInvoice(invoiceId);
		Invoice invoice = invoiceAbstractServiceImpl.getInvoice(invoiceId);
		model.addAttribute("invoice", invoice);
//		model.addAttribute("agencyName", serviceService.getAgencyName(invoice));
		return "billing/invoice/sendInvoiceDG";
	}

	@RequestMapping(value = "/sendInvoice/{invoiceId}", method = RequestMethod.POST)
	// @ResponseBody
	public String saveSendInvoiceDG(@PathVariable("invoiceId") Integer invoiceId,
			@ModelAttribute("invoice") Invoice invoice, BindingResult bindingResult, Model model) {
		logger.info("saveSendInvoiceDG()");
		logger.info(invoice.getBillingServiceStatuses().get(0).getRemarks());
		if (bindingResult.hasErrors()) {
			model.addAttribute("error", "Sorry!! Failed To Send");
		} else {
//			invoice = invoiceService.updateToDG(invoice, invoiceId);
			try {
				invoice = invoiceAbstractServiceImpl.sendInvoiceToDG(invoice, invoiceId);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (invoice != null)
				model.addAttribute("message", "Success!! Invoice Send To DG");
			else
				model.addAttribute("error", "Sorry!! Failed To Send");
		}

		return "billing/invoice/listInvoice";
		// return invoice;
	}

	/**
	 * PROVIDE INTERFACE TO UPLOAD SIGNED INVOICE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoiceId
	 * @param model
	 * @return
	 */
//	@RequestMapping(value = "/uploadSignedInvoice/{invoiceId}", method = RequestMethod.GET)
//	public String showUploadInvoice(@PathVariable("invoiceId") Integer invoiceId, Model model) {
//		logger.info("uploadSignedInvoice()");
//		Invoice invoice = new Invoice();
//		model.addAttribute("invoice", invoice);
//		return "billing/invoice/uploadSignedInvoice";
//	}
	@RequestMapping(value = "/uploadSignedInvoice/{invoiceId}", method = RequestMethod.GET)
	public String showUploadInvoice(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("uploadSignedInvoice()");
		ServiceStatus serviceStatus = new ServiceStatus();
		serviceStatus.setBillingService(new Invoice());
		model.addAttribute("serviceStatus", serviceStatus);
		return "billing/invoice/uploadSignedInvoice";
	}

	/**
	 * SAVING SIGNED UPLOADED INVOICE IN DATABASE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoiceId
	 * @param approvedCopyFile
	 * @param remarks
	 * @param model
	 * @return
	 * @throws IOException
	 */
//	@RequestMapping(value = "/uploadSignedInvoice/{invoiceId}", method = RequestMethod.POST)
//	public String saveUploadInvoice(@Valid @ModelAttribute("invoice") Invoice invoice, BindingResult bindingResult,
//			@PathVariable("invoiceId") Integer invoiceId,
//			@RequestParam("approvedCopyFile") MultipartFile approvedCopyFile, Model model) {
//		logger.info("saveUploadInvoice()");
//
//		final String PDF_TYPE = "application/pdf";
//		final long FIVE_MB_IN_BYTES = 5242880;
//
//		logger.info(approvedCopyFile.getContentType() + " ==> " + approvedCopyFile.getSize());
//
//		if (approvedCopyFile.isEmpty()) {
//			// logger.info("File Not Found");
//			bindingResult.rejectValue("approvedCopy", "upload.file.required");
//		} else if (!PDF_TYPE.equalsIgnoreCase(approvedCopyFile.getContentType())) {
//			// logger.info("PDF Not Found");
//			bindingResult.rejectValue("approvedCopy", "upload.invalid.file.type");
//		} else if (approvedCopyFile.getSize() > FIVE_MB_IN_BYTES) {
//			bindingResult.rejectValue("approvedCopy", "upload.exceeded.file.size");
//		}
//
//		if (bindingResult.hasErrors()) {
////			logger.info(bindingResult.getErrorCount() + " " + bindingResult.getFieldError("approvedCopy").getCode());
//			logger.info(bindingResult.getAllErrors().toString());
//			model.addAttribute("invoice", invoice);
//			model.addAttribute("error", "Sorry!! Failed To Upload");
//		} else {
//
//			byte[] approvedCopyByte = null;
//			try {
//				approvedCopyByte = approvedCopyFile.getBytes();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//
//			// invoice.setApprovedCopy(approvedCopyByte);
//			invoice = invoiceService.uploadApprovedInvoice(invoice, invoiceId);
////			Invoice invoice = invoiceService.uploadApprovedInvoice(remarks, approvedCopyByte, invoiceId);
//			model.addAttribute("message", "Success!! Signed Invoice has been Uploaded");
//		}
//
//		return "billing/invoice/uploadSignedInvoice";
//	}
	@RequestMapping(value = "/uploadSignedInvoice/{invoiceId}", method = RequestMethod.POST)
	public String saveUploadInvoice(@Valid @ModelAttribute("serviceStatus") ServiceStatus serviceStatus,
			BindingResult bindingResult, @PathVariable("invoiceId") Integer invoiceId,
			@RequestParam("approvedCopyFile") MultipartFile approvedCopyFile, Model model) {

		logger.info("saveUploadInvoice()");

		// final String PDF_TYPE = "application/pdf";
		// final long FIVE_MB_IN_BYTES = 5242880;

		logger.info(approvedCopyFile.getContentType() + " ==> " + approvedCopyFile.getSize());

//		Invoice invoice = invoiceService.getInvoice(invoiceId);
		Invoice invoice = invoiceAbstractServiceImpl.getInvoice(invoiceId);
		serviceStatus.setBillingService(invoice);

		if (approvedCopyFile.isEmpty()) {
//			logger.info("File Not Found");
			bindingResult.rejectValue("billingService.approvedCopy", "upload.file.required");
		} else if (!PDF_TYPE.equalsIgnoreCase(approvedCopyFile.getContentType())) {
//			logger.info("PDF Not Found");
			bindingResult.rejectValue("billingService.approvedCopy", "upload.invalid.file.type");
		} else if (approvedCopyFile.getSize() > FILE_UPLOAD_SIZE) {
			bindingResult.rejectValue("billingService.approvedCopy", "upload.exceeded.file.size");
		}

		if (bindingResult.hasErrors()) {
			model.addAttribute("error", "Sorry!! Failed To Upload");
		} else {

//			try {
//				// TO SAVE APPROVED INVOICE IN DATABASE
//				invoice.setApprovedCopy(approvedCopyFile.getBytes());
//			} catch (IOException e) {
//				e.printStackTrace();
//				model.addAttribute("serviceStatus", serviceStatus);
//				return "billing/invoice/uploadSignedInvoice";
//			}

			serviceStatus = serviceStatusService.saveUploadedInvoice(serviceStatus);

			model.addAttribute("message", "Success!! Signed Invoice has been Uploaded");
		}

		model.addAttribute("serviceStatus", serviceStatus);
		return "billing/invoice/uploadSignedInvoice";
	}

	@RequestMapping(value = "/mailInvoice/{invoiceId}", method = RequestMethod.GET)
	public String showInvoiceMail(@PathVariable("invoiceId") Integer invoiceId, Model model) {

		logger.info("mailInvoice()");

		return "billing/invoice/mailInvoice";
	}

	@RequestMapping(value = "/mailInvoice/{invoiceId}", method = RequestMethod.POST)
	public String sendInvoiceMail(@PathVariable("invoiceId") Integer invoiceId,
			@RequestParam("mailBody") String mailBody, @RequestParam("remarks") String remarks, Model model) {

//		invoiceService.sendMail(mailBody, remarks, invoiceId);
		try {
			invoiceAbstractServiceImpl.mailInvoiceToAgency(mailBody, remarks, invoiceId);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "billing/invoice/mailInvoice";
	}

	// UC-Billing-17
	/**
	 * TO GET VIEW OF UPDATE COUNT PAGE WITH DATA TO BE FILLED ACCORDING TO THE
	 * INVOICE NUMBER
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoiceId
	 * @param model
	 * @return
	 * @return
	 */
	@RequestMapping(path = "/updateCount/{invoiceId}", method = RequestMethod.GET)
	public String showUpdateCount(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("showUpdateCount()");

//		Invoice invoice = invoiceService.getInvoice(invoiceId);
		Invoice invoice = invoiceAbstractServiceImpl.getInvoice(invoiceId);
		invoice.getBillingBillableItems().forEach((billableItem) -> {
			billableItem.setProviderName(serviceService.getCGName(billableItem.getProvider()));
			billableItem.setConsumerName(serviceService.getNameContractTypeBased(billableItem.getConsumer(),
					invoice.getBillingContract().getBillingContractType().getContractTypeId()));
		});

		model.addAttribute("invoice", invoice);
		model.addAttribute("hashedBillableItems", serviceService.getCGHashedBillableItems(invoiceId));
		// model.addAttribute("agencyName", serviceService.getAgencyName(invoice));
		return "billing/invoice/updateCount";
	}

	// UC-Billing-17
	/**
	 * TO SAVE THE UPDATED DATA OF INVOICE FOR MESSAGE COUNT
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoiceId
	 * @param remarks
	 * @param invoice
	 * @param model
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(path = "/updateCount/{invoiceId}", method = RequestMethod.POST)
	public String saveUpdateCount(@PathVariable("invoiceId") Integer invoiceId,
			@ModelAttribute("invoice") Invoice invoice, Model model, BindingResult bindingResult) {
		logger.info("saveUpdateCount()");
		invoice = invoiceService.updateMessageCount(invoice, invoiceId);
		if (invoice != null)
			model.addAttribute("message", "Success!! Message Count has been Updated");
		else
			model.addAttribute("error", "Sorry!! Failed To Update Message Count");
		return "billing/invoice/updateCount";
	}

	@RequestMapping(path = "/updatePayment/{invoiceId}", method = RequestMethod.GET)
	public String updatePaymentGET(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("updatePaymentGET()");

		Invoice invoice = invoiceService.getInvoice(invoiceId);

		model.addAttribute("invoice", invoice);

		// TO SET CONSUMER NAME OF CG OR VESSEL INSTEAD OF LRITID OR VESSELID FETECHED
		// FOR PAID CONSUMERS FROM DATABASE
		invoice.getBillingPaymentDetailsForServices().forEach((paymentService) -> {
			paymentService.getBillingPaymentDetailsPerMembers().forEach((paymentMember) -> {
				paymentMember.setConsumerName(serviceService.getNameContractTypeBased(paymentMember.getConsumer(),
						invoice.getBillingContract().getBillingContractType().getContractTypeId()));
			});
		});

		// TO SET CONSUMER NAME OF CG OR VESSEL INSTEAD OF LRITID OR VESSELID FETECHED
		// PENDING CONSUMERS
		HashMap<String, PaymentDetailsPerMember> unCheckedMembers = serviceService.getUncheckedMembers(invoice);
		unCheckedMembers.forEach((key, value) -> {
			value.setConsumerName(serviceService.getNameContractTypeBased(value.getConsumer(),
					invoice.getBillingContract().getBillingContractType().getContractTypeId()));
			unCheckedMembers.put(key, value);
		});

		model.addAttribute("unCheckedMembers", unCheckedMembers);
		model.addAttribute("remainedAmount", serviceService.getRemainingAmountService(invoice));
//		model.addAttribute("agencyName", serviceService.getAgencyName(invoice));

		return "billing/invoice/updatePayment";
	}

	@RequestMapping(path = "/updatePayment/{invoiceId}", method = RequestMethod.POST)
	public String updatePaymentPOST(@PathVariable("invoiceId") Integer invoiceId,
			@RequestParam(name = "consumers", required = false) List<String> consumers,
			@ModelAttribute("invoice") Invoice invoice, Model model, BindingResult bindingResult) {

		logger.info("updatePaymentPOST()");

		if (consumers == null) {
			invoice = invoiceService.getInvoice(invoiceId);
			model.addAttribute("error", "Sorry!! Payment Failed, Select At least One Consumers");
		} else {
			invoice = invoiceService.updatePayment(invoice, consumers, invoiceId);
			if (invoice != null)
				model.addAttribute("message", "Success!! Payment has been Made");
			else
				model.addAttribute("error", "Sorry!! Payment Failed");
		}

		model.addAttribute("invoice", invoice);
		model.addAttribute("unCheckedMembers", serviceService.getUncheckedMembers(invoice));
		model.addAttribute("remainedAmount", serviceService.getRemainingAmountService(invoice));

		return "billing/invoice/updatePayment";
	}

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param model
	 * @return
	 */
	// For Shipping Company Invoices
	@RequestMapping(value = "/addShippingInvoice", method = RequestMethod.GET)
	public String showShippingInvoice(Model model) {
		logger.info("addShippingInvoice()");
		Invoice invoice = new Invoice();
		model.addAttribute("invoice", invoice);
		return "billing/invoice/addShippingInvoice";
	}

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param invoice
	 * @param invoiceBinding
	 * @param model
	 * @return
	 */
	// For Shipping Company Invoices
	@RequestMapping(value = "/addShippingInvoice", method = RequestMethod.POST)
	public String saveShippingInvoice(@ModelAttribute("invoice") Invoice invoice, BindingResult invoiceBinding,
			Model model) {
		logger.info("saveShippingInvoice()");
		invoice.setInvoiceNo(invoiceService.getInvoiceNumber(Calendar.getInstance().get(Calendar.YEAR)));
		invoice = invoiceService.saveShippingInvoiceService(invoice);
		if (invoice != null)
			model.addAttribute("message", "Success!! Invoice Has Been Generated");
		else
			model.addAttribute("error", "Sorry!! Unable To Save Invoice");
		model.addAttribute("invoice", invoice);
		return "billing/invoice/addShippingInvoice";
	}

	/*************************************************************************
	 * This Shipping company is depenended on the Portal DAtabase or POJO CLASS
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @return
	 */
	@RequestMapping(value = "/getShippingCompany", method = RequestMethod.GET)
	@ResponseBody
	public List<PortalShippingCompany> getShippingCompany() {
		return serviceService.getShippingCompanys();
	}

	@RequestMapping(path = "/getContractingGovernment", method = RequestMethod.GET)
	@ResponseBody
	public Collection<BillableItem> getContractingGovernment(@RequestParam("contractID") Integer contractID) {
		logger.info(contractID.toString());
		return new ArrayList<BillableItem>();
	}

	// REMINDER WORKFLOW
	@RequestMapping(path = "/sendReminder/{billingServiceId}", method = RequestMethod.GET)
	// @ResponseBody
	public String showSendReminder(@PathVariable("billingServiceId") Integer billingServiceId, Model model) {

		Invoice billingService = invoiceService.getInvoice(billingServiceId);

		ReminderLog reminderLog = new ReminderLog();
		reminderLog.setBillingService(billingService);
		model.addAttribute("reminderLog", reminderLog);

		// return reminderLog;
		return "billing/sendreminder";
	}

	@RequestMapping(path = "/sendReminder/{billingServiceId}", method = RequestMethod.POST)
	public String saveSendReminder(@PathVariable("billingServiceId") Integer billingServiceId,
			@ModelAttribute ReminderLog reminderLog, Model model) {
		Invoice billingService = invoiceService.getInvoice(billingServiceId);
		reminderLog.setBillingService(billingService);
		model.addAttribute("reminderLog", reminderLog);
		reminderLog = reminderLogService.save(reminderLog);
		if (reminderLog != null)
			model.addAttribute("message", "Success!! Reminder for Invoice has been Send");
		else
			model.addAttribute("error", "Sorry!! Failed To Send Reminder");

		return "billing/sendreminder";
	}

}
