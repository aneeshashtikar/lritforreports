
package in.gov.lrit.billing.controller.contract;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import java.sql.Date;

import in.gov.lrit.billing.controller.billingservice.invoice.InvoiceAbstractController;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAnnexure;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.CurrencyType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.service.contract.ContractAspDcService;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.contract.ContractTypeService;
import in.gov.lrit.billing.validator.contract.ContractValidator;
import in.gov.lrit.billing.validator.contract.MultipartFileValidator;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.service.LritAspInfoService;
import in.gov.lrit.ddp.service.LritDatacentreInfoService;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Controller to handle ContractAspDc related views
 * 
 * @author lrit-billing
 * 
 */
///lritbilling//
@Controller
@RequestMapping("/aspcontract")
public class ContractAspDcController {
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractAspDcController.class);
	
	private static final long SerialVersionUID = 10l; 
	
	@Autowired
	private ContractAspDcService contractAspDcService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private LritDatacentreInfoService lritDatacentreInfoService;

	@Autowired
	private ContractValidator contractValidator;

	
	@Autowired
	private LritAspInfoService lritAspInfoService;

	@Autowired
	private ContractTypeService contractTypeService;

	@Value("${dc.india.lritid}")
	private String agency1Code;

	/**
	 * For Asp-Dc Contract GET Method
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/aspcontract", method = RequestMethod.GET)
	public ModelAndView aspdcContract() {
		
		logger.info("aspdcContract()");
		
		ModelAndView model = new ModelAndView("/billing/contract/aspcontract");
		ContractAspDc contractAspDc = new ContractAspDc();
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		List<LritNameDdpVerDto> lritNameDdpVerDtos = contractService.getLritNameDdpVerDtoForAgency2(date);
		logger.info("Agency1Code-"+agency1Code);
		Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
		String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date); 
		logger.info("ddpVer"+ddpVer);
		List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
		List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
		model.addObject("currencyTypes",currencyTypes);
		logger.info("currencyTypes"+currencyTypes);
		List<LritContractingGovtMst> contractingGovtList = contractService.getAllContractingGovts();
		contractAspDc.setAgency1(agency1);
		
		model.addObject("contract", contractAspDc);
		model.addObject("contractingGovts", contractingGovtList);
		model.addObject("agency1CG", agency1CG);
		model.addObject("lritNameDtos", lritNameDdpVerDtos);
		return model;
	}
	
	/**
	 * For Save Asp-Dc Contract POST Method
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/aspcontract", method = RequestMethod.POST)
	public ModelAndView saveAspDc(@Valid @ModelAttribute("contract") ContractAspDc contractAspDc,
			BindingResult bindingResult,
			@RequestParam(name = "validFrom", required = false) Date validFrom,
			@RequestParam(name = "validTo", required = false) Date validTo,
			@RequestParam(name = "contractingGov1", required = false) List<String> contractingGov1,
			@RequestParam(name = "contractingGov2", required = false) Set<String> contractingGov2,
			@RequestParam(name = "contract_document") MultipartFile contractDoc) throws IOException {
		
		logger.info("saveAspDc()");
		logger.info("ContractNo-"+contractAspDc.getContractNumber());
		logger.info("validFrom-"+validFrom);
		logger.info("validTo-"+validTo);
		logger.info("contractingGov1-"+contractingGov1);
		logger.info("contractingGov2-"+contractingGov2);
		logger.info("contractAspDc"+contractAspDc.getBillingCurrencyType().getCurrencyType());
		contractAspDc.setValidFrom(validFrom);
		contractAspDc.setValidTo(validTo);
		
		byte[] contractDocument = contractDoc.getBytes();
		contractAspDc.setContractDocument(contractDocument);
		contractValidator.validate(contractAspDc, bindingResult);
		String contentType = contractDoc.getContentType();
		String pdf = "application/pdf";
		if(contractingGov1 == null) {
			bindingResult.rejectValue("agency1", "cgList1");
		}
		if(contractingGov2 == null) {
			bindingResult.rejectValue("agency2", "cgList1");
		}

		if (!contractDoc.isEmpty()) {
			if (!contentType.equals(pdf)) {
				bindingResult.rejectValue("contractDocument", "contractDocument1");
			}
		} else {
			bindingResult.rejectValue("contractDocument", "contractDocument");

		}
		
		if (bindingResult.hasErrors()) {
			logger.info("In bindingResult.hasError()");
			
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
			List<LritNameDdpVerDto> lritNameDdpVerDtos = contractService.getLritNameDdpVerDtoForAgency2(date);
			
			
			Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
			logger.info("agency1Name-"+agency1.getName());
			String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
			List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
			
			ModelAndView model = new ModelAndView("/billing/contract/aspcontract");
			List<LritContractingGovtMst> contractingGovtList = contractService.getAllContractingGovts();
			model.addObject("contractingGovts", contractingGovtList);
			contractAspDc.setAgency1(agency1);
			model.addObject("contract", contractAspDc);
			model.addObject("agency1CG", agency1CG);
			model.addObject("lritNameDtos", lritNameDdpVerDtos);
			model.addObject("error", "Something is missing...");
			model.addObject("contractingGov1", contractingGov1);
			return model;
		}
		logger.info("Out bindingResult.hasError()");
		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		for (int i = 0; i < contractingGov1.size(); i++) {
			ContractStakeholder contractStakeholder = new ContractStakeholder();
			contractStakeholder.setMemberId(contractingGov1.get(i));
			contractStakeholder.setIsagency1(true);
			contractStakeholder.setValidFrom(contractAspDc.getValidFrom());
			contractStakeholder.setValidTo(contractAspDc.getValidTo());
			contractStakeholders.add(contractStakeholder);
		}		
		for(String cgs : contractingGov2) {
			ContractStakeholder contractStakeholder = new ContractStakeholder();
			contractStakeholder.setMemberId(cgs);
			contractStakeholder.setIsagency1(false);
			contractStakeholder.setValidFrom(contractAspDc.getValidFrom());
			contractStakeholder.setValidTo(contractAspDc.getValidTo());
			contractStakeholders.add(contractStakeholder);
		}
		ContractType billingContractType = contractTypeService.findByContractId(1);
		contractAspDc.getAgency2().setContractType(billingContractType);
		contractAspDc.setContractStakeholders(contractStakeholders);
		contractAspDcService.save(contractAspDc);
		logger.info("ContractAspDc Saved");		
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("message", "Contract is added succesfully.");
		model.addObject("contractType", contractType);
		return model;
	}

	/**
	 * For Update Asp-Dc Contract Values or Extend the Contract
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateaspcontract", method = RequestMethod.POST)
	public ModelAndView updateAspDc(@Valid @ModelAttribute("contract") ContractAspDc contractAspDc,
			BindingResult bindingResult,
			@RequestParam(name = "contractingGov1", required = false) List<String> contractingGov1,
			@RequestParam(name = "contractingGov2", required = false) List<String> contractingGov2,
			@RequestParam(name = "contract_annexure") MultipartFile contractAnnex,
			@RequestParam(name = "remarks",required = false) String remarks,
			@RequestParam(name = "CG",required = false) List<String> contractGovs,
			@RequestParam(name = "fromDate",required = false) List<Date> fromDates,
			@RequestParam(name = "toDate",required = false) List<Date> toDates,
			@RequestParam(name = "agency1ContractGovValidFrom",required = false) List<String> agency1ContractGovValidFrom,
			@RequestParam(name = "agency1ContractGovValidTo",required = false) List<String> agency1ContractGovValidTo,
			@RequestParam(name = "agency2ContractGovValidFrom",required = false) List<String> agency2ContractGovValidFrom,
			@RequestParam(name = "agency2ContractGovValidTo",required = false) List<String> agency2ContractGovValidTo,
			@RequestParam(name = "contractAnnexureDate",required = false) String contractAnnexureDate) throws IOException, ParseException {
		logger.info("updateAspDc()");
		logger.info("ContractNo-"+contractAspDc.getContractNumber());
		logger.info("contractingGov1-"+contractingGov1);
		logger.info("contractingGov2-"+contractingGov2);
		logger.info("CG-"+contractGovs);
		logger.info("fromDate-"+fromDates);
		logger.info("toDate-"+toDates);
		logger.info("agency1ContractGovValidFrom"+agency1ContractGovValidFrom);
		logger.info("agency1ContractGovValidTo"+agency1ContractGovValidTo);
		logger.info("agency2ContractGovValidFrom"+agency2ContractGovValidFrom);
		logger.info("agency2ContractGovValidTo"+agency2ContractGovValidTo);
		
		contractValidator.validate(contractAspDc, bindingResult);
		if (contractingGov1 == null) {
			bindingResult.rejectValue("agency1", "cgList1");
		}

		if (contractingGov2 == null) {
			bindingResult.rejectValue("agency2", "cgList1");
		}
		
		if (bindingResult.hasErrors()) {
			logger.info("bindingResult.hasError()");
			ModelAndView model = new ModelAndView("/billing/contract/updateaspcontract");
			Integer contractId = contractAspDc.getContractId();
			Contract contract = contractService.getContractByContractId(contractId);
			List<String> agency1ContractStakeholders = contractService.getAgency1ContractStakeholders(contractId);
			List<String> agency2ContractStakeholders = contractService.getAgency2ContractStakeholders(contractId);
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
			List<LritContractingGovtMst> listCGInfoAgency1D = contractService.getStakeholderByDate(agency1ContractStakeholders, date);
			List<LritContractingGovtMst> listCGInfoAgency2D = contractService.getStakeholderByDate( agency2ContractStakeholders, date);
			List<LritContractingGovtMst> listCGInfoAgency1 = contractService.getNonStakeholderByDate(contract.getAgency1().getAgencyCode(), agency1ContractStakeholders, date);
			List<LritContractingGovtMst> listCGInfoAgency2 = contractService.getNonStakeholderByDate(contract.getAgency2().getAgencyCode(), agency2ContractStakeholders, date);

			logger.info("Agency 2 Contract Stakeholders "+listCGInfoAgency2D);

			List<ContractStakeholder> contractStakeholders1 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1D) {
				String lritId = lritContractingGovt.getCgLritid();
				ContractStakeholder contractStakeholder = contractService.getContractStakeholder(contractId, lritId);
				contractStakeholder.setName(lritContractingGovt.getCgName());
				contractStakeholders1.add(contractStakeholder);
			}
			logger.info(contractStakeholders1.toString());

			List<ContractStakeholder> contractNonStakeholders1 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1) {
				ContractStakeholder contractStakeholder = new ContractStakeholder(); 
				String lritId = lritContractingGovt.getCgLritid();
				String name = lritContractingGovt.getCgName(); 
				contractStakeholder.setName(name);
				contractStakeholder.setMemberId(lritId);
				contractNonStakeholders1.add(contractStakeholder);
			}
			logger.info(contractNonStakeholders1.toString());

			List<ContractStakeholder> contractNonStakeholders2 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency2) {
				ContractStakeholder contractStakeholder = new ContractStakeholder(); 
				String lritId = lritContractingGovt.getCgLritid();
				String name = lritContractingGovt.getCgName(); 
				contractStakeholder.setName(name);
				contractStakeholder.setMemberId(lritId);
				contractNonStakeholders2.add(contractStakeholder);
			}
			logger.info(contractNonStakeholders2.toString());

			List<ContractStakeholder> contractStakeholders2 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency2D) {
				String lritId = lritContractingGovt.getCgLritid();
				ContractStakeholder contractStakeholder = contractService.getContractStakeholder(contractId, lritId);
				contractStakeholder.setName(lritContractingGovt.getCgName());
				contractStakeholders2.add(contractStakeholder);
			}
			model.addObject("contract", contract);	
			model.addObject("error", "Contract with Same dates is already in system");
			model.addObject("agency1ContractStakeholders", contractStakeholders1);
			model.addObject("agency2ContractStakeholders", contractStakeholders2);
			model.addObject("agency1CG", contractNonStakeholders1);
			model.addObject("agency2CG", contractNonStakeholders2);
			return model;
		} 
		
		contractService.updateContract(contractAspDc, contractingGov1, contractingGov2, contractAnnex, remarks,contractAnnexureDate,contractGovs,fromDates,
										toDates,agency1ContractGovValidFrom,agency1ContractGovValidTo,agency2ContractGovValidFrom,agency2ContractGovValidTo);
		logger.info("After Update");
		
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("message", "Contract is updated succesfully.");
		model.addObject("contractType", contractType);
		return model;
	}
	
	/**
	 * For get Asp-Dc by Date
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping("/getAspDcsByDate")
	@ResponseBody
	public List<LritNameDdpVerDto> getAspDcByLritIDAndDate(@RequestParam("date") Date date) {
		List<LritNameDdpVerDto> lritNameDdpVerDtos = contractAspDcService.getAspDcsByDate(date);
		return lritNameDdpVerDtos; 
	}

}
