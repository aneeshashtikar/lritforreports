/**
 * @InvoiceAbstractController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 01-Nov-2019
 */
package in.gov.lrit.billing.controller.billingservice.invoice;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import in.gov.lrit.billing.dao.payment.PaymentDetailsForServiceRepository;
import in.gov.lrit.billing.model.billingservice.ReminderLog;
import in.gov.lrit.billing.model.billingservice.ServiceStatus;
import in.gov.lrit.billing.model.billingservice.Status;
import in.gov.lrit.billing.model.billingservice.invoice.ApprovedInvoice;
import in.gov.lrit.billing.model.billingservice.invoice.GeneratedInvoice;
import in.gov.lrit.billing.model.billingservice.invoice.Invoice;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.payment.PaymentDetailsForService;
import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.service.ReminderLogService;
import in.gov.lrit.billing.service.ServiceStatusService;
import in.gov.lrit.billing.service.billingservice.invoice.InvoiceAbstractService;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.payment.PaymentDetailsForServiceService;
import in.gov.lrit.billing.validator.service.ServiceValidator;
import in_.gov.lrit.billing.dto.InvoiceDto;
import in_.gov.lrit.ddp.dto.LritNameDto;
import net.sf.jasperreports.engine.JRException;

/**
 * @author lrit-billing
 *
 */
@Controller
@RequestMapping(path = "/invoice")
public class InvoiceAbstractController {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(InvoiceAbstractController.class);

	@Autowired
	private ReminderLogService reminderLogService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private ServiceValidator serviceValidator;

	@Autowired
	private PaymentDetailsForServiceService paymentDetailsForServiceService;
	
	@Autowired
	private InvoiceAbstractService invoiceAbstractService;

	@Autowired
	private ServiceStatusService serviceStatusService;

	@Value("${file.upload.size}")
	private long FILE_UPLOAD_SIZE;

	@Value("${file.upload.type}")
	private String PDF_TYPE;

	/**
	 * RETURN PDF AS RESPONSE
	 * 
	 * @author lrit-billing
	 * @param serviceId
	 * @param response
	 * @throws IOException
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/uploaded/{serviceId}", method = RequestMethod.GET)
	public void getFile(@PathVariable("serviceId") Integer serviceId, HttpServletResponse response) throws IOException {
		logger.info("INSIDE getFile()");
		logger.info("billingServiceId : " + serviceId);

		Invoice invoice = invoiceAbstractService.getInvoice(serviceId);

		try {
			GeneratedInvoice latestGeneratedInvoice = null;
			latestGeneratedInvoice = invoiceAbstractService.getLatestGeneratedInvoice(serviceId);
			if (latestGeneratedInvoice != null) {
				byte[] generatedCopy = latestGeneratedInvoice.getGeneratedCopy();

				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", String.format("attachment;filename=\"Invoice_Document_From_\\"
						+ invoice.getFromDate() + "\\To_" + invoice.getToDate() + ".pdf" + ""));

				BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
				fos1.write(generatedCopy);
				fos1.flush();
				fos1.close();
			}
		} catch (Exception e) {
			logger.error("EXCEPTION GET GENERATED FILE : ");
			e.printStackTrace();
		}
	}

	/**
	 * RETURN PDF OF GENERATED INVOICE
	 * 
	 * @author lrit-billing
	 * @param generatedInvoiceId
	 * @param response
	 * @throws IOException
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/generatedInvoice/{generatedInvoiceId}", method = RequestMethod.GET)
	public void getGeneratedInvoicePDF(@PathVariable("generatedInvoiceId") Integer generatedInvoiceId,
			HttpServletResponse response) {
		logger.info("INSIDE getGeneratedInvoicePDF()");
		logger.info("serviceId : " + generatedInvoiceId);

		try {
			GeneratedInvoice generatedInvoice = null;
			generatedInvoice = invoiceAbstractService.fetchGeneratedInvoice(generatedInvoiceId);
			if (generatedInvoice != null) {
				Invoice invoice = invoiceAbstractService
						.getInvoice(generatedInvoice.getService().getBillingServiceId());
				byte[] generatedCopy = generatedInvoice.getGeneratedCopy();

				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", String.format("attachment;filename=\"Invoice_Document_From_\\"
						+ invoice.getFromDate() + "\\To_" + invoice.getToDate() + ".pdf" + ""));

				BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
				fos1.write(generatedCopy);
				fos1.flush();
				fos1.close();
			}
		} catch (IOException e) {
			logger.error("EXCEPTION getGeneratedInvoicePDF()");
			logger.error("EXCEPTION WHILE BUFFEREDOUTPUTSTREAM");
			logger.error("IOException  : " + e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("EXCEPTION  : " + e);
		}
	}

	/**
	 * RETURN PDF FORMAT OF APPROVED INVOICE
	 * 
	 * @author lrit-billing
	 * @param approvedInvoiceId
	 * @param response
	 * @throws IOException
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/approvedInvoice/{approvedInvoiceId}", method = RequestMethod.GET)
	public void getApprovedInvoicePDF(@PathVariable("approvedInvoiceId") Integer approvedInvoiceId,
			HttpServletResponse response) {
		logger.info("INSIDE getGeneratedInvoicePDF()");
		logger.info("serviceId : " + approvedInvoiceId);

		try {
			ApprovedInvoice approvedInvoice = null;
			approvedInvoice = invoiceAbstractService.fetchApprovedInvoice(approvedInvoiceId);
			if (approvedInvoice != null) {
				Invoice invoice = invoiceAbstractService.getInvoice(approvedInvoice.getService().getBillingServiceId());
				byte[] approvedCopy = approvedInvoice.getApprovedCopy();

				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", String.format("attachment;filename=\"Invoice_Document_From_\\"
						+ invoice.getFromDate() + "\\To_" + invoice.getToDate() + ".pdf" + ""));

				BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
				fos1.write(approvedCopy);
				fos1.flush();
				fos1.close();
			}
		} catch (IOException e) {
			logger.error("EXCEPTION getApprovedInvoicePDF()");
			logger.error("EXCEPTION WHILE BUFFEREDOUTPUTSTREAM");
			logger.error("IOException  : " + e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("EXCEPTION : " + e);
			e.printStackTrace();
		}
	}

	/**
	 * VIEW OF INVOICE LISTING RETURNS listinvoice page
	 * 
	 * @author lrit-billing
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/listInvoice", method = RequestMethod.GET)
	public String listInvoice(Model model) {

		logger.info("INSIDE listInvoice() GET");

		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}
		model.addAttribute("contractTypes", contractTypes);

		logger.info("Returning listInvoice.jsp page");
		return "billing/invoice/listInvoice";
	}

	/**
	 * To populate invoice based on time period and contract Type.
	 * 
	 * @author lrit-billing
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/listInvoice", method = RequestMethod.POST)
	@ResponseBody
	public List<InvoiceDto> listInvoiceJson(@RequestParam("contractType") Integer contractType,
			@RequestParam(name = "fromDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date fromDate,
			@RequestParam(name = "toDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date toDate) {

		logger.info("INSIDE listInvoiceJson()");
		logger.info("contractType : " + contractType);
		logger.info("fromDate : " + fromDate);
		logger.info("toDate : " + toDate);

		List<InvoiceDto> fetchedInvoices = invoiceAbstractService.fetchInvoices(contractType, fromDate, toDate);
		return fetchedInvoices;
	}

	/**
	 * THIS RETURN addinvoice page
	 * 
	 * @author lrit-billing
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/addInvoice", method = RequestMethod.GET)
	public String addInvoice(Model model) {

		logger.info("INSIDE addInvoice()");

		Invoice invoice = new Invoice();
		List<ContractType> contractTypes = contractService.getContractTypes();

		// TO AVOID CSP INVOICE GENERATION
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}

		List<LritNameDto> agencys = contractService.getAgencyContractTypeWise(1);
		model.addAttribute("contractTypes", contractTypes);
		model.addAttribute("agencys", agencys);
		model.addAttribute("service", invoice);

		logger.info("Returning addInvoice.jsp page");
		return "billing/invoice/addInvoice";

	}

	/**
	 * TO SAVE THE INVOICE DETAILS IN DATABASE AND ALSO CONTRACTING GOVERNMENT
	 * DETAILS WITH MESSAGE COUNT AND COST
	 * 
	 * 
	 * @author lrit-billing
	 * @param invoice
	 * @param model
	 * @param result
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/addInvoice", method = RequestMethod.POST)
	public String saveInvoice(@Valid @ModelAttribute("service") Invoice invoice, BindingResult bindingResult,
			Model model) {
		logger.info("INSIDE saveInvoice()");
		logger.info("INVOICE : " + invoice);

		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}
		model.addAttribute("contractTypes", contractTypes);

		serviceValidator.validate(invoice, bindingResult);

		if (bindingResult.hasErrors()) {
			logger.error("BINDING RESULTS : " + bindingResult.getAllErrors().toString());

			if (invoice.getBillingContract() != null) {
				Contract contract = contractService.getContract(invoice.getBillingContract().getContractId());
				invoice.setBillingContract(contract);

				// FOLLOWING REQUIRED TO FETCH ALL CONTRACT FALLS ON THAT CATEGORY
				String agencyCode = contract.getAgency2().getAgencyCode();
				Integer contractTypeId = contract.getBillingContractType().getContractTypeId();
				Date fromDate = invoice.getFromDate();
				Date toDate = invoice.getToDate();

				List<Contract> contracts = contractService.getAgencyWiseContracts(agencyCode, contractTypeId, fromDate,
						toDate);
				if (contracts == null)
					logger.error("ERROR WHILE FETCHING CONTRACTS");

				List<LritNameDto> agencys = contractService.getAgencyContractTypeWise(contractTypeId);
				if (agencys == null)
					logger.error("ERROR WHILE FETCHING AGENCYS");

				model.addAttribute("agencys", agencys);
				model.addAttribute("contracts", contracts);
			}
			model.addAttribute("error", "Unable To Save Invoice");
			model.addAttribute("service", invoice);

			logger.info("Returning addInvoice.jsp page");
			return "billing/invoice/addInvoice";
		} else {
			logger.info("NO ERRORS READY TO PERSIST : ");
			try {
				invoice = invoiceAbstractService.generateInvoice(invoice);
				model.addAttribute("message", "Success!! Invoice Has Been Generated");
			} catch (SQLException e) {
				logger.error("EXCEPTION OCCURED PROVIDING DATABASE CONNECTIVITY TO JASPER COMPILER");
				model.addAttribute("error", "Unable To Save Invoice DATABASE CONNECTIVITY TO JASPER COMPILER");
				e.printStackTrace();
			} catch (JRException e) {
				logger.error("EXCEPTION DUE JASPER REPORT, JRXML FILE OF INVOICE");
				model.addAttribute("error", "Unable To Save Invoice, JRXML FILE OF INVOICE");
				e.printStackTrace();
			} catch (IOException e) {
				logger.error("EXCEPTION OCCURED IN PDF ,DUE TO ByteArrayOutputStream");
				model.addAttribute("error", "Unable To Save Invoice,DUE TO ByteArrayOutputStream");
				e.printStackTrace();
			} catch (ConstraintViolationException e) {
				logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
				model.addAttribute("error",
						"Unable To Save Invoice, Database Constraint failed Contact Database Administrator");
				e.printStackTrace();
			} catch (Exception e) {
				logger.error("EXCEPTION OCCURED IN INVOICE" + e);
				model.addAttribute("error", "Unable To Save Invoice " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Returning listInvoice.jsp page");
			return "billing/invoice/listInvoice";
		}
	}

	/**
	 * TO GET WHOLE VIEW OF INVOICE return viewInvoice.jsp
	 *
	 * @author lrit-billing
	 * @param invoiceId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/viewInvoice/{invoiceId}", method = RequestMethod.GET)
	public String viewInvoice(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("INSIDE saveInvoice()");
		logger.info("invoiceId : " + invoiceId);

		Invoice invoice = invoiceAbstractService.getInvoice(invoiceId);

		// TO SET NAME OF COUNTRY INSTEAD OF LRIT ID
		invoice = (Invoice) invoiceAbstractService.setProviderConsumerName(invoice);

		// TO SET PAYMENT MEMBER NAME
		invoice = (Invoice) invoiceAbstractService.setPaymentMembersName(invoice);

		List<GeneratedInvoice> generatedInvoices = invoiceAbstractService.fetchAllGeneratedInvoicePDF(invoiceId);
		List<ApprovedInvoice> approvedInvoices = invoiceAbstractService.fetchAllApprovedInvoicePDF(invoiceId);

		model.addAttribute("invoice", invoice);
		model.addAttribute("generatedInvoices", generatedInvoices);
		model.addAttribute("approvedInvoices", approvedInvoices);

		logger.info("Returning viewInvoice.jsp page");
		return "billing/invoice/viewInvoice";
	}

	/**
	 * TO FETCH THE INVOICE WHICH WAS NEEDED TO SEND TO DG
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/sendInvoice/{invoiceId}", method = RequestMethod.GET)
	public String showSendInvoiceDG(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("INSIDE showSendInvoiceDG()");
		logger.info("invoiceId : " + invoiceId);

		// TO AVOID URL SCRAMBLING STATUS BASED ACTION PERFORMED
		Status status = invoiceAbstractService.getLatestStatus(invoiceId);
		if (status.getStatusId() != 2 && status.getStatusId() != 1) {
			logger.info("status : " + status);

			List<ContractType> contractTypes = contractService.getContractTypes();
			for (int index = 0; index < contractTypes.size(); index++) {
				if (contractTypes.get(index).getContractTypeId() == 3)
					contractTypes.remove(index);
			}
			model.addAttribute("contractTypes", contractTypes);
			model.addAttribute("error", "Invalid Action performed on Invoice!!");

			logger.info("INVOICE ACTION MISS MATCH");
			logger.info("Returning listInvoice.jsp page");
			return "billing/invoice/listInvoice";
		}

		Invoice invoice = invoiceAbstractService.getInvoice(invoiceId);

		model.addAttribute("invoice", invoice);
		model.addAttribute("billingServiceID", invoiceId);
		// return "billing/invoice/sendInvoiceDG";

		logger.info("Returning sendInvoicePdfDG.jsp page");
		return "billing/invoice/sendInvoicePdfDG";
	}

	/**
	 * SEND MAIL TO DG AND SAVE INVOICE STATE
	 *
	 * @author lrit-billing
	 * @param invoiceId
	 * @param invoice
	 * @param bindingResult
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/sendInvoice/{invoiceId}", method = RequestMethod.POST)
	public String saveSendInvoiceDG(@PathVariable("invoiceId") Integer invoiceId,
			@ModelAttribute("invoice") Invoice invoice, BindingResult bindingResult, Model model) {
		logger.info("INSIDE saveSendInvoiceDG()");
		logger.info("invoiceId : " + invoiceId);
		logger.info("invoice : " + invoice);

		logger.info(invoice.getBillingServiceStatuses().get(0).getRemarks());
		if (bindingResult.hasErrors()) {
			logger.error("BINDING ERRORS : " + bindingResult.getAllErrors());
			model.addAttribute("error", "Failed To Send");
		} else {
			logger.info("NO ERRORS");
			try {
				invoice = invoiceAbstractService.sendInvoiceToDG(invoice, invoiceId);
				model.addAttribute("message", "Success!! Invoice Sent To DG");
			} catch (MessagingException e) {
				logger.error("MESSAGING EXCEPTION OCCURED");
				logger.error("PROBLEM WHILE SENDING MAIL TO DG" + e);
				model.addAttribute("error", "Failed To Send MessagingException");
				e.printStackTrace();
			} catch (ConstraintViolationException e) {
				logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
				model.addAttribute("error", "Database Constraint failed Contact Database Administrator");
				e.printStackTrace();
			} catch (Exception e) {
				logger.error("EXCEPTION OCCURED");
				logger.error("PROBLEM WHILE SENDING MAIL TO DG");
				model.addAttribute("error", "Failed To Send Exception");
				e.printStackTrace();
			}
		}

		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}
		model.addAttribute("contractTypes", contractTypes);

		logger.info("Returning listInvoice.jsp page");
		return "billing/invoice/listInvoice";
	}

	/**
	 * PROVIDE INTERFACE TO UPLOAD SIGNED INVOICE
	 * 
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/uploadSignedInvoice/{invoiceId}", method = RequestMethod.GET)
	public String showUploadInvoice(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("INSIDE  uploadSignedInvoice()");

		Status status = invoiceAbstractService.getLatestStatus(invoiceId);
		if (status.getStatusId() != 3) {
			logger.info("status : " + status);

			List<ContractType> contractTypes = contractService.getContractTypes();
			for (int index = 0; index < contractTypes.size(); index++) {
				if (contractTypes.get(index).getContractTypeId() == 3)
					contractTypes.remove(index);
			}
			model.addAttribute("contractTypes", contractTypes);
			model.addAttribute("error", "Invalid Action performed on invoice!!");

			logger.info("INVOICE ACTION MISS MATCH");
			logger.info("Returning listInvoice.jsp page");
			return "billing/invoice/listInvoice";
		}

		ServiceStatus serviceStatus = new ServiceStatus();
		serviceStatus.setBillingService(new Invoice());

		/* ApprovedInvoice approvedInvoice = new ApprovedInvoice(); */
		model.addAttribute("serviceStatus", serviceStatus);

		logger.info("Returning uploadSignedInvoice.jsp page");
		return "billing/invoice/uploadSignedInvoice";
	}

	/**
	 * SAVING SIGNED UPLOADED INVOICE IN DATABASE
	 * 
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @param approvedCopyFile
	 * @param remarks
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/uploadSignedInvoice/{invoiceId}", method = RequestMethod.POST)
	public String saveUploadInvoice(@Valid @ModelAttribute("serviceStatus") ServiceStatus serviceStatus,
			BindingResult bindingResult, @PathVariable("invoiceId") Integer invoiceId,
			@RequestParam("approvedCopyFile") MultipartFile approvedCopyFile, Model model) {
		logger.info("INSIDE saveUploadInvoice()");

		Invoice invoice = invoiceAbstractService.getInvoice(invoiceId);
		serviceStatus.setBillingService(invoice);

		ApprovedInvoice approvedInvoice = new ApprovedInvoice();
		approvedInvoice.setService(invoice);

		if (approvedCopyFile.isEmpty()) {
			// Error path has been changed from billingService.approvedCopy to date of
			// Service status as approvedCopy attribute has been removed from Invoice
			bindingResult.rejectValue("date", "upload.file.required");
		} else if (!PDF_TYPE.equalsIgnoreCase(approvedCopyFile.getContentType())) {
			bindingResult.rejectValue("date", "upload.invalid.file.type");
		} else if (approvedCopyFile.getSize() > FILE_UPLOAD_SIZE) {
			bindingResult.rejectValue("date", "upload.exceeded.file.size");
		}

		if (bindingResult.hasErrors()) {
			logger.error("BINDING ERRORS : " + bindingResult.getAllErrors().toString());
			model.addAttribute("error", "Failed To Upload");
			model.addAttribute("serviceStatus", serviceStatus);

			logger.info("Returning uploadSignedInvoice.jsp page");
			return "billing/invoice/uploadSignedInvoice";
		} else {
			try {
				byte[] approvedCopyBytes = approvedCopyFile.getBytes();
				approvedInvoice.setApprovedCopy(approvedCopyBytes);
				serviceStatus = serviceStatusService.saveUploadedInvoice(serviceStatus, approvedInvoice);
				model.addAttribute("message", "Success!! Signed Invoice has been Uploaded");
			} catch (ConstraintViolationException e) {
				logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
				model.addAttribute("error", "Database Constraint failed Contact Database Administrator");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error("IOEXCEPTION OCCURED");
				logger.error("EXCEPTION DUE TO UPLOAD SIGNED INVOICE PDF");

				model.addAttribute("error", "Failed To Upload");
				model.addAttribute("serviceStatus", serviceStatus);
				logger.info("Returning uploadSignedInvoice.jsp page");
				e.printStackTrace();
			}
		}
		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}
		model.addAttribute("contractTypes", contractTypes);

		logger.info("Returning listInvoice.jsp page");
		return "billing/invoice/listInvoice";
	}

	/**
	 * SEND MAIL TO AGENCY RETURNS mailInvoice.jsp
	 *
	 * @author lrit-billing
	 * @param invoiceId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/mailInvoice/{invoiceId}", method = RequestMethod.GET)
	public String showInvoiceMail(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("INSIDE mailInvoice()");

		Status status = invoiceAbstractService.getLatestStatus(invoiceId);
		if (status.getStatusId() != 4) {
			logger.info("status : " + status);

			List<ContractType> contractTypes = contractService.getContractTypes();
			for (int index = 0; index < contractTypes.size(); index++) {
				if (contractTypes.get(index).getContractTypeId() == 3)
					contractTypes.remove(index);
			}
			model.addAttribute("contractTypes", contractTypes);
			model.addAttribute("error", "Invalid Action performed on invoice!!");

			logger.info("INVOICE ACTION MISS MATCH");
			logger.info("Returning listInvoice.jsp page");
			return "billing/invoice/listInvoice";
		}

		logger.info("Returning mailInvoice.jsp page");
		return "billing/invoice/mailInvoice";
	}

	/**
	 * SEND MAIL TO AGENCYS
	 *
	 * @author lrit-billing
	 * @param invoiceId
	 * @param mailBody
	 * @param remarks
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/mailInvoice/{invoiceId}", method = RequestMethod.POST)
	public String sendInvoiceMail(@PathVariable("invoiceId") Integer invoiceId,
			@RequestParam("mailBody") String mailBody, @RequestParam("remarks") String remarks,
			@RequestParam("ccEmailId") String ccEmailId, Model model) {
		logger.info("INSIDE sendInvoiceMail()");
		logger.info("invoiceId : " + invoiceId);
		logger.info("mailBody : " + mailBody);
		logger.info("remarks : " + remarks);
		logger.info("ccEmailId : " + ccEmailId);

		String errorMessage = "";

		try {
			if (ccEmailId.equals("") || ccEmailId == null)
				invoiceAbstractService.mailInvoiceToAgency(mailBody, remarks, invoiceId);
			else
				invoiceAbstractService.mailInvoiceToAgency(mailBody, remarks, invoiceId, ccEmailId);

			model.addAttribute("message", "Success!! Mail has been Sent");
		} catch (MessagingException e) {
			logger.error("MESSAGING EXCEPTION OCCURED");
			logger.error("PROBLEM WHILE SENDING MAIL TO AGENCY" + e);
			model.addAttribute("error", "Failed!! Problem in sending mail to AGencys MessagingException");
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
			model.addAttribute("error", "Database Constraint failed Contact Database Administrator");
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("EXCEPTION OCCURED");
			logger.error("PROBLEM WHILE SENDING MAIL TO DG" + e);
			model.addAttribute("error", "Failed!! Problem in sending mail to AGencys Exception");
			e.printStackTrace();
		}

		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}
		model.addAttribute("contractTypes", contractTypes);

		if (!"".equals(errorMessage.trim()))
			model.addAttribute("error", errorMessage);

		logger.info("Returning listInvoice.jsp page");
		return "billing/invoice/listInvoice";
	}

	/**
	 * TO GET VIEW OF UPDATE COUNT PAGE WITH DATA TO BE FILLED ACCORDING TO THE
	 * INVOICE NUMBER
	 * 
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @param model
	 * @return
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(path = "/updateCount/{invoiceId}", method = RequestMethod.GET)
	public String showUpdateCount(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("showUpdateCount()");

		Status status = invoiceAbstractService.getLatestStatus(invoiceId);
		if (status.getStatusId() != 5) {
			logger.info("status : " + status);

			List<ContractType> contractTypes = contractService.getContractTypes();
			for (int index = 0; index < contractTypes.size(); index++) {
				if (contractTypes.get(index).getContractTypeId() == 3)
					contractTypes.remove(index);
			}
			model.addAttribute("contractTypes", contractTypes);
			model.addAttribute("error", "Invalid Action performed on invoice!!");

			logger.info("INVOICE ACTION MISS MATCH");
			logger.info("Returning listInvoice.jsp page");
			return "billing/invoice/listInvoice";
		}

		Invoice invoice = invoiceAbstractService.getInvoice(invoiceId);
		invoice = (Invoice) invoiceAbstractService.setProviderConsumerName(invoice);

		model.addAttribute("invoice", invoice);
		// model.addAttribute("hashedBillableItems",
		// invoiceAbstractService.getHashedBillableItems(invoiceId));
		logger.info("Billable Item ID " + invoice.getBillingBillableItems().get(0).getBillableItemId());

		logger.info("Returning updateCount.jsp page");
		return "billing/invoice/updateCount";
	}

	/**
	 * TO SAVE THE UPDATED DATA OF INVOICE FOR MESSAGE COUNT
	 * 
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @param remarks
	 * @param invoice
	 * @param model
	 * @param bindingResult
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(path = "/updateCount/{invoiceId}", method = RequestMethod.POST)
	public String saveUpdateCount(@PathVariable("invoiceId") Integer invoiceId,
			@ModelAttribute("invoice") Invoice invoice, Model model, BindingResult bindingResult) {
		logger.info("saveUpdateCount()");
		logger.info("invoiceId : " + invoiceId);
		logger.info("invoice : " + invoice);

//		String error = "";
//		String message = "";
		logger.info("Billable Item ID " + invoice.getBillingBillableItems().get(0).getBillableItemId());
//		Invoice updatedInvoice = null;
		try {
			Invoice updatedInvoice = (Invoice) invoiceAbstractService.updateMessageCount(invoice, invoiceId);
			model.addAttribute("message", "Success!! Message Count has been Updated.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error("EXCEPTION OCCURED PROVIDING DATABASE CONNECTIVITY TO JASPER COMPILER");
			model.addAttribute("error", "Unable To Update Count DATABASE CONNECTIVITY TO JASPER COMPILER");
			e.printStackTrace();
		} catch (JRException e) {
			// TODO Auto-generated catch block
			logger.error("EXCEPTION DUE JASPER REPORT, JRXML FILE OF INVOICE");
			model.addAttribute("error", "Unable To Update Count, JRXML FILE OF INVOICE");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("EXCEPTION OCCURED IN PDF ,DUE TO ByteArrayOutputStream");
			model.addAttribute("error", " Unable To Update Count,DUE TO ByteArrayOutputStream");
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
			model.addAttribute("error", "Database Constraint failed Contact Database Administrator");
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("EXCEPTION OCCURED IN INVOICE" + e.getMessage());
			model.addAttribute("error", "Unable To Update Count " + e.getMessage());
			e.printStackTrace();
		}
//		if (updatedInvoice != null)
//			message += "Success!! Message Count has been Updated.";
//		else
//			error += "Failed To Update Message Count.";
//		model.addAttribute("error", "Failed To Update Message Count.");
//
//		if (generatedInvoice != null)
//			message += "New invoice with updated message count has been generated";
//		else
//			error += "Failed To Generate Invoice and Save it.";
//		model.addAttribute("message", message);
//		model.addAttribute("error", error);

//		updatedInvoice = (Invoice) invoiceAbstractService.setProviderConsumerName(updatedInvoice);
//
//		model.addAttribute("invoice", updatedInvoice);
		// model.addAttribute("hashedBillableItems",
		// invoiceAbstractService.getHashedBillableItems(invoiceId));
		// System.out.println("Received service service ID " +
		// invoice.getBillingServiceId());
		// System.out.println("Billable Item ID " +
		// invoice.getBillingBillableItems().get(0).getBillableItemId());
		// return "billing/invoice/updateCount";
		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}
		model.addAttribute("contractTypes", contractTypes);

		logger.info("Returning listInvoice.jsp page");
		return "billing/invoice/listInvoice";
	}

	/**
	 * UPDATE PAYMENT VIEW page updatePayment.jsp
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(path = "/updatePayment/{invoiceId}", method = RequestMethod.GET)
	public String updatePaymentGET(@PathVariable("invoiceId") Integer invoiceId, Model model) {
		logger.info("updatePaymentGET()");

		Status status = invoiceAbstractService.getLatestStatus(invoiceId);
		if (status.getStatusId() != 5 && status.getStatusId() != 6 && status.getStatusId() != 7) {
			logger.info("status : " + status);

			List<ContractType> contractTypes = contractService.getContractTypes();
			for (int index = 0; index < contractTypes.size(); index++) {
				if (contractTypes.get(index).getContractTypeId() == 3)
					contractTypes.remove(index);
			}
			model.addAttribute("contractTypes", contractTypes);
			model.addAttribute("error", "Invalid Action performed on invoice!!");

			logger.info("INVOICE ACTION MISS MATCH");
			logger.info("Returning listInvoice.jsp page");
			return "billing/invoice/listInvoice";
		}

		Invoice invoice = invoiceAbstractService.getInvoice(invoiceId);

		model.addAttribute("invoice", invoice);

		// TO SET CONSUMER NAME OF CG OR VESSEL INSTEAD OF LRITID OR VESSELID FETECHED
		// FOR PAID CONSUMERS FROM DATABASE
		invoice = (Invoice) invoiceAbstractService.setPaymentConsumerName(invoice);

		// TO SET CONSUMER NAME OF CG OR VESSEL INSTEAD OF LRITID OR VESSELID FETECHED
		// PENDING CONSUMERS
		HashMap<String, PaymentDetailsPerMember> unCheckedMembers = invoiceAbstractService.getUncheckedMembers(invoice);

		model.addAttribute("unCheckedMembers", unCheckedMembers);
		model.addAttribute("remainedAmount", invoiceAbstractService.getRemainingAmountService(invoice));

		logger.info("Returning updatePayment.jsp page");
		return "billing/invoice/updatePayment";
	}

	/**
	 * SAVE UPDATED PAYMENT INFORMATION
	 * 
	 * @author lrit-billing
	 * @param invoiceFromUser
	 * @param bindingResult
	 * @param invoiceId
	 * @param consumers
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(path = "/updatePayment/{invoiceId}", method = RequestMethod.POST)
	public String updatePaymentPOST(@Valid @ModelAttribute("invoice") Invoice invoiceFromUser,
			BindingResult bindingResult, @PathVariable("invoiceId") Integer invoiceId,
			@RequestParam(name = "consumers", required = false) List<String> consumers,
			@RequestParam(name="paymentDate") String paymentDate,
			@RequestParam(name="paymentAdviceDocument") MultipartFile paymentAdviceDocument, Model model) {

		logger.info("INSIDE updatePaymentPOST()");
		logger.info("Invoice ID : " + invoiceId);
		logger.info("List of Consumers : " + consumers);

		Invoice fetchedInvoice = invoiceAbstractService.getInvoice(invoiceId);
		fetchedInvoice = (Invoice) invoiceAbstractService.setPaymentConsumerName(fetchedInvoice);
		
		Double remainingAmount = invoiceAbstractService.getRemainingAmountService(fetchedInvoice);
		Double paidAmount = invoiceFromUser.getBillingPaymentDetailsForServices().get(0).getPaidAmount();
		byte[] paymentDocument = getFileByte(paymentAdviceDocument);
		logger.info("paidAmount : " + paidAmount);
		logger.info("remainingAMount : " + remainingAmount);

		String errorMessage = "";

		if (consumers != null) {
			logger.info("IF CONSUMERS is selected");
			Double selectedAgencyAmount = 0.0;

			PaymentDetailsForService paymentDetailsForService = invoiceFromUser.getBillingPaymentDetailsForServices()
					.get(0);

			for (String consumer : consumers) {
				for (PaymentDetailsPerMember paymentDetailsPerMember : paymentDetailsForService
						.getBillingPaymentDetailsPerMembers()) {
					if (consumer.equals(paymentDetailsPerMember.getConsumer())) {
						selectedAgencyAmount += paymentDetailsPerMember.getCost();
					}
				}
			}

			if (selectedAgencyAmount > paidAmount) {
				bindingResult.rejectValue("billingPaymentDetailsForServices[0].paidAmount", "upload.invalid.file.type");
				logger.error("PAID AMOUNT LESS THAN SELECTED AGENCY");
				errorMessage = "PAID AMOUNT LESS THAN SELECTED AGENCY";
			}
		} else {
			if (paidAmount <= 0) {
				bindingResult.rejectValue("grandTotal", "upload.invalid.file.type");
				logger.error("PAID AMOUNT SHOULD NOT BE ZERO");
				errorMessage = "PAID AMOUNT SHOULD NOT BE ZERO";
			}
		}

		if (paidAmount > remainingAmount) {
			bindingResult.rejectValue("grandTotal", "upload.invalid.file.type");
			logger.error("PAID AMOUNT GREATER THAN REMAINIG AMOUNT");
			errorMessage = "PAID AMOUNT GREATER THAN REMAINIG AMOUNT";
		}

		if (bindingResult.hasErrors()) {
			logger.error("BINDING RESULT ERROR : " + bindingResult.getAllErrors().toString());

			model.addAttribute("error", errorMessage);
			// TO SET CONSUMER NAME OF CG OR VESSEL INSTEAD OF LRITID OR VESSELID FETECHED
			// FOR PAID CONSUMERS FROM DATABASE
			model.addAttribute("invoice", fetchedInvoice);
			model.addAttribute("unCheckedMembers", invoiceAbstractService.getUncheckedMembers(fetchedInvoice));
			model.addAttribute("remainedAmount", invoiceAbstractService.getRemainingAmountService(fetchedInvoice));

			logger.info("Returning updatePayment.jsp page");
			return "billing/invoice/updatePayment";

		} else {
			logger.info("No Error Persisiting Invoice Payment");
			try {
				invoiceFromUser = (Invoice) invoiceAbstractService.updatePayment(invoiceFromUser, consumers, invoiceId,paymentDocument,paymentDate);
				if (invoiceFromUser != null) {
					invoiceFromUser = invoiceAbstractService.getInvoice(invoiceId);
					model.addAttribute("message", "Success!! Payment has been Made");
				} else
					model.addAttribute("error", "Payment Failed");
			} catch (ConstraintViolationException e) {
				logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
				model.addAttribute("error", "Database Constraint failed Contact Database Administrator");
				e.printStackTrace();
			} catch (Exception e) {
				logger.error("EXCEPTION IN UPDATE PAYMENT : " + e);
				e.printStackTrace();
				model.addAttribute("error", "Payment Failed");
			}
		}

		if (!"".equals(errorMessage.trim())) {
			model.addAttribute("error", errorMessage);
		}

		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}
		model.addAttribute("contractTypes", contractTypes);

		logger.info("Returning listInvoice.jsp page");
		return "billing/invoice/listInvoice";
	}

	/**
	 * SEND REMINDER VIEW PAGE sendreminder.jsp
	 * 
	 * @author lrit-billing
	 * @param invoiceId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(path = "/sendReminder/{billingServiceId}", method = RequestMethod.GET)
	public String showSendReminder(@PathVariable("billingServiceId") Integer invoiceId, Model model) {
		logger.info("INSIDE showSendReminder");
		logger.info("invoiceId : " + invoiceId);

		Status status = invoiceAbstractService.getLatestStatus(invoiceId);
		if (status.getStatusId() != 6 && status.getStatusId() != 7 && status.getStatusId() != 5) {
			logger.info("status : " + status);

			List<ContractType> contractTypes = contractService.getContractTypes();
			for (int index = 0; index < contractTypes.size(); index++) {
				if (contractTypes.get(index).getContractTypeId() == 3)
					contractTypes.remove(index);
			}
			model.addAttribute("contractTypes", contractTypes);
			model.addAttribute("error", "Invalid Action performed on invoice!!");

			logger.info("INVOICE ACTION MISS MATCH");
			logger.info("Returning listInvoice.jsp page");
			return "billing/invoice/listInvoice";
		}

		ReminderLog reminderLog = new ReminderLog();
		reminderLog.setBillingService(invoiceAbstractService.getInvoice(invoiceId));
		model.addAttribute("reminderLog", reminderLog);
		model.addAttribute("remainedAmount",
				invoiceAbstractService.getRemainingAmountService(invoiceAbstractService.getInvoice(invoiceId)));

		logger.info("Returning sendreminder.jsp page");
		return "billing/sendreminder";
	}

	/**
	 * TO SEND REMINDER
	 * 
	 * @author lrit-billing
	 * @param billingServiceId
	 * @param reminderLog
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(path = "/sendReminder/{billingServiceId}", method = RequestMethod.POST)
	public String saveSendReminder(@PathVariable("billingServiceId") Integer billingServiceId,
			@ModelAttribute ReminderLog reminderLog, Model model) {

		String errorMessage = "";
		Invoice fetchedInvoice = invoiceAbstractService.getInvoice(billingServiceId);

		try {
			if (null != fetchedInvoice) {
				reminderLog.setBillingService(fetchedInvoice);
				reminderLog = reminderLogService.save(reminderLog);

				String mailBody = "Invoice No : " + fetchedInvoice.getInvoiceNo() + " <br>" + "Remaining Amount : "
						+ invoiceAbstractService.getRemainingAmountService(fetchedInvoice) + "<br>"
						+ "Invoice Ranges From : " + fetchedInvoice.getFromDate() + " Till : "
						+ fetchedInvoice.getToDate() + "<br>";

				HashMap<String, PaymentDetailsPerMember> hashMap = invoiceAbstractService
						.getUncheckedMembers(fetchedInvoice);

				hashMap.forEach((consumerName, members) -> {
					logger.info(consumerName);
				});

				invoiceAbstractService.mailInvoiceToAgency(mailBody, reminderLog.getRemarks(), billingServiceId);

				if (reminderLog != null)
					model.addAttribute("message", "Success!! Reminder for Invoice has been sent");
				else
					model.addAttribute("error", "Failed To Send Reminder");
			}
		} catch (MessagingException e) {
			logger.error("MESSAGING EXCEPTION OCCURED");
			logger.error("PROBLEM WHILE SENDING REMINDER TO AGENCY" + e);
			model.addAttribute("error", "Failed!! Problem in sending mail to AGencys MessagingException");
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
			e.printStackTrace();
			model.addAttribute("error", "Database Constraint failed Contact Database Administrator");
		} catch (Exception e) {
			logger.error("SAVING REMINDER EXCEPTION : " + e.toString());
			e.printStackTrace();
			model.addAttribute("error", "Failed To Send Reminder");
		}

//		model.addAttribute("reminderLog", reminderLog);

		List<ContractType> contractTypes = contractService.getContractTypes();
		for (int index = 0; index < contractTypes.size(); index++) {
			if (contractTypes.get(index).getContractTypeId() == 3)
				contractTypes.remove(index);
		}
		model.addAttribute("contractTypes", contractTypes);
		if (!"".equals(errorMessage.trim()))
			model.addAttribute("error", errorMessage);

		logger.info("Returning listInvoice.jsp page");
		return "billing/invoice/listInvoice";
	}

	/**
	 * RETURNS MESSAGE TYPE BASED COUNT FOR EACH CONTRACTING GOVERNMENT
	 * 
	 * 
	 * @author lrit-billing
	 * @param contractId
	 * @param fromDate
	 * @param toDate
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getBillableItems")
	@ResponseBody
	public HashMap<String, HashMap<String, List<BillableItem>>> getBillableItems(
			@RequestParam("contractId") Integer contractId,
			@RequestParam("fromDate") /* @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) */ java.sql.Date fromDate,
			@RequestParam("toDate") /* @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) */ java.sql.Date toDate,
			Model model) {
		logger.info("INSIDE getBillableItems");
		logger.info("contractId : " + contractId);
		logger.info("fromDate : " + fromDate);
		logger.info("toDate : " + toDate);

		logger.info("Returning getHashedBillableItems JSON DATA");
		HashMap<String, HashMap<String, List<BillableItem>>> hashedBillableItems = invoiceAbstractService
				.getHashedBillableItems(contractId, fromDate, toDate);

		return hashedBillableItems;
	}
	
	/**  
	 * to convert document in byte[]
	 * @author lrit-billing
	 */
	public byte[] getFileByte(MultipartFile billdoc) {
		byte[] document = null;
		try {
			document = billdoc.getBytes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return document;
	}
	
	/**  
	 * to get payment advice document
	 * @author lrit-billing
	 */

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/adviceDocument/{pmtDetServiceId}", method = RequestMethod.GET)
	public void getAdviceDocument(@PathVariable("pmtDetServiceId") Integer pmtDetServiceId, HttpServletResponse response) throws IOException {
		logger.info("getFile()");
		response.setContentType("application/pdf");
		PaymentDetailsForService paymentDetailsForService= paymentDetailsForServiceService.getPaymentDetailsByPaymentServiceId(pmtDetServiceId);
		response.setHeader("Content-Disposition", String.format("attachment;filename=\"Payment_Advice_Document_\\"
				+ paymentDetailsForService.getPaymentDate()+".pdf" + ""));
		BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
		fos1.write(paymentDetailsForService.getPaymentAdviceDocument());
		fos1.flush();
		fos1.close();
	}

}
