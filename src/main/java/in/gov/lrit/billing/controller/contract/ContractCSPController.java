
package in.gov.lrit.billing.controller.contract;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAnnexure;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.ContractShippingCompany;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.CurrencyType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.service.contract.ContractCSPService;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.contract.ContractTypeService;
import in.gov.lrit.billing.validator.contract.ContractValidator;
import in.gov.lrit.ddp.model.LritAspinfo;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.model.LritDatacentreInfo;
import in.gov.lrit.ddp.service.LritAspInfoService;
import in.gov.lrit.ddp.service.LritDatacentreInfoService;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Controller to handle ContractCSP related views
 * 
 * @author lrit-billing
 * 
 */
@Controller
@RequestMapping("/cspcontract")
public class ContractCSPController {

	// logger
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractCSPController.class);
	
	@Autowired
	private ContractCSPService contractCSPService;

	@Autowired
	private ContractService contractService;
	
	@Autowired
	private LritDatacentreInfoService lritDatacentreInfoService; 
	
	@Autowired
	private LritAspInfoService lritAspInfoService;
	
	@Autowired
	private ContractValidator contractValidator;
	
	@Value("${dc.india.lritid}")
	private String agency1Code;
	
	@Autowired
	private ContractTypeService contractTypeService;

	/**
	 * To get CSP Contract GET method
	 * @author lrit-billing
	 * 
	 */
	
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/cspcontract",method = RequestMethod.GET) 
	public ModelAndView cspContract() {
		logger.info("cspContract()");
		ModelAndView model = new ModelAndView("/billing/contract/cspcontract");
		
		ContractCSP contractCSP = new ContractCSP();
		Agency agency2 = contractCSPService.getAgencyCSP();
		contractCSP.setAgency2(agency2); 
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		
		Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
		logger.info("agency1Code " + agency1Code);
		//String ddpVer = lritDatacentreInfoService.getApplicableImmediateDdpVersionByDate(date);
		String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
		logger.info("ddpVer " + ddpVer);
		List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
		logger.info("agency1CG " + agency1CG);
		List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
		logger.info("currency"+currencyTypes.toString());
		logger.info("currency"+currencyTypes.size());
		List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
		for (int index = 0; index < currencyTypes.size(); index++) {
			CurrencyType newCurrencyType = currencyTypes.get(index);
			if (currencyTypes.get(index).getCurrencyType().equals("RS"))
				newCurrencyTypes.add(newCurrencyType);
		}
		model.addObject("currencyTypes",newCurrencyTypes);
		contractCSP.setAgency1(agency1);
		model.addObject("contract", contractCSP);
		model.addObject("agency1CG",agency1CG);
		return model; 
	}
	
	/**
	 * To Save CSP Contract 
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/cspcontract", method = RequestMethod.POST)
	public ModelAndView saveCSP(@Valid @ModelAttribute("contract") ContractCSP contractCSP,
			BindingResult bindingResult,
			@RequestParam(name = "contractingGov1", required = false) List<String> contractingGov1,
			@RequestParam(name = "contract_document") MultipartFile contractDoc) throws IOException {
		logger.info("saveCSP()");
		
		logger.info("contractCspNo"+contractCSP.getContractNumber());
		logger.info("contractingGov1-"+contractingGov1);
		logger.info("contractCSP"+contractCSP.getBillingCurrencyType().getCurrencyType());
		byte [] contractDocument=contractDoc.getBytes();
		contractCSP.setContractDocument(contractDocument);
		String contentType = contractDoc.getContentType();
		String pdf = "application/pdf";
		if(contractingGov1 == null) {
			bindingResult.rejectValue("agency1", "cgList1");
		}
		
		if(!contractDoc.isEmpty()){            
			if(!contentType.equals(pdf)) {
				bindingResult.rejectValue("contractDocument", "contractDocument1");
			}
		} else {
			bindingResult.rejectValue("contractDocument", "contractDocument");
		}
		contractValidator.validate(contractCSP,bindingResult);
		
		if(bindingResult.hasErrors()) {
			logger.info("bindingResult.hasErrors()");
			Agency agency2 = contractCSPService.getAgencyCSP();
			contractCSP.setAgency2(agency2); 
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		
			Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
			//String ddpVer = lritDatacentreInfoService.getApplicableImmediateDdpVersionByDate(date);
			String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
			List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
			contractCSP.setAgency1(agency1);
			ModelAndView model = new ModelAndView("/billing/contract/cspcontract");
			List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
			List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
			for (int index = 0; index < currencyTypes.size(); index++) {
				CurrencyType newCurrencyType = currencyTypes.get(index);
				if (currencyTypes.get(index).getCurrencyType().equals("RS"))
					newCurrencyTypes.add(newCurrencyType);
			}
			model.addObject("currencyTypes",newCurrencyTypes);
			model.addObject("contract", contractCSP);
			model.addObject("agency1CG",agency1CG);
			model.addObject("error", "Something is missing...");
			//model.addObject("msg2", "Check All tabs");
			//model.addObject("alt", "1");
			return model; 
		}
		java.sql.Date contractValidFrom = contractCSP.getValidFrom();
		java.sql.Date contractValidTo = contractCSP.getValidTo(); 
		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		for (int i = 0; i < contractingGov1.size(); i++) {
			ContractStakeholder contractStakeholder = new ContractStakeholder();
			contractStakeholder.setMemberId(contractingGov1.get(i));
			contractStakeholder.setIsagency1(true);
			contractStakeholder.setValidFrom(contractValidFrom);
			contractStakeholder.setValidTo(contractValidTo);
			contractStakeholders.add(contractStakeholder);
		}
		ContractStakeholder contractStakeholder = new ContractStakeholder();
		contractStakeholder.setMemberId(contractCSP.getAgency2().getAgencyCode());
		contractStakeholder.setIsagency1(false);
		contractStakeholder.setValidFrom(contractValidFrom);
		contractStakeholder.setValidTo(contractValidTo);
		contractStakeholders.add(contractStakeholder);
		ContractType billingContractType = contractTypeService.findByContractId(3);
		contractCSP.getAgency2().setContractType(billingContractType);
		contractCSP.setContractStakeholders(contractStakeholders);
		try {
			contractCSPService.save(contractCSP);
		} catch(Exception ex) {
			logger.info(ex.getMessage());
			Agency agency2 = contractCSPService.getAgencyCSP();
			contractCSP.setAgency2(agency2); 
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		
			Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
			//String ddpVer = lritDatacentreInfoService.getApplicableImmediateDdpVersionByDate(date);
			String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
			List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
			contractCSP.setAgency1(agency1);
			ModelAndView model = new ModelAndView("/billing/contract/cspcontract");
			model.addObject("contract", contractCSP);
			model.addObject("agency1CG",agency1CG);
			String errMsg = "Unable to save contract information\n"; 
			errMsg += "Reason: " + ex.getCause();
			model.addObject("error", errMsg);
			//model.addObject("msg2", "Check All tabs");
			//model.addObject("alt", "1");
			return model; 
		}
		logger.info("AFTER SUCCESSFULL SAVED");
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("message", "Contract is added succesfully.");
		//model.addObject("alt", "2");
		model.addObject("contractType", contractType);
		return model;
	}
	
	/**
	 * To Update CSP Contract or Extend Contract
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updatecspcontract", method = RequestMethod.POST)
	public ModelAndView updatecsp(@Valid @ModelAttribute("contract") ContractCSP contractcsp,
			BindingResult bindingResult,
			@RequestParam(name = "contractingGov1", required = false) List<String> contractingGov1,
			@RequestParam(name = "agency1ContractGovValidFrom",required = false) List<String> agency1ContractGovValidFrom,
			@RequestParam(name = "agency1ContractGovValidTo",required = false) List<String> agency1ContractGovValidTo,
			@RequestParam(name = "contract_annexure") MultipartFile contractAnnex,
			@RequestParam(name = "remarks",required = false) String remarks,
			@RequestParam(name = "contractAnnexureDate",required = false) String contractAnnexureDate) throws IOException, ParseException {
		logger.info("updatecsp()");
		contractValidator.validate(contractcsp, bindingResult);
		logger.info("ContractNo-"+contractcsp.getContractNumber());
	
		logger.info("contractingGov1-"+contractingGov1);
	
		if (contractingGov1 == null) {
			bindingResult.rejectValue("agency1", "cgList1");
		}
		List<ContractStakeholder> agency1ContractStakeholdersupdate = new ArrayList<ContractStakeholder>();
		for(int i = 0; i < contractingGov1.size(); i++) {
			ContractStakeholder contractStakholder = new ContractStakeholder();
			contractStakholder.setMemberId(contractingGov1.get(i));
			logger.info("contractingGov1"+contractingGov1.get(i));
			contractStakholder.setValidFrom(java.sql.Date.valueOf(agency1ContractGovValidFrom.get(i)));
			logger.info("FromDates"+agency1ContractGovValidFrom.get(i));
			contractStakholder.setValidTo(java.sql.Date.valueOf(agency1ContractGovValidTo.get(i)));
			logger.info("TODates"+agency1ContractGovValidTo.get(i));
			contractStakholder.setIsagency1(true);
			logger.info("contractStakeholder"+contractStakholder);
			agency1ContractStakeholdersupdate.add(contractStakholder);
		}
		
		if (bindingResult.hasErrors()) {
			logger.info("bindingResult.hasErrors()");
			Integer contractId = contractcsp.getContractId();
			Contract contract = contractService.getContractByContractId(contractId);
			List<String> agency1ContractStakeholders = contractService.getAgency1ContractStakeholders(contractId);
			List<String> agency2ContractStakeholders = contractService.getAgency2ContractStakeholders(contractId);
			java.sql.Date dateNew = new java.sql.Date(System.currentTimeMillis());
			List<LritContractingGovtMst> listCGInfoAgency1D = contractService.getStakeholderByDate(agency1ContractStakeholders, dateNew);
			List<LritContractingGovtMst> listCGInfoAgency2D = contractService.getStakeholderByDate( agency2ContractStakeholders, dateNew);
			List<LritContractingGovtMst> listCGInfoAgency1 = contractService.getNonStakeholderByDate(contract.getAgency1().getAgencyCode(), agency1ContractStakeholders, dateNew);

			logger.info("Agency 2 Contract Stakeholders "+listCGInfoAgency2D);

			List<ContractStakeholder> contractStakeholders1 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1D) {
				String lritId = lritContractingGovt.getCgLritid();
				ContractStakeholder contractStakeholder = contractService.getContractStakeholder(contractId, lritId);
				contractStakeholder.setName(lritContractingGovt.getCgName());
				contractStakeholders1.add(contractStakeholder);
			}
			logger.info(contractStakeholders1.toString());

			List<ContractStakeholder> contractNonStakeholders1 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1) {
				ContractStakeholder contractStakeholder = new ContractStakeholder(); 
				String lritId = lritContractingGovt.getCgLritid();
				String name = lritContractingGovt.getCgName(); 
				contractStakeholder.setName(name);
				contractStakeholder.setMemberId(lritId);
				contractNonStakeholders1.add(contractStakeholder);
			}
			logger.info(contractNonStakeholders1.toString());

			Agency agency2 = contractCSPService.getAgencyCSP();
			contract.setAgency2(agency2);
			
			ModelAndView model = new ModelAndView("/billing/contract/updatecspcontract");
			List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
			List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
			for (int index = 0; index < currencyTypes.size(); index++) {
				CurrencyType newCurrencyType = currencyTypes.get(index);
				if (currencyTypes.get(index).getCurrencyType().equals("RS"))
					newCurrencyTypes.add(newCurrencyType);
			}
			model.addObject("currencyTypes",newCurrencyTypes);
			model.addObject("currencyTypes",currencyTypes);
			model.addObject("error", "Contract with Same dates is already in system");
			model.addObject("contract", contract);
			model.addObject("agency1ContractStakeholders", contractStakeholders1);
			model.addObject("agency1CG", contractNonStakeholders1);
			return model;
			
		}
		
		// Update USA contract 
		contractService.updateContractCSP(contractcsp, contractingGov1, contractAnnex, remarks,contractAnnexureDate,agency1ContractGovValidFrom,agency1ContractGovValidTo);
		logger.info("After Contract Update");
		
		
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("message", "Contract is updated succesfully.");
		
		model.addObject("contractType", contractType);
		return model;
	}

	/**
	 * To get All Contracting Govts Details
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGDetails")
	@ResponseBody
	public List<LritAspinfo> getAllCGDetails() {
		return lritAspInfoService.findAllAspDetails();
	}

	/**
	 * To get Contracting Govts Details by ASP 
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGDetails/{aspLritid}")
	@ResponseBody
	public List<LritAspinfo> getCGDetails(@PathVariable("aspLritid") String aspLritid) {
		return lritAspInfoService.findAspDetails(aspLritid);
	}

	/**
	 * To get Contracting Govts. as per Agencu Code
	 * @author lrit-billing
	 * 
	 */
	@RequestMapping(value = "/getContractingGovernments1", method = RequestMethod.POST)
	public @ResponseBody List<LritContractingGovtMst> getContractingGovernments(
			@RequestParam("agencyCode") String agencyCode) {
		List<LritContractingGovtMst> listCGInfo = lritAspInfoService.getAspCgList(agencyCode);
		listCGInfo.addAll(lritDatacentreInfoService.findCGDetails(agencyCode));
		return listCGInfo;
	}

	/**
	 * To get DC Contracting Govts. as per agency Code
	 * @author lrit-billing
	 * 
	 */
	@RequestMapping(value = "/getDCContractingGovernments", method = RequestMethod.POST)
	public @ResponseBody List<LritDatacentreInfo> getDCContractingGovernments(
			@RequestParam("agencyCode") String agencyCode) {
		List<LritDatacentreInfo> listDCInfo = lritDatacentreInfoService.findDCDetails(agencyCode);
		return listDCInfo;

	}

}
