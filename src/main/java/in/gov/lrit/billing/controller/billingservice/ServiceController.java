/**
 * @ServiceController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 26-Aug-2019
 */
package in.gov.lrit.billing.controller.billingservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in.gov.lrit.billing.service.ServiceService;
import in.gov.lrit.billing.service.contract.ContractService;

/**
 * @author Yogendra Yadav (YY)
 *
 */
//@Controller
//@RequestMapping(value = "/service")
public class ServiceController {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ServiceController.class);

	@Autowired
	@Qualifier("serviceServiceImpl")
	ServiceService serviceService;

	@Autowired
	ContractService contractService;

	/**
	 * TO GET THE DETAILS OF CONTRACTING GOVERNMENET WITH BILLING CATEGORY AND COST
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGBillableItems")
	@ResponseBody
	public List<BillableItem> getCGBillableItems(@RequestParam("contractId") Integer contractId, Model model) {
//		System.out.println("contract id " + contractId);
		logger.info("Contract " + contractId);
		if (contractId == null)
			return null;

		List<BillableItem> billableItems = serviceService.getCGBillableItems(contractId);
		return billableItems;
	}

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGHashBillableItems")
	@ResponseBody
	public HashMap<String, HashMap<String, List<BillableItem>>> getCGHashBillableItems(
			@RequestParam("contractId") Integer contractId, Model model) {

		HashMap<String, HashMap<String, List<BillableItem>>> jsonData = new HashMap<String, HashMap<String, List<BillableItem>>>();
		logger.info("Contract " + contractId);
		if (contractId == null)
			return null;

		List<BillableItem> billableItems = serviceService.getCGBillableItems(contractId);
		for (BillableItem billableItem : billableItems) {
			List<BillableItem> messages = null;
			HashMap<String, List<BillableItem>> consumer = null;
			HashMap<String, HashMap<String, List<BillableItem>>> provider = null;

			if (jsonData.get(billableItem.getProvider()) != null) {
				consumer = jsonData.get(billableItem.getProvider());
				if (consumer.get(billableItem.getConsumer()) == null) {
					messages = new ArrayList<BillableItem>();
				} else
					messages = consumer.get(billableItem.getConsumer());

				messages.add(billableItem);
				consumer.put(billableItem.getConsumer(), messages);
				jsonData.put(billableItem.getProvider(), consumer);

			} else {
				consumer = new HashMap<String, List<BillableItem>>();
				messages = new ArrayList<BillableItem>();

				messages.add(billableItem);
				consumer.put(billableItem.getConsumer(), messages);
				jsonData.put(billableItem.getProvider(), consumer);
			}

			// jsonData.put(billableItem.getProvider(), value)

		}

		return jsonData;

	}

	/**
	 * TO GET THE DETAILS OF CONTRACTING GOVERNMENET WITH BILLING CATEGORY AND COST
	 * For Bill
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGBillableItemsForCSPBill")
	@ResponseBody
	public List<BillableItem> getCGBillableItemsForCSPBill(@RequestParam("contractId") Integer contractId, Model model) {
		System.out.println("contract id " + contractId);
		logger.info("Contract " + contractId);
		Contract contract = contractService.getContractByContractId(contractId);
		List<BillableItem> billableItems = serviceService.getCGBillableItems(contractId);

		/*
		 * double subtotal = 0; for (BillableItem billableItem : billableItems) {
		 * subtotal += billableItem.getCost(); // For Bill Provider is Consumer and
		 * Consumer is provider
		 * 
		 * billableItem.setConsumerName(serviceService.getCGName(billableItem.
		 * getProvider())); if
		 * (billableItem.getConsumer().equals(contract.getAgency2().getAgencyCode())) {
		 * billableItem.setProviderName(contract.getAgency2().getName()); } else
		 * billableItem.setProviderName(serviceService.getCGName(billableItem.
		 * getConsumer()));
		 * 
		 * } logger.info("Subtotal of billable Items " + subtotal);
		 */

		return billableItems;
	}

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGBillableItemsForBill")

	@ResponseBody
	public List<BillableItem> getCGBillableItemsForBill(@RequestParam("contractId") Integer contractId,
			Model model) {
		System.out.println("contract id " + contractId);
		logger.info("Contract " + contractId);
		List<BillableItem> billableItems = serviceService.getCGBillableItems(contractId);

		

		//logger.info("Subtotal of billable Items " + subtotal);
		return billableItems;
	}

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGName")
	@ResponseBody
	public String getCGName(@RequestParam("cgId") String cgId ){
		System.out.println("CG_ID " + cgId);
		String cgName = serviceService.getCGName(cgId);
		return cgName;
	}

	/**
	 * VESSEL BILLABLE ITEMS
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractId
	 * @param model
	 * @returnsystemCount
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getVesselBillableItems")
	@ResponseBody
	public List<BillableItem> getVesselsBillableItems(@RequestParam("contractId") Integer contractId, Model model) {
		logger.info(contractId + " ");

		List<BillableItem> billableItems = serviceService.getVesselsBillableItems(contractId);

		return billableItems;
	}

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getVesselHashBillableItems")
	@ResponseBody
	public HashMap<String, HashMap<String, List<BillableItem>>> getVesselHashBillableItems(
			@RequestParam("contractId") Integer contractId, Model model) {

		HashMap<String, HashMap<String, List<BillableItem>>> jsonData = new HashMap<String, HashMap<String, List<BillableItem>>>();
		logger.info("Contract " + contractId);
		if (contractId == null)
			return null;

		List<BillableItem> billableItems = serviceService.getVesselsBillableItems(contractId);
		for (BillableItem billableItem : billableItems) {
			List<BillableItem> messages = null;
			HashMap<String, List<BillableItem>> consumer = null;
			HashMap<String, HashMap<String, List<BillableItem>>> provider = null;

			if (jsonData.get(billableItem.getProvider()) != null) {
				consumer = jsonData.get(billableItem.getProvider());
				if (consumer.get(billableItem.getConsumer()) == null) {
					messages = new ArrayList<BillableItem>();
				} else
					messages = consumer.get(billableItem.getConsumer());

				messages.add(billableItem);
				consumer.put(billableItem.getConsumer(), messages);
				jsonData.put(billableItem.getProvider(), consumer);

			} else {
				consumer = new HashMap<String, List<BillableItem>>();
				messages = new ArrayList<BillableItem>();

				messages.add(billableItem);
				consumer.put(billableItem.getConsumer(), messages);
				jsonData.put(billableItem.getProvider(), consumer);
			}

			// jsonData.put(billableItem.getProvider(), value)
		}
		return jsonData;
	}

	/**
	 * RETURNS BILLABLE ITEMS BASED ON CONTRACT ID AND GIVEN DATE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractId
	 * @param fromDate
	 * @param toDate
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getBillableItems")
	@ResponseBody
	public HashMap<String, HashMap<String, List<BillableItem>>> getBillableItems(
			@RequestParam("contractId") Integer contractId, @RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, Model model) {

		HashMap<String, HashMap<String, List<BillableItem>>> billableItems = new HashMap<String, HashMap<String, List<BillableItem>>>();

		billableItems = serviceService.getHashedBillableItems(contractId, fromDate, toDate);

		return billableItems;
	}

	/**
	 * RETURNS BILLABLE ITEM FOR FOR GIVEN SERVICE ID (INVOICE/BILL)
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param billingServiceId
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getCGBillableItemsService")
	@ResponseBody
	public List<BillableItem> getCGBillableItemsService(@RequestParam("billingServiceId") Integer billingServiceId,
			Model model) {

		logger.info("Contract " + billingServiceId);
//		List<BillableItem> billableItems = serviceService.getCGBillableItemForService(billingServiceId);
//		return billableItems;
		return serviceService.getCGBillableItemForService(billingServiceId);
	}

}
