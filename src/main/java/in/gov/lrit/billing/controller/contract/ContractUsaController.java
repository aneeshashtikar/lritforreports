
package in.gov.lrit.billing.controller.contract;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAnnexure;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.ContractUsa;
import in.gov.lrit.billing.model.contract.CurrencyType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.contract.ContractTypeService;
import in.gov.lrit.billing.service.contract.ContractUsaService;
import in.gov.lrit.billing.validator.contract.ContractValidator;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.service.LritAspInfoService;
import in.gov.lrit.ddp.service.LritDatacentreInfoService;
import in_.gov.lrit.ddp.dto.LritNameDto;



/**
 * Controller to handle ContractUsa related views 
 * @author lrit-billing
 * 
 */
@Controller
@RequestMapping("/usacontract")
public class ContractUsaController {

	// logger
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractUsaController.class);

	@Autowired
	private ContractUsaService contractUsaService; 

	@Autowired
	private ContractService contractService;

	@Autowired
	private LritDatacentreInfoService lritDatacentreInfoService;

	@Autowired
	private LritAspInfoService lritAspInfoService;


	@Value("${dc.india.lritid}")
	private String agency1Code;

	@Value("${dc.usa.lritid}")
	private String agency2Code;


	@Autowired
	private ContractValidator contractValidator;

	@Autowired
	private ContractTypeService contractTypeService;
	

	/**
	 * For USA Contract GET
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value="/usacontract", method = RequestMethod.GET)
	public ModelAndView usaContract() {
		logger.info("usacontract()");
		ModelAndView model = new ModelAndView("/billing/contract/usacontract");
		ContractUsa contractUSA = new ContractUsa();

		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());

		Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
		//String ddpVer = lritDatacentreInfoService.getApplicableImmediateDdpVersionByDate(date);
		String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
		List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
		contractUSA.setAgency1(agency1);

		Agency agency2 = contractService.getAgencyByAgencyCode(agency2Code);
		List<LritContractingGovtMst> agency2CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency2Code, ddpVer);
		contractUSA.setAgency2(agency2); 
		List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
		List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
		for (int index = 0; index < currencyTypes.size(); index++) {
			CurrencyType newCurrencyType = currencyTypes.get(index);
			if (currencyTypes.get(index).getCurrencyType().equals("USD"))
				newCurrencyTypes.add(newCurrencyType);
		}
		model.addObject("currencyTypes",newCurrencyTypes);
	
		model.addObject("contract", contractUSA);
		model.addObject("agency1CG",agency1CG);
		model.addObject("agency2CG",agency2CG);
		return model; 
	}
	
	/**
	 * For Saving USA contract
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/usacontract",method = RequestMethod.POST)
	public ModelAndView saveUSA(@Valid @ModelAttribute("contract") ContractUsa contractUsa,BindingResult bindingResult,
			@RequestParam(name="contractingGov1", required=false) List<String> contractingGov1 ,
			@RequestParam(name="contractingGov2", required=false) List<String> contractingGov2,
			@RequestParam(name = "contract_document") MultipartFile contractDoc
			) throws IOException {
		logger.info("saveUSA()");
		logger.info("ContractNo-"+contractUsa.getContractNumber());
		logger.info("contractUsa"+contractUsa.getBillingCurrencyType().getCurrencyType());
		logger.info("contractingGov1-"+contractingGov1);
		logger.info("contractingGov2-"+contractingGov2);

		byte [] contractDocument=contractDoc.getBytes();
		contractUsa.setContractDocument(contractDocument);
		String contentType = contractDoc.getContentType();
		String pdf = "application/pdf";

		if(contractingGov1 == null) {
			bindingResult.rejectValue("agency1", "cgList1");

		}
		if(contractingGov2 == null) {
			bindingResult.rejectValue("agency2", "cgList1");
		}
		if(!contractDoc.isEmpty()){            
			if(!contentType.equals(pdf)) {
				bindingResult.rejectValue("contractDocument", "contractDocument1");
			}
		} else {
			bindingResult.rejectValue("contractDocument", "contractDocument");
		}

		contractValidator.validate(contractUsa,bindingResult);

		if(bindingResult.hasErrors()) {
			logger.info("In bindingResult.hasError()");
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());

			Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
			//String ddpVer = lritDatacentreInfoService.getApplicableImmediateDdpVersionByDate(date);
			String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
			List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
			contractUsa.setAgency1(agency1);

			Agency agency2 = contractService.getAgencyByAgencyCode(agency2Code);
			List<LritContractingGovtMst> agency2CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency2Code, ddpVer);
			contractUsa.setAgency2(agency2); 
			ModelAndView model = new ModelAndView("/billing/contract/usacontract");
			List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
			List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
			for (int index = 0; index < currencyTypes.size(); index++) {
				CurrencyType newCurrencyType = currencyTypes.get(index);
				if (currencyTypes.get(index).getCurrencyType().equals("USD"))
					newCurrencyTypes.add(newCurrencyType);
			}
			model.addObject("currencyTypes",newCurrencyTypes);
			model.addObject("contract", contractUsa);
			model.addObject("agency1CG",agency1CG);
			model.addObject("agency2CG",agency2CG);
			model.addObject("msg1", "Something is missing...");
			model.addObject("msg2", "Check All tabs...");
			model.addObject("alt", "1");
			return model;
		}

		java.sql.Date contractValidFrom = contractUsa.getValidFrom();
		java.sql.Date contractValidTo = contractUsa.getValidTo(); 
		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		for (int i = 0; i < contractingGov1.size(); i++) {
			ContractStakeholder contractStakeholder = new ContractStakeholder(); 
			contractStakeholder.setMemberId(contractingGov1.get(i));
			contractStakeholder.setIsagency1(true);
			contractStakeholder.setValidFrom(contractValidFrom);
			contractStakeholder.setValidTo(contractValidTo);
			contractStakeholders.add(contractStakeholder);
		}
		for (int i = 0; i < contractingGov2.size(); i++) {
			ContractStakeholder contractStakeholder = new ContractStakeholder(); 
			contractStakeholder.setMemberId(contractingGov2.get(i));
			contractStakeholder.setIsagency1(false);
			contractStakeholder.setValidFrom(contractValidFrom);
			contractStakeholder.setValidTo(contractValidTo);
			contractStakeholders.add(contractStakeholder);
		}

		ContractType billingContractType = contractTypeService.findByContractId(2);
		contractUsa.getAgency2().setContractType(billingContractType);
		contractUsa.setContractStakeholders(contractStakeholders);
		contractUsaService.save(contractUsa);
		logger.info("contractUsa Saved");		

		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("message", "Contract is added succesfully.");
		model.addObject("alt", "2");
		model.addObject("contractType", contractType);
		return model;
	}

	/**
	 * To update and Extend USA Contract
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateusacontract", method = RequestMethod.POST)
	public ModelAndView updateUSA(@Valid @ModelAttribute("contract") ContractUsa contractusa,
			BindingResult bindingResult,
			@RequestParam(name = "contractingGov1", required = false) List<String> contractingGov1,
			@RequestParam(name = "contractingGov2", required = false) List<String> contractingGov2,
			@RequestParam(name = "contract_annexure") MultipartFile contractAnnex,
			@RequestParam(name = "remarks",required = false) String remarks,
			@RequestParam(name = "agency1ContractGovValidFrom",required = false) List<String> agency1ContractGovValidFrom,
			@RequestParam(name = "agency1ContractGovValidTo",required = false) List<String> agency1ContractGovValidTo,
			@RequestParam(name = "agency2ContractGovValidFrom",required = false) List<String> agency2ContractGovValidFrom,
			@RequestParam(name = "agency2ContractGovValidTo",required = false) List<String> agency2ContractGovValidTo,
			@RequestParam(name = "contractAnnexureDate",required = false) String contractAnnexureDate) throws IOException, ParseException {

		logger.info("updateUSA()");
		logger.info("ContractNo-"+contractusa.getContractNumber());
		logger.info("contractingGov1-"+contractingGov1);
		logger.info("contractingGov2-"+contractingGov2);
		logger.info("agency1ContractGovValidFrom-"+agency1ContractGovValidFrom);
		logger.info("agency1ContractGovValidTo-"+agency1ContractGovValidTo);
		logger.info("agency2ContractGovValidFrom-"+agency2ContractGovValidFrom);
		logger.info("agency2ContractGovValidTo-"+agency2ContractGovValidTo);
		contractValidator.validate(contractusa, bindingResult);
		if (contractingGov1 == null) {
			bindingResult.rejectValue("agency1", "cgList1");
		}

		if (contractingGov2 == null) {
			bindingResult.rejectValue("agency2", "cgList1");
		}

		if (bindingResult.hasErrors()) {
			logger.info("In bindingResult.hasError()");
			Integer contractId = contractusa.getContractId();
			Contract contract = contractService.getContractByContractId(contractId);
			List<String> agency1ContractStakeholders = contractService.getAgency1ContractStakeholders(contractId);
			List<String> agency2ContractStakeholders = contractService.getAgency2ContractStakeholders(contractId);
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
			List<LritContractingGovtMst> listCGInfoAgency1D = contractService.getStakeholderByDate(agency1ContractStakeholders, date);
			List<LritContractingGovtMst> listCGInfoAgency2D = contractService.getStakeholderByDate( agency2ContractStakeholders, date);
			List<LritContractingGovtMst> listCGInfoAgency1 = contractService.getNonStakeholderByDate(contract.getAgency1().getAgencyCode(), agency1ContractStakeholders, date);
			List<LritContractingGovtMst> listCGInfoAgency2 = contractService.getNonStakeholderByDate(contract.getAgency2().getAgencyCode(), agency2ContractStakeholders, date);

			logger.info("Agency 2 Contract Stakeholders "+listCGInfoAgency2D);

			List<ContractStakeholder> contractStakeholders1 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1D) {
				String lritId = lritContractingGovt.getCgLritid();
				ContractStakeholder contractStakeholder = contractService.getContractStakeholder(contractId, lritId);
				contractStakeholder.setName(lritContractingGovt.getCgName());
				contractStakeholders1.add(contractStakeholder);
			}
			logger.info(contractStakeholders1.toString());

			List<ContractStakeholder> contractNonStakeholders1 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1) {
				ContractStakeholder contractStakeholder = new ContractStakeholder(); 
				String lritId = lritContractingGovt.getCgLritid();
				String name = lritContractingGovt.getCgName(); 
				contractStakeholder.setName(name);
				contractStakeholder.setMemberId(lritId);
				contractNonStakeholders1.add(contractStakeholder);
			}
			logger.info(contractNonStakeholders1.toString());

			List<ContractStakeholder> contractNonStakeholders2 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency2) {
				ContractStakeholder contractStakeholder = new ContractStakeholder(); 
				String lritId = lritContractingGovt.getCgLritid();
				String name = lritContractingGovt.getCgName(); 
				contractStakeholder.setName(name);
				contractStakeholder.setMemberId(lritId);
				contractNonStakeholders2.add(contractStakeholder);
			}
			logger.info(contractNonStakeholders2.toString());

			List<ContractStakeholder> contractStakeholders2 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency2D) {
				String lritId = lritContractingGovt.getCgLritid();
				ContractStakeholder contractStakeholder = contractService.getContractStakeholder(contractId, lritId);
				contractStakeholder.setName(lritContractingGovt.getCgName());
				contractStakeholders2.add(contractStakeholder);
			}
			logger.info(contractStakeholders2.toString());
			ModelAndView model = new ModelAndView("/billing/contract/updateusacontract");
			model.addObject("error", "Contract with Same dates is already in system");
			List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
			List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
			for (int index = 0; index < currencyTypes.size(); index++) {
				CurrencyType newCurrencyType = currencyTypes.get(index);
				if (currencyTypes.get(index).getCurrencyType().equals("USD"))
					newCurrencyTypes.add(newCurrencyType);
			}
			model.addObject("currencyTypes",newCurrencyTypes);
			model.addObject("contract", contract);		
			model.addObject("agency1ContractStakeholders", contractStakeholders1);
			model.addObject("agency2ContractStakeholders", contractStakeholders2);
			model.addObject("agency1CG", contractNonStakeholders1);
			model.addObject("agency2CG", contractNonStakeholders2);
			return model;		


		}
		// Update USA contract 
		contractService.updateContract(contractusa, contractingGov1, contractingGov2, contractAnnex, remarks,contractAnnexureDate,
				agency1ContractGovValidFrom, agency1ContractGovValidTo,agency2ContractGovValidFrom,agency2ContractGovValidTo);
		logger.info("ContractUSA Update");
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("message", "Contract is updated succesfully.");
		model.addObject("alt", "2");
		model.addObject("contractType", contractType);
		return model;
	}


}
