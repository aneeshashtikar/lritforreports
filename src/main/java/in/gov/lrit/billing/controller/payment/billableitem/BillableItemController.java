/**
 * @BillableItemController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 21-Aug-2019
 */
package in.gov.lrit.billing.controller.payment.billableitem;

import java.util.Collection;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategoryLog;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.payment.billableitem.PaymentBillableItemCategoryService;

/**
 * @author Yogendra Yadav (YY)
 *
 */
@Controller
@RequestMapping("/billable")
public class BillableItemController {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BillableItemController.class);

	@Autowired
	private PaymentBillableItemCategoryService paymentBillableItemCategoryService;

	@Autowired
	private ContractService contractService;

	/**
	 * TO ADD NEW BILLABLE ITEM
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/addBillableCategory", method = RequestMethod.GET)
	public String showBillableCategory(Model model) {
		logger.info("showBillableCategory()");

		PaymentBillableItemCategory paymentBillableItemCategory = new PaymentBillableItemCategory();
		PaymentBillableItemCategoryLog paymentBillableItemCategoryLog = new PaymentBillableItemCategoryLog();
		List<ContractType> contractTypeList = contractService.getContractTypes();

		model.addAttribute("billableItemCategory", paymentBillableItemCategory);
		model.addAttribute("paymentBillableItemCategoryLog", paymentBillableItemCategoryLog);
		model.addAttribute("contractTypeList", contractTypeList);

		return "billing/billableItem/addbillableitemcategory";
	}

	/**
	 * TO SAVE ENTERED NEW BILLABLE ITEM IN DATABASE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param paymentBillableItemCategory
	 * @param model
	 * @param result
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/addBillableCategory", method = RequestMethod.POST)
	public String saveBillableCategory(
			@ModelAttribute("billableItemCategory") PaymentBillableItemCategory paymentBillableItemCategory,
			BindingResult bindingResultPaymentCategory, Model model) {
		logger.info("saveBillableCategory()");

		try {
			paymentBillableItemCategory = paymentBillableItemCategoryService.save(paymentBillableItemCategory);
			logger.info(paymentBillableItemCategory.toString());
			if (paymentBillableItemCategory != null) {
				model.addAttribute("message", "Success!! New Billable Item is Added");
			}
		} catch (ConstraintViolationException e) {
			logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
			model.addAttribute("error", "");
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("EXCEPTION OCCURED IN BILLABLEITEMS" + e.getMessage());
			model.addAttribute("error", "Sorry!!  " + e.getMessage());
			e.printStackTrace();
		}

		// not needed
		model.addAttribute("billableItemCategory", new PaymentBillableItemCategory());

		List<ContractType> contractTypeList = contractService.getContractTypes();
		model.addAttribute("contractTypeList", contractTypeList);

		return "billing/billableItem/addbillableitemcategory";
	}

	/**
	 * CONTROLLER LEADS TO UPDATE PAGE FOR UPDATING THE BILLABLE ITEM CATEGORY
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param model
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateBillableCategory", method = RequestMethod.GET)
	public String showUpdateBillableCategory(Model model) {
		logger.info("showUpdateBillableCategory()");

		PaymentBillableItemCategory paymentBillableItemCategory = new PaymentBillableItemCategory();
		model.addAttribute("billableItemCategory", paymentBillableItemCategory);

		List<ContractType> contractTypeList = contractService.getContractTypes();
		model.addAttribute("contractTypeList", contractTypeList);

		return "billing/billableItem/updatebillableitemcategory";
	}

	/**
	 * TO UPDATE THE NEW DATA OF SELECTED BILLABLE ITEM CATEGORY
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param paymentBillableItemCategory ON WHICH BILLABLEITEM CATEGORY UPDATE TO
	 *                                    PERFORM
	 * @param model
	 * @param result
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateBillableCategory", method = RequestMethod.POST)
	public String saveUpdateBillableCategory(
			@ModelAttribute("billableItemCategory") PaymentBillableItemCategory paymentBillableItemCategory,
			BindingResult result, Model model) {
		logger.info("INSIDE saveUpdateBillableCategory()");
		logger.info("paymentBillableItemCategory : " + paymentBillableItemCategory);

		try {
			paymentBillableItemCategory = paymentBillableItemCategoryService.update(paymentBillableItemCategory);
			if (paymentBillableItemCategory != null)
				model.addAttribute("message", "Success!! New Billable Item is Added");
			else
				model.addAttribute("error", "Sorry!! Failed To add Billable Item");
		} catch (ConstraintViolationException e) {
			logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
			model.addAttribute("error", "");
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("EXCEPTION OCCURED IN BILLABLEITEMS" + e.getMessage());
			model.addAttribute("error", "Sorry!!  " + e.getMessage());
			e.printStackTrace();
		}

		// not needed
		model.addAttribute("billableItemCategory", new PaymentBillableItemCategory());

		List<ContractType> contractTypeList = contractService.getContractTypes();
		model.addAttribute("contractTypeList", contractTypeList);

		return "billing/billableItem/updatebillableitemcategory";
	}

	/**
	 * METHOD TO VIEW THE BILLABLE ITEM CATEGORY
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param billableItemCategoryId TO GET ID AND FETCH FROM FB
	 * @param model                  TO ATTACHED FETCHED DATA FROM DB TO FRONTEND
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/viewBillableCategory", method = { RequestMethod.GET, RequestMethod.POST })
	public String showViewBillableCategory(Model model) {
		logger.info("showViewBillableCategory()");

		PaymentBillableItemCategory paymentBillableItemCategory = new PaymentBillableItemCategory();
		model.addAttribute("billableItemCategory", paymentBillableItemCategory);

		List<ContractType> contractTypeList = contractService.getContractTypes();
		model.addAttribute("contractTypeList", contractTypeList);

		return "billing/billableItem/viewbillableitemcategory";
	}

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateBillableCategoryLog/{billableItemCategoryLogId}", method = { RequestMethod.GET })
	public String showViewBillableCategoryLog(
			@PathVariable("billableItemCategoryLogId") Integer billableItemCategoryLogId, Model model) {
		logger.info("showViewBillableCategory()");
		logger.info("billableItemCategoryLogId : " + billableItemCategoryLogId);

		PaymentBillableItemCategoryLog paymentBillableItemCategoryLog = paymentBillableItemCategoryService
				.getPaymentBillableItemCategoryLog(billableItemCategoryLogId);

		List<ContractType> contractTypeList = contractService.getContractTypes();
		model.addAttribute("contractTypeList", contractTypeList);

		if (paymentBillableItemCategoryLog != null) {

			Integer contractTypeId = paymentBillableItemCategoryLog.getPaymentBillableItemCategory()
					.getBillingContractType().getContractTypeId();

			Collection<PaymentBillableItemCategory> paymentBillableItemCategories = paymentBillableItemCategoryService
					.findByBillingContractType(contractTypeId);

			model.addAttribute("paymentBillableItemCategories", paymentBillableItemCategories);
			model.addAttribute("paymentBillableItemCategoryLog", paymentBillableItemCategoryLog);

			return "billing/billableItem/updatebillableitemcategorylog";
		}

		return "billing/billableItem/viewbillableitemcategory";
	}

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateBillableCategoryLog/{billableItemCategoryLogId}", method = { RequestMethod.POST })
	public String saveViewBillableCategoryLog(
			@ModelAttribute("paymentBillableItemCategoryLog") PaymentBillableItemCategoryLog paymentBillableItemCategoryLog,
			BindingResult result, @PathVariable("billableItemCategoryLogId") Integer billableItemCategoryLogId,
			Model model) {

		logger.info("showViewBillableCategory()");
		logger.info("billableItemCategoryLogId : " + billableItemCategoryLogId);
		logger.info("paymentBillableItemCategoryLog : " + paymentBillableItemCategoryLog);

		try {
			paymentBillableItemCategoryLog = paymentBillableItemCategoryService
					.saveBillableCategoryLog(paymentBillableItemCategoryLog);
			if (paymentBillableItemCategoryLog == null)
				model.addAttribute("error", "Unable To Add Billable Item Category Log");
			else {
				model.addAttribute("message", "Billable Item Category Log Added");
			}
		} catch (ConstraintViolationException e) {
			logger.error("EXCEPTION OCCURED DUE TO DATABASE CONSTRAINT");
			model.addAttribute("error", "" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("EXCEPTION OCCURED IN BILLABLEITEMS" + e.getMessage());
			model.addAttribute("error", "" + e.getMessage());
			e.printStackTrace();
		}

		PaymentBillableItemCategory paymentBillableItemCategory = new PaymentBillableItemCategory();
		model.addAttribute("billableItemCategory", paymentBillableItemCategory);

		List<ContractType> contractTypeList = contractService.getContractTypes();
		model.addAttribute("contractTypeList", contractTypeList);

		return "billing/billableItem/viewbillableitemcategory";
	}

	/**
	 * RETURN DYNAMIC DATA FOR FETCHING BILLABLE ITEMS CATEGORY FOR REQUESTED
	 * CONTRACT TYPE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractTypeId BASED ON CONTRACT TYPE BILLABLE ITEMS CATEGORY IS
	 *                       FETCHED
	 * @param response
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getBillableItemCategory")
	@ResponseBody
	public Collection<PaymentBillableItemCategory> getBillableItemList(
			@RequestParam("contractTypeId") Integer contractTypeId) {
		logger.info("INSIDE getBillableItemList()");
		logger.info("contractTypeId : " + contractTypeId);

		Collection<PaymentBillableItemCategory> paymentBillableItemCategories = paymentBillableItemCategoryService
				.findByBillingContractType(contractTypeId);

		return paymentBillableItemCategories;
	}

	/**
	 * RETURN BILLABLE ITEM CATEGORY DETAILS ACCORDING TO BILLABLE ITEM ID
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param billableItemCategoryId ID ON WHICH DETAILS TO BE FETCHED
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getBillableItemCategoryDetails")
	@ResponseBody
	public PaymentBillableItemCategory getBillableItemDetails(
			@RequestParam("billableItemCategoryId") Integer billableItemCategoryId) {
		logger.info("INSIDE getBillableItemDetails()");
		logger.info("billableItemCategoryId : " + billableItemCategoryId);

		PaymentBillableItemCategory paymentBillableItemCategory = paymentBillableItemCategoryService
				.getBillableItemCategory(billableItemCategoryId);

		return paymentBillableItemCategory;
	}

	/**
	 * RETURN BILLABLE ITEM CATEGORY DETAILS ACCORDING TO BILLABLE ITEM ID
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param billableItemCategoryId ID ON WHICH DETAILS TO BE FETCHED
	 * @return
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getLatestBillableItemCategoryDetails")
	@ResponseBody
	public PaymentBillableItemCategoryLog getLatestBillableItemDetails(
			@RequestParam("billableItemCategoryId") Integer billableItemCategoryId) {
		logger.info("INSIDE getBillableItemDetails()");
		logger.info("billableItemCategoryId : " + billableItemCategoryId);

		PaymentBillableItemCategoryLog paymentBillableItemCategoryLog = paymentBillableItemCategoryService
				.getLatestBillableItemCategoryLog(billableItemCategoryId);

		return paymentBillableItemCategoryLog;
	}

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/getBillableItemCategoryLogsDetails")
	@ResponseBody
	public List<PaymentBillableItemCategoryLog> fetchAllPaymentBillableItemCategoryLogDetails(
			@RequestParam("billableItemCategoryId") Integer billableItemCategoryId) {
		logger.info("INSIDE fetchAllPaymentBillableItemCategoryLogDetails()");
		logger.info("billableItemCategoryId : " + billableItemCategoryId);

		List<PaymentBillableItemCategoryLog> paymentBillableItemCategoryLogs = paymentBillableItemCategoryService
				.getPaymentBillableItemCategoryLogs(billableItemCategoryId);

		return paymentBillableItemCategoryLogs;
	}
}
