/**
 * @ContractShippingCompanyController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Nov 5, 2019
 */
package in.gov.lrit.billing.controller.contract;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAnnexure;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.ContractShippingCompany;
import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.CurrencyType;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.service.contract.ContractCSPService;
import in.gov.lrit.billing.service.contract.ContractService;
import in.gov.lrit.billing.service.contract.ContractShippingCompanyService;
import in.gov.lrit.billing.service.contract.ContractTypeService;
import in.gov.lrit.billing.validator.contract.ContractValidator;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.service.LritAspInfoService;
import in.gov.lrit.ddp.service.LritDatacentreInfoService;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**Description :- ContractShippingCompanyController for saving ShippingComapnyContract purpose
 * @author lrit-billing
 *
 */
@Controller
@RequestMapping("/shippingcompanycontract")
public class ContractShippingCompanyController {

	// logger
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractShippingCompanyController.class);

	@Autowired
	private ContractShippingCompanyService contractShippingCompanyService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private LritDatacentreInfoService lritDatacentreInfoService; 

	@Autowired
	private LritAspInfoService lritAspInfoService;

	@Autowired
	private ContractValidator contractValidator;

	@Autowired
	private ContractTypeService contractTypeService;

	@Value("${dc.india.lritid}")
	private String agency1Code;
	

	/**
	 * For Shipping Company Contract GET 
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/shippingcompanycontract",method = RequestMethod.GET) 
	public ModelAndView shippingCompanyContract() {
		logger.info("shippingCompnyContract()");

		ModelAndView model = new ModelAndView("/billing/contract/shippingcompanycontract");
		ContractShippingCompany contractShippingCompany = new ContractShippingCompany();
		List<LritNameDto> lritNameDtos = contractShippingCompanyService.getAgencyShippingCompany();
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());

		Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
		//String ddpVer = lritDatacentreInfoService.getApplicableImmediateDdpVersionByDate(date);
		String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
		List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
		contractShippingCompany.setAgency1(agency1);
		List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
		List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
		for (int index = 0; index < currencyTypes.size(); index++) {
			CurrencyType newCurrencyType = currencyTypes.get(index);
			if (currencyTypes.get(index).getCurrencyType().equals("RS"))
				newCurrencyTypes.add(newCurrencyType);
		}
		model.addObject("currencyTypes",newCurrencyTypes);
		model.addObject("contract", contractShippingCompany);
		model.addObject("agency1CG",agency1CG);
		model.addObject("lritNameDtos", lritNameDtos);

		return model; 
	}
	
	/**
	 * To save Shipping Company Contract
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/shippingcompanycontract", method = RequestMethod.POST)
	public ModelAndView saveShippingContract(@Valid @ModelAttribute("contract") ContractShippingCompany contractShippingCompany,
			BindingResult bindingResult,
			@RequestParam(name = "contractingGov1", required = false) List<String> contractingGov1,
			@RequestParam(name = "contract_document") MultipartFile contractDoc) throws IOException {
		logger.info("saveShippingContract()");
		logger.info("ContractNo-"+contractShippingCompany.getContractNumber());
		logger.info("contractingGov1-"+contractingGov1);
		logger.info("contractShippingCompany"+contractShippingCompany.getBillingCurrencyType().getCurrencyType());
		byte[] contractDocument = contractDoc.getBytes();
		contractShippingCompany.setContractDocument(contractDocument);
		contractValidator.validate(contractShippingCompany, bindingResult);
		String contentType = contractDoc.getContentType();
		String pdf = "application/pdf";

		if (contractingGov1 == null) {
			bindingResult.rejectValue("agency1", "cgList1");
		}
		if (!contractDoc.isEmpty()) {
			if (!contentType.equals(pdf)) {
				bindingResult.rejectValue("contractDocument", "contractDocument1");
			}
		} else {
			bindingResult.rejectValue("contractDocument", "contractDocument");

		}
		if (bindingResult.hasErrors()) {
			logger.info("bindingResult.hasErrors()");
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
			Agency agency1 = contractService.getAgencyByAgencyCode(agency1Code);
			String ddpVer = lritAspInfoService.getApplicableRegularDdpVersionByDate(date);
			List<LritContractingGovtMst> agency1CG = (List<LritContractingGovtMst>)lritDatacentreInfoService.findCGDetailsByAgencyCodeAndDdpVer(agency1Code, ddpVer);
			contractShippingCompany.setAgency1(agency1);
			List<LritNameDto> lritNameDtosShippingCompany = contractShippingCompanyService.getAgencyShippingCompany();
			ModelAndView model = new ModelAndView("/billing/contract/shippingcompanycontract");
			List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
			List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
			for (int index = 0; index < currencyTypes.size(); index++) {
				CurrencyType newCurrencyType = currencyTypes.get(index);
				if (currencyTypes.get(index).getCurrencyType().equals("RS"))
					newCurrencyTypes.add(newCurrencyType);
			}
			model.addObject("currencyTypes",newCurrencyTypes);
			model.addObject("agency1CG", agency1CG);
			model.addObject("contract_document", contractDoc);
			model.addObject("contract", contractShippingCompany);
			model.addObject("error", "Something is missing...");
			model.addObject("contractingGov1", contractingGov1);
			model.addObject("lritNameDtos", lritNameDtosShippingCompany);
			return model;
		}
		java.sql.Date contractValidFrom = contractShippingCompany.getValidFrom();
		java.sql.Date contractValidTo = contractShippingCompany.getValidTo(); 
		List<ContractStakeholder> contractStakeholders = new ArrayList<ContractStakeholder>();
		for (int i = 0; i < contractingGov1.size(); i++) {
			ContractStakeholder contractStakeholder = new ContractStakeholder();
			contractStakeholder.setMemberId(contractingGov1.get(i));
			contractStakeholder.setIsagency1(true);
			contractStakeholder.setValidFrom(contractValidFrom);
			contractStakeholder.setValidTo(contractValidTo);
			contractStakeholders.add(contractStakeholder);
		}
		List<Integer> shipppingCompanyVessels = contractShippingCompanyService.getShippingCompanyVessels(contractShippingCompany.getAgency2().getAgencyCode());
		for (int i = 0; i < shipppingCompanyVessels.size(); i++) {
			ContractStakeholder contractStakeholder = new ContractStakeholder();
			contractStakeholder.setMemberId(shipppingCompanyVessels.get(i).toString());
			contractStakeholder.setValidFrom(contractValidFrom);
			contractStakeholder.setValidTo(contractValidTo);

			contractStakeholder.setIsagency1(false);

			contractStakeholders.add(contractStakeholder);
		}
		contractShippingCompany.setContractStakeholders(contractStakeholders);
		ContractType billingContractType = contractTypeService.findByContractId(4);
		contractShippingCompany.getAgency2().setContractType(billingContractType);
		contractShippingCompanyService.save(contractShippingCompany);
		logger.info("AFTER SUCCESSFULL SAVED");
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("message", "Contract is added succesfully.");

		model.addObject("contractType", contractType);
		return model;
	}

	/**
	 * To Update or Extend Shipping Company Contract
	 * @author lrit-billing
	 * 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value = "/updateshippingcompanycontract", method = RequestMethod.POST)
	public ModelAndView updateShippingCompany(@Valid @ModelAttribute("contract") ContractShippingCompany contractShippingCompany,
			BindingResult bindingResult,
			@RequestParam(name = "contractingGov1", required = false) List<String> contractingGov1,
			@RequestParam(name = "agency1ContractGovValidFrom",required = false) List<String> agency1ContractGovValidFrom,
			@RequestParam(name = "agency1ContractGovValidTo",required = false) List<String> agency1ContractGovValidTo,
			@RequestParam(name = "contract_annexure") MultipartFile contractAnnex,
			@RequestParam(name = "remarks",required = false) String remarks,
			@RequestParam(name = "contractAnnexureDate",required = false) String contractAnnexureDate) throws IOException, ParseException {
		logger.info("updateShipingCompany()");
		logger.info("ContractNo-"+contractShippingCompany.getContractNumber());
		logger.info("contractingGov1-"+contractingGov1);
		contractValidator.validate(contractShippingCompany, bindingResult);

		if (contractingGov1 == null) {
			bindingResult.rejectValue("agency1", "cgList1");
		}
		if (bindingResult.hasErrors()) {
			logger.info("bindingResult.hasErrors()");
			Integer contractId = contractShippingCompany.getContractId();
			Contract contract = contractService.getContractByContractId(contractId);
			List<String> agency1ContractStakeholders = contractService.getAgency1ContractStakeholders(contractId);
			List<String> agency2ContractStakeholders = contractService.getAgency2ContractStakeholders(contractId);
			java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
			List<LritContractingGovtMst> listCGInfoAgency1D = contractService.getStakeholderByDate(agency1ContractStakeholders, date);
			List<LritContractingGovtMst> listCGInfoAgency2D = contractService.getStakeholderByDate( agency2ContractStakeholders, date);
			List<LritContractingGovtMst> listCGInfoAgency1 = contractService.getNonStakeholderByDate(contract.getAgency1().getAgencyCode(), agency1ContractStakeholders, date);
			logger.info("Agency 2 Contract Stakeholders "+listCGInfoAgency2D);
			List<ContractStakeholder> contractStakeholders1 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1D) {
				String lritId = lritContractingGovt.getCgLritid();
				ContractStakeholder contractStakeholder = contractService.getContractStakeholder(contractId, lritId);
				contractStakeholder.setName(lritContractingGovt.getCgName());
				contractStakeholders1.add(contractStakeholder);
			}
			logger.info(contractStakeholders1.toString());

			List<ContractStakeholder> contractNonStakeholders1 = new ArrayList<ContractStakeholder>();
			for(LritContractingGovtMst lritContractingGovt : listCGInfoAgency1) {
				ContractStakeholder contractStakeholder = new ContractStakeholder(); 
				String lritId = lritContractingGovt.getCgLritid();
				String name = lritContractingGovt.getCgName(); 
				contractStakeholder.setName(name);
				contractStakeholder.setMemberId(lritId);
				contractNonStakeholders1.add(contractStakeholder);
			}
			logger.info(contractNonStakeholders1.toString());

			ModelAndView model = new ModelAndView("/billing/contract/updateshippingcompanycontract");
			List<CurrencyType> currencyTypes = contractService.getAllCurrencyTypes();
			List<CurrencyType> newCurrencyTypes = new ArrayList<CurrencyType>();
			for (int index = 0; index < currencyTypes.size(); index++) {
				CurrencyType newCurrencyType = currencyTypes.get(index);
				if (currencyTypes.get(index).getCurrencyType().equals("RS"))
					newCurrencyTypes.add(newCurrencyType);
			}
			model.addObject("currencyTypes",newCurrencyTypes);
			model.addObject("contract", contract);
			model.addObject("agency1ContractStakeholders", contractStakeholders1);
			model.addObject("agency1CG", contractNonStakeholders1);
			model.addObject("error", "Contract with Same dates is already in system");
			return model;

		}
		// Update shipping company contract 
		contractService.updateContractShippingCompany(contractShippingCompany, contractingGov1, contractAnnex, remarks, contractAnnexureDate,agency1ContractGovValidFrom,agency1ContractGovValidTo);
		logger.info("After Contract Update");
		ModelAndView model = new ModelAndView("/billing/contract/listofcontracts");
		List<ContractType> contractType = contractService.getContractTypes();
		model.addObject("message", "Contract is updated succesfully.");

		model.addObject("contractType", contractType);
		return model;
	}

}
