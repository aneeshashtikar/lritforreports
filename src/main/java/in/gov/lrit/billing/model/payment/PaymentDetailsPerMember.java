package in.gov.lrit.billing.model.payment;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * @author lrit-billing
 *
 */
/**
 * The persistent class for the billing_payment_details_per_member database
 * table.
 * 
 */
@Entity
@Table(name = "billing_payment_details_per_member")
@NamedQuery(name = "BillingPaymentDetailsPerMember.findAll", query = "SELECT b FROM PaymentDetailsPerMember b")
public class PaymentDetailsPerMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="billing_payment_details_per_member_seq_gen",sequenceName="billing_payment_details_per_member_seq", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="billing_payment_details_per_member_seq_gen")
	@Column(name = "pmt_det_member_id")
	private Integer pmtDetMemberId;

	private String consumer;
	
	@Transient
	private String consumerName;


	private double cost;

	@Column(name = "paid_amount")
	private double paidAmount;

	private String provider;


	@Transient
	private String providerName;


	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	// bi-directional many-to-one association to BillingPaymentDetailsForService
	@ManyToOne
	@JoinColumn(name = "pmt_det_service_id")
	@JsonBackReference
	private PaymentDetailsForService billingPaymentDetailsForService;

	public PaymentDetailsPerMember() {
	}

	public Integer getPmtDetMemberId() {
		return this.pmtDetMemberId;
	}

	public void setPmtDetMemberId(Integer pmtDetMemberId) {
		this.pmtDetMemberId = pmtDetMemberId;
	}

	public String getConsumer() {
		return this.consumer;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}

	public double getCost() {
		return this.cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getProvider() {
		return this.provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}
	


	public PaymentDetailsForService getBillingPaymentDetailsForService() {
		return this.billingPaymentDetailsForService;
	}

	public void setBillingPaymentDetailsForService(PaymentDetailsForService billingPaymentDetailsForService) {
		this.billingPaymentDetailsForService = billingPaymentDetailsForService;
	}

	/*
	 * @Override public String toString() { return
	 * "PaymentDetailsPerMember [pmtDetMemberId=" + pmtDetMemberId + ", consumer=" +
	 * consumer + ", consumerName=" + consumerName + ", cost=" + cost +
	 * ", paidAmount=" + paidAmount + ", provider=" + provider + ", providerName=" +
	 * providerName + ", billingPaymentDetailsForService=" +
	 * billingPaymentDetailsForService + "]"; }
	 */
	
	

}