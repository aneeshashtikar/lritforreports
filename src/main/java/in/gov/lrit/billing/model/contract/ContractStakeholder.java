package in.gov.lrit.billing.model.contract;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * @author lrit-billing
 *
 */
/**
 * The persistent class for the billing_contract_stakeholders database table.
 * 
 */
@Entity
@Table(name = "billing_contract_stakeholders")
@NamedQuery(name = "BillingContractStakeholder.findAll", query = "SELECT b FROM ContractStakeholder b")
public class ContractStakeholder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="billing_contract_stakeholders_seq_gen",sequenceName="billing_contract_stakeholders_seq", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="billing_contract_stakeholders_seq_gen")
	private Integer id;

	private Boolean isagency1;

	@Column(name = "member_id")
	private String memberId;

	@Transient
	private String name;
	
	@Column(name = "valid_from")
	private Date validFrom;
	
	
	@Column(name = "valid_to")
	private Date validTo; 
	
	
	// uni-directional many-to-one association to BillingContract
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "contract_id")
	private Contract billingContract;
	
	

	public ContractStakeholder() {
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsagency1() {
		return this.isagency1;
	}

	public void setIsagency1(Boolean isagency1) {
		this.isagency1 = isagency1;
	}

	public String getMemberId() {
		return this.memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Contract getBillingContract() {
		return this.billingContract;
	}

	public void setBillingContract(Contract billingContract) {
		this.billingContract = billingContract;
	}

	
	
	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	@Override
	public String toString() {
		return "ContractStakeholder [id=" + id + ", isagency1=" + isagency1 + ", memberId=" + memberId + ", validFrom="
				+ validFrom + ", validTo=" + validTo + "]";
	}


	
}