package in.gov.lrit.billing.model.billingservice.invoice;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import in.gov.lrit.billing.model.billingservice.Service;

/**
 * The persistent class for the billing_generated_copy database table.
 * 
 */
@Entity
@Table(name = "billing_approved_invoice")
public class ApprovedInvoice {

	@Id
	@SequenceGenerator(name = "billing_approved_invoice_seq_gen", sequenceName = "billing_approved_invoice_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "billing_approved_invoice_seq_gen")
	@Column(name = "billing_approved_invoice_id")
	private Integer approvedInvoiceId;

	@Column(name = "approved_copy")
	private byte[] approvedCopy;

	@CreationTimestamp
	private Date date;

	@ManyToOne
	@JoinColumn(name = "billing_service_id", referencedColumnName = "billing_service_id")
	private Service service;

	public Integer getApprovedInvoiceId() {
		return approvedInvoiceId;
	}

	public void setApprovedInvoiceId(Integer approvedInvoiceId) {
		this.approvedInvoiceId = approvedInvoiceId;
	}

	public byte[] getApprovedCopy() {
		return approvedCopy;
	}

	public void setApprovedCopy(byte[] approvedCopy) {
		this.approvedCopy = approvedCopy;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@Override
	public String toString() {
		return "ApprovedInvoice [approvedInvoiceId=" + approvedInvoiceId + ", date=" + date + ", service=" + service
				+ "]";
	}

}
