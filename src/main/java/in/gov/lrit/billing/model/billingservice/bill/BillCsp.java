package in.gov.lrit.billing.model.billingservice.bill;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the billing_service_bill_csp database table.
 * 
 */

/**
 * @author lrit-billing
 *
 */
@Entity
@Table(name = "billing_service_bill_csp")
public class BillCsp extends Bill implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "dollar_rate")
	private double dollarRate;

	public BillCsp() {
	}

	public double getDollarRate() {
		return this.dollarRate;
	}

	public void setDollarRate(double dollarRate) {
		this.dollarRate = dollarRate;
	}

	

}