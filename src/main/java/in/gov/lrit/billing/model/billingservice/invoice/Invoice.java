package in.gov.lrit.billing.model.billingservice.invoice;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import in.gov.lrit.billing.model.billingservice.Service;

/**
 * The persistent class for the billing_service_invoice database table.
 * 
 */
@Entity
@Table(name = "billing_service_invoice")
public class Invoice extends Service implements Serializable {
	private static final long serialVersionUID = 1L;

//	@Column(name = "approved_copy")
//	private byte[] approvedCopy;
//
//	@Column(name = "generated_copy")
//	private byte[] generatedCopy;

	@Column(name = "invoice_no")
	private String invoiceNo;

	public Invoice() {
	}

//	public byte[] getApprovedCopy() {
//		return this.approvedCopy;
//	}
//
//	public void setApprovedCopy(byte[] approvedCopy) {
//		this.approvedCopy = approvedCopy;
//	}
//
//	public byte[] getGeneratedCopy() {
//		return this.generatedCopy;
//	}
//
//	public void setGeneratedCopy(byte[] generatedCopy) {
//		this.generatedCopy = generatedCopy;
//	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	@Override
	public String toString() {
		return "Invoice [invoiceNo=" + invoiceNo + ", toString()=" + super.toString() + "]";
	}

}