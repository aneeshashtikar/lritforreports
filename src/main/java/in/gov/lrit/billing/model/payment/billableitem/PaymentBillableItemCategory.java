package in.gov.lrit.billing.model.payment.billableitem;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;

import in.gov.lrit.billing.model.contract.ContractType;

/**
 * The persistent class for the billing_payment_billable_item_category database
 * table.
 * 
 */
@Entity
@Table(name = "billing_payment_billable_item_category")
@NamedQuery(name = "BillingPaymentBillableItemCategory.findAll", query = "SELECT b FROM PaymentBillableItemCategory b")
public class PaymentBillableItemCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "billable_item_category_id_seq_gen", sequenceName = "billable_item_category_id_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "billable_item_category_id_seq_gen")
	@Column(name = "billable_item_category_id")
	private Integer billableItemCategoryId;

	@Column(name = "item_name")
	private String itemName;

	private double rate;

//	@DateTimeFormat(iso = ISO.DATE)
//	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "valid_from")
	private java.sql.Date validFrom;

//	@DateTimeFormat(iso = ISO.DATE)
//	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "valid_till")
	private java.sql.Date validTill;

	@CreationTimestamp
	@Column(name = "entry_date")
	private Date entryDate;

	private String remarks;

	// bi-directional many-to-one association to BillingBillableItemRule
	@OneToMany(mappedBy = "billingPaymentBillableItemCategory")
	@JsonBackReference
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<BillableItemRule> billingBillableItemRules;

	// uni-directional many-to-one association to BillingContractType
	@ManyToOne
	@JoinColumn(name = "contract_type_id")
	private ContractType billingContractType;

//	@OneToMany(mappedBy = "paymentBillableItemCategory", cascade = { CascadeType.PERSIST,
//			CascadeType.MERGE }, orphanRemoval = true)
//	@LazyCollection(LazyCollectionOption.FALSE)
//	@JsonBackReference
//	private List<PaymentBillableItemCategoryLog> paymentBillableItemCategoryLogs;

	@Transient
	private PaymentBillableItemCategoryLog paymentBillableItemCategoryLog;

	/**
	 * @return the paymentBillableItemCategoryLog
	 */
	public PaymentBillableItemCategoryLog getPaymentBillableItemCategoryLog() {
		return paymentBillableItemCategoryLog;
	}

	/**
	 * @param paymentBillableItemCategoryLog the paymentBillableItemCategoryLog to
	 *                                       set
	 */
	public void setPaymentBillableItemCategoryLog(PaymentBillableItemCategoryLog paymentBillableItemCategoryLog) {
		this.paymentBillableItemCategoryLog = paymentBillableItemCategoryLog;
	}

	/**
	 * METHOD UPDATED TO CHECK WHETHER NULL SO THAT INSTAINTED OBJECT IS RETURNED
	 * 
	 * @return the paymentBillableItemCategoryLogs
	 */
//	public List<PaymentBillableItemCategoryLog> getPaymentBillableItemCategoryLogs() {
//		if (this.paymentBillableItemCategoryLogs == null || this.paymentBillableItemCategoryLogs.isEmpty())
//			this.paymentBillableItemCategoryLogs = new ArrayList<PaymentBillableItemCategoryLog>();

//		return this.paymentBillableItemCategoryLogs;
//	}

	/**
	 * MANUAL METHOD TO GET LAST CATEGORY LOG IF SINGLE ENTRY AVAILABLE
	 * 
	 * @return
	 */
//	public PaymentBillableItemCategoryLog fetchLastPaymentBillableItemCategoryLog() {
//		PaymentBillableItemCategoryLog paymentBillableItemCategoryLog = null;
//
//		if (this.paymentBillableItemCategoryLogs != null || !this.paymentBillableItemCategoryLogs.isEmpty()) {
//			paymentBillableItemCategoryLog = this.paymentBillableItemCategoryLogs
//					.get(this.paymentBillableItemCategoryLogs.size());
//		}
//
//		return paymentBillableItemCategoryLog;
//	}

	/**
	 * @param paymentBillableItemCategoryLogs the paymentBillableItemCategoryLogs to
	 *                                        set
	 */
//	public void setPaymentBillableItemCategoryLogs(
//			List<PaymentBillableItemCategoryLog> paymentBillableItemCategoryLogs) {
//		this.paymentBillableItemCategoryLogs = paymentBillableItemCategoryLogs;
//	}

	/**
	 * MANUAL METHOD TO ATTACH PaymentBillableItemCategoryLog
	 * 
	 * @param paymentBillableItemCategoryLog
	 */
//	public void setLastPaymentBillableItemCategoryLog(PaymentBillableItemCategoryLog paymentBillableItemCategoryLog) {
//		if (this.paymentBillableItemCategoryLogs == null || this.paymentBillableItemCategoryLogs.isEmpty())
//			this.paymentBillableItemCategoryLogs = new ArrayList<PaymentBillableItemCategoryLog>();
//
//		if (paymentBillableItemCategoryLog.getPaymentBillableItemCategory() == null)
//			paymentBillableItemCategoryLog.setPaymentBillableItemCategory(this);
//
//		this.paymentBillableItemCategoryLogs.add(paymentBillableItemCategoryLog);
//	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public PaymentBillableItemCategory() {
	}

	public Integer getBillableItemCategoryId() {
		return billableItemCategoryId;
	}

	public void setBillableItemCategoryId(Integer billableItemCategoryId) {
		this.billableItemCategoryId = billableItemCategoryId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public java.sql.Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(java.sql.Date validFrom) {
		this.validFrom = validFrom;
	}

	public java.sql.Date getValidTill() {
		return validTill;
	}

	public void setValidTill(java.sql.Date validTill) {
		this.validTill = validTill;
	}

	public List<BillableItemRule> getBillingBillableItemRules() {
		return billingBillableItemRules;
	}

	public void setBillingBillableItemRules(List<BillableItemRule> billingBillableItemRules) {
		this.billingBillableItemRules = billingBillableItemRules;
	}

	public ContractType getBillingContractType() {
		return billingContractType;
	}

	public void setBillingContractType(ContractType billingContractType) {
		this.billingContractType = billingContractType;
	}

	/**
	 * @author lrit-billing
	 * @return
	 */
	@Override
	public String toString() {
		return "PaymentBillableItemCategory [billableItemCategoryId=" + billableItemCategoryId + ", itemName="
				+ itemName + ", rate=" + rate + ", validFrom=" + validFrom + ", validTill=" + validTill + ", entryDate="
				+ entryDate + ", remarks=" + remarks + ", billingBillableItemRules=" + billingBillableItemRules
				+ ", billingContractType=" + billingContractType + "]";
	}

}