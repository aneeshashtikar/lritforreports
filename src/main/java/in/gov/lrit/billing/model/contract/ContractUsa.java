package in.gov.lrit.billing.model.contract;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import java.sql.Timestamp;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author lrit-billing
 *
 */
/**
 * The persistent class for the billing_usa_contract database table.
 * 
 */
@Entity
@Table(name = "billing_usa_contract")
@NamedQuery(name = "BillingUsaContract.findAll", query = "SELECT b FROM ContractUsa b")
public class ContractUsa extends Contract implements Serializable {
	private static final long serialVersionUID = 1L;


	
	@NotNull(message = "{contractusa.amendment.number}")
	@Column(name="amendment_number")
	private String amendmentNumber;


	@NotNull(message = "{contractusa.award.amount}")
	//@Min(value = 1L,message = "{contractusa.award.amount}")
	@DecimalMin(value = "0.1", inclusive = true,message = "{contractusa.award.amount}")
	@Column(name="award_amount")
	private double awardAmount;

	
	@NotNull(message = "{contractusa.contract.specialist}")
	@Column(name="contract_specialist")
	private String contractSpecialist;


	@NotNull(message = "{contractusa.routing.code}")
	@Column(name="routing_code")
	private String routingCode;

	
	//@Min(value = 1L,message = "{contractusa.sam.number}")
	@Column(name="sam_registration_number")
	private Integer samRegistrationNumber;

	@NotNull(message = "{contractusa.sam.date}")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@Column(name = "sam_registration_date")
	private Date samRegistrationDate;

	public String getAmendmentNumber() {
		return amendmentNumber;
	}

	public void setAmendmentNumber(String amendmentNumber) {
		this.amendmentNumber = amendmentNumber;
	}

	public double getAwardAmount() {
		return awardAmount;
	}

	public void setAwardAmount(double awardAmount) {
		this.awardAmount = awardAmount;
	}

	public String getContractSpecialist() {
		return contractSpecialist;
	}

	public void setContractSpecialist(String contractSpecialist) {
		this.contractSpecialist = contractSpecialist;
	}

	public String getRoutingCode() {
		return routingCode;
	}

	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}

	public Integer getSamRegistrationNumber() {
		return samRegistrationNumber;
	}

	public void setSamRegistrationNumber(Integer samRegistrationNumber) {
		this.samRegistrationNumber = samRegistrationNumber;
	}

	public Date getSamRegistrationDate() {
		return samRegistrationDate;
	}

	public void setSamRegistrationDate(Date samRegistrationDate) {
		this.samRegistrationDate = samRegistrationDate;
	}

}