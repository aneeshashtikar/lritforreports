package in.gov.lrit.billing.model.billingservice;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.payment.PaymentDetailsForService;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;

/**
 * The persistent class for the billing_service database table.
 * 
 */
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Entity
@Table(name = "billing_service")
@NamedQuery(name = "BillingService.findAll", query = "SELECT b FROM Service b")
@Proxy(lazy = false)
public class Service implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "billing_billing_service_seq_gen", sequenceName = "billing_billing_service", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "billing_billing_service_seq_gen")
	@Column(name = "billing_service_id")
	private Integer billingServiceId;

	@CreationTimestamp
	private java.util.Date date;

	// @NotNull(message = "{service.fromdate.notnull}")
	@Column(name = "from_date")

	/* @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) */
	@NotNull(message = "{service.fromdate.notnull}")
	private Date fromDate;

	@DecimalMin(value = "0.1", inclusive = true, message = "{service.cost.min}")
	@Column(name = "grand_total")
	private double grandTotal;

	private double levies;

	// @DecimalMin(value = "0.1", inclusive = true, message = "{service.cost.min}")
	@Column(name = "sub_total")
	private double subTotal;

	private double taxes;

	@Column(name = "to_date")

	/* @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) */
	@NotNull(message = "{service.todate.notnull}")
	private Date toDate;

	// bi-directional many-to-one association to BillingBillableItem
	@OneToMany(mappedBy = "billingService", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	@JsonManagedReference
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<BillableItem> billingBillableItems;

	// bi-directional many-to-one association to BillingPaymentDetailsForService
	@OneToMany(mappedBy = "billingService", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = false)
	@JsonManagedReference
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PaymentDetailsForService> billingPaymentDetailsForServices;

	// bi-directional many-to-one association to BillingReminderLog
	@OneToMany(mappedBy = "billingService")
	@JsonManagedReference
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ReminderLog> billingReminderLogs;

	// uni-directional many-to-one association to BillingContract
	@ManyToOne
	@JoinColumn( name = "contract_id", referencedColumnName = "contract_id")
//	@JoinColumn(name = "contract_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Contract billingContract;

	// bi-directional many-to-one association to BillingServiceStatus
	@OneToMany(mappedBy = "billingService", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	@JsonManagedReference
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ServiceStatus> billingServiceStatuses;

	public Service() {
	}

	public java.util.Date getDate() {
		return this.date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public double getGrandTotal() {
		return this.grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public double getLevies() {
		return this.levies;
	}

	public void setLevies(double levies) {
		this.levies = levies;
	}

	public double getSubTotal() {
		return this.subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getTaxes() {
		return this.taxes;
	}

	public void setTaxes(double taxes) {
		this.taxes = taxes;
	}

	public Integer getBillingServiceId() {
		return billingServiceId;
	}

	public void setBillingServiceId(Integer billingServiceId) {
		this.billingServiceId = billingServiceId;
	}

	public List<BillableItem> getBillingBillableItems() {
		return billingBillableItems;
	}

	public void setBillingBillableItems(List<BillableItem> billingBillableItems) {
		this.billingBillableItems = billingBillableItems;
	}

	public List<PaymentDetailsForService> getBillingPaymentDetailsForServices() {
		return billingPaymentDetailsForServices;
	}

	public void setBillingPaymentDetailsForServices(List<PaymentDetailsForService> billingPaymentDetailsForServices) {
		this.billingPaymentDetailsForServices = billingPaymentDetailsForServices;
	}

	public List<ReminderLog> getBillingReminderLogs() {
		return billingReminderLogs;
	}

	public void setBillingReminderLogs(List<ReminderLog> billingReminderLogs) {
		this.billingReminderLogs = billingReminderLogs;
	}

	public Contract getBillingContract() {
		return billingContract;
	}

	public void setBillingContract(Contract billingContract) {
		this.billingContract = billingContract;
	}

	public List<ServiceStatus> getBillingServiceStatuses() {
		return billingServiceStatuses;
	}

	public void setBillingServiceStatuses(List<ServiceStatus> billingServiceStatuses) {
		this.billingServiceStatuses = billingServiceStatuses;
	}

	@Override
	public String toString() {
		return "Service [billingServiceId=" + billingServiceId + ", date=" + date + ", fromDate=" + fromDate
				+ ", grandTotal=" + grandTotal + ", levies=" + levies + ", subTotal=" + subTotal + ", taxes=" + taxes
				+ ", toDate=" + toDate + ", billingContract=" + billingContract + "]";
	}

}