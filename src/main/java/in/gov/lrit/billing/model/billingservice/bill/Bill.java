package in.gov.lrit.billing.model.billingservice.bill;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import in.gov.lrit.billing.model.billingservice.Service;

/**
 * The persistent class for the billing_service_bill database table.
 * 
 */

/**
 * @author lrit-billing
 *
 */
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Entity
@Table(name = "billing_service_bill")
public class Bill extends Service implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "bill_no")
	private String billNo;

	private byte[] billdoc;

	public Bill() {
	}

	public String getBillNo() {
		return this.billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public byte[] getBilldoc() {
		return this.billdoc;
	}

	public void setBilldoc(byte[] billdoc) {
		this.billdoc = billdoc;
	}

	

}