package in.gov.lrit.billing.model.billingservice;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

/**
 * The persistent class for the billing_reminder_log database table.
 * 
 */
@Entity
@Table(name = "billing_reminder_log")
@NamedQuery(name = "BillingReminderLog.findAll", query = "SELECT b FROM ReminderLog b")
public class ReminderLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="billing_reminder_log_seq_gen",sequenceName="billing_reminder_log_seq", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="billing_reminder_log_seq_gen")
	@Column(name = "reminder_log_id")
	private Integer reminderLogId;

	private String remarks;

	@CreationTimestamp
	@Column(name = "send_date")
	private Timestamp sendDate;

	// bi-directional many-to-one association to BillingService
	@ManyToOne
	@JoinColumn(name = "billing_service_id")
	private Service billingService;

	public ReminderLog() {
	}

	public Integer getReminderLogId() {
		return this.reminderLogId;
	}

	public void setReminderLogId(Integer reminderLogId) {
		this.reminderLogId = reminderLogId;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Timestamp getSendDate() {
		return this.sendDate;
	}

	public void setSendDate(Timestamp sendDate) {
		this.sendDate = sendDate;
	}

	public Service getBillingService() {
		return this.billingService;
	}

	public void setBillingService(Service billingService) {
		this.billingService = billingService;
	}

}