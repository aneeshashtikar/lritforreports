/**
 * @PaymentBillableItemCategoryLog.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0
 */
package in.gov.lrit.billing.model.payment.billableitem;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author lrit-billing
 *
 */
@Entity
@Table(name = "billing_billable_item_category_log")
public class PaymentBillableItemCategoryLog {

	@Id
	@SequenceGenerator(name = "billing_billable_item_category_log_id_seq_gen", sequenceName = "billing_billable_item_category_log_id_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "billing_billable_item_category_log_id_seq_gen")
	@Column(name = "billing_billable_item_category_log_id")
	private Integer paymentBillableItemCategoryLogId;

	@ManyToOne()
	@JoinColumn(name = "billable_item_category_id")
	@JsonManagedReference
//	@LazyCollection(LazyCollectionOption.FALSE)
	private PaymentBillableItemCategory paymentBillableItemCategory;

	@CreationTimestamp
	@Column(name = "entry_date")
	private Date entryDate;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@Column(name = "valid_from")
	private Date validFrom;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@Column(name = "valid_till")
	private Date validTill;

	@Column(name = "rate")
	private Double rate;

	@Column(name = "remark")
	private String remark;

	/**
	 * @return the paymentBillableItemCategoryLogId
	 */
	public Integer getPaymentBillableItemCategoryLogId() {
		return paymentBillableItemCategoryLogId;
	}

	/**
	 * @param paymentBillableItemCategoryLogId the paymentBillableItemCategoryLogId
	 *                                         to set
	 */
	public void setPaymentBillableItemCategoryLogId(Integer paymentBillableItemCategoryLogId) {
		this.paymentBillableItemCategoryLogId = paymentBillableItemCategoryLogId;
	}

	/**
	 * @return the paymentBillableItemCategory
	 */
	public PaymentBillableItemCategory getPaymentBillableItemCategory() {
		return paymentBillableItemCategory;
	}

	/**
	 * @param paymentBillableItemCategory the paymentBillableItemCategory to set
	 */
	public void setPaymentBillableItemCategory(PaymentBillableItemCategory paymentBillableItemCategory) {
		this.paymentBillableItemCategory = paymentBillableItemCategory;
	}

	/**
	 * @return the entryDate
	 */
	public Date getEntryDate() {
		return entryDate;
	}

	/**
	 * @param entryDate the entryDate to set
	 */
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTill
	 */
	public Date getValidTill() {
		return validTill;
	}

	/**
	 * @param validTill the validTill to set
	 */
	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	/**
	 * @return the rate
	 */
	public Double getRate() {
		return rate;
	}

	/**
	 * @param rate the rate to set
	 */
	public void setRate(Double rate) {
		this.rate = rate;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @author lrit-billing
	 * @return
	 */
	@Override
	public String toString() {
		return "PaymentBillableItemCategoryLog [paymentBillableItemCategoryLogId=" + paymentBillableItemCategoryLogId
				+ ", paymentBillableItemCategory=" + paymentBillableItemCategory + ", entryDate=" + entryDate
				+ ", validFrom=" + validFrom + ", validTill=" + validTill + ", rate=" + rate + ", remark=" + remark
				+ "]";
	}

}
