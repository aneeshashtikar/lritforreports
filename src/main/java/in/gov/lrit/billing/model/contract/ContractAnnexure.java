package in.gov.lrit.billing.model.contract;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the billing_contract_annexure database table.
 * 
 */

/**
 * @author lrit-billing
 *
 */
@Entity
@Table(name="billing_contract_annexure")
@NamedQuery(name="BillingContractAnnexure.findAll", query="SELECT b FROM ContractAnnexure b")
public class ContractAnnexure implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="billing_contract_annexures_seq_gen",sequenceName="billing_contract_annexures", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="billing_contract_annexures_seq_gen")
	@Column(name="contract_annexure_id")
	private Integer contractAnnexureId;

	@NotNull(message = "Upload Contract Document")
	@Column(name="annexure_document")
	private byte[] annexureDocument;
	
	
	
	private java.sql.Date date;

	private String remarks;


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contract_id")
	@JsonBackReference
	private Contract billingContract;

	public ContractAnnexure() {
	}

	public Integer getContractAnnexureId() {
		return this.contractAnnexureId;
	}

	public void setContractAnnexureId(Integer contractAnnexureId) {
		this.contractAnnexureId = contractAnnexureId;
	}

	public byte[] getAnnexureDocument() {
		return this.annexureDocument;
	}

	public void setAnnexureDocument(byte[] annexureDocument) {
		this.annexureDocument = annexureDocument;
	}




	/**
	 * @return the date
	 */
	public java.sql.Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Contract getBillingContract() {
		return this.billingContract;
	}

	public void setBillingContract(Contract billingContract) {
		this.billingContract = billingContract;
	}

}