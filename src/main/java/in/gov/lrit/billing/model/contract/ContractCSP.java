package in.gov.lrit.billing.model.contract;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * @author lrit-billing
 *
 */
/**
 * The persistent class for the billing_csp_contract database table.
 * 
 */
@Entity
@Table(name = "billing_csp_contract")
public class ContractCSP extends Contract implements Serializable {
	private static final long serialVersionUID = 1L;

	@DecimalMin(value = "0.1", inclusive = true, message = "{contractcsp.management.charges}")
	@Column(name = "management_charges")
	private double managementCharges;

	public ContractCSP() {
	}

	public double getManagementCharges() {
		return this.managementCharges;
	}

	public void setManagementCharges(double managementCharges) {
		this.managementCharges = managementCharges;
	}


}