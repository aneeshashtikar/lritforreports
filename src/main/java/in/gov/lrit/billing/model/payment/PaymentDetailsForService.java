package in.gov.lrit.billing.model.payment;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import in.gov.lrit.billing.model.billingservice.Service;

/**
 * @author lrit-billing
 *
 */
/**
 * The persistent class for the billing_payment_details_for_service database
 * table.
 * 
 */
@Entity
@Table(name = "billing_payment_details_for_service")
@NamedQuery(name = "BillingPaymentDetailsForService.findAll", query = "SELECT b FROM PaymentDetailsForService b")
public class PaymentDetailsForService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="billing_payment_details_for_service_seq_gen",sequenceName="billing_payment_details_for_service_seq", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="billing_payment_details_for_service_seq_gen")
	@Column(name = "pmt_det_service_id")
	private Integer pmtDetServiceId;

	@Column(name = "paid_amount")
	private double paidAmount;

	//@CreationTimestamp
	@Column(name = "payment_date")
	private Date paymentDate;

	@Column(name = "transaction_info")
	private String transactionInfo;

	// bi-directional many-to-one association to BillingService
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "billing_service_id")
	@JsonBackReference
	private Service billingService;

	// bi-directional many-to-one association to BillingPaymentDetailsPerMember
	@OneToMany(mappedBy = "billingPaymentDetailsForService", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE }, orphanRemoval = true)
	@JsonManagedReference
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PaymentDetailsPerMember> billingPaymentDetailsPerMembers;
	
	
	@Column(name = "payment_advice")
	private byte[] paymentAdviceDocument;

	public PaymentDetailsForService() {
	}

	public Integer getPmtDetServiceId() {
		return this.pmtDetServiceId;
	}

	public void setPmtDetServiceId(Integer pmtDetServiceId) {
		this.pmtDetServiceId = pmtDetServiceId;
	}

	public double getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getTransactionInfo() {
		return this.transactionInfo;
	}

	public void setTransactionInfo(String transactionInfo) {
		this.transactionInfo = transactionInfo;
	}

	public Service getBillingService() {
		return this.billingService;
	}

	public void setBillingService(Service billingService) {
		this.billingService = billingService;
	}

	public List<PaymentDetailsPerMember> getBillingPaymentDetailsPerMembers() {
		return this.billingPaymentDetailsPerMembers;
	}

	public void setBillingPaymentDetailsPerMembers(List<PaymentDetailsPerMember> billingPaymentDetailsPerMembers) {
		this.billingPaymentDetailsPerMembers = billingPaymentDetailsPerMembers;
	}

	public PaymentDetailsPerMember addBillingPaymentDetailsPerMember(
			PaymentDetailsPerMember billingPaymentDetailsPerMember) {
		getBillingPaymentDetailsPerMembers().add(billingPaymentDetailsPerMember);
		billingPaymentDetailsPerMember.setBillingPaymentDetailsForService(this);

		return billingPaymentDetailsPerMember;
	}

	public PaymentDetailsPerMember removeBillingPaymentDetailsPerMember(
			PaymentDetailsPerMember billingPaymentDetailsPerMember) {
		getBillingPaymentDetailsPerMembers().remove(billingPaymentDetailsPerMember);
		billingPaymentDetailsPerMember.setBillingPaymentDetailsForService(null);

		return billingPaymentDetailsPerMember;
	}
	
	

	public byte[] getPaymentAdviceDocument() {
		return paymentAdviceDocument;
	}

	public void setPaymentAdviceDocument(byte[] paymentAdviceDocument) {
		this.paymentAdviceDocument = paymentAdviceDocument;
	}

	@Override
	public String toString() {
		return "PaymentDetailsForService [pmtDetServiceId=" + pmtDetServiceId + ", paidAmount=" + paidAmount
				+ ", paymentDate=" + paymentDate + ", transactionInfo=" + transactionInfo + ", billingService="
				+ billingService + ", billingPaymentDetailsPerMembers=" + billingPaymentDetailsPerMembers + "]";
	}
	
}