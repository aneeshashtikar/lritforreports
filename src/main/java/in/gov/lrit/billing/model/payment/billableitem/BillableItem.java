package in.gov.lrit.billing.model.payment.billableitem;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

import in.gov.lrit.billing.model.billingservice.Service;

/**
 * The persistent class for the billing_billable_item database table.
 * 
 */
@Entity
@Table(name = "billing_billable_item")
public class BillableItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "billing_billable_items_seq_gen", sequenceName = "billing_billable_items", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "billing_billable_items_seq_gen")
	@Column(name = "billable_item_id")
	private Integer billableItemId;

	private String consumer;

	@Transient
	private String consumerName;

	private double cost;

	@Column(name = "date_range_from")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date dateRangeFrom;

	@Column(name = "date_range_to")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date dateRangeTo;

	@Column(name = "manual_count")
	private Integer manualCount;

	private String provider;

	@Transient
	private String providerName;

	@Column(name = "system_count")
	private Integer systemCount;

	// bi-directional many-to-one association to BillingPaymentBillableItemCategory
	@ManyToOne
	@JoinColumn(name = "billable_item_category_id")
	private PaymentBillableItemCategory billingPaymentBillableItemCategory;

	// bi-directional many-to-one association to BillingService
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "billing_service_id")
	@JsonBackReference
	private Service billingService;

	@Transient
	private double rate;
	
	
	
	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public BillableItem() {
	}

	public Integer getBillableItemId() {
		return billableItemId;
	}

	public void setBillableItemId(Integer billableItemId) {
		this.billableItemId = billableItemId;
	}

	public String getConsumer() {
		return consumer;
	}

	/**
	 * @return the consumerName
	 */
	public String getConsumerName() {
		return consumerName;
	}

	/**
	 * @param consumerName the consumerName to set
	 */
	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	/**
	 * @return the providerName
	 */
	public String getProviderName() {
		return providerName;
	}

	/**
	 * @param providerName the providerName to set
	 */
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public Date getDateRangeFrom() {
		return dateRangeFrom;
	}

	public void setDateRangeFrom(Date dateRangeFrom) {
		this.dateRangeFrom = dateRangeFrom;
	}

	public Date getDateRangeTo() {
		return dateRangeTo;
	}

	public void setDateRangeTo(Date dateRangeTo) {
		this.dateRangeTo = dateRangeTo;
	}

	public Integer getManualCount() {
		return manualCount;
	}

	public void setManualCount(Integer manualCount) {
		this.manualCount = manualCount;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Integer getSystemCount() {
		return systemCount;
	}

	public void setSystemCount(Integer systemCount) {
		this.systemCount = systemCount;
	}

	public PaymentBillableItemCategory getBillingPaymentBillableItemCategory() {
		return billingPaymentBillableItemCategory;
	}

	public void setBillingPaymentBillableItemCategory(PaymentBillableItemCategory billingPaymentBillableItemCategory) {
		this.billingPaymentBillableItemCategory = billingPaymentBillableItemCategory;
	}

	public Service getBillingService() {
		return billingService;
	}

	public void setBillingService(Service billingService) {
		this.billingService = billingService;
	}

	@Override
	public String toString() {
		return "BillableItem [billableItemId=" + billableItemId + ", consumer=" + consumer + ", consumerName="
				+ consumerName + ", cost=" + cost + ", dateRangeFrom=" + dateRangeFrom + ", dateRangeTo=" + dateRangeTo
				+ ", manualCount=" + manualCount + ", provider=" + provider + ", providerName=" + providerName
				+ ", systemCount=" + systemCount + ", billingPaymentBillableItemCategory="
				+ billingPaymentBillableItemCategory + ", rate=" + rate + "]";
	}


	


}