package in.gov.lrit.billing.model.contract;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * @author lrit-billing
 *
 */
/**
 * The persistent class for the billing_csp_contract database table.
 * 
 */
@Entity
@Table(name = "billing_shippingcompany_contract")
public class ContractShippingCompany extends Contract implements Serializable {
	private static final long serialVersionUID = 1L;

	@DecimalMin(value = "0.0", inclusive = true, message = "{contractcsp.management.charges}")
	@NotNull(message = "Messages charges can not be null")
	@Column(name = "management_charges")
	private double managementCharges;

	public ContractShippingCompany() {
	}

	public double getManagementCharges() {
		return this.managementCharges;
	}

	public void setManagementCharges(double managementCharges) {
		this.managementCharges = managementCharges;
	}

}