/**
 * @InvoiceDocumentLog.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0 18-Nov-2019
 */
package in.gov.lrit.billing.model.billingservice.invoice;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

import in.gov.lrit.billing.model.billingservice.Service;

/**
 * 
 * @author lrit-billing
 *
 */
public class InvoiceDocumentLog {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "invoice_document_log_id")
	private Integer invoiceDocumentLogId;

	@Column(name = "approved_copy")
	private byte[] approvedCopy;

	@Column(name = "generated_copy")
	private byte[] generatedCopy;

	@CreationTimestamp
	private java.util.Date date;

	@ManyToOne
	@JoinColumn(name = "billing_service_id")
	private Service billingService;
}
