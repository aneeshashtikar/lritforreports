package in.gov.lrit.billing.model.payment.billableitem;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the billing_billable_item_rules database table.
 * 
 */
@Entity
@Table(name="billing_billable_item_rules")
@NamedQuery(name="BillingBillableItemRule.findAll", query="SELECT b FROM BillableItemRule b")
public class BillableItemRule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="billable_item_rules_id")
	private Integer billableItemRulesId;

	@Column(name="access_type")
	private Integer accessType;

	@Column(name="msg_type")
	private Integer msgType;

	@Column(name="receipt_code")
	private Integer receiptCode;

	@Column(name="reference_msg_type")
	private Integer referenceMsgType;

	@Column(name="reference_request_type")
	private Integer referenceRequestType;

	@Column(name="request_type")
	private Integer requestType;

	//bi-directional many-to-one association to BillingPaymentBillableItemCategory
	@ManyToOne
	@JoinColumn(name="billable_item_category_id")
	private PaymentBillableItemCategory billingPaymentBillableItemCategory;

	public BillableItemRule() {
	}

	public Integer getBillableItemRulesId() {
		return this.billableItemRulesId;
	}

	public void setBillableItemRulesId(Integer billableItemRulesId) {
		this.billableItemRulesId = billableItemRulesId;
	}

	public Integer getAccessType() {
		return this.accessType;
	}

	public void setAccessType(Integer accessType) {
		this.accessType = accessType;
	}

	public Integer getMsgType() {
		return this.msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}

	public Integer getReceiptCode() {
		return this.receiptCode;
	}

	public void setReceiptCode(Integer receiptCode) {
		this.receiptCode = receiptCode;
	}

	public Integer getReferenceMsgType() {
		return this.referenceMsgType;
	}

	public void setReferenceMsgType(Integer referenceMsgType) {
		this.referenceMsgType = referenceMsgType;
	}

	public Integer getReferenceRequestType() {
		return this.referenceRequestType;
	}

	public void setReferenceRequestType(Integer referenceRequestType) {
		this.referenceRequestType = referenceRequestType;
	}

	public Integer getRequestType() {
		return this.requestType;
	}

	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	public PaymentBillableItemCategory getBillingPaymentBillableItemCategory() {
		return this.billingPaymentBillableItemCategory;
	}

	public void setBillingPaymentBillableItemCategory(PaymentBillableItemCategory billingPaymentBillableItemCategory) {
		this.billingPaymentBillableItemCategory = billingPaymentBillableItemCategory;
	}

}