package in.gov.lrit.billing.model.contract;

import java.io.Serializable;
import javax.persistence.*;

/**
 * @author lrit-billing
 *
 */

/**
 * The persistent class for the billing_contract_type database table.
 * 
 */
@Entity
@Table(name="billing_contract_type")
@NamedQuery(name="BillingContractType.findAll", query="SELECT b FROM ContractType b")
public class ContractType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="contract_type_id")
	private Integer contractTypeId;

	private String typename;

	public ContractType() {
	}

	public Integer getContractTypeId() {
		return this.contractTypeId;
	}

	public void setContractTypeId(Integer contractTypeId) {
		this.contractTypeId = contractTypeId;
	}

	public String getTypename() {
		return this.typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	@Override
	public String toString() {
		return "ContractType [contractTypeId=" + contractTypeId + ", typename=" + typename + "]";
	}

}