package in.gov.lrit.billing.model.contract;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import in.gov.lrit.billing.model.contract.agency.Agency;

/**
 * The persistent class for the billing_contract database table.
 * 
 */

/**
 * @author lrit-billing
 *
 */
@Entity
@Table(name = "billing_contract")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Proxy(lazy = false)
public class Contract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "billing_contract_id_seq_gen", sequenceName = "billing_contract_id", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "billing_contract_id_seq_gen")
	@Column(name = "contract_id")
	private Integer contractId;

	@ManyToOne
	@JoinColumn(name = "agency_code1", referencedColumnName = "agency_code")
	private Agency agency1;

	@ManyToOne
	@JoinColumn(name = "agency_code2", referencedColumnName = "agency_code")
	private Agency agency2;

	@Transient
	private String agency2Name;

	public String getAgency2Name() {
		return agency2Name;
	}

	public void setAgency2Name(String agency2Name) {
		this.agency2Name = agency2Name;
	}

	@Column(name = "contract_document")
	private byte[] contractDocument;

	@NotNull(message = "{contract.length}")
	@Column(name = "contract_number")
	private String contractNumber;

	@NotNull(message = "{contract.valid.from}")
	@Column(name = "valid_from")


	// @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date validFrom;

	@NotNull(message = "{contract.valid.to}")
	@Column(name = "valid_to")
	// @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date validTo;

	private Boolean validity;

	@ManyToOne(/* cascade = javax.persistence.CascadeType.PERSIST */)
	@JoinColumn(name = "contract_type_id")
	private ContractType billingContractType;

	@JsonManagedReference
	@Cascade({ CascadeType.ALL })
	@Transient
	@OneToMany(mappedBy = "billingContract")
	private List<ContractStakeholder> contractStakeholders;

	// @Cascade({CascadeType.ALL})
	// @Transient
	// @OneToMany(mappedBy = "billingContract")
	// private List<ContractStakeholder> contractStakeholders2;
	// bi-directional many-to-one association to ContractAnnexure
	// @JsonManagedReference
	// @Cascade({ CascadeType.ALL })
	// @Transient
	// @OneToMany(mappedBy = "billingContract")
	// bi-directional many-to-one association to BillingServiceStatus

	@OneToMany(mappedBy = "billingContract", cascade = { javax.persistence.CascadeType.MERGE,
			javax.persistence.CascadeType.PERSIST }, orphanRemoval = true)
	@JsonManagedReference
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ContractAnnexure> billingContractAnnexures;

	@ManyToOne
	@JoinColumn(name = "currency_type")
	private CurrencyType billingCurrencyType;

	
	@Transient
	private int contractLength;
	
	
	@Transient
	private List<ContractStakeholder> agency2ContractStakeholders;

	
	/**
	 * @return the currentContractStakeholders
	 */
	public List<ContractStakeholder> getCurrentContractStakeholders() {
		return agency2ContractStakeholders;
	}

	/**
	 * @param currentContractStakeholders the currentContractStakeholders to set
	 */
	public void setCurrentContractStakeholders(List<ContractStakeholder> agency2ContractStakeholders) {
		this.agency2ContractStakeholders = agency2ContractStakeholders;
	}

	/**
	 * @return the contractLength
	 */
	public int getContractLength() {
		return contractLength;
	}

	/**
	 * @param contractLength the contractLength to set
	 */
	public void setContractLength(int contractLength) {
		this.contractLength = contractLength;
	}

	/**
	 * @return the billingCurrencyType
	 */
	public CurrencyType getBillingCurrencyType() {
		return billingCurrencyType;
	}

	/**
	 * @param billingCurrencyType the billingCurrencyType to set
	 */
	public void setBillingCurrencyType(CurrencyType billingCurrencyType) {
		this.billingCurrencyType = billingCurrencyType;
	}

	public Contract() {
	}

	/**
	 * @return the contractId
	 */
	public Integer getContractId() {
		return contractId;
	}

	/**
	 * @param contractId the contractId to set
	 */
	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	/**
	 * @return the agency1
	 */
	public Agency getAgency1() {
		return agency1;
	}

	/**
	 * @param agency1 the agency1 to set
	 */
	public void setAgency1(Agency agency1) {
		this.agency1 = agency1;
	}

	/**
	 * @return the agency2
	 */
	public Agency getAgency2() {
		return agency2;
	}

	/**
	 * @param agency2 the agency2 to set
	 */
	public void setAgency2(Agency agency2) {
		this.agency2 = agency2;
	}

	/**
	 * @return the contractDocument
	 */
	public byte[] getContractDocument() {
		return contractDocument;
	}

	/**
	 * @param contractDocument the contractDocument to set
	 */
	public void setContractDocument(byte[] contractDocument) {
		this.contractDocument = contractDocument;
	}

	/**
	 * @return the contractNumber
	 */
	public String getContractNumber() {
		return contractNumber;
	}

	/**
	 * @param contractNumber the contractNumber to set
	 */
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the validity
	 */
	public Boolean getValidity() {
		return validity;
	}

	/**
	 * @param validity the validity to set
	 */
	public void setValidity(Boolean validity) {
		this.validity = validity;
	}

	/**
	 * @return the billingContractType
	 */
	public ContractType getBillingContractType() {
		return billingContractType;
	}

	/**
	 * @param billingContractType the billingContractType to set
	 */
	public void setBillingContractType(ContractType billingContractType) {
		this.billingContractType = billingContractType;
	}

	/**
	 * @return the billingContractAnnexures
	 */
	public List<ContractAnnexure> getBillingContractAnnexures() {
		return billingContractAnnexures;
	}

	/**
	 * @param billingContractAnnexures the billingContractAnnexures to set
	 */
	public void setBillingContractAnnexures(List<ContractAnnexure> billingContractAnnexures) {

		if (this.billingContractAnnexures == null) {
			this.billingContractAnnexures = billingContractAnnexures;
		}

		if (billingContractAnnexures != null) {
			// this.billingContractAnnexures.retainAll(billingContractAnnexures);
			this.billingContractAnnexures.addAll(billingContractAnnexures);

		}
		// this.billingContractAnnexures = billingContractAnnexures;
	}

	/**
	 * @return the contractStakeholders2
	 */
	public List<ContractStakeholder> getContractStakeholders() {
		return contractStakeholders;
	}

	/**
	 * @param contractStakeholders2 the contractStakeholders2 to set
	 */
	public void setContractStakeholders(List<ContractStakeholder> contractStakeholders) {
		this.contractStakeholders = contractStakeholders;
	}

	@Override
	public String toString() {
		return "Contract [contractId=" + contractId + ", agency1=" + agency1 + ", agency2=" + agency2 + ", agency2Name="
				+ agency2Name + ", contractNumber=" + contractNumber + ", validFrom=" + validFrom + ", validTo="
				+ validTo + ", validity=" + validity + ", billingContractType=" + billingContractType + "]";

	}


}