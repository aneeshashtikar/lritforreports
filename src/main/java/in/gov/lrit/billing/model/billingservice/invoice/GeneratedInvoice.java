package in.gov.lrit.billing.model.billingservice.invoice;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import in.gov.lrit.billing.model.billingservice.Service;

/**
 * The persistent class for the billing_generated_copy database table.
 * 
 */
@Entity
@Table(name = "billing_generated_invoice")
public class GeneratedInvoice {

	@Id
	@SequenceGenerator(name = "billing_generated_invoice_seq_gen", sequenceName = "billing_generated_invoice_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "billing_generated_invoice_seq_gen")
	@Column(name = "billing_generated_invoice_id")
	private Integer generatedInvoiceId;

	@Column(name = "generated_copy")
	private byte[] generatedCopy;

	@CreationTimestamp
	private Date date;

	@ManyToOne
	@JoinColumn(name = "billing_service_id", referencedColumnName = "billing_service_id")
	private Service service;

	public Integer getGeneratedInvoiceId() {
		return generatedInvoiceId;
	}

	public void setGeneratedInvoiceId(Integer generatedInvoiceId) {
		this.generatedInvoiceId = generatedInvoiceId;
	}

	public byte[] getGeneratedCopy() {
		return generatedCopy;
	}

	public void setGeneratedCopy(byte[] generatedCopy) {
		this.generatedCopy = generatedCopy;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@Override
	public String toString() {
		return "GeneratedInvoice [generatedInvoiceId=" + generatedInvoiceId + ", date=" + date + ", service=" + service
				+ "]";
	}

}
