package in.gov.lrit.billing.model.billingservice;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * The persistent class for the billing_service_status database table.
 * 
 */
@Entity
@Table(name = "billing_service_status")
@NamedQuery(name = "BillingServiceStatus.findAll", query = "SELECT b FROM ServiceStatus b")
public class ServiceStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="billing_service_status_seq_gen",sequenceName="billing_service_status_seq", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="billing_service_status_seq_gen")
	@Column(name = "service_status_id")
	private Integer serviceStatusId;

	@CreationTimestamp
	private Date date;

	@NotNull
	private String remarks;

	// bi-directional many-to-one association to BillingService
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "billing_service_id")
	@JsonBackReference
	private Service billingService;

	@Transient
	private String statusName;
	// uni-directional many-to-one association to BillingStatus
	@ManyToOne
	@JoinColumn(name = "status_id")
	private Status billingStatus;

	public ServiceStatus() {
	}
	
	 

	public String getStatusName() {
		return statusName;
	}



	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}



	public Integer getServiceStatusId() {
		return this.serviceStatusId;
	}

	public void setServiceStatusId(Integer serviceStatusId) {
		this.serviceStatusId = serviceStatusId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Service getBillingService() {
		return this.billingService;
	}

	public void setBillingService(Service billingService) {
		this.billingService = billingService;
	}

	public Status getBillingStatus() {
		return this.billingStatus;
	}

	public void setBillingStatus(Status billingStatus) {
		this.billingStatus = billingStatus;
	}

	@Override
	public String toString() {
		return "ServiceStatus [serviceStatusId=" + serviceStatusId + ", date=" + date + ", remarks=" + remarks
				+ ", billingService=" + billingService + ", billingStatus=" + billingStatus + "]";
	}

}