/**
 * @Agency.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 20, 2019
 */
package in.gov.lrit.billing.model.contract.agency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import in.gov.lrit.billing.model.contract.ContractType;

/**
 * The persistent class for the billing_agency database table.
 * 
 */
/**
 * @author lrit-billing
 *
 */
@Entity
@Table(name = "billing_agency")
@NamedQuery(name = "Agency.findAll", query = "SELECT a FROM Agency a")
public class Agency {


	@Column(name = "name")
	private String name;

	@Id
	@Column(name = "agency_code")
	private String agencyCode;

	// Mailing address for invoices and bills
	@NotNull(message = "{agency1.correspondenceAddress}")
	@Column(name = "correspondence_address")
	private String correspondenceAddress;


	@Column(name = "general_contact")
	private String generalContact;

	@NotNull(message = "{agency1.generalPhone}")
	@Column(name = "general_phone")
	private String generalPhone;

	
	@Email
	@Column(name = "general_email")
	private String generalEmail;

	@NotNull(message = "{agency1.generalFax}")
	@Column(name = "general_fax")
	private String generalFax;

	@NotNull(message = "{agency1.financialContact}")
	// Financial contact person
	@Column(name = "financial_contact")
	private String financialContact;

	@NotNull(message = "{agency1.financialPhone}")
	@Column(name = "financial_phone")
	private String financialPhone;

	@NotNull
	@NotEmpty
	@Email
	@Column(name = "financial_email")
	private String financialEmail;

	@NotNull(message = "{agency1.nameOfCompany}")
	@Column(name = "name_of_company")
	private String nameOfCompany;

	@NotNull(message = "{agency.transactionInstruction}")
	@Column(name = "transaction_instruction")
	private String transactionInstruction;

	@ManyToOne()
	@JoinColumn(name = "contract_type_id")
	private ContractType contractType;

	public ContractType getContractType() {
		return contractType;
	}

	public void setContractType(ContractType contractType) {
		this.contractType = contractType;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the agencyCode
	 */
	public String getAgencyCode() {
		return agencyCode;
	}

	/**
	 * @param agencyCode the agencyCode to set
	 */
	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	/**
	 * @return the correspondenceAddress
	 */
	public String getCorrespondenceAddress() {
		return correspondenceAddress;
	}

	/**
	 * @param correspondenceAddress the correspondenceAddress to set
	 */
	public void setCorrespondenceAddress(String correspondenceAddress) {
		this.correspondenceAddress = correspondenceAddress;
	}

	/**
	 * @return the generalContact
	 */
	public String getGeneralContact() {
		return generalContact;
	}

	/**
	 * @param generalContact the generalContact to set
	 */
	public void setGeneralContact(String generalContact) {
		this.generalContact = generalContact;
	}

	/**
	 * @return the generalPhone
	 */
	public String getGeneralPhone() {
		return generalPhone;
	}

	/**
	 * @param generalPhone the generalPhone to set
	 */
	public void setGeneralPhone(String generalPhone) {
		this.generalPhone = generalPhone;
	}

	/**
	 * @return the generalEmail
	 */
	public String getGeneralEmail() {
		return generalEmail;
	}

	/**
	 * @param generalEmail the generalEmail to set
	 */
	public void setGeneralEmail(String generalEmail) {
		this.generalEmail = generalEmail;
	}

	/**
	 * @return the generalFax
	 */
	public String getGeneralFax() {
		return generalFax;
	}

	/**
	 * @param generalFax the generalFax to set
	 */
	public void setGeneralFax(String generalFax) {
		this.generalFax = generalFax;
	}

	/**
	 * @return the financialContact
	 */
	public String getFinancialContact() {
		return financialContact;
	}

	/**
	 * @param financialContact the financialContact to set
	 */
	public void setFinancialContact(String financialContact) {
		this.financialContact = financialContact;
	}

	/**
	 * @return the financialPhone
	 */
	public String getFinancialPhone() {
		return financialPhone;
	}

	/**
	 * @param financialPhone the financialPhone to set
	 */
	public void setFinancialPhone(String financialPhone) {
		this.financialPhone = financialPhone;
	}

	/**
	 * @return the financialEmail
	 */
	public String getFinancialEmail() {
		return financialEmail;
	}

	/**
	 * @param financialEmail the financialEmail to set
	 */
	public void setFinancialEmail(String financialEmail) {
		this.financialEmail = financialEmail;
	}

	/**
	 * @return the nameOfCompany
	 */
	public String getNameOfCompany() {
		return nameOfCompany;
	}

	/**
	 * @param nameOfCompany the nameOfCompany to set
	 */
	public void setNameOfCompany(String nameOfCompany) {
		this.nameOfCompany = nameOfCompany;
	}

	/**
	 * @return the transactionInstruction
	 */
	public String getTransactionInstruction() {
		return transactionInstruction;
	}

	/**
	 * @param transactionInstruction the transactionInstruction to set
	 */
	public void setTransactionInstruction(String transactionInstruction) {
		this.transactionInstruction = transactionInstruction;
	}

	@Override
	public String toString() {
		return "Agency [name=" + name + ", agencyCode=" + agencyCode + ", correspondenceAddress="
				+ correspondenceAddress + ", generalContact=" + generalContact + ", generalPhone=" + generalPhone
				+ ", generalEmail=" + generalEmail + ", generalFax=" + generalFax + ", financialContact="
				+ financialContact + ", financialPhone=" + financialPhone + ", financialEmail=" + financialEmail
				+ ", nameOfCompany=" + nameOfCompany + ", transactionInstruction=" + transactionInstruction + "]";
	}

}
