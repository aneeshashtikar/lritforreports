package in.gov.lrit.billing.model.contract;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import in.gov.lrit.portal.model.user.PortalCsoDpaDetail;

import java.sql.Timestamp;
import java.util.List;


/**
 * @author lrit-billing
 *
 */
/**
 * The persistent class for the billing_asp_dc_contract database table.
 * 
 */
@Entity
@Table(name="billing_asp_dc_contract")
@NamedQuery(name="AspDcContract.findAll", query="SELECT a FROM ContractAspDc a")
public class ContractAspDc extends Contract implements Serializable  {
	private static final long serialVersionUID = 1L;

	@NotNull(message = "{contract.invoice.period.notnull}")
	@Min(value = 0L,message = "{contract.invoice.period}")
	@Column(name="invoice_due_period")
	private Integer invoiceDuePeriod;

	@Column(name="renewal_mode")
	private String renewalMode;

	@NotNull(message = "{contract.roi.late.payment.notnull}")
	@DecimalMin(value = "0.0",inclusive = true ,message = "{contract.roi}")
	@Column(name="roi_late_payment")
	private double roiLatePayment;

	@NotNull(message = "{contract.threshold.amount.notnull}")
	@DecimalMin(value = "0.0",inclusive = true ,message = "{contract.threshold.amount}")
	@Column(name="threshold_amount")
	private double thresholdAmount;

	@NotNull(message = "{contract.threshold.period.notnull}")
	@Min(value = 0L, message = "{contract.threshold.period}")
	@Column(name="threshold_period",nullable = false)
	private Integer thresholdPeriod;

	
	@Column(name="governing_law")
	private String governingLaw;
	


	public ContractAspDc() {
	}
	
	/**
	 * @return the invoiceDuePeriod
	 */
	public Integer getInvoiceDuePeriod() {
		return invoiceDuePeriod;
	}

	/**
	 * @param invoiceDuePeriod the invoiceDuePeriod to set
	 */
	public void setInvoiceDuePeriod(Integer invoiceDuePeriod) {
		this.invoiceDuePeriod = invoiceDuePeriod;
	}

	/**
	 * @return the renewalMode
	 */
	public String getRenewalMode() {
		return renewalMode;
	}

	/**
	 * @param renewalMode the renewalMode to set
	 */
	public void setRenewalMode(String renewalMode) {
		this.renewalMode = renewalMode;
	}

	/**
	 * @return the roiLatePayment
	 */
	public double getRoiLatePayment() {
		return roiLatePayment;
	}

	/**
	 * @param roiLatePayment the roiLatePayment to set
	 */
	public void setRoiLatePayment(double roiLatePayment) {
		this.roiLatePayment = roiLatePayment;
	}

	/**
	 * @return the thresholdAmount
	 */
	public double getThresholdAmount() {
		return thresholdAmount;
	}

	/**
	 * @param thresholdAmount the thresholdAmount to set
	 */
	public void setThresholdAmount(double thresholdAmount) {
		this.thresholdAmount = thresholdAmount;
	}

	/**
	 * @return the thresholdPeriod
	 */
	public Integer getThresholdPeriod() {
		return thresholdPeriod;
	}

	/**
	 * @param thresholdPeriod the thresholdPeriod to set
	 */
	public void setThresholdPeriod(Integer thresholdPeriod) {
		this.thresholdPeriod = thresholdPeriod;
	}

	/**
	 * @return the governingLaw
	 */
	public String getGoverningLaw() {
		return governingLaw;
	}

	/**
	 * @param governingLaw the governingLaw to set
	 */
	public void setGoverningLaw(String governingLaw) {
		this.governingLaw = governingLaw;
	}
	
}