package in.gov.lrit.billing.model.contract;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author lrit-billing
 *
 */
/**
 * The persistent class for the billing_currency_type database table.
 * 
 */
@Entity
@Table(name = "billing_currency_type")
public class CurrencyType implements Serializable {
	@Id
	@Column(name = "currency_type", columnDefinition="character varying (10)")
	private String currencyType; 
	
	@Column(name = "conversion_rate")
	private double conversionRate;

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public double getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(double conversionRate) {
		this.conversionRate = conversionRate;
	}

	@Override
	public String toString() {
		return "CurrencyType [currencyType=" + currencyType + ", conversionRate=" + conversionRate + "]";
	} 
	
	
	
}
