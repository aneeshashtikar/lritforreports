/**
 * @FileValidator.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 30-Sep-2019
 */
package in.gov.lrit.billing.validator.service;

import java.sql.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import in.gov.lrit.billing.model.billingservice.Service;

/**
 * @author Yogendra Yadav (YY)
 *
 */
@Component("serviceValidator")
public class ServiceValidator implements Validator {

	public static final String PDF_TYPE = "application/pdf";
	public static final long FIVE_MB_IN_BYTES = 5242880;

	/**
	 * @author Yogendra Yadav
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return false;
	}

	/**
	 * @author Yogendra Yadav
	 */
	@Override
	public void validate(Object target, Errors errors) {
		Service service = (Service) target;

		// System.out.println("IDFFD");
//		MultipartFile file = service.getFile();
//		if (file.isEmpty()) {
//			errors.rejectValue("file", "upload.file.required");
//		} else if (!PDF_TYPE.equalsIgnoreCase(file.getContentType())) {
//			errors.rejectValue("file", "upload.invalid.file.type");
//		} else if (file.getSize() > FIVE_MB_IN_BYTES) {
//			errors.rejectValue("file", "upload.exceeded.file.size");
//		}

		// System.out.println(file.getContentType() + file.getOriginalFilename() +
		// file.getName() + file.getSize());s

		Date validFrom = service.getFromDate();
		Date validTo = service.getToDate();
		// System.out.println("validFrom " + validFrom + " validTo " + validTo);
//		if (validFrom == null) {
//			errors.rejectValue("fromDate", "service.fromdate.notnull");
//		} else if (validTo == null) {
//			errors.rejectValue("toDate", "service.todate.notnull");
		if ((validFrom != null && validTo != null) && validFrom.compareTo(validTo) > 0) {
			// System.out.println("In If");
			errors.rejectValue("fromDate", "service.fromDate");
			System.out.println("validFrom is greater than validTo");
			// System.out.println("Compare to validFrom " + validFrom + " validTo " +
			// validTo);
		}

	}

}
