/**
 * @CustomFileValidator.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Sep 30, 2019
 */
package in.gov.lrit.billing.validator.contract;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import in.gov.lrit.billing.model.contract.Contract;

/**
 * For File validation
 * 
 * @author lrit_billing
 * 
 */
@Component("multipartFileValidator")
public class MultipartFileValidator implements Validator {

	
	public static final String PNG_MIME_TYPE="image/png";
	public static final long TEN_MB_IN_BYTES = 10485760;
	  
	@Override
	public boolean supports(Class<?> clazz) {
		 return MultipartFile.class.isAssignableFrom(clazz);
	}


	/**
	 * File Validation
	 * @author lrit-billing
	 * 
	 */

	@Override
	public void validate(Object target, Errors errors) {
		MultipartFile multipart = (MultipartFile)target;
        String contentType = multipart.getContentType();
        
		
        if(multipart.isEmpty()){
            errors.rejectValue("contractDocument", "upload.file.required");
        }else if(!contentType.equals("application/pdf")) {
        	errors.rejectValue("contractDocument", "upload.file.contenttype");
        }

	}
	

	
}
