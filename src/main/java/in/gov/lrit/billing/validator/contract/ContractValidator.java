package in.gov.lrit.billing.validator.contract;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import in.gov.lrit.billing.dao.contract.ContractRepository;
import in.gov.lrit.billing.dao.payment.billableitem.PaymentBillableItemCategoryLogRepository;
import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAspDc;
import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.contract.ContractUsa;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategoryLog;
import in.gov.lrit.billing.service.contract.ContractUsaServiceImpl;
/**
 * Contract Validator for providing validation of fields
 * 
 * @author lrit_billing
 * 
 */
@Component("contractValidator")
public class ContractValidator implements Validator {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractValidator.class);
	
	/** The email matcher. */
	private Matcher emailMatcher;

	/** The email pattern. */
	private Pattern emailPattern;

	private Matcher phoneMatcher;

	private Pattern phonePattern;

	/** The Constant EMAIL_REGEX. */
	private static final String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

	private static final String INTEGER_REGEX ="^[+]*[-\\s\\./0-9]*$";


	@Autowired
	private ContractRepository contractRepo; 
	
	@Autowired
	private PaymentBillableItemCategoryLogRepository paymentBillableItemCategoryLogRepo;
	
	

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Contract.class.isAssignableFrom(clazz);
	}


	public ContractValidator() {
		emailPattern = Pattern.compile(EMAIL_REGEX);
		phonePattern=Pattern.compile(INTEGER_REGEX);
	}


	/**
	 * To Validate the Contract Fields with Regular Expressions 
	 * @author lrit-billing
	 * 
	 */

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		Contract contract = (Contract)target; 

		Date validFrom = contract.getValidFrom();
		Date validTo = contract.getValidTo();
		
		if ( validFrom.compareTo(validTo) > 0 ) {
			errors.rejectValue("validTo","contract.validTo");

		}

		
		if ( contract instanceof ContractAspDc ) {
			ContractAspDc contractAspDc = (ContractAspDc) contract;
			long dateDiff = validTo.getTime() - validFrom.getTime();  
			long daysBetweenValidToValidFrom = TimeUnit.DAYS.convert(dateDiff, TimeUnit.MILLISECONDS)+1; 
			long thresholdPeriod = contractAspDc.getThresholdPeriod(); 
			
			if ( thresholdPeriod > daysBetweenValidToValidFrom ) {
				errors.rejectValue("thresholdPeriod", "contractaspdc.thresholdperiod.constraint");
			}
		}
		
		ContractType contractType = contract.getBillingContractType();
		List<PaymentBillableItemCategoryLog> paymentBillableItemCategoryLogs = paymentBillableItemCategoryLogRepo.getBiclByCtypeAndPeriod(contractType,validFrom,validTo); 
		logger.info("paymentBillableItemCategoryLogs"+paymentBillableItemCategoryLogs);
		if(paymentBillableItemCategoryLogs == null || paymentBillableItemCategoryLogs.size() < 1) {
			errors.rejectValue("validFrom", "contract.billableItem");
		} 
		
		String agency2Code = contract.getAgency2().getAgencyCode(); 
		
		logger.info("Agency2Code"+agency2Code);
		
		if (agency2Code.contains("-")) {
			String codes[] = agency2Code.split("-"); 
			agency2Code = codes[0]; 
		}
		List<Contract> listContracts = contractRepo.getContractBetweenDates(validFrom,validTo,agency2Code);
		
		
		if(listContracts.size() == 1) {
			Integer contractId = listContracts.get(0).getContractId();
			logger.info("ContractId"+contractId);
			if(!contractId.equals(contract.getContractId())) {
				errors.rejectValue("validFrom", "contract");
			} 
				
		} else if (listContracts.size() > 0) { 
			errors.rejectValue("validFrom", "contract");
		}	
		

		String currencyType = contract.getBillingCurrencyType().getCurrencyType();
		
		if(currencyType.equals("")) {
			errors.rejectValue("billingCurrencyType.currencyType", "billingCurrencyType.currencyType");
		}


		String agency1GeneralEmail = contract.getAgency1().getGeneralEmail();
		String agency1FinancialEmail = contract.getAgency1().getFinancialEmail();
		String agency2GeneralEmail = contract.getAgency2().getGeneralEmail();
		String agency2FinancialEmail = contract.getAgency2().getFinancialEmail();
		
		logger.info("agency1GeneralEmail"+agency1GeneralEmail);
		logger.info("agency1FinancialEmail"+agency1FinancialEmail);
		logger.info("agency2GeneralEmail"+agency2GeneralEmail);
		logger.info("agency1GeneralEmail"+agency1GeneralEmail);
		
		
		emailMatcher = emailPattern.matcher(agency1GeneralEmail);
		if (!(emailMatcher.matches())) {
			errors.rejectValue("agency1.generalEmail", "agency1.generalEmail");
		}
		emailMatcher = emailPattern.matcher(agency1FinancialEmail);
		if (!(emailMatcher.matches())) {
			errors.rejectValue("agency1.financialEmail", "agency1.financialEmail");
		}
		emailMatcher = emailPattern.matcher(agency2GeneralEmail);
		if (!(emailMatcher.matches())) {
			errors.rejectValue("agency2.generalEmail", "agency2.generalEmail");
		}
		emailMatcher = emailPattern.matcher(agency2FinancialEmail);
		if (!(emailMatcher.matches())) {
			errors.rejectValue("agency2.financialEmail", "agency2.financialEmail");
		}

		String agency1generalPhone = contract.getAgency1().getGeneralPhone();
		String agency1generalFax = contract.getAgency1().getGeneralFax();
		String agency1financialPhone = contract.getAgency1().getFinancialPhone();
		String agency2generalPhone = contract.getAgency2().getGeneralPhone();
		String agency2generalFax = contract.getAgency2().getGeneralFax();
		String agency2financialPhone = contract.getAgency2().getFinancialPhone();

		phoneMatcher = phonePattern.matcher(agency1generalPhone);
		if(!(phoneMatcher.matches())) {
			errors.rejectValue("agency1.generalPhone", "agency1.generalPhone");
		}
		phoneMatcher = phonePattern.matcher(agency1generalFax);
		if(!(phoneMatcher.matches())) {
			errors.rejectValue("agency1.generalFax", "agency1.generalFax");
		}
		phoneMatcher = phonePattern.matcher(agency1financialPhone);
		if(!(phoneMatcher.matches())) {
			errors.rejectValue("agency1.financialPhone", "agency1.financialPhone");
		}
		phoneMatcher = phonePattern.matcher(agency2generalPhone);
		if(!(phoneMatcher.matches())) {
			errors.rejectValue("agency2.generalPhone", "agency2.generalPhone");
		}
		phoneMatcher = phonePattern.matcher(agency2generalFax);
		if(!(phoneMatcher.matches())) {
			errors.rejectValue("agency1.generalFax", "agency1.generalFax");
		}
		phoneMatcher = phonePattern.matcher(agency2financialPhone);
		if(!(phoneMatcher.matches())) {
			errors.rejectValue("agency1.financialPhone", "agency1.financialPhone");
		}

	}






}
