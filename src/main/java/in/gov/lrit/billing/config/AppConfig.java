/**
 * @AppConfig.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 09-Sep-2019
 */
package in.gov.lrit.billing.config;

import java.text.SimpleDateFormat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Yogendra Yadav (YY)
 *
 */
@Configuration
public class AppConfig {

	@Bean
	public SimpleDateFormat simpleDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}
}
