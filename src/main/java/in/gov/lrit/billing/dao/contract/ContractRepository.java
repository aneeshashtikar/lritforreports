/**
 * @ContractRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.contract;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.billing.model.contract.Contract;
import in.gov.lrit.billing.model.contract.ContractAnnexure;
import in.gov.lrit.billing.model.contract.CurrencyType;
import in.gov.lrit.billing.model.contract.agency.Agency;

import in_.gov.lrit.billing.dto.ContractDto;


/**
 * Description :- Queries for Contract model
 * @author lrit-billing
 *
 */
public interface ContractRepository extends JpaRepository<Contract, Integer> {

	/**
	 * Query for fetch all Contract between given dates
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Contract a where (a.validFrom <= :validFrom) and (a.validTo >= :validTo)")
	public List<Contract> findAllByValidDates(@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	/**
	 * Query for getting all Contract except Shipping Company
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Contract a where a.billingContractType.contractTypeId NOT IN (4) and ((a.validFrom <= :validFrom) and (a.validTo >= :validTo)) and a.validity IS NULL")
	public List<Contract> findAllCGContractByDate(@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	/**
	 * Query for fetch all Contract From Contract Type and Given Dates
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Contract a where a.billingContractType.contractTypeId = :contractTypeId and ((a.validFrom <= :validFrom) and (a.validTo >= :validTo)) and a.validity IS NULL ")
	public List<Contract> findAllByContractTypeAndDate(@Param("contractTypeId") Integer contractTypeId,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	/**
	 * Query for getting all Agency Code by Contract Id and Given Dates
	 * @author lrit-billing
	 * 
	 */
	@Query("select a.agency2.agencyCode from Contract a where a.billingContractType.contractTypeId = :contractTypeId and ((a.validFrom <= :validFrom) and (a.validTo >= :validTo))")
	public List<String> findAllCompanyCodeBetweenDates(@Param("contractTypeId") Integer contractTypeId,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	/**
	 * Query for fetching all Contract by Agency Code and Given dates
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Contract a where ((a.validFrom <= :validFrom) and (a.validTo >= :validTo)) and a.agency2.agencyCode = :agencyCode")
	public List<Contract> findAllContractsOnAgencyCode(@Param("agencyCode") String agencyCode,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	/**
	 * Query for  fetching all Contract by Agency Code and Given dates and Contract ID
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Contract a where a.billingContractType.contractTypeId = :contractTypeId and ((a.validFrom <= :validFrom) and (a.validTo >= :validTo))	 and a.agency2.agencyCode = :agencyCode")
	public List<Contract> findAllContractsOnAgencyCode(@Param("contractTypeId") Integer contractTypeId,
			@Param("agencyCode") String agencyCode, @Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	/**
	 * Get all Contract by Dates
	 * @author lrit-billing
	 * 
	 */
	public Collection<Contract> findAllByValidFromBetween(Date validFrom, Date validTill);

	/**
	 * Query for  fetching contract by Contract ID
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Contract a where a.contractId = :contractId")
	public Contract findByContractId(@Param("contractId") Integer contractId);

	/**
	 * Query for  fetching all Contract by Contract Type
	 * @author lrit-billing
	 * 
	 */
	@Query("select c from Contract c where c.billingContractType.contractTypeId = :contractType")
	public List<Contract> getListContracts(@Param("contractType") Integer contractType);

	/**
	 * Query for  fetching list of Contracts by Contract Type and Given Dates
	 * @author lrit-billing
	 * 
	 */
	@Query("select c from Contract c where (c.billingContractType.contractTypeId = :contractType) and  (c.validFrom <= :validFrom) and (c.validTo >= :validTo)")
	public List<Contract> getListContracts(@Param("contractType") Integer contractType,
			@Param("validFrom") Date validFrom, @Param("validTo") Date validTo);

	/**
	 * Query for get Contract By Contract Id
	 * @author lrit-billing
	 * 
	 */
	@Query("select c from Contract c where c.contractId = :contractId")
	public Contract getContractByContractId(@Param("contractId") Integer contractId);

	/**
	 * Query for  terminate the contract By contract ID
	 * @author lrit-billing
	 * 
	 */
	@Transactional
	@Modifying
	@Query("update Contract c set c.validity = false where c.contractId = :contractId")
	public void terminateContractByContractId(@Param("contractId") Integer contractId);

	/**
	 * Query for fetching agency 1 Contract Stakeholders
	 * @author lrit-billing
	 * 
	 */
	@Query("select cs.memberId from ContractStakeholder cs where cs.billingContract.contractId = :contractId and cs.isagency1 = 't' ")
	public List<String> getAgency1ContractStakeholders(@Param("contractId") Integer contractId);

	/**
	 * Query for fetching agency 2 Contract Stakeholders
	 * @author lrit-billing
	 * 
	 */
	@Query("select cs.memberId from ContractStakeholder cs where cs.billingContract.contractId = :contractId and cs.isagency1 = 'f' ")
	public List<String> getAgency2ContractStakeholders(@Param("contractId") Integer contractId);

	/**
	 * Query for get Agency By agency Code
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Agency a where a.agencyCode like :agencyCode")
	public Agency getAgencyByAgencyCode(@Param("agencyCode") String agencyCode);

	/**
	 * Query for to get India as Agency
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Agency a where a.agencyCode like '3065'")
	public Agency getAgencyIndia();

	/**
	 * Query for fetching Contract between Dates and Agency
	 * @author lrit-billing
	 * 
	 */
	@Query("select c from Contract c where c.validFrom <= :validTo and c.validTo >= :validFrom and c.agency2.agencyCode = :agencyCode")
	public List<Contract> getContractBetweenDates(@Param("validFrom") Date validFrom, 
			@Param("validTo") Date validTo,@Param("agencyCode") String agencyCode);
	
	
	/**
	 * Query for Contract Dto by Contract Type
	 * @author lrit-billing
	 * 
	 */
	@Query("select new in_.gov.lrit.billing.dto.ContractDto(a.contractNumber,a.billingContractType.typename,a.validFrom, a.validTo,a.validity,a.agency2.name, a.contractId)"
			+ " from Contract a where a.billingContractType.contractTypeId = :contractType")
	public List<ContractDto> getAllContractsByContractType(@Param("contractType") Integer contractType);

	/**
	 * Query for  Contract Dto by Contract Type and given Dates
	 * @author lrit-billing
	 * 
	 */
	@Query("select new in_.gov.lrit.billing.dto.ContractDto(a.contractNumber,a.billingContractType.typename,a.validFrom, a.validTo,a.validity,a.agency2.name, a.contractId)"
			+ " from Contract a where (a.billingContractType.contractTypeId = :contractType) and  (a.validFrom <= :fromDate) and (a.validTo >= :toDate)")
	public List<ContractDto> getAllContractsByContractType(@Param("contractType") Integer contractType,@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	/**
	 * Query for get all annexure by contract Id
	 * @author lrit-billing
	 * 
	 */
	@Query("select ca from ContractAnnexure ca where billingContract.contractId = :contractId")
	public List<ContractAnnexure> getAllContractsAnnexureById(@Param("contractId") Integer contractId);

	/**
	 * Query for get annexure by annexure Id
	 * @author lrit-billing
	 * 
	 */
	@Query("select ca from ContractAnnexure ca where ca.contractAnnexureId = :annexureId")
	public ContractAnnexure getContractAnnexureById(@Param("annexureId")  Integer annexureId);

	/**
	 * @return
	 */
	@Query("select ct from CurrencyType ct")
	public List<CurrencyType> getAllCurrencyTypes();
	

	
}
