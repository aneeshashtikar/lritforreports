/**
 * @GeneratedInvoiceRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 12-Dec-2019
 */
package in.gov.lrit.billing.dao.billingservice.invoice;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.billingservice.invoice.GeneratedInvoice;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public interface GeneratedInvoiceRepository extends JpaRepository<GeneratedInvoice, Integer> {

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param serviceId
	 * @return
	 */
	@Query(value = "select * from billing_generated_invoice where billing_service_id = :serviceId order by date desc limit 1", nativeQuery = true)
	GeneratedInvoice getLatestGeneratedInvoice(@Param("serviceId") Integer serviceId);

	@Query("Select gen from GeneratedInvoice gen where gen.service.billingServiceId = :billingServiceId")
	List<GeneratedInvoice> findAllByService(@Param("billingServiceId") Integer billingServiceId);

}
