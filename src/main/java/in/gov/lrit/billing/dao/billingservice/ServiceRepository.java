/**
 * @ServiceRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.billingservice;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import in.gov.lrit.billing.model.billingservice.Service;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public interface ServiceRepository extends JpaRepository<Service, Integer> {

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param billingServiceId
	 * @return
	 */
	@Query("select bbi from Service i left join i.billingBillableItems bbi where i.billingServiceId = :billingServiceId")
	public List<BillableItem> getPaymentDetailMember(@Param("billingServiceId") Integer billingServiceId);

}
