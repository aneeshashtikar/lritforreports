/**
 * @GeneratedInvoiceRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 12-Dec-2019
 */
package in.gov.lrit.billing.dao.billingservice.invoice;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.billingservice.invoice.ApprovedInvoice;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public interface ApprovedInvoiceRepository extends JpaRepository<ApprovedInvoice, Integer> {

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param serviceId
	 * @return
	 */
	@Query(value = "select * from billing_approved_invoice where billing_service_id = :serviceId order by date desc limit 1", nativeQuery = true)
	ApprovedInvoice getLatestApprovedInvoice(@Param("serviceId") Integer serviceId);

	@Query("Select app from ApprovedInvoice app where app.service.billingServiceId = :billingServiceId")
	List<ApprovedInvoice> findAllByService(@Param("billingServiceId") Integer billingServiceId);
}
