/**
 * @ContractUsaRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.payment;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.billing.model.payment.PaymentDetailsPerMember;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public interface PaymentDetailsForMemberRepository extends JpaRepository<PaymentDetailsPerMember, Integer> {

}
