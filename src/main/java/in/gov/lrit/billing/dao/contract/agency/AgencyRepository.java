/**
 * @AgencyRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.contract.agency;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.contract.agency.Agency;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Description :- Queries related Agency model
 * @author lrit-billing
 *
 */
public interface AgencyRepository extends JpaRepository<Agency, String> {

	/**
	 * Query for Fetching Agency list by Contract Type
	 * @author lrit-billing
	 * 
	 */
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(ag.agencyCode, ag.name) from Agency ag where ag.contractType.contractTypeId = :contractType")
	List<LritNameDto> getAgencyListByContractType(@Param("contractType") Integer contractType);

}
