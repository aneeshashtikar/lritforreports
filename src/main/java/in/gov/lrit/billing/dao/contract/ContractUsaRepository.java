/**
 * @ContractUsaRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.contract;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.contract.ContractUsa;

/**
 * Description :- Queries for ContractUsa model
 * @author lrit-billing
 *
 */
public interface ContractUsaRepository extends JpaRepository<ContractUsa, Integer> {

	/**
	 * Query for terminate the Contract By Contract ID
	 * @author lrit-billing
	 * 
	 */
	@Query("update ContractUsa c set c.validity = false where c.contractId = :contractId")
	public void terminateContractByContractId(@Param("contractId") Integer contractId);

}
