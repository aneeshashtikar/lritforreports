/**
 * @ServiceBillRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.billingservice.bill;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.billingservice.bill.Bill;
import in.gov.lrit.billing.model.contract.ContractCSP;
import in_.gov.lrit.billing.dto.BillDto;

/**
 * Description :- Queries for Bill model
 * @author lrit-billing
 *
 */
public interface BillRepository extends JpaRepository<Bill, Integer> {

	/**
	 * Query for getting all Bills by Contract Type
	 * @author lrit-billing
	 * 
	 */
	@Query("Select new in_.gov.lrit.billing.dto.BillDto(a.billNo,a.fromDate, a.toDate, a.date, bss.billingStatus, a.billingServiceId, a.billingContract.billingContractType.contractTypeId)"
			+ " from Bill a INNER JOIN a.billingServiceStatuses bss where (a.billingContract.billingContractType.contractTypeId = :contractTypeId) "
			+ "and bss.date = (select max(date) from ServiceStatus where billingService.billingServiceId = bss.billingService.billingServiceId)")
	List<BillDto> findAllBillDtoByContractType(@Param("contractTypeId") Integer contractType);

	/**
	 * Query For getting all Bills by Contract Type,From and to Date
	 * @author lrit-billing
	 * 
	 */
	@Query("Select new in_.gov.lrit.billing.dto.BillDto(a.billNo,a.fromDate,a.toDate,a.date) from Bill a where "
			+ "((a.billingContract.billingContractType.contractTypeId = :contractTypeId) and (a.fromDate >= :fromDate and a.toDate <= :toDate))")
	List<BillDto> findAllBillDtoByContractTypeDate(@Param("contractTypeId") Integer contractType,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	/**
	 * Query for getting CSP Contract By ContractId
	 * @author lrit-billing
	 * 
	 */
	@Query("Select c from ContractCSP c where c.contractId = :contractId")
	ContractCSP getCSPContractByContractId(@Param("contractId") Integer contractId);

}
