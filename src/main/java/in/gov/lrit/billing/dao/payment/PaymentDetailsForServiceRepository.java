/**
 * @ContractUsaRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.payment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.payment.PaymentDetailsForService;

/**
 * @author lrit-billing
 *
 */
public interface PaymentDetailsForServiceRepository extends JpaRepository<PaymentDetailsForService, Integer> {

	@Query("select pmt from PaymentDetailsForService pmt where pmtDetServiceId = :pmtDetServiceId")
	PaymentDetailsForService getPaymentDetailsByPaymentServiceId(@Param("pmtDetServiceId") Integer pmtDetServiceId);

}
