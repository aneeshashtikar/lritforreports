/**
 * @ServiceBillCspRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.billingservice.bill;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.billing.model.billingservice.bill.BillCsp;

/**Description :- Queries for BillCSP model
 * @author lrit-billing
 *
 */
public interface BillCspRepository extends JpaRepository<BillCsp, Integer> {

}
