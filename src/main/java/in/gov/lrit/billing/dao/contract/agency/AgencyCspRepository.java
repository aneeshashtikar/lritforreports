/**
 * @AgencyRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.contract.agency;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.billing.model.contract.agency.Agency;

/**
 * @author lrit-billing
 *
 */
public interface AgencyCspRepository extends JpaRepository<Agency, Integer> {

}
