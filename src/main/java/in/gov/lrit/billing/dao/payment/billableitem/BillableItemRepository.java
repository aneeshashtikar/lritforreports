/**
 * @ContractUsaRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.payment.billableitem;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.payment.billableitem.BillableItem;

/**
 * @author lrit-billing
 *
 */
public interface BillableItemRepository extends JpaRepository<BillableItem, Integer> {

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param billingServiceId
	 * @return
	 */
	@Query("Select bi from BillableItem bi where bi.billingService.billingServiceId = :billingServiceId")
	public List<BillableItem> findAllByBillingServiceId(@Param("billingServiceId") Integer billingServiceId);
	
	@Query(value = "select count(*) from billing_billable_messagecount3 bbm where bbm.service_provider like :provider and bbm.service_requestor like :consumer and (bbm.message_timestamp between :from and :to) and bbm.bill_categoryid = :bic" , nativeQuery = true)
	public int getSystemCountForAspDc(@Param("provider") String provider, @Param("consumer") String consumer, @Param("from") Date from, @Param("to") Date to, @Param("bic") int bic); 
	
	@Query(value = "select count(*) from billing_billable_cspmessagecount3 bbc where bbc.service_provider like :provider and bbc.service_requestor like :consumer and (bbc.message_timestamp between :from and :to) and bbc.bill_categoryid = :bic" , nativeQuery = true)
	public int getSystemCountForCSP(@Param("provider") String provider, @Param("consumer") String consumer, @Param("from") Date from, @Param("to") Date to, @Param("bic") int bic);
	
}
