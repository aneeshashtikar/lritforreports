/**
 * @ContractShippingCompanyRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.contract;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.billing.model.contract.ContractShippingCompany;


/**
 * Description :- Queries for ContractShippingCompany model
 * @author lrit-billing
 *
 */

public interface ContractShippingCompanyRepository  extends JpaRepository<ContractShippingCompany, Integer> {

}
