/**
 * @ServiceInvoiceRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.billingservice.invoice;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.billingservice.invoice.Invoice;
import in.gov.lrit.billing.model.payment.billableitem.BillableItem;
import in_.gov.lrit.billing.dto.BillInvoiceListDto;
import in_.gov.lrit.billing.dto.InvoiceDto;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */

	@Query("Select a from Invoice a where ((a.billingContract.billingContractType.contractTypeId = :contractTypeId) and (a.fromDate >= :fromDate and a.toDate <= :toDate))")
	Collection<Invoice> findAllByContractTypeDate(@Param("contractTypeId") Integer contractType,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @return
	 */
	@Query("Select a from Invoice a where (a.billingContract.billingContractType.contractTypeId = :contractTypeId)")
	Collection<Invoice> findAllByContractType(@Param("contractTypeId") Integer contractType);

	/**
	 * RETURNS GENERIC INVOICE LIST ON CONTRACT TYPE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @return
	 */
	@Query("Select new in_.gov.lrit.billing.dto.BillInvoiceListDto(a.invoiceNo,	 a.fromDate, a.toDate, a.date, bss.billingStatus, a.billingServiceId) from Invoice a INNER JOIN a.billingServiceStatuses bss where (a.billingContract.billingContractType.contractTypeId = :contractTypeId) and bss.date = (select max(date) from ServiceStatus where billingService.billingServiceId = bss.billingService.billingServiceId)")
	List<BillInvoiceListDto> findAllBillInvoiceDtoByContractType(@Param("contractTypeId") Integer contractType);

	/**
	 * 
	 * RETURNS GENERIC INVOICE LIST ON CONTRACT TYPE AND GIVEN TIME DURATION
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Query("Select new in_.gov.lrit.billing.dto.BillInvoiceListDto(a.invoiceNo,	 a.fromDate, a.toDate, a.date, bss.billingStatus, a.billingServiceId) from Invoice a INNER JOIN a.billingServiceStatuses bss where (a.billingContract.billingContractType.contractTypeId = :contractTypeId) and bss.date = (select max(date) from ServiceStatus where billingService.billingServiceId = bss.billingService.billingServiceId) and (a.fromDate >= :fromDate and a.toDate <= :toDate)")
	List<BillInvoiceListDto> findAllBillInvoiceDtoByContractType(@Param("contractTypeId") Integer contractType,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	/**
	 * RETURNS INVOICE LIST ON CONTRACT TYPE
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @return
	 */
//	@Query("Select new in_.gov.lrit.billing.dto.InvoiceDto(a.invoiceNo, a.fromDate, a.toDate, a.date, bss.billingStatus, a.billingServiceId) from Invoice a INNER JOIN a.billingServiceStatuses bss")
	@Query("Select new in_.gov.lrit.billing.dto.InvoiceDto(a.invoiceNo, a.fromDate, a.toDate, a.date, bss.billingStatus, a.billingServiceId) from Invoice a INNER JOIN a.billingServiceStatuses bss where (a.billingContract.billingContractType.contractTypeId = :contractTypeId) and bss.date = (select max(date) from ServiceStatus ss where ss.billingService.billingServiceId = bss.billingService.billingServiceId)")
	List<InvoiceDto> findAllInvoiceDtoByContractType(@Param("contractTypeId") Integer contractType);

//	@Query("Select new in.gov.lrit.billing.model.billingservice.InvoiceDTO(a.invoiceNo, a.fromDate, a.toDate, a.date, bss.billingStatus, a.billingServiceId) from Invoice a INNER JOIN a.billingServiceStatuses bss where (a.billingContract.billingContractType.contractTypeId = :contractTypeId) and bss.date = (select max(date) from ServiceStatus ss where ss.billingService.billingServiceId = bss.billingService.billingServiceId)")
//	List<InvoiceDTO> findAllInvoiceDtoByContractType(@Param("contractTypeId") Integer contractType);

	/**
	 * 
	 * RETURNS INVOICE LIST ON CONTRACT TYPE AND GIVEN TIME DURATION
	 * 
	 * @author Yogendra Yadav
	 * @param contractType
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Query("Select new in_.gov.lrit.billing.dto.InvoiceDto(a.invoiceNo, a.fromDate, a.toDate, a.date, bss.billingStatus, a.billingServiceId) from Invoice a INNER JOIN a.billingServiceStatuses bss where (a.billingContract.billingContractType.contractTypeId = :contractTypeId) and bss.date = (select max(date) from ServiceStatus where billingService.billingServiceId = bss.billingService.billingServiceId) and (a.fromDate >= :fromDate and a.toDate <= :toDate)")
	List<InvoiceDto> findAllInvoiceDtoByContractType(@Param("contractTypeId") Integer contractType,
			@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query("select bbi from Invoice i left join i.billingBillableItems bbi where i.billingServiceId = :billingServiceId")
	public List<BillableItem> getPaymentDetailMember(@Param("billingServiceId") Integer billingServiceId);

	/**
	 * To Count Number of Invoice Generated Based On Given Year
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param year
	 * @return
	 */
	@Query("select count(*) from Invoice i where EXTRACT(YEAR From (i.date)) = :year")
	public Integer getInvoiceCount(@Param("year") Integer year);
}
