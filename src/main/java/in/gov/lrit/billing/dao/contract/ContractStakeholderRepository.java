/**
 * @ContractStakeholderRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 23, 2019
 */
package in.gov.lrit.billing.dao.contract;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.contract.ContractStakeholder;

/**
 * Description :- Queries for ContractStakeholder model
 * @author lrit-billing
 *
 */
public interface ContractStakeholderRepository extends JpaRepository<ContractStakeholder, Integer> {

	/**
	 * Query for providers by contract Id
	 * @author lrit-billing
	 * 
	 */
	@Query("Select cs.memberId from ContractStakeholder cs where cs.isagency1 = 'yes' and cs.billingContract.contractId = :contractId ")
	public List<String> getProviders(@Param("contractId") Integer contractId);

	/**
	 * Query for consumers by contract Id
	 * @author lrit-billing
	 * 
	 */
	@Query("Select cs.memberId from ContractStakeholder cs where cs.isagency1 = 'no' and cs.billingContract.contractId = :contractId ")
	public List<String> getConsumers(@Param("contractId") Integer contractId);
	
	/**
	 * Query for getting Contract Stakeholders by Contract Id and Agency Code
	 * @author lrit-billing
	 * 
	 */
	@Query("Select cs.memberId from ContractStakeholder cs where cs.isagency1 = :isAgency1 and cs.billingContract.contractId = :contractId ")
	public List<String> getContractStakeholders(@Param("contractId") Integer contractId, @Param("isAgency1") boolean isAgency1);
	
	/**
	 * Query for Contract Stakeholders list by Contract ID
	 * @author lrit-billing
	 * 
	 */
	@Query("Select cs from ContractStakeholder cs where cs.isagency1 = :isAgency1 and cs.billingContract.contractId = :contractId ")
	public List<ContractStakeholder> getContractStakeholderList(@Param("contractId") Integer contractId, @Param("isAgency1") boolean isAgency1);
	
	/**
	 * Query for distinct Contract Stakeholders
	 * @author lrit-billing
	 * 
	 */
	@Query("Select distinct(cs.memberId) from ContractStakeholder cs where cs.isagency1 = :isAgency1 and cs.billingContract.contractId = :contractId ")
	public Set<String> getDistinctContractStakeholders(@Param("contractId") Integer contractId, @Param("isAgency1") boolean isAgency1);
	
	/**
	 * Query for get Valid To date of Memeber Id(Contract Stakeholders)
	 * 
	 * @author lrit-billing
	 * 
	 */
	@Query(value = "select cs.valid_to from billing_contract_stakeholders cs where cs.member_id = :memberId order by valid_to desc limit 1", nativeQuery = true)
	public Date getLatestValidToByMemberId(@Param("memberId") String memberId);

	/**
	 * Query for Contract Stakeholder by member Id
	 * @author lrit-billing
	 * 
	 */
	@Query("Select cs from ContractStakeholder cs where cs.memberId = :memberId and cs.billingContract.contractId = :contractId")
	public ContractStakeholder getContractStakeholder(@Param("contractId") int contractId, @Param("memberId") String memberId);

	/**
	 * Query for Contract Stakeholders List by Date
	 * @author lrit-billing
	 * 
	 */
	@Query("Select cs from ContractStakeholder cs where cs.isagency1 = :isAgency1 and cs.billingContract.contractId = :contractId and cs.validFrom <= :toDate and cs.validTo >= :fromDate")
	public List<ContractStakeholder> getValidContractStakeholderListByDates(Integer contractId, boolean isAgency1,
			java.sql.Date fromDate, java.sql.Date toDate);

	/**
	 * Query for get latest Contract Stakeholders by Contract ID and Memeber ID
	 * @author lrit-billing
	 * 
	 */
	@Query("Select cs from ContractStakeholder cs where cs.isagency1 = :isAgency1 and cs.billingContract.contractId = :contractId and cs.validFrom = cs.billingContract.validFrom and cs.memberId = :memberId")
	public ContractStakeholder getLatestContractStakeholder(int contractId, String memberId);

	
}
