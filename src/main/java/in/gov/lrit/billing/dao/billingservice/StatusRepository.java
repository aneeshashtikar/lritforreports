/**
 * @StatusRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.billingservice;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.billing.model.billingservice.Status;

/**
 * @author Yogendra Yadav (YY)
 *
 */
@Repository
public interface StatusRepository extends JpaRepository<Status, Integer> {

}
