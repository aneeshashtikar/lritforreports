/**
 * @ReminderLogRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.billing.model.billingservice.ReminderLog;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public interface ReminderLogRepository extends JpaRepository<ReminderLog, Integer> {

}
