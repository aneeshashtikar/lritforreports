/**
 * @ContractCSPRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.contract;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.contract.ContractCSP;
import in.gov.lrit.billing.model.contract.agency.Agency;
import in.gov.lrit.billing.model.contract.ContractCSP;

/**
 * Description :- Queries for ContractCSP model
 * @author lrit-billing
 *
 */
public interface ContractCSPRepository extends JpaRepository<ContractCSP, Integer> {

	/**
	 * Query for Terminate the CSP Contract
	 * @author lrit-billing
	 * 
	 */
	@Query("update ContractCSP c set c.validity = false where c.contractId = :contractId")
	public void terminateContractByContractId(@Param("contractId") Integer contractId);

	/**
	 * Query for getting CSP from Agency
	 * @author lrit-billing
	 * 
	 */
	@Query("select a from Agency a where agencyCode like '0000'")
	public Agency getAgencyCSP();


}
