/**
 * @PaymentBillableItemCategoryLogRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author lrit-billing
 * @version 1.0
 */
package in.gov.lrit.billing.dao.payment.billableitem;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import in.gov.lrit.billing.model.contract.ContractType;

import in.gov.lrit.billing.model.contract.ContractType;
import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategory;

import in.gov.lrit.billing.model.payment.billableitem.PaymentBillableItemCategoryLog;

/**
 * @author yogendra
 *
 */
public interface PaymentBillableItemCategoryLogRepository
		extends JpaRepository<PaymentBillableItemCategoryLog, Integer> {

	@Query(value = "Select * from billing_billable_item_category_log where billable_item_category_id = :billableItemCategoryId  order by entry_date desc limit 1", nativeQuery = true)
	public PaymentBillableItemCategoryLog getLatestUpdatedCategoryLog(
			@Param("billableItemCategoryId") Integer billableItemCategoryId);

	@Query("select plog from PaymentBillableItemCategoryLog plog where paymentBillableItemCategory.billableItemCategoryId = :paymentBillableItemCategory")
	public List<PaymentBillableItemCategoryLog> findAllByPaymentBillableItemCategory(
			@Param("paymentBillableItemCategory") Integer paymentBillableItemCategory);

	@Query("select plog from PaymentBillableItemCategoryLog plog where plog.validFrom <= :validTill and plog.validTill >= :validFrom and plog.paymentBillableItemCategory.billableItemCategoryId = :billableItemCategoryId")
	public List<PaymentBillableItemCategoryLog> fetchValidatedBillableLogs(@Param("validFrom") Date validFrom,
			@Param("validTill") Date validTill, @Param("billableItemCategoryId") Integer billableItemCategoryId);

	@Query(value= "select rate from billing_billable_item_category_log where billable_item_category_id = :bic and valid_from <= :fromDate and valid_till >= :toDate",nativeQuery = true)
	public Double getBillableItemCategoryRate(@Param("bic") Integer bic, @Param("fromDate") Date validFrom, @Param("toDate") Date validTill);


	@Query("select plog from PaymentBillableItemCategoryLog plog where plog.validFrom <= :validFrom and plog.validTill >= :validTill and plog.paymentBillableItemCategory.billingContractType = :contractType")
	public List<PaymentBillableItemCategoryLog> getBiclByCtypeAndPeriod(@Param("contractType")ContractType contractType, @Param("validFrom") Date validFrom,
			@Param("validTill") Date validTo);

	@Query("select pbicl.paymentBillableItemCategory from PaymentBillableItemCategoryLog pbicl where pbicl.paymentBillableItemCategory.billingContractType = :contractType and pbicl.validFrom <= :validFrom and pbicl.validTill >= :validTo")
	public List<PaymentBillableItemCategory> findPbicByBillingContractTypeAndValidFromAndValidTill(
			@Param("contractType") ContractType contractType, @Param("validFrom") Date validFrom,
			@Param("validTo") Date validTo);

}
