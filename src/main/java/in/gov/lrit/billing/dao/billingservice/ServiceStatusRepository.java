/**
 * @ServiceStatusRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Yogendra Yadav (YY)
 * @version 1.0 23-Jul-2019
 */
package in.gov.lrit.billing.dao.billingservice;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.billingservice.ServiceStatus;

/**
 * @author Yogendra Yadav (YY)
 *
 */
public interface ServiceStatusRepository extends JpaRepository<ServiceStatus, Integer> {

	@Query(value = "Select ss from ServiceStatus ss where ss.billingService.billingServiceId = :billingServiceId order by ss.date desc")
	public List<ServiceStatus> getLatestStatuses(@Param("billingServiceId") Integer billingServiceId);

//	@Query(value = "Select ss from billing_service_status ss where ss.billingService = :billingServiceId order by ss.date desc LIMIT 1", nativeQuery = true)
//	public ServiceStatus getLatestStatus(@Param("billingServiceId") Integer billingServiceId);

	@Query("Select ss from ServiceStatus ss where ss.billingService.billingServiceId = :billingServiceId order by ss.date desc")
	public Page<ServiceStatus> getLatestStatuses(@Param("billingServiceId") Integer billingServiceId,
			Pageable pageable);

	@Query(value = "select * from billing_service_status where billing_service_id = :serviceId order by date desc limit 1", nativeQuery = true)
	ServiceStatus getLatestStatus(@Param("serviceId") Integer serviceId);

}
