package in.gov.lrit.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;


@Configuration
@EnableJpaRepositories(basePackages = { "in.gov.lrit.gis.ddp.repository",
		"in.gov.lrit.ddp.dao" }, entityManagerFactoryRef = "ddpEntityManagerFactory", transactionManagerRef = "ddpTransactionManager")
public class DdpDataSourceConfig {
	@Autowired
	private Environment env;

	@Primary
	@Bean
	@ConfigurationProperties(prefix = "datasource.ddp")
	public DataSourceProperties ddpDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Primary
	@Bean
	public DataSource ddpDataSource() {
		DataSourceProperties ddpDataSourceProperties = ddpDataSourceProperties();
		return DataSourceBuilder.create().driverClassName(ddpDataSourceProperties.getDriverClassName())
				.url(ddpDataSourceProperties.getUrl()).username(ddpDataSourceProperties.getUsername())
				.password(ddpDataSourceProperties.getPassword()).build();
	}

	@Primary
	@Bean
	public PlatformTransactionManager ddpTransactionManager() {
		EntityManagerFactory factory = ddpEntityManagerFactory().getObject();
		return new JpaTransactionManager(factory);
	}

	@Primary
	@Bean
	public LocalContainerEntityManagerFactoryBean ddpEntityManagerFactory() {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(ddpDataSource());
		factory.setPackagesToScan(new String[] { "in.gov.lrit.gis.ddp.model", "in.gov.lrit.ddp.model" });
		factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
		jpaProperties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
		// jpaProperties.put("hibernate.show-sql",
		// env.getProperty("spring.jpa.show-sql"));
		factory.setJpaProperties(jpaProperties);

		return factory;
	}

	@Primary
	@Bean
	public DataSourceInitializer ddpDataSourceInitializer() {
		DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
		dataSourceInitializer.setDataSource(ddpDataSource());
		// ResourceDatabasePopulator databasePopulator = new
		// ResourceDatabasePopulator();
		// databasePopulator.addScript(new ClassPathResource("security-data.sql"));
		// dataSourceInitializer.setDatabasePopulator(databasePopulator);
		dataSourceInitializer.setEnabled(env.getProperty("datasource.security.initialize", Boolean.class, false));
		return dataSourceInitializer;
	}
}
