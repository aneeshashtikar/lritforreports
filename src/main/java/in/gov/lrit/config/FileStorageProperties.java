package in.gov.lrit.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
    private String journalExtractUploadDir;
    private String journalExtractCsvDir;
	public String getJournalExtractUploadDir() {
		return journalExtractUploadDir;
	}
	public void setJournalExtractUploadDir(String journalExtractUploadDir) {
		this.journalExtractUploadDir = journalExtractUploadDir;
	}
	public String getJournalExtractCsvDir() {
		return journalExtractCsvDir;
	}
	public void setJournalExtractCsvDir(String journalExtractCsvDir) {
		this.journalExtractCsvDir = journalExtractCsvDir;
	}
    
    
    
}
