package in.gov.lrit.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(basePackages = { "in.gov.lrit.portal", "in.gov.lrit.billing.dao",
		"in.gov.lrit.ide" }, entityManagerFactoryRef = "portalEntityManagerFactory", transactionManagerRef = "portalTransactionManager")
public class PortalDataSourceConfig {
	@Autowired
	private Environment env;

	@Bean
	@ConfigurationProperties(prefix = "datasource.portal")
	public DataSourceProperties portalDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	public DataSource portalDataSource() {
		DataSourceProperties portalDataSourceProperties = portalDataSourceProperties();
		return DataSourceBuilder.create().driverClassName(portalDataSourceProperties.getDriverClassName())
				.url(portalDataSourceProperties.getUrl()).username(portalDataSourceProperties.getUsername())
				.password(portalDataSourceProperties.getPassword()).build();
	}

	@Bean
	public PlatformTransactionManager portalTransactionManager() {
		EntityManagerFactory factory = portalEntityManagerFactory().getObject();
		return new JpaTransactionManager(factory);
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean portalEntityManagerFactory() {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(portalDataSource());
		factory.setPackagesToScan(
				new String[] { "in.gov.lrit.portal", "in.gov.lrit.billing.model", "in.gov.lrit.ide" });
		factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
		jpaProperties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
		// jpaProperties.put("hibernate.show-sql",
		// env.getProperty("spring.jpa.show-sql"));
		factory.setJpaProperties(jpaProperties);

		return factory;
	}

	@Bean
	public DataSourceInitializer portalDataSourceInitializer() {
		DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
		dataSourceInitializer.setDataSource(portalDataSource());
		// ResourceDatabasePopulator databasePopulator = new
		// ResourceDatabasePopulator();
		// databasePopulator.addScript(new ClassPathResource("security-data.sql"));
		// dataSourceInitializer.setDatabasePopulator(databasePopulator);
		dataSourceInitializer.setEnabled(env.getProperty("datasource.security.initialize", Boolean.class, false));
		return dataSourceInitializer;
	}

}
