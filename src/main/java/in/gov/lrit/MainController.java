package in.gov.lrit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @LritApplication.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author   Kavita Sharma
 * @version 1.0
 */
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class MainController {
	
	/**
	 * 
	 * retrun login page for "/"
	 * @return
	 */
	@RequestMapping("/")
	public String loginPage() {
		return "login";
	}
	/**
	 * This mehtod returns the dashboard page.
	 * 
	 * @author Kavita Sharma
	 * @return String verticalgeoserver page
	 */
	@RequestMapping("/verticalgeoserver")
	public String verticalgeoserverPage(){
			 
		return "map/verticalgeoserver";
	}
	
	
	
	/**
	 * This method returns a sample form page.
	 * 
	 * @author Kavita Sharma
	 * @return String Sampleform page
	 */
	@RequestMapping("/sampleform")
	public String sampleformPage() {
		return "Sampleform";
	}

	@RequestMapping("/finalform")
	public String finalformPage() {
		return "samplejsp/finalform";
	}

	@RequestMapping("/userdetails")
	public String userdetailsPage() {
		return "userdetails";
	}
	
	
	
	
}
