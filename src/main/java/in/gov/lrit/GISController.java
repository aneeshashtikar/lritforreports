/**
 * @(#)GISController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.io.ParseException;

import in.gov.lrit.gis.ddp.model.CountrywaterPolygonsAvail;
import in.gov.lrit.gis.ddp.model.LritCoastalStateSo;
import in.gov.lrit.gis.ddp.model.PortCountries;
import in.gov.lrit.gis.ddp.model.PortModel;
import in.gov.lrit.gis.ddp.model.SARCgLritIdModel;
import in.gov.lrit.gis.ddp.model.SOFetchModel;
import in.gov.lrit.gis.ddp.model.lritIdMapping;
import in.gov.lrit.gis.ddp.repository.LritCoastalStateSoRepository;
import in.gov.lrit.gis.ddp.repository.PortCountriesRepository;
import in.gov.lrit.gis.ddp.repository.PortRepository;
import in.gov.lrit.gis.ddp.repository.SARCgLritIdRepository;
import in.gov.lrit.gis.ddp.repository.SOFetchRepository;
import in.gov.lrit.gis.ddp.repository.countryWaterPolygonAvailRepository;
import in.gov.lrit.gis.ddp.repository.lritIdMappingRepository;
import in.gov.lrit.portal.gis.lritdb.model.CustomPolygon;
import in.gov.lrit.portal.gis.lritdb.model.Freedraw;
import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaData;
import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaModel;
import in.gov.lrit.portal.gis.lritdb.model.PolygonShape;
import in.gov.lrit.portal.gis.lritdb.model.SURPICModel;
import in.gov.lrit.portal.gis.lritdb.model.ShipCompanyFetchModel;
import in.gov.lrit.portal.gis.lritdb.model.ShipFetchModel;
import in.gov.lrit.portal.gis.lritdb.model.shipDetailsForDisplay;
import in.gov.lrit.portal.gis.lritdb.model.shipHistoryDetails;
import in.gov.lrit.portal.gis.lritdb.repository.CsoDetailsFetchRepository;
import in.gov.lrit.portal.gis.lritdb.repository.CustomPolygonRepository;
import in.gov.lrit.portal.gis.lritdb.repository.SURPICFetchRepository;
import in.gov.lrit.portal.gis.lritdb.repository.ShipCompanyFetchRepository;
import in.gov.lrit.portal.gis.lritdb.repository.ShipFetchRepository;
import in.gov.lrit.portal.gis.lritdb.repository.ShipFrequencyRepository;
import in.gov.lrit.portal.gis.lritdb.service.GeographicAreaFileStorageService;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.user.PortalUser;
import in.gov.lrit.portal.repository.user.UserManagementRepository;
import in_.gov.lrit.session.UserSessionInterface;

@Controller
@RequestMapping("/map")
public class GISController {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GISController.class);

	@Value("${lrit.geoserver_url}")
	private String geoserver_url;
	
	@Autowired
	lritIdMappingRepository lritIdMappingRepositoryObj;

	@Autowired
	SOFetchRepository SOFetchRepositoryObj;
	
	@Autowired
	LritCoastalStateSoRepository LritCoastalStateSoRepositoryObj;

	@Autowired
	ShipFetchRepository ShipFetchRepositoryObj;

	@Autowired
	countryWaterPolygonAvailRepository countryWaterPolygonAvailRepositoryObj;

	@Autowired
	ShipCompanyFetchRepository ShipCompanyFetchRepositoryObj;

	@Autowired
	PortRepository PortRepositoryObj;

	@Autowired
	PortCountriesRepository PortCountriesRepositoryObj;

	@Autowired
	CsoDetailsFetchRepository CsoDetailsFetchRepositoryObj;

	@Autowired
	CustomPolygonRepository customPolygonRepository;

	@Autowired
	SURPICFetchRepository SURPICFetchRepositoryObj;

	@Autowired
	SARCgLritIdRepository SARCgLritIdRepositoryObj;
	
	@Autowired
	UserManagementRepository UserManagementRepositoryObj;

	@Autowired
	ShipFrequencyRepository ShipFrequencyRepositoryObj;
	
	@RequestMapping("/test")
	public String test() throws ParseException {
		return "testing";
	}

	@RequestMapping("/ShipCompany")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<ShipCompanyFetchModel> findShipCompany(@RequestParam("requestorsLritId") List<String> requestorsLritIdList) {
		try {
			logger.info("Getting Ship Company Info");
			List<String> requestorsLritIdListMod = new ArrayList<String>();
			for(int p=0; p<requestorsLritIdList.size();p++)
			{
				requestorsLritIdListMod.add(requestorsLritIdList.get(p).replaceAll("\"", ""));
			}
			//requestorsLritId = requestorsLritId.replaceAll("\"", "");
			
			List<ShipCompanyFetchModel> data = (List<ShipCompanyFetchModel>) ShipCompanyFetchRepositoryObj
					.findAllCompanyNameByLritId(requestorsLritIdListMod);
			return data;
		} catch (Exception E) {
			logger.error("Getting Ship Company Info Error: " + E);
			return null;
		}
	}

	@RequestMapping("/SearchShipByCompany")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<List<String>> findShipByCompanyName(@RequestParam("companyCode") String companyCodeList) {
		try {
			logger.info("Search Ship By Company: " + companyCodeList);
			String temp = companyCodeList.replace("[", "");
			temp = temp.replace("]", "");

			String[] companyCodeArr = temp.split(",");
			List<List<String>> data = new ArrayList<>();
			for (int m = 0; m < companyCodeArr.length; m++) {
				String companyCode = companyCodeArr[m].replaceAll("\"", "");
				List<String> dataTemp = (List<String>) ShipCompanyFetchRepositoryObj.findByCompanyName(companyCode);
				data.add(dataTemp);

			}
			return data;
		} catch (Exception E) {
			logger.error("Search Ship By Company Error: " + E);
			return null;
		}
	}
	
	
	@RequestMapping("/SearchCompanyShipByImo")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<ShipFetchModel> findCompanyShipByImo(@RequestParam("companyCode") String companyCode) {
		try {
			logger.info("Search Company Ship By imo no : " + companyCode);
			companyCode = companyCode.replaceAll("\"", "");
			List<String> imoNoList = (List<String>) ShipCompanyFetchRepositoryObj.findByCompanyName(companyCode);
			
			List<ShipFetchModel> data = new ArrayList<>();
			if(!imoNoList.isEmpty())
				data = (List<ShipFetchModel>) ShipFetchRepositoryObj.findShipByImoforComaony(imoNoList);
			
			return data;
		} catch (Exception E) {
			logger.error("Search Ship By Company Error: " + E);
			return null;
		}
	}
	

	@RequestMapping("/SO")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<List<SOFetchModel>> findSO(@RequestParam("cg_lritid") String cg_lritidList) {
		try {
			logger.info("Fetching Standing Order: " + cg_lritidList);
			String[] cg_lritidArr = cg_lritidList.split(",");
			List<List<SOFetchModel>> data = new ArrayList<>();
			for (int m = 0; m < cg_lritidArr.length; m++) {
				String cg_lritid = cg_lritidArr[m];
				List<SOFetchModel> dataTemp = (List<SOFetchModel>) SOFetchRepositoryObj
						.findBycglritid(cg_lritid.trim());
				data.add(dataTemp);
				//System.out.println(dataTemp.get(m).getPoslist());
			}
			
			//List<String> shipData = ShipFetchRepositoryObj.findShipByPolygon((data.get(0)).get(0).getPoslist());
			
			// System.out.println(shipData);
			return data;

		} catch (Exception E) {
			logger.error("Fetching Standing Order Error: " + E);
			return null;
		}
	}

	@RequestMapping("/WaterPolygons")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<CountrywaterPolygonsAvail> findWaterPolygons() {
		try {
			logger.info("Fetching WaterPolygons List");
			List<CountrywaterPolygonsAvail> data = (List<CountrywaterPolygonsAvail>) countryWaterPolygonAvailRepositoryObj
					.findallGroupBy();
			
			
			
			 List<LritCoastalStateSo> dataSO = (List<LritCoastalStateSo>) LritCoastalStateSoRepositoryObj .getOpenStandingOrder();
			 
			 List<PortCountries> dataPort = (List<PortCountries>) PortCountriesRepositoryObj.findPortCountries();
			 
			 for(int m=0; m< dataSO.size(); m++) {
				 boolean foundSoFlag = false;
			 	 int  foundSOIndex = 0;
			 	 String openLritId = dataSO.get(m).getCg_lritid();
			 	 
				 for(int n=0; n< data.size(); n++)
					{
					 String lritId = data.get(n).getCglritID();
						if(openLritId.trim().equalsIgnoreCase(lritId.trim()))
						{
							foundSoFlag=true;
							foundSOIndex = m;
						}	
						if (!(openLritId.trim().equalsIgnoreCase(lritId.trim())) && foundSoFlag==true)
						{
							break;
						}
						
					}
				 
				 if(foundSoFlag==true)
					{
						CountrywaterPolygonsAvail temp = new CountrywaterPolygonsAvail();
						temp.setCglritID(dataSO.get(foundSOIndex).getCg_lritid());
						temp.setType("SO");
						data.add(foundSOIndex+1,temp);
					}
			}
			 
			 for(int m=0; m< dataPort.size(); m++) {
				 boolean foundPortFlag = false;
			 	 int  foundPortIndex = 0;
			 	 String portLritId = dataPort.get(m).getCg_lritid();
			 	 
				 for(int n=0; n< data.size(); n++)
					{
					 String lritId = data.get(n).getCglritID();
						if(portLritId.trim().equalsIgnoreCase(lritId.trim()))
						{
							foundPortFlag=true;
							foundPortIndex = m;
						}	
						if (!(portLritId.trim().equalsIgnoreCase(lritId.trim())) && foundPortFlag==true)
						{
							break;
						}
						
					}
				 
				 if(foundPortFlag==true)
					{
						CountrywaterPolygonsAvail temp = new CountrywaterPolygonsAvail();
						temp.setCglritID(dataPort.get(foundPortIndex).getCg_lritid());
						temp.setType("Port");
						data.add(foundPortIndex+1,temp);
					}
			}
			 
			
			return data;
		} catch (Exception E) {
			logger.error("Fetching WaterPolygons List Error: " + E);
			return null;
		}
	}

	@RequestMapping("/CountryBoundary")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<SOFetchModel> findCountryBoundaries() {
		try {
			logger.info("Fetching CountryBoundary List");
			List<SOFetchModel> data = (List<SOFetchModel>) SOFetchRepositoryObj.findCountryBoundary();

			return data;
		} catch (Exception E) {
			logger.error("Fetching CountryBoundary List Error: " + E);
			return null;
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SelectedWaterPolygons")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<SOFetchModel> findSelectedWaterPolygons(@RequestParam Map<String, String> selectedWaterPolygonsName) {
		try {
			logger.info("Fetching Selected WaterPolygons Data: " + selectedWaterPolygonsName);
			ObjectMapper mapper = new ObjectMapper();

			@SuppressWarnings("unchecked")
			Map<String, String> map = mapper.readValue(selectedWaterPolygonsName.get("selectedWaterPolygonsName"),
					Map.class);

			List<String> country = new ArrayList<String>();
			List<List<String>> waterPolygonList = new ArrayList<>();

			for (Map.Entry<String, String> m : map.entrySet()) {
				if (!(m.getValue()).equals("[]")) {
					List<String> waterPolygonTemp = new ArrayList<String>();
					country.add((String) m.getKey());
					String val = (String) m.getValue();
					String temp = val.replace("[", "");
					temp = temp.replace("]", "");
					
					String[] len = temp.split(",");
					for (int p = 0; p < len.length; p++) {
						String waterPolygonName = len[p].replaceAll("\"", "");
						waterPolygonTemp.add(waterPolygonName);
					}
					
					waterPolygonList.add(waterPolygonTemp);
				}
			}
			
			List<SOFetchModel> data = null;
			for (int i = 0; i < country.size(); i++) {

				List<String> waterPolygon = waterPolygonList.get(i);
				List<String> waterPolygonSelect = new ArrayList<String>();
				List<String> waterPolygonSO = new ArrayList<String>();
				List<String> PortData = new ArrayList<String>();
				for(int p=0; p<waterPolygon.size();p++)
				{	if(waterPolygon.get(p).equalsIgnoreCase("OpenSo"))
						waterPolygonSO.add(waterPolygon.get(p));
					else if(waterPolygon.get(p).equalsIgnoreCase("Port"))
						PortData.add(waterPolygon.get(p));
					else
						waterPolygonSelect.add(waterPolygon.get(p));
					
				}
				//System.out.println("selectedWaterPolygon: " + country.get(i) + " " + waterPolygon );
				List<SOFetchModel> tempData = (List<SOFetchModel>) SOFetchRepositoryObj
						.findBycglritidWaterPolygons(country.get(i), waterPolygon);
				
				if(!waterPolygonSO.isEmpty())
				{
					List<SOFetchModel> tempDataSO = (List<SOFetchModel>) SOFetchRepositoryObj.getOpenSO(country.get(i));
					
					for(int q=0; q<tempDataSO.size();q++)
					{
						tempDataSO.get(q).setType("OpenSo");
					}
					tempData.addAll(tempDataSO);
				}
				
				/*if(!PortData.isEmpty())
				{
					List<SOFetchModel> tempDataSO = (List<SOFetchModel>) SOFetchRepositoryObj.getOpenSO(country.get(i));
					
					for(int q=0; q<tempDataSO.size();q++)
					{
						tempDataSO.get(q).setType("OpenSo");
					}
					tempData.addAll(tempDataSO);
				}*/
				
				if (i == 0)
					data = tempData;
				else
				{
					if(tempData!=null)
						data.addAll(tempData);
				}
			}
			//System.out.println(data.size());
			return data;
		} catch (Exception E) {
			logger.error("Fetching Selected WaterPolygons Data Error: " + E);
			return null;
		}
	}

	
	
	@RequestMapping(method = RequestMethod.POST, value = "/SelectedPorts")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<PortModel> findSelectedPorts(@RequestParam Map<String, String> selectedPortsName) {
		try {
			logger.info("Fetching Selected Port Data: " + selectedPortsName);
			ObjectMapper mapper = new ObjectMapper();

			@SuppressWarnings("unchecked")
			Map<String, String> map = mapper.readValue(selectedPortsName.get("selectedPortsName"),Map.class);

			List<String> country = new ArrayList<String>();
			List<List<String>> portList = new ArrayList<>();

			for (Map.Entry<String, String> m : map.entrySet()) {
				if (!(m.getValue()).equals("[]")) {
					List<String> portTemp = new ArrayList<String>();
					country.add((String) m.getKey());
					String val = (String) m.getValue();
					String temp = val.replace("[", "");
					temp = temp.replace("]", "");
					
					String[] len = temp.split(",");
					for (int p = 0; p < len.length; p++) {
						String waterPolygonName = len[p].replaceAll("\"", "");
						portTemp.add(waterPolygonName);
					}
					
					portList.add(portTemp);
				}
			}
			
			List<PortModel> data = null;
			if(!portList.isEmpty())
			{
				for (int i = 0; i < country.size(); i++) {
					
					List<String> ports = portList.get(i);
					
					List<String> PortData = new ArrayList<String>();
					
					for(int p=0; p<ports.size();p++)
					{	 
						if(ports.get(p).equalsIgnoreCase("Port"))
							PortData.add(ports.get(p));
						
						
					}
					List<PortModel> tempData = null;
					if(!PortData.isEmpty())
					{
						List<PortModel> tempDataLoc = (List<PortModel>) PortRepositoryObj.findByCglritid(country.get(i).trim());
						List<String> locodeArray =new ArrayList<String>();
						
						
						for(int x=0; x<tempDataLoc.size();x++)
						{
							locodeArray.add( tempDataLoc.get(x).getLocode());
						
						}
						if(!locodeArray.isEmpty())
							tempData = (List<PortModel>) PortRepositoryObj.findPortDetailsForDisplay(locodeArray);
					}
					
					if (i == 0)
						data = tempData;
					else
					{
						if(tempData!=null)
							data.addAll(tempData);
					}
				}
			}
			//System.out.println(data.size());
			return data;
		} catch (Exception E) {
			logger.error("Fetching Selected port Data Error: " + E);
			return null;
		}
	}

	
	@RequestMapping("/ShipPosition")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<shipDetailsForDisplay> findShip(@RequestParam("cg_lritid") List<String> cg_lritid) {
		try {
			logger.info("ShipPositions of cg_lritid: " + cg_lritid);
			List<shipDetailsForDisplay> data = (List<shipDetailsForDisplay>) ShipFetchRepositoryObj.findAll(cg_lritid);			
			
			return data;
		} catch (Exception E) {
			logger.error("ShipPositions of cg_lritid Error: " + E);
			return null;
		}
	}

	
	@RequestMapping("/ShipHistory")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<shipHistoryDetails> findShipHistory(@RequestParam("imoNo") String imoNo,
			@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {
		try {
			logger.info("ShipHistory of imoNo: " + imoNo);
			Timestamp dtStart = Timestamp.valueOf(startDate);
			Timestamp dtend = Timestamp.valueOf(endDate);
			//System.out.println("startDate: " + startDate);
			//System.out.println("startDate: " + endDate);
			List<shipHistoryDetails> data = (List<shipHistoryDetails>) ShipFetchRepositoryObj.findbyimoNo(imoNo,
					dtStart, dtend);
			//System.out.println(data.get(0).getData_user_provider());
			//System.out.println(data.get(0).getData_user_requestor());// 7345611
			return data;
		} catch (Exception E) {
			logger.error("ShipHistory of imoNo Error: " + E);
			return null;
		}
	}

	@RequestMapping("/mapHome")
	public String AdminViewJSP(Model model) {
		try {
			
			logger.info("get flagCountryOfLogin from Session");
			ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
			HttpSession session = attr.getRequest().getSession();
			UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
			PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
			String requestorLritid=lritIdmaster.getLritId();
			String userCategory = userSession.getCategory();
			String userLoginId = userSession.getLoginId();
			String companyCode = "none";
			PortalUser portalUserInfo =  UserManagementRepositoryObj.getUserDetails(userLoginId);
			if(userCategory.equals("USER_CATEGORY_SC"))
			{				
				companyCode = portalUserInfo.getPortalShippingCompany().getCompanyCode();				
			}
			
			List<String> Activities_list = UserManagementRepositoryObj.getAllActivitiesByLoginId(userLoginId);
			List<String> assignLritIdsShipView = new ArrayList<String>();
			for (String activity_name : Activities_list) {				
				if(activity_name.equalsIgnoreCase("superUser"))
					{
						System.out.println("superUser");		
						assignLritIdsShipView = (List<String>) lritIdMappingRepositoryObj.findUserLritIdsByDC();
					}
			}
		
			if (assignLritIdsShipView.isEmpty())
				assignLritIdsShipView.add(requestorLritid);
			
			boolean sarAreaViewStatus = portalUserInfo.getSarAreaView();
			//System.out.println("company code " + companyCode);
			logger.info("loading map Page and lritIDs & corresponding lritNames");
			List<lritIdMapping> data = (List<lritIdMapping>) lritIdMappingRepositoryObj.findAllLritIds();

			List<String> allLritIds = new ArrayList<String>();
			List<String> allLritCountries = new ArrayList<String>();

			for (int m = 0; m < data.size(); m++) {
				allLritIds.add(data.get(m).getCglritID().trim());
				String tempLritName  = (data.get(m).getCglritName().trim()).replaceAll(",", "*");
				allLritCountries.add(tempLritName);
			}

			model.addAttribute("lritNames", allLritCountries);
			model.addAttribute("lritIds", allLritIds);
			model.addAttribute("assignLritIdsShipView", assignLritIdsShipView);
			model.addAttribute("flagCountryOfLogin", requestorLritid);
			model.addAttribute("geoserver_url", geoserver_url);
			model.addAttribute("companyCode", companyCode);
			model.addAttribute("sarAreaViewStatus", sarAreaViewStatus);
			
		} catch (Exception E) {
			logger.error("loading map Page Error: " + E);
			
		}
		return "map/verticalgeoserver";
	}

	// --- SAR SURPIC Request
	@RequestMapping("/sarsurpicrequest")
	public String sarSurpicRequest() {
		try {
			logger.info("loading SURPICRequestForm Page");
			
		} catch (Exception E) {
			logger.error("loading SURPICRequestForm Page Error: " + E);
			
		}
		return "map/SURPICRequestForm";
	}

	@RequestMapping("/PortCountries")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<PortCountries> findPortsCountries() {
		try {
			logger.info("loading Countries lritId having ports.");
			List<PortCountries> data = (List<PortCountries>) PortCountriesRepositoryObj.findPortCountries();
			return data;
		} catch (Exception E) {
			logger.error("loading Countries lritId having ports Error: " + E);
			return null;
		}
	}

	@RequestMapping("/Port")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<PortModel> findPorts(@RequestParam("cg_lritId") String cg_lritId) {
		try {
			logger.info("loading all ports name of cglritId: " + cg_lritId);
			List<PortModel> data = (List<PortModel>) PortRepositoryObj.findByCglritid(cg_lritId.trim());
			return data;
		} catch (Exception E) {
			logger.error("loading all ports name by cglritId Error: " + E);
			return null;
		}
	}

	@RequestMapping("/PortDetailsForDisplay")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<PortModel> findPortDetailsforDisplay(@RequestParam("locode") List<String> locode) {
		try {
			logger.info("loading ports Details for Display: " + locode);
			List<PortModel> data = (List<PortModel>) PortRepositoryObj.findPortDetailsForDisplay(locode);
			return data;
		} catch (Exception E) {
			logger.error("loading ports Details for Display Error: " + E);
			return null;
		}
	}

	@RequestMapping("/ShipDetailsOnSearchCriteria")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<ShipFetchModel> findShipDetailsOnSearchCriteria(@RequestParam("searchCriteria") String searchCriteria,
			@RequestParam("searchVal") String searchVal, @RequestParam("cg_lritId") List<String> cg_lritId) {
		try {
			logger.info("loading ship Details for " + searchCriteria + ": " + searchVal);
			
			ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
			HttpSession session = attr.getRequest().getSession();
			UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
			PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
			String requestorLritid=lritIdmaster.getLritId();
			String userCategory = userSession.getCategory();
			String userLoginId = userSession.getLoginId();
			String companyCode = "none";
			if(userCategory.equals("USER_CATEGORY_SC"))
			{
				PortalUser portalUserInfo =  UserManagementRepositoryObj.getUserDetails(userLoginId);
				companyCode = portalUserInfo.getPortalShippingCompany().getCompanyCode();
				
			}
			
			List<ShipFetchModel> data = new ArrayList<ShipFetchModel>();
			List<ShipFetchModel> dataReturn = new ArrayList<ShipFetchModel>();
			if (searchCriteria.equals("ShipName")) {
				data = (List<ShipFetchModel>) ShipFetchRepositoryObj.findShipDetailsOnShipName(searchVal,cg_lritId);
			} else if (searchCriteria.equals("IMONo.")) {
				data = (List<ShipFetchModel>) ShipFetchRepositoryObj.findShipDetailsOnIMONo(searchVal,cg_lritId);
			} else if (searchCriteria.equals("SEIDNo.")) {
				data = (List<ShipFetchModel>) ShipFetchRepositoryObj.findShipDetailsOnSEIDNo(searchVal,cg_lritId);
			} else if (searchCriteria.equals("DNIDNo.")) {
				data = (List<ShipFetchModel>) ShipFetchRepositoryObj.findShipDetailsOnDnidNo(searchVal,cg_lritId);
			} else {
				data = (List<ShipFetchModel>) ShipFetchRepositoryObj.findShipDetailsOnMMSINo(searchVal,cg_lritId);
			}
			List<ShipFetchModel> companyShipData = new ArrayList<ShipFetchModel>();
			if (companyCode!="none")
			{	companyShipData = findCompanyShipByImo(companyCode);
			
				for(int p=0;p<companyShipData.size();p++)
				{
					for(int q=0;q<data.size();q++)
					{						
						if ((data.get(q).getImo_no().trim().equalsIgnoreCase(companyShipData.get(p).getImo_no().trim())))
							{
								dataReturn.add(data.get(q));							
							}
					}
					
				}
			}
			else
				dataReturn = data;
			
			
			return dataReturn;
		} catch (Exception E) {
			logger.error("loading ship Details for " + searchCriteria + " of " + searchVal + " Error: " + E);
			return null;
		}
	}

	
	// ---------------------------------------------------------------
	// -- Userdefined Area Related Methods-----------------------

	// -- Save Userdefined Area Form----------------------------------
	@RequestMapping("/saveform")
	public String saveform(Model model) {
		try {
			logger.info("customPolygonForm Page");
			
		} catch (Exception E) {
			logger.error("customPolygonForm Page Error: " + E);
			
		}
		return "map/customPolygonForm";
	}

	// --
	// --- Save Userdefined Area -------------------------------------
	@RequestMapping(path = "/saveuserarea", consumes = "application/json")
	@ResponseBody
	public PolygonShape saveuserarea(@RequestBody PolygonShape pg) {
		try {
			logger.info("save user area");
			if (!pg.getPolygonName().equals("")) {
				String checkPolygonStatus = "accept";
				if (pg.getPolygonId() == null || pg.getPolygonId().equals("")) {
					checkPolygonStatus = processCustomPolygon(pg.getPolygonName().trim());	
				}
				CustomPolygon cp = new CustomPolygon();
				if(checkPolygonStatus.equalsIgnoreCase("accept"))
				{
					
					cp.setCreation_date(new Date());
					int randomNumber = new Random().nextInt();
					String generatedString = randomNumber + "";
					if (pg.getPolygonId() != null && !pg.getPolygonId().equals("")) {
						cp.setPolygonId(pg.getPolygonId());
					} else {
						cp.setPolygonId(generatedString);
					}
					cp.setPolygon_name(pg.getPolygonName());
	
					ObjectMapper mapper = new ObjectMapper();
					String json = "";
					try {
						json = mapper.writeValueAsString(pg);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
					HttpSession session = attr.getRequest().getSession();
					UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");									
					String userLoginId = userSession.getLoginId();					
					cp.setCreated_by(userLoginId);
					cp.setPolygon_points(json);
					cp.setPolygon_type(pg.getPolygontype());
					CustomPolygon retCustomPolygon = customPolygonRepository.save(cp);
					pg.setPolygonId(retCustomPolygon.getPolygonId());
					return pg;
				}
				else
				{
					pg.setPolygonName(checkPolygonStatus);	
					return pg;
				}
					
			}
			return pg;

		} catch (Exception E) {
			logger.error("save user area Error: " + E);
			return null;
		}

	}

	// --------------------------------------------------------------
	public String processCustomPolygon(String search) {
		try {
			logger.info("process Custom Polygon ");
			List<CustomPolygon> poly;
			poly = customPolygonRepository.findBypolygonname(search.toUpperCase());	
			if(poly.isEmpty())
				return "accept";
			else
				return "reject";
		} catch (Exception E) {
			logger.error("process Custom Polygon Error: " + E);
			
		}
		return "map/customPolygonSearchForm";
	} 

	@RequestMapping("/searchCustomPolygon")
	@ResponseBody
	public List<CustomPolygon> searchCustomPolygon(Model model) {
		try {
			logger.info("process Custom Polygon ");
			List<CustomPolygon> poly;
			List<CustomPolygon> polyAllowed = new ArrayList<CustomPolygon>();
			poly = customPolygonRepository.findAll();		
			
			
			ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
			HttpSession session = attr.getRequest().getSession();
			UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");			
			String userCategory = userSession.getCategory();
			String userLoginId = userSession.getLoginId();
			PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
			String requestorLritid=lritIdmaster.getLritId();
			String companyCode = "none";
			PortalUser portalUserInfo =  UserManagementRepositoryObj.getUserDetails(userLoginId);
			if(userCategory.equals("USER_CATEGORY_SC"))
			{				
				companyCode = portalUserInfo.getPortalShippingCompany().getCompanyCode();				
			}					
			
			List<String> Activities_list = UserManagementRepositoryObj.getAllActivitiesByLoginId(userLoginId);
			List<String> assignLritIdsShipView = new ArrayList<String>();
			for (String activity_name : Activities_list) {				
				if(activity_name.equalsIgnoreCase("superUser"))
					{
							
						assignLritIdsShipView = (List<String>) lritIdMappingRepositoryObj.findUserLritIdsByDC();
					}
			}
			
			for (CustomPolygon cp : poly) {				
				if(cp.getCreated_by()!=null)
				{
					PortalUser portalUserInfoPolygon =  UserManagementRepositoryObj.getUserDetails(cp.getCreated_by());
					PortalLritIdMaster lritIdmasterPolygon = portalUserInfoPolygon.getRequestorsLritId();
					String requestorLritidPolygon=lritIdmasterPolygon.getLritId();
					String companyCodePolygon = "none";
					if(userCategory.equals("USER_CATEGORY_SC"))
					{	
					  companyCodePolygon = portalUserInfoPolygon.getPortalShippingCompany().getCompanyCode();
					}
					if (assignLritIdsShipView.isEmpty())
					{
						if(userCategory.equals("USER_CATEGORY_SC") && requestorLritid.equalsIgnoreCase(requestorLritidPolygon))
						 {	
							if(companyCode.equalsIgnoreCase(companyCodePolygon))
								polyAllowed.add(cp);				 
						 }					
						else if(userCategory.equals(portalUserInfoPolygon.getCategory())  && requestorLritid.equalsIgnoreCase(requestorLritidPolygon))
						 {					
							polyAllowed.add(cp);				 
						 }
					}
					else
					{
						boolean showPolygonSuperUser = false;
						for(int a=0;a<assignLritIdsShipView.size();a++)
						{
							if(requestorLritidPolygon.equalsIgnoreCase(assignLritIdsShipView.get(a)))
								showPolygonSuperUser = true;
						}
						if(showPolygonSuperUser==true)
							polyAllowed.add(cp);	
					}
				}
				else
				{
					polyAllowed.add(cp);
				}
			}
			
			
			model.addAttribute("userareas", polyAllowed);

			return polyAllowed;
		} catch (Exception E) {
			logger.error("process Custom Polygon Error: " + E);
			return null;
		}
	}
	
	
	@RequestMapping("/custompolygonform")
	public String customPolygonForm(Model model,
			@RequestParam(value = "userareaOption", required = false, defaultValue = "anonymous") String option,
			@RequestParam(value = "search", required = false, defaultValue = "") String search) {
		try {
			logger.info("Custom Polygon form");
			model.addAttribute("userareaOption", option);
			model.addAttribute("search", search);
			
		//	if (option.equalsIgnoreCase("show"))
				//return processCustomPolygon(model, search);
			return "map/customPolygonForm";
		} catch (Exception E) {
			logger.error("Custom Polygon form Error: " + E);
			return "map/customPolygonForm";
		}
	}

	@RequestMapping("/getselecteduserarea")
	@ResponseBody
	public List<PolygonShape> getSelectedUserArea(@RequestParam(value = "id") List<String> id) {
		List<CustomPolygon> poly;
		try {
			logger.info("get selected user area");
			poly = customPolygonRepository.findByPolygonIdIn(id);
			List<PolygonShape> userarea = new ArrayList<PolygonShape>();
			PolygonShape polygon = null;


				
			for (CustomPolygon cp : poly) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					polygon = mapper.readValue(cp.getPolygon_points(), PolygonShape.class);
					polygon.setPolygonId(cp.getPolygonId());

				} catch (Exception e) {
					e.printStackTrace();
				}
				userarea.add(polygon);	
					
			}
			return userarea;
		} catch (Exception E) {
			logger.error("get selected user area Error: " + E);
			return null;
		}
	}

	@RequestMapping("/closeselecteduserarea")
	@ResponseBody
	public String closeSelectedUserArea(@RequestParam(value = "id") String id) {
		try {
			logger.info("delete selected user area");
			customPolygonRepository.deleteById(id);
			return "success";
		} catch (Exception E) {
			logger.error("delete selected user area Error: " + E);
			return null;
		}
	}

	// ---------------------------------------------------------------
	// --- Add Geographic area Request
	@RequestMapping("/addgeographicalarea")
	public String addgeographicallarea() {
		try {
			logger.info("add geographical area");
			
		} catch (Exception E) {
			logger.error("add geographical area Error: " + E);
			
		}
		return "map/addGeographicAreaForm";
	}
	// ----------------------

	@RequestMapping("/SurpicArea")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<SURPICModel> findSurpicArea(@RequestParam("cg_lritid") List<String> cg_lritid,
			@RequestParam("timestamp1") String startDate, @RequestParam("timestamp2") String endDate, @RequestParam("accessType") List<Integer> accessType)
			throws ParseException {
		try {
			logger.info("Surpic Area Request");
			
			Timestamp dtStart = Timestamp.valueOf(startDate);
			Timestamp dtend = Timestamp.valueOf(endDate);			
			
			//System.out.println(newCgLritId);
			List<String> modified_cg_lritid = new ArrayList<String>();
			for (int i = 0; i < cg_lritid.size(); i++) {
				boolean mainCgLritIdFlag = true;
				if (mainCgLritIdFlag == true) {
					modified_cg_lritid.add(cg_lritid.get(i).trim());
					mainCgLritIdFlag = false;
				}
				List<String> cg_lritidTemp = new ArrayList<String>();
				cg_lritidTemp.add(cg_lritid.get(i).trim());
				List<SARCgLritIdModel> newCgLritId = findAllCgLritIdSurpicArea(cg_lritidTemp);
				for (int j = 0; j < newCgLritId.size(); j++) {					
					
					if (cg_lritid.get(i).trim().equalsIgnoreCase(newCgLritId.get(j).getCglritID().trim())) {						
						modified_cg_lritid.add(newCgLritId.get(j).getSar_asp_dc_CglritID().trim());
					}
				}
			}
			
			List<SURPICModel> data = (List<SURPICModel>) SURPICFetchRepositoryObj.findSurpicArea(modified_cg_lritid,
					dtStart, dtend,accessType);			
			
			return data;
		} catch (Exception E) {
			logger.error("Surpic Area Request Error: " + E);
			return null;
		}
	}
	
	
	@RequestMapping("/SurpicAreaByMessageID")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<SURPICModel> findSurpicAreaByMessageID(@RequestParam("message_idList") List<String> message_idList)
			throws ParseException {
		try {
			logger.info("Surpic Area Request By Message Id");			
		
			List<SURPICModel> data = (List<SURPICModel>) SURPICFetchRepositoryObj.findSurpicAreaByMessageID(message_idList);
			
			/*for (int i = 0; i < data.size(); i++) {
				
				SARCgLritIdModel temp = findCgLritIdByDataUserProvider(data.get(i).getData_user_requestor());				
				if (temp!=null)
					data.get(i).setData_user_requestor(temp.getCglritID());
			}*/
			
			return data;
		} catch (Exception E) {
			logger.error("Surpic Area Request  By Message Id Error: " + E);
			return null;
		}
	}
	

	@RequestMapping("/ShipsInSurpicArea")
	@ResponseBody
	@CrossOrigin(origins = "*")
	public List<shipHistoryDetails> findShipsInSurpicArea(@RequestParam("message_id") List<String> message_id) {
		try {
			logger.info("Ships in Surpic Area Request");			
			List<shipHistoryDetails> data = (List<shipHistoryDetails>) ShipFetchRepositoryObj
					.findShipByMessageId(message_id); // cg_lritid
			
			for (int i = 0; i < data.size(); i++) {
				
				SARCgLritIdModel temp = findCgLritIdByDataUserProvider(data.get(i).getData_user_provider());				
				if (temp!=null)
					data.get(i).setData_user_provider(temp.getCglritID());
			}
			
			return data;
		} catch (Exception E) {
			logger.error("Ships in Surpic Area Request Error: " + E);
			return null;
		}
	}

	
	@RequestMapping("/findAllCgLritIdSurpicArea")
	@ResponseBody
	@CrossOrigin(origins = "*")	
	public List<SARCgLritIdModel> findAllCgLritIdSurpicArea(@RequestParam("cg_lritId") List<String> cg_lritId) {
		try {
			//logger.info("find All CgLritId for Surpic Area Request");
			List<SARCgLritIdModel> data1 = (List<SARCgLritIdModel>) SARCgLritIdRepositoryObj
					.findSarServiceLritId(cg_lritId);

			List<SARCgLritIdModel> data2 = (List<SARCgLritIdModel>) SARCgLritIdRepositoryObj.findDcLritId(cg_lritId);

			List<SARCgLritIdModel> data3 = (List<SARCgLritIdModel>) SARCgLritIdRepositoryObj.findAspLritId(cg_lritId);

			data1.addAll(data2);
			data1.addAll(data3);
			return data1;
		} catch (Exception E) {
			logger.error("find All CgLritId for Surpic Area Request Error: " + E);
			return null;
		}
	}
		
	
	public SARCgLritIdModel findCgLritIdByDataUserProvider(String cg_lritId) {
		try {
			logger.info("find All CgLritId for datauserProvider");
			
			SARCgLritIdModel data = null;
			
			data = (SARCgLritIdModel) SARCgLritIdRepositoryObj
					.findLritIdFromSarServiceID(cg_lritId);
			if (data==null)
			  data = (SARCgLritIdModel) SARCgLritIdRepositoryObj.findLritIdFromDcId(cg_lritId);
			
			if (data==null)
			 data = (SARCgLritIdModel) SARCgLritIdRepositoryObj.findLritIdFromAspId(cg_lritId);

			
			return data;
			
		} catch (Exception E) {
			logger.error("find All CgLritId for Surpic Area Request Error: " + E);
			return null;
		}
	}
	
	
	@Autowired
	GeographicAreaFileStorageService geographicAreaFileStorageService;		 
	
	 @ResponseBody	
	 @PostMapping(path = "/uploadFile", consumes = "application/json")
	    public String uploadFile(@RequestBody GeographicAreaData file) {
			try {
				logger.info("Geographic area Upload");		 
				ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
				HttpSession session = attr.getRequest().getSession();
				UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
				String loginid=userSession.getLoginId();	
				GeographicAreaModel retObj = geographicAreaFileStorageService.storeFile(file,loginid);		       
				return retObj.getGmlId()+"";
			} catch (Exception E) {
				logger.error("Geographic area Upload Error: " + E);
				return null;	
			}
	    }
	 
	
	 @GetMapping("/downloadFile/{fileId}")
	    public ResponseEntity<Resource> downloadFile(@PathVariable int fileId) {
	        // Load file from database
	        GeographicAreaModel dbFile = geographicAreaFileStorageService.getFile(fileId);		        
	        
	        return ResponseEntity.ok()
	                .contentType(MediaType.parseMediaType("application/xml"))
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getGmlId() + "\"")
	                .body(new ByteArrayResource(dbFile.getUploaded_file()));
	    } 
	 
	 
	 @RequestMapping("/getselectedGeographicarea")
	 @ResponseBody
	 public GeographicAreaData getselectedGeographicarea(@RequestParam (value = "id" ) int id){	
		 GeographicAreaData geographicAreaData = geographicAreaFileStorageService.getGeographicArea(id);
		// System.out.println(geographicAreaData.getData());
		 return geographicAreaFileStorageService.getGeographicArea(id);
	 }
	 
	 @RequestMapping("/getGeographicarealist")
	 @ResponseBody
	 public List<GeographicAreaModel> getGeographicareaList(){
		 try {
				logger.info("Display Geographic area List");
				//return geographicAreaFileStorageService.getGeographicAreaList();
				ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
				HttpSession session = attr.getRequest().getSession();
				UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
				String loginid=userSession.getLoginId();
				return geographicAreaFileStorageService.getGeographicAreaListPerUser(loginid);
				
		 } catch (Exception E) {
				logger.error("Display Geographic area List Error: " + E);
				E.printStackTrace();
				return null;	
			}
	 }
	 
	 
	 @RequestMapping(method = RequestMethod.GET, value = "/getDDPPolygons")
		@ResponseBody
		@CrossOrigin(origins = "*")
		public List<SOFetchModel> getDDPPolygons(Model model)
				{
		 try {
			logger.info("Display DDP Polygon List");
			ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
			HttpSession session = attr.getRequest().getSession();
			UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
			PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
			String cg_lrit=lritIdmaster.getLritId();
		 	//String cg_lrit ="1065";
		 	ArrayList<String> waterPolygonString = new ArrayList<String>( Arrays.asList("TerritorialSea", "InternalWaters", "CoastalSea","CustomCoastal") );
		 	List<SOFetchModel> tempData = (List<SOFetchModel>) SOFetchRepositoryObj
					.findBycglritidWaterPolygons(cg_lrit, waterPolygonString);
		 		return tempData;	
		 } catch (Exception E) {
				logger.error("Display DDP Polygon List Error: " + E);
				return null;	
			}
		}
	 
	 
	    @RequestMapping(method = RequestMethod.GET, value = "/getnextpolygonid")
		@ResponseBody
		@CrossOrigin(origins = "*")
		public String getNextPolygonID(@RequestParam (value = "type" ) String polygontype)
				{
		 try {
			 logger.info("Get Next Geographic Area ID");
			 ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
			 HttpSession session = attr.getRequest().getSession();
			 UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
			 PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
			 String cg_lrit=lritIdmaster.getLritId();			 
			 
			 //String cg_lrit ="1065";
			 String areacodePrefix="";
			 if(polygontype.equalsIgnoreCase("InternalWaters")) {
				 areacodePrefix = "GAIW" + cg_lrit ;
				// System.out.println(polygontype + "  : " +areacodePrefix);
			 }else if (polygontype.equalsIgnoreCase("CustomCoastalAreas")){
				 areacodePrefix = "GACA" + cg_lrit ;
			 }else if (polygontype.equalsIgnoreCase("CustomCoastal")){
				 areacodePrefix = "GACA" + cg_lrit ; 
			 }else if (polygontype.equalsIgnoreCase("TerritorialSea")){
				 areacodePrefix = "GATS" + cg_lrit ; 
			 }else if (polygontype.equalsIgnoreCase("SeawardAreaOf1000NM")){
				 areacodePrefix = "GAOT" + cg_lrit ;
			 }else if (polygontype.equalsIgnoreCase("CoastalSea")){
				 areacodePrefix = "GAOT" + cg_lrit ;
			 }
			 //System.out.println(polygontype + "  : " +areacodePrefix);
			 if(!areacodePrefix.equals(""))	 return areacodePrefix+"_"+ (geographicAreaFileStorageService.getNextPolygonID(areacodePrefix)+1);	
			 else return "";
		 } catch (Exception E) {
				logger.error("Get Next Geographic Area ID Error: " + E);
				return "";	
			}
		} 

	    @RequestMapping("/getShipIMOByDDPPolygon")
		@ResponseBody
	 	public List<String> getShipIMOByDDPPolygon(@RequestParam("cgLritid") String cgLritid,  @RequestParam("polygonType")String polygonType,
				@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate)
	 	{
	 		
	 		List<String> waterPolygonList = new ArrayList<String>();
	 		
	 		ArrayList<String> waterPolygonString = new ArrayList<String>( Arrays.asList(polygonType));
	 		
	 		waterPolygonList.add(polygonType);
	 		
	 		//System.out.println("selectedWaterPolygon: " + cgLritid + " " + waterPolygonString );
	 		
	 		List<SOFetchModel> tempData = (List<SOFetchModel>) SOFetchRepositoryObj
					.findBycglritidWaterPolygons(cgLritid, waterPolygonString);
	 		
	 		Timestamp dtStart = Timestamp.valueOf("2020-01-02 0:00:00");
			Timestamp dtend = Timestamp.valueOf("2020-01-21 0:00:00");
			List<String> shipDataFinal = new ArrayList<>();
	 		for (int p = 0; p<tempData.size();p++)
	 		{
	 			List<String> shipData = ShipFetchRepositoryObj.findShipByPolygon(tempData.get(p).getPoslist(),dtStart,dtend);
	 			shipDataFinal.addAll(shipData);
	 		}
			System.out.println(shipDataFinal);
	 		return shipDataFinal;
	 	}
	 	
	     @RequestMapping("/getShipIMOByCustomPolygon")
		 @ResponseBody
	 	public List<String> getShipIMOByCustomPolygon( @RequestParam("PolyCoords")String PolyCoords ,@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate
				)
	 	{
	    	 
	    	// Timestamp dtStart = Timestamp.valueOf(startDate);
			//	Timestamp dtend = Timestamp.valueOf(endDate);
				
				Timestamp dtStart = Timestamp.valueOf("2020-01-20 0:00:00");
				Timestamp dtend = Timestamp.valueOf("2020-01-21 0:00:00");
				
	    	 System.out.println("PolyCoords: " + PolyCoords);
	    	 
	    	 List<String> shipData = ShipFetchRepositoryObj.findShipByCustomPolygon(PolyCoords,dtStart,dtend);
	    	 System.out.println("shipData Custom: " + shipData);
			
	 		return shipData;
	 	}
	 
}
