package in.gov.lrit.security.config;

import java.util.Collection;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.portal.model.user.PortalUserInterface;
import in.gov.lrit.portal.model.user.SecurityUserModel;
import in.gov.lrit.portal.service.user.IUserManagementService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
	private CustomUserService customUserService;
	
	@Autowired
	private IUserManagementService iUserManagementService;
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
			.getLogger(CustomAuthenticationProvider.class);
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		logger.info("Inside Authentication method");
		String loginId = authentication.getName();
		String password = (String) authentication.getCredentials();
		//String loginId = "kavita";
		//String password = "Welcome@123";
		
		logger.info("Login email : "+loginId);
	    logger.info("Password : "+password);
	    
	    if(loginId == null  || password == null || loginId.equals(null) || password.equals(null) ){
	    	logger.info("Loginid or password is null");
	    	throw new BadCredentialsException("Enter Loginid and Password");
	    }
	    if(loginId.isEmpty() | password.isEmpty())
	    {
	    	logger.info("Loginid or password is empty");
	    	throw new BadCredentialsException("Loginid OR Password is Empty");
	    }
	    
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        
		
		
        HttpSession session = attr.getRequest().getSession();
        if(session == null){
        	logger.info("Session is null");
        	throw new BadCredentialsException("Captcha Validation failed.");
        }
        
        
        /**
         * Captcha Validation
         */
        String inputCaptcha = attr.getRequest().getParameter("captcha");
        String generatedCaptcha = (String)session.getAttribute("captcha");        
		
		  if(inputCaptcha == null || generatedCaptcha == null){
		  logger.info("Input Captcha: "+inputCaptcha+" GeneratedCaptcha:" +generatedCaptcha); 
		  throw new BadCredentialsException("Captcha Validation failed."); }
		 
       
		  /* validate captcha*/
		if (!inputCaptcha.equals(generatedCaptcha)) {
			logger.info("Request param captcha " + inputCaptcha);
			logger.info("Session attr captcha " + generatedCaptcha);
			logger.info("In: CustomAuthenticationProvider Captcha validation falied.");
			throw new BadCredentialsException("Captcha Validation failed.");
		}
        
        
        /**
         *Loading the user Details in SecurityUserModel
         */
        SecurityUserModel   secureUserModel=customUserService.loadUserByUsername(loginId);
        logger.info("Portal User from login Id :"+secureUserModel);
        
		/**
		 *  Check Whether loginid exist
		 */
		if (secureUserModel ==null || secureUserModel.getUsername() == null || secureUserModel.getUsername().equals(null) ) {
        	logger.info("In: CustomAuthenticationProvider LoginId not found.");
            throw new BadCredentialsException("Email address does not exist.");
        }

		/**
         * Check whether pasword is null
         */
        if(secureUserModel.getPassword()==null)
        {
        	logger.error("password in portal_users table is null for loginId :" +loginId);
        	throw new BadCredentialsException("Login Id or password is incorrect");
        }
		
		
		/**
		 * Load the basic user detail to validate the user
		 */
		PortalUserInterface portalInterface = iUserManagementService.getPortalUserInterface(loginId);
		
		/**
		 Check the DB password with the entered user password if wrong then
		 */
		if (password==null || !BCrypt.checkpw(password, secureUserModel.getPassword())) { // replace your custom code here for
			/**
			 * If the password is incorrect than check the number of attempts of the user
			 */
			int noOfLoginAttempt = portalInterface.getNoOfPasswordAttempt();
			noOfLoginAttempt=noOfLoginAttempt-1;
			/**
			 * Update the login attempt of the user
			 */
			iUserManagementService.updateLoginAttempt(noOfLoginAttempt, loginId);
			if(noOfLoginAttempt == 1)
			 {
				 throw new BadCredentialsException("login attempt exhaust");
				 //SENT EMAIL TO USER TO RESET PASSWORD
			 }
			/**
			 * If the login attempt not exhausted then send the error message on page
			 */
			 else
			 { 
				 logger.info("Entered password for loginid "+loginId + " is incorrect ");
				 throw new BadCredentialsException("login id or password is incorrect");
				 
			 }
		}
		/**
		 *  Check if deregistered date is not null or the status of the user is deregister 
		 *  
		 */
		 if(portalInterface.getDeregisterDate()!=null || portalInterface.getStatus().equals("deregister")) {
			 throw new BadCredentialsException("User is no longer registered in the system");
		 }
		
		 /**
		  * After user Validation set the login attempt of the user to 4
		  */
		 iUserManagementService.updateLoginAttempt(4, loginId);

		 /**
		  * Get all the activity list in Granted authorities
		  */
		 Collection<? extends GrantedAuthority> authorities = secureUserModel.getAuthorities();
		 logger.info("Activities assign to the user for loginid "+loginId , authorities);
		return new UsernamePasswordAuthenticationToken(secureUserModel, password, authorities);
	}


	@Override
	public boolean supports(Class<?> authentication) { // TODO return
		return true;

	}
	


}
