package in.gov.lrit.security.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.model.user.SecurityUserModel;
import in.gov.lrit.portal.repository.user.UserManagementRepository;

@Service
public class CustomUserService implements UserDetailsService {

	 @Autowired
	private UserManagementRepository usermanagementRepository;
	 
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
				.getLogger(CustomUserService.class);
	 
	public SecurityUserModel loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("inside CustomUserService for loginId : "+username);
		
		boolean flag =usermanagementRepository.existsById(username);
		if(flag)
		{
		List<String> Activities_list = usermanagementRepository.getAllActivitiesByLoginId(username);
		logger.info("activity list for login Id"+username + Activities_list);
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String activity_name : Activities_list) {
			logger.info(" activity_name  " + activity_name);
			authorities.add(new SimpleGrantedAuthority(activity_name));
		}
		/*Getting the password from user table*/
		String password=usermanagementRepository.getPasswordByLoginId(username);
		logger.info("Password from the table for loginid" +username +"password: "+password);
		SecurityUserModel securityUserModel = new SecurityUserModel(username,password,
				true,true,true,true,authorities);
		logger.info("Inside CustomUserService SecurityUserModel" +securityUserModel);
		return securityUserModel;
	}
	else {
		return null;
	}
	}
	
}
