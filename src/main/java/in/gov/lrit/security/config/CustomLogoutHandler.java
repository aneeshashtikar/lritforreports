package in.gov.lrit.security.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.portal.service.logging.PortalLoggingService;

@Component
public class CustomLogoutHandler implements LogoutHandler {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CustomLogoutHandler.class);

	@Autowired
	private PortalLoggingService loggingService;
	
	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		// TODO Auto-generated method stub
	
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		Integer id = (Integer) session.getAttribute("loggingid");
		loggingService.UpdateLogoutTime(id);
		/*
		 * try { response.sendRedirect("/logout"); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
	}
	 
	 

}
