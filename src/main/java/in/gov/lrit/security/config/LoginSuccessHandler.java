package in.gov.lrit.security.config;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import in.gov.lrit.portal.model.logging.PortalLoginAudit;
import in.gov.lrit.portal.service.logging.PortalLoggingService;
import in.gov.lrit.portal.service.user.IUserManagementService;
import in_.gov.lrit.session.ContractingGovernmentInterface;
import in_.gov.lrit.session.UserSessionInterface;

@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private IUserManagementService iUserManagementService;
	
	@Autowired
	private PortalLoggingService loggingService;
	
	
	
	//using slf4j
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(LoginSuccessHandler.class);

	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		// TODO Auto-generated method stub
		logger.info("Inside LoginSuccessHandler method ");
		HttpSession session = request.getSession();
		session.setMaxInactiveInterval(24000);
		String currentUserLoginId;
		Set<String> currentUserAuthorities;
		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp ts = new Timestamp(time);
		logger.info("Current date and time :"+ts); 
		

		/*Set some session variables*/
		if(authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) { 
			currentUserLoginId= authentication.getName();
			logger.info("Login Email of Current User "+currentUserLoginId);
			currentUserAuthorities = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
			logger.info("Authorities assigned to current user"+currentUserAuthorities);
			UserSessionInterface User  = iUserManagementService.getUserSessionByLoginId(currentUserLoginId);
			session.setAttribute("USER", User);
			if(User.getCategory().equals("USER_CATEGORY_CG"))
			{
				String sarAuthority=iUserManagementService.getSarAuthority(currentUserLoginId);
				session.setAttribute("CoastGuard", sarAuthority);
				
			}
			List<String> pendingTaskList= new ArrayList<String>();
			pendingTaskList=iUserManagementService.getActivityListByType(currentUserLoginId, "pendingtask");
			if(!pendingTaskList.isEmpty())
				session.setAttribute("pendingtask",pendingTaskList);
			else
				logger.info("no pending task assigned to the user");
			
			List<String> contextList= new ArrayList<String>();
			contextList=iUserManagementService.getContextListByLoginId(currentUserLoginId);
			
			List<ContractingGovernmentInterface> cgList= new ArrayList<ContractingGovernmentInterface>();
			cgList=iUserManagementService.getCGByContext(contextList);
			if(!contextList.isEmpty()) 
				session.setAttribute("context",cgList); 
			else
				 logger.info("no context assigned to the user");
			
			String hostIp = request.getRemoteAddr();
			String hostName = request.getRemoteHost();
            try {
                    if (hostName.equals(request.getRemoteAddr())) {
                            InetAddress addr = InetAddress.getByName(request.getRemoteAddr());
                            hostName = addr.getHostName();
                    }

                    if (InetAddress.getLocalHost().getHostAddress().equals(request.getRemoteAddr())) {
                            hostName = "Local Host";
                    }

            }
            catch (UnknownHostException e) {
                    e.printStackTrace();
            }
			/**
			 * Current user Logging in DB
			 */
			PortalLoginAudit portalLoginAudit=new PortalLoginAudit();
			portalLoginAudit.setHostIp(hostIp);
			portalLoginAudit.setHostName(hostName);
			portalLoginAudit.setLoginDateTime(ts);
			portalLoginAudit.setLoginId(currentUserLoginId);
			portalLoginAudit.setLoginStatus("login");
			PortalLoginAudit portalLoginAudit2=loggingService.saveLoginStatus(portalLoginAudit);
			session.setAttribute("loggingid", portalLoginAudit2.getId());
			logger.info("LoginSuccessHandler End"+portalLoginAudit2);
		}

		response.sendRedirect("/lrit/map/mapHome");
	}

}
