package in.gov.lrit.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import in.gov.lrit.security.config.LoginSuccessHandler;
import in.gov.lrit.security.config.CustomLogoutHandler;

@Configuration
@EnableWebSecurity
@ComponentScan
(basePackages= { "in.gov.lrit", "in.gov.lrit.security.config", "in.gov.lrit.portal",
})
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SecurityConfig.class);

	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;
	
	@Autowired
	private LoginSuccessHandler loginSuccessHandler;
	
	@Autowired
	private CustomLogoutHandler customLogoutFilter;
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(customAuthenticationProvider);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		//http.addFilterBefore( new PreAuthFilter(), BasicAuthenticationFilter );
		http.csrf().disable();

		
		http.authorizeRequests().antMatchers("/").permitAll()
		.antMatchers("/users/setpassword").permitAll()
		.antMatchers("/users/forgotpassword").permitAll()
		.antMatchers("/bower_components/**").permitAll()
		.antMatchers("/dist/**").permitAll()
		.antMatchers("/plugins/**").permitAll()
		.antMatchers("/resources/img/**").permitAll()
		.antMatchers("/resources/**").permitAll()
		.antMatchers("/CaptchaServlet").permitAll()
		.antMatchers("/index2").permitAll()
		.antMatchers("/**")
		.authenticated()
		.antMatchers("/logging/setgeolocation").authenticated()
		.and()
		.formLogin().loginPage("/login").permitAll().failureUrl("/").successHandler(loginSuccessHandler);

		http.logout().addLogoutHandler(customLogoutFilter).logoutSuccessUrl("/").invalidateHttpSession(true).deleteCookies("JSESSIONID");
			
		http.sessionManagement().invalidSessionUrl("/").sessionFixation().newSession().maximumSessions(1);
		   http.sessionManagement()
	        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
		   
	}
}