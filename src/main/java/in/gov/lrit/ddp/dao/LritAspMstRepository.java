package in.gov.lrit.ddp.dao;

import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.ddp.model.LritAspMst;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Queries related to LritAspMst
 * @author lrit_billing
 *
 */
public interface LritAspMstRepository extends JpaRepository<LritAspMst, String> {
	
	/**
	 * Query for getLrit Name Dto
	 */
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(lam.aspLritid, lam.name) from LritAspMst lam") 
	public List<LritNameDto> getLritNameDto();
	  
	/**
	 * Query  for Lrit Name Dto By ddp Version
	 */
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(lam.aspLritid, lam.name) from LritAspMst lam where lam.regularVersionNumber = :regularversionNumber") 
	public List<LritNameDto> getLritNameDtoByDddpVersion(@Param("regularversionNumber") String regularversionNumber); 

	/**
	 * Query for get Agency Name by agency Code 
	 */
	@Query("select distinct(lam.name) from LritAspMst lam where lam.aspLritid like  :agencyCode")
	public String getAgencyName(@Param("agencyCode") String agencyCode);

	/**
	 * Query get Agency Name by Agency Code and DDP version
	 *
	 * 
	 */
	@Query("select lam.name from LritAspMst lam where lam.aspLritid like  :agencyCode and lam.regularVersionNumber = :regularversionNumber")
	public String getAgencyNameByAgencyCodeAndDddpVersion(@Param("agencyCode") String agencyCode, @Param("regularversionNumber") String regularversionNumber);
	

	/**
	 * Query for get LritNameDto for Agency LritName Dto
	 */
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(lam.aspLritid, lam.name) from LritAspMst lam where lam.aspLritid not in ('4005')") 
	public List<LritNameDto> getLritNameDtoForAgency2();

	/**
	 * Query for get Lrit Name for Agency 2 By DDp version 
	 *
	 * 
	 */
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(lam.aspLritid, lam.name) from LritAspMst lam where lam.aspLritid not in ('4005') and lam.regularVersionNumber = :regularversionNumber") 
	public List<LritNameDto> getLritNameDtoForAgency2ByDdpVersion(@Param("regularversionNumber") String regularversionNumber);
	
	/**
	 * Query for get LritNameDto for Agency LritName Dto by Date
	 */
	@Query(nativeQuery=true) 
	public List<LritNameDdpVerDto> getLritNameDdpVerDtoForAgency2ByDate(@Param("date") java.sql.Date date);
	
}
