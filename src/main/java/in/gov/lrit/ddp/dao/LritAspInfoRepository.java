/**
 * @LritAspInfoRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 20, 2019
 */
package in.gov.lrit.ddp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.ddp.model.LritAspinfo;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**Queries related to LritAspinfo
 * @author lrit_billing
 *
 */

public interface LritAspInfoRepository extends JpaRepository<LritAspinfo, String> {

	/**
	 * Query for fetch list of AspLritid  
	 *
	 * 
	 */
	@Query("select DISTINCT lai.aspLritid from LritAspinfo lai ")
	List<String> getAspLritId();

	/**
	 * Query for Fetch the LritAspInfo by AspLritId
	 * 
	 * 
	 */
	@Query("select lai from LritAspinfo lai where lai.aspLritid like :agencyCode")
	public List<LritAspinfo> findByAspLritid(@Param("agencyCode") String aspLritid);

	/**
	 * Query  for Fetching AspCg List
	 *
	 * 
	 */
	@Query("select lai.contractingGovernment from LritAspinfo lai where lai.aspLritid like :agencyCode")
	public	List<LritContractingGovtMst> getAspCgList(@Param("agencyCode") String aspLritid);
	
	/**
	 * Query for Fetching Stakeholders by agency Code
	 *
	 * 
	 */
	@Query("select lai.contractingGovernment from LritAspinfo lai where lai.aspLritid like :agencyCode and lai.contractingGovernment.cgLritid in (:stakeholderlist)") 
	public List<LritContractingGovtMst> findStakeholder(@Param("agencyCode") String agencyCode,@Param("stakeholderlist") List<String> stakeholderlist );
	
	/**
	 * Query  for Fetching Non Stakeholders by agency Code
	 *
	 * 
	 */
	@Query("select lai.contractingGovernment from LritAspinfo lai where lai.aspLritid like :agencyCode and lai.contractingGovernment.cgLritid not in (:stakeholderlist)") 
	public List<LritContractingGovtMst> findNonStakeholder(@Param("agencyCode") String agencyCode,@Param("stakeholderlist") List<String> stakeholderlist );

	/**
	 * Query for Fetching stakeholder by agency Code and Ddp Version
	 *
	 * 
	 */
	@Query("select lai.contractingGovernment from LritAspinfo lai where lai.aspLritid like :agencyCode and lai.regularversionNumber = :regularversionNumber and lai.regularversionNumber = lai.contractingGovernment.regularVersionNumber and lai.contractingGovernment.cgLritid in (:stakeholderlist)") 
	public List<LritContractingGovtMst> findStakeholderByAgencyCodeAndDdpVersion(@Param("agencyCode") String agencyCode, @Param("regularversionNumber") String regularversionNumber, @Param("stakeholderlist") List<String> stakeholderlist );
	
	/**
	 * Query Find Non Stakeholders By agency Code and Ddp Version
	 *
	 * 
	 */
	@Query("select lai.contractingGovernment from LritAspinfo lai where lai.aspLritid like :agencyCode and lai.regularversionNumber = :regularversionNumber and lai.regularversionNumber = lai.contractingGovernment.regularVersionNumber and lai.contractingGovernment.cgLritid not in (:stakeholderlist)") 
	public List<LritContractingGovtMst> findNonStakeholderByAgencyCodeAndDdpVersion(@Param("agencyCode") String agencyCode, @Param("regularversionNumber") String regularversionNumber, @Param("stakeholderlist") List<String> stakeholderlist );
	
	/**
	 * Query  for Fetching Contracting Govts details by agency Code
	 *
	 * 
	 */
	@Query("select lai.contractingGovernment from LritAspinfo lai where lai.aspLritid like :agencyCode and lai.regularversionNumber = :regularversionNumber and lai.regularversionNumber = lai.contractingGovernment.regularVersionNumber")
	List<LritContractingGovtMst> findCGDetailsByAgencyCodeAndDdpVersion(@Param("agencyCode") String agencyCode, @Param("regularversionNumber") String regularversionNumber);
	
}
