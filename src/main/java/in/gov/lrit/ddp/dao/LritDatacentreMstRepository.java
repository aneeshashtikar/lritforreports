package in.gov.lrit.ddp.dao;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.ddp.model.LritDatacentreMst;
import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Queries related to LritDatacentreMst
 * @author lrit_billing
 *
 */
public interface LritDatacentreMstRepository extends JpaRepository<LritDatacentreMst, String> {
	

	/**
	 * Query for get list of  LritName Dtos
	 */  
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(ldm.dcLritid, ldm.name) from LritDatacentreMst ldm") 
	List<LritNameDto> getLritNameDto();
	
	/**
	 * Query for get list of  LritName Dtos By Ddp Version
	 */
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(ldm.dcLritid, ldm.name) from LritDatacentreMst ldm where ldm.immediateVersionNumber = :immediateversionNumber") 
	List<LritNameDto> getLritNameDtoByDdpVersion(@Param("immediateversionNumber") String immediateversionNumber);
	
	/**
	 * Query for get list of Dc Lrit Id
	 */
	@Query("select distinct(ldm.name) from LritDatacentreMst ldm where ldm.dcLritid like :dcLritid")
	String getNameByDCLritid(@Param("dcLritid") String dcLritid);

	/**
	 * Query for get list of get Name Dc lrit Id Version
	 */
	@Query("select ldm.name from LritDatacentreMst ldm where ldm.dcLritid like :dcLritid and ldm.immediateVersionNumber = :immediateversionNumber") 
	String getNameByDCLritidByDdpVersion(@Param("dcLritid") String dcLritid, @Param("immediateversionNumber") String immediateversionNumber);
	
	/**
	 * Query for get lrit Name Dto for Agency 2
	 */
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(ldm.dcLritid, ldm.name) from LritDatacentreMst ldm where ldm.dcLritid not in ('3065','3153')") 
	Collection<? extends LritNameDto> getLritNameDtoForAgency2();
	
	/**
	 * Query for get lrit Name Dto for Agency 2 by Ddp Version
	 */
	@Query("select new in_.gov.lrit.ddp.dto.LritNameDto(ldm.dcLritid, ldm.name) from LritDatacentreMst ldm where ldm.dcLritid not in ('3065','3153') and ldm.immediateVersionNumber = :immediateversionNumber") 
	Collection<? extends LritNameDto> getLritNameDtoForAgency2ByDdpVersion(@Param("immediateversionNumber") String immediateversionNumber);

	/**
	 * Query for get lrit Name Dto for Agency 2 by Date
	 */
	@Query(nativeQuery=true) 
	public List<LritNameDdpVerDto> getLritNameDdpVerDtoForAgency2ByDate(@Param("date") java.sql.Date date);

}
