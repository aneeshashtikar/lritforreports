package in.gov.lrit.ddp.dao;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.ddp.model.LritAspMst;
import in.gov.lrit.ddp.model.LritDdpImmediateMst;
import in.gov.lrit.ddp.model.LritDdpMst;
import in.gov.lrit.ddp.model.LritDdpRegularMst; 


/**
 * Queries related to LritDdpImmediateMst
 * @author lrit_billing
 *
 */

public interface LritDdpImmediateMstRepository extends JpaRepository<LritDdpImmediateMst, String> {

	/**
	 * Query for get Immediate Ddp Version by Date
	 */
	@Query(value = "select immediateversion_no from lrit_ddp_immediate_mst where immediateversionimplementationat <= :date order by immediateversionimplementationat desc limit 1", nativeQuery = true)
	public String getApplicableImmediateDdpVersionByDate(@Param("date") Date date);

	/**
	 * Query for get date of ddp Version by ddp Version
	 */
	@Query("select ldim.immediateVersionImplementationAt from LritDdpImmediateMst ldim where ldim.immediateVersionNumber = :ddpVer")
	public Date getImmediateDdpVersionImplementationDate(String ddpVer); 
}
