/**
 * @LritDatacentreInfoRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 21, 2019
 */
package in.gov.lrit.ddp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.billing.model.contract.ContractStakeholder;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.model.LritDatacentreInfo;


/**
 * Queries related to LritDatacentreInfo
 * @author lrit_billing
 *
 */

public interface LritDatacentreInfoRepository extends JpaRepository<LritDatacentreInfo, Integer> {

	/**
	 * Query for get Dclrit ID
	 */
	@Query("select DISTINCT ldi.dcLritid from LritDatacentreInfo ldi")
	List<String> getDcLritId();

	/**
	 * Query for get list Contracting Govts ids by Agency Code
	 */
	@Query("select DISTINCT ldi.cgLritid from LritDatacentreInfo ldi where ldi.dcLritid like :agencyCode")
	List<String> getContractingGoverments(@Param("agencyCode") String agencyCode);

	/**
	 * Query for get list DC by agency Code
	 */
	@Query("select ldi from LritDatacentreInfo ldi where ldi.dcLritid like :agencyCode")
	List<LritDatacentreInfo> getDcDetails(@Param("agencyCode") String agencyCode);

	/**
	 * Query for get list Contracting Govts by agency Code
	 */
	@Query("select ldi.contractingGovernment from LritDatacentreInfo ldi where ldi.dcLritid like :agencyCode")
	List<LritContractingGovtMst> findCGDetails(@Param("agencyCode") String agencyCode);
	
	/**
	 * Query for get list of Stakeholders by agency Code
	 */
	@Query("select ldi.contractingGovernment from LritDatacentreInfo ldi where ldi.dcLritid like :agencyCode and ldi.contractingGovernment.cgLritid in (:stakeholderlist)") 
	List<LritContractingGovtMst> findStakeholder(@Param("agencyCode") String agencyCode,@Param("stakeholderlist") List<String> stakeholderlist );
	
	/**
	 * Query for get list of Non Stakeholders by agency Code
	 */
	@Query("select ldi.contractingGovernment from LritDatacentreInfo ldi where ldi.dcLritid like :agencyCode and ldi.contractingGovernment.cgLritid not in (:stakeholderlist)") 
	List<LritContractingGovtMst> findNonStakeholder(@Param("agencyCode") String agencyCode,@Param("stakeholderlist") List<String> stakeholderlist );
	
	/**
	 * Query for get list of  Stakeholders by agency Code and Ddp Version
	 */
	@Query("select ldi.contractingGovernment from LritDatacentreInfo ldi where ldi.dcLritid like :agencyCode and ldi.regularversionNumber = :ddpVer and ldi.regularversionNumber = ldi.contractingGovernment.regularVersionNumber and ldi.contractingGovernment.cgLritid in (:stakeholderlist)") 
	List<LritContractingGovtMst> findStakeholderByAgencyCodeAndDdpVersion(@Param("agencyCode") String agencyCode,  @Param("ddpVer") String ddpVer, @Param("stakeholderlist") List<String> stakeholderlist );
	
	/**
	 * Query for get list of Non Stakeholders by agency Code and Ddp Version
	 */
	@Query("select ldi.contractingGovernment from LritDatacentreInfo ldi where ldi.dcLritid like :agencyCode and ldi.regularversionNumber = :ddpVer and ldi.regularversionNumber = ldi.contractingGovernment.regularVersionNumber and ldi.contractingGovernment.cgLritid not in (:stakeholderlist)") 
	List<LritContractingGovtMst> findNonStakeholderByAgencyCodeAndDdpVersion(@Param("agencyCode") String agencyCode, @Param("ddpVer") String ddpVer, @Param("stakeholderlist") List<String> stakeholderlist );

	/**
	 * Query for get list of  Contracting Govts by agency Code and Ddp Version
	 */
	@Query("select ldi.contractingGovernment from LritDatacentreInfo ldi where ldi.dcLritid like :agencyCode and  ldi.regularversionNumber = :ddpVer and ldi.regularversionNumber = ldi.contractingGovernment.regularVersionNumber")
	List<LritContractingGovtMst> findCGDetailsByAgencyCodeAndDdpVersion(@Param("agencyCode") String agencyCode, @Param("ddpVer") String ddpVer);

	
}
