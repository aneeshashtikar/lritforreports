package in.gov.lrit.ddp.dao;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.ddp.model.LritAspMst;
import in.gov.lrit.ddp.model.LritDdpMst;
import in.gov.lrit.ddp.model.LritDdpRegularMst; 


/**
 * Queries related to LritDdpRegularMst
 * @author lrit_billing
 *
 */

public interface LritDdpRegularMstRepository extends JpaRepository<LritDdpRegularMst, String> {

	/**
	 * Query for get Regular Ddp version by date
	 */
	@Query(value = "select regularversion_no from lrit_ddp_regular_mst where regularversionimplementationat <= :date order by regularversionimplementationat desc limit 1", nativeQuery = true)
	public String getApplicableRegularDdpVersionByDate(@Param("date") Date date); 
}
