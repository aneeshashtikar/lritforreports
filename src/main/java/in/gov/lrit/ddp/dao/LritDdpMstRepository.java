package in.gov.lrit.ddp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import in.gov.lrit.ddp.model.LritAspMst;
import in.gov.lrit.ddp.model.LritDdpMst; 



/**
 * Queries related to LritDdpMst
 * @author lrit_billing
 *
 */

public interface LritDdpMstRepository extends JpaRepository<LritDdpMst, String> {

}
