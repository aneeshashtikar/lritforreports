package in.gov.lrit.ddp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.ddp.model.LritContractGovtList;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in_.gov.lrit.ddp.dto.LritNameDto; 


/**
 * Queries related to LritContractingGovtMst
 * @author lrit_billing
 *
 */
public interface LritContractingGovtMstRepository extends JpaRepository<LritContractingGovtMst, String> {

	
	/**
	 * Query for get Contracting Govts Name
	 */
	@Query("select distinct(lcgm.cgName) from LritContractingGovtMst lcgm where lcgm.cgLritid like :consumer ")
	public String getCGName(@Param("consumer") String consumer);
	
	/**
	 * Query for get Contracting Govts name by lritid and Ddp Version
	 */
	@Query("select lcgm.cgName from LritContractingGovtMst lcgm where lcgm.cgLritid like :consumer and lcgm.regularVersionNumber = :regularversionNumber")
	public String getCGNameByDdpVersion(@Param("consumer") String consumer, @Param("regularversionNumber") String regularversionNumber);


	/**
	 * Query for get list Contracting Govts by Ddp Version and list of Contract Stakeholders
	 */
	@Query("select lcgm from LritContractingGovtMst lcgm where lcgm.regularVersionNumber = :ddpVer and lcgm.cgLritid in (:agencyContractStakeholders)")
	public List<LritContractingGovtMst> findCGsByDDPVerAndLritids(@Param("ddpVer") String ddpVer,
			@Param("agencyContractStakeholders") List<String> agencyContractStakeholders);

	/**
	 * Query for get List of Non stakeholders by Ddp Version
	 */
	@Query("select lcgm from LritContractingGovtMst lcgm where lcgm.regularVersionNumber = :ddpVer and lcgm.cgLritid not in (:agencyContractStakeholders)")
	public List<LritContractingGovtMst> findNonStakeholderByAndDdpVersion(@Param("ddpVer") String ddpVer,
			@Param("agencyContractStakeholders") List<String> agencyContractStakeholders);

	/**
	 * @return
	 */
	@Query(value="select distinct on (cg_lritid) * from lrit_contract_govt_mst order by cg_lritid",nativeQuery = true)
	public List<LritContractingGovtMst> getAllContractingGovts();
	
	@Query(value="select distinct cg_lritid as cgLritid, cg_name as cgName from lrit_contract_govt_mst ",nativeQuery = true)
	public List<LritContractGovtList> getAllContractingGovtsNames();
}
