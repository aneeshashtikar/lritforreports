package in.gov.lrit.ddp.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the lrit_datacentre_info database table.
 * @author lrit_billing
 */
@Entity
@Table(name="lrit_datacentre_info")
@NamedQuery(name="LritDatacentreInfo.findAll", query="SELECT l FROM LritDatacentreInfo l")
public class LritDatacentreInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="dc_infoid", columnDefinition="VARCHAR(50)")
	private String dcInfoid;

	@Column(name="cg_lritid", columnDefinition="VARCHAR(50)")
	private String cgLritid;

	@Column(name="dc_lritid", columnDefinition="VARCHAR(50)")
	private String dcLritid;

	@ManyToOne
	@JoinColumn(name="cg_lritid",insertable = false, updatable = false)
	private LritContractingGovtMst contractingGovernment;
	
	@Column(name="regularversion_no", columnDefinition="VARCHAR(20)")
	private String regularversionNumber;
	
	@Column(name="dc_immediateversion_no", columnDefinition="VARCHAR(20)")
	private String dcImmediateversionNumber;

	@Column(name="cg_regularversion_no", columnDefinition="VARCHAR(20)")
	private String cgRegularversionNumber;

	public LritDatacentreInfo() {
	}

	public String getDcInfoid() {
		return this.dcInfoid;
	}

	public void setDcInfoid(String dcInfoid) {
		this.dcInfoid = dcInfoid;
	}

	public String getCgLritid() {
		return this.cgLritid;
	}

	public void setCgLritid(String cgLritid) {
		this.cgLritid = cgLritid;
	}

	public String getDcLritid() {
		return this.dcLritid;
	}

	public void setDcLritid(String dcLritid) {
		this.dcLritid = dcLritid;
	}

	
	

	public String getRegularversionNumber() {
		return regularversionNumber;
	}

	public void setRegularversionNumber(String regularversionNumber) {
		this.regularversionNumber = regularversionNumber;
	}

	public String getDcImmediateversionNumber() {
		return dcImmediateversionNumber;
	}

	public void setDcImmediateversionNumber(String dcImmediateversionNumber) {
		this.dcImmediateversionNumber = dcImmediateversionNumber;
	}

	public String getCgRegularversionNumber() {
		return cgRegularversionNumber;
	}

	public void setCgRegularversionNumber(String cgRegularversionNumber) {
		this.cgRegularversionNumber = cgRegularversionNumber;
	}

	/**
	 * @return the contractingGovernment
	 */
	public LritContractingGovtMst getContractingGovernment() {
		return contractingGovernment;
	}

	/**
	 * @param contractingGovernment the contractingGovernment to set
	 */
	public void setContractingGovernment(LritContractingGovtMst contractingGovernment) {
		this.contractingGovernment = contractingGovernment;
	}

	
	

}