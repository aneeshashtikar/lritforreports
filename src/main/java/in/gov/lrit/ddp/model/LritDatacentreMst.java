package in.gov.lrit.ddp.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;

/**
 *  The persistent class for the lrit_datacentre_mst database table.
 * @author lrit_billing
 *
 */


@Entity
@Table(name="lrit_datacentre_mst")
@NamedQuery(name="LritDatacentreMst.findAll", query="SELECT l FROM LritDatacentreMst l")
@NamedQuery(name = "LritMst.findAll", query = "SELECT l FROM LritAspMst l")
@NamedNativeQuery(name = "LritDatacentreMst.getLritNameDdpVerDtoForAgency2ByDate", 
				query = "select distinct on (sq.dc_lritid) sq.dc_lritid as lritid, sq.name as name, sq.immediateversion_no as ddpVer "
						+ "from (select distinct on (lam.dc_lritid, ldrm.immediateversionimplementationat) lam.dc_lritid, lam.name, "
						+ "lam.immediateversion_no from lrit_datacentre_mst lam, lrit_ddp_immediate_mst ldrm "
						+ "where lam.immediateversion_no = ldrm.immediateversion_no and ldrm.immediateversionimplementationat <= :date "
						+ " and lam.dc_lritid not in ('3065', '3153') "
						+ "order by lam.dc_lritid, ldrm.immediateversionimplementationat desc) sq;",
				resultSetMapping = "LritNameDdpVerDto"
		)
/*
 * @SqlResultSetMapping(name = "LritNameDdpVerDto", classes = @ConstructorResult
 * ( targetClass = LritNameDdpVerDto.class, columns = {
 * 
 * @ColumnResult(name = "lritid"),
 * 
 * @ColumnResult(name = "name"),
 * 
 * @ColumnResult(name = "ddpVer") } ) )
 */
public class LritDatacentreMst {
	@Id
	@Column(name="dc_lritid", columnDefinition="VARCHAR(50)")
	private String dcLritid; 
	
	@Column(name="immediateversion_no", columnDefinition="VARCHAR(20)")
	private String immediateVersionNumber; 
	
	@Column(name="type", columnDefinition="VARCHAR(500)")
	private String type;
	
	@Column(name="ide_interface_location", columnDefinition="VARCHAR(500)")
	private String ideInterfaceLocation; 
	
	@Column(name="ddp_interface_location", columnDefinition="VARCHAR(500)")
	private String ddpInterfaceLocation; 
	
	@Column(columnDefinition="VARCHAR(500)")
	private String name; 
	
	@Column(name="additional_information", columnDefinition="VARCHAR(500)")
	private String additionalInformation;

	public String getDcLritid() {
		return dcLritid;
	}

	public void setDcLritid(String dcLritid) {
		this.dcLritid = dcLritid;
	}

	public String getImmediateVersionNumber() {
		return immediateVersionNumber;
	}

	public void setImmediateVersionNumber(String immediateVersionNumber) {
		this.immediateVersionNumber = immediateVersionNumber;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdeInterfaceLocation() {
		return ideInterfaceLocation;
	}

	public void setIdeInterfaceLocation(String ideInterfaceLocation) {
		this.ideInterfaceLocation = ideInterfaceLocation;
	}

	public String getDdpInterfaceLocation() {
		return ddpInterfaceLocation;
	}

	public void setDdpInterfaceLocation(String ddpInterfaceLocation) {
		this.ddpInterfaceLocation = ddpInterfaceLocation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	
	
}
