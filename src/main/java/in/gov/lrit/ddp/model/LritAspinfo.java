package in.gov.lrit.ddp.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the lrit_aspinfo database table.
 *  @author lrit_billing
 */


@Entity
@Table(name="lrit_aspinfo")
@NamedQuery(name="LritAspinfo.findAll", query="SELECT l FROM LritAspinfo l")
public class LritAspinfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="asp_infoid", columnDefinition="VARCHAR(50)")
	private String aspInfoid;

	
	@Column(name="asp_lritid", columnDefinition="VARCHAR(50)")
	private String aspLritid;

	@ManyToOne
	@JoinColumn(name="cg_lritid", columnDefinition="VARCHAR(50)")
	private LritContractingGovtMst contractingGovernment;

	@Column(columnDefinition="VARCHAR(50)")
	private String conditions;

	@Column(name="regularversion_no", columnDefinition="VARCHAR(20)")
	private String regularversionNumber;
	
	@Column(name="asp_regularversion_no", columnDefinition="VARCHAR(20)")
	private String aspRegularversionNumber;

	@Column(name="cg_regularversion_no", columnDefinition="VARCHAR(20)")
	private String cgRegularversionNumber;

	public LritAspinfo() {
	}

	public String getAspInfoid() {
		return this.aspInfoid;
	}

	public void setAspInfoid(String aspInfoid) {
		this.aspInfoid = aspInfoid;
	}

	public String getAspLritid() {
		return this.aspLritid;
	}

	public void setAspLritid(String aspLritid) {
		this.aspLritid = aspLritid;
	}



	/**
	 * @return the contractingGovernment
	 */
	public LritContractingGovtMst getContractingGovernment() {
		return contractingGovernment;
	}

	/**
	 * @param contractingGovernment the contractingGovernment to set
	 */
	public void setContractingGovernment(LritContractingGovtMst contractingGovernment) {
		this.contractingGovernment = contractingGovernment;
	}

	public String getConditions() {
		return this.conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public String getRegularversionNumber() {
		return regularversionNumber;
	}

	public void setRegularversionNumber(String regularversionNumber) {
		this.regularversionNumber = regularversionNumber;
	}

	public String getAspRegularversionNumber() {
		return aspRegularversionNumber;
	}

	public void setAspRegularversionNumber(String aspRegularversionNumber) {
		this.aspRegularversionNumber = aspRegularversionNumber;
	}

	public String getCgRegularversionNumber() {
		return cgRegularversionNumber;
	}

	public void setCgRegularversionNumber(String cgRegularversionNumber) {
		this.cgRegularversionNumber = cgRegularversionNumber;
	}

	
	
}