package in.gov.lrit.ddp.model;

public interface LritContractGovtList {

	String getCgLritid();
	String getCgName();
}
