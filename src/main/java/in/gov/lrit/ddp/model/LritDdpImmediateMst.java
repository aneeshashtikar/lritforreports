package in.gov.lrit.ddp.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the lrit_ddp_immediate_mst database table.
 * @author lrit_billing
 *
 */

@Entity
@Table(name="lrit_ddp_immediate_mst")
@NamedQuery(name="LritDdpImmediateMst.findAll", query="SELECT l FROM LritDdpImmediateMst l")
public class LritDdpImmediateMst {
	@Id
	@Column(name="immediateversion_no", columnDefinition="VARCHAR(20)")
	private String immediateVersionNumber; 
	
	
	
	@Column(name="immediateversionpublishedat", columnDefinition="TIMESTAMP WITH TIME ZONE")
	private Date immediateVersionPublishedAt; 
	
	@Column(name="immediateversionimplementationat", columnDefinition="TIMESTAMP WITH TIME ZONE")
	private Date immediateVersionImplementationAt; 
	
	@Column(name="schemaversion", columnDefinition="VARCHAR(20)")
	private String schemaVersion;

	public String getImmediateVersionNumber() {
		return immediateVersionNumber;
	}

	public void setImmediateVersionNumber(String immediateVersionNumber) {
		this.immediateVersionNumber = immediateVersionNumber;
	}

	public Date getImmediateVersionPublishedAt() {
		return immediateVersionPublishedAt;
	}

	public void setImmediateVersionPublishedAt(Date immediateVersionPublishedAt) {
		this.immediateVersionPublishedAt = immediateVersionPublishedAt;
	}

	public Date getImmediateVersionImplementationAt() {
		return immediateVersionImplementationAt;
	}

	public void setImmediateVersionImplementationAt(Date immediateVersionImplementationAt) {
		this.immediateVersionImplementationAt = immediateVersionImplementationAt;
	}

	public String getSchemaVersion() {
		return schemaVersion;
	}

	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}
	
	
}
