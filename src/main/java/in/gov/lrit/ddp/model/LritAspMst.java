package in.gov.lrit.ddp.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import in_.gov.lrit.ddp.dto.LritNameDdpVerDto;
/**
 * The persistent class for the lrit_asp_mst database table.
 * @author lrit_billing
 *
 */

@Entity
@Table(name = "lrit_asp_mst")

@NamedQuery(name = "LritAspMst.findAll", query = "SELECT l FROM LritAspMst l")
@NamedNativeQuery(name = "LritAspMst.getLritNameDdpVerDtoForAgency2ByDate", 
				query = "select distinct on (sq.asp_lritid) sq.asp_lritid as lritid, sq.name as name, sq.regularversion_no as ddpVer "
						+ "from (select distinct on (lam.asp_lritid, ldrm.regularversionimplementationat) lam.asp_lritid, lam.name, "
						+ "lam.regularversion_no from lrit_asp_mst lam, lrit_ddp_regular_mst ldrm "
						+ "where lam.regularversion_no = ldrm.regularversion_no and ldrm.regularversionimplementationat <= :date "
						+ "and lam.asp_lritid not in ('4005') "
						+ "order by lam.asp_lritid, ldrm.regularversionimplementationat desc) sq;",
				resultSetMapping = "LritNameDdpVerDto"
		)
@SqlResultSetMapping(name = "LritNameDdpVerDto",
				classes = @ConstructorResult ( targetClass = LritNameDdpVerDto.class,
				  columns = { 
						  @ColumnResult(name = "lritid"),
						  @ColumnResult(name = "name"),
						  @ColumnResult(name = "ddpVer")
				  }
				)
		)

public class LritAspMst {
	@Id
	@Column(name = "asp_lritid", columnDefinition="VARCHAR(50)")
	private String aspLritid;

	@Column(name = "regularversion_no", columnDefinition="VARCHAR(20)")
	private String regularVersionNumber;

	@Column(columnDefinition="VARCHAR(1000)")
	private String name;

	public LritAspMst() {
		super();
	}

	/**
	 * @return the aspLritid
	 */
	public String getAspLritid() {
		return aspLritid;
	}

	/**
	 * @param aspLritid the aspLritid to set
	 */
	public void setAspLritid(String aspLritid) {
		this.aspLritid = aspLritid;
	}

	

	public String getRegularVersionNumber() {
		return regularVersionNumber;
	}

	public void setRegularVersionNumber(String regularVersionNumber) {
		this.regularVersionNumber = regularVersionNumber;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
