package in.gov.lrit.ddp.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the lrit_ddp_mst database table.
 * @author lrit_billing
 *
 */

@Entity
@Table(name="lrit_ddp_mst")
@NamedQuery(name="LritDdpMst.findAll", query="SELECT l FROM LritDdpMst l")
public class LritDdpMst {
	@Id
	@Column(name="ddpversion_no")
	private String ddpversionNo; 
	
	
	
	@Column(name="regularversionpublishedat")
	private Date regularVersionPublishedAt; 
	
	@Column(name="regularversionimplementationat")
	private Date regularVersionImplementationAt; 
	
	
	@Column(name="immediateversionpublishedat")
	private Date immediateVersionPublishedAt;
	
	@Column(name="immediateversionimplementationat")
	private Date immediateVersionImplementationAt;
	
	@Column(name="schemaversion")
	private String schemaVersion;
}
