package in.gov.lrit.ddp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the lrit_contract_govt_mst database table.
 * @author lrit_billing
 *
 */


@Entity
@Table(name="lrit_contract_govt_mst")
@NamedQuery(name="LritContractingGovtMst.findAll", query="SELECT l FROM LritContractingGovtMst l")
public class LritContractingGovtMst {
	@Id
	@Column(name="cg_lritid", columnDefinition="VARCHAR(50)")
	private String cgLritid; 
	
	@Column(name="regularversion_no", columnDefinition="VARCHAR(20)")
	private String regularVersionNumber; 
	
	@Column(name="iso_code", columnDefinition="VARCHAR(10)")
	private String isoCode;
	
	@Column(name="cg_name", columnDefinition="VARCHAR(50)")
	private String cgName; 
	
	@Column(name="parent_lritid", columnDefinition="VARCHAR(50)")
	private String parentLritid;

	/**
	 * @return the cgLritid
	 */
	public String getCgLritid() {
		return cgLritid;
	}

	/**
	 * @param cgLritid the cgLritid to set
	 */
	public void setCgLritid(String cgLritid) {
		this.cgLritid = cgLritid;
	}

	

	public String getRegularVersionNumber() {
		return regularVersionNumber;
	}

	public void setRegularVersionNumber(String regularVersionNumber) {
		this.regularVersionNumber = regularVersionNumber;
	}

	/**
	 * @return the isoCode
	 */
	public String getIsoCode() {
		return isoCode;
	}

	/**
	 * @param isoCode the isoCode to set
	 */
	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	/**
	 * @return the cgName
	 */
	public String getCgName() {
		return cgName;
	}

	/**
	 * @param cgName the cgName to set
	 */
	public void setCgName(String cgName) {
		this.cgName = cgName;
	}

	/**
	 * @return the parentLritid
	 */
	public String getParentLritid() {
		return parentLritid;
	}

	/**
	 * @param parentLritid the parentLritid to set
	 */
	public void setParentLritid(String parentLritid) {
		this.parentLritid = parentLritid;
	}

	@Override
	public String toString() {
		return "LritContractingGovtMst [cgLritid=" + cgLritid + ", regularVersionNumber=" + regularVersionNumber + ", isoCode="
				+ isoCode + ", cgName=" + cgName + ", parentLritid=" + parentLritid + "]";
	} 
	
	
}
