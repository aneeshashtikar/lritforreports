package in.gov.lrit.ddp.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the lrit_ddp_regular_mst database table.
 * @author lrit_billing
 *
 */

@Entity
@Table(name="lrit_ddp_regular_mst")
@NamedQuery(name="LritDdpRegularMst.findAll", query="SELECT l FROM LritDdpRegularMst l")
public class LritDdpRegularMst {
	@Id
	@Column(name="regularversion_no", columnDefinition="VARCHAR(20)")
	private String regularVersionNumber; 
	
	
	
	@Column(name="regularversionpublishedat", columnDefinition="TIMESTAMP WITH TIME ZONE")
	private Date regularVersionPublishedAt; 
	
	@Column(name="regularversionimplementationat", columnDefinition="TIMESTAMP WITH TIME ZONE")
	private Date regularVersionImplementationAt; 
	
	@Column(name="schemaversion", columnDefinition="VARCHAR(20)")
	private String schemaVersion;

	public String getRegularVersionNumber() {
		return regularVersionNumber;
	}

	public void setRegularVersionNumber(String regularVersionNumber) {
		this.regularVersionNumber = regularVersionNumber;
	}

	public Date getRegularVersionPublishedAt() {
		return regularVersionPublishedAt;
	}

	public void setRegularVersionPublishedAt(Date regularVersionPublishedAt) {
		this.regularVersionPublishedAt = regularVersionPublishedAt;
	}

	public Date getRegularVersionImplementationAt() {
		return regularVersionImplementationAt;
	}

	public void setRegularVersionImplementationAt(Date regularVersionImplementationAt) {
		this.regularVersionImplementationAt = regularVersionImplementationAt;
	}

	public String getSchemaVersion() {
		return schemaVersion;
	}

	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}
	
	
}
