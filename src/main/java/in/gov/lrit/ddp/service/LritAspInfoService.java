/**
 * @LritAspInfoService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 22, 2019
 */
package in.gov.lrit.ddp.service;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.ddp.model.LritAspinfo;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.model.LritDatacentreInfo;
import in_.gov.lrit.ddp.dto.LritNameDto;

/**
 * Service layer interface to handle LritAspInfo entity related activities 
 * @author lrit_billing
 *
 */
public interface LritAspInfoService {

	/**
	 * @param agencyCode
	 * @return
	 */
//	List<String> getContractingGoverments(String agencyCode);

	
	public List<LritAspinfo> findAllAspDetails();


	/**
	 * @param aspLritid
	 * @return
	 */
	
	List<LritAspinfo> findAspDetails(String aspLritid);


	List<LritContractingGovtMst> getAspCgList(String aspLritid);


	public List<LritContractingGovtMst> getAspCgListByAgencyCodeAndDdpVer(String agencyCode, String ddpVer);

	
	/**
	 * 
	 * @param date
	 * @return DDP version applicable before date
	 */
	String getApplicableRegularDdpVersionByDate( Date date); 
}
