/**
 * @LritDatacentreInfoService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 22, 2019
 */
package in.gov.lrit.ddp.service;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.query.Param;

import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.model.LritDatacentreInfo;

/**
 *  Service layer interface to handle LritDatacentreInfo entity related activities 
 * @author lrit_billing
 *
 */
public interface LritDatacentreInfoService {

	/**
	 * @param agencyCode
	 * @return
	 */
	List<String> getContractingGoverments(String agencyCode);

	/**
	 * @param agencyCode
	 * @return
	 */
	List<LritDatacentreInfo> findDCDetails(String agencyCode);
	
	List<LritContractingGovtMst> findCGDetails(String agencyCode);

	Collection<? extends LritContractingGovtMst> findCGDetailsByAgencyCodeAndDdpVer(String agencyCode, String ddpVer);


	/**
	 * 
	 * @param date
	 * @return immediate ddp version applicable for given date
	 */
	String getApplicableImmediateDdpVersionByDate(Date date);

	/**
	 * @param ddpVer
	 * @return
	 */
	Date getImmediateDdpVersionImplementationDate(String ddpVer);
	
}
