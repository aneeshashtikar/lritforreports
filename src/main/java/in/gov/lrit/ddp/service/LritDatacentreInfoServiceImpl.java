/**
 * @LritDatacentreInfoServiceImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 22, 2019
 */
package in.gov.lrit.ddp.service;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.ddp.dao.LritDatacentreInfoRepository;
import in.gov.lrit.ddp.dao.LritDdpImmediateMstRepository;
import in.gov.lrit.ddp.dao.LritDdpRegularMstRepository;
import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.ddp.model.LritDatacentreInfo;

/**
 * Service layer interface to handle LritDatacentreInfo entity related activities
 * @author lrit_billing
 *
 */
@Service
public class LritDatacentreInfoServiceImpl implements LritDatacentreInfoService {

	@Autowired
	private LritDatacentreInfoRepository lritDataCentreInfoRepo;
	
	
	@Autowired
	private LritDdpImmediateMstRepository lritDdpImmediateMstRepo; 
	
	/**
	 * To get list of CG lrit ids by agency Code
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<String> getContractingGoverments(String agencyCode) {
		return lritDataCentreInfoRepo.getContractingGoverments(agencyCode);
	}

	/**
	 * To DC details as per agency code
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritDatacentreInfo> findDCDetails(String agencyCode) {
		// TODO Auto-generated method stub
		return lritDataCentreInfoRepo.getDcDetails(agencyCode);
	}

	/**
	 * To find CG details by Agency Code
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> findCGDetails(String agencyCode) {
		return lritDataCentreInfoRepo.findCGDetails(agencyCode);		
	}

	/**
	 * To find CG details by agency Code and Ddp version
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Collection<? extends LritContractingGovtMst> findCGDetailsByAgencyCodeAndDdpVer(String agencyCode,
			String ddpVer) {
		
		return lritDataCentreInfoRepo.findCGDetailsByAgencyCodeAndDdpVersion(agencyCode, ddpVer);
	}

	/**
	 * To immediate ddp Version by date
	 * @author lrit-billing
	 * 
	 */
	@Override
	public String getApplicableImmediateDdpVersionByDate(Date date) {
		// TODO Auto-generated method stub
		return lritDdpImmediateMstRepo.getApplicableImmediateDdpVersionByDate(date);
	}

	/**
	 * To get Immediate version date by version
	 * @author lrit-billing
	 * 
	 */
	@Override
	public Date getImmediateDdpVersionImplementationDate(String ddpVer) {
		// TODO Auto-generated method stub
		Date date = null; 
		date = lritDdpImmediateMstRepo.getImmediateDdpVersionImplementationDate(ddpVer);
		return date;
		
	}

	

}
