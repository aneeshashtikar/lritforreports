/**
 * @LritAspInfoImpl.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author Mayur Kulkarni 
 * @version 1.0 Aug 22, 2019
 */
package in.gov.lrit.ddp.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.billing.controller.contract.ContractController;
import in.gov.lrit.ddp.dao.LritAspInfoRepository;
import in.gov.lrit.ddp.dao.LritDdpImmediateMstRepository;
import in.gov.lrit.ddp.dao.LritDdpRegularMstRepository;
import in.gov.lrit.ddp.model.LritAspinfo;
import in.gov.lrit.ddp.model.LritContractingGovtMst;

/**
 * Service layer interface to handle LritAspInfo entity related activities 
 * @author lrit_billing
 *
 */
@Service
public class LritAspInfoServiceImpl implements LritAspInfoService {

	@Autowired
	private LritAspInfoRepository lritAspInfoRepo;
	
	// logger
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ContractController.class);
	
	@Autowired
	private LritDdpRegularMstRepository lritDdpRegularMstRepo; 

	/**
	 * To find all Asp Details
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritAspinfo> findAllAspDetails() {
		return lritAspInfoRepo.findAll();
	}

	/**
	 * To find Lrit Asp by Asp Lrit Id
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritAspinfo> findAspDetails(String aspLritid) {
		return lritAspInfoRepo.findByAspLritid(aspLritid);
	}

	/**
	 * To get Asp Cg list by asp lrit id
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> getAspCgList(String aspLritid) {
		return lritAspInfoRepo.getAspCgList(aspLritid);
	}

	/**
	 * To get asp cg list by Agency Code and Ddp Version
	 * @author lrit-billing
	 * 
	 */
	@Override
	public List<LritContractingGovtMst> getAspCgListByAgencyCodeAndDdpVer(String agencyCode, String ddpVer) {
		// TODO Auto-generated method stub
		return lritAspInfoRepo.findCGDetailsByAgencyCodeAndDdpVersion(agencyCode, ddpVer);
	}

	/**
	 * To get regular version by date 
	 * @author lrit-billing
	 * 
	 */
	@Override
	public String getApplicableRegularDdpVersionByDate(Date date) {
		// TODO Auto-generated method stub
		//java.util.Date utilDate = new java.util.Date(date.getTime());
		logger.info("getApplicableRegularDdpVersionByDate " + date );
		return lritDdpRegularMstRepo.getApplicableRegularDdpVersionByDate(date);
	
	}
}
