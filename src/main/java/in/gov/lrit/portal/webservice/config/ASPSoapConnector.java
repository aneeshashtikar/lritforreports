package in.gov.lrit.portal.webservice.config;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;

import org.imo.gisis.xml.lrit._2008.Response;
import org.imo.gisis.xml.lrit.dnidrequest._2008.DNIDRequestType;
import org.springframework.context.annotation.Configuration;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;


@Configuration
public class ASPSoapConnector extends WebServiceGatewaySupport {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ASPSoapConnector.class);
	
	/* ASP Web Service */
	public Response callASPWebService(String url, DNIDRequestType dnidReq) throws SOAPException
	{
		/*return getWebServiceTemplate().marshalSendAndReceive(url, dnidReq);*/
		logger.info("Inside call ASP web service");
		Response resp=null;
		try {
		MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.DEFAULT_SOAP_PROTOCOL);
		SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		WebServiceTemplate wsTemplet = getWebServiceTemplate();
		wsTemplet.setMessageFactory(saajSoapMessageFactory);
		resp = (Response) wsTemplet.marshalSendAndReceive(url, dnidReq);
		logger.info("Response " + resp.getResponse());

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return resp;
	}
	
}
