package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;


/**
 * The persistent class for the portal_georaphical_area database table.
 * 
 */
@Entity
@Table(name="portal_georaphical_area")
public class PortalGeoraphicalArea implements Serializable {

	@Override
	public String toString() {
		return "PortalGeoraphicalArea [areaCode=" + areaCode + ", areaName=" + areaName + ", areaType=" + areaType
				+ ", creationDate=" + creationDate + ", leftBottomLatitude=" + leftBottomLatitude
				+ ", leftBottomLongjitude=" + leftBottomLongjitude + ", loginId=" + loginId + ", rejectionDate="
				+ rejectionDate + ", rightTopLatitude=" + rightTopLatitude + ", rightTopLongjitude="
				+ rightTopLongjitude + ", uploadedFilePath=" + Arrays.toString(uploadedFilePath) + ", status=" + status
				+ "]";
	}

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="area_code")
	private String areaCode;

	@Column(name="area_name")
	private String areaName;

	@Column(name="area_type")
	private String areaType;

	@Column(name="creation_date")
	private Timestamp creationDate;

	@Column(name="left_bottom_latitude")
	private BigDecimal leftBottomLatitude;

	@Column(name="left_bottom_longjitude")
	private BigDecimal leftBottomLongjitude;

	@Column(name="login_id")
	private String loginId;

	@Column(name="rejection_date")
	private Timestamp rejectionDate;

	@Column(name="right_top_latitude")
	private BigDecimal rightTopLatitude;

	@Column(name="right_top_longjitude")
	private BigDecimal rightTopLongjitude;

	@Column(name="uploaded_file")
	private byte[] uploadedFilePath;
	
	@Column(name="status")
	private String status;

	//bi-directional many-to-one association to PortalGeoraphicalAreaRequest
	//@OneToMany(mappedBy="portalGeoraphicalArea")
	//private List<PortalGeoraphicalAreaRequest> portalGeoraphicalAreaRequests;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PortalGeoraphicalArea() {
	}

	public String getAreaCode() {
		return this.areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaType() {
		return this.areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public Timestamp getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public BigDecimal getLeftBottomLatitude() {
		return this.leftBottomLatitude;
	}

	public void setLeftBottomLatitude(BigDecimal leftBottomLatitude) {
		this.leftBottomLatitude = leftBottomLatitude;
	}

	public BigDecimal getLeftBottomLongjitude() {
		return this.leftBottomLongjitude;
	}

	public void setLeftBottomLongjitude(BigDecimal leftBottomLongjitude) {
		this.leftBottomLongjitude = leftBottomLongjitude;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Timestamp getRejectionDate() {
		return this.rejectionDate;
	}

	public void setRejectionDate(Timestamp rejectionDate) {
		this.rejectionDate = rejectionDate;
	}

	public BigDecimal getRightTopLatitude() {
		return this.rightTopLatitude;
	}

	public void setRightTopLatitude(BigDecimal rightTopLatitude) {
		this.rightTopLatitude = rightTopLatitude;
	}

	public BigDecimal getRightTopLongjitude() {
		return this.rightTopLongjitude;
	}

	public void setRightTopLongjitude(BigDecimal rightTopLongjitude) {
		this.rightTopLongjitude = rightTopLongjitude;
	}

	public byte[] getUploadedFilePath() {
		return this.uploadedFilePath;
	}

	public void setUploadedFilePath(byte[] uploadedFilePath) {
		this.uploadedFilePath = uploadedFilePath;
	}

	/*
	 * public List<PortalGeoraphicalAreaRequest> getPortalGeoraphicalAreaRequests()
	 * { return this.portalGeoraphicalAreaRequests; }
	 * 
	 * public void
	 * setPortalGeoraphicalAreaRequests(List<PortalGeoraphicalAreaRequest>
	 * portalGeoraphicalAreaRequests) { this.portalGeoraphicalAreaRequests =
	 * portalGeoraphicalAreaRequests; }
	 */

	/*
	 * public PortalGeoraphicalAreaRequest
	 * addPortalGeoraphicalAreaRequest(PortalGeoraphicalAreaRequest
	 * portalGeoraphicalAreaRequest) {
	 * getPortalGeoraphicalAreaRequests().add(portalGeoraphicalAreaRequest);
	 * portalGeoraphicalAreaRequest.setPortalGeoraphicalArea(this);
	 * 
	 * return portalGeoraphicalAreaRequest; }
	 * 
	 * public PortalGeoraphicalAreaRequest
	 * removePortalGeoraphicalAreaRequest(PortalGeoraphicalAreaRequest
	 * portalGeoraphicalAreaRequest) {
	 * getPortalGeoraphicalAreaRequests().remove(portalGeoraphicalAreaRequest);
	 * portalGeoraphicalAreaRequest.setPortalGeoraphicalArea(null);
	 * 
	 * return portalGeoraphicalAreaRequest; }
	 */

}