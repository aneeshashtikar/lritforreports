package in.gov.lrit.portal.model.pendingtask;

public interface ShipEquipmentHstInterface {
	
	public Integer getDnidNo();
	public Integer getMemberNo();

}
