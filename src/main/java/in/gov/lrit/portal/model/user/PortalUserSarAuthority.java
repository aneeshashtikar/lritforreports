package in.gov.lrit.portal.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;

@Entity
@Table(name="portal_user_sarauthority")
//@TableGenerator(name="portal_user_sarauthority")
//@IdClass(PortalUserSarAuthorityId.class)
public class PortalUserSarAuthority implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Id
	
	@Id
	@SequenceGenerator(name="seq-gen",sequenceName="seq_portal_sarauthority", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.AUTO, generator="seq-gen")
	private Integer Id;
	
	/*
	@OneToOne
	@JoinColumn(name="loginid")
	private PortalUser loginId;
*/
	
	
	@Column(name="login_id")
	private String loginId;
	
	//@Id
	@ManyToOne
	@JoinColumn(name="sar_authority",nullable = false)
	private PortalLritIdMaster sarAuthority;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginid) {
		this.loginId = loginid;
	}

	public PortalLritIdMaster getsarAuthority() {
		return sarAuthority;
	}

	public void setsarAuthority(PortalLritIdMaster sarauthority) {
		this.sarAuthority = sarauthority;
	}

	@Override
	public String toString() {
		return "PortalUserSarauthority [loginId=" + loginId + ", sarAuthority=" + sarAuthority + "]";
	}
}
