package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the dc_ddpnotification database table.
 * 
 */
@Entity
@Table(name="dc_ddpnotification")
@NamedQuery(name="DcDdpnotification.findAll", query="SELECT d FROM DcDdpnotification d")
public class DcDdpnotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="message_id")
	private String messageId;

	@Column(name="ddpnotification_timestamp")
	private Timestamp ddpnotificationTimestamp;

	@Column(name="ddpversion_no")
	private String ddpversionNo;

	@Column(name="lrit_timestamp")
	private Timestamp lritTimestamp;

	private String message;

	@Column(name="message_type")
	private Integer messageType;

	@Column(name="schema_version")
	private BigDecimal schemaVersion;

	private Integer test;

	@Column(name="update_type")
	private Integer updateType;

	public DcDdpnotification() {
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Timestamp getDdpnotificationTimestamp() {
		return this.ddpnotificationTimestamp;
	}

	public void setDdpnotificationTimestamp(Timestamp ddpnotificationTimestamp) {
		this.ddpnotificationTimestamp = ddpnotificationTimestamp;
	}

	public String getDdpversionNo() {
		return this.ddpversionNo;
	}

	public void setDdpversionNo(String ddpversionNo) {
		this.ddpversionNo = ddpversionNo;
	}

	public Timestamp getLritTimestamp() {
		return this.lritTimestamp;
	}

	public void setLritTimestamp(Timestamp lritTimestamp) {
		this.lritTimestamp = lritTimestamp;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public BigDecimal getSchemaVersion() {
		return this.schemaVersion;
	}

	public void setSchemaVersion(BigDecimal schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	public Integer getTest() {
		return this.test;
	}

	public void setTest(Integer test) {
		this.test = test;
	}

	public Integer getUpdateType() {
		return this.updateType;
	}

	public void setUpdateType(Integer updateType) {
		this.updateType = updateType;
	}

}