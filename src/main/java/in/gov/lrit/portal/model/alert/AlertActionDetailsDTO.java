package in.gov.lrit.portal.model.alert;

import java.math.BigInteger;
import java.sql.Timestamp;

public interface AlertActionDetailsDTO {
	
	public String getUserId();
	public Timestamp getClosedTimestamp();
	public String getStatus();
	public String getRemark();
	public BigInteger getAlertTxnId();
	public AlertDetailsDTO getAlertDetails();

}
