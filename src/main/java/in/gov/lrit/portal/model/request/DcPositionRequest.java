package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the dc_position_request database table.
 * 
 */
@Entity
@Table(name="dc_position_request")
@NamedQuery(name="DcPositionRequest.findAll", query="SELECT d FROM DcPositionRequest d")
public class DcPositionRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="message_id")
	private String messageId;

	@Column(name="access_type")
	private Integer accessType;

	@Column(name="data_user_provider")
	private String dataUserProvider;

	@Column(name="data_user_requestor")
	private String dataUserRequestor;

	@Column(name="ddpversion_no")
	private String ddpversionNo;

	private Integer distance;

	@Column(name="imo_no")
	private String imoNo;

	@Column(name="lrit_timestamp")
	private Timestamp lritTimestamp;

	@Column(name="message_type")
	private Integer messageType;

	@Column(name="next_timestamp")
	private Timestamp nextTimestamp;

	private String place;

	private String port;

	@Column(name="port_facility")
	private String portFacility;

	@Column(name="position_request_timestamp")
	private Timestamp positionRequestTimestamp;

	@Column(name="receipt_flag")
	private Boolean receiptFlag;

	@Column(name="req_duration_start")
	private Timestamp reqDurationStart;

	@Column(name="req_duration_stop")
	private Timestamp reqDurationStop;

	@Column(name="request_status")
	private String requestStatus;

	@Column(name="request_type")
	private Integer requestType;

	@Column(name="schema_version")
	private BigDecimal schemaVersion;

	private Integer test;

	public DcPositionRequest() {
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Integer getAccessType() {
		return this.accessType;
	}

	public void setAccessType(Integer accessType) {
		this.accessType = accessType;
	}

	public String getDataUserProvider() {
		return this.dataUserProvider;
	}

	public void setDataUserProvider(String dataUserProvider) {
		this.dataUserProvider = dataUserProvider;
	}

	public String getDataUserRequestor() {
		return this.dataUserRequestor;
	}

	public void setDataUserRequestor(String dataUserRequestor) {
		this.dataUserRequestor = dataUserRequestor;
	}

	public String getDdpversionNo() {
		return this.ddpversionNo;
	}

	public void setDdpversionNo(String ddpversionNo) {
		this.ddpversionNo = ddpversionNo;
	}

	public Integer getDistance() {
		return this.distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public String getImoNo() {
		return this.imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public Timestamp getLritTimestamp() {
		return this.lritTimestamp;
	}

	public void setLritTimestamp(Timestamp lritTimestamp) {
		this.lritTimestamp = lritTimestamp;
	}

	public Integer getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public Timestamp getNextTimestamp() {
		return this.nextTimestamp;
	}

	public void setNextTimestamp(Timestamp nextTimestamp) {
		this.nextTimestamp = nextTimestamp;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getPort() {
		return this.port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getPortFacility() {
		return this.portFacility;
	}

	public void setPortFacility(String portFacility) {
		this.portFacility = portFacility;
	}

	public Timestamp getPositionRequestTimestamp() {
		return this.positionRequestTimestamp;
	}

	public void setPositionRequestTimestamp(Timestamp positionRequestTimestamp) {
		this.positionRequestTimestamp = positionRequestTimestamp;
	}

	public Boolean getReceiptFlag() {
		return this.receiptFlag;
	}

	public void setReceiptFlag(Boolean receiptFlag) {
		this.receiptFlag = receiptFlag;
	}

	public Timestamp getReqDurationStart() {
		return this.reqDurationStart;
	}

	public void setReqDurationStart(Timestamp reqDurationStart) {
		this.reqDurationStart = reqDurationStart;
	}

	public Timestamp getReqDurationStop() {
		return this.reqDurationStop;
	}

	public void setReqDurationStop(Timestamp reqDurationStop) {
		this.reqDurationStop = reqDurationStop;
	}

	public String getRequestStatus() {
		return this.requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public Integer getRequestType() {
		return this.requestType;
	}

	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	public BigDecimal getSchemaVersion() {
		return this.schemaVersion;
	}

	public void setSchemaVersion(BigDecimal schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	public Integer getTest() {
		return this.test;
	}

	public void setTest(Integer test) {
		this.test = test;
	}

}