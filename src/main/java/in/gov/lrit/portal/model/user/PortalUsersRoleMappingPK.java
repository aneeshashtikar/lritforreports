package in.gov.lrit.portal.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PortalUsersRoleMappingPK implements Serializable{
	@Column(name="role_id")
	private String roleId;

	@Column(name="login_id")
	private String loginId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	@Override
	public String toString() {
		return "PortalUsersRoleMappingPK [roleId=" + roleId + ", loginId=" + loginId + "]";
	}

}
