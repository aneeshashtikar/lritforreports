package in.gov.lrit.portal.model.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="lrit_latest_ship_postion")
//@NamedQuery(name="LritLatestShipPostion.findAll", query="SELECT p FROM LritLatestShipPostion p")
public class LritLatestShipPostion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	 

	//  ship_type character varying(10),
	
	  
	 
	
	@Id
	@Column(name="imo_no")
	private String imoNo;

	@Column(name="asp_id")
	private String aspId;

	private BigDecimal course;

	@Column(name="csp_id")
	private String cspId;

	@Column(name="ship_type")
	private String shiptype;
	
	@Column(name="data_user_provider")
	private Long dataUserProvider;

	@Column(name="data_user_requestor")
	private Long dataUserRequestor;

	@Column(name="dc_id")
	private Long dcId;

	@Column(name="ddpversion_no")
	private String ddpversionNo;

	//private String flag;

	private BigDecimal latitude;

	private BigDecimal longitude;

	@Column(name="message_id")
	private String messageId;

	@Column(name="message_type")
	private Long messageType;

	@Column(name="mmsi_no")
	private String mmsiNo;

	@Column(name="reference_id")
	private String referenceId;

	@Column(name="ship_borne_equipment_id")
	private String shipBorneEquipmentId;

	@Column(name="ship_name")
	private String shipName;

	@Column(name="ship_status")
	private String shipStatus;

	private BigDecimal speed;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp timestamp1;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp timestamp4;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp timestamp5;

	public LritLatestShipPostion() {
	}

	public String getImoNo() {
		return this.imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public String getAspId() {
		return this.aspId;
	}

	public void setAspId(String aspId) {
		this.aspId = aspId;
	}

	public BigDecimal getCourse() {
		return this.course;
	}

	public void setCourse(BigDecimal course) {
		this.course = course;
	}

	public String getCspId() {
		return this.cspId;
	}

	public void setCspId(String cspId) {
		this.cspId = cspId;
	}

	public Long getDataUserProvider() {
		return this.dataUserProvider;
	}

	public void setDataUserProvider(Long dataUserProvider) {
		this.dataUserProvider = dataUserProvider;
	}

	public Long getDataUserRequestor() {
		return this.dataUserRequestor;
	}

	public void setDataUserRequestor(Long dataUserRequestor) {
		this.dataUserRequestor = dataUserRequestor;
	}

	public Long getDcId() {
		return this.dcId;
	}

	public void setDcId(Long dcId) {
		this.dcId = dcId;
	}

	public String getDdpversionNo() {
		return this.ddpversionNo;
	}

	public void setDdpversionNo(String ddpversionNo) {
		this.ddpversionNo = ddpversionNo;
	}

	/*
	 * public String getFlag() { return this.flag; }
	 * 
	 * public void setFlag(String flag) { this.flag = flag; }
	 */

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Long getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Long messageType) {
		this.messageType = messageType;
	}

	public String getMmsiNo() {
		return this.mmsiNo;
	}

	public void setMmsiNo(String mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public String getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getShipBorneEquipmentId() {
		return this.shipBorneEquipmentId;
	}

	public void setShipBorneEquipmentId(String shipBorneEquipmentId) {
		this.shipBorneEquipmentId = shipBorneEquipmentId;
	}

	public String getShipName() {
		return this.shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getShipStatus() {
		return this.shipStatus;
	}

	public void setShipStatus(String shipStatus) {
		this.shipStatus = shipStatus;
	}

	public BigDecimal getSpeed() {
		return this.speed;
	}

	public void setSpeed(BigDecimal speed) {
		this.speed = speed;
	}

	public Timestamp getTimestamp1() {
		return this.timestamp1;
	}

	public void setTimestamp1(Timestamp timestamp1) {
		this.timestamp1 = timestamp1;
	}

	public Timestamp getTimestamp4() {
		return this.timestamp4;
	}

	public void setTimestamp4(Timestamp timestamp4) {
		this.timestamp4 = timestamp4;
	}

	public Timestamp getTimestamp5() {
		return this.timestamp5;
	}

	public void setTimestamp5(Timestamp timestamp5) {
		this.timestamp5 = timestamp5;
	}
	
}
