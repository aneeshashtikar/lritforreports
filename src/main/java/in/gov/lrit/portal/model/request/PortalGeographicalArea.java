/*
 * package in.gov.lrit.portal.model.request;
 * 
 * import java.io.Serializable; import javax.persistence.*; import
 * java.sql.Timestamp; import java.util.List;
 * 
 * 
 *//**
	 * The persistent class for the portal_geographical_area database table.
	 * 
	 *//*
		 * @Entity
		 * 
		 * @Table(name="portal_geographical_area")
		 * 
		 * @NamedQuery(name="PortalGeographicalArea.findAll",
		 * query="SELECT p FROM PortalGeographicalArea p") public class
		 * PortalGeographicalArea implements Serializable { private static final long
		 * serialVersionUID = 1L;
		 * 
		 * 
		 * @Id
		 * 
		 * @GeneratedValue(strategy = GenerationType.IDENTITY)
		 * 
		 * @Column(name = "gml_id",columnDefinition = "serial") private int gmlId;
		 * 
		 * @Column(name="area_type") private String areaType;
		 * 
		 * @Column(name="creation_date") private Timestamp creationDate;
		 * 
		 * @Column(name="login_id") private String loginId;
		 * 
		 * @Column(name="rejection_date") private Timestamp rejectionDate;
		 * 
		 * private String status;
		 * 
		 * @Column(name="uploaded_file") private byte[] uploadedFile;
		 * 
		 * public PortalGeographicalArea() {
		 * 
		 * 
		 * }
		 * 
		 * public Integer getGmlId() { return this.gmlId; }
		 * 
		 * public void setGmlId(Integer gmlId) { this.gmlId = gmlId; }
		 * 
		 * public String getAreaType() { return this.areaType; }
		 * 
		 * public void setAreaType(String areaType) { this.areaType = areaType; }
		 * 
		 * public Timestamp getCreationDate() { return this.creationDate; }
		 * 
		 * public void setCreationDate(Timestamp creationDate) { this.creationDate =
		 * creationDate; }
		 * 
		 * public String getLoginId() { return this.loginId; }
		 * 
		 * public void setLoginId(String loginId) { this.loginId = loginId; }
		 * 
		 * public Timestamp getRejectionDate() { return this.rejectionDate; }
		 * 
		 * public void setRejectionDate(Timestamp rejectionDate) { this.rejectionDate =
		 * rejectionDate; }
		 * 
		 * 
		 * public String getStatus() { return this.status; }
		 * 
		 * public void setStatus(String status) { this.status = status; }
		 * 
		 * 
		 * public byte[] getUploadedFile() { return this.uploadedFile;
		 * 
		 * 
		 * }
		 * 
		 * public void setUploadedFile(byte[] uploadedFile) { this.uploadedFile =
		 * uploadedFile; }
		 * 
		 * @OneToMany(mappedBy = "gmlId", cascade = CascadeType.ALL,fetch =
		 * FetchType.EAGER) List<PortalGmlidAreacodeMapping> areaCodes;
		 * 
		 * public List<PortalGmlidAreacodeMapping> getAreaCodes() { return areaCodes; }
		 * public void setAreaCodes(List<PortalGmlidAreacodeMapping> areaCodes) {
		 * this.areaCodes = areaCodes;
		 * 
		 * }
		 * 
		 * 
		 * 
		 * }
		 */