package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;

public class MappingNames implements Serializable{
	
	private String companyName;
	
	private String userName;
	
	private String csoName;
	
	private String acsoName;
	
	private String dpaName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCsoName() {
		return csoName;
	}

	public void setCsoName(String csoName) {
		this.csoName = csoName;
	}

	public String getAcsoName() {
		return acsoName;
	}

	public void setAcsoName(String acsoName) {
		this.acsoName = acsoName;
	}

	public String getDpaName() {
		return dpaName;
	}

	public void setDpaName(String dpaName) {
		this.dpaName = dpaName;
	}

	@Override
	public String toString() {
		return "MappingNames [companyName=" + companyName + ", userName=" + userName + ", csoName=" + csoName
				+ ", acsoName=" + acsoName + ", dpaName=" + dpaName + "]";
	}

}
