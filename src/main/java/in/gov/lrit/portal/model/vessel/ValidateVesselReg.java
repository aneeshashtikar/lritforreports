package in.gov.lrit.portal.model.vessel;

public class ValidateVesselReg {
	
	private static final String numPattern = "/^[0-9s]+$/";
	
	private static final String numAlphaPattern = "/^[0-9a-zA-Z \\-'_]+$/";
	
	private static final String alphaPattern = "/^[a-zA-Z]+$/";

}
