package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;



/**
 * The persistent class for the portal_sar_request database table.
 * 
 */
@Entity
@Table(name="portal_sar_request")
public class PortalSarRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_req",sequenceName="seq_portal_surpic_request", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_req")
	@Column(name="request_id")
	private Long requestId;
	
	@Column(name="access_type")
	private String accessType;

	@Column(name="end_time")
	private Date endTime;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "message_id", referencedColumnName = "message_id")
	private PortalRequest portalRequest;

	@Column(name="request_type")
	private int requestType;

	@Column(name="start_time")
	private Date startTime;

	@Column(name="vessel_imo")
	private String vesselImo;

	@Column(name="vessel_mmsi")
	private Integer mmsiNo;

	@Column(name="vessel_name")
	private String vesselName;
	
	@Transient
	public String userProvider;
	

	public String getUserProvider() {
		return userProvider;
	}

	public void setUserProvider(String userProvider) {
		this.userProvider = userProvider;
	}

	public PortalSarRequest() {
	}

	public String getAccessType() {
		return this.accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	
	public PortalRequest getPortalRequest() {
		return portalRequest;
	}

	public void setPortalRequest(PortalRequest portalRequest) {
		this.portalRequest = portalRequest;
	}

	public int getRequestType() {
		return this.requestType;
	}

	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getVesselImo() {
		return this.vesselImo;
	}

	public void setVesselImo(String vesselImo) {
		this.vesselImo = vesselImo;
	}

	
	public Integer getMmsiNo() {
		return mmsiNo;
	}

	public void setMmsiNo(Integer mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public String getVesselName() {
		return this.vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}

}