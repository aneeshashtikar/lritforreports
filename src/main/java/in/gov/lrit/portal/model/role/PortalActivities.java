package in.gov.lrit.portal.model.role;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

@Entity
@Table(name="portal_activities")
public class PortalActivities implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8510778492344134066L;

	@Id
	@Column(name="activity_id",nullable = false,length = 50)
	private String activityId;

	@Column(nullable = false,length = 50)
	private String activityname;
	
	@Column(nullable = false,length = 100)
	private String activityurl;
	
	@Column(nullable = false,length = 200)
	private String description;
	@Transient
	public boolean flag=false;
	
	/*@Column(nullable = false,length=50)
	private String type;*/

	public String getActivityId() {
		return activityId;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getActivityname() {
		return activityname;
	}

	public void setActivityname(String activityname) {
		this.activityname = activityname;
	}

	public String getActivityurl() {
		return activityurl;
	}

	public void setActivityurl(String activityurl) {
		this.activityurl = activityurl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PortalActivities [activityId=" + activityId + ", activityname=" + activityname + ", activityurl="
				+ activityurl + ", description=" + description + ", flag=" + flag + "]";
	}


	/*public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}*/

/*	@Override
	public String toString() {
		return "PortalActivities [activityId=" + activityId + ", activityname=" + activityname + ", activityurl="
				+ activityurl + ", description=" + description + ", type=" + type + "]";
	}*/
/*
	@Override
	public String toString() {
		return "PortalActivities [activityId=" + activityId + ", activityname=" + activityname + ", activityurl="
				+ activityurl + ", description=" + description + "]";
	}*/
	
	
	
}