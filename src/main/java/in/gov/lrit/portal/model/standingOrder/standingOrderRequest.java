package in.gov.lrit.portal.model.standingOrder;


import in.gov.lrit.portal.model.request.PortalRequest;
import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the portal_standingorders_request database table.
 * 
 */

@Entity
@Table(name="portal_standingorders_request")
public class standingOrderRequest implements Serializable {

private static final long serialVersionUID = 1L;

@Id
@SequenceGenerator(name="SO_Request_Seq", sequenceName = "standingorders_request_seq" , allocationSize=1)
@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SO_Request_Seq" )     
@Column(name="standingorder_id")
private int soId;

@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(name = "message_id", referencedColumnName = "message_id")
private PortalRequest portalRequest;

@Column(name="parentso_id")
private String parentsoid;

@Column(name="lrit_id")
private String lritId;

@Column(name="status")
private String status;


public int getStandingorderId() {
	return soId;
}

public void setStandingorderId(int standingorderid) {
	this.soId = standingorderid;
}

public String getParentsoid() {
	return parentsoid;
}

public void setParentsoid(String parentsoid) {
	this.parentsoid = parentsoid;
}

@Override
public String toString() {
	return "standingOrderDetail [standingorderid=" + soId + ", portalRequest=" + portalRequest + ", parentsoid=" + parentsoid
			+ ", lritId=" + lritId + "Status=" + status
			+ "]";
}

public PortalRequest getPortalRequest() {
	return portalRequest;// = new PortalRequest();
}

public void setPortalRequest(PortalRequest portalRequest) {
	this.portalRequest = portalRequest;
}

public String getLritId() {
	return lritId;
}

public void setLritId(String lritId) {
	this.lritId = lritId;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

}
