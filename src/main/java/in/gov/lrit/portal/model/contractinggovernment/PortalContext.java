package in.gov.lrit.portal.model.contractinggovernment;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the portal_context database table.
 * 
 */
@Entity
@Table(name="portal_context")
//@NamedQuery(name="PortalContext.findAll", query="SELECT p FROM PortalContext p")
public class PortalContext implements Serializable {
	@Override
	public String toString() {
		return "PortalContext [id=" + id + "]";
	}

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PortalContextPK id;

	public PortalContext() {
	}

	public PortalContextPK getId() {
		return this.id;
	}

	public void setId(PortalContextPK id) {
		this.id = id;
	}

}