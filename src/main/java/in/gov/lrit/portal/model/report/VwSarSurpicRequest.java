package in.gov.lrit.portal.model.report;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;



/**
 * The persistent class for the vw_sar_surpic_request database table.
 * 
 */
@Entity
@Table(name="vw_sar_surpic_request")
@NamedQuery(name="VwSarSurpicRequest.findAll", query="SELECT v FROM VwSarSurpicRequest v")
@IdClass(CompositeKeyForSarSurpicRequest.class)
public class VwSarSurpicRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name="destination", nullable=false)
	private String destination;

	@Column(name="message", nullable=false)
	private String message;

	@Id
	@Column(name="message_id", nullable=false)
	private String messageId;

	@Column(name="originator", nullable=false)
	private String originator;

	@Id
	@Column(name="receipt_message_id", nullable=false)
	private String receiptMessageId;

	@Column(name="req_timestmp", nullable=false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp reqTimestmp;

	@Column(name="requestor", nullable=false)
	private String requestor;

	@Column(name="res_timestamp", nullable=false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private String resTimestamp;

	@Column(name="sar_cir_area", nullable=false)
	private String sarCirArea;

	@Column(name="sar_rec_area", nullable=false)
	private String sarRecArea;

	@Column(name="status", nullable=false)
	private String status;

	public VwSarSurpicRequest() {
	}

	public String getDestination() {
		return this.destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getOriginator() {
		return this.originator;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public String getReceiptMessageId() {
		return this.receiptMessageId;
	}

	public void setReceiptMessageId(String receiptMessageId) {
		this.receiptMessageId = receiptMessageId;
	}

	public Timestamp getReqTimestmp() {
		return this.reqTimestmp;
	}

	public void setReqTimestmp(Timestamp reqTimestmp) {
		this.reqTimestmp = reqTimestmp;
	}

	public String getRequestor() {
		return this.requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getResTimestamp() {
		return this.resTimestamp;
	}

	public void setResTimestamp(String resTimestamp) {
		this.resTimestamp = resTimestamp;
	}

	public String getSarCirArea() {
		return this.sarCirArea;
	}

	public void setSarCirArea(String sarCirArea) {
		this.sarCirArea = sarCirArea;
	}

	public String getSarRecArea() {
		return this.sarRecArea;
	}

	public void setSarRecArea(String sarRecArea) {
		this.sarRecArea = sarRecArea;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "VwSarSurpicRequest [destination=" + destination + ", message=" + message + ", messageId=" + messageId
				+ ", originator=" + originator + ", receiptMessageId=" + receiptMessageId + ", reqTimestmp="
				+ reqTimestmp + ", requestor=" + requestor + ", resTimestamp=" + resTimestamp + ", sarCirArea="
				+ sarCirArea + ", sarRecArea=" + sarRecArea + ", status=" + status + "]";
	}
	

}