package in.gov.lrit.portal.model.common;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the asp_terminal_frequency database table.
 * 
 */
@Entity
@Table(name="asp_terminal_frequency")
@NamedQuery(name="AspTerminalFrequency.findAll", query="SELECT a FROM AspTerminalFrequency a")
public class AspTerminalFrequency implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="message_id")
	private String messageId;

	@Column(name="cg_lrit_id")
	private String cgLritId;

	@Column(name="dnid_no")
	private Integer dnidNo;

	@Column(name="frequency_rate")
	private Integer frequencyRate;

	@Column(name="imo_no")
	private Integer imoNo;

	@Column(name="member_no")
	private Integer memberNo;

	private Timestamp starttime;

	private String status;

	public AspTerminalFrequency() {
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getCgLritId() {
		return this.cgLritId;
	}

	public void setCgLritId(String cgLritId) {
		this.cgLritId = cgLritId;
	}

	public Integer getDnidNo() {
		return this.dnidNo;
	}

	public void setDnidNo(Integer dnidNo) {
		this.dnidNo = dnidNo;
	}

	public Integer getFrequencyRate() {
		return this.frequencyRate;
	}

	public void setFrequencyRate(Integer frequencyRate) {
		this.frequencyRate = frequencyRate;
	}

	public Integer getImoNo() {
		return this.imoNo;
	}

	public void setImoNo(Integer imoNo) {
		this.imoNo = imoNo;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}