package in.gov.lrit.portal.model.vessel;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * The persistent class for the portal_relation database table.
 * 
 */

@Entity
@Table(name="portal_relation")
@NamedQuery(name="PortalRelation.findAll", query="SELECT p FROM PortalRelation p")
public class PortalRelation implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment") 
	@Column(name="relation_id", nullable=false)
	private Integer relationId;
	
	@Column(name="description")
	private String description;

	public Integer getRelationId() {
		return relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PortalRelation [relationId=" + relationId + ", description=" + description + "]";
	}
	
}