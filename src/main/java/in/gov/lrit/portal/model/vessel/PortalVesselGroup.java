package in.gov.lrit.portal.model.vessel;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the portal_vessel_group database table.
 * 
 */
@Entity
@Table(name="portal_vessel_group")
//@NamedQuery(name="PortalVesselGroup.findAll", query="SELECT p FROM PortalVesselGroup p")
public class PortalVesselGroup implements Serializable {
private static final long serialVersionUID = 1L;

@Column(name="group_name")
private String groupName;

@Column(name="description")
private String description;

@Column(name="short_name")
private String shortName;

@Id
@GeneratedValue(generator="increment")
@GenericGenerator(name="increment", strategy = "increment") 
@Column(name="group_id", nullable=false)
private Long groupId;

public PortalVesselGroup() {
}

public String getGroupName() {
return this.groupName;
}

public void setGroupName(String groupName) {
this.groupName = groupName;
}

public String getDescription() {
return this.description;
}

public void setDescription(String description) {
this.description = description;
}

public String getShortName() {
return this.shortName;
}

public void setShortName(String shortName) {
this.shortName = shortName;
}

public Long getGroupId() {
return this.groupId;
}

public void setGroupId(Long groupId) {
this.groupId = groupId;
}

@OneToMany
private Set<PortalVesselDetail> vessels;

@Override
public String toString() {
	return "PortalVesselGroup [groupName=" + groupName + ", description=" + description + ", shortName=" + shortName
			+ ", groupId=" + groupId + "]";
}

}
