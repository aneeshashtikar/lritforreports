package in.gov.lrit.portal.model.alert;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name="portal_alert_txn")
public class AlertDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="alert__txn_id")
	private int alertTxnId;
	
	@Column(name="alert_id")
	private int alertId;
	
	@Column(name="message")
	private String message;
	
	@Column(name="created_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp createdDate;
	
	@Column(name="context_id")
	private String contextId;
	
	@Column(name="status")
	private String status;
	
	@Column(name="created_by")
	private String createdBy;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="alert_id",insertable=false, updatable=false)
	private AlertMasterDetails alertDetails;
	
	@Column(name = "login_id")
	private String loginID;
	
	@Transient
	  public boolean flag = false;

	public int getAlertTxnId() {
		return alertTxnId;
	}

	public void setAlertTxnId(int alertTxnId) {
		this.alertTxnId = alertTxnId;
	}

	public int getAlertId() {
		return alertId;
	}

	public void setAlertId(int alertId) {
		this.alertId = alertId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public AlertMasterDetails getAlertDetails() {
		return alertDetails;
	}

	public void setAlertDetails(AlertMasterDetails alertDetails) {
		this.alertDetails = alertDetails;
	}

	public String getLoginID() {
		return loginID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "AlertDetails [alertTxnId=" + alertTxnId + ", alertId=" + alertId + ", message=" + message
				+ ", createdDate=" + createdDate + ", contextId=" + contextId + ", status=" + status + ", createdBy="
				+ createdBy + ", alertDetails=" + alertDetails + ", loginID=" + loginID + ", flag=" + flag + "]";
	}

}
