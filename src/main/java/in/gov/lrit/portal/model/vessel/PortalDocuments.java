package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.sql.Array;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the portal_documents database table.
 * 
 */
@Entity
@Table(name="portal_documents")
@NamedQuery(name="PortalDocuments.findAll", query="SELECT p FROM PortalDocuments p")
public class PortalDocuments implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue(generator="documentIdSeq", strategy=GenerationType.SEQUENCE)
@SequenceGenerator(name="documentIdSeq", sequenceName="portal_documents_seq", allocationSize=1) 
@Column(name="id", nullable=false)
private Integer documentId;

@Column(name="ref_id")
private String referenceId; 

@Column(name="table_name")
private String tableName;

@Column(name="doc")
private byte[] document;

@Column(name="doc_purpose")
private String docPurpose;

public Integer getDocumentId() {
return documentId;
}

public void setDocumentId(Integer documentId) {
this.documentId = documentId;
}

public String getReferenceId() {
return referenceId;
}

public void setReferenceId(String referenceId) {
this.referenceId = referenceId;
}

public String getTableName() {
return tableName;
}

public void setTableName(String tableName) {
this.tableName = tableName;
}

public byte[] getDocument() {
return document;
}

public void setDocument(byte[] document) {
this.document = document;
}

public String getDocPurpose() {
return docPurpose;
}

public void setDocPurpose(String docPurpose) {
this.docPurpose = docPurpose;
}

@Override
public String toString() {
return "PortalDocuments [documentId=" + documentId + ", referenceId=" + referenceId + ", tableName=" + tableName
+ ", document=" + Arrays.toString(document) + ", docPurpose=" + docPurpose + "]";
}

}