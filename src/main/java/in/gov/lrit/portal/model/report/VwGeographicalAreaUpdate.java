package in.gov.lrit.portal.model.report;

import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "vw_geographical_area_update")
public class VwGeographicalAreaUpdate {
	
	@Id
	@Column(name = "request_message_id")
	private String requestMessageId;
	
	@Column(name = "area_id")
	private String areaId;
	
	@Column(name = "ddp_version")
	private String ddpVersion;
	
	@Column(name = "request_timestamp")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp requestTimestamp;
	
	@Column(name = "response_message_id")
	private String responseMessageId;
	
	@Column(name = "message_response")
	private String messageResponse;
	
	@Column(name = "action_type")
	private BigInteger actionType;
	
	@Column(name = "data_user_requestor")
	private String dataUserRequestor;

	public String getRequestMessageId() {
		return requestMessageId;
	}

	public void setRequestMessageId(String requestMessageId) {
		this.requestMessageId = requestMessageId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getDdpVersion() {
		return ddpVersion;
	}

	public void setDdpVersion(String ddpVersion) {
		this.ddpVersion = ddpVersion;
	}

	public Timestamp getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(Timestamp requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public String getResponseMessageId() {
		return responseMessageId;
	}

	public void setResponseMessageId(String responseMessageId) {
		this.responseMessageId = responseMessageId;
	}

	public String getMessageResponse() {
		return messageResponse;
	}

	public void setMessageResponse(String messageResponse) {
		this.messageResponse = messageResponse;
	}

	public BigInteger getActionType() {
		return actionType;
	}

	public void setActionType(BigInteger actionType) {
		this.actionType = actionType;
	}

	public String getDataUserRequestor() {
		return dataUserRequestor;
	}

	public void setDataUserRequestor(String dataUserRequestor) {
		this.dataUserRequestor = dataUserRequestor;
	}

	@Override
	public String toString() {
		return "VwGeographicalAreaUpdate [requestMessageId=" + requestMessageId + ", areaId=" + areaId + ", ddpVersion="
				+ ddpVersion + ", requestTimestamp=" + requestTimestamp + ", responseMessageId=" + responseMessageId
				+ ", messageResponse=" + messageResponse + ", actionType=" + actionType + ", dataUserRequestor="
				+ dataUserRequestor + "]";
	}
	
	

}
