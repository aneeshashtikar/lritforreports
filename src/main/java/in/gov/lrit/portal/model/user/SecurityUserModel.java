package in.gov.lrit.portal.model.user;


import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


public class SecurityUserModel implements UserDetails {

	public SecurityUserModel() {
		super();
	}

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private String username = null;
	private String password = null;
	/* Spring Security fields */
	private List<GrantedAuthority> grantedAuthorities  = null;
	private boolean accountNonExpired = true;
	private boolean accountNonLocked = true;
	private boolean credentialsNonExpired = true;
	private boolean enabled = false;
	
	
	public SecurityUserModel(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
			List<GrantedAuthority> authorities) {
		
		this.username=username;
		this.password=password;
		this.enabled=enabled;
		this.accountNonExpired=accountNonExpired;
		this.credentialsNonExpired=credentialsNonExpired;
		this.accountNonLocked=accountNonLocked;
		this.grantedAuthorities=authorities;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return this.grantedAuthorities;
	}

	@Override
	public String getPassword() { // TODO Auto-generated method stub
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getUsername() { // TODO Auto-generated method stub
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override public boolean isAccountNonExpired() { // TODO Auto-generated method stub
		   return accountNonExpired; }

	@Override public boolean isAccountNonLocked() { // TODO Auto-generated method stub
		   return accountNonLocked; }

	@Override public boolean isCredentialsNonExpired() { // TODO Auto-generated method stub 
		return credentialsNonExpired; }
	

	@Override
	public boolean isEnabled() { // TODO Auto-generated method stub
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "SecurityUserModel [username=" + username + ", password=" + password + ", authorities=" + grantedAuthorities
				+ ", accountNonExpired=" + accountNonExpired + ", accountNonLocked=" + accountNonLocked
				+ ", credentialsNonExpired=" + credentialsNonExpired + ", enabled=" + enabled + "]";
	}

}
