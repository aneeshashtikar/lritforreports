package in.gov.lrit.portal.model.user;

import java.io.Serializable;

public interface PortalLritIdMasterInterface extends Serializable{
	public String getCountryName();
}
