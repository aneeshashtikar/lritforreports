package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the portal_manufacturer_details database table.
 * 
 */
@Entity
@Table(name="portal_model_details")
@NamedQuery(name="PortalModelDetail.findAll", query="SELECT p FROM PortalModelDetail p")
public class PortalModelDetail implements Serializable {
private static final long serialVersionUID = 1L;

@Id 
@GeneratedValue(generator="modelIdSeq", strategy=GenerationType.SEQUENCE)
@SequenceGenerator(name="modelIdSeq", sequenceName="model_id_seq", allocationSize=1) 
@Column(name="model_id", nullable=false)
private Integer modelId;

@ManyToOne
@JoinColumn(name="manufacturer_id", nullable=false)
private PortalManufacturerDetails manuDet;

@Column(name="model_type")
private String modelType;

@Column(name="model_name")
private String modelName;

@Column(name="status")
private String status;

@CreationTimestamp
@Column(name="reg_date")
private Timestamp regDate;

@Column(name="de_reg_date")
private Timestamp deRegDate;

public PortalModelDetail() {
}

public Integer getModelId() {
	return modelId;
}

public void setModelId(Integer modelId) {
	this.modelId = modelId;
}

public PortalManufacturerDetails getManuDet() {
	return manuDet;
}

public void setManuDet(PortalManufacturerDetails manuDet) {
	this.manuDet = manuDet;
}

public String getModelType() {
	return modelType;
}

public void setModelType(String modelType) {
	this.modelType = modelType;
}

public String getModelName() {
	return modelName;
}

public void setModelName(String modelName) {
	this.modelName = modelName;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public Timestamp getRegDate() {
	return regDate;
}

public void setRegDate(Timestamp regDate) {
	this.regDate = regDate;
}

public Timestamp getDeRegDate() {
	return deRegDate;
}

public void setDeRegDate(Timestamp deRegDate) {
	this.deRegDate = deRegDate;
}

@Override
public String toString() {
	return "PortalModelDetail [modelId=" + modelId + ", manuDet=" + manuDet + ", modelType=" + modelType
			+ ", modelName=" + modelName + ", status=" + status + ", regDate=" + regDate + ", deRegDate=" + deRegDate
			+ "]";
}

}