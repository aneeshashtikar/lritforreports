package in.gov.lrit.portal.model.pendingtask;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the portal_pending_task_mst database table.
 * 
 */
@Entity
@Table(name="portal_pending_task_mst")
@NamedQuery(name="PortalPendingTaskMst.findAll", query="SELECT p FROM PortalPendingTaskMst p")
public class PortalPendingTaskMst implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_pendingtask", sequenceName="pendingtask_id", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_pendingtask")

	@Column(name="pendingtask_id")
	private Integer pendingtaskId;
	
	@Column(name="action_date")
	private Timestamp actionDate;

	@Column(name="approval_login_id")
	private String approvalLoginId;

	private String category;

	private String message;

	@Column(name="reference_id")
	private String referenceId;

	@Column(name="request_date")
	private Timestamp requestDate;

	@Column(name="requester_login_id")
	private String requesterLoginId;

	@Column(name="requestor_lritid")
	private String requestorLritid;

	private String status;
	
	private String remark;

	public PortalPendingTaskMst() {
	}

	public Integer getPendingtaskId() {
		return this.pendingtaskId;
	}

	public void setPendingtaskId(Integer pendingtaskId) {
		this.pendingtaskId = pendingtaskId;
	}

	public Timestamp getActionDate() {
		return this.actionDate;
	}

	public void setActionDate(Timestamp actionDate) {
		this.actionDate = actionDate;
	}

	public String getApprovalLoginId() {
		return this.approvalLoginId;
	}

	public void setApprovalLoginId(String approvalLoginId) {
		this.approvalLoginId = approvalLoginId;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public Timestamp getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Timestamp requestDate) {
		this.requestDate = requestDate;
	}

	public String getRequesterLoginId() {
		return this.requesterLoginId;
	}

	public void setRequesterLoginId(String requesterLoginId) {
		this.requesterLoginId = requesterLoginId;
	}

	public String getRequestorLritid() {
		return this.requestorLritid;
	}

	public void setRequestorLritid(String requestorLritid) {
		this.requestorLritid = requestorLritid;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}