package in.gov.lrit.portal.model.logging;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the portal_login_audit database table.
 * 
 */
@Entity
@Table(name="portal_login_audit")
@NamedQuery(name="PortalLoginAudit.findAll", query="SELECT p FROM PortalLoginAudit p")
public class PortalLoginAudit implements Serializable {
	@Override
	public String toString() {
		return "PortalLoginAudit [id=" + id + ", geolocation=" + geolocation + ", hostIp=" + hostIp + ", hostName="
				+ hostName + ", loginDateTime=" + loginDateTime + ", loginId=" + loginId + ", loginStatus="
				+ loginStatus + ", logoutTime=" + logoutTime + "]";
	}

	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "loginIdSequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "loginIdSequence", sequenceName = "seq_portal_login", allocationSize=1)
	private Integer id;

	private String geolocation;

	@Column(name="host_ip")
	private String hostIp;

	@Column(name="host_name")
	private String hostName;

	@Column(name="login_date_time")
	private Timestamp loginDateTime;

	@Column(name="login_id")
	private String loginId;

	@Column(name="login_status")
	private String loginStatus;

	@Column(name="logout_time")
	private Timestamp logoutTime;

	public PortalLoginAudit() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGeolocation() {
		return this.geolocation;
	}

	public void setGeolocation(String geolocation) {
		this.geolocation = geolocation;
	}

	public String getHostIp() {
		return this.hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

	public String getHostName() {
		return this.hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Timestamp getLoginDateTime() {
		return this.loginDateTime;
	}

	public void setLoginDateTime(Timestamp loginDateTime) {
		this.loginDateTime = loginDateTime;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getLoginStatus() {
		return this.loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	public Timestamp getLogoutTime() {
		return this.logoutTime;
	}

	public void setLogoutTime(Timestamp logoutTime) {
		this.logoutTime = logoutTime;
	}

}