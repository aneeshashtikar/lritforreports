package in.gov.lrit.portal.model.user;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.role.PortalRoles;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Set;
import java.sql.Array;
import in.gov.lrit.portal.model.user.PortalShippingCompany;

/**
 * The persistent class for the portal_users database table.
 * 
 */
@Entity
@Table(name = "portal_users") 
//@Table(name = "portal_users") portal_users1
//@NamedQuery(name="PortalUser.findAll", query="SELECT p FROM PortalUser p")
public class PortalUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "login_id")
	private String loginId;

	private String address1;

	private String address2;

	private String address3;

	private String category;

	private String city;

	private String country;

	@Column(name = "deregister_date")
	private Timestamp deregisterDate;

	private String district;

	private String emailid;

	private String fax;

	private String landmark;

	private String mobileno;

	private String name;

	@Column(name = "no_of_password_attempt")
	private Integer noOfPasswordAttempt;

	private String password;

	private String pincode;

	@Column(name = "register_date")
	private Timestamp registerDate;

	/*
	 * @Column(name="requestors_lrit_id") private String requestorsLritId;
	 */

	@Column(name = "sar_area_view")
	private Boolean sarAreaView;

	private String state;

	private String status;

	private String telephone;

	@Column(name = "update_date")
	private Timestamp updateDate;

	@Column(name = "updated_by")
	private String updatedBy;

	
	@Column(name = "uploaded_file_path")
	private byte[] uploadedFilePath;

	@Column(name = "validate_token")
	private String validateToken;

	private String website;
	
	
	private Timestamp tokenvalidupto;
	
	
	

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "portal_role_user_mapping", joinColumns = {
			@JoinColumn(name = "login_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id") })
	private Set<PortalRoles> roles;

	@ManyToOne
	@JoinColumn(name = "requestors_lrit_id", nullable = false)
	private PortalLritIdMaster requestorsLritId;
	@Transient
	private String confirmpassword;
	//@Transient
	@Column(name="alternateemailid")
	private String alternateemailid;

	public String getAlternateemailid() {
		return alternateemailid;
	}



	public void setAlternateemailid(String alternateemailid) {
		this.alternateemailid = alternateemailid;
	}



	public PortalUser() {
	}
	
	//Changed on 1/10/19 
	//Reason: added company code as foreignkey in users
	
	
	@Column(name="relation")
	private String   relation;
	
	
	@ManyToOne
	@NotFound(
		    action = NotFoundAction.IGNORE)
	@JoinColumn(name = "company_code", nullable = false)
	private PortalShippingCompany portalShippingCompany;
	

	

	public PortalShippingCompany getPortalShippingCompany() {
		return portalShippingCompany;
	}



	public void setPortalShippingCompany(PortalShippingCompany portalShippingCompany2) {
		this.portalShippingCompany = portalShippingCompany2;
	}
	

	public String getRelation() {
		return relation;
	}



	public void setRelation(String relation) {
		this.relation = relation;
	}

	
	
	
	
	
	
	
	
	
	//.... end of change 




	public Timestamp getTokenvalidupto() {
		return tokenvalidupto;
	}



	public void setTokenvalidupto(Timestamp tokenvalidupto) {
		this.tokenvalidupto = tokenvalidupto;
	}



	






	public String getConfirmpassword() {
		return confirmpassword;
	}



	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}



	public String getLoginId() {
		return this.loginId;
	}

	public Set<PortalRoles> getRoles() {
		return roles;
	}

	public void setRoles(Set<PortalRoles> roles) {
		this.roles = roles;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Timestamp getDeregisterDate() {
		return this.deregisterDate;
	}

	public void setDeregisterDate(Timestamp deregisterDate) {
		this.deregisterDate = deregisterDate;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getEmailid() {
		return this.emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getLandmark() {
		return this.landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getMobileno() {
		return this.mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNoOfPasswordAttempt() {
		return this.noOfPasswordAttempt;
	}

	public void setNoOfPasswordAttempt(Integer noOfPasswordAttempt) {
		this.noOfPasswordAttempt = noOfPasswordAttempt;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Timestamp getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}

	public PortalLritIdMaster getRequestorsLritId() {
		return this.requestorsLritId;
	}

	public void setRequestorsLritId(PortalLritIdMaster requestorsLritId) {
		this.requestorsLritId = requestorsLritId;
	}

	public Boolean getSarAreaView() {
		return this.sarAreaView;
	}

	public void setSarAreaView(Boolean sarAreaView) {
		this.sarAreaView = sarAreaView;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Timestamp getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public byte[] getUploadedFilePath() {
		return this.uploadedFilePath;
	}

	public void setUploadedFilePath(byte[] uploadedFilePath) {
		this.uploadedFilePath = uploadedFilePath;
	}

	public String getValidateToken() {
		return this.validateToken;
	}

	public void setValidateToken(String validateToken) {
		this.validateToken = validateToken;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}



	@Override
	public String toString() {
		return "PortalUser [loginId=" + loginId + ", address1=" + address1 + ", address2=" + address2 + ", address3="
				+ address3 + ", category=" + category + ", city=" + city + ", country=" + country + ", deregisterDate="
				+ deregisterDate + ", district=" + district + ", emailid=" + emailid + ", fax=" + fax + ", landmark="
				+ landmark + ", mobileno=" + mobileno + ", name=" + name + ", noOfPasswordAttempt="
				+ noOfPasswordAttempt + ", password=" + password + ", pincode=" + pincode + ", registerDate="
				+ registerDate + ", sarAreaView=" + sarAreaView + ", state=" + state + ", status=" + status
				+ ", telephone=" + telephone + ", updateDate=" + updateDate + ", updatedBy=" + updatedBy
				+ ", validateToken=" + validateToken
				+ ", website=" + website + ", tokenvalidupto=" + tokenvalidupto + ", roles=" + roles
				+ ", requestorsLritId=" + requestorsLritId + ", confirmpassword=" + confirmpassword
				+ ", alternateemailid=" + alternateemailid + ", relation=" + relation + ", portalShippingCompany="
				+ portalShippingCompany + "]";
	}



/*	@Override
	public String toString() {
		return "PortalUser [loginId=" + loginId + ", address1=" + address1 + ", address2=" + address2 + ", address3="
				+ address3 + ", category=" + category + ", city=" + city + ", country=" + country + ", deregisterDate="
				+ deregisterDate + ", district=" + district + ", emailid=" + emailid + ", fax=" + fax + ", landmark="
				+ landmark + ", mobileno=" + mobileno + ", name=" + name + ", noOfPasswordAttempt="
				+ noOfPasswordAttempt + ", password=" + password + ", pincode=" + pincode + ", registerDate="
				+ registerDate + ", sarAreaView=" + sarAreaView + ", state=" + state + ", status=" + status
				+ ", telephone=" + telephone + ", updateDate=" + updateDate + ", updatedBy=" + updatedBy
				 + ", validateToken=" + validateToken +", relation="+relation
				+ ", website=" + website + ", tokenvalidupto=" + tokenvalidupto + ", roles=" + roles
				+ ", requestorsLritId=" + requestorsLritId + ", confirmpassword=" + confirmpassword
				+ ", portalShippingCompany=" + portalShippingCompany + "]";
	}
*/

/*	", uploadedFilePath=" + Arrays.toString(uploadedFilePath) + 
 * 
 * 
 * @Override
	public String toString() {
		return "PortalUser [loginId=" + loginId + ", address1=" + address1 + ", address2=" + address2 + ", address3="
				+ address3 + ", category=" + category + ", city=" + city + ", country=" + country + ", deregisterDate="
				+ deregisterDate + ", district=" + district + ", emailid=" + emailid + ", fax=" + fax + ", landmark="
				+ landmark + ", mobileno=" + mobileno + ", name=" + name + ", noOfPasswordAttempt="
				+ noOfPasswordAttempt + ", password=" + password + ", pincode=" + pincode + ", registerDate="
				+ registerDate + ", requestorsLritId=" + requestorsLritId + ", sarAreaView=" + sarAreaView + ", state="
				+ state + ", status=" + status + ", telephone=" + telephone + ", updateDate=" + updateDate
				+ ", updatedBy=" + updatedBy + ", uploadedFilePath=" + Arrays.toString(uploadedFilePath)
				+ ", validateToken=" + validateToken + ", website=" + website + ", roles=" + roles + "]";
	}
*/
	
	
	
	
}