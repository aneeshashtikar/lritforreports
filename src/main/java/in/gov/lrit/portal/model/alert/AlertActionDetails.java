package in.gov.lrit.portal.model.alert;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "portal_alert_action")
public class AlertActionDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "alert__txn_id")
	private BigInteger alertTxnId;
	
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "timestamp")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp closedTimestamp;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "user_category")
	private String userCategory;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "alert__txn_id", insertable=false, updatable=false)
	private AlertDetails alertDetails;
		
	public BigInteger getAlertTxnId() {
		return alertTxnId;
	}

	public void setAlertTxnId(BigInteger alertTxnId) {
		this.alertTxnId = alertTxnId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getClosedTimestamp() {
		return closedTimestamp;
	}

	public void setClosedTimestamp(Timestamp closedTimestamp) {
		this.closedTimestamp = closedTimestamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getUserCategory() {
		return userCategory;
	}

	public void setUserCategory(String userCategory) {
		this.userCategory = userCategory;
	}

	public AlertDetails getAlertDetails() {
		return alertDetails;
	}

	public void setAlertDetails(AlertDetails alertDetails) {
		this.alertDetails = alertDetails;
	}

	@Override
	public String toString() {
		return "AlertActionDetails [alertTxnId=" + alertTxnId + ", userId=" + userId + ", closedTimestamp="
				+ closedTimestamp + ", status=" + status + ", remark=" + remark + ", userCategory=" + userCategory
				+ ", alertDetails=" + alertDetails + "]";
	}

	 
}
