package in.gov.lrit.portal.model.user;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;


/**
 * The persistent class for the portal_cso_dpa_details_transit database table.
 * 
 */
@Entity
@Table(name="portal_cso_dpa_details_transit")
//@NamedQuery(name="PortalCsoDpaDetailsTransit.findAll", query="SELECT p FROM PortalCsoDpaDetailsTransit p")
public class PortalCsoDpaDetailsTransit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "csoDpaTransitId", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "csoDpaTransitId", sequenceName = "cso_dpa_transit_id", allocationSize=1)
	private Long id;

	@Column(name="company_code")
	private String companyCode;

	@Column(name="deregister_date")
	private Timestamp deregisterDate;

	private String emailid;

	@Column(name="login_id")
	private String loginId;

	@Column(name="lrit_csoid_seq")
	private String lritCsoidSeq;

	@Column(name="mobile_no")
	private String mobileNo;

	private String name;

	@Column(name="parent_csoid")
	private String parentCsoid;

	@Column(name="register_date")
	private Timestamp registerDate;

	@Column(name="telephone_office")
	private String telephoneOffice;

	@Column(name="telephone_residence")
	private String telephoneResidence;

	private String type;

	public PortalCsoDpaDetailsTransit() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyCode() {
		return this.companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Timestamp getDeregisterDate() {
		return this.deregisterDate;
	}

	public void setDeregisterDate(Timestamp timestamp) {
		this.deregisterDate = timestamp;
	}

	public String getEmailid() {
		return this.emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getLritCsoidSeq() {
		return this.lritCsoidSeq;
	}

	public void setLritCsoidSeq(String lritCsoidSeq) {
		this.lritCsoidSeq = lritCsoidSeq;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentCsoid() {
		return this.parentCsoid;
	}

	public void setParentCsoid(String parentCsoid) {
		this.parentCsoid = parentCsoid;
	}

	public Timestamp getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}

	public String getTelephoneOffice() {
		return this.telephoneOffice;
	}

	public void setTelephoneOffice(String telephoneOffice) {
		this.telephoneOffice = telephoneOffice;
	}

	public String getTelephoneResidence() {
		return this.telephoneResidence;
	}

	public void setTelephoneResidence(String telephoneResidence) {
		this.telephoneResidence = telephoneResidence;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}