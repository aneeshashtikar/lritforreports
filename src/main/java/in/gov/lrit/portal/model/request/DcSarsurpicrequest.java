package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the dc_sarsurpicrequest database table.
 * 
 */
@Entity
@Table(name="dc_sarsurpicrequest")
@NamedQuery(name="DcSarsurpicrequest.findAll", query="SELECT d FROM DcSarsurpicrequest d")
public class DcSarsurpicrequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="message_id")
	private String messageId;

	@Column(name="access_type")
	private Integer accessType;

	@Column(name="circular_area")
	private String circularArea;

	@Column(name="data_user_requestor")
	private String dataUserRequestor;

	@Column(name="ddpversion_no")
	private String ddpversionNo;

	@Column(name="lrit_timestamp")
	private Timestamp lritTimestamp;

	@Column(name="message_type")
	private Integer messageType;

	@Column(name="no_of_positions")
	private Integer noOfPositions;

	@Column(name="rectangular_area")
	private String rectangularArea;

	@Column(name="sarsurpicrequest_timestamp")
	private Timestamp sarsurpicrequestTimestamp;

	@Column(name="schema_version")
	private BigDecimal schemaVersion;

	@Column(name="ship_type")
	private String shipType;

	private Integer test;

	public DcSarsurpicrequest() {
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Integer getAccessType() {
		return this.accessType;
	}

	public void setAccessType(Integer accessType) {
		this.accessType = accessType;
	}

	public String getCircularArea() {
		return this.circularArea;
	}

	public void setCircularArea(String circularArea) {
		this.circularArea = circularArea;
	}

	public String getDataUserRequestor() {
		return this.dataUserRequestor;
	}

	public void setDataUserRequestor(String dataUserRequestor) {
		this.dataUserRequestor = dataUserRequestor;
	}

	public String getDdpversionNo() {
		return this.ddpversionNo;
	}

	public void setDdpversionNo(String ddpversionNo) {
		this.ddpversionNo = ddpversionNo;
	}

	public Timestamp getLritTimestamp() {
		return this.lritTimestamp;
	}

	public void setLritTimestamp(Timestamp lritTimestamp) {
		this.lritTimestamp = lritTimestamp;
	}

	public Integer getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public Integer getNoOfPositions() {
		return this.noOfPositions;
	}

	public void setNoOfPositions(Integer noOfPositions) {
		this.noOfPositions = noOfPositions;
	}

	public String getRectangularArea() {
		return this.rectangularArea;
	}

	public void setRectangularArea(String rectangularArea) {
		this.rectangularArea = rectangularArea;
	}

	public Timestamp getSarsurpicrequestTimestamp() {
		return this.sarsurpicrequestTimestamp;
	}

	public void setSarsurpicrequestTimestamp(Timestamp sarsurpicrequestTimestamp) {
		this.sarsurpicrequestTimestamp = sarsurpicrequestTimestamp;
	}

	public BigDecimal getSchemaVersion() {
		return this.schemaVersion;
	}

	public void setSchemaVersion(BigDecimal schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	public String getShipType() {
		return this.shipType;
	}

	public void setShipType(String shipType) {
		this.shipType = shipType;
	}

	public Integer getTest() {
		return this.test;
	}

	public void setTest(Integer test) {
		this.test = test;
	}

}