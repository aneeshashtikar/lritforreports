package in.gov.lrit.portal.model.standingOrder;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the portal_standingorders_vessel_exclusion database table.
 * 
 */

@Entity
@Table(name="portal_standingorders_vessel_exclusion")
public class SOVesselExclusion implements Serializable {

private static final long serialVersionUID = 1L;

@Id
@SequenceGenerator(name="SO_Request_Seq", sequenceName = "portal_standingorders_vessel_exclusion_seq" , allocationSize=1)
@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SO_Request_Seq" )     
@Column(name="id")
private int id;

@Column(name = "standingorder_id")
private int soId;

@Column(name="vessel_category_id")
private String vesselCode;

@Column(name="excluded_vessel_type")
private String vesselType;

@Column(name = "area_code")
private String areacode;

@Override
public String toString() {
	return vesselType + " " + vesselCode;
}

public String getVesselCode() {
	return vesselCode;
}

public void setVesselCode(String vesselCode) {
	this.vesselCode = vesselCode;
}

public String getVesselType() {
	return vesselType;
}

public void setVesselType(String vesselType) {
	this.vesselType = vesselType;
}

public int getSOId() {
	return soId;
}

public void setSOId(int sOId) {
	soId = sOId;
}

public String getAreacode() {
	return areacode;
}

public void setAreacode(String areacode) {
	this.areacode = areacode;
}



}
