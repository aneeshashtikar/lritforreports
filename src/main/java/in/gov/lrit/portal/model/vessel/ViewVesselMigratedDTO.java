package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.util.List;

//added on 11/12/2019
public class ViewVesselMigratedDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/*private DocumentForm docs;*/
	
	//updated on 13/1/2020
	private List<PortalDocuments> docs;
	
	private PortalVesselDetail vesselDet;
		
	private List<MappingNames> nameslist;

	/*public DocumentForm getDocs() {
		return docs;
	}

	public void setDocs(DocumentForm docs) {
		this.docs = docs;
	}*/
	
	public List<PortalDocuments> getDocs() {
		return docs;
	}

	public void setDocs(List<PortalDocuments> docs) {
		this.docs = docs;
	}

	public PortalVesselDetail getVesselDet() {
		return vesselDet;
	}

	public void setVesselDet(PortalVesselDetail vesselDet) {
		this.vesselDet = vesselDet;
	}

	public List<MappingNames> getNameslist() {
		return nameslist;
	}

	public void setNameslist(List<MappingNames> nameslist) {
		this.nameslist = nameslist;
	}

	@Override
	public String toString() {
		return "ViewVesselDTO [docs=" + docs + ", vesselDet=" + vesselDet + ", nameslist=" + nameslist + "]";
	}

}
