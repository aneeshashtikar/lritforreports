package in.gov.lrit.portal.model.vessel;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;


/**
 * The persistent class for the portal_imo_vessel_category database table.
 * 
 */
@Entity
@Table(name="portal_imo_vessel_category")
@NamedQuery(name="PortalImoVesselCategory.findAll", query="SELECT p FROM PortalImoVesselCategory p")
public class PortalImoVesselCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="vessel_category_id")
	private String vesselCategoryId;

	@Column(name="category_name")
	private String categoryName;

	public PortalImoVesselCategory() {
	}

	public String getVesselCategoryId() {
		return this.vesselCategoryId;
	}

	public void setVesselCategoryId(String vesselCategoryId) {
		this.vesselCategoryId = vesselCategoryId;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Override
	public String toString() {
		return "PortalImoVesselCategory [vesselCategoryId=" + vesselCategoryId + ", categoryName=" + categoryName + "]";
	}
	
}
