package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.stereotype.Component;



/**
 * The persistent class for the portal_requests database table.
 * 
 */
@Component
@Entity
@Table(name="config_para")
public class Configpara implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@Column(name="paraname")
	private String paramname;
	
	@Column(name="paravalue")
	private String paramvalue;

	public String getParamname() {
		return paramname;
	}

	public void setParamname(String paramname) {
		this.paramname = paramname;
	}

	public String getParamvalue() {
		return paramvalue;
	}

	public void setParamvalue(String paramvalue) {
		this.paramvalue = paramvalue;
	}





}