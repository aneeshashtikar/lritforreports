package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.util.Arrays;

public class DocumentForm implements Serializable {

private byte[] emailDoc;

private byte[] tataLritDoc;

private byte[] flagRegDoc;

private byte[] conformanceTestCertDoc;

private byte[] additionalDoc;

public byte[] getEmailDoc() {
return emailDoc;
}

public void setEmailDoc(byte[] emailDoc) {
this.emailDoc = emailDoc;
}

public byte[] getTataLritDoc() {
return tataLritDoc;
}

public void setTataLritDoc(byte[] tataLritDoc) {
this.tataLritDoc = tataLritDoc;
}

public byte[] getFlagRegDoc() {
return flagRegDoc;
}

public void setFlagRegDoc(byte[] flagRegDoc) {
this.flagRegDoc = flagRegDoc;
}

public byte[] getConformanceTestCertDoc() {
return conformanceTestCertDoc;
}

public void setConformanceTestCertDoc(byte[] conformanceTestCertDoc) {
this.conformanceTestCertDoc = conformanceTestCertDoc;
}

public byte[] getAdditionalDoc() {
return additionalDoc;
}

public void setAdditionalDoc(byte[] additionalDoc) {
this.additionalDoc = additionalDoc;
}

@Override
public String toString() {
return "DocumentForm [emailDoc=" + Arrays.toString(emailDoc) + ", tataLritDoc=" + Arrays.toString(tataLritDoc)
+ ", flagRegDoc=" + Arrays.toString(flagRegDoc) + ", conforanceTestCertDoc="
+ Arrays.toString(conformanceTestCertDoc) + ", additionalDoc=" + Arrays.toString(additionalDoc) + "]";
}

}

