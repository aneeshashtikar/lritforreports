package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="portal_vessel_reporting_status_history")
@NamedQuery(name="PortalVesselReportingStatusHistory.findAll", query="SELECT p FROM PortalVesselReportingStatusHistory p")
public class PortalVesselReportingStatusHistory implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="vesselReportingStatusId", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="vesselReportingStatusId", sequenceName="vessel_reporting_status", allocationSize=1)
	@Column(name="vessel_reporting_status_id", nullable=false)
	private Long vesselReportingStatusId;
	
	@Column(name="pendingtask_id")
	private Integer pendingTaskId;
	
	@Column(name="reporting_status")
	private String reportingStatus;
	
	@Column(name="reason")
	private String reason;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="supporting_docs")
	private byte[] supportingDocument;

	public Long getVesselReportingStatusId() {
		return vesselReportingStatusId;
	}

	public void setVesselReportingStatusId(Long vesselReportingStatusId) {
		this.vesselReportingStatusId = vesselReportingStatusId;
	}

	public Integer getPendingTaskId() {
		return pendingTaskId;
	}

	public void setPendingTaskId(Integer pendingTaskId) {
		this.pendingTaskId = pendingTaskId;
	}

	public String getReportingStatus() {
		return reportingStatus;
	}

	public void setReportingStatus(String reportingStatus) {
		this.reportingStatus = reportingStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public byte[] getSupportingDocument() {
		return supportingDocument;
	}

	public void setSupportingDocument(byte[] supportingDocument) {
		this.supportingDocument = supportingDocument;
	}

	@Override
	public String toString() {
		return "PortalVesselReportingStatusHistory [vesselReportingStatusId=" + vesselReportingStatusId
				+ ", pendingTaskId=" + pendingTaskId + ", reportingStatus=" + reportingStatus + ", reason=" + reason
				+ ", remark=" + remark + ", supportingDocument=" + Arrays.toString(supportingDocument) + "]";
	}
	
}
