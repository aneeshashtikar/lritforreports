package in.gov.lrit.portal.model.report;

import java.io.Serializable;

public class CompositeKeyForASPShipPosition implements Serializable {

	private static final long serialVersionUID = 1L;

	private String imoNo;
	private String messageId;
}
