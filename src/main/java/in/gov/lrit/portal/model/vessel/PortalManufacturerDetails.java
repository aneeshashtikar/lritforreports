package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;


/**
 * The persistent class for the portal_manufacturer_details database table.
 * 
 */
@Entity
@Table(name="portal_manufacturer_details")
@NamedQuery(name="PortalManufacturerDetails.findAll", query="SELECT p FROM PortalManufacturerDetails p")
public class PortalManufacturerDetails implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue(generator="manufacturerIdSeq", strategy=GenerationType.SEQUENCE)
@SequenceGenerator(name="manufacturerIdSeq", sequenceName="seq_vessal", allocationSize=1) 
@Column(name="manufacturer_id", nullable=false)
private Integer manufacturerId;

@Column(name="manufacturer_name")
private String manufacturerName;

@Column(name="status")
private String status;

@CreationTimestamp
@Column(name="reg_date")
private Timestamp regDate;

@Column(name="de_reg_date")
private Timestamp deRegDate;

public PortalManufacturerDetails() {
}

public Integer getManufacturerId() {
	return manufacturerId;
}

public void setManufacturerId(Integer manufacturerId) {
	this.manufacturerId = manufacturerId;
}

public String getManufacturerName() {
	return manufacturerName;
}

public void setManufacturerName(String manufacturerName) {
	this.manufacturerName = manufacturerName;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public Timestamp getRegDate() {
	return regDate;
}

public void setRegDate(Timestamp regDate) {
	this.regDate = regDate;
}

public Timestamp getDeRegDate() {
	return deRegDate;
}

public void setDeRegDate(Timestamp deRegDate) {
	this.deRegDate = deRegDate;
}

@Override
public String toString() {
	return "PortalManufacturerDetails [manufacturerId=" + manufacturerId + ", manufacturerName=" + manufacturerName
			+ ", status=" + status + ", regDate=" + regDate + ", deRegDate=" + deRegDate + "]";
}

}