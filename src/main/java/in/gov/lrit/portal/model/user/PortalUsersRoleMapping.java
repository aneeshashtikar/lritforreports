package in.gov.lrit.portal.model.user;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="portal_role_user_mapping")
public class PortalUsersRoleMapping implements Serializable{
	@EmbeddedId
	private PortalUsersRoleMappingPK id;

	public PortalUsersRoleMappingPK getId() {
		return id;
	}

	public void setId(PortalUsersRoleMappingPK id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "PortalUsersRoleMapping [id=" + id + "]";
	}
	
	
}
