package in.gov.lrit.portal.model.vessel;

public interface ManufacturerList {
	
	String getManufacturerId();
	String getManufacturerName();

}
