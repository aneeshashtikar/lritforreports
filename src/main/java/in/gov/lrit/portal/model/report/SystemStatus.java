package in.gov.lrit.portal.model.report;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name = "dc_systemstatus")
public class SystemStatus {
	
	
	@Id
	@Column(name = "message_id")
	private String messageId;
	
	@Column(name = "originator")
	private String originator;
	
	@Column(name = "receiver")
	private String destination;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "lrit_timestamp")
	private Timestamp receivedTime;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getOriginator() {
		return originator;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Timestamp getReceivedTimestamp() {
		return receivedTime;
	}

	public void setReceivedTimestamp(Timestamp receivedTimestamp) {
		this.receivedTime = receivedTimestamp;
	}

	@Override
	public String toString() {
		return "SystemStatus [messageId=" + messageId + ", originator=" + originator + ", destination=" + destination
				+ ", receivedTimestamp=" + receivedTime + "]";
	}
	
}
