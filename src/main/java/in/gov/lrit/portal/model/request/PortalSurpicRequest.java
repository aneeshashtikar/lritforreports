package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the portal_surpic_request database table.
 * 
 */
/**
 * @author poojak
 *
 */
@Entity
@Table(name="portal_surpic_request")
//@NamedQuery(name="PortalSurpicRequest.findAll", query="SELECT p FROM PortalSurpicRequest p")
public class PortalSurpicRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_req",sequenceName="seq_portal_surpic_request", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_req")
	@Column(name="request_id")
	private Long requestId;

	@Column(name="access_type")
	private Integer accessType;

	@Column(name="circular_area")
	private String circularArea;

	@Column(name="data_user_provider")
	private String dataUserProvider;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "message_id", referencedColumnName = "message_id")
	private PortalRequest portalRequest;

	@Column(name="message_type")
	private Integer messageType;

	@Column(name="number_of_position")
	private Integer numberOfPosition;

	@Column(name="rectangular_area")
	private String rectangularArea;

	@Column(name="ship_type")
	private String shipType;
    
	@Transient
	public String Countries;
	 
	@Transient
	public String offsetLatitudedegree;
	
	@Transient
	public String offsetLatitudemin;
	
	@Transient
	public String offsetLatitudedir;
	
	@Transient
	public String offsetLongitudedir;
	
	@Transient
	public String offsetLongitudedegree;

	@Transient
	public String offsetLongitudemin;
	

	@Transient
	public String rectangleLatitudedegree;
	
	@Transient
	public String rectangleLatitudemin;
	
	@Transient
	public String rectangleLatitudedir;
	
	@Transient
	public String rectangleLongitudedir;
	
	@Transient
	public String rectangleLongitudedegree;

	@Transient
	public String rectangleLongitudemin;
	
	
	@Transient
	public String circleLatitudedegree;
	
	@Transient
	public String circleLatitudemin;
	
	@Transient
	public String circleLatitudedir;
	
	@Transient
	public String circleLongitudedir;
	
	@Transient
	public String circleLongitudedegree;

	@Transient
	public String circleLongitudemin;
	
	@Transient
	public String radius;
	
	
	
	public PortalSurpicRequest() {
	}

	public Long getRequestId() {
		return this.requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public Integer getAccessType() {
		return this.accessType;
	}

	public void setAccessType(Integer accessType) {
		this.accessType = accessType;
	}

	public String getCircularArea() {
		return this.circularArea;
	}

	public void setCircularArea(String circularArea) {
		this.circularArea = circularArea;
	}

	public String getDataUserProvider() {
		return this.dataUserProvider;
	}

	public void setDataUserProvider(String dataUserProvider) {
		this.dataUserProvider = dataUserProvider;
	}

	

	public PortalRequest getPortalRequest() {
		return portalRequest;
	}

	public void setPortalRequest(PortalRequest portalRequest) {
		this.portalRequest = portalRequest;
	}

	public Integer getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public Integer getNumberOfPosition() {
		return this.numberOfPosition;
	}

	public void setNumberOfPosition(Integer numberOfPosition) {
		this.numberOfPosition = numberOfPosition;
	}

	public String getRectangularArea() {
		return this.rectangularArea;
	}

	public void setRectangularArea(String rectangularArea) {
		this.rectangularArea = rectangularArea;
	}

	public String getShipType() {
		return this.shipType;
	}

	public void setShipType(String shipType) {
		this.shipType = shipType;
	}

	public String getOffsetLatitudedegree() {
		return offsetLatitudedegree;
	}

	public void setOffsetLatitudedegree(String offsetLatitudedegree) {
		this.offsetLatitudedegree = offsetLatitudedegree;
	}

	public String getOffsetLatitudemin() {
		return offsetLatitudemin;
	}

	public void setOffsetLatitudemin(String offsetLatitudemin) {
		this.offsetLatitudemin = offsetLatitudemin;
	}

	public String getOffsetLatitudedir() {
		return offsetLatitudedir;
	}

	public void setOffsetLatitudedir(String offsetLatitudedir) {
		this.offsetLatitudedir = offsetLatitudedir;
	}

	public String getOffsetLongitudedir() {
		return offsetLongitudedir;
	}

	public void setOffsetLongitudedir(String offsetLongitudedir) {
		this.offsetLongitudedir = offsetLongitudedir;
	}

	public String getOffsetLongitudedegree() {
		return offsetLongitudedegree;
	}

	public void setOffsetLongitudedegree(String offsetLongitudedegree) {
		this.offsetLongitudedegree = offsetLongitudedegree;
	}

	public String getOffsetLongitudemin() {
		return offsetLongitudemin;
	}

	public void setOffsetLongitudemin(String offsetLongitudemin) {
		this.offsetLongitudemin = offsetLongitudemin;
	}

	public String getRectangleLatitudedegree() {
		return rectangleLatitudedegree;
	}

	public void setRectangleLatitudedegree(String rectangleLatitudedegree) {
		this.rectangleLatitudedegree = rectangleLatitudedegree;
	}

	public String getRectangleLatitudemin() {
		return rectangleLatitudemin;
	}

	public void setRectangleLatitudemin(String rectangleLatitudemin) {
		this.rectangleLatitudemin = rectangleLatitudemin;
	}

	public String getRectangleLatitudedir() {
		return rectangleLatitudedir;
	}

	public void setRectangleLatitudedir(String rectangleLatitudedir) {
		this.rectangleLatitudedir = rectangleLatitudedir;
	}

	public String getRectangleLongitudedir() {
		return rectangleLongitudedir;
	}

	public void setRectangleLongitudedir(String rectangleLongitudedir) {
		this.rectangleLongitudedir = rectangleLongitudedir;
	}

	public String getRectangleLongitudedegree() {
		return rectangleLongitudedegree;
	}

	public void setRectangleLongitudedegree(String rectangleLongitudedegree) {
		this.rectangleLongitudedegree = rectangleLongitudedegree;
	}

	public String getRectangleLongitudemin() {
		return rectangleLongitudemin;
	}

	public void setRectangleLongitudemin(String rectangleLongitudemin) {
		this.rectangleLongitudemin = rectangleLongitudemin;
	}

	public String getCircleLatitudedegree() {
		return circleLatitudedegree;
	}

	public void setCircleLatitudedegree(String circleLatitudedegree) {
		this.circleLatitudedegree = circleLatitudedegree;
	}

	public String getCircleLatitudemin() {
		return circleLatitudemin;
	}

	public void setCircleLatitudemin(String circleLatitudemin) {
		this.circleLatitudemin = circleLatitudemin;
	}

	public String getCircleLatitudedir() {
		return circleLatitudedir;
	}

	public void setCircleLatitudedir(String circleLatitudedir) {
		this.circleLatitudedir = circleLatitudedir;
	}

	public String getCircleLongitudedir() {
		return circleLongitudedir;
	}

	public void setCircleLongitudedir(String circleLongitudedir) {
		this.circleLongitudedir = circleLongitudedir;
	}

	public String getCircleLongitudedegree() {
		return circleLongitudedegree;
	}

	public void setCircleLongitudedegree(String circleLongitudedegree) {
		this.circleLongitudedegree = circleLongitudedegree;
	}

	public String getCircleLongitudemin() {
		return circleLongitudemin;
	}

	public void setCircleLongitudemin(String circleLongitudemin) {
		this.circleLongitudemin = circleLongitudemin;
	}

	public String getCountries() {
		return Countries;
	}

	public void setCountries(String countries) {
		Countries = countries;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	
	

	@Override
	public String toString() {
		return "PortalSurpicRequest [requestId=" + requestId + ", accessType=" + accessType + ", circularArea="
				+ circularArea + ", dataUserProvider=" + dataUserProvider + ", portalRequest=" + portalRequest
				+ ", messageType=" + messageType + ", numberOfPosition=" + numberOfPosition + ", rectangularArea="
				+ rectangularArea + ", shipType=" + shipType + ", offsetLatitudedegree=" + offsetLatitudedegree
				+ ", offsetLatitudemin=" + offsetLatitudemin + ", offsetLatitudedir=" + offsetLatitudedir
				+ ", offsetLongitudedir=" + offsetLongitudedir + ", offsetLongitudedegree=" + offsetLongitudedegree
				+ ", offsetLongitudemin=" + offsetLongitudemin + ", rectangleLatitudedegree=" + rectangleLatitudedegree
				+ ", rectangleLatitudemin=" + rectangleLatitudemin + ", rectangleLatitudedir=" + rectangleLatitudedir
				+ ", rectangleLongitudedir=" + rectangleLongitudedir + ", rectangleLongitudedegree="
				+ rectangleLongitudedegree + ", rectangleLongitudemin=" + rectangleLongitudemin
				+ ", circleLatitudedegree=" + circleLatitudedegree + ", circleLatitudemin=" + circleLatitudemin
				+ ", circleLatitudedir=" + circleLatitudedir + ", circleLongitudedir=" + circleLongitudedir
				+ ", circleLongitudedegree=" + circleLongitudedegree + ", circleLongitudemin=" + circleLongitudemin
				+ ", radius=" + radius + "]";
	}

	
}