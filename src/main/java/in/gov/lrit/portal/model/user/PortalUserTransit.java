package in.gov.lrit.portal.model.user;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;


/**
 * The persistent class for the portal_user_transit database table.
 * 
 */
@Entity
@Table(name="portal_user_transit")
//@NamedQuery(name="PortalUserTransit.findAll", query="SELECT p FROM PortalUserTransit p")
public class PortalUserTransit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "portalUserTransitId", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "portalUserTransitId", sequenceName = "portal_user_transit_id", allocationSize=1)
	private Long id;

	private String address1;

	private String address2;

	private String address3;

	private String category;

	private String city;

	@Column(name="company_code")
	private String companyCode;

	private String country;

	@Column(name="deregister_date")
	private Timestamp deregisterDate;

	private String district;

	private String emailid;

	@Column(name="alternateemailid")
	private String alternateemailid;

	
	private String fax;

	private String landmark;

	@Column(name="login_id")
	private String loginId;

	private String mobileno;

	private String name;

	@Column(name="no_of_password_attempt")
	private Integer noOfPasswordAttempt;

	private String password;

	@Column(name="pendingtask_id")
	private Long pendingtaskId;

	private String pincode;

	@Column(name="register_date")
	private Timestamp registerDate;

	private String relation;

	@Column(name="requestors_lrit_id")
	private String requestorsLritId;

	@Column(name="sar_area_view")
	private Boolean sarAreaView;

	private String state;

	private String status;

	private String telephone;

	private Timestamp tokenvalidupto;

	@Column(name="update_date")
	private Timestamp updateDate;

	@Column(name="updated_by")
	private String updatedBy;

	@Column(name="uploaded_file_path")
	private byte[] uploadedFilePath;

	@Column(name="validate_token")
	private String validateToken;

	private String website;

	public PortalUserTransit() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress1() {
		return this.address1;
	}

	public String getAlternateemailid() {
		return alternateemailid;
	}

	public void setAlternateemailid(String alternateemailid) {
		this.alternateemailid = alternateemailid;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCompanyCode() {
		return this.companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Timestamp getDeregisterDate() {
		return this.deregisterDate;
	}

	public void setDeregisterDate(Timestamp deregisterDate) {
		this.deregisterDate = deregisterDate;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getEmailid() {
		return this.emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getLandmark() {
		return this.landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getMobileno() {
		return this.mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNoOfPasswordAttempt() {
		return this.noOfPasswordAttempt;
	}

	public void setNoOfPasswordAttempt(Integer noOfPasswordAttempt) {
		this.noOfPasswordAttempt = noOfPasswordAttempt;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getPendingtaskId() {
		return this.pendingtaskId;
	}

	public void setPendingtaskId(Long pendingtaskId) {
		this.pendingtaskId = pendingtaskId;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Timestamp getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}

	public String getRelation() {
		return this.relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getRequestorsLritId() {
		return this.requestorsLritId;
	}

	public void setRequestorsLritId(String requestorsLritId) {
		this.requestorsLritId = requestorsLritId;
	}

	public Boolean getSarAreaView() {
		return this.sarAreaView;
	}

	public void setSarAreaView(Boolean sarAreaView) {
		this.sarAreaView = sarAreaView;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Timestamp getTokenvalidupto() {
		return this.tokenvalidupto;
	}

	public void setTokenvalidupto(Timestamp tokenvalidupto) {
		this.tokenvalidupto = tokenvalidupto;
	}

	public Timestamp getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public byte[] getUploadedFilePath() {
		return this.uploadedFilePath;
	}

	public void setUploadedFilePath(byte[] uploadedFilePath) {
		this.uploadedFilePath = uploadedFilePath;
	}

	public String getValidateToken() {
		return this.validateToken;
	}

	public void setValidateToken(String validateToken) {
		this.validateToken = validateToken;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Override
	public String toString() {
		return "PortalUserTransit [id=" + id + ", address1=" + address1 + ", address2=" + address2 + ", address3="
				+ address3 + ", category=" + category + ", city=" + city + ", companyCode=" + companyCode + ", country="
				+ country + ", deregisterDate=" + deregisterDate + ", district=" + district + ", emailid=" + emailid
				+ ", alternateemailid=" + alternateemailid + ", fax=" + fax + ", landmark=" + landmark + ", loginId="
				+ loginId + ", mobileno=" + mobileno + ", name=" + name + ", noOfPasswordAttempt=" + noOfPasswordAttempt
				+ ", password=" + password + ", pendingtaskId=" + pendingtaskId + ", pincode=" + pincode
				+ ", registerDate=" + registerDate + ", relation=" + relation + ", requestorsLritId=" + requestorsLritId
				+ ", sarAreaView=" + sarAreaView + ", state=" + state + ", status=" + status + ", telephone="
				+ telephone + ", tokenvalidupto=" + tokenvalidupto + ", updateDate=" + updateDate + ", updatedBy="
				+ updatedBy +  ", validateToken="
				+ validateToken + ", website=" + website + "]";
	}

/*	@Override
	public String toString() {
		return "PortalUserTransit [id=" + id + ", address1=" + address1 + ", address2=" + address2 + ", address3="
				+ address3 + ", category=" + category + ", city=" + city + ", companyCode=" + companyCode + ", country="
				+ country + ", deregisterDate=" + deregisterDate + ", district=" + district + ", emailid=" + emailid
				+ ", fax=" + fax + ", landmark=" + landmark + ", loginId=" + loginId + ", mobileno=" + mobileno
				+ ", name=" + name + ", noOfPasswordAttempt=" + noOfPasswordAttempt + ", password=" + password
				+ ", pendingtaskId=" + pendingtaskId + ", pincode=" + pincode + ", registerDate=" + registerDate
				+ ", relation=" + relation + ", requestorsLritId=" + requestorsLritId + ", sarAreaView=" + sarAreaView
				+ ", state=" + state + ", status=" + status + ", telephone=" + telephone + ", tokenvalidupto="
				+ tokenvalidupto + ", updateDate=" + updateDate + ", updatedBy=" + updatedBy + ", uploadedFilePath="
				+ Arrays.toString(uploadedFilePath) + ", validateToken=" + validateToken + ", website=" + website + "]";
	}
*/
}