package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Arrays;

import javax.persistence.*;

/**
 * The persistent class for the portal_vessel_details database table.
 * 
 */

@Entity
@Table(name="portal_vessel_details")
@NamedQuery(name="PortalVesselDetail.findAll", query="SELECT p FROM PortalVesselDetail p")
public class PortalVesselDetail implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@GeneratedValue(generator="vesselSeq", strategy=GenerationType.SEQUENCE)
@SequenceGenerator(name="vesselSeq", sequenceName="seq_vessal", allocationSize=1) 
@Column(name="vessel_id", nullable=false)
private Integer vesselId; //

@Column(name="call_sign")
private String callSign;  //

@Column(name="dead_weight")
private BigInteger deadWeight; //

@Column(name="gross_tonnage")
private BigInteger grossTonnage; //

@Column(name="imo_no", nullable=false, unique=true)
private String imoNo; //

@Column(name="mmsi_no", nullable=false, unique=true)
private String mmsiNo; //

@Column(name="pi_club")
private String piClub; //

/*@CreationTimestamp
@Temporal(TemporalType.DATE)*/
@Column(name="reg_date")
private Timestamp regDate; //

@Column(name="registration_no")
private BigInteger registrationNo; //

@Column(name="status")
private String status; //

/*@CreationTimestamp
@Temporal(TemporalType.DATE)*/
@Column(name="reg_status_date")
private Timestamp regStatusDate; //

@Column(name="time_delay")
private BigInteger timeDelay; //

@Column(name="vessel_category")
private String vesselCategory; //

@Column(name="vessel_name", nullable=false)
private String vesselName; //

/*@Column(name="vessel_subcategory")
private String vesselSubcategory; *///

@Column(name="vessel_type")
private String vesselType; //

@Column(name="year_of_built")
private BigInteger yearOfBuilt; //

@Column(name="vessel_image")
private byte[] vesselImage; //

@Column(name="imo_vessel_type")
private String imoVesselType; //

/*@Column(name="owner_companycode")
private String ownerCompanyCode;

@Column(name="manager_companycode")
private String technicalCompanyCode;

@Column(name="comm_companycode")
private String commercialCompanyCode;

@Column(name="cso_id")
private String csoId;*/

@Column(name="registration_status")
private String regStatus;

@ManyToOne
@JoinColumn(name="group_id")
private PortalVesselGroup portalVesselGroup;

//column added on 9/10/2019
@Column(name="email_doc")
private byte[] emailDoc;

@Column(name="tata_lrit_doc")
private byte[] tataLritDoc;

@Column(name="flag_reg_doc")
private byte[] flagRegDoc;

@Column(name="conforance_test_cert_doc")
private byte[] conformanceTestCertDoc;

@Column(name="additional_doc")
private byte[] additionalDoc;

//added on 13/11/2019 for vessel action management process
@Column(name="reporting_status_change_reason")
private String reportingStatusChangeReason;

//added on 13/11/2019 for vessel action management process
@Column(name="reporting_status_change_remarks")
private String reportingStatusChangeRemarks;

/*@OneToMany(fetch=FetchType.LAZY)
@JoinColumn(name="lrit_csoid_seq", nullable = false, insertable = false, updatable = false)
private Set<PortalCSODPADetails> csodpaDet;*/

/*@OneToMany(mappedBy="")
private Set<PortalShippingCompanyVesselRelation> pscvr;*/

public PortalVesselDetail() {
	
}

public PortalVesselDetail(String imoNo, String vesselName, String mmsiNo, String callSign) {
	super();
	this.callSign = callSign;
	this.imoNo = imoNo;
	this.mmsiNo = mmsiNo;
	this.vesselName = vesselName;
}

public PortalVesselDetail(String imoNo, String vesselName, String mmsiNo, String callSign, String status) {
	super();
	this.callSign = callSign;
	this.imoNo = imoNo;
	this.mmsiNo = mmsiNo;
	this.vesselName = vesselName;
	this.status = "Submitted";
}

public Integer getVesselId() {
	return vesselId;
}

public void setVesselId(Integer vesselId) {
	this.vesselId = vesselId;
}

public String getCallSign() {
	return callSign;
}

public void setCallSign(String callSign) {
	this.callSign = callSign;
}

public BigInteger getDeadWeight() {
	return deadWeight;
}

public void setDeadWeight(BigInteger deadWeight) {
	this.deadWeight = deadWeight;
}

public BigInteger getGrossTonnage() {
	return grossTonnage;
}

public void setGrossTonnage(BigInteger grossTonnage) {
	this.grossTonnage = grossTonnage;
}

public String getImoNo() {
	return imoNo;
}

public void setImoNo(String imoNo) {
	this.imoNo = imoNo;
}

public String getMmsiNo() {
	return mmsiNo;
}

public void setMmsiNo(String mmsiNo) {
	this.mmsiNo = mmsiNo;
}

public String getPiClub() {
	return piClub;
}

public void setPiClub(String piClub) {
	this.piClub = piClub;
}

public Timestamp getRegDate() {
	return regDate;
}

public void setRegDate(Timestamp regDate) {
	this.regDate = regDate;
}

public BigInteger getRegistrationNo() {
	return registrationNo;
}

public void setRegistrationNo(BigInteger registrationNo) {
	this.registrationNo = registrationNo;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public Timestamp getRegStatusDate() {
	return regStatusDate;
}

public void setRegStatusDate(Timestamp regStatusDate) {
	this.regStatusDate = regStatusDate;
}

public BigInteger getTimeDelay() {
	return timeDelay;
}

public void setTimeDelay(BigInteger timeDelay) {
	this.timeDelay = timeDelay;
}

public String getVesselCategory() {
	return vesselCategory;
}

public void setVesselCategory(String vesselCategory) {
	this.vesselCategory = vesselCategory;
}

public String getVesselName() {
	return vesselName;
}

public void setVesselName(String vesselName) {
	this.vesselName = vesselName;
}

/*public String getVesselSubcategory() {
	return vesselSubcategory;
}

public void setVesselSubcategory(String vesselSubcategory) {
	this.vesselSubcategory = vesselSubcategory;
}*/

public String getVesselType() {
	return vesselType;
}

public void setVesselType(String vesselType) {
	this.vesselType = vesselType;
}

public BigInteger getYearOfBuilt() {
	return yearOfBuilt;
}

public void setYearOfBuilt(BigInteger yearOfBuilt) {
	this.yearOfBuilt = yearOfBuilt;
}

public byte[] getVesselImage() {
	return vesselImage;
}

public void setVesselImage(byte[] vesselImage) {
	this.vesselImage = vesselImage;
}

public String getImoVesselType() {
	return imoVesselType;
}

public void setImoVesselType(String imoVesselType) {
	this.imoVesselType = imoVesselType;
}

public byte[] getEmailDoc() {
	return emailDoc;
}

public void setEmailDoc(byte[] emailDoc) {
	this.emailDoc = emailDoc;
}

public byte[] getTataLritDoc() {
	return tataLritDoc;
}

public void setTataLritDoc(byte[] tataLritDoc) {
	this.tataLritDoc = tataLritDoc;
}

public byte[] getFlagRegDoc() {
	return flagRegDoc;
}

public void setFlagRegDoc(byte[] flagRegDoc) {
	this.flagRegDoc = flagRegDoc;
}

public byte[] getConformanceTestCertDoc() {
	return conformanceTestCertDoc;
}

public void setConformanceTestCertDoc(byte[] conformanceTestCertDoc) {
	this.conformanceTestCertDoc = conformanceTestCertDoc;
}

public byte[] getAdditionalDoc() {
	return additionalDoc;
}

public void setAdditionalDoc(byte[] additionalDoc) {
	this.additionalDoc = additionalDoc;
}

public PortalVesselGroup getPortalVesselGroup() {
	return portalVesselGroup;
}

public void setPortalVesselGroup(PortalVesselGroup portalVesselGroup) {
	this.portalVesselGroup = portalVesselGroup;
}

public String getRegStatus() {
	return regStatus;
}

public void setRegStatus(String regStatus) {
	this.regStatus = regStatus;
}

public String getReportingStatusChangeReason() {
	return reportingStatusChangeReason;
}

public void setReportingStatusChangeReason(String reportingStatusChangeReason) {
	this.reportingStatusChangeReason = reportingStatusChangeReason;
}

public String getReportingStatusChangeRemarks() {
	return reportingStatusChangeRemarks;
}

public void setReportingStatusChangeRemarks(String reportingStatusChangeRemarks) {
	this.reportingStatusChangeRemarks = reportingStatusChangeRemarks;
}

@Override
public String toString() {
	return "PortalVesselDetail [vesselId=" + vesselId + ", callSign=" + callSign + ", deadWeight=" + deadWeight
			+ ", grossTonnage=" + grossTonnage + ", imoNo=" + imoNo + ", mmsiNo=" + mmsiNo + ", piClub=" + piClub
			+ ", regDate=" + regDate + ", registrationNo=" + registrationNo + ", status=" + status + ", regStatusDate="
			+ regStatusDate + ", timeDelay=" + timeDelay + ", vesselCategory=" + vesselCategory + ", vesselName="
			+ vesselName + ", vesselType=" + vesselType + ", yearOfBuilt="
			+ yearOfBuilt + ", imoVesselType=" + imoVesselType
			+ ", regStatus=" + regStatus + ", portalVesselGroup=" + portalVesselGroup + ", emailDoc="
			+ Arrays.toString(emailDoc) + ", tataLritDoc=" + Arrays.toString(tataLritDoc) + ", flagRegDoc="
			+ Arrays.toString(flagRegDoc) + ", conformanceTestCertDoc=" + Arrays.toString(conformanceTestCertDoc)
			+ ", additionalDoc=" + Arrays.toString(additionalDoc) + ", reportingStatusChangeReason="
			+ reportingStatusChangeReason + ", reportingStatusChangeRemarks=" + reportingStatusChangeRemarks + "]";
}

}