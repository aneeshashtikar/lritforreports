package in.gov.lrit.portal.model.user;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Parameter;
import org.springframework.lang.Nullable;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
@Entity
@Table(name="portal_cso_dpa_details")
@TableGenerator(name="portal_cso_dpa_details")
public class PortalCsoDpaDetail implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	
	
	@Column(length = 50,name="lrit_csoid_seq")
	private String csoidseq;

	@Column(nullable = false,length = 50)
	private String name;

	@Column(name="telephone_office",nullable = false)
	private String telephoneOffice;
	
	@Column(nullable = false,length = 50)
	private String emailid;
	
	@Column(name="telephone_residence",nullable = false)
	private String telephoneResidence;
	
	@Column(name="mobile_no",nullable = false)
	private String mobileNo;
	
	
	@Column(name="deregister_date")
	private Timestamp deregisterDate;

	@Column(length=50)
	private String type;
	
	
	@Column(name="register_date")
	private  Timestamp registerDate;
	
	@Transient
	private String flag;

	
/*	@ManyToOne
	@JoinColumn(name = "company_code", nullable = false)
	private PortalShippingCompany portalShippingCompany;*/
	//Changes done on 1/10/19
	@Column(name="company_code")
	private String companyCode;
	
	@Column(name="alternative_csoid",nullable = false)
	private String parentCsoid;
	
	
	public String getParentCsoid() {
		return parentCsoid;
	}



	public void setParentCsoid(String parentCsoid) {
		this.parentCsoid = parentCsoid;
	}



	public String getCompanyCode() {
		return companyCode;
	}



	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}



	@Column(name="login_id")
	private String loginId;
	
	
	
	
	
	
	public Timestamp getRegisterDate() {
		return registerDate;
	}



	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}



	public String getLoginId() {
		return loginId;
	}



	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}



	public PortalCsoDpaDetail() {
		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp timestamp = new Timestamp(time);
		this.registerDate=timestamp;
	}
	
	

	public String getFlag() {
		return flag;
	}



	public void setFlag(String flag) {
		this.flag = flag;
	}



	public String getCsoidseq() {
		return csoidseq;
	}


	public void setCsoidseq(String csoidseq) {
		this.csoidseq = csoidseq;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getTelephoneOffice() {
		return telephoneOffice;
	}


	public void setTelephoneOffice(String telephoneOffice) {
		this.telephoneOffice = telephoneOffice;
	}


	public String getEmailid() {
		return emailid;
	}


	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}


	public String getTelephoneResidence() {
		return telephoneResidence;
	}


	public void setTelephoneResidence(String telephoneResidence) {
		this.telephoneResidence = telephoneResidence;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public Timestamp getDeregisterDate() {
		return deregisterDate;
	}


	public void setDeregisterDate(Timestamp deregisterDate) {
		this.deregisterDate = deregisterDate;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "PortalCsoDpaDetail [csoidseq=" + csoidseq + ", name=" + name + ", telephoneOffice=" + telephoneOffice
				+ ", emailid=" + emailid + ", telephoneResidence=" + telephoneResidence + ", mobileNo=" + mobileNo
				+ ", deregisterDate=" + deregisterDate + ", type=" + type + ", registerDate=" + registerDate + ", flag="
				+ flag + ", companyCode=" + companyCode + ", parentCsoid=" + parentCsoid + ", loginId=" + loginId + "]";
	}


}
