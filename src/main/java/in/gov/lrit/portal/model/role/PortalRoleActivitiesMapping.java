package in.gov.lrit.portal.model.role;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the portal_role_activities_mapping database table.
 * 
 */
@Entity
@Table(name="portal_role_activities_mapping")
///@NamedQuery(name="PortalRoleActivitiesMapping.findAll", query="SELECT p FROM PortalRoleActivitiesMapping p")
public class PortalRoleActivitiesMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PortalRoleActivitiesMappingPK id;

	public PortalRoleActivitiesMapping() {
	}

	public PortalRoleActivitiesMappingPK getId() {
		return this.id;
	}

	public void setId(PortalRoleActivitiesMappingPK id) {
		this.id = id;
	}

}