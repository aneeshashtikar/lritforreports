package in.gov.lrit.portal.model.vessel;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import in.gov.lrit.portal.model.vessel.PortalVesselCategory;


/**
 * The persistent class for the portal_vessel_type database table.
 * 
 */
@Entity
@Table(name="portal_vessel_type")
@NamedQuery(name="PortalVesselType.findAll", query="SELECT p FROM PortalVesselType p")
public class PortalVesselType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="vessel_type_id")
	private String vesselTypeId;

	@Column(name="vessel_type")
	private String vesselType;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "portal_vessel_type_category_mapping",
	        joinColumns = @JoinColumn(name = "vessel_type"),
	        inverseJoinColumns = @JoinColumn(name = "vessel_category"))
	private Set<PortalVesselCategory> getVesselCategory = new HashSet<>();
	
	public PortalVesselType() {
	}

	public String getVesselTypeId() {
		return vesselTypeId;
	}

	public void setVesselTypeId(String vesselTypeId) {
		this.vesselTypeId = vesselTypeId;
	}

	public String getVesselType() {
		return vesselType;
	}

	public void setVesselType(String vesselType) {
		this.vesselType = vesselType;
	}

	public Set<PortalVesselCategory> getGetVesselCategory() {
		return getVesselCategory;
	}

	public void setGetVesselCategory(Set<PortalVesselCategory> getVesselCategory) {
		this.getVesselCategory = getVesselCategory;
	}

	@Override
	public String toString() {
		return "PortalVesselType [vesselTypeId=" + vesselTypeId + ", vesselType=" + vesselType + ", getVesselCategory="
				+ getVesselCategory + "]";
	}
	
}
