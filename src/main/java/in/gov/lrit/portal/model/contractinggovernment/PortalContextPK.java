package in.gov.lrit.portal.model.contractinggovernment;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the portal_context database table.
 * 
 */
@Embeddable
public class PortalContextPK implements Serializable {
	@Override
	public String toString() {
		return "PortalContextPK [loginId=" + loginId + ", contextId=" + contextId + "]";
	}

	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="login_id", insertable=false, updatable=false)
	private String loginId;

	@Column(name="context_id", insertable=false, updatable=false)
	private String contextId;

	public PortalContextPK() {
	}
	public String getLoginId() {
		return this.loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getContextId() {
		return this.contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PortalContextPK)) {
			return false;
		}
		PortalContextPK castOther = (PortalContextPK)other;
		return 
			this.loginId.equals(castOther.loginId)
			&& this.contextId.equals(castOther.contextId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.loginId.hashCode();
		hash = hash * prime + this.contextId.hashCode();
		
		return hash;
	}
}