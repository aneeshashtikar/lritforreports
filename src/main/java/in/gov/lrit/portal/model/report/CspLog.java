package in.gov.lrit.portal.model.report;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name ="dc_position_report")
@IdClass(CompositeKeyForCspLog.class)
public class CspLog {
	
	@Id
	@Column(name = "message_id")
	private String messageId;
	
	@Id
	@Column(name = "imo_no")
	private String imoNo;
	
	@Column(name = "mmsi_no")
	private String mmsiNo;
	
	@Column(name = "ship_name")	
	private String shipName;
	
	@Column(name = "ship_borne_equipment_id")
	private String shipBorneEquipmentId;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "timestamp1")
	private Timestamp timestamp1;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "timestamp4")
	private Timestamp timestamp4;
	
	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "longitude")
	private String longitude;
	
	@Column(name = "data_user_provider")
	private String dataUserProvider;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getImoNo() {
		return imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public String getMmsiNo() {
		return mmsiNo;
	}

	public void setMmsiNo(String mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getShipBorneEquipmentId() {
		return shipBorneEquipmentId;
	}

	public void setShipBorneEquipmentId(String shipBorneEquipmentId) {
		this.shipBorneEquipmentId = shipBorneEquipmentId;
	}

	public Timestamp getTimestamp1() {
		return timestamp1;
	}

	public void setTimestamp1(Timestamp timestamp1) {
		this.timestamp1 = timestamp1;
	}

	public Timestamp getTimestamp4() {
		return timestamp4;
	}

	public void setTimestamp4(Timestamp timestamp4) {
		this.timestamp4 = timestamp4;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDataUserProvider() {
		return dataUserProvider;
	}

	public void setDataUserProvider(String dataUserProvider) {
		this.dataUserProvider = dataUserProvider;
	}

	@Override
	public String toString() {
		return "CspLog [messageId=" + messageId + ", imoNo=" + imoNo + ", mmsiNo=" + mmsiNo + ", shipName=" + shipName
				+ ", shipBorneEquipmentId=" + shipBorneEquipmentId + ", timestamp1=" + timestamp1 + ", timestamp4="
				+ timestamp4 + ", latitude=" + latitude + ", longitude=" + longitude + ", dataUserProvider="
				+ dataUserProvider + "]";
	}
	
	
}
