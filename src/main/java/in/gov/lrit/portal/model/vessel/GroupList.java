package in.gov.lrit.portal.model.vessel;

public interface GroupList {

	Long getGroupId();
	String getGroupName();
	
}
