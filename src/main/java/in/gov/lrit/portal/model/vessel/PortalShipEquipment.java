package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the portal_ship_equipement database table.
 * 
 */
@Entity
@Table(name="portal_ship_equipement")
@NamedQuery(name="PortalShipEquipment.findAll", query="SELECT p FROM PortalShipEquipment p")
public class PortalShipEquipment implements Serializable {
private static final long serialVersionUID = 1L;

@Id
@Column(name="shipborne_equipment_id", nullable=false)
private String shipborneEquipmentId; 

@Column(name="dnid_no")
private Integer dnidNo; 

@Column(name="imo_no", nullable=false)
private String imoNo; 

@Column(name="member_no")
private Integer memberNo; 

@Column(name="ocean_region")
private String oceanRegion; 

@Column(name="request_payload")
private String requestPayload; 

@Column(name="response_payload")
private String responsePayload; 

@OneToOne(cascade=CascadeType.ALL)
@JoinColumn(name="vessel_id")
private PortalVesselDetail vesselDet; 

@ManyToOne
@JoinColumn(name="model_id", nullable=false)
private PortalModelDetail modelDet;

public PortalShipEquipment() {
}

public PortalShipEquipment(PortalVesselDetail vesseldet)
{
	this.vesselDet = vesseldet;
}

public String getShipborneEquipmentId() {
return this.shipborneEquipmentId;
}

public void setShipborneEquipmentId(String shipborneEquipmentId) {
this.shipborneEquipmentId = shipborneEquipmentId;
}

public Integer getDnidNo() {
return this.dnidNo;
}

public void setDnidNo(Integer dnidNo) {
this.dnidNo = dnidNo;
}

public String getImoNo() {
return this.imoNo;
}

public void setImoNo(String imoNo) {
this.imoNo = imoNo;
}

public Integer getMemberNo() {
return this.memberNo;
}

public void setMemberNo(Integer memberNo) {
this.memberNo = memberNo;
}

public String getOceanRegion() {
return this.oceanRegion;
}

public void setOceanRegion(String oceanRegion) {
this.oceanRegion = oceanRegion;
}


public String getRequestPayload() {
	return requestPayload;
}

public void setRequestPayload(String requestPayload) {
	this.requestPayload = requestPayload;
}

public String getResponsePayload() {
	return responsePayload;
}

public void setResponsePayload(String responsePayload) {
	this.responsePayload = responsePayload;
}

public PortalModelDetail getModelDet() {
	return modelDet;
}

public void setModelDet(PortalModelDetail modelDet) {
	this.modelDet = modelDet;
}

public PortalVesselDetail getVesselDet() {
	return vesselDet;
}

public void setVesselDet(PortalVesselDetail vesselDet) {
	this.vesselDet = vesselDet;
}

@Override
public String toString() {
	return "PortalShipEquipment [shipborneEquipmentId=" + shipborneEquipmentId + ", dnidNo=" + dnidNo + ", imoNo="
			+ imoNo + ", memberNo=" + memberNo + ", oceanRegion=" + oceanRegion + ", requestPayload=" + requestPayload
			+ ", responsePayload=" + responsePayload + ", vesselDet=" + vesselDet + ", modelDet=" + modelDet + "]";
}

}

