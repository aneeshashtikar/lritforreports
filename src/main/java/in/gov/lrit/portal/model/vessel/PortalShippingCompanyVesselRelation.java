package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

/**
 * The persistent class for the portal_shippingcompany_vessel_relation database table.
 * 
 */

@Entity
@Table(name="portal_shippingcompany_vessel_relation")
@NamedQuery(name="PortalShippingCompanyVesselRelation.findAll", query="SELECT p FROM PortalShippingCompanyVesselRelation p")
public class PortalShippingCompanyVesselRelation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name="relation")
	private Integer rel;
	
	@CreationTimestamp
	@Column(name="reg_date")
	private Timestamp regrDate;
	
	@Column(name="de_reg_date")
	private Timestamp deRegDate;
	
	@EmbeddedId
	private VesselLoginMappingId customId;
	
	@Column(name="current_cso_id")
	private String ccsoId;
	
	@Column(name="alternate_cso_id")
	private String acsoId;
	
	@Column(name="dpa_id")
	private String dpaId;
	
	@Transient
	private String status;

	public VesselLoginMappingId getCustomId() {
		return customId;
	}

	public void setCustomId(VesselLoginMappingId customId) {
		this.customId = customId;
	}

	public Timestamp getRegrDate() {
		return regrDate;
	}

	public void setRegrDate(Timestamp regrDate) {
		this.regrDate = regrDate;
	}

	public Timestamp getDeRegDate() {
		return deRegDate;
	}

	public void setDeRegDate(Timestamp deRegDate) {
		this.deRegDate = deRegDate;
	}

	public String getCcsoId() {
		return ccsoId;
	}

	public Integer getRel() {
		return rel;
	}

	public void setRel(Integer rel) {
		this.rel = rel;
	}

	public void setCcsoId(String ccsoId) {
		this.ccsoId = ccsoId;
	}

	public String getAcsoId() {
		return acsoId;
	}

	public void setAcsoId(String acsoId) {
		this.acsoId = acsoId;
	}

	public String getDpaId() {
		return dpaId;
	}

	public void setDpaId(String dpaId) {
		this.dpaId = dpaId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PortalShippingCompanyVesselRelation [rel=" + rel + ", regrDate=" + regrDate + ", deRegDate=" + deRegDate
				+ ", customId=" + customId + ", ccsoId=" + ccsoId + ", acsoId=" + acsoId + ", dpaId=" + dpaId
				+ ", status=" + status + "]";
	}
	
}