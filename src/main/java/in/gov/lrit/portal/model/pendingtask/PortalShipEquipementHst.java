package in.gov.lrit.portal.model.pendingtask;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the portal_ship_equipement_hst database table.
 * 
 */
@Entity
@Table(name="portal_ship_equipement_hst")
@NamedQuery(name="PortalShipEquipementHst.findAll", query="SELECT p FROM PortalShipEquipementHst p")
public class PortalShipEquipementHst implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_seid_hst", sequenceName="portal_ship_equipement_hst_id",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_seid_hst")
	@Column(name="id")
	private Integer id;

	@Column(name="dnid_no")
	private Integer dnidNo;

	@Column(name="imo_no")
	private String imoNo;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="model_id")
	private Integer modelId;

	@Column(name="ocean_region")
	private String oceanRegion;

	@Column(name="response_payload")
	private String responsePayload;

	@Column(name="request_payload")
	private String requestPayload;

	@Column(name="shipborne_equipment_id")
	private String shipborneEquipmentId;

	private String status;

	@Column(name="vessel_id")
	private Integer vesselId;
	
	@Column(name="update_date")
	private Timestamp updateDate;

	public PortalShipEquipementHst() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDnidNo() {
		return this.dnidNo;
	}

	public void setDnidNo(Integer dnidNo) {
		this.dnidNo = dnidNo;
	}


	public String getImoNo() {
		return this.imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Integer getModelId() {
		return this.modelId;
	}

	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}

	public String getOceanRegion() {
		return this.oceanRegion;
	}

	public void setOceanRegion(String oceanRegion) {
		this.oceanRegion = oceanRegion;
	}


	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(String responsePayload) {
		this.responsePayload = responsePayload;
	}

	public String getRequestPayload() {
		return this.requestPayload;
	}

	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}


	public String getShipborneEquipmentId() {
		return this.shipborneEquipmentId;
	}

	public void setShipborneEquipmentId(String shipborneEquipmentId) {
		this.shipborneEquipmentId = shipborneEquipmentId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getVesselId() {
		return this.vesselId;
	}

	public void setVesselId(Integer vesselId) {
		this.vesselId = vesselId;
	}

}