package in.gov.lrit.portal.model.vessel;

public interface VesselListInterface {

	public Integer getVesselId();
	public String getCallSign();
	public String getImoNo();
	public String getMmsiNo();
	public String getVesselName();
	/*public String getStatus();*/
	public String getRegStatus();
}
