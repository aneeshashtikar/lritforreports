package in.gov.lrit.portal.model.standingOrder;


import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the standingOrderCountriesExclusion database table.
 * 
 */

@Entity
@Table(name="portal_standingorders_countries_exclusion")
public class SOCountriesExclusion implements Serializable {

private static final long serialVersionUID = 1L;

@Id
@SequenceGenerator(name="SO_Request_Seq", sequenceName = "portal_standingorders_countries_exclusion_seq" , allocationSize=1)
@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SO_Request_Seq" )     
@Column(name="id")
private int id;

@Column(name = "standingorder_id")
private int soId;

@Column(name="excluded_countries")
private String excludedcountries;

@Column(name = "area_code")
private String areacode;


@Override
public String toString() {
	return excludedcountries;
}


public String getExcludedCountries() {
	return excludedcountries;
}

public void setExcludedCountries(String excludedcountries) {
	this.excludedcountries = excludedcountries;
}

public int getSOId() {
	return soId;
}

public void setSOId(int sOId) {
	soId = sOId;
}

public String getAreacode() {
	return areacode;
}

public void setAreacode(String areacode) {
	this.areacode = areacode;
}

}
