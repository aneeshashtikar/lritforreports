package in.gov.lrit.portal.model.alert;

import java.sql.Timestamp;

public interface AlertDetailsDTO {
	
	public String getMessage();
	public Timestamp getCreatedDate();

}
