package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * The persistent class for the portal_requests database table.
 * 
 */
@Entity
@Table(name="portal_requests")
public class PortalRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="message_id")
	private String messageId;

	@Column(name="approvers_login_id")
	private String approversLoginId;

	@Column(name="current_ddp_version")
	private String currentddpversion;

	@Column(name="request_category")
	private String requestCategory;

	@Column(name="request_generation_time")
	private Timestamp requestGenerationTime;

	@Column(name="requestors_login_id")
	private String requestorsLoginId;

	private String status;

	@Column(name="status_date")
	private Timestamp statusDate;
	
	@Column(name="request_payload")
	private String requestpayload;
	
	
	@Column(name="response_payload")
	private String responsepayload;

	@OneToOne(mappedBy = "portalRequest")
	private PortalDdpRequest ddpRequest;

	public PortalRequest() {
	}


	public String getApproversLoginId() {
		return this.approversLoginId;
	}



	public void setApproversLoginId(String approversLoginId) {
		this.approversLoginId = approversLoginId;
	}


	public String getCurrentddpversion() {
		return currentddpversion;
	}

	public void setCurrentddpversion(String currentddpversion) {
		this.currentddpversion = currentddpversion;
	}

	public String getRequestCategory() {
		return this.requestCategory;
	}

	public void setRequestCategory(String requestCategory) {
		this.requestCategory = requestCategory;
	}

	public Timestamp getRequestGenerationTime() {
		return this.requestGenerationTime;
	}

	public void setRequestGenerationTime(Timestamp requestGenerationTime) {
		this.requestGenerationTime = requestGenerationTime;
	}

	public String getRequestorsLoginId() {
		return this.requestorsLoginId;
	}

	public void setRequestorsLoginId(String requestorsLoginId) {
		this.requestorsLoginId = requestorsLoginId;
	}

	public String getStatus() {
		return this.status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getStatusDate() {
		return this.statusDate;
	}

	public void setStatusDate(Timestamp statusDate) {
		this.statusDate = statusDate;
	}

	public String getMessageId() {
		return messageId;
	}


	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	


	public String getRequestpayload() {
		return requestpayload;
	}


	public void setRequestpayload(String requestpayload) {
		this.requestpayload = requestpayload;
	}


	public String getResponsepayload() {
		return responsepayload;
	}


	public void setResponsepayload(String responsepayload) {
		this.responsepayload = responsepayload;
	}


	@Override
	public String toString() {
		return "PortalRequest [messageId=" + messageId + ", approversLoginId=" + approversLoginId
				+ ", currentddpversion=" + currentddpversion + ", requestCategory=" + requestCategory
				+ ", requestGenerationTime=" + requestGenerationTime + ", requestorsLoginId=" + requestorsLoginId
				+ ", status=" + status + ", statusDate=" + statusDate + ", ddpRequest=" + ddpRequest + "]";
	}
	

}