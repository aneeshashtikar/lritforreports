package in.gov.lrit.portal.model.vessel;

public interface EditVesselInterface {

	public String getCallSign();
	public String getVesselName();
}
