package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.util.List;

import javax.sound.sampled.Port;

public class VesselRegistrationDTO implements Serializable{
private static final long serialVersionUID = 1L;

private DocumentForm docs;

private PortalShipEquipment shipEq;

private List<PortalShippingCompanyVesselRelation> pscvrList;

public DocumentForm getDocs() {
return docs;
}

public void setDocs(DocumentForm docs) {
this.docs = docs;
}

public PortalShipEquipment getShipEq() {
return shipEq;
}

public void setShipEq(PortalShipEquipment shipEq) {
this.shipEq = shipEq;
}

public List<PortalShippingCompanyVesselRelation> getPscvrList() {
return pscvrList;
}

public void setPscvrList(List<PortalShippingCompanyVesselRelation> pscvrList) {
this.pscvrList = pscvrList;
}

@Override
public String toString() {
return "VesselRegistrationDTO [docs=" + docs + ", shipEq=" + shipEq + ", pscvrList=" + pscvrList + "]";
}

/*@Override
public String toString() {
return "VesselRegistration [shipEq=" + shipEq + ", pscvrList=" + pscvrList + "]";
}*/

}