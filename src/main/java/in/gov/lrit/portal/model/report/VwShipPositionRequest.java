package in.gov.lrit.portal.model.report;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vw_asp_log_position_report")
public class VwShipPositionRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "message_id")
	private String messageId;
	
	@Column(name = "originator")
	private String originator;
	
	@Column(name = "destination")
	private String destination;
	
	@Column(name = "message_type")
	private BigInteger messageType;
	
	@Column(name = "receive_time")
	private String receiveTime;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getOriginator() {
		return originator;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public BigInteger getMessageType() {
		return messageType;
	}

	public void setMessageType(BigInteger messageType) {
		this.messageType = messageType;
	}

	public String getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(String receiveTime) {
		this.receiveTime = receiveTime;
	}

	@Override
	public String toString() {
		return "VwShipPositionRequest [messageId=" + messageId + ", Originator=" + originator + ", Destination="
				+ destination + ", messageType=" + messageType + ", receiveTime=" + receiveTime + "]";
	}
	
	

}
