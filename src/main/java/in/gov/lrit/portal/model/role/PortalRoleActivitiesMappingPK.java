package in.gov.lrit.portal.model.role;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the portal_role_activities_mapping database table.
 * 
 */
@Embeddable
public class PortalRoleActivitiesMappingPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="role_id")
	private String roleId;

	@Column(name="activity_id")
	private String activityId;

	public PortalRoleActivitiesMappingPK() {
	}
	public String getRoleId() {
		return this.roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getActivityId() {
		return this.activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PortalRoleActivitiesMappingPK)) {
			return false;
		}
		PortalRoleActivitiesMappingPK castOther = (PortalRoleActivitiesMappingPK)other;
		return 
			this.roleId.equals(castOther.roleId)
			&& this.activityId.equals(castOther.activityId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.roleId.hashCode();
		hash = hash * prime + this.activityId.hashCode();
		
		return hash;
	}
}