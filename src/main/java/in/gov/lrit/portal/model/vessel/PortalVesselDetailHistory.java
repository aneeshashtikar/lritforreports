package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="portal_vessel_details_history")
@NamedQuery(name="PortalVesselDetailHistory.findAll", query="SELECT p FROM PortalVesselDetailHistory p")
public class PortalVesselDetailHistory implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="vesselDetailsHstId", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="vesselDetailsHstId", sequenceName="vessel_details_history", allocationSize=1)
	@Column(name="vessel_details_hst_id", nullable=false)
	private Long vesselDetHstId;
	
	@Column(name="pendingtask_id")
	private Integer pendingTaskId;
	
	@Column(name="vessel_name")
	private String vesselName;
	
	@Column(name="call_sign")
	private String callSign;
	
	@Column(name="vessel_group")
	private String vesselGroup;
	
	@Column(name="updated_date")
	private Timestamp updatedDate;

	public Long getVesselDetHstId() {
		return vesselDetHstId;
	}

	public void setVesselDetHstId(Long vesselDetHstId) {
		this.vesselDetHstId = vesselDetHstId;
	}

	public Integer getPendingTaskId() {
		return pendingTaskId;
	}

	public void setPendingTaskId(Integer pendingTaskId) {
		this.pendingTaskId = pendingTaskId;
	}

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}

	public String getCallSign() {
		return callSign;
	}

	public void setCallSign(String callSign) {
		this.callSign = callSign;
	}

	public String getVesselGroup() {
		return vesselGroup;
	}

	public void setVesselGroup(String vesselGroup) {
		this.vesselGroup = vesselGroup;
	}
	
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "PortalVesselDetailHistory [vesselDetHstId=" + vesselDetHstId + ", pendingTaskId=" + pendingTaskId
				+ ", vesselName=" + vesselName + ", callSign=" + callSign + ", vesselGroup=" + vesselGroup
				+ ", updatedDate=" + updatedDate + "]";
	}
	
}
