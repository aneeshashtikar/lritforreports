package in.gov.lrit.portal.model.contractinggovernment;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "portal_lrit_id_master_hst")
public class PortalLritIdMasterHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_req",sequenceName="seq_portal_lrit_id_master", initialValue=205, allocationSize=12)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="seq_req")
	int id;

	@Column(name = "lrit_id")
	private String lritId;

	@Column(name = "costal_area")
	private String coastalArea;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "country_name")
	private String countryName;

	@Column(name = "reg_date")
	private Timestamp regDate;

	@Column(name = "de_reg_date")
	private Timestamp deregDate;

	private String type;

	public PortalLritIdMasterHistory() {
	}

	public String getLritId() {
		return this.lritId;
	}

	public void setLritId(String lritId) {
		this.lritId = lritId;
	}

	public String getCoastalArea() {
		return this.coastalArea;
	}

	public void setCoastalArea(String coastalArea) {
		this.coastalArea = coastalArea;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Timestamp getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getDeregDate() {
		return deregDate;
	}

	public void setDeregDate(Timestamp deregDate) {
		this.deregDate = deregDate;
	}

	@Override
	public String toString() {
		return "PortalLritIdMasterHistory [id=" + id + ", lritId=" + lritId + ", coastalArea=" + coastalArea
				+ ", countryCode=" + countryCode + ", countryName=" + countryName + ", regDate=" + regDate
				+ ", deregDate=" + deregDate + ", type=" + type + "]";
	}

}
