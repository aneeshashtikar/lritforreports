package in.gov.lrit.portal.model.request;

public interface ShipPositionNonFlag {

	public String getImoNo();
	public String getMmsiNo(); 
	public String getVesselName(); 
	public String getDataUserProvider();
	
	
	
}