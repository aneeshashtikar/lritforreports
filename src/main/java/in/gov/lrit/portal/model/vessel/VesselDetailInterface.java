package in.gov.lrit.portal.model.vessel;

public interface VesselDetailInterface {
	
	public Integer getVesselId();
	public String getCallSign();
	public String getImoNo();
	public String getMmsiNo();
	public String getVesselName();

}




