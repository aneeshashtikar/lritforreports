package in.gov.lrit.portal.model.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public interface LritLatestShipPostionInterface extends Serializable {
	public String getImoNo();
	public BigDecimal getLatitude();
	public BigDecimal getLongitude();
	public String getShipName();
	public BigDecimal getSpeed();
	public Timestamp getTimestamp1();

}
