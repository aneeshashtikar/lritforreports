package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.util.List;

public class ApprovedVesselRegistartionDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	//added on 6/11/2019
	/*private DocumentForm docs;*/
	
	//updated on 13/1/2020
	private List<PortalDocuments> docs;
	
	private PortalShipEquipment shipEq;
	
	/*private List<PortalShippingCompanyVesselRelation> pscvrList;*/
	
	private List<MappingNames> nameslist;
	
	/*public DocumentForm getDocs() {
		return docs;
	}

	public void setDocs(DocumentForm docs) {
		this.docs = docs;
	}*/
	
	public List<PortalDocuments> getDocs() {
		return docs;
	}

	public void setDocs(List<PortalDocuments> docs) {
		this.docs = docs;
	}

	public PortalShipEquipment getShipEq() {
		return shipEq;
	}

	public void setShipEq(PortalShipEquipment shipEq) {
		this.shipEq = shipEq;
	}

	public List<MappingNames> getNameslist() {
		return nameslist;
	}

	public void setNameslist(List<MappingNames> nameslist) {
		this.nameslist = nameslist;
	}

	/*public List<PortalShippingCompanyVesselRelation> getPscvrList() {
		return pscvrList;
	}

	public void setPscvrList(List<PortalShippingCompanyVesselRelation> pscvrList) {
		this.pscvrList = pscvrList;
	}*/

	/*@Override
	public String toString() {
		return "ApprovedVesselRegistartionDTO [docs=" + docs + ", shipEq=" + shipEq + ", pscvrList=" + pscvrList
				+ ", nameslist=" + nameslist + "]";
	}*/

	@Override
	public String toString() {
		return "ApprovedVesselRegistartionDTO [docs=" + docs + ", shipEq=" + shipEq + ", nameslist=" + nameslist + "]";
	}
	
	/*@Override
	public String toString() {
		return "ApprovedVesselRegistartionDTO [docs=" + docs + ", shipEq=" + shipEq + ", pscvrList=" + pscvrList + "]";
	}*/

	/*public List<String> getCompanyName() {
		return companyName;
	}

	public void setCompanyName(List<String> companyName) {
		this.companyName = companyName;
	}

	public List<String> getUserName() {
		return userName;
	}

	public void setUserName(List<String> userName) {
		this.userName = userName;
	}

	public List<String> getCcsoName() {
		return ccsoName;
	}

	public void setCcsoName(List<String> ccsoName) {
		this.ccsoName = ccsoName;
	}

	public List<String> getAcsoName() {
		return acsoName;
	}

	public void setAcsoName(List<String> acsoName) {
		this.acsoName = acsoName;
	}

	public List<String> getDpaName() {
		return dpaName;
	}

	public void setDpaName(List<String> dpaName) {
		this.dpaName = dpaName;
	}*/

	/*@Override
	public String toString() {
		return "ApprovedVesselRegistartionDTO [docs=" + docs + ", shipEq=" + shipEq + ", pscvrList=" + pscvrList
				+ ", companyName=" + companyName + ", userName=" + userName + ", ccsoName=" + ccsoName + ", acsoName="
				+ acsoName + ", dpaName=" + dpaName + "]";
	}*/

	/*@Override
	public String toString() {
		return "ApprovedVesselRegistartionDTO [shipEq=" + shipEq + ", pscvrList=" + pscvrList + ", companyName="
				+ companyName + ", userName=" + userName + ", ccsoName=" + ccsoName + ", acsoName=" + acsoName
				+ ", dpaName=" + dpaName + "]";
	}*/

}