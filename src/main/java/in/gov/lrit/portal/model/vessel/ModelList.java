package in.gov.lrit.portal.model.vessel;

public interface ModelList {
	
	Integer getModelId();
	String getModelName();

}
