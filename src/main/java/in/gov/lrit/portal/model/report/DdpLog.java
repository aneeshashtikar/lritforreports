package in.gov.lrit.portal.model.report;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "dc_ddprequest")
public class DdpLog {

	@Id
	@Column(name = "message_id")
	private String messageId;
	
	@Column(name = "reference_id")
	private String referenceMessageId;
	
	@Column(name = "update_type")
	private String updateType;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "lrit_timestamp")
	private Timestamp receivedTime;
	
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getReferenceMessageId() {
		return referenceMessageId;
	}

	public void setReferenceMessageId(String referenceMessageId) {
		this.referenceMessageId = referenceMessageId;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	public Timestamp getReceivedTime() {
		return receivedTime;
	}

	public void setReceivedTime(Timestamp receivedTime) {
		this.receivedTime = receivedTime;
	}

	@Override
	public String toString() {
		return "DdpLog [messageId=" + messageId + ", referenceMessageId=" + referenceMessageId + ", updateType="
				+ updateType + ", receivedTime=" + receivedTime + "]";
	}
	
	
}
