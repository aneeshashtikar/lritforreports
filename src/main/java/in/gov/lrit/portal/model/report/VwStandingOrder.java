package in.gov.lrit.portal.model.report;

import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name = "vw_lrit_standing_orders")
public class VwStandingOrder {
	
	@Id
	@Column(name = "so_id")
	private BigInteger soId;
	
	@Column(name = "request_message_id")
	private String requestMessageId;
	
	@Column(name = "lrit_id")
	private String lritId;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "current_ddp_version")
	private String ddpVersion;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "raised_date")
	private Timestamp raisedDate;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "country_name")
	private String countryName;
	
	@Column(name = "response_message_id")
	private String resposeMessageId;
	
	@Column(name = "response_message")
	private String responseMessage;
	
	@Column(name = "excluded_areas")
	private String excludedAreas;
	
	@Column(name = "excluded_countries")
	private String excludedCountries;
	
	@Column(name = "excluded_vessels")
	private String excludedVessels;

	public BigInteger getSoId() {
		return soId;
	}

	public void setSoId(BigInteger soId) {
		this.soId = soId;
	}

	public String getRequestMessageId() {
		return requestMessageId;
	}

	public void setRequestMessageId(String requestMessageId) {
		this.requestMessageId = requestMessageId;
	}

	public String getLritId() {
		return lritId;
	}

	public void setLritId(String lritId) {
		this.lritId = lritId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDdpVersion() {
		return ddpVersion;
	}

	public void setDdpVersion(String ddpVersion) {
		this.ddpVersion = ddpVersion;
	}

	public Timestamp getRaisedDate() {
		return raisedDate;
	}

	public void setRaisedDate(Timestamp raisedDate) {
		this.raisedDate = raisedDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getResposeMessageId() {
		return resposeMessageId;
	}

	public void setResposeMessageId(String resposeMessageId) {
		this.resposeMessageId = resposeMessageId;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getExcludedAreas() {
		return excludedAreas;
	}

	public void setExcludedAreas(String excludedAreas) {
		this.excludedAreas = excludedAreas;
	}

	public String getExcludedCountries() {
		return excludedCountries;
	}

	public void setExcludedCountries(String excludedCountries) {
		this.excludedCountries = excludedCountries;
	}

	public String getExcludedVessels() {
		return excludedVessels;
	}

	public void setExcludedVessels(String excludedVessels) {
		this.excludedVessels = excludedVessels;
	}

	@Override
	public String toString() {
		return "VwStandingOrder [soId=" + soId + ", requestMessageId=" + requestMessageId + ", lritId=" + lritId
				+ ", status=" + status + ", ddpVersion=" + ddpVersion + ", raisedDate=" + raisedDate + ", userName="
				+ userName + ", countryName=" + countryName + ", resposeMessageId=" + resposeMessageId
				+ ", responseMessage=" + responseMessage + ", excludedAreas=" + excludedAreas + ", excludedCountries="
				+ excludedCountries + ", excludedVessels=" + excludedVessels + "]";
	}
	
	

}
