package in.gov.lrit.portal.model.request;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaModel;

/**
 * The persistent class for the portal_georaphical_area_request database table.
 * 
 */
@Entity
@Table(name="portal_geographical_area_request")
//@NamedQuery(name="PortalGeoraphicalAreaRequest.findAll", query="SELECT p FROM PortalGeoraphicalAreaRequest p")
public class PortalGeoraphicalAreaRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * @Id
	 * 
	 * @Column(name="request_id") private String requestId;
	 */
	
	//@Id
	//@GeneratedValue
	//@Column(name="request_id")
	@Id
	@SequenceGenerator(name="seq_req",sequenceName="seq_portal_geographical_area_request")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_req")
	@Column(name="request_id")
	private Integer requestId;
	
	@Column(name="actual_file")
	private byte[] actualFile;
	
	
	@Column(name="gml_id")
	private int gmlId;
	
	

	/*
	 * @Column(name="file_type") private String fileType;
	 */

	
	/*
	 * @Column(name="area_code") private String areaCode;
	 */
	//bi-directional many-to-one association to PortalGeoraphicalArea
	//@ManyToOne
	/*
	 * @OneToOne
	 * 
	 * @JoinColumn(name="area_code") private PortalGeoraphicalArea
	 * portalGeoraphicalArea;
	 */

	/*
	 * public String getAreaCode() { return areaCode; }
	 * 
	 * 
	 * public void setAreaCode(String areaCode) { this.areaCode = areaCode; }
	 */

	public int getGmlId() {
		return gmlId;
	}


	public void setGmlId(int gmlId) {
		this.gmlId = gmlId;
	}

	//bi-directional many-to-one association to PortalRequest
	//@ManyToOne
	/*
	 * @OneToOne
	 * 
	 * @JoinColumn(name="message_id")
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "message_id", referencedColumnName = "message_id")
	private PortalRequest portalRequest;
	
	@ManyToOne
	@JoinColumn(name="gml_id",insertable = false, updatable = false)
	private GeographicAreaModel geographicAreaModel;

	public PortalGeoraphicalAreaRequest() {
	}

	
	public Integer getRequestId() {
		return requestId;
	}


	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}


	public byte[] getActualFile() {
		return this.actualFile;
	}

	public void setActualFile(byte[] actualFile) {
		this.actualFile = actualFile;
	}

	
	
	/*
	 * public String getFileType() { return this.fileType; }
	 * 
	 * public void setFileType(String fileType) { this.fileType = fileType; }
	 */

	/*
	 * public PortalGeoraphicalArea getPortalGeoraphicalArea() { return
	 * this.portalGeoraphicalArea; }
	 * 
	 * public void setPortalGeoraphicalArea(PortalGeoraphicalArea
	 * portalGeoraphicalArea) { this.portalGeoraphicalArea = portalGeoraphicalArea;
	 * }
	 */

	public PortalRequest getPortalRequest() {
		return this.portalRequest;
	}

	public void setPortalRequest(PortalRequest portalRequest) {
		this.portalRequest = portalRequest;
	}

}