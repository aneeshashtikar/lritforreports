package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the dc_position_report database table.
 * 
 */
@Entity
@Table(name="dc_position_report")
@NamedQuery(name="DcPositionReport.findAll", query="SELECT d FROM DcPositionReport d")
public class DcPositionReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="message_id")
	private String messageId;

	@Column(name="asp_id")
	private String aspId;

	@Column(name="csp_id")
	private String cspId;

	@Column(name="csso_flag")
	private Boolean cssoFlag;

	@Column(name="data_user_provider")
	private String dataUserProvider;

	@Column(name="data_user_requestor")
	private String dataUserRequestor;

	@Column(name="dc_id")
	private String dcId;

	@Column(name="ddpversion_no")
	private String ddpversionNo;

	@Column(name="imo_no")
	private String imoNo;

	private BigDecimal latitude;

	private BigDecimal longitude;

	@Column(name="lrit_timestamp")
	private Timestamp lritTimestamp;

	@Column(name="message_type")
	private Integer messageType;

	@Column(name="mmsi_no")
	private String mmsiNo;

	@Column(name="next_timestamp")
	private Timestamp nextTimestamp;

	@Column(name="reference_id")
	private String referenceId;

	@Column(name="report_status")
	private String reportStatus;

	@Column(name="response_type")
	private Integer responseType;

	@Column(name="schema_version")
	private BigDecimal schemaVersion;

	@Column(name="ship_borne_equipment_id")
	private String shipBorneEquipmentId;

	@Column(name="ship_name")
	private String shipName;

	@Column(name="ship_type")
	private String shipType;

	private Integer test;

	private Timestamp timestamp1;

	private Timestamp timestamp2;

	private Timestamp timestamp3;

	private Timestamp timestamp4;

	private Timestamp timestamp5;

	public DcPositionReport() {
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getAspId() {
		return this.aspId;
	}

	public void setAspId(String aspId) {
		this.aspId = aspId;
	}

	public String getCspId() {
		return this.cspId;
	}

	public void setCspId(String cspId) {
		this.cspId = cspId;
	}

	public Boolean getCssoFlag() {
		return this.cssoFlag;
	}

	public void setCssoFlag(Boolean cssoFlag) {
		this.cssoFlag = cssoFlag;
	}

	public String getDataUserProvider() {
		return this.dataUserProvider;
	}

	public void setDataUserProvider(String dataUserProvider) {
		this.dataUserProvider = dataUserProvider;
	}

	public String getDataUserRequestor() {
		return this.dataUserRequestor;
	}

	public void setDataUserRequestor(String dataUserRequestor) {
		this.dataUserRequestor = dataUserRequestor;
	}

	public String getDcId() {
		return this.dcId;
	}

	public void setDcId(String dcId) {
		this.dcId = dcId;
	}

	public String getDdpversionNo() {
		return this.ddpversionNo;
	}

	public void setDdpversionNo(String ddpversionNo) {
		this.ddpversionNo = ddpversionNo;
	}

	public String getImoNo() {
		return this.imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public Timestamp getLritTimestamp() {
		return this.lritTimestamp;
	}

	public void setLritTimestamp(Timestamp lritTimestamp) {
		this.lritTimestamp = lritTimestamp;
	}

	public Integer getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public String getMmsiNo() {
		return this.mmsiNo;
	}

	public void setMmsiNo(String mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public Timestamp getNextTimestamp() {
		return this.nextTimestamp;
	}

	public void setNextTimestamp(Timestamp nextTimestamp) {
		this.nextTimestamp = nextTimestamp;
	}

	public String getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getReportStatus() {
		return this.reportStatus;
	}

	public void setReportStatus(String reportStatus) {
		this.reportStatus = reportStatus;
	}

	public Integer getResponseType() {
		return this.responseType;
	}

	public void setResponseType(Integer responseType) {
		this.responseType = responseType;
	}

	public BigDecimal getSchemaVersion() {
		return this.schemaVersion;
	}

	public void setSchemaVersion(BigDecimal schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	public String getShipBorneEquipmentId() {
		return this.shipBorneEquipmentId;
	}

	public void setShipBorneEquipmentId(String shipBorneEquipmentId) {
		this.shipBorneEquipmentId = shipBorneEquipmentId;
	}

	public String getShipName() {
		return this.shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getShipType() {
		return this.shipType;
	}

	public void setShipType(String shipType) {
		this.shipType = shipType;
	}

	public Integer getTest() {
		return this.test;
	}

	public void setTest(Integer test) {
		this.test = test;
	}

	public Timestamp getTimestamp1() {
		return this.timestamp1;
	}

	public void setTimestamp1(Timestamp timestamp1) {
		this.timestamp1 = timestamp1;
	}

	public Timestamp getTimestamp2() {
		return this.timestamp2;
	}

	public void setTimestamp2(Timestamp timestamp2) {
		this.timestamp2 = timestamp2;
	}

	public Timestamp getTimestamp3() {
		return this.timestamp3;
	}

	public void setTimestamp3(Timestamp timestamp3) {
		this.timestamp3 = timestamp3;
	}

	public Timestamp getTimestamp4() {
		return this.timestamp4;
	}

	public void setTimestamp4(Timestamp timestamp4) {
		this.timestamp4 = timestamp4;
	}

	public Timestamp getTimestamp5() {
		return this.timestamp5;
	}

	public void setTimestamp5(Timestamp timestamp5) {
		this.timestamp5 = timestamp5;
	}

}