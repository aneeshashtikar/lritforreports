package in.gov.lrit.portal.model.standingOrder;


import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the portal_standingorders_geographicalareas database table.
 * 
 */

@Entity
@Table(name="portal_standingorders_geographicalareas")
public class SOGeographicalAreas implements Serializable {

private static final long serialVersionUID = 1L;

@Id
@SequenceGenerator(name="SO_Request_Seq", sequenceName = "portal_standingorders_geographicalareas_seq" , allocationSize=1)
@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SO_Request_Seq" )     
@Column(name="id")
private int id;

@Column(name = "standingorder_id")
private int soId;

@Column(name="area_code")
private String areacode;  //

public String getAreacode() {
	return areacode;
}

public void setAreacode(String areacode) {
	this.areacode = areacode;
}

@Override
public String toString() {
	return areacode;
	//return "standingOrderGeographicalAreas [standingOrderId=" + SOId + ", areacode=" + areacode + "]";
}

public int getSOId() {
	return soId;
}

public void setSOId(int sOId) {
	soId = sOId;
}

}
