package in.gov.lrit.portal.model.route;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "portal_vessel_route")
public class Route {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "route_id", nullable = false)
	private String routeId;
	@Column(name = "imo_no", nullable = false)
	private String imoNo;
	@Column(name = "route_description")
	private String routeDescription;
	@Column(name = "route_status")
	private String routeStatus;

	@DateTimeFormat(pattern = "yyyy-mm-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "request_start_date", nullable = false)
	private Date startDate;

	@Transient
	private String startDateString;

	@Transient
	private String endDateString;

	@DateTimeFormat(pattern = "yyyy-mm-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "request_stop_date", nullable = false)
	private Date endDate;

	@Column(name = "country_list", nullable = false)
	private String countryList;
	@Transient
	public boolean flag = false;

	public boolean isFlag() {
		return this.flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getRouteId() {
		return this.routeId;
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public String getImoNo() {
		return this.imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public String getRouteDescription() {
		return this.routeDescription;
	}

	public void setRouteDescription(String routeDescription) {
		this.routeDescription = routeDescription;
	}

	public String getRouteStatus() {
		return this.routeStatus;
	}

	public void setRouteStatus(String routeStatus) {
		this.routeStatus = routeStatus;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCountryList() {
		return this.countryList;
	}

	public void setCountryList(String countryList) {
		this.countryList = countryList;
	}

	public String getStartDateString() {
		return startDateString;
	}

	public void setStartDateString(String startDateString) {
		this.startDateString = startDateString;
	}

	public String getEndDateString() {
		return endDateString;
	}

	public void setEndDateString(String endDateString) {
		this.endDateString = endDateString;
	}

	public String toString() {
		return "Route [routeId=" + this.routeId + ", imoNo=" + this.imoNo + ", routeDescription="
				+ this.routeDescription + ", routeStatus=" + this.routeStatus + ", startDate=" + this.startDate
				+ ", endDate=" + this.endDate + ", countryList=" + this.countryList + "]";
	}

}
