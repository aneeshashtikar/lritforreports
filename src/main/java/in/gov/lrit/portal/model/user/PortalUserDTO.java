package in.gov.lrit.portal.model.user;

import java.util.List;
import java.util.Set;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

public class PortalUserDTO {

	private PortalUser portaluser;
	/*private PortalShippingCompany portalShippingCompany;*/
	private PortalUserSarAuthority portalUserSarAuthority;
	
	private List<String> roles;
	private List<PortalCsoDpaDetail> currentcsoList;
	
	@Transient
	private int csolength;
	
	@Transient
	private int dpalength;

	public int getDpalength() {
		return dpalength;
	}

	public void setDpalength(int dpalength) {
		this.dpalength = dpalength;
	}

	public int getCsolength() {
		return csolength;
	}

	public void setCsolength(int csolength) {
		this.csolength = csolength;
	}

	public List<PortalCsoDpaDetail> getCurrentcsoList() {
		return currentcsoList;
	}

	public void setCurrentcsoList(List<PortalCsoDpaDetail> currentcsoList) {
		this.currentcsoList = currentcsoList;
	}

	public List<PortalCsoDpaDetail> getAlternatecsoList() {
		return alternatecsoList;
	}

	public void setAlternatecsoList(List<PortalCsoDpaDetail> alternatecsoList) {
		this.alternatecsoList = alternatecsoList;
	}

	private List<PortalCsoDpaDetail> alternatecsoList;

	private List<PortalCsoDpaDetail> dpaList;
	



	public List<PortalCsoDpaDetail> getDpaList() {
		return dpaList;
	}

	public void setDpaList(List<PortalCsoDpaDetail> dpaList) {
		this.dpaList = dpaList;
	}

	/*public List<PortalCsoDpaDetail> getCsoList() {
		return csoList;
	}

	public void setCsoList(List<PortalCsoDpaDetail> csoList) {
		this.csoList = csoList;
	}
*/
	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public PortalUser getPortaluser() {
		return portaluser;
	}
	
	public void setPortaluser(PortalUser portaluser) {
		this.portaluser = portaluser;
	}

/*	public PortalShippingCompany getPortalShippingCompany() {
		return portalShippingCompany;
	}
	
	public void setPortalShippingCompany(PortalShippingCompany portalShippingCompany) {
		this.portalShippingCompany = portalShippingCompany;
	}*/

	public PortalUserSarAuthority getPortalUserSarAuthority() {
		return portalUserSarAuthority;
	}

	public void setPortalUserSarAuthority(PortalUserSarAuthority portalUserSarAuthority) {
		this.portalUserSarAuthority = portalUserSarAuthority;
	}
/*
	@Override
	public String toString() {
		return "PortalUserDTO [portaluser=" + portaluser + ", portalUserSarAuthority=" + portalUserSarAuthority
				+ ", roles=" + roles + ", currentcsoList=" + currentcsoList + ", alternatecsoList=" + alternatecsoList
				+ ", dpaList=" + dpaList + "]";
	}
	*/

	@Override
	public String toString() {
		return "PortalUserDTO [portaluser=" + portaluser + ", portalUserSarAuthority=" + portalUserSarAuthority
				+ ", roles=" + roles + ", currentcsoList=" + currentcsoList + ", csolength=" + csolength
				+ ", dpalength=" + dpalength + ", alternatecsoList=" + alternatecsoList + ", dpaList=" + dpaList + "]";
	}
	
	
	
	
	
	
/*
	@Override
	public String toString() {
		return "PortalUserDTO [portaluser=" + portaluser + ", portalUserSarAuthority=" + portalUserSarAuthority
				+ ", roles=" + roles + ", currentcsoList=" + currentcsoList + ", alternatecsoList=" + alternatecsoList
				+ "]";
	}
	
	
	
	
	*/
/*
	@Override
	public String toString() {
		return "PortalUserDTO [portaluser=" + portaluser 
				+ ", portalUserSarAuthority=" + portalUserSarAuthority + ", roles=" + roles + ", csolist=" + csoList
				+ "]";
	}
*/
	/*@Override
	public String toString() {
		return "PortalUserDTO [portaluser=" + portaluser + ", portalShippingCompany=" + portalShippingCompany
				+ ", portalUserSarAuthority=" + portalUserSarAuthority + ", roles=" + roles + "]";
	}*/

	/*@Override
	public String toString() {
		return "PortalUserDTO [portaluser=" + portaluser + ", portalUserSarAuthority=" + portalUserSarAuthority
				+ ", roles=" + roles + "]";
	}
	*/
	
	
/*
	@Override
	public String toString() {
		return "PortalUserDTO [portaluser=" + portaluser + ", portalShippingCompany=" + portalShippingCompany
				+ ", portalUserSarAuthority=" + portalUserSarAuthority + ", role=" + role + "]";
	}
*/
/*	@Override
	public String toString() {
		return "PortalUserDTO [portaluser=" + portaluser + ", portalShippingCompany=" + portalShippingCompany
				+ ", portalUserSarAuthority=" + portalUserSarAuthority + "]";
	}*/ 
	
	
	
	
}
