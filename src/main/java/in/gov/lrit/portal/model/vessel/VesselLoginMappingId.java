package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import in.gov.lrit.portal.model.user.PortalUser;
import in.gov.lrit.portal.model.vessel.PortalVesselDetail;

@Embeddable
public class VesselLoginMappingId implements Serializable{
	private static final long serialVersionUID = 1L;

	@ManyToOne
    @JoinColumn(name = "user_id")
    private PortalUser userDet;
 
	@ManyToOne
    @JoinColumn(name = "vessel_id")
    private PortalVesselDetail vesselDet;
 
    public VesselLoginMappingId() {}
 
    public VesselLoginMappingId(PortalUser userDet, PortalVesselDetail vesselDet) {
		super();
		this.userDet = userDet;
		this.vesselDet = vesselDet;
	}
  
	public PortalUser getUserDet() {
        return userDet;
    }
 
    public PortalVesselDetail getVesselDet() {
        return vesselDet;
    }
 
    public void setUserDet(PortalUser userDet) {
		this.userDet = userDet;
	}

	public void setVesselDet(PortalVesselDetail vesselDet) {
		this.vesselDet = vesselDet;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VesselLoginMappingId)) return false;
        VesselLoginMappingId that = (VesselLoginMappingId) o;
        return Objects.equals(getUserDet(), that.getUserDet()) &&
                Objects.equals(getVesselDet(), that.getVesselDet());
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(getUserDet(), getVesselDet());
    }

	@Override
	public String toString() {
		return "VesselLoginMappingId [userDet=" + userDet + ", vesselDet=" + vesselDet + "]";
	}
    
	
	/*@Column(name="vessel_id")
	Integer vesselId;
	//updated on 01-10-2019
	@Column(name="company_code")
	String companyCode;

	@Column(name="user_id")
	String userId;
	
	public VesselLoginMappingId() {
		
	}
	
	public VesselLoginMappingId(Integer vesselId, String userId) {
		super();
		this.vesselId = vesselId;
		this.userId = userId;
	}



	public Integer getVesselId() {
		return vesselId;
	}

	public void setVesselId(Integer vesselId) {
		this.vesselId = vesselId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass())
			return false;

		VesselLoginMappingId that = (VesselLoginMappingId) o;
		return Objects.equals(vesselId, that.vesselId) &&
				Objects.equals(userId, that.userId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(vesselId, userId);
	}

	@Override
	public String toString() {
		return "VesselLoginMappingId [vesselId=" + vesselId + ", userId=" + userId + "]";
	}*/

	

	/*public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompany_code() {
		return companyCode;
	}

	public void setCompany_code(String companyCode) {
		this.companyCode = companyCode;
	}*/

	/* @ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="company_code", nullable = false, insertable = false, updatable = false)
		private Set<PortalShippingCompany> shipComp;

	    @ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="vessel_id", nullable = false, insertable = false, updatable = false)
		private Set<PortalVesselDetail> vesdet;

		public Set<PortalShippingCompany> getShipComp() {
			return shipComp;
		}

		public void setShipComp(Set<PortalShippingCompany> shipComp) {
			this.shipComp = shipComp;
		}

		public Set<PortalVesselDetail> getVesdet() {
			return vesdet;
		}

		public void setVesdet(Set<PortalVesselDetail> vesdet) {
			this.vesdet = vesdet;
		}*/

}
