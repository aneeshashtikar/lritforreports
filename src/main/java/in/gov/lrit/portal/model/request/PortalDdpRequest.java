package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;

/**
 * The persistent class for the portal_ddp_request database table.
 * 
 */
@Entity
@Table(name="portal_ddp_request")
public class PortalDdpRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_req_ddp",sequenceName="seq_portal_ddp_req", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_req_ddp")
	@Column(name="request_id")
	private int requestid;
	
	@Column(name="archived_ddp_version")
	private String archivedDdpVersion;
	
	@Column(name="archived_ddp_version_timestamp")
	private Timestamp archivedDdpVersiondate;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "message_id", referencedColumnName = "message_id")
	private PortalRequest portalRequest;

	@Column(name="test")
	private int testMessage;

	@Column(name="update_type")
	private int updateType;

	@Column(name="request_timestamp")
	private Timestamp requesttimestamp;
	
	public PortalDdpRequest() {
	}

	public PortalRequest getPortalRequest() {
		return portalRequest;
	}

	public void setPortalRequest(PortalRequest portalRequest) {
		this.portalRequest = portalRequest;
	}

	public String getArchivedDdpVersion() {
		return this.archivedDdpVersion;
	}

	public void setArchivedDdpVersion(String archivedDdpVersion) {
		this.archivedDdpVersion = archivedDdpVersion;
	}

	public int getRequestid() {
		return requestid;
	}

	public Timestamp getArchivedDdpVersiondate() {
		return archivedDdpVersiondate;
	}

	public void setArchivedDdpVersiondate(Timestamp archivedDdpVersiondate) {
		this.archivedDdpVersiondate = archivedDdpVersiondate;
	}

	public void setRequestid(int requestid) {
		this.requestid = requestid;
	}

	public Timestamp getRequesttimestamp() {
		return requesttimestamp;
	}

	public void setRequesttimestamp(Timestamp requesttimestamp) {
		this.requesttimestamp = requesttimestamp;
	}


	public int getTestMessage() {
		return this.testMessage;
	}

	public void setTestMessage(int testMessage) {
		this.testMessage = testMessage;
	}

	public int getUpdateType() {
		return this.updateType;
	}

	public void setUpdateType(int updateType) {
		this.updateType = updateType;
	}

	@Override
	public String toString() {
		return "PortalDdpRequest [requestid=" + requestid + ", archivedDdpVersion=" + archivedDdpVersion
				+ ", portalRequest=" + portalRequest + ", testMessage=" + testMessage + ", updateType=" + updateType
				+ ", requesttimestamp=" + requesttimestamp + "]";
	}
	
	
	
}