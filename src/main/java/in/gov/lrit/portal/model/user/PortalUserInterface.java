package in.gov.lrit.portal.model.user;

import java.io.Serializable;
import java.sql.Timestamp;

public interface PortalUserInterface extends Serializable{
	public String getLoginId();
	public String getName();
	public String getCategory();
	public String getStatus();
	public Timestamp getDeregisterDate();
	public Integer getNoOfPasswordAttempt();
	public String getCountry();
	/*public String getRequestorsLritId();*/
	/*public PortalLritIdMasterInterface getRequestorsLritId();*/
	
	
}
