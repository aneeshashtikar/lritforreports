package in.gov.lrit.portal.model.alert;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="portal_alert_mst")
public class AlertMasterDetails {

	@Id
	@Column(name="alert_id")
	private int alertId;
	
	@Column(name="alert_name")
	private String alertName;
	
	@Column(name="alert_desc")
	private String alertDesc;
	
	@Column(name="servelity")
	private String severity;

	public int getAlertId() {
		return alertId;
	}

	public void setAlertId(int alertId) {
		this.alertId = alertId;
	}

	public String getAlertName() {
		return alertName;
	}

	public void setAlertName(String alertName) {
		this.alertName = alertName;
	}

	public String getAlertDesc() {
		return alertDesc;
	}

	public void setAlertDesc(String alertDesc) {
		this.alertDesc = alertDesc;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}
}
