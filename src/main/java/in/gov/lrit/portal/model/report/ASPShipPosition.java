package in.gov.lrit.portal.model.report;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "asp_shipposition")
@IdClass(CompositeKeyForASPShipPosition.class)
public class ASPShipPosition {
	@Id
	@Column(name = "imo_no")
	private String imoNo;
	
	@Id
	@Column(name = "message_id")
	private String messageId;
	
	@Column(name ="mmsi_no")
	private String mmsiNo;
	
	@Column(name ="ship_name")
	private String shipName;
	
	@Column(name = "shipborne_equipment_id")
	private String shipborneEquId;
	
	@Column(name = "latitude")
	private BigDecimal lattitude;
	
	@Column(name = "longitude")
	private BigDecimal longitude;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "timestamp1")
	private Timestamp timestamp1;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "timestamp2")
	private Timestamp timestamp2;
	
	@Column(name = "timestamp3")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp timestamp3;
	
	@Column(name = "timestamp4")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp timestamp4;

	public String getImoNo() {
		return imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public String getMmsiNo() {
		return mmsiNo;
	}

	public void setMmsiNo(String mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getShipborneEquId() {
		return shipborneEquId;
	}

	public void setShipborneEquId(String shipborneEquId) {
		this.shipborneEquId = shipborneEquId;
	}

	public BigDecimal getLattitude() {
		return lattitude;
	}

	public void setLattitude(BigDecimal lattitude) {
		this.lattitude = lattitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public Timestamp getTimestamp1() {
		return timestamp1;
	}

	public void setTimestamp1(Timestamp timestamp1) {
		this.timestamp1 = timestamp1;
	}

	public Timestamp getTimestamp2() {
		return timestamp2;
	}

	public void setTimestamp2(Timestamp timestamp2) {
		this.timestamp2 = timestamp2;
	}

	public Timestamp getTimestamp3() {
		return timestamp3;
	}

	public void setTimestamp3(Timestamp timestamp3) {
		this.timestamp3 = timestamp3;
	}

	public Timestamp getTimestamp4() {
		return timestamp4;
	}

	public void setTimestamp4(Timestamp timestamp4) {
		this.timestamp4 = timestamp4;
	}

	@Override
	public String toString() {
		return "ASPShipPosition [imoNo=" + imoNo + ", mmsiNo=" + mmsiNo + ", shipName=" + shipName + ", shipborneEquId="
				+ shipborneEquId + ", lattitude=" + lattitude + ", longitude=" + longitude + ", timestamp1="
				+ timestamp1 + ", timestamp2=" + timestamp2 + ", timestamp3=" + timestamp3 + ", timestamp4="
				+ timestamp4 + "]";
	}

	
}
