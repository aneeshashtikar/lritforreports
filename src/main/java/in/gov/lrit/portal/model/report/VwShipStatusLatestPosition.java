package in.gov.lrit.portal.model.report;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import in.gov.lrit.portal.model.user.PortalUser;


@Entity
@Table(name = "vw_shipstatus_latestposition")
public class VwShipStatusLatestPosition {
	
	@Id
	@Column(name = "vessel_id")
	private BigInteger vesselId;
	
	@Column(name = "imo_no")
	private String imoNo;
	
	@Column(name = "mmsi_no")
	private String mmsiNo;
	
	@Column(name = "ship_name")
	private String shipName;
	
	@Column(name = "call_sign")
	private String callSign;
	
	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "longitude")
	private String longitude;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "timestamp1")
	private Date timestamp1;
	
	@Column(name = "vesselstatus")
	private String status;
	
	@Column(name = "vesselflag")
	private String vesselFlag;
	
	
	@OneToOne
	@JoinColumn(name = "login_id")
	private PortalUser portalUser;
	
	public VwShipStatusLatestPosition() {
		super();
	}

	public VwShipStatusLatestPosition(BigInteger vesselId, String imoNo, String mmsiNo, String shipName,
			String callSign, String latitude, String longitude, Date timestamp1, String status, String vesselFlag,
			PortalUser portalUser) {
		super();
		this.vesselId = vesselId;
		this.imoNo = imoNo;
		this.mmsiNo = mmsiNo;
		this.shipName = shipName;
		this.callSign = callSign;
		this.latitude = latitude;
		this.longitude = longitude;
		this.timestamp1 = timestamp1;
		this.status = status;
		this.vesselFlag = vesselFlag;
		this.portalUser = portalUser;
	}

	public BigInteger getVesselId() {
		return vesselId;
	}

	public void setVesselId(BigInteger vesselId) {
		this.vesselId = vesselId;
	}

	public String getImoNo() {
		return imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public String getMmsiNo() {
		return mmsiNo;
	}

	public void setMmsiNo(String mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getCallSign() {
		return callSign;
	}

	public void setCallSign(String callSign) {
		this.callSign = callSign;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Date getTimestamp1() {
		return timestamp1;
	}

	public void setTimestamp1(Date timestamp1) {
		this.timestamp1 = timestamp1;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PortalUser getPortalUser() {
		return portalUser;
	}

	public void setPortalUser(PortalUser portalUser) {
		this.portalUser = portalUser;
	}
	
	

	@Override
	public String toString() {
		return "VwShipStatusLatestPosition [vesselId=" + vesselId + ", imoNo=" + imoNo + ", mmsiNo=" + mmsiNo
				+ ", shipName=" + shipName + ", callSign=" + callSign + ", latitude=" + latitude + ", longitude="
				+ longitude + ", timestamp1=" + timestamp1 + ", status=" + status + ", portalUser=" + portalUser + "]";
	}
	
	

}
