package in.gov.lrit.portal.model.user;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the portal_users_password database table.
 * 
 */
@Entity
@Table(name="portal_users_password")
//@NamedQuery(name="PortalUsersPassword.findAll", query="SELECT p FROM PortalUsersPassword p")
public class PortalUsersPassword implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id@GeneratedValue
	@Column(name="portal_usr_psw_id")
	private Integer portalUsrPswId;

	@Column(name="expiry_date")
	private Timestamp expiryDate;

	@Column(name="login_id")
	private String loginId;

	private String password;

	public PortalUsersPassword() {
	}

	public Integer getPortalUsrPswId() {
		return this.portalUsrPswId;
	}

	public void setPortalUsrPswId(Integer portalUsrPswId) {
		this.portalUsrPswId = portalUsrPswId;
	}

	public Timestamp getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Timestamp expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}