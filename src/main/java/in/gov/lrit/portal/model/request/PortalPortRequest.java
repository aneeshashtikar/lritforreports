package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the portal_port_request database table.
 * 
 */
@Entity
@Table(name="portal_port_request")
public class PortalPortRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_req_port",sequenceName="seq_portal_port_request", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_req_port")
	@Column(name="request_id")
	private int requestid;

	@Column(name="access_type")
	private int accessType;

	private double distance;

	//@Temporal(TemporalType.DATE)
	@Column(name="end_time")
	private Date endTime;

	@Column(name="imo_no")
	private String imoNo;

	@Transient
	private int requesttypedistance;

	@Column(name="mmsi_no")
	private Integer mmsiNo;

	@Column(name="vessel_name")
	private String vesselName;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "message_id", referencedColumnName = "message_id")
	private PortalRequest portalRequest;

	@Column(name="port_code")
	private String portCode;

	@Column(name="portfacility_code")
	private String portfacilityCode;

	@Column(name="request_type")
	private int requestType;

	//@Temporal(TemporalType.DATE)
	@Column(name="start_time")
	private Date startTime;

	@Transient
	public String userProvider;
	
	public PortalPortRequest() {
	}

	public int getAccessType() {
		return this.accessType;
	}

	public void setAccessType(int accessType) {
		this.accessType = accessType;
	}

	public double getDistance() {
		return this.distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	
	public String getUserProvider() {
		return userProvider;
	}

	public void setUserProvider(String userProvider) {
		this.userProvider = userProvider;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getRequesttypedistance() {
		return requesttypedistance;
	}

	public void setRequesttypedistance(int requesttypedistance) {
		this.requesttypedistance = requesttypedistance;
	}

	public String getImoNo() {
		return imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public Integer getMmsiNo() {
		return mmsiNo;
	}

	public void setMmsiNo(Integer mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}

	public PortalRequest getPortalRequest() {
		return portalRequest;
	}

	public void setPortalRequest(PortalRequest portalRequest) {
		this.portalRequest = portalRequest;
	}

	public String getPortCode() {
		return this.portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getPortfacilityCode() {
		return this.portfacilityCode;
	}

	public void setPortfacilityCode(String portfacilityCode) {
		this.portfacilityCode = portfacilityCode;
	}

	public int getRequestType() {
		return this.requestType;
	}

	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Override
	public String toString() {
		return "PortalPortRequest [requestid=" + requestid + ", accessType=" + accessType + ", distance=" + distance
				+ ", endTime=" + endTime + ", imoNo=" + imoNo + ", requesttypedistance=" + requesttypedistance
				+ ", mmsiNo=" + mmsiNo + ", vesselName=" + vesselName + ", portalRequest=" + portalRequest
				+ ", portCode=" + portCode + ", portfacilityCode=" + portfacilityCode + ", requestType=" + requestType
				+ ", startTime=" + startTime + "]";
	}

}