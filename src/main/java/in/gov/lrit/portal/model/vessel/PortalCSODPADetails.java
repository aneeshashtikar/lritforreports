package in.gov.lrit.portal.model.vessel;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in.gov.lrit.portal.model.user.PortalUser;



@Entity
@Table(name="portal_cso_dpa_details")
@NamedQuery(name="PortalCSODPADetails.findAll", query="SELECT p FROM PortalCSODPADetails p")
public class PortalCSODPADetails implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy="uuid")
	@Column(name="lrit_csoid_seq")
	private String lritCsoIdSeq;
	
	@Column(name="name")
	private String name;
	
	/*@Column(name="companyCode")
	private String companyCode;*/
	
	@Column(name="telephone_office")
	private String telephoneOffice;
	
	@Column(name="emailid")
	private String emailId;
	
	@Column(name="telephone_residence")
	private String telephoneResidence;
	
	@Column(name="mobile_no")
	private String mobileNo;
	
	@CreationTimestamp
	@Column(name="register_date")
	private Timestamp regDate;
	
	@Column(name="deregister_date")
	private Timestamp deRegDate;
	
	@Column(name="type")
	private String type;
	
	/*@Column(name="parent_csoid")
	private String parentCsoId;*/
	
	//added on 11 Nov 2019
	@Column(name="alternative_csoid")
	private String alternateCsoId;
	
	@ManyToOne
	@JoinColumn(name="company_code", nullable=false)
	private PortalShippingCompany shipCom;
	
	@ManyToOne
	@JoinColumn(name="login_id", nullable=false)
	private PortalUser portalUser;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelephoneOffice() {
		return telephoneOffice;
	}

	public void setTelephoneOffice(String telephoneOffice) {
		this.telephoneOffice = telephoneOffice;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getTelephoneResidence() {
		return telephoneResidence;
	}

	public void setTelephoneResidence(String telephoneResidence) {
		this.telephoneResidence = telephoneResidence;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getLritCsoIdSeq() {
		return lritCsoIdSeq;
	}

	public void setLritCsoIdSeq(String lritCsoIdSeq) {
		this.lritCsoIdSeq = lritCsoIdSeq;
	}

	/*public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}*/

	public Timestamp getDeRegDate() {
		return deRegDate;
	}

	public void setDeRegDate(Timestamp deRegDate) {
		this.deRegDate = deRegDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/*public String getCsoId() {
		return csoId;
	}

	public void setCsoId(String csoId) {
		this.csoId = csoId;
	}*/

	public PortalShippingCompany getShipCom() {
		return shipCom;
	}

	public Timestamp getRegDate() {
		return regDate;
	}

	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}

	/*public String getParentCsoId() {
		return parentCsoId;
	}

	public void setParentCsoId(String parentCsoId) {
		this.parentCsoId = parentCsoId;
	}*/
	
	public String getAlternateCsoId() {
		return alternateCsoId;
	}

	public void setAlternateCsoId(String alternateCsoId) {
		this.alternateCsoId = alternateCsoId;
	}
	
	public void setShipCom(PortalShippingCompany shipCom) {
		this.shipCom = shipCom;
	}

	public PortalUser getPortalUser() {
		return portalUser;
	}

	public void setPortalUser(PortalUser portalUser) {
		this.portalUser = portalUser;
	}

	@Override
	public String toString() {
		return "PortalCSODPADetails [lritCsoIdSeq=" + lritCsoIdSeq + ", name=" + name + ", telephoneOffice="
				+ telephoneOffice + ", emailId=" + emailId + ", telephoneResidence=" + telephoneResidence
				+ ", mobileNo=" + mobileNo + ", regDate=" + regDate + ", deRegDate=" + deRegDate + ", type=" + type
				+ ", alternateCsoId=" + alternateCsoId + ", shipCom=" + shipCom + ", portalUser=" + portalUser + "]";
	}

	/*@Override
	public String toString() {
		return "PortalCSODPADetails [lritCsoIdSeq=" + lritCsoIdSeq + ", name=" + name + ", telephoneOffice="
				+ telephoneOffice + ", emailId=" + emailId + ", telephoneResidence=" + telephoneResidence
				+ ", mobileNo=" + mobileNo + ", regDate=" + regDate + ", deRegDate=" + deRegDate + ", type=" + type
				+ ", parentCsoId=" + parentCsoId + ", shipCom=" + shipCom + ", portalUser=" + portalUser + "]";
	}*/

	/*@Override
	public String toString() {
		return "PortalCSODPADetails [lritCsoIdSeq=" + lritCsoIdSeq + ", name=" + name + ", telephoneOffice="
				+ telephoneOffice + ", emailId=" + emailId + ", telephoneResidence=" + telephoneResidence
				+ ", mobileNo=" + mobileNo + ", deRegDate=" + deRegDate + ", type=" + type + ", shipCom=" + shipCom + "]";
	}*/

	/*@Override
	public String toString() {
		return "PortalCSODPADetails [name=" + name + ", telephoneOffice=" + telephoneOffice + ", emailId=" + emailId
				+ ", telephoneResidence=" + telephoneResidence + ", mobileNo=" + mobileNo + ", lritCsoIdSeq="
				+ lritCsoIdSeq + ", companyCode=" + companyCode + ", deRegDate=" + deRegDate + ", type=" + type
				+ ", csoId=" + csoId + "]";
	} */
	
}