package in.gov.lrit.portal.model.vessel;


public interface ShipEquipmentAndVesselInterface {

	public String getShipborneEquipmentId();
	public Integer getDnidNo() ;
	public Integer getMemberNo() ;
	public VesselDetailInterface getVesselDet() ;
}
