package in.gov.lrit.portal.model.user;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "portal_shipping_company") 
//@TableGenerator(name="portal_shipping_company")
public class PortalShippingCompany implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id 
	@Column(name="company_code",length = 50)
	private String companyCode;

	@Column(name="company_name",nullable = false,length = 50)
	private String companyName;
	
	
	
	
	/*@OneToOne(optional=false,fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private PortalCsoDetail portalalternateCsoDetails;
	
	
	
	@OneToOne(optional=false,fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private PortalCsoDetail portalcurrentCsoDetails;*/
	
	/*Changed 1/10/19
	 * Reason: as User will have Shipping company instead of shipping company having user 
	 * 
	 * 
	 * @OneToOne
	@JoinColumn(name="loginId")
    private PortalUser loginId;
	
	
	public PortalUser getLoginId() {
		return loginId;
	}

	public void setLoginId(PortalUser loginId) {
		this.loginId = loginId;
	}*/

	public PortalShippingCompany() {
	}

	public String getCompanyCode() {
		return this.companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}


	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
/*
	public PortalCsoDetail getPortalalternateCsoDetails() {
		return portalalternateCsoDetails;
	}

	public void setPortalalternateCsoDetails(PortalCsoDetail portalalternateCsoDetails) {
		this.portalalternateCsoDetails = portalalternateCsoDetails;
	}

	public PortalCsoDetail getPortalcurrentCsoDetails() {
		return portalcurrentCsoDetails;
	}

	public void setPortalcurrentCsoDetails(PortalCsoDetail portalcurrentCsoDetails) {
		this.portalcurrentCsoDetails = portalcurrentCsoDetails;
	}

	@Override
	public String toString() {
		return "PortalShippingCompany [companyCode=" + companyCode + ", companyName=" + companyName
				+ ", portalalternateCsoDetails=" + portalalternateCsoDetails + ", portalcurrentCsoDetails="
				+ portalcurrentCsoDetails + "]";
	}*/

	@Override
	public String toString() {
		return "PortalShippingCompany [companyCode=" + companyCode + ", companyName=" + companyName +  "]";
	}

	
}
