package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the lrit_latest_ship_position_non_flag database table.
 * 
 */
@Entity
@Table(name="lrit_latest_ship_position_non_flag")
//@NamedQuery(name="LritLatestShipPositionNonFlag.findAll", query="SELECT l FROM LritLatestShipPositionNonFlag l")
public class LritLatestShipPositionNonFlag implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="imo_no")
	private String imoNo;

	@Column(name="asp_id")
	private String aspId;

	private BigDecimal course;

	@Column(name="csp_id")
	private String cspId;

	@Column(name="data_user_provider")
	private String dataUserProvider;

	@Column(name="data_user_requestor")
	private String dataUserRequestor;

	@Column(name="dc_id")
	private String dcId;

	@Column(name="ddpversion_no")
	private String ddpversionNo;

	private BigDecimal latitude;

	private BigDecimal longitude;

	@Column(name="message_id")
	private String messageId;

	@Column(name="message_type")
	private Long messageType;

	@Column(name="mmsi_no")
	private String mmsiNo;

	@Column(name="reference_id")
	private String referenceId;

	@Column(name="ship_borne_equipment_id")
	private String shipBorneEquipmentId;

	@Column(name="ship_name")
	private String vesselName;

	@Column(name="ship_status")
	private String shipStatus;

	@Column(name="ship_type")
	private String shipType;

	private BigDecimal speed;

	private Timestamp timestamp1;

	private Timestamp timestamp2;

	private Timestamp timestamp3;

	private Timestamp timestamp4;

	private Timestamp timestamp5;

	public LritLatestShipPositionNonFlag() {
	}

	public String getImoNo() {
		return this.imoNo;
	}

	public void setImoNo(String imoNo) {
		this.imoNo = imoNo;
	}

	public String getAspId() {
		return this.aspId;
	}

	public void setAspId(String aspId) {
		this.aspId = aspId;
	}

	public BigDecimal getCourse() {
		return this.course;
	}

	public void setCourse(BigDecimal course) {
		this.course = course;
	}

	public String getCspId() {
		return this.cspId;
	}

	public void setCspId(String cspId) {
		this.cspId = cspId;
	}

	public String getDataUserProvider() {
		return this.dataUserProvider;
	}

	public void setDataUserProvider(String dataUserProvider) {
		this.dataUserProvider = dataUserProvider;
	}

	public String getDataUserRequestor() {
		return this.dataUserRequestor;
	}

	public void setDataUserRequestor(String dataUserRequestor) {
		this.dataUserRequestor = dataUserRequestor;
	}

	public String getDcId() {
		return this.dcId;
	}

	public void setDcId(String dcId) {
		this.dcId = dcId;
	}

	public String getDdpversionNo() {
		return this.ddpversionNo;
	}

	public void setDdpversionNo(String ddpversionNo) {
		this.ddpversionNo = ddpversionNo;
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Long getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Long messageType) {
		this.messageType = messageType;
	}

	public String getMmsiNo() {
		return this.mmsiNo;
	}

	public void setMmsiNo(String mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public String getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getShipBorneEquipmentId() {
		return this.shipBorneEquipmentId;
	}

	public void setShipBorneEquipmentId(String shipBorneEquipmentId) {
		this.shipBorneEquipmentId = shipBorneEquipmentId;
	}

	

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}

	public String getShipStatus() {
		return this.shipStatus;
	}

	public void setShipStatus(String shipStatus) {
		this.shipStatus = shipStatus;
	}

	public String getShipType() {
		return this.shipType;
	}

	public void setShipType(String shipType) {
		this.shipType = shipType;
	}

	public BigDecimal getSpeed() {
		return this.speed;
	}

	public void setSpeed(BigDecimal speed) {
		this.speed = speed;
	}

	public Timestamp getTimestamp1() {
		return this.timestamp1;
	}

	public void setTimestamp1(Timestamp timestamp1) {
		this.timestamp1 = timestamp1;
	}

	public Timestamp getTimestamp2() {
		return this.timestamp2;
	}

	public void setTimestamp2(Timestamp timestamp2) {
		this.timestamp2 = timestamp2;
	}

	public Timestamp getTimestamp3() {
		return this.timestamp3;
	}

	public void setTimestamp3(Timestamp timestamp3) {
		this.timestamp3 = timestamp3;
	}

	public Timestamp getTimestamp4() {
		return this.timestamp4;
	}

	public void setTimestamp4(Timestamp timestamp4) {
		this.timestamp4 = timestamp4;
	}

	public Timestamp getTimestamp5() {
		return this.timestamp5;
	}

	public void setTimestamp5(Timestamp timestamp5) {
		this.timestamp5 = timestamp5;
	}

	@Override
	public String toString() {
		return "LritLatestShipPositionNonFlag [imoNo=" + imoNo + ", aspId=" + aspId + ", course=" + course + ", cspId="
				+ cspId + ", dataUserProvider=" + dataUserProvider + ", dataUserRequestor=" + dataUserRequestor
				+ ", dcId=" + dcId + ", ddpversionNo=" + ddpversionNo + ", latitude=" + latitude + ", longitude="
				+ longitude + ", messageId=" + messageId + ", messageType=" + messageType + ", mmsiNo=" + mmsiNo
				+ ", referenceId=" + referenceId + ", shipBorneEquipmentId=" + shipBorneEquipmentId + ", vesselName="
				+ vesselName + ", shipStatus=" + shipStatus + ", shipType=" + shipType + ", speed=" + speed
				+ ", timestamp1=" + timestamp1 + ", timestamp2=" + timestamp2 + ", timestamp3=" + timestamp3
				+ ", timestamp4=" + timestamp4 + ", timestamp5=" + timestamp5 + "]";
	}
	

}