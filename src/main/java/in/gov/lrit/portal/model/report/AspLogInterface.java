package in.gov.lrit.portal.model.report;

import java.io.Serializable;
import java.sql.Timestamp;

public interface AspLogInterface extends Serializable{
	
	public String getMessageId();
	
	public Timestamp getReceivedTime();

}
