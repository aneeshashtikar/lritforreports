package in.gov.lrit.portal.model.request;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the portal_coastal_request database table.
 * 
 */
@Entity
@Table(name="portal_coastal_request")
//@NamedQuery(name="PortalCoastalRequest.findAll", query="SELECT p FROM PortalCoastalRequest p")
public class PortalCoastalRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_req",sequenceName="seq_portal_coastal_request", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_req")
	@Column(name="request_id")
	private int requestid;
	
	@Transient
	public String userProvider;
	
	@Column(name="access_type")
	private String accessType;

	//@DateTimeFormat(pattern = "yyyy-mm-dd")
	//@Temporal(TemporalType.DATE)
	@Column(name="end_time")
	private Date endTime;

	@Column(name="foreign_imo")
	private String vesselImo;

	@Column(name="foreign_mmsi")
	private Integer mmsiNo;

	@Column(name="foreign_vessel_name")
	private String vesselName;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "message_id", referencedColumnName = "message_id")
	private PortalRequest portalRequest;


	@Column(name="request_type")
	private Integer requestType;

	
	//@DateTimeFormat(pattern = "yyyy-mm-dd")
	//@Temporal(TemporalType.DATE)
	@Column(name="start_time")
	private Date startTime;

	public PortalCoastalRequest() {
	}

	public String getAccessType() {
		return this.accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}



	public int getRequestid() {
		return requestid;
	}

	public void setRequestid(int requestid) {
		this.requestid = requestid;
	}

	
	public String getUserProvider() {
		return userProvider;
	}

	public void setUserProvider(String userProvider) {
		this.userProvider = userProvider;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getVesselImo() {
		return vesselImo;
	}

	public void setVesselImo(String vesselImo) {
		this.vesselImo = vesselImo;
	}

	public Integer getMmsiNo() {
		return mmsiNo;
	}

	public void setMmsiNo(Integer mmsiNo) {
		this.mmsiNo = mmsiNo;
	}

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}

	public PortalRequest getPortalRequest() {
		return portalRequest;
	}

	public void setPortalRequest(PortalRequest portalRequest) {
		this.portalRequest = portalRequest;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	

	public Integer getRequestType() {
		return this.requestType;
	}

	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	

}