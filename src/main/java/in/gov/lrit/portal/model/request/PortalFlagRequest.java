package in.gov.lrit.portal.model.request;

import java.io.Serializable;

import javax.persistence.*;


import java.util.Date;


/**
 * The persistent class for the portal_flag_request database table.
 * 
 */
@Entity
@Table(name="portal_flag_request")
public class PortalFlagRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="seq_req_flag",sequenceName="seq_portal_flag_req", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_req_flag")
	@Column(name="request_id")
	private int requestid;
	
	@Transient
	public String imono;
	
	@Column(name="access_type")
	private String accessType;

	//@DateTimeFormat(pattern = "DD/MM/YYYY HH:mm:ss")
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_time")
	private Date endTime;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "message_id", referencedColumnName = "message_id")
	private PortalRequest portalRequest;

	@Column(name="request_type")
	private int requestType;

	//@DateTimeFormat(pattern = "DD/MM/YYYY HH:mm:ss")
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start_time")
	private Date startTime;

	@Column(name="vessel_id")
	private Integer vesselId;

	public PortalFlagRequest() {
	}

	public String getImono() {
		return imono;
	}

	public void setImono(String imono) {
		this.imono = imono;
	}

	public String getAccessType() {
		return this.accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}


	public PortalRequest getPortalRequest() {
		return portalRequest;
	}

	public void setPortalRequest(PortalRequest portalRequest) {
		this.portalRequest = portalRequest;
	}

	public int getRequestType() {
		return this.requestType;
	}

	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Integer getVesselId() {
		return this.vesselId;
	}

	public void setVesselId(Integer vesselId) {
		this.vesselId = vesselId;
	}

	@Override
	public String toString() {
		return "PortalFlagRequest [requestid=" + requestid + ", imono=" + imono + ", accessType=" + accessType
				+ ", endTime=" + endTime + ", portalRequest=" + portalRequest + ", requestType=" + requestType
				+ ", startTime=" + startTime + ", vesselId=" + vesselId + "]";
	}
	

}