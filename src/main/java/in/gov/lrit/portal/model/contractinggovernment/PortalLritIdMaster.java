package in.gov.lrit.portal.model.contractinggovernment;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="portal_lrit_id_master")
//@TableGenerator(name="portal_lrit_id_master")
public class PortalLritIdMaster implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="lrit_id",length = 10)
	@Digits(integer = Integer.MAX_VALUE, fraction = 0)
	@Size(min = 0,max = 4)
	@NotNull
	private String lritId;

	@Column(name="coastal_area",length = 10)
	private String coastalArea;
	
	@NotNull
	@Column(name="country_code",length = 10)
	private String countryCode;
	
	@NotNull
	@Column(name="country_name",length = 100)
	private String countryName;
	
	@NotNull	
	@Column(length=10)
	private String type;
	

		@Column(name="reg_date")
	private Timestamp regDate;
	
	
	public PortalLritIdMaster() {
	}

	public String getLritId() {
		return this.lritId;
	}

	public void setLritId(String lritId) {
		this.lritId = lritId;
	}

	public String getCoastalArea() {
		return this.coastalArea;
	}

	public void setCoastalArea(String coastalArea) {
		this.coastalArea = coastalArea;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Timestamp getRegDate() {
		return regDate;
	}

	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}

	@Override
	public String toString() {
		return "PortalLritIdMaster [lritId=" + lritId + ", coastalArea=" + coastalArea + ", countryCode=" + countryCode
				+ ", countryName=" + countryName + ", type=" + type + ", regDate=" + regDate + "]";
	}

	
}
