package in.gov.lrit.portal.model.role;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.JoinColumn;
//@Entity
@Entity
@Table(name = "portal_roles")
//@TableGenerator(name="portal_roles")
public class PortalRoles implements Serializable{
	/**
	 * 
	 *//*
	private static final long serialVersionUID = 2043081626088011194L;*/

	

	@Id
	@Column(name="role_id" ,length = 50)
	private String roleId;

	@Column(unique=true,length = 50)
	private String rolename;
	
	@Column(nullable = false,length = 50)
	private String shortname;
	
	@Column(length=50)
	
	private String status="active";
	@ManyToMany(fetch = FetchType.EAGER)
	 @JoinTable(name = "portal_role_activities_mapping", 
    joinColumns = { @JoinColumn(name = "role_id") }, 
    inverseJoinColumns = { @JoinColumn(name = "activity_id") })
	Set<PortalActivities> activities;
	@Transient
	public boolean flag=false;
	
	 public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	
	//=new HashSet<PortalActivities>()
	public Set<PortalActivities> getActivities() {
		return activities;
	}

	public void setActivities(Set<PortalActivities> activities) {
		this.activities = activities;
	}

	
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PortalRoles [roleId=" + roleId + ", rolename=" + rolename + ", shortname=" + shortname + ", status="
				+ status + ", activities=" + activities + ", flag=" + flag + "]";
	}

	/*@Override
	public String toString() {
		return "PortalRoles [roleId=" + roleId + ", rolename=" + rolename + ", shortname=" + shortname + ", status="
				+ status + ", flag=" + flag + "]";
	}
*/

	/*@Override
	public String toString() {
		return "PortalRoles [roleId=" + roleId + ", rolename=" + rolename + ", shortname=" + shortname + ", status="
				+ status + ", activities=" + activities + "]";
	}
	*/


}
