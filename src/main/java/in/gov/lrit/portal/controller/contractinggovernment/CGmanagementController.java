package in.gov.lrit.portal.controller.contractinggovernment;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.service.contractinggovernment.ICGManagementService;
import in.gov.lrit.portal.service.user.IUserManagementService;

@Controller
@RequestMapping(value = "/contractinggovernment")
public class CGmanagementController {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CGmanagementController.class);
	@Autowired
	ICGManagementService cgManagementService;
	@Autowired
	IUserManagementService usermanagementService;
	
	
	
	@PreAuthorize(value = "hasAuthority('registercontractinggovernment')")
	// Register cg or sar
	@RequestMapping(method = RequestMethod.GET, value = "/registercontractinggovernment")
	public String createContractingGovernment(Model model) {
		logger.info("Inside createContractingGovernment method");

		PortalLritIdMaster portalLritIdMaster = new PortalLritIdMaster();

		// List<PortalLritIdMaster>
		model.addAttribute("cglist", cgManagementService.getContractingGovList());
		model.addAttribute("portalLritIdMaster", portalLritIdMaster);

		/*return "users/registercontractinggovernment";*/
		return "contractinggovernment/registercontractinggovernment";
	}

	@PreAuthorize(value = "hasAuthority('registercontractinggovernment')")
	// Persist Cg or Sar request
	@RequestMapping(method = RequestMethod.POST, value = "/persistcontractinggovernment")
	public String persistContratingGovernment(
			@Valid @ModelAttribute("portalLritIdMaster") PortalLritIdMaster portalLritIdMaster,
			BindingResult bindingResult, Model model) {
		PortalLritIdMaster portalLritId=null;
		
		
		if (bindingResult.hasErrors()) {

			logger.error("THE BINDING RESULT IS " + bindingResult.toString());
			model.addAttribute("cglist", cgManagementService.getContractingGovList());
			model.addAttribute("errorMsg",
					"Register of CG or SAR failed, kindly check the fields below for more details.");
			/*return "users/registercontractinggovernment";*/
			return "contractinggovernment/registercontractinggovernment";
		}
		logger.info("Inside persistContratingGovernment method" + portalLritIdMaster.toString());
		if (portalLritIdMaster.getType().equalsIgnoreCase("cg")) {
			// verify country_name is unique
			boolean flag = cgManagementService.checkCountryExistance(portalLritIdMaster.getCountryName());
			if (flag) {
				model.addAttribute("errorMsg", "CountryName already exist.");
				model.addAttribute("cglist", cgManagementService.getContractingGovList());
			/*	return "users/registercontractinggovernment";*/
				return "contractinggovernment/registercontractinggovernment";
			}
			//verify country code is unique
			boolean existCountryCode = cgManagementService.checkCountryCode(portalLritIdMaster.getCountryCode());
			if (existCountryCode) {
				model.addAttribute("errorMsg", "CountryCode already exist.");
				model.addAttribute("cglist", cgManagementService.getContractingGovList());
				/*return "users/registercontractinggovernment";*/
				return "contractinggovernment/registercontractinggovernment";
			}
			
		} else if (portalLritIdMaster.getType().equalsIgnoreCase("sar")) {
			// verify coastal_area is unique
			boolean flag = cgManagementService.checkCoastalAreaExistance(portalLritIdMaster.getCoastalArea());
			if (flag) {
				model.addAttribute("errorMsg", "Sar Coastal Area already exist.");
				model.addAttribute("cglist", cgManagementService.getContractingGovList());
				/*return "users/registercontractinggovernment";*/
				return "contractinggovernment/registercontractinggovernment";
			}
		} else {
			logger.info("partalLritIdMaster type is " + portalLritIdMaster.getType());
			model.addAttribute("cglist", cgManagementService.getContractingGovList());
			/*return "users/registercontractinggovernment";*/
			return "contractinggovernment/registercontractinggovernment";
		}

		// verify lritId already present
		boolean flag = cgManagementService.checkLritId(portalLritIdMaster.getLritId());
		if (flag) {
			model.addAttribute("errorMsg", "LritId already exist");
			model.addAttribute("cglist", cgManagementService.getContractingGovList());
			/*return "users/registercontractinggovernment";*/
			return "contractinggovernment/registercontractinggovernment";
		}

		// persist in database
		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp ts = new Timestamp(time);
		logger.info("Current date and time :" + ts);
		portalLritIdMaster.setRegDate(ts);
		cgManagementService.persistContractingGovernment(portalLritIdMaster);
		
		 portalLritId=new PortalLritIdMaster();
		model.addAttribute("successMsg", "Registration of CG / SAR is done successfully");
		model.addAttribute("cglist", cgManagementService.getContractingGovList());
		model.addAttribute("portalLritIdMaster", portalLritId);
		/*return "users/registercontractinggovernment";*/
		return "contractinggovernment/registercontractinggovernment";
	}

	// delete cg
	@PreAuthorize(value = "hasAuthority('deregistercontractinggovernment')")
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST,value = "/deregistercontractinggov")
	public String deregisterContractingGovernment( String lritId, Model model) {
		logger.info("Inside deregister cg " + lritId);
		boolean flag = usermanagementService.getUser(lritId);
		Gson gson = new Gson();
		String json =null;

		if (flag) {
			logger.info("User is already registered to Contacting government");
			model.addAttribute("errorMsg", "Can't delete Contracting Government having lritId =" + lritId
					+ " some user are registered with it. ");
			json = gson.toJson("Can't delete Contracting Government having lritId =" + lritId + " some user are registered with it.");
			return json;
		}

		// Get the details of CG using lritId
		PortalLritIdMaster cgdetails = cgManagementService.getDetails(lritId);
		
		
		
		String countryName = cgdetails.getCountryName();
		// Get List of sar having same country name
		List<PortalLritIdMaster> sarList = cgManagementService.getSarList(countryName);
		// put the data in history table
		cgManagementService.saveCg(cgdetails);
		cgManagementService.saveSar(sarList);
		// delete entry using country name

		logger.info("Country Name " + countryName);
		cgManagementService.deleteCgSar(countryName);
		cgManagementService.deleteCg(lritId);
		logger.info("cg has been deregistered");
		model.addAttribute("successMsg", "Contracting Government has been deregistered");
		json = gson.toJson("Contracting Government has been deregistered");
		
		return json;
	}
	@PreAuthorize(value = "hasAnyAuthority('deregistercontractinggovernment','viewcontractinggovernment','editcontractinggovernment')")
	// View CG
	@RequestMapping(method = RequestMethod.GET, value = "/viewcontractinggovernment")
	public String viewContractingGovernment() {
		return "contractinggovernment/viewcontractinggov";
		/*return "users/viewcontractinggov";*/
	}

	@PreAuthorize(value = "hasAnyAuthority('viewcontractinggovernment','deregistercontractinggovernment','editcontractinggovernment')")
	// Retrieve list of CG
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/getcontractinggovlist")
	public String getContractingGovList() {
		logger.info("Inside getContractingGovList");
		// return usermanagementService.getContractingGovList();
		Gson gson = new Gson();
		String json = gson.toJson(cgManagementService.getContractingGovList());
		logger.info("usermanagementService.getContractingGovList() : " + cgManagementService.getContractingGovList());
		return json;

		// return null;
	}

	// edit cg
	@PreAuthorize(value = "hasAuthority('editcontractinggovernment')")
	@RequestMapping(value = "/editcontractinggov")
	public String editContractingGovernment(@RequestParam("lritId") String lritId, Model model) {

		logger.info("Inside editContractingGovernment method " + lritId);
		boolean flag = cgManagementService.checkLritId(lritId);
		if (!flag) {
			model.addAttribute("errorMsg", "lritId is not present on database");

			model.addAttribute("portalLritIdMaster", new PortalLritIdMaster());
			/*return "users/viewcontractinggov";*/
			return "contractinggovernment/viewcontractinggov";
		}
		PortalLritIdMaster portalLritIdMaster = cgManagementService.getDetails(lritId);
		model.addAttribute("countryName", portalLritIdMaster.getCountryName());
		model.addAttribute("countryCode", portalLritIdMaster.getCountryCode());
		model.addAttribute("portalLritIdMaster", new PortalLritIdMaster());
		logger.info("countryCode" + portalLritIdMaster.getCountryCode());
		/*return "users/viewsar";*/
		return "contractinggovernment/viewsar";
	}

	@PreAuthorize(value = "hasAuthority('editcontractinggovernment')")

	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/getsarlist")
	public String getSarList(String countryName) {
		logger.info("Inside getSarList");
		// return usermanagementService.getContractingGovList();
		Gson gson = new Gson();
		String json = gson.toJson(cgManagementService.getSarList(countryName));
		logger.info("usermanagementService.getSarList() : ");
		return json;
	}

	@PreAuthorize(value = "hasAuthority('editcontractinggovernment')")

	@RequestMapping(method = RequestMethod.POST, value = "/addsar")
	public String persistSar(@Valid @ModelAttribute("portalLritIdMaster") PortalLritIdMaster portalLritIdMaster,
			BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {

			logger.error("THE BINDING RESULT IS " + bindingResult.toString());

			model.addAttribute("errorMsg", " SAR registration failed, kindly check the fields below for more details.");
			model.addAttribute("countryName", portalLritIdMaster.getCountryName());
			model.addAttribute("countryCode", portalLritIdMaster.getCountryCode());
			return "contractinggovernment/viewsar";
			/*return "users/viewsar";*/
		}
		logger.info("Inside persistContratingGovernment method" + portalLritIdMaster.toString());

		// verify coastal_area is unique
		boolean flag = cgManagementService.checkCoastalAreaExistance(portalLritIdMaster.getCoastalArea());
		if (flag) {
			model.addAttribute("errorMsg", "Sar Coastal Area already exist.");
			model.addAttribute("countryName", portalLritIdMaster.getCountryName());
			model.addAttribute("countryCode", portalLritIdMaster.getCountryCode());
			return "contractinggovernment/viewsar";
			/*return "users/viewsar";*/
		}

		// verify lritId already present
		boolean flag1 = cgManagementService.checkLritId(portalLritIdMaster.getLritId());
		if (flag1) {
			model.addAttribute("errorMsg", "LritId already exist");
			model.addAttribute("countryName", portalLritIdMaster.getCountryName());
			model.addAttribute("countryCode", portalLritIdMaster.getCountryCode());
			return "contractinggovernment/viewsar";
			/*return "users/viewsar";*/
		}

		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp regDate = new Timestamp(time);
		portalLritIdMaster.setRegDate(regDate);
		// persist in database
		cgManagementService.persistContractingGovernment(portalLritIdMaster);
		model.addAttribute("successMsg", "Registration of CG / SAR is done successfully");
		model.addAttribute("countryName", portalLritIdMaster.getCountryName());
		model.addAttribute("countryCode", portalLritIdMaster.getCountryCode());
		portalLritIdMaster.setLritId(null);
		portalLritIdMaster.setCoastalArea(null);

	/*	return "users/viewsar";*/
		return "contractinggovernment/viewsar";
	}

	@PreAuthorize(value = "hasAuthority('editcontractinggovernment')")

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/deregistersar")
	public String deleteSar(String lritId, Model model) {
		logger.info("Inside deleteSar lritid " + lritId);
		Gson gson = new Gson();
		String json =null; 
		PortalLritIdMaster portalLritIdMaster = cgManagementService.getDetails(lritId);

		// Check if lritId of sar is assigned to any user
		boolean flag = cgManagementService.checkExistenceSarLritId(lritId);
		if (flag) {
			model.addAttribute("errorMsg",
					" SAR deregistration failed , User is registered with SAR region having lritId=" + lritId + ".");
			model.addAttribute("countryName", portalLritIdMaster.getCountryName());
			model.addAttribute("countryCode", portalLritIdMaster.getCountryCode());
			model.addAttribute("portalLritIdMaster", new PortalLritIdMaster());
			//return "users/viewsar";
			json=gson.toJson(" SAR deregistration failed , User is registered with SAR region having lritId=" + lritId + ".");
			//return " SAR deregistration failed , User is registered with SAR region having lritId=" + lritId + ".";
			return json;
			}

		cgManagementService.saveCg(portalLritIdMaster);
		// Delete sar entry
		cgManagementService.deleteSar(lritId);
		
		model.addAttribute("countryName", portalLritIdMaster.getCountryName());
		model.addAttribute("countryCode", portalLritIdMaster.getCountryCode());
		model.addAttribute("portalLritIdMaster", new PortalLritIdMaster());
		model.addAttribute("successMsg", "SAR Region has been deregistered");
		json=gson.toJson("SAR has been deregistered");
		return json;
	}

	@PreAuthorize(value = "hasAnyAuthority('editcontractinggovernment','createuser')")

	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/getsarauthorities")
	public List<PortalLritIdMaster> getSarAuthorities(@RequestParam("countrylritId") String countrylritId) {
		logger.info("Inside getSarAuthorities Controller method " + countrylritId);
		List<PortalLritIdMaster> portalLritIdMasters = cgManagementService.getSarAuthorityDetails(countrylritId);
		logger.info("Out getSarAuthorities Controller method " + portalLritIdMasters.toString());
		return portalLritIdMasters;

	}
}


