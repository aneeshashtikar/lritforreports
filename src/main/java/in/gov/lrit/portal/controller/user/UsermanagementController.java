/**

 * @UsermanagementController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author   Bhavika Ninawe
 * @version 1.0
 * UsermanagementController used to handle all the user based access
 */
package in.gov.lrit.portal.controller.user;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.google.gson.Gson;
import in.gov.lrit.portal.CommonUtil.Encrypt;
import in.gov.lrit.portal.CommonUtil.Utility;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.role.PortalRoles;
import in.gov.lrit.portal.model.user.ChangePassword;
import in.gov.lrit.portal.model.user.PortalCsoDpaDetail;
import in.gov.lrit.portal.model.user.PortalUser;
import in.gov.lrit.portal.model.user.PortalUserDTO;
import in.gov.lrit.portal.model.user.PortalUserInterface;
import in.gov.lrit.portal.model.user.PortalUsersRoleMapping;
import in.gov.lrit.portal.model.user.PortalUsersRoleMappingPK;
import in.gov.lrit.portal.service.contractinggovernment.ICGManagementService;
import in.gov.lrit.portal.service.user.IRoleManagementService;
import in.gov.lrit.portal.service.user.IUserManagementService;
import in_.gov.lrit.session.UserSessionInterface;

@Controller
@RequestMapping(value = "/users")
public class UsermanagementController {
	@Autowired
	IUserManagementService usermanagementService;
	@Autowired
	ICGManagementService cgManagementService;
	@Autowired
	IRoleManagementService rolemanagementService;

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(UsermanagementController.class);

	// create user
	/**
	 * This method returns a page createuser page where role and contacting
	 * government list is already populated
	 * 
	 * @return String createuser
	 */
	@PreAuthorize(value = "hasAuthority('createuser')")
	@RequestMapping(method = RequestMethod.GET, value = "/createuser")
	public String createUser(Model model) {
		logger.info("Inside createUser method");
		List<PortalRoles> portalRoles = new ArrayList<>();
		List<PortalLritIdMaster> portalLritIdMasterslist = new ArrayList<>();
		PortalUserDTO portalUserDTO = new PortalUserDTO();
		portalRoles = rolemanagementService.getRoles("active");
		logger.debug("Size of active roles  " + portalRoles.size());

		logger.debug("Info of active list of roles " + portalRoles.toString());

		model.addAttribute("portalroles", portalRoles);
		portalLritIdMasterslist = cgManagementService.getContractingGovList();
		logger.debug("Size of contracting government  " + portalLritIdMasterslist.size());

		logger.debug("Info of active list of contracting government " + portalLritIdMasterslist.toString());
		// logger.info("contracting government list :" +
		// portalLritIdMasterslist.toString());
		model.addAttribute("listContractingGov", portalLritIdMasterslist);

		List<PortalCsoDpaDetail> csoList = new ArrayList<>();
		PortalCsoDpaDetail portalCsoDpaDetail = new PortalCsoDpaDetail();

		csoList.add(portalCsoDpaDetail);

		List<PortalCsoDpaDetail> dpaList = new ArrayList<>();
		dpaList.add(portalCsoDpaDetail);
		portalUserDTO.setCurrentcsoList(csoList);
		portalUserDTO.setCsolength(1);
		portalUserDTO.setDpalength(1);
		portalUserDTO.setAlternatecsoList(csoList);
		model.addAttribute("portaluserdto", portalUserDTO);
		model.addAttribute("currentcsoList", csoList);
		model.addAttribute("dpaList", dpaList);

		logger.info("Getting Out of createUser method ");
		return "users/createuser";
	}

	/**
	 * This method persist the user and return success/error message to user
	 * 
	 * @return String createuser or return csoalternatecsomapping page
	 */
	@PreAuthorize(value = "hasAuthority('createuser')")
	@RequestMapping(method = RequestMethod.POST, value = "/persistuser")
	public String persistUser(@Valid @ModelAttribute("portaluserdto") PortalUserDTO portalUserDTO,
			@RequestParam("uploadedFile") MultipartFile uploadedFile, HttpServletRequest request, Model model,
			BindingResult bindingResult) {
		boolean emailFlag = false;
		Set<PortalUsersRoleMapping> userRoleMapping = new HashSet<>();
		PortalUsersRoleMapping portalUsersRoleMapping = null;
		PortalUsersRoleMappingPK mappingPK = null;

		for (Object object : bindingResult.getAllErrors()) {
			if (object instanceof FieldError) {
				FieldError fieldError = (FieldError) object;

				System.out.println(fieldError.getCode());
			}

			if (object instanceof ObjectError) {
				ObjectError objectError = (ObjectError) object;

				System.out.println(objectError.getCode());
			}
		}
		logger.info("Inside persistUser Controller " + portalUserDTO.toString());
		List<PortalRoles> portalRoles = new ArrayList<>();

		List<PortalLritIdMaster> portalLritIdMasterslist = new ArrayList<>();
		portalRoles = rolemanagementService.getRoles("active");

		portalLritIdMasterslist = cgManagementService.getContractingGovList();
		logger.info("cg list :" + portalLritIdMasterslist.toString());

		List<PortalCsoDpaDetail> csoList = new ArrayList<>();
		PortalCsoDpaDetail portalCsoDpaDetail = new PortalCsoDpaDetail();

		csoList.add(portalCsoDpaDetail);

		List<PortalCsoDpaDetail> dpaList = new ArrayList<>();
		dpaList.add(portalCsoDpaDetail);

		byte[] uploadDocument = null;

		PortalUser portalUser = portalUserDTO.getPortaluser();

		// check user already exist
		boolean flag = usermanagementService.checkUserExist(portalUser.getLoginId());
		if (flag) {
			logger.info("LoginId already exist in database " + portalUser.getLoginId());
			model.addAttribute("errorMsg", "LoginId already exist");
			portalUserDTO.setCurrentcsoList(csoList);
			portalUserDTO.setDpaList(dpaList);
			portalUserDTO.setCsolength(1);
			portalUserDTO.setDpalength(1);
			model.addAttribute("portaluserdto", portalUserDTO);
			logger.info("portalUserDTO before sending to create user page :" + portalUserDTO);
			model.addAttribute("currentcsoList", csoList);
			model.addAttribute("portalroles", portalRoles);
			model.addAttribute("dpaList", dpaList);
			model.addAttribute("listContractingGov", portalLritIdMasterslist);
			return "users/createuser";
			// return "acknowledgement";
		}
		logger.info("LoginId doesn't exist in database " + portalUser.getLoginId());
		// Convert MultipartFile uploaded file in bytes
		try {
			uploadDocument = uploadedFile.getBytes();
		} catch (IOException e) {
			logger.error("Error generated while converting multipart file into bytes " + e.getMessage());
			e.printStackTrace();
		}
		logger.info("File converted from multipart to bytes successfully");
		portalUser.setUploadedFilePath(uploadDocument);
		portalUserDTO.setPortaluser(portalUser);

		// Convert String of roleid into mapping pojo

		List<String> roles = portalUserDTO.getRoles();
		for (String role : roles) {
			portalUsersRoleMapping = new PortalUsersRoleMapping();
			mappingPK = new PortalUsersRoleMappingPK();
			mappingPK.setLoginId(portalUserDTO.getPortaluser().getLoginId());
			mappingPK.setRoleId(role);
			portalUsersRoleMapping.setId(mappingPK);
			logger.debug("portalUsersRoleMapping pojo " + portalUsersRoleMapping.toString());

			userRoleMapping.add(portalUsersRoleMapping);
		}

		usermanagementService.persistUser(portalUserDTO, userRoleMapping);
		logger.info("User is persisted in database successfully");

		if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_SC")) {
			logger.info("Category of user is shipping company");
			// open mapping jsp. status of user will be incomplete
			List<PortalCsoDpaDetail> portalCsoDpaDetailsList = new ArrayList<>();
			portalCsoDpaDetailsList = usermanagementService.getCsoList(portalUser.getLoginId());
			logger.info("length of list cso  " + portalCsoDpaDetailsList.size());
			logger.info("cso List " + portalCsoDpaDetailsList.toString());
			model.addAttribute("currentcsoList", portalCsoDpaDetailsList);
			model.addAttribute("portaldto", portalUserDTO);
			logger.info("Displaying csoalternatecsomapping jsp ");
			return "users/csoalternatecsomapping";

		}
		try {
			sendmail(portalUserDTO.getPortaluser());
		} catch (Exception ex) {
			logger.error("Exception while sending mail");
			ex.printStackTrace();
			logger.error("Error occured : " + ex.getMessage(), ex);
			emailFlag = true;
			model.addAttribute("errorMsg",
					"User has been registered, but the e-mail notification could not be sent to the registered emailid");
		}
		if (!emailFlag) {
			logger.info("mail is send successfully ");
			model.addAttribute("successMsg",
					"User has been registered successfully and mail is sent to the registered emailid");
		}

		// after registering user successfully redirect user to create user again with
		// empty form instead of ack
		portalUserDTO = new PortalUserDTO();
		portalUserDTO.setCsolength(1);
		portalUserDTO.setDpalength(1);
		model.addAttribute("portaluserdto", portalUserDTO);
		model.addAttribute("currentcsoList", csoList);
		model.addAttribute("portalroles", portalRoles);
		model.addAttribute("dpaList", dpaList);
		model.addAttribute("listContractingGov", portalLritIdMasterslist);

		logger.info("List of active roles " + portalRoles.toString());
		logger.info("List of contracting government " + portalLritIdMasterslist.toString());
		return "users/createuser";

	}

	/**
	 * This method persist cso mapping when user assign alternate cso to cso after
	 * creating user
	 * 
	 * @return String createuser or return csoalternatecsomapping page
	 */
	@PreAuthorize(value = "hasAuthority('createuser')")
	@RequestMapping(method = RequestMethod.POST, value = "/persistcsomapping")
	public String persistCsoAcsoMapping(@Valid PortalUserDTO portalUserDTO, Model model) {
		logger.info("Inside persistCsoAcsoMapping method . portaluserdto : " + portalUserDTO.toString());
		boolean flag = false;
		boolean emailFlag = false;
		List<PortalCsoDpaDetail> csoList = portalUserDTO.getCurrentcsoList();

		for (PortalCsoDpaDetail cso : csoList) {
			if (!cso.getParentCsoid().isEmpty()) {
				flag = true;
				logger.info("Flag is set true ");
				break;
			}
		}

		if (flag == false) {
			logger.info("Flag is false");
			logger.info("No cso is mapped with alternate cso ");
			model.addAttribute("currentcsoList", csoList);
			model.addAttribute("portaldto", portalUserDTO);
			model.addAttribute("errorMsg", "Minimum one cso should have alternate cso ");
			return "users/csoalternatecsomapping";

		}
		usermanagementService.persistAlternateCsoId(portalUserDTO.getCurrentcsoList());
		logger.info("After persisting cso and alternate cso mapping in database");

		usermanagementService.updateStatus(portalUserDTO.getPortaluser().getLoginId(), "registered");
		logger.info("After updating status of user from incomplete to registered");

		PortalUser portalUser = usermanagementService.getPortalUser(portalUserDTO.getPortaluser().getLoginId());
		logger.info("Retrieving user details using loginid . Prtal user " + portalUser.toString());

		try {
			sendmail(portalUser);
		} catch (Exception ex) {
			logger.error("Exception while sending mail");
			ex.printStackTrace();
			logger.error("Error " + ex.getMessage(), ex);
			emailFlag = true;
			model.addAttribute("errorMsg",
					"User has been registered, but the e-mail notification could not be sent to the registered emailid");
		}
		if (!emailFlag) {
			logger.info("Mail is send to regsitered user emailid");
			model.addAttribute("successMsg",
					"User has been registered successfully and mail is sent to the registered emailid");
		}

		List<PortalRoles> portalRoles = new ArrayList<>();
		portalUserDTO = new PortalUserDTO();
		List<PortalLritIdMaster> portalLritIdMasterslist = new ArrayList<>();
		portalRoles = rolemanagementService.getRoles("active");
		logger.info("size of active roles  : " + portalRoles.size());
		logger.debug("List of active roles : " + portalRoles.toString());

		portalLritIdMasterslist = cgManagementService.getContractingGovList();
		logger.info("Size of contracting government : " + portalLritIdMasterslist.size());
		logger.info("List of contracting government : " + portalLritIdMasterslist.toString());

		csoList = new ArrayList<>();
		PortalCsoDpaDetail portalCsoDpaDetail = new PortalCsoDpaDetail();

		csoList.add(portalCsoDpaDetail);

		List<PortalCsoDpaDetail> dpaList = new ArrayList<>();
		dpaList.add(portalCsoDpaDetail);

		portalUserDTO.setCurrentcsoList(csoList);
		portalUserDTO.setDpaList(csoList);
		portalUserDTO.setCsolength(1);
		portalUserDTO.setDpalength(1);
		model.addAttribute("portaluserdto", portalUserDTO);
		model.addAttribute("currentcsoList", csoList);
		model.addAttribute("portalroles", portalRoles);
		model.addAttribute("dpaList", dpaList);
		model.addAttribute("listContractingGov", portalLritIdMasterslist);
		logger.info("Dispalying createuser page ");
		return "users/createuser";
	}

	// edit user
	/**
	 * This method fetch the data of users and return the edit user jsp 
	 * 
	 * @return String jsp
	 */
	@PreAuthorize(value = "hasAuthority('edituser')")
	@RequestMapping(method = RequestMethod.GET, value = "/edituser")
	public String editUser(@RequestParam("loginId") String loginId, Model model) {
		logger.info("Inside edit user " + loginId);
		String page = null;
		PortalUserDTO portalUserDTO = usermanagementService.getUserDetails(loginId);
		logger.info("potalUserDTO " + portalUserDTO.toString());
		if (portalUserDTO.getPortaluser() == null) {
			model.addAttribute("errorMsg", "Enter valid loginid.");
			return "users/viewusers";
		}
		List<PortalRoles> portalRoles = rolemanagementService.getRoles("active");
		// Check category
		if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_SC")) {

			// get cso details of shipping company

			List<PortalCsoDpaDetail> currentCsoList = portalUserDTO.getCurrentcsoList();
			model.addAttribute("csolength", currentCsoList.size());
			portalUserDTO.setCsolength(currentCsoList.size());

			List<PortalCsoDpaDetail> dpaList = portalUserDTO.getDpaList();
			portalUserDTO.setDpalength(dpaList.size());
			model.addAttribute("currentcsoList", currentCsoList);
			model.addAttribute("dpaList", dpaList);
			model.addAttribute("dpalength", dpaList.size());
			model.addAttribute("category", "Shipping Company");
			model.addAttribute("portaluserdto", portalUserDTO);
			page = "users/editusershippingcompany";

		} else {
			if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_NAVY"))
				model.addAttribute("category", "Navy");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_PA"))
				model.addAttribute("category", "PortAuthority");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_DGS"))
				model.addAttribute("category", "DGS");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_CG"))
				model.addAttribute("category", "CoastGuard");
			model.addAttribute("portaluserdto", portalUserDTO);
			page = "users/edituser";
		}

		// Set role
		for (PortalRoles role : portalRoles) {

			for (PortalRoles assignedrole : portalUserDTO.getPortaluser().getRoles()) {

				if (role.getRoleId().equals(assignedrole.getRoleId())) {

					role.setFlag(true);
					break;
				}
			}
		}

		model.addAttribute("portalRoles", portalRoles);
		logger.info("Outside edit user");
		return page;
	}

	// persisting edit user cso and aletrnate cso mapping
	/**
	 * This method used to persist edit user cso and alternate cso mapping
	 * 
	 * @return String acknowledgement
	 */
	@PreAuthorize(value = "hasAuthority('edituser')")
	@RequestMapping(method = RequestMethod.POST, value = "/persisteditcsomapping")
	public String persistEditCsoAcsoMapping(@Valid PortalUserDTO portalUserDTO, Model model) {
		logger.info("inside portaluserdto " + portalUserDTO.toString());
		boolean flag = false;
		List<PortalCsoDpaDetail> csoList = portalUserDTO.getCurrentcsoList();
		for (PortalCsoDpaDetail cso : csoList) {

			if (!cso.getParentCsoid().isEmpty()) {
				flag = true;
				logger.info("Flag is set true ");
				break;
			}
		}
		if (flag == false) {
			logger.info("Flag is false");
			model.addAttribute("currentcsoList", csoList);
			model.addAttribute("portaldto", portalUserDTO);
			model.addAttribute("errorMsg", "Minimum one alternate cso required");
			return "users/editcsoalternatecsomapping";

		}

		usermanagementService.persistAlternateCsoId(portalUserDTO.getCurrentcsoList());

		usermanagementService.updateStatus(portalUserDTO.getPortaluser().getLoginId(), "registered");
		model.addAttribute("successMsg", "Registered user is successfully edited.");
		return "acknowledgement";

	}

	// When system incharge edits other users profile
	/**
	 * This method used to persist edit user 
	 * 
	 * @return String acknowledgement
	 */
	@PreAuthorize(value = "hasAuthority('edituser')")
	@RequestMapping(method = RequestMethod.POST, value = "/persistedituser")
	public String persistEditUser(@Valid @ModelAttribute("portaluserdto") PortalUserDTO portalUserDTO,
			@RequestParam("uploadedFile") MultipartFile uploadedFile, Model model) {
		PortalUsersRoleMapping portalUsersRoleMapping = null;
		PortalUsersRoleMappingPK mappingPK = null;
		Set<PortalUsersRoleMapping> userRoleMapping = new HashSet<>();
		logger.info("Inside persistEditUser controller " + portalUserDTO.toString());
		logger.info(uploadedFile.getContentType() + uploadedFile.getName() + "//" + uploadedFile.getOriginalFilename()
				+ "//" + uploadedFile.getSize() + "//");

		byte[] uploadDocument = null;
		if (!uploadedFile.isEmpty()) {
			try {
				logger.info("uploaded file is notempty");
				uploadDocument = uploadedFile.getBytes();
				logger.info("uploadDocument" + uploadDocument);
				PortalUser portalUser = portalUserDTO.getPortaluser();
				portalUser.setUploadedFilePath(uploadDocument);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		PortalUser portalUser = portalUserDTO.getPortaluser();
		List<String> roles = portalUserDTO.getRoles();
		if (roles != null && roles.size() > 0) {

			for (String role : roles) {
				portalUsersRoleMapping = new PortalUsersRoleMapping();
				mappingPK = new PortalUsersRoleMappingPK();
				mappingPK.setLoginId(portalUserDTO.getPortaluser().getLoginId());
				mappingPK.setRoleId(role);
				portalUsersRoleMapping.setId(mappingPK);
				// logger.info("Create role_user mapping pojo " +
				// portalUsersRoleMapping.toString());

				userRoleMapping.add(portalUsersRoleMapping);
			}
		}
		portalUserDTO.setPortaluser(portalUser);

		usermanagementService.updateUser(portalUserDTO, userRoleMapping);

		// If user is system incharge send mail
		if (portalUser.getCategory().equals("USER_CATEGORY_SC")) {
			model.addAttribute("portalcsoList", portalUserDTO.getCurrentcsoList());
			List<PortalCsoDpaDetail> portalCsoDpaDetailsList = new ArrayList<>();
			portalCsoDpaDetailsList = usermanagementService.getCsoList(portalUser.getLoginId());
			logger.info("length " + portalCsoDpaDetailsList.size());
			for (PortalCsoDpaDetail cso : portalCsoDpaDetailsList) {
				logger.info("cso " + cso.toString());
			}
			model.addAttribute("currentcsoList", portalCsoDpaDetailsList);
			model.addAttribute("portaldto", portalUserDTO);

			return "users/editcsoalternatecsomapping";

		}
		model.addAttribute("successMsg", "User has been edited");
	

		return "acknowledgement";

	}

	// persisting edit user cso and aletrnate cso mapping
	/**
	 * This method used to persist edit user cso and alternate cso mapping where user changes its own profile
	 * 
	 * @return String /users/userprofile
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/persisteditcsomappinguserprofile")
	public String persistEditCsoAcsoMappingUserProfile(@Valid PortalUserDTO portalUserDTO, Model model) {
		logger.info("inside portaluserdto " + portalUserDTO.toString());
		boolean flag = false;
		List<PortalCsoDpaDetail> csoList = portalUserDTO.getCurrentcsoList();
		for (PortalCsoDpaDetail cso : csoList) {

			if (!cso.getParentCsoid().isEmpty()) {
				flag = true;
				logger.info("Flag is set true ");
				break;
			}
		}
		if (flag == false) {
			logger.info("Flag is false");
			model.addAttribute("currentcsoList", csoList);
			model.addAttribute("portaldto", portalUserDTO);
			model.addAttribute("errorMsg", "Minimum one alternate cso required");
			return "users/editcsoalternatecsomapping";

		}

		usermanagementService.persistAlternateCsoId(portalUserDTO.getCurrentcsoList());
		// usermanagementService.updateStatus(portalUserDTO.getPortaluser().getLoginId(),
		// "registered");
		// PortalUser portalUser =
		// usermanagementService.getPortalUser(portalUserDTO.getPortaluser().getLoginId());
		// sendmail(portalUser);
		model.addAttribute("successMsg", "Registered user is successfully edited.");
	
		portalUserDTO = usermanagementService.getUserDetails(portalUserDTO.getPortaluser().getLoginId());
		// Check category
		if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_SC")) {

			// get cso details of shipping company

			List<PortalCsoDpaDetail> currentCsoList = portalUserDTO.getCurrentcsoList();
			model.addAttribute("csolength", currentCsoList.size());
			portalUserDTO.setCsolength(currentCsoList.size());

			List<PortalCsoDpaDetail> dpaList = portalUserDTO.getDpaList();
			portalUserDTO.setDpalength(dpaList.size());
			model.addAttribute("currentcsoList", currentCsoList);

			model.addAttribute("dpaList", dpaList);
			model.addAttribute("dpalength", dpaList.size());
			model.addAttribute("category", "Shipping Company");
			model.addAttribute("portaluserdto", portalUserDTO);
			// page = "/users/userprofile";

		} else {
			if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_NAVY"))
				model.addAttribute("category", "Navy");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_PA"))
				model.addAttribute("category", "PortAuthority");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_DGS"))
				model.addAttribute("category", "DGS");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_CG"))
				model.addAttribute("category", "CoastGuard");
			model.addAttribute("portaluserdto", portalUserDTO);
			// page = "/users/userprofile";
		}
		return "/users/userprofile";

	}

	/**
	 * This method returns page of viewusers
	 * 
	 * @return String users/viewusers
	 */
	@PreAuthorize(value = "hasAnyAuthority('edituser','viewuser','deleteuser')")
	@RequestMapping(method = RequestMethod.GET, value = "/viewuser")
	public String viewUser(Model model) {
		logger.info("Inside view user");
		return "users/viewusers";
	}

	// Get user list according to status register, deregister,incomplete
	/**
	 * This method list of users
	 * 
	 * @return String users/viewusers
	 */
	@PreAuthorize(value = "hasAnyAuthority('edituser','viewuser','deleteuser')")
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/getuserlist")
	public Collection<PortalUserInterface> getUserList(String Status) {
		logger.info("Inside getUserList   " + Status);

		Collection<PortalUserInterface> portalList = usermanagementService.getUserUsingStatus(Status);

		return portalList;
	}

	// Get All user list
	@PreAuthorize(value = "hasAnyAuthority('edituser','viewuser','deleteuser')")
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/getalluserlist")
	public Collection<PortalUserInterface> getAllUserList(String Status) {
		logger.info("Inside getUserList   " + Status);
		Collection<PortalUserInterface> portalList = usermanagementService.getAllUser();
		if(portalList!=null) {
			logger.info("size of List "+ portalList.size());
			for(PortalUserInterface list :portalList) {
				logger.debug("portalusers "+list.toString());
			}
		}
		return portalList;
	}

	@PreAuthorize(value = "hasAnyAuthority('viewuser','edituser','deleteuser')")

	@RequestMapping(method = RequestMethod.GET, value = "/viewuserdetails")
	public String viewUserDetails(@RequestParam("loginId") String loginId, Model model) {
		logger.info("Inside view user loginId: " + loginId);
		if (loginId == null) {
			logger.info("loginId is null " + loginId);
			model.addAttribute("errorMsg", "Invalid loginId");
			return "users/viewusers";
		}
		PortalUserDTO portalUserDto = usermanagementService.getUserDetails(loginId);

		if (portalUserDto == null) {
			logger.info("loginId is null " + loginId);
			model.addAttribute("errorMsg", "Invalid loginId");
			return "users/viewusers";
		}
		logger.info("portal dto " + portalUserDto.toString());
		if (portalUserDto.getPortaluser().getCategory().equals("USER_CATEGORY_NAVY"))
			model.addAttribute("category", "Navy");
		else if (portalUserDto.getPortaluser().getCategory().equals("USER_CATEGORY_PA"))
			model.addAttribute("category", "PortAuthority");
		else if (portalUserDto.getPortaluser().getCategory().equals("USER_CATEGORY_DGS"))
			model.addAttribute("category", "DGS");
		else if (portalUserDto.getPortaluser().getCategory().equals("USER_CATEGORY_CG"))
			model.addAttribute("category", "CoastGuard");
		else if (portalUserDto.getPortaluser().getCategory().equals("USER_CATEGORY_SC"))
			// get cso details of shipping company
			model.addAttribute("category", "Shipping Company");
		List<PortalRoles> portalRoles = rolemanagementService.getRoles("active");
		for (PortalRoles role : portalRoles) {

			for (PortalRoles assignedrole : portalUserDto.getPortaluser().getRoles()) {

				if (role.getRoleId().equals(assignedrole.getRoleId())) {

					role.setFlag(true);
					break;
				}
			}
		}

		model.addAttribute("portalRoles", portalRoles);
		model.addAttribute("portaluserdto", portalUserDto);
		model.addAttribute("loginId", portalUserDto.getPortaluser().getLoginId());

		if (portalUserDto.getPortaluser().getCategory().equals("USER_CATEGORY_SC")) {
			List<PortalCsoDpaDetail> currentCsoList = portalUserDto.getCurrentcsoList();
			List<PortalCsoDpaDetail> dpaList = portalUserDto.getDpaList();
			model.addAttribute("dpaList", dpaList);
			model.addAttribute("currentcsoList", currentCsoList);
			return "users/viewusersdetailsshipingcompany";
		}
		return "users/viewusersdetails";
	}
	
	/**
	 * This method returns registration form of user using loginId
	 * 
	 * @return ResponseEntity
	 */
	@PreAuthorize(value = "hasAnyAuthority('viewuser','edituser')")
	@RequestMapping(value = "/download", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InputStreamResource> download(@RequestParam("loginId") String loginId, HttpServletResponse response) throws IOException {
		logger.info("Inside download file. LoginId= "+loginId);
		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Access-Control-Allow-Methods", "GET, POST");
		headers.add("Access-Control-Allow-Headers", "Content-Type");
		headers.add("Content-Disposition", "inline; filename=registrationForm.pdf");
		PortalUserDTO portalUserDTO = usermanagementService.getUserDetails(loginId);
		//if user is deregistered
		logger.info("Portal user details "+portalUserDTO.toString());
		if(portalUserDTO.getPortaluser()==null) {
		logger.info("Portaluser  is null "); 
		return  new ResponseEntity ("No document uploaded!!", HttpStatus.OK);
		}
			
		byte[] file = portalUserDTO.getPortaluser().getUploadedFilePath();
		if(file==null) {
		logger.info("file is null");
		return  new ResponseEntity ("No document uploaded!!", HttpStatus.OK);
		}
		
		return  new ResponseEntity<InputStreamResource>(new InputStreamResource(new ByteArrayInputStream(file)),headers,HttpStatus.OK);
	}		
	

	/**
	 * This method deregisterUser used for deregister user from system using loginId 
	 * 
	 * @return Json
	 */
	@PreAuthorize(value = "hasAuthority('deleteuser')")
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/deregisteruser")
	public String deregisterUser(String loginId, Model model, HttpServletResponse response) {
		logger.info("Inside deregisterUser method. loginId " + loginId);
		Gson gson = new Gson();
		String json = null;
		// getDetails of user using loginId
		PortalUserDTO portalUserDTO = usermanagementService.getUserDetails(loginId);
		logger.info("PortalDTO " + portalUserDTO.toString());
		if (portalUserDTO.getPortaluser() == null) {
			logger.info("User is already inactivated");
			json = gson.toJson("User is already inactivated");
			return json;
		} else {
			PortalUser portalUser = portalUserDTO.getPortaluser();
			// Check category
			if (portalUser.getCategory().equals("USER_CATEGORY_SC")) {
				logger.info("Category " + portalUser.getCategory());
				logger.info("Company Code " + portalUserDTO.getPortaluser().getPortalShippingCompany().toString());
				boolean flag = usermanagementService.getVessel(portalUserDTO.getPortaluser().getLoginId());
				logger.info("flag of companycode " + flag);

				if (flag) {
					// shipping company user cannot be deleted as user is connected to vessel
					json = gson.toJson(
							"User can not be deregistered it is associated with some ships. Kindly deregister ships");
					return json;
				} else { // shipping company user can be deleted as user doesn't have relation with
							// vessel
					int f = usermanagementService.deregisterUser(loginId);
					logger.info("flag " + f);
				}

			} else {
				int flag = usermanagementService.deregisterUser(loginId);
				logger.info("flag " + flag);

			}

		}
		json = gson.toJson("User has deregistered successfully");
		return json;

	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/checkLoginId")
	public String checkLoginId(@RequestParam String loginId) {
		logger.info("Inside checkLoginID " + loginId);
		boolean flag = usermanagementService.checkUserExist(loginId);

		Gson gson = new Gson();
		String json = gson.toJson(flag);
		return json;
	}

	// When user updates its own profile
	@RequestMapping(method = RequestMethod.POST, value = "/persistedituserprofile")
	public String persistEditUserProfile(@Valid @ModelAttribute("portaluserdto") PortalUserDTO portalUserDTO,
			@RequestParam("uploadedFile") MultipartFile uploadedFile, Model model) {
		PortalUsersRoleMapping portalUsersRoleMapping = null;
		PortalUsersRoleMappingPK mappingPK = null;
		Set<PortalUsersRoleMapping> userRoleMapping = new HashSet<>();
		logger.info("Inside persistEditUserProfile controller " + portalUserDTO.toString());
		logger.info(uploadedFile.getContentType() + uploadedFile.getName() + "//" + uploadedFile.getOriginalFilename()
				+ "//" + uploadedFile.getSize() + "//");

		byte[] uploadDocument = null;
		if (!uploadedFile.isEmpty()) {
			try {
				logger.info("uploaded file is notempty");
				uploadDocument = uploadedFile.getBytes();
				logger.info("uploadDocument" + uploadDocument);
				PortalUser portalUser = portalUserDTO.getPortaluser();
				portalUser.setUploadedFilePath(uploadDocument);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		PortalUser portalUser = portalUserDTO.getPortaluser();
	//	List<String> roles = portalUserDTO.getRoles();

		portalUserDTO.setPortaluser(portalUser);

		usermanagementService.updateUser(portalUserDTO, userRoleMapping);

		// If user is system incharge send mail
		if (portalUser.getCategory().equals("USER_CATEGORY_SC")) {
			model.addAttribute("portalcsoList", portalUserDTO.getCurrentcsoList());
			List<PortalCsoDpaDetail> portalCsoDpaDetailsList = new ArrayList<>();
			portalCsoDpaDetailsList = usermanagementService.getCsoList(portalUser.getLoginId());
			logger.info("length " + portalCsoDpaDetailsList.size());
			for (PortalCsoDpaDetail cso : portalCsoDpaDetailsList) {
				logger.info("cso " + cso.toString());
			}
			model.addAttribute("currentcsoList", portalCsoDpaDetailsList);
			model.addAttribute("portaldto", portalUserDTO);

			return "users/editcsoalternatecsomappinguserprofile";

		}
		model.addAttribute("successMsg", "User has been edited");
		// return "acknowledgement";
//if roles is null direct it to user/profile 

		logger.info("checking role size");
		// check category other than shiping company
		if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_NAVY"))
			model.addAttribute("category", "Navy");
		else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_PA"))
			model.addAttribute("category", "PortAuthority");
		else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_DGS"))
			model.addAttribute("category", "DGS");
		else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_CG"))
			model.addAttribute("category", "CoastGuard");
		model.addAttribute("portaluserdto", portalUserDTO);

		return "/users/userprofile";

	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/checkCompanyCode")
	public String checkCompanyCode(@RequestParam(value = "companyCode") String companyCode) {
		logger.info("Company code " + companyCode);
		String companyName = usermanagementService.checkCompanyCodeExist(companyCode);
		if (companyName != null) {
			logger.info("Company Name is not null. CompanyName "+companyName);
		}
		/*Gson gson = new Gson();
		String json = gson.toJson(companyName);

		return json;*/
		return companyName;

	}

	@RequestMapping(value = "/userprofile", method = RequestMethod.GET)
	public String viewUserProfile(Model model, HttpServletRequest request) {

		logger.info("Inside viewUserProfile");
		// get Login id from session
		HttpSession session = request.getSession(false);
		UserSessionInterface userInterface = (UserSessionInterface) session.getAttribute("USER");
		logger.info("loginID " + userInterface.toString());
		String page = null;
		PortalUserDTO portalUserDTO = usermanagementService.getUserDetails(userInterface.getLoginId());
		logger.info("potalUserDTO " + portalUserDTO.toString());

		// Check category
		if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_SC")) {

			// get cso details of shipping company

			List<PortalCsoDpaDetail> currentCsoList = portalUserDTO.getCurrentcsoList();
			model.addAttribute("csolength", currentCsoList.size());
			portalUserDTO.setCsolength(currentCsoList.size());

			List<PortalCsoDpaDetail> dpaList = portalUserDTO.getDpaList();
			portalUserDTO.setDpalength(dpaList.size());
			model.addAttribute("currentcsoList", currentCsoList);

			model.addAttribute("dpaList", dpaList);
			model.addAttribute("dpalength", dpaList.size());
			model.addAttribute("category", "Shipping Company");
			model.addAttribute("portaluserdto", portalUserDTO);
			page = "/users/userprofile";

		} else {
			if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_NAVY"))
				model.addAttribute("category", "Navy");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_PA"))
				model.addAttribute("category", "PortAuthority");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_DGS"))
				model.addAttribute("category", "DGS");
			else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_CG"))
				model.addAttribute("category", "CoastGuard");
			model.addAttribute("portaluserdto", portalUserDTO);
			page = "/users/userprofile";
		}

		logger.info("Outside edit user");
		return page;
		// return "/users/userprofile";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/changepassword")
	public String changePassword(Model model) {
		logger.info("Inside Change Password ");

		model.addAttribute("changepassword", new ChangePassword());
		return "users/changepassword";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/savechangepassword")
	public String savechangePassword(@Valid @ModelAttribute("changepassword") ChangePassword changePassword,
			Model model, HttpServletRequest request) {
		logger.info("Inside Save Change Password " + changePassword.toString());
		// Check user has provided old password is correct

		HttpSession session = request.getSession(false);
		UserSessionInterface userInterface = (UserSessionInterface) session.getAttribute("USER");
		logger.info("loginID " + userInterface.toString());

		String loginId = userInterface.getLoginId();

		PortalUser portalUser = usermanagementService.getPortalUser(loginId);
		String hashedPassword = Utility.bcryptPassword(changePassword.getNewPassword());
		logger.info("PortalUser details:" + portalUser.toString());
		if (portalUser.getPassword() == null) {
			logger.info("PortalUser has password null");
			model.addAttribute("errorMsg", "Invalid entry.");
			return "users/changepassword";
		}
		if (!BCrypt.checkpw(changePassword.getOldPassword(), portalUser.getPassword())) {
			logger.info("old password doent match");
			model.addAttribute("errorMsg", "Invalid Old password");
			return "users/changepassword";
		}

		// Check new and confirmpassword has same data
		if (!changePassword.getNewPassword().equals(changePassword.getConfirmPassword())) {
			model.addAttribute("errorMsg", "New password and Confirm Psssword  must be same.");
			return "users/changepassword";
		}
		// new password should not be same as last 3 password
		if (BCrypt.checkpw(changePassword.getNewPassword(), portalUser.getPassword())) {
			model.addAttribute("errorMsg", "New Password matches with last 3 password.");
			return "users/changepassword";
		}
		boolean flag = usermanagementService.checkPassword(changePassword.getNewPassword(), loginId);
		if (flag) {
			model.addAttribute("errorMsg", "New Password matches with last 3 password.");
			return "users/changepassword";
		}
		// Get loginID from session and update password in encrypted format
		if (portalUser.getPassword() != null)
			usermanagementService.persistUserPassword(portalUser.getLoginId(), portalUser.getPassword());

		usermanagementService.updatePassword(portalUser.getLoginId(), hashedPassword);
		model.addAttribute("successMsg", "Password has been set.");
		model.addAttribute("changepassword", new ChangePassword());
		return "users/changepassword";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/setpassword")
	public String setPassword(HttpServletRequest request, Model model) {
		String login = request.getParameter("login");
		logger.info("Calling defaultHandler. Login Email for Set password =" + login);
		HttpSession session = request.getSession();
		session.setAttribute("loginid", login);
		model.addAttribute("portaluser", new PortalUser());
		logger.info("Encrypted loginid i.e login email value is " + login + " added in the session.");
		return "users/setuserpassword";

	}

	public void sendmail(PortalUser portalUser) throws ConnectException, Exception {
		String token = null;
		String link = null;
		String emailId = portalUser.getEmailid();
		logger.info("emailid "+emailId);
		String alternateEmailId=portalUser.getAlternateemailid();
		logger.info("alternateEmaildId "+alternateEmailId);
		
		token = usermanagementService.generateToken(portalUser.getLoginId());
		if(emailId!=null) {
		String sha256loginid = Encrypt.encryptString(emailId);
		logger.info("encrypted loginId" + sha256loginid);

		String path = usermanagementService.getWebPath(portalUser.getRequestorsLritId());
		logger.info("webpath " + path);
		link = path + "?login=" + sha256loginid;
		String mailcontent = "<tr><td><font style=FONT-size:12px size=2 face=Arial>"
				+ "<left>Dear user,<br><br> You have been registered in Lrit. "
				+ " </left><br> <BR><BR>Contact Person Name :- " + portalUser.getName() + " <br/>Login Id :- <b>"
				+ portalUser.getLoginId() + " <br>Token  :- <b>" + token
				+ "</b><br> <center>Click here To activate you account and Enter the the token.<br> <a href='" + link
				+ "'>Click here.</a></center>" + " </td></tr></table></td></tr><tr><td valign=top width=99%>";

		
		usermanagementService.sendMail("nsdg@cdac.in", portalUser.getEmailid(), "LRIT Registration", mailcontent);
		if(alternateEmailId!=null)
			usermanagementService.sendMail("nsdg@cdac.in", portalUser.getAlternateemailid(), "LRIT Registration", mailcontent);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/setpassword")
	public String setPassword(@Valid @ModelAttribute("portaluser") PortalUser portalUser, Model model,
			HttpServletRequest request) {
		logger.info("Inside set Password " + portalUser.toString());
		HttpSession session = request.getSession(false);
		if (session == null) {

			logger.error("Encrypted loginemail and user enterd login email NOT MATCH");
			model.addAttribute("errorMsg", "SORRY, Loginemail id Validation Fail against activation link");
			return "users/setuserpassword";
		}
		String loginid = (String) session.getAttribute("loginid");
		logger.info(" loginid from session = " + loginid);
		
	
		// loginId exist
		PortalUser userDetails = usermanagementService.getPortalUser(portalUser.getLoginId());
		if (userDetails == null) {
			logger.info("LoginId provided by user is invalid " + portalUser.getLoginId());
			model.addAttribute("errorMsg", "Enter valid loginId");
			return "users/setuserpassword";
		}

		String sha256loginid = Encrypt.encryptString(userDetails.getEmailid());

		// Check set password request is coming from proper emailid
		if (loginid == null || !loginid.trim().equals(sha256loginid)) {

			logger.error("Encrypted loginemail and user enterd login email NOT MATCH");
			model.addAttribute("errorMsg", "SORRY, Loginemail id Validation Fail against activation link");
			return "users/setuserpassword";
		}
		// check password and confirm password has same data
		if (!portalUser.getPassword().trim().equals(portalUser.getConfirmpassword().trim())) {
			logger.info("Confirm password and password must me same");
			model.addAttribute("errorMsg", "Confirm password and password must me same ");
			return "users/setuserpassword";
		}
		//if token is null in database it means user has already set the password 
		if (userDetails.getValidateToken()==null) {
			logger.info("Token for that user is null " + portalUser.getValidateToken());
			model.addAttribute("errorMsg", "Password already set  ");
			return "users/setuserpassword";
		}
		
		// Check provided token is same as in database
		if (!portalUser.getValidateToken().trim().equals(userDetails.getValidateToken())) {
			logger.info("Token provided by user is invalid " + portalUser.getValidateToken());
			model.addAttribute("errorMsg", "Enter valid Token");
			return "users/setuserpassword";
		}
		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp timestamp = new Timestamp(time);
		// check expire of token validation
		if (timestamp.after(userDetails.getTokenvalidupto())) {
			logger.info("Either Token is used, or it is expired. Please try again. " + timestamp);
			model.addAttribute("errorMsg", "Either Token is used, or it is expired. Please try again.");
			return "users/setuserpassword";
		}
		String hashedPassword = Utility.bcryptPassword(portalUser.getPassword().trim());
		logger.info("password after hashed " + hashedPassword + " password from database ");

		if (userDetails.getPassword() != null) {

			if (BCrypt.checkpw(portalUser.getPassword(), userDetails.getPassword())) {
				logger.info("Password matches with last 3 password");
				model.addAttribute("errorMsg", "Password matches with last 3 password.");
				return "users/setuserpassword";
			}
			// check password provided should be same as last 3 password
			boolean flag = usermanagementService.checkPassword(portalUser.getPassword(), portalUser.getLoginId());
			if (flag) {
				logger.info("Password matches with last 3 password");
				model.addAttribute("errorMsg", "Password matches with last 3 password.");
				return "users/setuserpassword";
			}
		}
		// Put entry in userpassword table with expiry date
		if (userDetails.getPassword() != null)
			usermanagementService.persistUserPassword(portalUser.getLoginId(), userDetails.getPassword());
		// update new password
		usermanagementService.updatePassword(portalUser.getLoginId(), hashedPassword);
		model.addAttribute("successMsg", "Password has been set.");
		model.addAttribute("portaluser", new PortalUser());

		return "users/setuserpassword";

	}

	@RequestMapping(value = "/forgotpassword", method = RequestMethod.GET)
	public String forgotPassword(Model model) {
		logger.info("Inside forgot password");
		model.addAttribute("portaluser", new PortalUser());

		return "users/forgotpassword";
	}

	@RequestMapping(value = "/forgotpassword", method = RequestMethod.POST)
	public String resetPassword(@Valid @ModelAttribute("portaluser") PortalUser portalUser, Model model) {
		logger.info("Inside resetPassword " + portalUser.toString());
		boolean flag = false;
		String loginId = portalUser.getLoginId();
		if (loginId == null || loginId.isEmpty()) {
			model.addAttribute("errorMsg", "LoginId is null.");
			return "users/forgotpassword";
		}

		// Check user is present and active
		//PortalUser user = usermanagementService.getPortalUser(loginId);
		PortalUser user = usermanagementService.getActivePortalUser(loginId);
		
		
		
		if (user == null) {
			model.addAttribute("errorMsg", "Invalid LoginId.");
			return "users/forgotpassword";
		}
		if (user.getEmailid() == null || user.getEmailid().isEmpty()) {
			model.addAttribute("errorMsg", "Email id is empty unable to send reset password link.");
			return "users/forgotpassword";
		}
		logger.info("user details "+user.toString());
		// send mail on emailId
		try {
			sendmail(user);
		} catch (Exception ex) {
			logger.error("Exception while sending mail");
			ex.printStackTrace();
			logger.error("Error " + ex.getMessage(), ex);
			flag = true;

			// but the e-mail notification could not be sent to the registered emailid"
			model.addAttribute("errorMsg", "e-mail notification could not be sent to the registered emailid");
		}
		if (!flag) {
			model.addAttribute("successMsg", "Email has been send for set password on registered email id");
		}
		model.addAttribute("portaluser", new PortalUser());
		return "users/forgotpassword";
	}

	@RequestMapping(value = "/companycsodetails", method = RequestMethod.GET)
	public String CompanyCsoDetails(@RequestParam("imoNo") String imoNo, Model model) {
		PortalUser userDetails = usermanagementService.getUserDetailsByImo(imoNo);
		PortalCsoDpaDetail currentCsoDetails = usermanagementService.getCurrentCsoDetails(imoNo);
		PortalCsoDpaDetail alternateCsoDetails = usermanagementService.getAlternateCsoDetails(imoNo);
		model.addAttribute("userDetails", userDetails);
		model.addAttribute("csoDetails", currentCsoDetails);
		model.addAttribute("alternateCsoDetails", alternateCsoDetails);
		return "users/company_cso_details";
	}

	@ResponseBody
	@RequestMapping(value = "/checkcsoidmapping", method = RequestMethod.GET)
	public boolean checkCsoIdMapping(@RequestParam("csoId") String csoId) {
		logger.info("Inside checkCsoIdMapping Controller method : csoId " + csoId);
		boolean flag = usermanagementService.checkCsoIdVesselMapping(csoId);
		logger.info("mapping of csoid and vessel exist " + flag);
		return flag;

	}

	@ResponseBody
	@RequestMapping(value = "/checkdpamapping", method = RequestMethod.GET)
	public boolean checkDpaMapping(@RequestParam("csoId") String csoId) {
		logger.info("Inside checkDpaMapping Controller method : csoId " + csoId);
		boolean flag = usermanagementService.checkDpaVesselMapping(csoId);
		logger.info("mapping of csoid and vessel exist " + flag);
		return flag;

	}

}