package in.gov.lrit.portal.controller.request;
import com.vividsolutions.jts.geom.Geometry;
import in.gov.lrit.gis.ddp.model.DdpVersion;
import in.gov.lrit.gis.ddp.model.LritContractGovtMst;
import in.gov.lrit.gis.ddp.model.LritPolygon;
import in.gov.lrit.gis.ddp.model.LritPort;
import in.gov.lrit.gis.ddp.model.LritPortFacilityType;
import in.gov.lrit.portal.CommonUtil.ICommonUtil;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in.gov.lrit.portal.model.request.DcPositionReport;
import in.gov.lrit.portal.model.request.DcPositionRequest;
import in.gov.lrit.portal.model.request.DcSarsurpicrequest;
import in.gov.lrit.portal.model.request.LritLatestShipPositionFlag;
import in.gov.lrit.portal.model.request.PortalCoastalRequest;
import in.gov.lrit.portal.model.request.PortalDdpRequest;
import in.gov.lrit.portal.model.request.PortalFlagRequest;
import in.gov.lrit.portal.model.request.PortalPortRequest;
import in.gov.lrit.portal.model.request.PortalRequest;
import in.gov.lrit.portal.model.request.PortalSarRequest;
import in.gov.lrit.portal.model.request.PortalSurpicRequest;
import in.gov.lrit.portal.model.request.ShipPositionNonFlag;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.service.request.IDdpMessagingManagement;
import in.gov.lrit.portal.service.request.IRequestmanagement;
import in.gov.lrit.portal.service.request.IShipdetails;
import in.gov.lrit.portal.service.request.SoapConnector;
import in_.gov.lrit.session.UserSessionInterface;
import responses.portalresponse.PortalResponse;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.imo.gisis.xml.lrit.ddprequest._2008.DDPRequestType;
import org.imo.gisis.xml.lrit.positionrequest._2008.RequestDurationType;
import org.imo.gisis.xml.lrit.surpicrequest._2014.SURPICRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.imo.gisis.xml.lrit.positionrequest._2008.ShipPositionRequestType;

@Controller
@RequestMapping({"/requests"})
public class RequestController
{
	@Autowired
	IRequestmanagement manageRequest;
	@Autowired
	ICommonUtil commonutil;
	@Autowired
	SoapConnector sc;
	@Autowired
	WSConfig ws;
	@Autowired
	IDdpMessagingManagement ddpobj;
	@Autowired
	IShipdetails sd;
	String messageid;
	PortalResponse response;

	@Value("${lrit.cgid}")
	private String cgid;

	String DcID="3065";

	private static final Logger logger = LoggerFactory.getLogger(RequestController.class);

	/* This method is for get ddp request page. */
	@PreAuthorize(value = "hasAuthority('ddprequest')")
	@RequestMapping(value={"/ddprequest"}, method={RequestMethod.GET})
	public String getDDPRequestPage(Model model)
	{
		List<DdpVersion> ddpversion = ddpobj.getDdpVersionview();   //find ddp version and implementation date from ddp vesrion view.
		String ddpversionfinal=commonutil.getDDPversion(ddpversion);
		String ddpversiondate = commonutil.getDDPversionDate(ddpversion);
		logger.info("In ddprequest page");
		PortalDdpRequest portalDdpRequest = new PortalDdpRequest();
		PortalRequest portalRequest = new PortalRequest();
		model.addAttribute("ddpversiondate", ddpversiondate);
		portalRequest.setCurrentddpversion(ddpversionfinal);
		portalDdpRequest.setPortalRequest(portalRequest);
		model.addAttribute("portalddprequest", portalDdpRequest);
		return "requests/ddprequestpage";
	}

	@PreAuthorize(value = "hasAuthority('ddprequest')")
	/* This method is for persist ddp request & sending request to DC. */
	@RequestMapping(value={"/persistddprequest"}, method={RequestMethod.POST})
	public String persistPortalDdpRequest(@Valid @ModelAttribute("portalddprequest") PortalDdpRequest portalddprequest, Model model, BindingResult bindingResult)
			throws Exception,JAXBException
	{
		logger.info("In persist ddpreq method");

		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		List<DdpVersion> ddpversion = ddpobj.getDdpVersionview();   //find ddp version and implementation date from ddp vesrion view.
		String ddpversiondate = commonutil.getDDPversionDate(ddpversion);
		model.addAttribute("ddpversiondate", ddpversiondate);
		DDPRequestType ddp = new DDPRequestType();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId(); //get lritid from session 
		String requestorLritid=lritIdmaster.getLritId();

		logger.info("requestors CGID=" + requestorLritid);
		Timestamp ts =commonutil.getCurrentTimeStamp();    //get current timestamp.
		String test = commonutil.getParavalues("Test");     //get test param from configpara

		String schema_version = this.commonutil.getParavalues("Schema_Version");  //schema vesrion from configpara
		Double sv = new Double(schema_version);
		BigDecimal schemaversion = BigDecimal.valueOf(sv.doubleValue());

		logger.info("Current date and time :" + ts);
		messageid = commonutil.generateMessageID(DcID);   //generate message id
		logger.info("messageid=" + messageid);

		PortalRequest portalreq = portalddprequest.getPortalRequest();
		portalreq.setMessageId(messageid);
		portalreq.setRequestorsLoginId(loginid);
		portalreq.setRequestGenerationTime(ts);
		portalreq.setRequestCategory("DDPRequest");
		portalddprequest.setRequesttimestamp(ts);
		portalddprequest.setTestMessage(Integer.parseInt(test));
		logger.info("Payload:" + portalddprequest.toString());

		XMLGregorianCalendar xmlDate = commonutil.getgregorianTimeStamp(ts);      //convert date to xmlgregoriandate

		BigInteger messagetype = BigInteger.valueOf(9);

		ddp.setMessageId(messageid);
		String refid =ddpobj.getDdpNotificationMessageid();    //get reference id
		logger.info("referenceid="+refid);
		try
		{
			if (portalddprequest.getUpdateType() == 4)    //if request update type is archived then set archived version and date
			{
				ddp.setArchivedDDPVersionNum(portalddprequest.getArchivedDdpVersion());
				String archivedversion=portalddprequest.getArchivedDdpVersion();

				Timestamp archivedversiontimestamp=ddpobj.getArchivedDdpversionTimestamp(archivedversion);
				portalddprequest.setArchivedDdpVersiondate(archivedversiontimestamp);
				Timestamp datearchived=portalddprequest.getArchivedDdpVersiondate();
				XMLGregorianCalendar xmlDateArchived = commonutil.getgregorianTimeStamp(datearchived); 
				ddp.setArchivedDDPTimeStamp(xmlDateArchived);
			}
			else
			{
				ddp.setArchivedDDPVersionNum(null);
				ddp.setArchivedDDPTimeStamp(null);
			}
			ddp.setReferenceId(refid);
			ddp.setDDPVersionNum(portalreq.getCurrentddpversion());
			ddp.setOriginator(DcID);  //dcid
			ddp.setTimeStamp(xmlDate);
			ddp.setSchemaVersion(schemaversion);
			ddp.setMessageType(messagetype);
			ddp.setTest(BigInteger.valueOf(Long.parseLong(test)));
			ddp.setUpdateType(BigInteger.valueOf(portalddprequest.getUpdateType()));

			JAXBContext jaxbContext = JAXBContext.newInstance(DDPRequestType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(ddp, sw);
			logger.info("Request payload:" + sw.toString());
			portalreq.setRequestpayload(sw.toString());
			if (requestorLritid.equalsIgnoreCase("1065"))
			{
				portalddprequest.setPortalRequest(portalreq);
				manageRequest.saveddprequest(portalddprequest);
				logger.info("sending ddp request to dc:" + ddp.toString());
				try
				{
					String responseStatus = sc.callWebService( ddp);     //calling dc webservice
					logger.info("responseStatus"+responseStatus);
					if (responseStatus.equalsIgnoreCase("true"))
					{
						model.addAttribute("successMsg", "DDP Request has been successfully submitted. MessageId=" + messageid);
						manageRequest.setResponsePayload("success", messageid);
					}
					if  (!(responseStatus.equalsIgnoreCase("true")))
					{
						model.addAttribute("errorMsg", "Error Message : "+responseStatus+" MessageID= " + messageid);
						manageRequest.setResponsePayload(responseStatus, messageid);
					}
				}

				catch (Exception e)
				{
					logger.info("in catch in ddp");
					model.addAttribute("errorMsg", "Could not able to send request to DC. MessageID= " + messageid);
					manageRequest.setResponsePayload("SoapFault", messageid);
					e.printStackTrace();
				}

			}

		}catch(Exception e)
		{
			logger.error("Archived version dows not exist");
			model.addAttribute("errorMsg", "Archived DDP version does not exist.");
			e.printStackTrace();
		}
		if (!(requestorLritid.equalsIgnoreCase("1065")))
		{
			logger.info("in pending task.");
			String message = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a ddp request with UpdateType "+portalddprequest.getUpdateType();
			portalreq.setStatus("pending");
			portalreq.setStatusDate(ts);
			portalddprequest.setPortalRequest(portalreq);
			PortalPendingTaskMst pendingtask = new PortalPendingTaskMst();
			pendingtask.setStatus("pending");
			pendingtask.setCategory("DDP_Request");
			pendingtask.setMessage(message);
			pendingtask.setReferenceId(messageid);
			pendingtask.setRequestDate(ts);
			pendingtask.setRequesterLoginId(loginid);
			pendingtask.setRequestorLritid(requestorLritid);
			manageRequest.saveddprequest(portalddprequest);
			manageRequest.savependingtask(pendingtask);
			model.addAttribute("successMsg", "DDP Request has been sent for approval. Messageid=" + messageid);
		}
		return "acknowledgement";
	}


	/* This method is for get flag request page. */
	@PreAuthorize(value = "hasAuthority('flagrequest')")
	@RequestMapping(value={"/flagrequest"}, method={RequestMethod.GET})
	public String getFlagRequestPage(Model model)
	{
		logger.info("in get flag request");
		model.addAttribute("portalflagrequest", new PortalFlagRequest());
		return "requests/flagrequestpage";
	}


	// method to get flag ships details.
	@PreAuthorize(value = "hasAnyAuthority('flagrequest','createroute','editroute')")
	@RequestMapping(value={"/getshipdetails"}, method={RequestMethod.GET})
	@ResponseBody
	public List<VesselDetailInterface> getShipdetails(Model model)
	{
		logger.info("Inside getShip details for flag request");
		List<String> regStatus = new ArrayList<String>();
		regStatus.add("SHIP_REGISTERED");
		regStatus.add("DNID_DOWNLOADED");
		List<String> shipstatus=new ArrayList<String>();
		shipstatus.add("ACTIVE");
		List<VesselDetailInterface> vesselList = sd.getFlagShipDetailsList(shipstatus,regStatus);
		return vesselList;
	}


	//this method is use to persist flag request and send to DC. 
	@PreAuthorize(value = "hasAuthority('flagrequest')")
	@RequestMapping(value={"/persistflagrequest"}, method={RequestMethod.POST})
	public String persistPortalFlagRequest(@Valid @ModelAttribute("portalflagrequest") PortalFlagRequest portalflagrequest, Model model, BindingResult bindingResult)
			throws Exception
	{
		logger.info("in Persist flag method");

		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes(); //get data from session
		HttpSession session =attr.getRequest().getSession();
		UserSessionInterface userSession =	(UserSessionInterface) session.getAttribute("USER"); 
		String	loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId(); //get  lritid from session 
		String requestorLritid=lritIdmaster.getLritId();


		List<DdpVersion> ddpversion = ddpobj.getDdpVersionview();   //find ddp version and implementation date from ddp vesrion view.
		String ddpversionfinal=commonutil.getDDPversion(ddpversion);

		Timestamp ts =commonutil.getCurrentTimeStamp();    //get current timestamp.
		String test = commonutil.getParavalues("Test");     //get test param from configpara

		String schema_version = this.commonutil.getParavalues("Schema_Version");  //schema vesrion from configpara
		Double sv = new Double(schema_version);
		BigDecimal schemaversion = BigDecimal.valueOf(sv.doubleValue());
		
		try
		{
			String imo= portalflagrequest.getImono();
			int vesselid= sd.getVesselDetail(imo);    //get vessel id
			logger.info("Vesselid before=" + vesselid);
			portalflagrequest.setVesselId(vesselid);
			logger.info("Vesselid after=" + vesselid);
			messageid = commonutil.generateMessageID(requestorLritid);   //generate msgid 
			logger.info("messageid=" + messageid);

			portalflagrequest.setAccessType("2");  //accesstype 2
			PortalRequest portalreq = new PortalRequest();
			portalreq.setCurrentddpversion(ddpversionfinal);
			portalreq.setMessageId(messageid);
			portalreq.setRequestorsLoginId(loginid);
			portalreq.setRequestGenerationTime(ts);
			portalreq.setRequestCategory("FlagRequest");
			logger.info("Portal Flag req:" + portalflagrequest.toString());

			ShipPositionRequestType flag = new ShipPositionRequestType();
			BigInteger messagetype = BigInteger.valueOf(4);
			BigInteger accesstype = BigInteger.valueOf(2);
			XMLGregorianCalendar xmlDate = commonutil.getgregorianTimeStamp(ts);      //convert date to xmlgregoriandate

			
			flag.setAccessType(accesstype);
			flag.setSchemaVersion(schemaversion);
			flag.setMessageId(messageid);
			flag.setDataUserProvider(requestorLritid);   //lrit id of country who will request for position both are same in flag req. 
			flag.setDataUserRequestor(requestorLritid);
			flag.setDDPVersionNum(ddpversionfinal);
			flag.setIMONum(imo);
			flag.setMessageType(messagetype);

			RequestDurationType requestduration = new RequestDurationType();

			if ((portalflagrequest.getRequestType() == 2) || (portalflagrequest.getRequestType() == 3) || 
					(portalflagrequest.getRequestType() == 4) || (portalflagrequest.getRequestType() == 5) || 
					(portalflagrequest.getRequestType() == 6)|| 
					(portalflagrequest.getRequestType() == 7)|| 
					(portalflagrequest.getRequestType() == 8)|| 
					(portalflagrequest.getRequestType() == 10)|| 
					(portalflagrequest.getRequestType() == 11))
			{
				XMLGregorianCalendar starttime = null;
				XMLGregorianCalendar stoptime = null;
				GregorianCalendar gcstartdate = new GregorianCalendar();
				GregorianCalendar gcenddate= new GregorianCalendar();
				gcstartdate.setTime(portalflagrequest.getStartTime());
				gcenddate.setTime(portalflagrequest.getEndTime());
				try
				{
					starttime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcstartdate);
					stoptime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcenddate);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				requestduration.setStartTime(starttime);
				requestduration.setStopTime(stoptime);
				flag.setRequestDuration(requestduration);
			}
			else
			{
				flag.setRequestDuration(null);
			}
			flag.setPortFacility(null);
			flag.setRequestType(BigInteger.valueOf(portalflagrequest.getRequestType()));
			flag.setTimeStamp(xmlDate);
			flag.setTest(BigInteger.valueOf(Long.parseLong(test)));


			JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { ShipPositionRequestType.class });
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(flag, sw);
			logger.info("flag request payload:" + sw.toString());
			portalreq.setRequestpayload(sw.toString());

			if (requestorLritid.equalsIgnoreCase("1065"))  //if cglritid is 1065 then send request otherwise forward req for approval
			{
				portalflagrequest.setPortalRequest(portalreq);
				manageRequest.saveflagrequest(portalflagrequest);
				logger.info("sending flag request to dc:" + flag.toString());

				try
				{
					String responseStatus = sc.callWebService(flag);  //send request to dc.cal dc webservice
					if (responseStatus.equalsIgnoreCase("true"))
					{
						model.addAttribute("successMsg", "Flag Request has been successfully submitted. MessageId=" + messageid);
						manageRequest.setResponsePayload("success", messageid);
					}
					if  (!(responseStatus.equalsIgnoreCase("true")))
					{
						model.addAttribute("errorMsg", "Error Message : "+responseStatus+" MessageID= " + messageid);
						manageRequest.setResponsePayload(responseStatus, messageid);
					}
				}
				catch (Exception e)
				{
					logger.error("Dc webservice is down.");
					model.addAttribute("errorMsg", "Could not able to send request to DC. MessageID= " + messageid);
					manageRequest.setResponsePayload("SoapFault", messageid);
					e.printStackTrace();
				}
			}

			if (!(requestorLritid.equalsIgnoreCase("1065")))
			{
				logger.info("Current date and time in pending :" + ts);
				String message = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a Flag request with RequestType "+portalflagrequest.getRequestType();
				portalreq.setStatus("pending");
				portalreq.setStatusDate(ts);
				portalflagrequest.setPortalRequest(portalreq);
				PortalPendingTaskMst pendingtask = new PortalPendingTaskMst();
				pendingtask.setStatus("pending");
				pendingtask.setCategory("Flag_Request");
				pendingtask.setMessage(message);
				pendingtask.setReferenceId(messageid);
				pendingtask.setRequestDate(ts);
				pendingtask.setRequesterLoginId(loginid);
				pendingtask.setRequestorLritid(requestorLritid);
				manageRequest.saveflagrequest(portalflagrequest);
				manageRequest.savependingtask(pendingtask);
				model.addAttribute("successMsg", "Flag Request has been sent for approval. Messageid=" + messageid);
			}

		}
		catch (Exception e)
		{
			logger.error("in last catch in flag");
			logger.error("IMO NO is not present.");
			model.addAttribute("errorMsg", "IMO No does not exist.");
			e.printStackTrace();
		}
		return "acknowledgement";
	}

	//this method is used to get port request page

	@PreAuthorize(value = "hasAuthority('portrequest')")
	@RequestMapping(value={"/portrequest"}, method={RequestMethod.GET})
	public String getPortRequestPage(Model model)

	{
		logger.info("in get port request page");
		model.addAttribute("portalportrequest", new PortalPortRequest());
		return "requests/portrequestpage";
	}

	//this method is used to persist port request page

	@PreAuthorize(value = "hasAuthority('portrequest')")
	@RequestMapping(value={"/persistportrequest"}, method={RequestMethod.POST})
	public String persistportalportrequest(@Valid @ModelAttribute("portalportrequest") PortalPortRequest portalportrequest, Model model, BindingResult bindingResult)
			throws Exception
	{

		logger.info("in persist port request");
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();   // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
		String requestorLritid=lritIdmaster.getLritId();

		messageid = commonutil.generateMessageID(cgid);   //generate messageid
		logger.info("messageid=" + messageid);
		Timestamp ts = commonutil.getCurrentTimeStamp();
		logger.info("timestamp in portal=" + ts);

		List<DdpVersion> ddpversion = ddpobj.getDdpVersionview();   //find ddp version and implementation date from ddp vesrion view.
		String ddpversionfinal=commonutil.getDDPversion(ddpversion);

		String Test = commonutil.getParavalues("Test");
		String schema_version = commonutil.getParavalues("Schema_Version");


		PortalRequest portalreq = new PortalRequest();
		portalreq.setCurrentddpversion(ddpversionfinal);
		portalreq.setMessageId(messageid);
		portalreq.setRequestorsLoginId(loginid);
		portalreq.setRequestGenerationTime(ts);
		portalreq.setRequestCategory("PortRequest");

		logger.info("Payload:" + portalportrequest.toString());

		ShipPositionRequestType port = new ShipPositionRequestType();
		Double sv = new Double(schema_version);
		BigDecimal schemaversion = BigDecimal.valueOf(sv.doubleValue());
		BigInteger messagetype = BigInteger.valueOf(4);

		XMLGregorianCalendar xmlDate = commonutil.getgregorianTimeStamp(ts);      //convert date to xmlgregoriandate
		
		logger.info("GREGORIAN DATE="+xmlDate);

		port.setAccessType(BigInteger.valueOf(portalportrequest.getAccessType()));
		port.setSchemaVersion(schemaversion);
		port.setMessageId(messageid);
		port.setDataUserProvider(portalportrequest.getUserProvider());
		port.setDataUserRequestor(requestorLritid);
		port.setDDPVersionNum(ddpversionfinal);
		port.setIMONum(portalportrequest.getImoNo());
		port.setMessageType(messagetype);
		port.setTimeStamp(xmlDate);
		port.setTest(BigInteger.valueOf(Long.parseLong(Test)));

		RequestDurationType requestduration = new RequestDurationType();

		XMLGregorianCalendar starttime = null;
		XMLGregorianCalendar stoptime = null;
		GregorianCalendar gcdate = new GregorianCalendar();
		GregorianCalendar gcdate2 = new GregorianCalendar();
		if (portalportrequest.getAccessType() == 3)
		{
			portalportrequest.setRequestType(portalportrequest.getRequesttypedistance());
			port.setDistance((int)portalportrequest.getDistance());
			port.setRequestType(BigInteger.valueOf(portalportrequest.getRequesttypedistance()));
			if ((portalportrequest.getRequesttypedistance() == 2) || (portalportrequest.getRequesttypedistance() == 3) || 
					(portalportrequest.getRequesttypedistance() == 4) || 
					(portalportrequest.getRequesttypedistance() == 5) || 
					(portalportrequest.getRequesttypedistance() == 6) || 
					(portalportrequest.getRequesttypedistance() == 10) || 
					(portalportrequest.getRequesttypedistance() == 11)|| 
					(portalportrequest.getRequesttypedistance() == 8))
			{
				gcdate.setTime(portalportrequest.getStartTime());
				gcdate2.setTime(portalportrequest.getEndTime());
				try
				{
					starttime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcdate);
					stoptime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcdate2);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				requestduration.setStartTime(starttime);
				requestduration.setStopTime(stoptime);

				logger.info("start:" + requestduration.getStartTime());
				logger.info("stop:" + requestduration.getStopTime());

				port.setRequestDuration(requestduration);
			}
			else
			{
				port.setRequestDuration(null);
			}
			logger.info("requesttype" + portalportrequest.getRequesttypedistance());
		}
		else if (portalportrequest.getAccessType() == 5)
		{
			//port.setDistance((Integer) null);
			logger.info("distance=="+port.getDistance());
			port.setRequestType(BigInteger.valueOf(portalportrequest.getRequestType()));
			if ((portalportrequest.getRequestType() == 2) || (portalportrequest.getRequestType() == 3) || 
					(portalportrequest.getRequestType() == 4) || (portalportrequest.getRequestType() == 5) || 
					(portalportrequest.getRequestType() == 6) || (portalportrequest.getRequestType() == 7) || 
					(portalportrequest.getRequestType() == 10) || (portalportrequest.getRequestType() == 11)|| (portalportrequest.getRequestType() == 8))
			{
				gcdate.setTime(portalportrequest.getStartTime());
				gcdate2.setTime(portalportrequest.getEndTime());
				try
				{
					starttime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcdate);
					stoptime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcdate2);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				requestduration.setStartTime(starttime);
				requestduration.setStopTime(stoptime);

				logger.info("start:" + requestduration.getStartTime());
				logger.info("stop:" + requestduration.getStopTime());

				port.setRequestDuration(requestduration);
			}
			else
			{
				port.setRequestDuration(null);
			}
			logger.info("requesttype=" + portalportrequest.getRequesttypedistance());
		}
		logger.info("ports1=="+portalportrequest.getPortCode());
		logger.info("ports2=="+portalportrequest.getPortfacilityCode());
		if(portalportrequest.getPortCode()==null ||portalportrequest.getPortCode()==""||portalportrequest.getPortCode().isEmpty())
		{
			logger.info("1");
			port.setPort(null);
			port.setPortFacility(portalportrequest.getPortfacilityCode());
		}
		else if(portalportrequest.getPortfacilityCode()!=null||portalportrequest.getPortfacilityCode()!=""||portalportrequest.getPortfacilityCode().isEmpty())
		{
			logger.info("2");
			
			
			port.setPort(portalportrequest.getPortCode());
			port.setPortFacility(null);
		}
		logger.info("port ==" + port.getPort());
		logger.info("port facility ==" + port.getPortFacility());
		logger.info("sending port request to dc:" + port.toString());
		JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { ShipPositionRequestType.class });
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(port, sw);
		logger.info("payload:" + sw.toString());
		portalreq.setRequestpayload(sw.toString());

		if (requestorLritid.equalsIgnoreCase("1065"))
		{
			portalportrequest.setPortalRequest(portalreq);
			manageRequest.saveportrequest(portalportrequest);
			logger.info("sending port request to dc:" + port.toString());
			try
			{

				String responseStatus = sc.callWebService(port);
				if (responseStatus.equalsIgnoreCase("true"))
				{
					model.addAttribute("successMsg", "Port Request has been successfully submitted. MessageId=" + messageid);
					manageRequest.setResponsePayload("success", messageid);
				}
				if  (!(responseStatus.equalsIgnoreCase("true")))
				{
					model.addAttribute("errorMsg", "Error Message : "+responseStatus+" MessageID= " + messageid);
					manageRequest.setResponsePayload(responseStatus, messageid);
				}
			}
			catch (Exception e)
			{
				logger.info("in catch in port");
				model.addAttribute("errorMsg", "Could not able to send request to DC. MessageID= " + messageid);
				manageRequest.setResponsePayload("SoapFault", messageid);
				e.printStackTrace();
			}
		}
		if (!(requestorLritid.equalsIgnoreCase("1065")))
		{
			String message = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a port request with RequestType "+portalportrequest.getRequestType();
			portalreq.setStatus("pending");
			portalreq.setStatusDate(ts);
			portalportrequest.setPortalRequest(portalreq);
			PortalPendingTaskMst pendingtask = new PortalPendingTaskMst();
			pendingtask.setStatus("pending");
			pendingtask.setCategory("Port_Request");
			pendingtask.setMessage(message);
			pendingtask.setReferenceId(messageid);
			pendingtask.setRequestDate(ts);
			pendingtask.setRequesterLoginId(loginid);
			pendingtask.setRequestorLritid(requestorLritid);
			manageRequest.saveportrequest(portalportrequest);
			manageRequest.savependingtask(pendingtask);
			model.addAttribute("successMsg", "Port Request has been sent for approval. Messageid=" + messageid);
		}

		return "acknowledgement";
	}


	//this method is used to get flag vessels within 1000nm

	@PreAuthorize(value = "hasAuthority('portrequest')")
	@RequestMapping(value={"/getshipdetailsflagfromdc"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})

	@ResponseBody
	public List<LritLatestShipPositionFlag> getFlagShipdetailsfromDc(Model model)
	{
		logger.info("Inside getFlagShipdetailsfromDc ");


		List<DdpVersion> ddpversion = ddpobj.getDdpVersionview();   //find ddp version and implementation date from ddp vesrion view.
		String ddpRegularVersion=commonutil.getDDPRegularversion(ddpversion);
		logger.info("Regular Version " + ddpRegularVersion);

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();   // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
		String requestorLritid=lritIdmaster.getLritId();

		LritPolygon polygon = ddpobj.getCountryPolygon(requestorLritid, ddpRegularVersion);    //get 1000nm polygon

		Geometry geo = polygon.getPoslist();
		//logger.info("1000nm polygon=" + geo);
		List<LritLatestShipPositionFlag> latestflagships = new ArrayList<LritLatestShipPositionFlag>();
		try
		{
			latestflagships = sd.getShipwithinBoundry(geo);   //get flag ships within 1000nm

			logger.info("count of ships within 1000nm " + latestflagships.size());
			return latestflagships;
		}
		catch(Exception e)
		{
			latestflagships=null;
			logger.error("No ships available in 1000nm");
			e.printStackTrace();

			return latestflagships;
		}

	}

	//this method is used to get non flag vessels

	@PreAuthorize(value = "hasAuthority('portrequest')")
	@RequestMapping(value={"/getshipdetailsnonflagfromdc"}, method={RequestMethod.GET})
	@ResponseBody
	public Collection<ShipPositionNonFlag> getShipDetailsNonFlagfromdc(Model model)
	{
		logger.info("Inside getShipDetailsNonFlagfromdc");

		Collection<ShipPositionNonFlag> latestnonflagships=new ArrayList<ShipPositionNonFlag>();
		try{
			latestnonflagships = sd.getNonFlagShips();  //get non flag ships 
			return latestnonflagships;

		}
		catch(Exception e)
		{
			latestnonflagships=null;
			logger.error("No ships available in nonflag position table");
			e.printStackTrace();
			return latestnonflagships;
		}
		//return latestnonflagships;
	}

	@PreAuthorize(value = "hasAuthority('portrequest')")
	@RequestMapping(value={"/getreportingrate"}, method={RequestMethod.GET})
	@ResponseBody
	public List<DcPositionRequest> getReportingrate(String imo)
	{
		logger.info("Insidegetreportingrate");

		logger.info("IMOOOO=" + imo);
		List<Integer> accesstype=new ArrayList<Integer>();
		accesstype.add(5);
		accesstype.add(3);
		List<DcPositionRequest> dcpositionreq=new ArrayList<DcPositionRequest>();
		try
		{
			dcpositionreq =sd.getReportingratefromDc(imo, "active", 4, accesstype);

			logger.info("size of reporting rate=" + dcpositionreq.size());
			for (int i = 0; i < dcpositionreq.size(); i++)
			{
				logger.info("imo" + ((DcPositionRequest)dcpositionreq.get(i)).getImoNo());
				logger.info("reqtype" + ((DcPositionRequest)dcpositionreq.get(i)).getRequestType());
				logger.info("accesstype" + ((DcPositionRequest)dcpositionreq.get(i)).getAccessType());

			}
			return dcpositionreq;
		}
		catch(Exception e)
		{
			dcpositionreq=null;
			logger.info("no active request on imo="+imo);
			e.printStackTrace();
			return dcpositionreq;
		}

	}

	@PreAuthorize(value = "hasAuthority('portrequest')")
	@RequestMapping(value={"/getportdetails"}, method={RequestMethod.GET})
	@ResponseBody
	public List<LritPort> getportdetails()
	{
		logger.info("Inside port details");

		List<DdpVersion> ddpversion = ddpobj.getDdpVersionview();   //find ddp version and implementation date from ddp vesrion view.
		String ddpRegularVersion=commonutil.getDDPRegularversion(ddpversion);


		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();   // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
		String requestorLritid=lritIdmaster.getLritId();


		List<LritPort> ports = ddpobj.getPortList(requestorLritid, ddpRegularVersion);
		logger.info("size of ports=" + ports.size());
		/*
		 * for (int i = 0; i < ports.size(); i++) { logger.info("portcode" +
		 * ((LritPort)ports.get(i)).getId().getLocode()); logger.info("portname" +
		 * ((LritPort)ports.get(i)).getName()); }
		 */
		return ports;
	}

	@PreAuthorize(value = "hasAuthority('portrequest')")
	@RequestMapping(value={"/getportfacility"}, method={RequestMethod.GET})
	@ResponseBody
	public List<LritPortFacilityType> getportfacility()
	{
		logger.info("Inside port facility");
		List<DdpVersion> ddpversion = ddpobj.getDdpVersionview();   //find ddp version and implementation date from ddp vesrion view.
		String ddpRegularVersion=commonutil.getDDPRegularversion(ddpversion);


		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();   // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
		String requestorLritid=lritIdmaster.getLritId();


		List<LritPortFacilityType> portfacility = ddpobj.getPortFacilityList(requestorLritid, ddpRegularVersion);
		logger.info("size of ports=" + portfacility.size());
		/*
		 * for (int i = 0; i < portfacility.size(); i++) {
		 * logger.info("portcode facility" +
		 * ((LritPortFacilityType)portfacility.get(i)).getId().getImoportfacilitynumber(
		 * )); logger.info("portname" +
		 * ((LritPortFacilityType)portfacility.get(i)).getName()); }
		 */
		return portfacility;
	}


	///////////coastal surpic
	@PreAuthorize(value = "hasAuthority('coastalsurpicrequest')")
	@RequestMapping(value={"/coastalsurpicrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	public String getCoastalSurpicRequestPage(Model model)
	{
		logger.info("coastal Surpic request page");

		logger.info("Inside countrylist");

		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String regularversion = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			regularversion = ((DdpVersion)ddpversion.get(j)).getApplicableregularversion();
		}
		logger.info("Regular version=" + regularversion);

		List<LritContractGovtMst> countrylist = this.ddpobj.getCountrylist(regularversion);
		logger.info("size of counties=" + countrylist.size());
		for (int i = 0; i < 3; i++) {
			logger.info(" countrylist" + ((LritContractGovtMst)countrylist.get(i)).getCgName());
		}
		model.addAttribute("countrylist", countrylist);
		model.addAttribute("portalsurpicrequest", new PortalSurpicRequest());

		return "/requests/coastalsurpicrequest";
	}


	@PreAuthorize(value = "hasAuthority('coastalsurpicrequest')")
	@RequestMapping(value={"/persistcoastalsurpicrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	public String persistportalcoastalsurpicrequest(@Valid @ModelAttribute("portalsurpicrequest") PortalSurpicRequest portalsurpicrequest, Model model, BindingResult bindingResult)
			throws Exception
	{
		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes(); // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface  userSession = (UserSessionInterface) session.getAttribute("USER"); 
		String loginid=userSession.getLoginId();


		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
		String requestorLritid=lritIdmaster.getLritId();

		String userCategory=userSession.getCategory();  //get user category from session



		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String regularversion = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			regularversion = ((DdpVersion)ddpversion.get(j)).getApplicableregularversion();
		}



		logger.info("Regular version=" + regularversion);


		//List<DdpVersion> ddpversion1 = ddpobj.getRegularversionno();
		String ddpversionfinal = null;
		for (int j = 0; j < ddpversion.size(); j++)
		{
			ddpversionfinal = ((DdpVersion)ddpversion.get(j)).getDdpversion();
		}

		List<LritContractGovtMst> countrylist = ddpobj.getCountrylist(regularversion);
		logger.info("size of counties=" + countrylist.size());
		//logger.info("countriesss="+countrylist.toString());
		for (int i = 0; i < 3; i++) {
			logger.info(" countrylist" + ((LritContractGovtMst)countrylist.get(i)).getCgName());
		}
		model.addAttribute("countrylist", countrylist);

		messageid = commonutil.generateMessageID(this.cgid);
		logger.info("messageid=" + messageid);
		Timestamp ts = commonutil.getCurrentTimeStamp();
		logger.info("ts in portal=" + ts);

		//String latestddpversion = commonutil.getParavalues("current_ddpversion_no");
		String Test = commonutil.getParavalues("Test");
		String schema_version = commonutil.getParavalues("Schema_Version");

		SURPICRequest surpic = new SURPICRequest();
		PortalRequest portalreq = new PortalRequest();
		portalreq.setCurrentddpversion(ddpversionfinal);
		portalreq.setMessageId(messageid);
		portalreq.setRequestorsLoginId(loginid);
		portalreq.setRequestGenerationTime(ts);
		portalreq.setRequestCategory("CoastalSurpicRequest");

		int position = portalsurpicrequest.getNumberOfPosition().intValue();
		portalsurpicrequest.setNumberOfPosition(Integer.valueOf(position));
		String shiptype = portalsurpicrequest.getShipType();//1
		if(shiptype!=null)
		{
		portalsurpicrequest.setShipType(shiptype);//2
		String[] elements = shiptype.split(",");//3
		List<String> fixedLenghtList = Arrays.asList(elements);//4
		ArrayList<String> listOfString = new ArrayList(fixedLenghtList);//5
		surpic.getShipTypes().addAll(listOfString);  
		}
		else
		{
			portalsurpicrequest.setShipType(null);
			surpic.getShipTypes();  
		}
		

		logger.info("SHIPPosition=" + portalsurpicrequest.getNumberOfPosition());
		logger.info("SHIPtype=" + portalsurpicrequest.getShipType());//6

		String datauserprovidercountry=portalsurpicrequest.getCountries();  //1

		if (datauserprovidercountry==null||datauserprovidercountry.equalsIgnoreCase("0"))
		{
			portalsurpicrequest.setDataUserProvider(null); //4
			surpic.setDataUserProvider(null); 
		}
		else
		{
			String datauserproviderlritid=ddpobj.getDataUserProviderlritid(datauserprovidercountry); //2
			portalsurpicrequest.setDataUserProvider(datauserproviderlritid); //4
			surpic.setDataUserProvider(datauserproviderlritid);  //for coastal surpic
			logger.info("LRITID=="+datauserproviderlritid); //3
			
			
		}

		portalsurpicrequest.setAccessType(Integer.valueOf(1));
		portalsurpicrequest.setMessageType(Integer.valueOf(6));
		String RectangularArea = null;
		String CircularArea = null;

		logger.info("deg" + portalsurpicrequest.getRectangleLatitudedegree());
		logger.info("min" + portalsurpicrequest.getRectangleLatitudemin());
		logger.info("deg" + portalsurpicrequest.getRectangleLatitudedegree().isEmpty());
		if (!(portalsurpicrequest.getRectangleLatitudedegree().isEmpty()) && !(portalsurpicrequest.getRectangleLatitudemin().isEmpty()))
		{
			logger.info("in  if");
			String rectanglelatdeg = portalsurpicrequest.getRectangleLatitudedegree();
			String rectanglelatmin = portalsurpicrequest.getRectangleLatitudemin();
			String rectanglelatdir = portalsurpicrequest.getRectangleLatitudedir();
			String rectanglelongdeg = portalsurpicrequest.getRectangleLongitudedegree();
			String rectanglelongmin = portalsurpicrequest.getRectangleLongitudemin();
			String rectanglelongdir = portalsurpicrequest.getRectangleLongitudedir();

			String offsetlatdeg = portalsurpicrequest.getOffsetLatitudedegree();
			String offsetlatmin = portalsurpicrequest.getOffsetLatitudemin();
			String offsetlatdir = portalsurpicrequest.getOffsetLatitudedir();
			String offsetlongdeg = portalsurpicrequest.getOffsetLongitudedegree();
			String offsetlongmin = portalsurpicrequest.getOffsetLongitudemin();
			String offsetlongdir = portalsurpicrequest.getOffsetLongitudedir();

			RectangularArea = rectanglelatdeg + "." + rectanglelatmin + "." + rectanglelatdir + ":" + rectanglelongdeg + "." + rectanglelongmin + "." + rectanglelongdir + ":" + offsetlatdeg + "." + offsetlatmin + "." + offsetlatdir + ":" + offsetlongdeg + "." + offsetlongmin + "." + offsetlongdir;

			logger.info("rectangular area==" + RectangularArea);
			portalsurpicrequest.setRectangularArea(RectangularArea);
			surpic.setCircularArea(null);
			surpic.setRectangularArea(RectangularArea);
		}
		else
		{
			logger.info("in else if");
			String circlelatdeg = portalsurpicrequest.getCircleLatitudedegree();
			String circlelatmin = portalsurpicrequest.getCircleLatitudemin();
			String circlelatdir = portalsurpicrequest.getCircleLatitudedir();
			String circlelongdeg = portalsurpicrequest.getCircleLongitudedegree();
			String circlelongmin = portalsurpicrequest.getCircleLongitudemin();
			String circlelongdir = portalsurpicrequest.getCircleLongitudedir();
			String radius = portalsurpicrequest.getRadius();
			if (radius.length()==1)
			{
			 radius=00+radius;		
			}
			if (radius.length()==2)
			{
			 radius=0+radius;		
			}
			 CircularArea = circlelatdeg + "." + circlelatmin + "." + circlelatdir + ":" + circlelongdeg + "." + circlelongmin + "." + circlelongdir + ":" + radius;
             
			logger.info("circular area==" + CircularArea);
			portalsurpicrequest.setCircularArea(CircularArea);
			surpic.setRectangularArea(null);
			surpic.setCircularArea(CircularArea);
		}
		Double sv = new Double(schema_version);
		BigDecimal schemaversion = BigDecimal.valueOf(sv.doubleValue());
		BigInteger messagetype = BigInteger.valueOf(6L);
		BigInteger accesstype = BigInteger.valueOf(1L);
		surpic.setSchemaVersion(schemaversion);
		surpic.setTest(BigInteger.valueOf(Long.parseLong(Test)));
		surpic.setNumberOfPositions(position);

		

		//surpic.getShipTypes();

		LocalDateTime ldt = ts.toLocalDateTime();
		XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
		cal.setYear(ldt.getYear());
		cal.setMonth(ldt.getMonthValue());
		cal.setDay(ldt.getDayOfMonth());

		cal.setHour(ldt.getHour());
		cal.setMinute(ldt.getMinute());

		cal.setSecond(ldt.getSecond());
		cal.setFractionalSecond(new BigDecimal("0." + ldt.getNano()));

		System.out.println("calender in dc::" + cal);

		surpic.setAccessType(accesstype);
		surpic.setSchemaVersion(schemaversion);
		surpic.setMessageId(this.messageid);
		surpic.setDataUserRequestor(requestorLritid);

		surpic.setDDPVersionNum(ddpversionfinal);
		surpic.setMessageType(messagetype);
		surpic.setTimeStamp(cal);

		logger.info("sending surpic request to dc:" + surpic.toString());
		JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { SURPICRequest.class });
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(surpic, sw);
		logger.info("payload:" + sw.toString());

		portalreq.setRequestpayload(sw.toString());

		if (userCategory.equalsIgnoreCase("USER_CATEGORY_DGS"))  
		{
			portalsurpicrequest.setPortalRequest(portalreq);
			manageRequest.savesurpicrequest(portalsurpicrequest);
			logger.info("sending coastal request to dc:" + surpic.toString());
			try
			{

				String responseStatus = sc.callWebService( surpic);
				if (responseStatus.equalsIgnoreCase("true"))
				{
					model.addAttribute("CircularArea", CircularArea);
					model.addAttribute("RectangularArea", RectangularArea);
					model.addAttribute("successMsg", "Coastal Surpic Request has been successfully submitted. MessageId=" + messageid);
					manageRequest.setResponsePayload("success", messageid);
				}
				if  (!(responseStatus.equalsIgnoreCase("true")))
				{
					model.addAttribute("errorMsg", "Error Message : "+responseStatus+" MessageID= " + messageid);
					manageRequest.setResponsePayload(responseStatus, messageid);
				}
			}
			catch (Exception e)
			{
				logger.info("in catch in coastal surpic");
				model.addAttribute("errorMsg", "Could not able to send request to DC. MessageID= " + messageid);
				manageRequest.setResponsePayload("SoapFault", messageid);
				e.printStackTrace();
			}
		}
		if (!(userCategory.equalsIgnoreCase("USER_CATEGORY_DGS")))
		{
			String message = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a Coastal Surpic request with RequestType ";
			portalreq.setStatus("pending");
			portalreq.setStatusDate(ts);
			portalsurpicrequest.setPortalRequest(portalreq);
			PortalPendingTaskMst pendingtask = new PortalPendingTaskMst();
			pendingtask.setStatus("pending");
			pendingtask.setCategory("CoastalSurpic_Request");
			pendingtask.setMessage(message);
			pendingtask.setReferenceId(messageid);
			pendingtask.setRequestDate(ts);
			pendingtask.setRequesterLoginId(loginid);
			pendingtask.setRequestorLritid(requestorLritid);
			manageRequest.savesurpicrequest(portalsurpicrequest);
			manageRequest.savependingtask(pendingtask);
			model.addAttribute("successMsg", "Coastal Surpic Request has been sent for approval. Messageid=" + messageid);
			model.addAttribute("CircularArea", CircularArea);
			model.addAttribute("RectangularArea", RectangularArea);	
		}
		return "/requests/coastalsurpicrequest";
		//return "acknowledgement";
	}


	////////////

	@PreAuthorize(value = "hasAuthority('sarsurpicrequest')")
	@RequestMapping(value={"/surpicrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	public String getSarSurpicRequestPage(Model model)
	{
		logger.info("Surpic request page");

		logger.info("Inside countrylist");

		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String regularversion = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			regularversion = ((DdpVersion)ddpversion.get(j)).getApplicableregularversion();
		}
		logger.info("Regular version=" + regularversion);

		List<LritContractGovtMst> countrylist = this.ddpobj.getCountrylist(regularversion);
		logger.info("size of counties=" + countrylist.size());
		for (int i = 0; i < 3; i++) {
			logger.info(" countrylist" + ((LritContractGovtMst)countrylist.get(i)).getCgName());
		}
		model.addAttribute("countrylist", countrylist);
		model.addAttribute("portalsurpicrequest", new PortalSurpicRequest());

		return "/requests/surpicrequestpage";
	}

	@PreAuthorize(value = "hasAuthority('sarsurpicrequest')")
	@RequestMapping(value={"/persistsurpicrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	public String persistportalsarsurpicrequest(@Valid @ModelAttribute("portalsurpicrequest") PortalSurpicRequest portalsurpicrequest, Model model, BindingResult bindingResult)
			throws Exception
	{
		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes(); // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface  userSession = (UserSessionInterface) session.getAttribute("USER"); 
		String loginid=userSession.getLoginId();


		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
		String requestorLritid=lritIdmaster.getLritId();

		String userCategory=userSession.getCategory();  //get user category from session

		String sarid=(String) session.getAttribute("CoastGuard"); 

		BigInteger messagetype = BigInteger.valueOf(6);
		BigInteger accesstype = BigInteger.valueOf(6);

		//String sarid="2030";
		logger.info("SARID"+sarid);

		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String regularversion = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			regularversion = ((DdpVersion)ddpversion.get(j)).getApplicableregularversion();
		}



		logger.info("Regular version=" + regularversion);


		//List<DdpVersion> ddpversion1 = ddpobj.getRegularversionno();
		String ddpversionfinal = null;
		for (int j = 0; j < ddpversion.size(); j++)
		{
			ddpversionfinal = ((DdpVersion)ddpversion.get(j)).getDdpversion();
		}

		List<LritContractGovtMst> countrylist = ddpobj.getCountrylist(regularversion);
		logger.info("size of counties=" + countrylist.size());
		//logger.info("countriesss="+countrylist.toString());
		for (int i = 0; i < 3; i++) {
			logger.info(" countrylist" + ((LritContractGovtMst)countrylist.get(i)).getCgName());
		}
		model.addAttribute("countrylist", countrylist);

		messageid = commonutil.generateMessageID(this.cgid);
		logger.info("messageid=" + messageid);
		Timestamp ts = commonutil.getCurrentTimeStamp();
		logger.info("ts in portal=" + ts);

		String Test = commonutil.getParavalues("Test");
		String schema_version = commonutil.getParavalues("Schema_Version");

		SURPICRequest surpic = new SURPICRequest();
		PortalRequest portalreq = new PortalRequest();
		portalreq.setCurrentddpversion(ddpversionfinal);
		portalreq.setMessageId(messageid);
		portalreq.setRequestorsLoginId(loginid);
		portalreq.setRequestGenerationTime(ts);
		portalreq.setRequestCategory("SARSurpicRequest");

		int position = portalsurpicrequest.getNumberOfPosition().intValue();
		portalsurpicrequest.setNumberOfPosition(Integer.valueOf(position));

		logger.info("SHIPPosition=" + portalsurpicrequest.getNumberOfPosition());

		portalsurpicrequest.setAccessType(Integer.valueOf(6));
		portalsurpicrequest.setMessageType(Integer.valueOf(6));
		String RectangularArea = null;
		String CircularArea = null;

		logger.info("deg" + portalsurpicrequest.getRectangleLatitudedegree());
		logger.info("min" + portalsurpicrequest.getRectangleLatitudemin());
		logger.info("deg" + portalsurpicrequest.getRectangleLatitudedegree().isEmpty());
		if (!(portalsurpicrequest.getRectangleLatitudedegree().isEmpty()) && !(portalsurpicrequest.getRectangleLatitudemin().isEmpty()))
		{
			logger.info("in  if");
			String rectanglelatdeg = portalsurpicrequest.getRectangleLatitudedegree();
			String rectanglelatmin = portalsurpicrequest.getRectangleLatitudemin();
			String rectanglelatdir = portalsurpicrequest.getRectangleLatitudedir();
			String rectanglelongdeg = portalsurpicrequest.getRectangleLongitudedegree();
			String rectanglelongmin = portalsurpicrequest.getRectangleLongitudemin();
			String rectanglelongdir = portalsurpicrequest.getRectangleLongitudedir();

			String offsetlatdeg = portalsurpicrequest.getOffsetLatitudedegree();
			String offsetlatmin = portalsurpicrequest.getOffsetLatitudemin();
			String offsetlatdir = portalsurpicrequest.getOffsetLatitudedir();
			String offsetlongdeg = portalsurpicrequest.getOffsetLongitudedegree();
			String offsetlongmin = portalsurpicrequest.getOffsetLongitudemin();
			String offsetlongdir = portalsurpicrequest.getOffsetLongitudedir();

			RectangularArea = rectanglelatdeg + "." + rectanglelatmin + "." + rectanglelatdir + ":" + rectanglelongdeg + "." + rectanglelongmin + "." + rectanglelongdir + ":" + offsetlatdeg + "." + offsetlatmin + "." + offsetlatdir + ":" + offsetlongdeg + "." + offsetlongmin + "." + offsetlongdir;

			logger.info("rectangular area==" + RectangularArea);
			portalsurpicrequest.setRectangularArea(RectangularArea);
			surpic.setCircularArea(null);
			surpic.setRectangularArea(RectangularArea);
		}
		else
		{
			logger.info("in else if");
			String circlelatdeg = portalsurpicrequest.getCircleLatitudedegree();
			String circlelatmin = portalsurpicrequest.getCircleLatitudemin();
			String circlelatdir = portalsurpicrequest.getCircleLatitudedir();
			String circlelongdeg = portalsurpicrequest.getCircleLongitudedegree();
			String circlelongmin = portalsurpicrequest.getCircleLongitudemin();
			String circlelongdir = portalsurpicrequest.getCircleLongitudedir();
			String radius = portalsurpicrequest.getRadius();
			if (radius.length()==1)
			{
			 radius=00+radius;		
			}
			if (radius.length()==2)
			{
			 radius=0+radius;		
			}
			CircularArea = circlelatdeg + "." + circlelatmin + "." + circlelatdir + ":" + circlelongdeg + "." + circlelongmin + "." + circlelongdir + ":" + radius;

			logger.info("circular area==" + CircularArea);
			portalsurpicrequest.setCircularArea(CircularArea);
			surpic.setRectangularArea(null);
			surpic.setCircularArea(CircularArea);
		}
		Double sv = new Double(schema_version);
		BigDecimal schemaversion = BigDecimal.valueOf(sv.doubleValue());

		surpic.setSchemaVersion(schemaversion);
		surpic.setTest(BigInteger.valueOf(Long.parseLong(Test)));
		surpic.setNumberOfPositions(position);
		surpic.getShipTypes();

		LocalDateTime ldt = ts.toLocalDateTime();
		XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
		cal.setYear(ldt.getYear());
		cal.setMonth(ldt.getMonthValue());
		cal.setDay(ldt.getDayOfMonth());

		cal.setHour(ldt.getHour());
		cal.setMinute(ldt.getMinute());

		cal.setSecond(ldt.getSecond());
		cal.setFractionalSecond(new BigDecimal("0." + ldt.getNano()));

		System.out.println("calender in dc::" + cal);

		surpic.setAccessType(accesstype);
		surpic.setSchemaVersion(schemaversion);
		surpic.setMessageId(this.messageid);
		surpic.setDataUserRequestor(sarid);
		surpic.setDataUserProvider(null);
		surpic.setDDPVersionNum(ddpversionfinal);
		surpic.setMessageType(messagetype);
		surpic.setTimeStamp(cal);

		logger.info("sending sar surpic request to dc:" + surpic.toString());
		JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { SURPICRequest.class });
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(surpic, sw);
		logger.info("payload:" + sw.toString());
		portalreq.setRequestpayload(sw.toString());

		if (userCategory.equalsIgnoreCase("USER_CATEGORY_CG"))  
		{
			portalsurpicrequest.setPortalRequest(portalreq);
			manageRequest.savesurpicrequest(portalsurpicrequest);
			logger.info("sending SAR surpic request to dc:" + surpic.toString());
			try
			{

				String responseStatus = sc.callWebService( surpic);
				if (responseStatus.equalsIgnoreCase("true"))
				{
					model.addAttribute("successMsg", "SAR Surpic Request has been successfully submitted. MessageId=" + messageid);
					model.addAttribute("CircularArea", CircularArea);
					model.addAttribute("RectangularArea", RectangularArea);					
					manageRequest.setResponsePayload("success", messageid);
				}
				if  (!(responseStatus.equalsIgnoreCase("true")))
				{
					model.addAttribute("errorMsg", "Error Message : "+responseStatus+" MessageID= " + messageid);
					manageRequest.setResponsePayload(responseStatus, messageid);
				}
			}
			catch (Exception e)
			{
				logger.info("in catch in sarsurpic");
				model.addAttribute("errorMsg", "Could not able to send request to DC. MessageID= " + messageid);
				manageRequest.setResponsePayload("SoapFault", messageid);
				e.printStackTrace();
			}
		}
		if (!(userCategory.equalsIgnoreCase("USER_CATEGORY_CG")))
		{
			String message = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a SAR Surpic request with RequestType ";
			portalreq.setStatus("pending");
			portalreq.setStatusDate(ts);
			portalsurpicrequest.setPortalRequest(portalreq);
			PortalPendingTaskMst pendingtask = new PortalPendingTaskMst();
			pendingtask.setStatus("pending");
			pendingtask.setCategory("SARSurpic_Request");
			pendingtask.setMessage(message);
			pendingtask.setReferenceId(messageid);
			pendingtask.setRequestDate(ts);
			pendingtask.setRequesterLoginId(loginid);
			pendingtask.setRequestorLritid(requestorLritid);
			manageRequest.savesurpicrequest(portalsurpicrequest);
			manageRequest.savependingtask(pendingtask);
			model.addAttribute("CircularArea", CircularArea);
			model.addAttribute("RectangularArea", RectangularArea);	
			model.addAttribute("successMsg", "SAR Surpic Request has been sent for approval. Messageid=" + messageid);
		}

		return "requests/surpicrequestpage";
		//return "acknowledgementSurpic";
	}

	@PreAuthorize(value = "hasAuthority('sarpollrequest')")
	@RequestMapping(value={"/sarrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	public String getSarRequestPage(Model model)
	{
		logger.info("sar poll request page");
		model.addAttribute("portalsarsurpicrequest", new PortalSarRequest());
		return "requests/sarpoll";
	}

	@PreAuthorize(value = "hasAuthority('sarpollrequest')")
	@RequestMapping(value={"/getshipdetailsinsurpic"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	@ResponseBody
	public List<DcPositionReport> getShipdetailsinsurpic(Model model)
	{
		List<DcSarsurpicrequest> surpicarea=new ArrayList<DcSarsurpicrequest>();
		List<DcPositionReport> shipsinsurpic=new ArrayList<DcPositionReport>();
		logger.info("Inside getShip details In surpic area");
		try {
			surpicarea = this.ddpobj.getSurpicarea(6);

			logger.info("size=" + surpicarea.size());

			List<String> msgid = new ArrayList();
			for (int i = 0; i < surpicarea.size(); i++)
			{
				msgid.add(((DcSarsurpicrequest)surpicarea.get(i)).getMessageId());
				logger.info("messageid++=" + msgid);
			}
			shipsinsurpic = ddpobj.getShipsinSurpic(msgid);

			logger.info("size=" + shipsinsurpic);
			for (int i = 0; i < shipsinsurpic.size(); i++) {
				logger.info("ships=" + ((DcPositionReport)shipsinsurpic.get(i)).getMessageId());
			}
			model.addAttribute("shiplist", shipsinsurpic);
			return shipsinsurpic;
		}catch(Exception e)
		{
			//shipsinsurpic=null;
			logger.info("no ships in surpic");
			model.addAttribute("errorMsg", "No data available");
			return shipsinsurpic;
		}
		//return shipsinsurpic;
	}

	@PreAuthorize(value = "hasAuthority('sarpollrequest')")
	@RequestMapping(value={"/getreportingratesar"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	@ResponseBody
	public List<DcPositionRequest> getReportingratesar(String imo, int messagetype,int accesstype)
	{
		logger.info("Insidegetreportingrate sar with msgtype="+messagetype);

		List<Integer> accesstypelist=new ArrayList<Integer>();
		accesstypelist.add(accesstype);

		logger.info("IMOOOO=" + imo);
		List<DcPositionRequest> dcpositionreq = sd.getReportingratefromDc(imo, "active", messagetype, accesstypelist);

		logger.info("size of reporting rate=" + dcpositionreq.size());
		for (int i = 0; i < dcpositionreq.size(); i++)
		{
			logger.info("imo" + ((DcPositionRequest)dcpositionreq.get(i)).getImoNo());
			logger.info("reqtype" + ((DcPositionRequest)dcpositionreq.get(i)).getRequestType());
			logger.info("accesstype" + ((DcPositionRequest)dcpositionreq.get(i)).getAccessType());
		}
		return dcpositionreq;
	}

	@PreAuthorize(value = "hasAuthority('sarpollrequest')")
	@RequestMapping(value={"/persistsarrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	public String persistportalsarrequest(@Valid @ModelAttribute("portalsarsurpicrequest") PortalSarRequest portalsarrequest, Model model, BindingResult bindingResult)
			throws Exception
	{
		logger.info("in persist sar method");
		portalsarrequest.setAccessType("6");

		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes(); // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface  userSession = (UserSessionInterface) session.getAttribute("USER"); 
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
		String requestorLritid=lritIdmaster.getLritId();

		String sarid=(String) session.getAttribute("CoastGuard"); 

		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String ddpversionfinal = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			ddpversionfinal = ((DdpVersion)ddpversion.get(j)).getDdpversion();
		}
		logger.info("Regular version=" + ddpversionfinal);

		String Test = this.commonutil.getParavalues("Test");
		String schema_version = this.commonutil.getParavalues("Schema_Version");

		this.messageid = this.commonutil.generateMessageID(requestorLritid);
		logger.info("messageid=" + this.messageid);
		Timestamp ts = this.commonutil.getCurrentTimeStamp();
		logger.info("ts in portal=" + ts);

		PortalRequest portalreq = new PortalRequest();
		portalreq.setCurrentddpversion(ddpversionfinal);
		portalreq.setMessageId(this.messageid);
		portalreq.setRequestorsLoginId(loginid);
		portalreq.setRequestGenerationTime(ts);
		portalreq.setRequestCategory("SarRequest");

		portalsarrequest.setPortalRequest(portalreq);

		ShipPositionRequestType sar = new ShipPositionRequestType();
		Double sv = new Double(schema_version);
		BigDecimal schemaversion = BigDecimal.valueOf(sv.doubleValue());
		BigInteger messagetype = BigInteger.valueOf(5L);
		BigInteger accesstype = BigInteger.valueOf(6L);

		LocalDateTime ldt = ts.toLocalDateTime();
		XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
		cal.setYear(ldt.getYear());
		cal.setMonth(ldt.getMonthValue());
		cal.setDay(ldt.getDayOfMonth());
		cal.setHour(ldt.getHour());
		cal.setMinute(ldt.getMinute());
		cal.setSecond(ldt.getSecond());
		cal.setFractionalSecond(new BigDecimal("0." + ldt.getNano()));

		System.out.println("calender in dc::" + cal);

		sar.setAccessType(accesstype);
		sar.setSchemaVersion(schemaversion);
		sar.setMessageId(this.messageid);
		sar.setDataUserProvider(portalsarrequest.getUserProvider());
		sar.setDataUserRequestor(sarid);
		sar.setDDPVersionNum(ddpversionfinal);
		sar.setIMONum(portalsarrequest.getVesselImo());
		sar.setMessageType(messagetype);

		RequestDurationType requestduration = new RequestDurationType();
		if (portalsarrequest.getRequestType() == 7)
		{
			XMLGregorianCalendar starttime = null;
			XMLGregorianCalendar stoptime = null;
			GregorianCalendar gcdate = new GregorianCalendar();
			GregorianCalendar gcdate2 = new GregorianCalendar();
			gcdate.setTime(portalsarrequest.getStartTime());
			gcdate2.setTime(portalsarrequest.getEndTime());
			try
			{
				starttime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcdate);
				stoptime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcdate2);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			requestduration.setStartTime(starttime);
			requestduration.setStopTime(stoptime);

			logger.info("start:" + requestduration.getStartTime());
			logger.info("stop:" + requestduration.getStopTime());
			sar.setRequestDuration(requestduration);
		}
		else
		{
			sar.setRequestDuration(null);
		}
		sar.setPortFacility(null);

		sar.setRequestType(BigInteger.valueOf(portalsarrequest.getRequestType()));
		sar.setTimeStamp(cal);
		sar.setTest(BigInteger.valueOf(Long.parseLong(Test)));

		logger.info("sending request to dc:" + sar.toString());
		JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { ShipPositionRequestType.class });
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(sar, sw);
		logger.info("payload:" + sw.toString());
		//portalreq.setRequestpayload(sw.toString());
		//this.manageRequest.savesarrequest(portalsarrequest);
		if (requestorLritid.equalsIgnoreCase("1065"))
		{
			portalsarrequest.setPortalRequest(portalreq);
			manageRequest.savesarrequest(portalsarrequest);
			logger.info("sending SAR poll request to dc:" + sar.toString());

			try
			{

				String responseStatus = sc.callWebService( sar);
				if (responseStatus.equalsIgnoreCase("true"))
				{
					model.addAttribute("successMsg", "SAR Poll Request has been successfully submitted. MessageId=" + messageid);
					manageRequest.setResponsePayload("success", messageid);
				}
				if  (!(responseStatus.equalsIgnoreCase("true")))
				{
					model.addAttribute("errorMsg", "Error Message : "+responseStatus+" MessageID= " + messageid);
					manageRequest.setResponsePayload(responseStatus, messageid);
				}
			}
			catch (Exception e)
			{
				logger.info("in catch in sar");
				model.addAttribute("errorMsg", "Could not able to send request to DC. MessageID= " + messageid);
				manageRequest.setResponsePayload("SoapFault", messageid);
				e.printStackTrace();
			}
		}
		if (!(requestorLritid.equalsIgnoreCase("1065")))
		{
			String message = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a SAR POll request with RequestType "+portalsarrequest.getRequestType();
			portalreq.setStatus("pending");
			portalreq.setStatusDate(ts);
			portalsarrequest.setPortalRequest(portalreq);
			PortalPendingTaskMst pendingtask = new PortalPendingTaskMst();
			pendingtask.setStatus("pending");
			pendingtask.setCategory("SAR_Requests");
			pendingtask.setMessage(message);
			pendingtask.setReferenceId(messageid);
			pendingtask.setRequestDate(ts);
			pendingtask.setRequesterLoginId(loginid);
			pendingtask.setRequestorLritid(requestorLritid);
			manageRequest.savesarrequest(portalsarrequest);
			manageRequest.savependingtask(pendingtask);
			model.addAttribute("successMsg", "SAR POll Request has been sent for approval. Messageid=" + messageid);
		}
		return "acknowledgement";
	}

	@PreAuthorize(value = "hasAuthority('coastalrequest')")
	@RequestMapping(value={"/coastalrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	public String getcoastalRequestPage(Model model)
	{
		logger.info("coastal  request page");
		model.addAttribute("portalcoastalsurpicrequest", new PortalCoastalRequest());
		return "requests/coastalrequestpage";
	}

	@PreAuthorize(value = "hasAuthority('coastalrequest')")
	@RequestMapping(value={"/persistcoastalrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	public String persistportalcoastalrequest(@Valid @ModelAttribute("portalsarsurpicrequest") PortalCoastalRequest portalcoastalrequest, Model model, BindingResult bindingResult)
			throws Exception
	{
		logger.info("in persist coastal method");
		portalcoastalrequest.setAccessType("1");

		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes(); // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface  userSession = (UserSessionInterface) session.getAttribute("USER"); 
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId();  //get lritid from session
		String requestorLritid=lritIdmaster.getLritId();



		List<DdpVersion> ddpversion = ddpobj.getDdpVersionview();
		String ddpversionfinal = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			ddpversionfinal = ((DdpVersion)ddpversion.get(j)).getDdpversion();
		}
		logger.info("Regular version=" + ddpversionfinal);

		String Test = this.commonutil.getParavalues("Test");
		String schema_version = this.commonutil.getParavalues("Schema_Version");

		this.messageid = this.commonutil.generateMessageID(requestorLritid);
		logger.info("messageid=" + this.messageid);
		Timestamp ts = this.commonutil.getCurrentTimeStamp();
		logger.info("ts in portal=" + ts);

		PortalRequest portalreq = new PortalRequest();
		portalreq.setCurrentddpversion(ddpversionfinal);
		portalreq.setMessageId(this.messageid);
		portalreq.setRequestorsLoginId(loginid);
		portalreq.setRequestGenerationTime(ts);
		portalreq.setRequestCategory("CoastalRequest");

		//portalcoastalrequest.setPortalRequest(portalreq);

		ShipPositionRequestType coastal = new ShipPositionRequestType();
		Double sv = new Double(schema_version);
		BigDecimal schemaversion = BigDecimal.valueOf(sv.doubleValue());
		BigInteger messagetype = BigInteger.valueOf(4L);
		BigInteger accesstype = BigInteger.valueOf(1L);

		LocalDateTime ldt = ts.toLocalDateTime();
		XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
		cal.setYear(ldt.getYear());
		cal.setMonth(ldt.getMonthValue());
		cal.setDay(ldt.getDayOfMonth());
		cal.setHour(ldt.getHour());
		cal.setMinute(ldt.getMinute());
		cal.setSecond(ldt.getSecond());
		cal.setFractionalSecond(new BigDecimal("0." + ldt.getNano()));

		System.out.println("calender in dc::" + cal);

		logger.info("data user provider"+portalcoastalrequest.getUserProvider());
		coastal.setAccessType(accesstype);
		coastal.setSchemaVersion(schemaversion);
		coastal.setMessageId(this.messageid);
		coastal.setDataUserProvider(portalcoastalrequest.getUserProvider());//need o change
		coastal.setDataUserRequestor(requestorLritid);
		coastal.setDDPVersionNum(ddpversionfinal);
		coastal.setIMONum(portalcoastalrequest.getVesselImo());
		coastal.setMessageType(messagetype);

		RequestDurationType requestduration = new RequestDurationType();
		if ((portalcoastalrequest.getRequestType().intValue() == 2) || (portalcoastalrequest.getRequestType().intValue() == 6) || (portalcoastalrequest.getRequestType().intValue() == 10) 
				|| (portalcoastalrequest.getRequestType().intValue() == 11)
				|| (portalcoastalrequest.getRequestType().intValue() == 8)|| (portalcoastalrequest.getRequestType().intValue() == 3)|| (portalcoastalrequest.getRequestType().intValue() == 4|| (portalcoastalrequest.getRequestType().intValue() == 5|| (portalcoastalrequest.getRequestType().intValue() == 7))))
		{
			XMLGregorianCalendar starttime = null;
			XMLGregorianCalendar stoptime = null;
			GregorianCalendar gcdate = new GregorianCalendar();
			GregorianCalendar gcdate2 = new GregorianCalendar();
			gcdate.setTime(portalcoastalrequest.getStartTime());
			gcdate2.setTime(portalcoastalrequest.getEndTime());
			try
			{
				starttime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcdate);
				stoptime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcdate2);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			requestduration.setStartTime(starttime);
			requestduration.setStopTime(stoptime);

			logger.info("start:" + requestduration.getStartTime());
			logger.info("stop:" + requestduration.getStopTime());
			coastal.setRequestDuration(requestduration);
		}
		else
		{
			coastal.setRequestDuration(null);
		}
		coastal.setPortFacility(null);

		coastal.setRequestType(BigInteger.valueOf(portalcoastalrequest.getRequestType().intValue()));
		coastal.setTimeStamp(cal);
		coastal.setTest(BigInteger.valueOf(Long.parseLong(Test)));

		logger.info("sending request to dc:" + coastal.toString());
		JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { ShipPositionRequestType.class });
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(coastal, sw);
		logger.info("payload:" + sw.toString());
		portalreq.setRequestpayload(sw.toString());
		//this.manageRequest.savecoastalrequest(portalcoastalrequest);
		if (requestorLritid.equalsIgnoreCase("1065"))
		{
			portalcoastalrequest.setPortalRequest(portalreq);
			manageRequest.savecoastalrequest(portalcoastalrequest);
			logger.info("sending coastal request to dc:" + coastal.toString());

			try
			{

				String responseStatus = sc.callWebService( coastal);
				if (responseStatus.equalsIgnoreCase("true"))
				{
					model.addAttribute("successMsg", "coastal Request has been successfully submitted. MessageId=" + messageid);
					manageRequest.setResponsePayload("success", messageid);
				}
				if  (!(responseStatus.equalsIgnoreCase("true")))
				{
					model.addAttribute("errorMsg", "Error Message : "+responseStatus+" MessageID= " + messageid);
					manageRequest.setResponsePayload(responseStatus, messageid);
				}
			}
			catch (Exception e)
			{
				logger.info("in catch in flag");
				model.addAttribute("errorMsg", "Could not able to send request to DC. MessageID= " + messageid);
				manageRequest.setResponsePayload("SoapFault", messageid);
				e.printStackTrace();
			}
		}
		if (!(requestorLritid.equalsIgnoreCase("1065")))
		{
			String message = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a coastal request with RequestType "+portalcoastalrequest.getRequestType();
			portalreq.setStatus("pending");
			portalreq.setStatusDate(ts);
			portalcoastalrequest.setPortalRequest(portalreq);
			PortalPendingTaskMst pendingtask = new PortalPendingTaskMst();
			pendingtask.setStatus("pending");
			pendingtask.setCategory("Coastal_Requests");
			pendingtask.setMessage(message);
			pendingtask.setReferenceId(messageid);
			pendingtask.setRequestDate(ts);
			pendingtask.setRequesterLoginId(loginid);
			pendingtask.setRequestorLritid(requestorLritid);
			manageRequest.savecoastalrequest(portalcoastalrequest);
			manageRequest.savependingtask(pendingtask);
			model.addAttribute("successMsg", "Coastal Request has been sent for approval. Messageid=" + messageid);
		}
		return "acknowledgement";
	}
}