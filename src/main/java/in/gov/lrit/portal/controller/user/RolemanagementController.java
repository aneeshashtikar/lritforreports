/**

 * @RolemanagementController.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author   Bhavika Ninawe
 * @version 1.0
 * RolemanagementController Controller to handle all the role based access.
 */
package in.gov.lrit.portal.controller.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import in.gov.lrit.portal.model.role.PortalActivities;
import in.gov.lrit.portal.model.role.PortalRoles;
import in.gov.lrit.portal.service.user.IRoleManagementService;

@Controller
@RequestMapping(value = "/roles")
public class RolemanagementController {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RolemanagementController.class);

	@Autowired
	IRoleManagementService rolemanagementService;

	/**
	 * This method returns a page for create role
	 * 
	 * @return String createrole page
	 */
	@PreAuthorize(value = "hasAuthority('createrole')")
	@RequestMapping(method = RequestMethod.GET, value = "/createrole")
	public String createRole(HttpServletRequest request, Model model) {
		logger.info("Inside CreateRole controller method");
		List<PortalActivities> activities = new ArrayList<PortalActivities>();
		activities = rolemanagementService.getActivities();
		model.addAttribute("activities", activities);
		model.addAttribute("portalrole", new PortalRoles());
	
		logger.info("displaying createRole jsp");
		return "role/createrole";

	}

	/**
	 * This method persist the role and provide corresponding message as per
	 * condition and on success it return to createrole page
	 * 
	 * @return String createrole page
	 */
	@PreAuthorize(value = "hasAuthority('createrole')")
	@RequestMapping(method = RequestMethod.POST, value = "/persistrole")
	public String persistRole(@Valid @ModelAttribute("portalrole") PortalRoles portalRoles, Model model,
			BindingResult bindingResult) {
		logger.info("Inside Persisting roles controller method . Portal Role: " + portalRoles.toString());
		List<PortalActivities> activities = new ArrayList<PortalActivities>();
		
		// Check role name already exist in database
		boolean flag = rolemanagementService.checkRoleName(portalRoles.getRolename());
		if (!flag) {

			logger.info("roleName not exist in database");
			boolean existShortName = rolemanagementService.checkShorName(portalRoles.getShortname());
			if (!existShortName) {
				rolemanagementService.saveRole(portalRoles);

				activities = rolemanagementService.getActivities();
				model.addAttribute("activities", activities);
				model.addAttribute("successMsg", "Role has been created");
				logger.info("Outside registerrole method");
				model.addAttribute("portalrole", new PortalRoles());
			} else {

				model.addAttribute("errorMsg", "ShortName already exist in database");
				model.addAttribute("activities", portalRoles.getActivities());
				model.addAttribute("portalrole", portalRoles);
			}

		} else {

			model.addAttribute("errorMsg", "RoleName already exist in database");
			model.addAttribute("activities", portalRoles.getActivities());
			model.addAttribute("portalrole", portalRoles);
		}

		return "role/createrole";

	}

	/**
	 * This method view the page of viewrole
	 * 
	 * @return String createrole page
	 */
	@PreAuthorize(value = "hasAnyAuthority('viewrole','deleterole','editrole')")
	@RequestMapping(method = RequestMethod.GET, value = "/viewrole")
	/*@Transactional*/
	public String viewRole(Model model) {
		logger.info("Inside viewRole method");
		return "role/viewrole";

	}

	/**
	 * This method populates the list of roles and return list of roles in json
	 * 
	 * @return String json
	 */
	@PreAuthorize(value = "hasAnyAuthority('viewrole','deleterole','editrole')")
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/getrolelist")
	public String getRoleList(String Status) {
		logger.info("Inside getRoleList method. Status :" + Status);
		Gson gson = new Gson();
		String json = gson.toJson(rolemanagementService.getRoles(Status));
		logger.info("usermanagementService.getRoles() : ");
		return json;
	}

	/**
	 * This method used for getting role details using roleid
	 * 
	 * @return String viewroledetail
	 */
	@PreAuthorize(value = "hasAnyAuthority('viewrole','deleterole','editrole')")
	@RequestMapping(method = RequestMethod.GET, value = "/viewroledetails")
	public String viewRole(@RequestParam("roleId") String roleId, Model model) {
		logger.info("Inside viewRole method. roleId: " + roleId);
		if (roleId == null) {
			logger.info("roleId is null.");
			model.addAttribute("errorMsg", "Invalid roleId");
			return "role/viewrole";
		}

		PortalRoles portalrole = rolemanagementService.getRoleDetails(roleId);

		if (portalrole == null) {
			logger.info("Role details fetched from database is null. roleid " + roleId);
			model.addAttribute("errorMsg", "Invalid roleId");
			return "role/viewrole";
		}
		logger.info("Role details fetched from database. PortalRole :" + portalrole.toString());

		model.addAttribute("portalrole", portalrole);
		model.addAttribute("activities", portalrole.getActivities());
		return "role/viewroledetail";
	}

	/**
	 * This method used to deregister role using role id
	 * 
	 * @return String json
	 */
	@PreAuthorize(value = "hasAuthority('deleterole')")
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/deregisterrole")
	public String deregisterRole(String roleId, Model model, HttpServletRequest request) {
		logger.info("Inside deregisterRole method " + roleId);

		// Change the status to deactivate in portal role table
		rolemanagementService.deregisterRole(roleId);
		logger.info("role has been deregistered");
		model.addAttribute("successMsg", "Role has been Deactivated");

		Gson gson = new Gson();
		String json = gson.toJson("Role has been Inactivated");

		return json;

	}

	/**
	 * This method used to show data in edit role page using role id
	 * 
	 * @return String editrole
	 */
	@PreAuthorize(value = "hasAuthority('editrole')")
	@RequestMapping(method = RequestMethod.GET, value = "/editrole")
	public String editRole(@RequestParam("roleId") String roleId, Model model) {
		logger.info("Inside editRole method. roleId: " + roleId);
		PortalRoles portalrole = rolemanagementService.getRoleDetails(roleId);

		if (portalrole == null) {
			logger.info("portalrole is null ");
			model.addAttribute("errorMsg", "Invalid roleId");
			return "role/viewrole";
		}
		logger.info("portal role details " + portalrole.toString());

		// fetch all activities
		List<PortalActivities> activities = new ArrayList<PortalActivities>();
		activities = rolemanagementService.getActivities();
		model.addAttribute("portalrole", portalrole);

		Set<PortalActivities> assignedActivities = portalrole.getActivities();
		logger.info("assignedActivities length " + assignedActivities.size());

		if (assignedActivities.size() != 0) {
			logger.info("assigned activities is not null");
			for (PortalActivities portalActivities : activities) {
				for (PortalActivities act : assignedActivities) {

					if (portalActivities.getActivityId().equals(act.getActivityId())) {

						portalActivities.setFlag(true);
						break;
					}
				}
			}
		}
		logger.info("after setting flag values");
		model.addAttribute("activities", activities);
		return "role/editrole";
	}

	/**
	 * This method used to save edited role
	 * 
	 * @return String editrole
	 */
	@PreAuthorize(value = "hasAuthority('editrole')")
	@RequestMapping(method = RequestMethod.POST, value = "/saveeditrole")
	public String editRole(@Valid @ModelAttribute("portalrole") PortalRoles portalRoles, Model model) {
		logger.info("Inside persisteditRole method ");
		logger.info("Activity assigned after Edit " + portalRoles.toString());
		PortalRoles users = rolemanagementService.getRoleDetails(portalRoles.getRoleId());
		logger.info("\nActivity assigned before Edit " + users);
		rolemanagementService.editRole(portalRoles);
		model.addAttribute("portalrole", portalRoles);
		List<PortalActivities> activities = new ArrayList<PortalActivities>();
		activities = rolemanagementService.getActivities();
		model.addAttribute("activities", activities);
		Set<PortalActivities> assignedActivities = portalRoles.getActivities();
		logger.info("assignedActivities length " + assignedActivities.size());
		if (assignedActivities.size() != 0) {
			logger.info("assigned activities is not null");
			for (PortalActivities portalActivities : activities) {
				for (PortalActivities act : assignedActivities) {

					if (portalActivities.getActivityId().equals(act.getActivityId())) {

						portalActivities.setFlag(true);
						break;
					}
				}
			}
		}
		model.addAttribute("successMsg", "Role has been edited");
		return "role/editrole";
	}
}
