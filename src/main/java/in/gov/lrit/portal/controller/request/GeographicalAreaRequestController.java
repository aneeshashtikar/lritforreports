/**

 * @GeographicalAreaRequest.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author   Kavita Sharma
 * @version 1.0
 * Geographical Area Request Controller to handle all the geographical area update request
 */
package in.gov.lrit.portal.controller.request;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.gson.Gson;

import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaModel;
import in.gov.lrit.portal.gis.lritdb.model.GeographicalAreaInterface;
import in.gov.lrit.portal.model.request.PortalGeoraphicalArea;
import in.gov.lrit.portal.service.request.GeographicalAreaService;

@Controller
@RequestMapping("/requests")
public class GeographicalAreaRequestController {

	@Autowired
	private GeographicalAreaService geographicalAreaService;
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GeographicalAreaRequestController.class);
	
	
	/**
	 * This mehtod returns a page containing list of geo area pending for ide approval.
	 * User can Delete, Send to IDE and Edit the geo area from this page
	 * @return String ga_for_ide_approval page
	 */
	@PreAuthorize(value = "hasAuthority('viewGeographicalArea')")

	@RequestMapping("/gaforideapproval")
	public String getGeographicalAreaPage(HttpServletRequest request,Model model)
	{
		return "/requests/ga_for_ide_approval";
	}
	
	/**
	 * This mehtod returns a List of Geo Area based on the Status 
	 * @return List of Goegraphical Area
	 */
	@PreAuthorize(value = "hasAuthority('viewGeographicalArea')")

	@RequestMapping(value="/showGeographicalAreaList",  method = RequestMethod.GET)
	@ResponseBody
	public List<GeographicAreaModel> showGeographicalAreaList(String geoStatus)
	{
		logger.info("inside GeographicalAreaRequest ");
		logger.info("Getting the Geographical Area List for status: " +geoStatus);
		List<GeographicAreaModel> geoArea=geographicalAreaService.showGeographicalAreaList(geoStatus);
		logger.info("the data from the database" + geoArea);
		//Gson gson = new Gson();
		//String json = gson.toJson(geoArea);
		//logger.info("data from the database json"+json);
		return geoArea;
	}
	
	/**
	 * This mehtod sends the geo area deletion request to DDP. Which already been approved by IDE and added to DDP
	 * @return Succes message or error message
	 */
	@PreAuthorize(value = "hasAuthority('deleteGeographicalArea')")
	@RequestMapping(value="/deleteGeoAreaFromDDP",  method = RequestMethod.POST)
	@ResponseBody
	public String  deleteGeographicalAreaFromDDP(@RequestParam("gmlId")int gmlId, Model model) throws IOException
	{
		Integer actionType=2;
		String message=null;
		logger.info("inside controller data"+gmlId);
		message = geographicalAreaService.geographcalAreaUpdate(gmlId, actionType);
		return message;
	}
	
	/**
	 * This method sends the Geo area update request to IDE to approve the Geo Area
	 * @return Succes message or error message
	 */
	@PreAuthorize(value = "hasAuthority('processingGeographicalArea')")

	@RequestMapping(value="/sendtoide" , method = RequestMethod.POST)
	@ResponseBody
	public String  sendToIde(int gmlId, Model model ) throws IOException
	{
		Integer actionType=0;
		String message;
		logger.info("Inside send to IDE controller");
		logger.info("Sending IDE approval request for AreaCode" +gmlId);
		message=geographicalAreaService.geographcalAreaUpdate(gmlId, actionType);
		
		Gson gson = new Gson();
		String json = gson.toJson(message);
		return json;
		
	}
	
	
	/**
	 * @return returns jsp ga_disapprove_from_ide
	 */
	@PreAuthorize(value = "hasAnyAuthority('viewGeographicalArea','processingGeographicalArea')")

	@RequestMapping("/gadisapprovefromide")
	public String getDisapproveGeographicalAreaPage(HttpServletRequest request, Model model) {
		return "/requests/ga_disapprove_from_ide";
	}
	 
	
	/**
	 * @return returns jsp ga_send_to_ddp
	 */
	@PreAuthorize(value = "hasAuthority('viewGeographicalArea')")

	@RequestMapping("/galistsendtoddp")
	public String getGaListSendToDdp(HttpServletRequest request,Model model)
	{
		return "/requests/ga_send_to_ddp";
	}
	
	/**
	 * This method sends the Geo area update  to DDP 
	 * @return Succes message or error message
	 */
	
	@PreAuthorize(value = "hasAuthority('processingGeographicalArea')")
	@RequestMapping(value="/sendtoDdp" , method = RequestMethod.POST)
	@ResponseBody
	public String sendToDdp(int gmlId, Model model ) throws IOException
	{
		Integer actionType=1;
		String message=null;
		logger.info("Inside send to IDE controller");
		logger.info("Sending IDE approval request for AreaCode" + gmlId);
		message = geographicalAreaService.geographcalAreaUpdate(gmlId, actionType);
		Gson gson = new Gson();
		String json = gson.toJson(message);
		return json;
		
	}
	
	
	/**
	 * @return ga_approved_from_ddp
	 */
	@PreAuthorize(value = "hasAuthority('viewGeographicalArea')")

	@RequestMapping("/gaapprovedfromddp")
	public String getGeographicalAreaApprovedFromDdp(HttpServletRequest request,Model model)
	{
		return "/requests/ga_approved_from_ddp";
	}
	
	/**
	 * This method DELTES THE Geo area from local DB
	 * @return Succes message or error message
	 */
	@PreAuthorize(value = "hasAuthority('processingGeographicalArea')")

	@RequestMapping(value="/deleteGeoArea",  method = RequestMethod.POST)
	@ResponseBody
	public String  deleteGeographicalArea(@RequestParam("gmlId")int gmlId, Model model) throws IOException
	{
		String message;
		logger.info("inside delete geographical area controller , gmaiId: " + gmlId);
		int result = geographicalAreaService.deleteGeoArea(gmlId);
		if(result==1) {
			logger.info("inside success");
			message="Geographical Area Deleted Successfully";
		}
		else {
			message="Failed to Delete Geographical Area ";
		}
		return message;
	}
	
}
