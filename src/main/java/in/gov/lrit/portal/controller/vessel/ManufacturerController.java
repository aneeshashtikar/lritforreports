package in.gov.lrit.portal.controller.vessel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import in.gov.lrit.portal.model.vessel.PortalManufacturerDetails;
import in.gov.lrit.portal.service.vessel.ManufacturerDetailsService;
import in.gov.lrit.portal.service.vessel.ModelService;
import in.gov.lrit.portal.service.vessel.VesselGroupService;

@Controller
@RequestMapping("/manufacturer")
public class ManufacturerController {

	@Autowired
	private ManufacturerDetailsService manufacturerService;
	
	@Autowired
	private ModelService modelService;
	
	private VesselGroupService service;
	
	private static final Logger logger = LoggerFactory.getLogger(ModelController.class);

	//this method is used to view all Manufacturer
	@PreAuthorize(value = "hasAnyAuthority('editmanufacturerandmodel','viewmanufacturerandmodel')")
	@RequestMapping(value="/viewallmanufacturer", method=RequestMethod.GET)
	public String viewManufacturer(Model model)
	{
		logger.info("inside viewManufacturer() method");
		model.addAttribute("portalManu", new PortalManufacturerDetails());
		return "/vessel/allmanufacturer";
	}
	
	//this method is used to get list of Manufacturer
	@PreAuthorize(value = "hasAnyAuthority('editmanufacturerandmodel','viewmanufacturerandmodel')")
	@RequestMapping(value="/getmanufacturer", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<PortalManufacturerDetails> getManufacturerList()
	{	
		logger.info("inside getManufacturerList() method");
		Iterable<PortalManufacturerDetails> manulist = manufacturerService.getAllManufacturer();
		return manulist;
	}
	
	//this method is used to persist Manufacturer
	@PreAuthorize(value = "hasAuthority('editmanufacturerandmodel')")
	@RequestMapping(value="/persistmanufacturer", method=RequestMethod.POST)
	public String persistManufacturer(PortalManufacturerDetails newManu, Model model)
	{
		logger.info("inside persistManufacturer() method");
		logger.info(newManu.toString());

		boolean flag = false;
		String manufacturerName = newManu.getManufacturerName();

		/*Check if manufacturer name contains alphabet only*/
		flag=service.isStringOnlyAlphabet(manufacturerName);
		if(flag == false)
		{
			logger.error("Device Manufacturer Name should contains only alphabet");
			model.addAttribute("error", "Device Manufacturer Name should contains only alphabet");			
		}
		else
		{
			/*Check if manufacturer name already present*/
			flag = manufacturerService.isManufacturerExist(manufacturerName);
			if(flag == true)
			{
				logger.error("Device Manufacturer with name already exist");
				model.addAttribute("error", "Device Manufacturer with name already exist");
			}
			else
			{
				/*Persisting new manufacture entry in portal_model_details table*/ 
				newManu.setStatus("Active");
				PortalManufacturerDetails manu = manufacturerService.addManufacturer(newManu);
				logger.info("Device Manufacturer persists successfully in portal_model_details table");
				model.addAttribute("success", "Device Manufacturer has been added successfully");
			}
		}
		model.addAttribute("portalManu", new PortalManufacturerDetails());
		return "/vessel/allmanufacturer";
	}
	
	//this method is used to delete Manufacturer
	@PreAuthorize(value = "hasAuthority('editmanufacturerandmodel')")
	@RequestMapping(value="/deletemanufacturer/{manufacturerId}", method=RequestMethod.GET)
	public String deleteManufacturer(@PathVariable Integer manufacturerId, Model model)
	{
		boolean flag=false;
		logger.info("inside deleteManufacturer() method");
		logger.info("manufacturerId " + manufacturerId);
		
		flag = modelService.updateModelByManuId(manufacturerId);
		if(flag == true)
		{
			flag = manufacturerService.updateManuById(manufacturerId);
			if(flag == true)
			{ 
				logger.info("Device Manufacturer and Model deleted successfully");
				model.addAttribute("success", "Device Manufacturer and Model has been deleted successfully");
			}
		}
		else
		{
			logger.info("Device Manufacturer cannot be deleted");
			model.addAttribute("error", "Device Manufacturer cannot be deleted");
		}
		model.addAttribute("portalManu", new PortalManufacturerDetails());
		return "/vessel/allmanufacturer";
	}
}