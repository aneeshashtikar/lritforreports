package in.gov.lrit.portal.controller.alert;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import com.google.gson.Gson;
import in.gov.lrit.portal.model.alert.AlertDetails;/*
import in.gov.lrit.portal.model.alert.ClosedAlertDetails;*/
import in.gov.lrit.portal.model.alert.AlertActionDetails;
import in.gov.lrit.portal.model.alert.AlertActionDetailsDTO;
import in.gov.lrit.portal.service.alert.IAlertService;
import in_.gov.lrit.session.ContractingGovernmentInterface;
import in_.gov.lrit.session.UserSessionInterface;

@Controller
@RequestMapping("/alerts")
public class AlertController {
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AlertController.class);
	
	@Autowired IAlertService alertService;
	
	@RequestMapping("/viewallalerts")
	public String viewAllAlerts(Model model, @SessionAttribute("USER") UserSessionInterface user)
	{
		String loginId = user.getLoginId();
		String userCategory = user.getCategory();
		model.addAttribute("loginId", loginId);
		model.addAttribute("userCategory", userCategory);
		return "/alerts/viewallalerts";
	}
	
	@RequestMapping(value="/getallalerts", method=RequestMethod.POST)
	@ResponseBody
	public List<AlertDetails> getAllAlerts(@SessionAttribute("USER") UserSessionInterface user, HttpSession session)
	{
		String loginId = user.getLoginId();
		String userCategory = user.getCategory();
		List<String> roleList = alertService.getRoleAgainstLoginId(loginId);
		List<ContractingGovernmentInterface> contextList = new ArrayList<ContractingGovernmentInterface>();
		contextList = (List<ContractingGovernmentInterface>) session.getAttribute("context");
		List<String> contextIdList = new ArrayList<String>();
		if (contextList != null) {
			for (ContractingGovernmentInterface context : contextList) {
				contextIdList.add(context.getLritId());
			}
			contextIdList.add("3065");
		} 
		for (String string : contextIdList) { logger.info("contextid="+string); }
		
		List<AlertDetails> alertlist = alertService.findAllAlerts(roleList, userCategory, contextIdList);
		return alertlist;
	}
	
	@RequestMapping("/viewveryhighseverity")
	public String viewVeryHighSeverity(@RequestParam("severity") String severity, Model model, @SessionAttribute("USER") UserSessionInterface user)
	{
		String loginId = user.getLoginId();
		String userCategory = user.getCategory();
		model.addAttribute("loginId", loginId);
		model.addAttribute("userCategory", userCategory);
		logger.info("user cat="+ userCategory);
		model.addAttribute("Severity", severity);
		return "/alerts/alertswithseverity";
	}
	
	
	  @RequestMapping(value="/getalertswithseverity", method=RequestMethod.POST)
	  @ResponseBody
	  public List<AlertDetails> alertWithSeverity(String severity, Model model, @SessionAttribute("USER") UserSessionInterface user, HttpSession session)
	  { 
		String loginId = user.getLoginId();
		String category = user.getCategory();
		List<String> roleList = alertService.getRoleAgainstLoginId(loginId);
		List<ContractingGovernmentInterface> contextList = new ArrayList<ContractingGovernmentInterface>();
		contextList = (List<ContractingGovernmentInterface>) session.getAttribute("context");
		List<String> contextIdList = new ArrayList<String>();
		if (contextList != null) {
			for (ContractingGovernmentInterface context : contextList) {
				contextIdList.add(context.getLritId());
			}
			contextIdList.add("3065");
		} 
			  for (String string : contextIdList) { logger.info("contextid="+string); }
			 
		  List<AlertDetails> userSpecificAlerts= new ArrayList<AlertDetails>();
		  if (category.equals("USER_CATEGORY_SC"))
		  {
			  userSpecificAlerts = alertService.getShippingCoAlerts(roleList, severity, loginId);  
		  }
		  else
		  {
			  logger.info("category=" +category);
			  userSpecificAlerts = alertService.getUserSpecificAlerts(roleList, severity, category, contextIdList);  
		  }
		  
			return userSpecificAlerts;
		  
	  }
	  
	
	  @RequestMapping(value="/getalertcount", method = RequestMethod.GET)
	  @ResponseBody 
	  public  Map<String, Integer> getAlertCount(@SessionAttribute("USER") UserSessionInterface user, HttpSession session)
	  {
		  String loginId = user.getLoginId();
		  String category = user.getCategory();
		  List<String> roleList = alertService.getRoleAgainstLoginId(loginId);
		  List<ContractingGovernmentInterface> contextList = new ArrayList<ContractingGovernmentInterface>();
			contextList = (List<ContractingGovernmentInterface>) session.getAttribute("context");
			logger.info("contextList="+ contextList);
			List<String> contextIdList = new ArrayList<String>();
			if (contextList != null) {
				for (ContractingGovernmentInterface context : contextList) {
					contextIdList.add(context.getLritId());
				}
				contextIdList.add("3065");
			} 
			for (String string : contextIdList) { logger.info("contextid="+string); }
		  
		  Map<String, Integer> alertCount = new HashMap<String, Integer>();
		  
		  if (category.equals("USER_CATEGORY_SC"))
		  {
			  alertCount = alertService.getShippingCoAlertCount(roleList, loginId); 
		  }
		  else
		  {
			  alertCount = alertService.getUserSpecificAlertCount(roleList, category, contextIdList);
		  }
		  return alertCount;
		  
	  }
	  
	@RequestMapping(value = "/insertintoalerttxn", method = RequestMethod.POST)
	@ResponseBody
	public String closeAlert(BigInteger alertTxnID, String loginId, String remark, String userCategory, Model model) {
		String msg = null;
		logger.info("alertTxnNo=" + alertTxnID + "-" + "loginId" + loginId + "-" + "remark" + remark + "-" +userCategory);
		int flag = alertService.insertIntoAlertActionTaken(alertTxnID, loginId, remark, userCategory); // data is inserted using
																							// insert query not using
																							// pojo.
		if (flag == 1)
			msg = "Alert Closed Successfully";
		else
			msg = "Something went wrong!! please check";

		return msg;

	}
	
	@RequestMapping("/getClosedAlertsPage")
	public String getClosedAlertsPage()
	{
		return "/alerts/viewclosedalerts";
	}
	
	
	@RequestMapping(value = "/getClosedAlerts", method = RequestMethod.POST)
	@ResponseBody
	public List<AlertActionDetailsDTO> getClosedAlerts(Date startDate, Date endDate, HttpSession session, @SessionAttribute("USER") UserSessionInterface user)
	{
		String userCategory = user.getCategory();
		logger.info("category in closed alert="+userCategory);
		List<ContractingGovernmentInterface> contextList = new ArrayList<ContractingGovernmentInterface>();
		contextList = (List<ContractingGovernmentInterface>) session.getAttribute("context");
		logger.info("contextList="+ contextList);
		List<String> contextIdList = new ArrayList<String>();
		if (contextList != null) {
			for (ContractingGovernmentInterface context : contextList) {
				contextIdList.add(context.getLritId());
			}
			contextIdList.add("3065");
		} 
		for (String string : contextIdList) { logger.info("contextid="+string); }
		
		List<AlertActionDetailsDTO> closedAlertsList = new ArrayList<AlertActionDetailsDTO>();
		closedAlertsList = alertService.getClosedAlerts(userCategory,contextIdList, startDate, endDate);
		
		for (AlertActionDetailsDTO closedAlertsDetails : closedAlertsList) {
			logger.info("closedAlerts=" + closedAlertsDetails);
		}
		return closedAlertsList;
	}
}
