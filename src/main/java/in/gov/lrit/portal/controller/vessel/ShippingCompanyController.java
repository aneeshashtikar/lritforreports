package in.gov.lrit.portal.controller.vessel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in.gov.lrit.portal.service.vessel.ShippingCompanyService;

@Controller
@RequestMapping("/company")
public class ShippingCompanyController {
	
	@Autowired
	private ShippingCompanyService companyService;

	private static final Logger logger = LoggerFactory.getLogger(ShippingCompanyController.class);
	
	//this method is used to all company list
	@PreAuthorize(value = "hasAuthority('viewvesselcompanywise')")
	@RequestMapping(value="/allcompanylist", method=RequestMethod.GET)
	public String allCompanyList()
	{
		logger.info("inside allCompanyList() method");
		return "/vessel/allcompany";
	}
	
	//this method is used to get all registered company list
	@RequestMapping(value="/getallcompany", method = RequestMethod.GET)
	@ResponseBody
	public Iterable<PortalShippingCompany> getAllCompany()
	{
		logger.info("inside getAllCompany() company method");
		Iterable<PortalShippingCompany> list = companyService.getAllCompanyList();	
		return list;
	}
	
}