package in.gov.lrit.portal.controller.vessel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import in.gov.lrit.portal.model.user.PortalUser;
import in.gov.lrit.portal.model.vessel.PortalVesselDetail;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.model.vessel.VesselListInterface;
import in.gov.lrit.portal.service.vessel.UserService;
import in.gov.lrit.portal.service.vessel.VesselService;
import in_.gov.lrit.session.UserSessionInterface;

@Controller
@RequestMapping("/vessels")
public class VesselDetailController {

	@Autowired
	private VesselService vesselService;

	@Autowired
	private ShipEquipmentController controller;

	@Autowired
	private UserService userService;

	private Logger logger = LoggerFactory.getLogger(VesselDetailController.class);

	//this method is used to show submitted vessel list
	@PreAuthorize(value = "hasAnyAuthority('approvevesselregistration','viewvessel')")
	@RequestMapping(value="/submittedvessel", method=RequestMethod.GET)
	public String submittedVesselDetails()
	{
		logger.info("inside submittedVessel() method");
		return "/vessel/submittedship";
	}

	//this method is used to get submitted vessel list
	@PreAuthorize(value = "hasAnyAuthority('approvevesselregistration','viewvessel')")
	@RequestMapping("/getsubmittedvessel")
	@ResponseBody
	public ArrayList<VesselListInterface> getSubmittedVesselDetails()
	{
		logger.info("inside getSubmittedVessel() method");	
		ArrayList<VesselListInterface> submittedvessellist = vesselService.getByRegStatusAndStatus("SUBMITTED", "ACTIVE");
		return submittedvessellist;
	}

	//this method is used to get submitted vessel list
	@PreAuthorize(value = "hasAnyAuthority('approvevesselregistration','viewvessel')")
	@RequestMapping("/registeredvessel")
	@ResponseBody
	public ArrayList<VesselListInterface> registeredVesselDetails()
	{
		logger.info("inside registeredVesselDetails() method");	
		ArrayList<VesselListInterface> registeredvessellist = vesselService.getByRegStatusAndStatus("SHIP_REGISTERED", "ACTIVE");
		return registeredvessellist;
	}


	//this method is used to get submitted vessel list
	@PreAuthorize(value = "hasAnyAuthority('approvevesselregistration','viewvessel')")
	@RequestMapping("/soldvessel")
	@ResponseBody
	public ArrayList<VesselListInterface> soldVesselDetails()
	{
		logger.info("inside soldVesselDetails() method");	
		ArrayList<VesselListInterface> soldvessellist = vesselService.getByRegStatusAndStatus("SOLD", "ACTIVE");
		return soldvessellist;
	}


	//this method is used to get submitted vessel list
	@PreAuthorize(value = "hasAnyAuthority('approvevesselregistration','viewvessel')")
	@RequestMapping("/scrapvessel")
	@ResponseBody
	public ArrayList<VesselListInterface> scrapVesselDetails()
	{
		logger.info("inside scrapVesselDetails() method");	
		ArrayList<VesselListInterface> scrapvessellist = vesselService.getByRegStatusAndStatus("SCRAP", "ACTIVE");
		return scrapvessellist;
	}


	//this method is used to get all active vessel list for vessel action
	/*@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/activevessel", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselDetailInterface> activeVesselDetails(@RequestParam String regStatus)
	{
		logger.info("inside activeVessel() method");
		logger.info("regStatus : "+regStatus);	
		ArrayList<VesselDetailInterface> activevessellist = null;	
		if(regStatus.equals("SEID_DELETED"))
			activevessellist = vesselService.getByRegStatusAndStatus("SEID_DELETED", "ACTIVE");
		else
			activevessellist = vesselService.getByStatus("ACTIVE");
		return  activevessellist;
	}*/

	//this method is used to get all active vessel list 
	/*	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/getactivevessel", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselDetailInterface> getActiveVesselDetails()
	{
		logger.info("inside getActiveVessel() method");
		ArrayList<VesselDetailInterface> activevessellist = vesselService.getByStatus("ACTIVE");	
		return  activevessellist;
	}*/

	//this method is used to get all inactive vessel list
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/inactivevessel", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> getInactiveVesselDetails()
	{
		logger.info("inside getInactiveVessel() method");		
		ArrayList<VesselListInterface> activevessellist = vesselService.getByStatus("INACTIVE");
		return activevessellist;
	}

	//this method is used to get all inactive vessel list
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/allactivevessel", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> getAllActiveVesselDetails()
	{
		logger.info("inside getAllActiveVesselDetails() method");
		List<String> regStatus = new ArrayList<String>();
		regStatus.add("SOLD");
		regStatus.add("SCRAP");
		regStatus.add("INACTIVE");
		regStatus.add("SUBMITTED");
		ArrayList<VesselListInterface> activevessellist = vesselService.getAllActiveVessel("ACTIVE", regStatus);
		return activevessellist;
	}

	//this method is used to show all vessel list
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/allvessel", method=RequestMethod.GET)
	public String allVesselDetails(Model model)
	{
		logger.info("inside allVessel() method");
		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();
		logger.info("userCategory : "+userCategory);
		if(userCategory.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String companyCode = portalUser.getPortalShippingCompany().getCompanyCode();
			model.addAttribute("companyCode",companyCode);
		}
		/*	else
		{
			model.addAttribute("view", "true");
			return "/vessel/allcompany";
		}*/
		model.addAttribute("userCategory",userCategory);
		return "/vessel/allvessel";
	}

	//this method is used to show all vessel list
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/allvessel", method=RequestMethod.POST)
	public String allVesselDetailsForCompany(@RequestParam String companyCode, Model model)
	{
		logger.info("inside allVesselDetailsForCompany() method");
		String userCategory = "USER_CATEGORY_SC";
		model.addAttribute("companyCode",companyCode);
		model.addAttribute("userCategory",userCategory);
		return "/vessel/allvessel";
	}

	//this method is used to get all vessel list for logged in shipping company
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/allvesselforcompany", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<PortalVesselDetail> allVesselForCompany(@RequestParam String companyCode)
	{
		logger.info("inside allVesselForCompany() method");
		logger.info("companyCode : "+companyCode);
		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String category = userSession.getCategory();
		logger.info("category : "+category);

		ArrayList<PortalVesselDetail> companyvessellist = null; 

		if(category.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String relation = portalUser.getRelation();

			if(relation == null)
			{
				companyvessellist = vesselService.getVesselForCompanyByLoginId(loginId);
			}
			else if(relation.equals("owner"))
			{
				companyvessellist = vesselService.getVesselForCompany(companyCode);
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		else
		{
			companyvessellist = vesselService.getVesselForCompany(companyCode);
		}

		/*for (PortalVesselDetail portalVesselDetail : companyvessellist) {
			logger.info("inside for");
			logger.info("imoNo : "+portalVesselDetail.getImoNo());
		}*/
		return companyvessellist;
	}	

	//this method is used to get active vessel list for logged in shipping company
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/activevesselforcompany", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<PortalVesselDetail> activeVesselForCompany(@RequestParam String companyCode)
	{
		logger.info("inside activeVesselForCompany() method");
		logger.info("companyCode : "+companyCode);
		ArrayList<PortalVesselDetail> companyvessellist = vesselService.getVesselForCompanyByStatus(companyCode, "ACTIVE");
		for (PortalVesselDetail portalVesselDetail : companyvessellist) {
			logger.info("inside for");
			logger.info("imoNo : "+portalVesselDetail.getImoNo());
		}
		return companyvessellist;
	}

	//this method is used to get inactive vessel list for logged in shipping company
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/inactivevesselforcompany", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> inactiveVesselForCompany(@RequestParam String companyCode)
	{
		logger.info("inside inactivevesselforcompany() method");
		logger.info("companyCode : "+companyCode);

		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String category = userSession.getCategory();
		logger.info("category : "+category);

		ArrayList<VesselListInterface> companyvessellist = null;

		if(category.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String relation = portalUser.getRelation();

			if(relation == null)
			{
				companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatusAndLoginId(loginId, "INACTIVE", "INACTIVE");
			}
			else if(relation.equals("owner"))
			{
				companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "INACTIVE", "INACTIVE");
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		else
		{
			companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "INACTIVE", "INACTIVE");
		}

		/*ArrayList<PortalVesselDetail> companyvessellist = vesselService.getVesselForCompanyByStatus(companyCode, "INACTIVE");
		for (PortalVesselDetail portalVesselDetail : companyvessellist) {
			logger.info("inside for");
			logger.info("imoNo : "+portalVesselDetail.getImoNo());
		}*/
		return companyvessellist;
	}


	//this method is used to get all active vessel list for logged in shipping company
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/allactivevesselforcompany", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> getAllactiveVesselForCompany(@RequestParam String companyCode)
	{
		logger.info("inside getAllactiveVesselForCompany() method");
		logger.info("companyCode : "+companyCode);

		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String category = userSession.getCategory();
		logger.info("category : "+category);

		ArrayList<VesselListInterface> companyvessellist = null;

		List<String> regStatus = new ArrayList<String>();
		regStatus.add("SOLD");
		regStatus.add("SCRAP");
		regStatus.add("INACTIVE");
		regStatus.add("SUBMITTED");

		if(category.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String relation = portalUser.getRelation();

			if(relation == null)
			{
				companyvessellist = vesselService.getAllActiveVesselForCompanyByLoginId(loginId, "ACTIVE", regStatus);
			}
			else if(relation.equals("owner"))
			{
				companyvessellist = vesselService.getAllActiveVesselForCompany(companyCode, "ACTIVE", regStatus);
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		else
		{
			companyvessellist = vesselService.getAllActiveVesselForCompany(companyCode, "ACTIVE", regStatus);
		}

		/*ArrayList<PortalVesselDetail> companyvessellist = vesselService.getVesselForCompanyByStatus(companyCode, "INACTIVE");
			for (PortalVesselDetail portalVesselDetail : companyvessellist) {
				logger.info("inside for");
				logger.info("imoNo : "+portalVesselDetail.getImoNo());
			}*/
		return companyvessellist;
	}


	//this method is used to get submitted vessel list for logged in shipping company
	@PreAuthorize(value = "hasAnyAuthority('approvevesselregistration','viewvessel')")
	@RequestMapping(value="/submittedvesselforcompany", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> submittedVesselForCompany(@RequestParam String companyCode)
	{
		logger.info("inside submittedVesselForCompany() method");
		logger.info("companyCode : "+companyCode);

		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String category = userSession.getCategory();
		logger.info("category : "+category);

		ArrayList<VesselListInterface> companyvessellist = null;

		if(category.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String relation = portalUser.getRelation();

			if(relation == null)
			{
				companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatusAndLoginId(loginId, "ACTIVE", "SUBMITTED");
			}
			else if(relation.equals("owner"))
			{
				companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SUBMITTED");
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		else
		{
			companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SUBMITTED");
		}	
		/*ArrayList<PortalVesselDetail> companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SUBMITTED");
		for (PortalVesselDetail portalVesselDetail : companyvessellist) {
			logger.info("inside for");
			logger.info("imoNo : "+portalVesselDetail.getImoNo());
		}*/
		return companyvessellist;
	}

	//this method is used to get registered vessel list for logged in shipping company
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/registeredcompanyvessel", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> registeredVesselForCompany(@RequestParam String companyCode)
	{
		logger.info("inside registeredVesselForCompany() method");
		logger.info("companyCode : "+companyCode);

		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String category = userSession.getCategory();
		logger.info("category : "+category);

		ArrayList<VesselListInterface> activevessellist = null;

		if(category.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String relation = portalUser.getRelation();

			if(relation == null)
			{
				activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatusAndLoginId(loginId, "ACTIVE", "SHIP_REGISTERED");
			}
			else if(relation.equals("owner"))
			{
				activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SHIP_REGISTERED");
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		else
		{
			activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SHIP_REGISTERED");
		}

		/*ArrayList<PortalVesselDetail> companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SHIP_REGISTERED");
		for (PortalVesselDetail portalVesselDetail : companyvessellist) {
			logger.info("inside for");
			logger.info("imoNo : "+portalVesselDetail.getImoNo());
		}*/
		return activevessellist;
	}


	//this method is used to get registered vessel list for logged in shipping company
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/soldcompanyvessel", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> soldVesselForCompany(@RequestParam String companyCode)
	{
		logger.info("inside soldVesselForCompany() method");
		logger.info("companyCode : "+companyCode);

		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String category = userSession.getCategory();
		logger.info("category : "+category);

		ArrayList<VesselListInterface> activevessellist = null;

		if(category.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String relation = portalUser.getRelation();

			if(relation == null)
			{
				activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatusAndLoginId(loginId, "ACTIVE", "SOLD");
			}
			else if(relation.equals("owner"))
			{
				activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SOLD");
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		else
		{
			activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SOLD");
		}

		/*ArrayList<PortalVesselDetail> companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SHIP_REGISTERED");
			for (PortalVesselDetail portalVesselDetail : companyvessellist) {
				logger.info("inside for");
				logger.info("imoNo : "+portalVesselDetail.getImoNo());
			}*/
		return activevessellist;
	}


	//this method is used to get registered vessel list for logged in shipping company
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/scrapcompanyvessel", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> scrapVesselForCompany(@RequestParam String companyCode)
	{
		logger.info("inside scrapVesselForCompany() method");
		logger.info("companyCode : "+companyCode);

		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String category = userSession.getCategory();
		logger.info("category : "+category);

		ArrayList<VesselListInterface> activevessellist = null;

		if(category.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String relation = portalUser.getRelation();

			if(relation == null)
			{
				activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatusAndLoginId(loginId, "ACTIVE", "SCRAP");
			}
			else if(relation.equals("owner"))
			{
				activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SCRAP");
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		else
		{
			activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SCRAP");
		}

		/*ArrayList<PortalVesselDetail> companyvessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SHIP_REGISTERED");
			for (PortalVesselDetail portalVesselDetail : companyvessellist) {
				logger.info("inside for");
				logger.info("imoNo : "+portalVesselDetail.getImoNo());
			}*/
		return activevessellist;
	}


	//this method is used get all vessel list from database
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/getallvessel", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> getAllVesselDetails()
	{
		logger.info("inside getAllVessel() method");
		ArrayList<VesselListInterface> allvessellist = vesselService.getAllVesselList();
		return allvessellist;
	}

	//this method is used get all vessel list from database for company
	/*	@RequestMapping(value="/activevesselcompany", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<PortalVesselDetail> activeVesselDetailsForCompany(@RequestParam String regStatus, @RequestParam String companyCode)
	{
		logger.info("inside activeVesselDetailsForCompany() method");
		logger.info("regStatus : "+regStatus);	

		ArrayList<PortalVesselDetail> activevessellist = null;	
		if(regStatus.equals("SEID_DELETED"))
			activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SEID_DELETED");
		else
			activevessellist = vesselService.getVesselForCompanyByStatus(companyCode, "ACTIVE");
		return  activevessellist;
	}*/

	//this method is used to get all active vessel list for vessel action for a shipping company
	@PreAuthorize(value = "hasAnyAuthority('vesselaction','viewvessel', 'addshipborneequipment')")
	@RequestMapping(value="/activevesselcompany", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> activeVesselForCompany(@RequestParam String regStatus, @RequestParam String companyCode)
	{
		logger.info("inside activeVesselForCompany() method");
		logger.info("regStatus : "+regStatus);	

		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		PortalUser portalUser = userService.getbyloginId(loginId).get();
		String relation = portalUser.getRelation();

		ArrayList<VesselListInterface> activevessellist = null;	

		if(regStatus.equals("SEID_DELETED"))
		{
			if(relation == null)
			{
				activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatusAndLoginId(loginId, "ACTIVE", "SEID_DELETED");
			}
			else if(relation.equals("owner"))
			{
				activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SEID_DELETED");
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		else
		{
			if(relation == null)
			{
				activevessellist = vesselService.getVesselForCompanyByRegStatusAndLoginId(loginId);
			}
			else if(relation.equals("owner"))
			{
				activevessellist = vesselService.getVesselForCompanyByRegStatus(companyCode);
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}

		/*if(regStatus.equals("SEID_DELETED"))
			activevessellist = vesselService.getVesselForCompanyByStatusAndRegStatus(companyCode, "ACTIVE", "SEID_DELETED");
		else
			activevessellist = vesselService.getVesselForCompanyByRegStatus(companyCode);*/
		return  activevessellist;
	}


	//this method is used to get all active vessel list for vessel action
	@PreAuthorize(value = "hasAnyAuthority('vesselaction','viewvessel', 'addshipborneequipment')")
	@RequestMapping(value="/activevessel", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> activeVesselDetails(@RequestParam String regStatus)
	{
		logger.info("inside activeVesselDetails() method");
		logger.info("regStatus : "+regStatus);	
		ArrayList<VesselListInterface> activevessellist = null;	
		if(regStatus.equals("SEID_DELETED"))
			activevessellist = vesselService.getByRegStatusAndStatus("SEID_DELETED", "ACTIVE");
		else
			activevessellist = vesselService.getByRegStatus();
		return  activevessellist;
	}

	//this method is used show sold vessel eligible to repurchase for a company from database
	@PreAuthorize(value = "hasAuthority('vesselaction')")
	@RequestMapping(value="/repurchasevessellist", method = RequestMethod.GET)
	public String repurchaseVesselDetails(Model model)
	{
		logger.info("inside repurchaseVesselDetails() method");
		
		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();

		if(userCategory.equals("USER_CATEGORY_SC"))    //to show register vessel form to shipping company only with default value
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String ownerCompanyCode = portalUser.getPortalShippingCompany().getCompanyCode();
			logger.info("ownerCompanyCode : " + ownerCompanyCode);
			model.addAttribute("ownerCompanyCode", ownerCompanyCode);
		}
		/*model.addAttribute("userCategory", userCategory);
		logger.info("request : "+request);
		model.addAttribute("request", request);
		return "/vessel/showallforaction";*/
		
		return "/vessel/repurchasevessel";
	}


	//this method is used get sold vessel eligible to repurchase for a company from database
	@PreAuthorize(value = "hasAuthority('vesselaction')")
	@RequestMapping(value="/repurchasevessel", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<VesselListInterface> getRepurchaseVesselDetails(@RequestParam String companyCode)
	{
		logger.info("inside getRepurchaseVesselDetails() method");
		
		UserSessionInterface userSession = controller.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String category = userSession.getCategory();
		logger.info("category : "+category);

		ArrayList<VesselListInterface> activevessellist = null;

		if(category.equals("USER_CATEGORY_SC"))
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String relation = portalUser.getRelation();

			if(relation == null)
			{
				String companyCodeforUser = portalUser.getPortalShippingCompany().getCompanyCode();
				activevessellist = vesselService.getRepurchaseVesselList(companyCodeforUser);
			}
			else if(relation.equals("owner"))
			{
				activevessellist = vesselService.getRepurchaseVesselList(companyCode);
			}
			else
			{
				logger.info("Valid relation for user with shipping company is not present");
			}
		}
		return activevessellist;
	}

}
