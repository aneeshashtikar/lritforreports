
package in.gov.lrit.portal.controller.request;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean; 
import org.springframework.context.annotation.Configuration; 
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import in.gov.lrit.portal.service.request.SoapConnector;

@Configuration 
public class WSConfig {

	@Value("${lrit.dc_url}")
	private String dcUrl;
	@Bean 
	public Jaxb2Marshaller marshaller() { 
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller(); 
		// this is the package name specified in the	<generatePackage> specified in // pom.xml

		marshaller.setContextPaths("org.imo.gisis.xml.lrit._2008","org.imo.gisis.xml.lrit.positionrequest._2008","org.imo.gisis.xml.lrit.ddprequest._2008"
				,"org.imo.gisis.xml.lrit.coastalstatestandingorderupdate._2014","org.imo.gisis.xml.lrit.geographicalareaupdate._2014","org.imo.gisis.xml.lrit.surpicrequest._2014","responses.portalresponse","org.imo.gisis.xml.lrit.dnidrequest._2008");

		return	marshaller; 
	}

	@Bean
	public SoapConnector soapConnector(Jaxb2Marshaller marshaller) {
		SoapConnector client = new SoapConnector();
		client.setDefaultUri(dcUrl);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller); 
		return client; 
	}

}
