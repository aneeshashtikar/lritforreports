package in.gov.lrit.portal.controller.logging;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.portal.service.logging.PortalLoggingService;

@Controller
@RequestMapping("/logging")
public class PortalLoggingController {

	@Autowired
    private	PortalLoggingService portalLoggingService;
	
	@RequestMapping(value="/setgeolocation" , method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public void SetGeoLocation(String latitude,String longitude)
	{
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	    HttpSession session = attr.getRequest().getSession();
	    Integer id=(Integer) session.getAttribute("loggingid");
		portalLoggingService.UpdateGeoLocation(id, latitude, longitude);
	//	return flag;
	}
}
