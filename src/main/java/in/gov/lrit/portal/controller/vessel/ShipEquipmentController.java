package in.gov.lrit.portal.controller.vessel;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.hibernate.exception.ConstraintViolationException;
import org.imo.gisis.xml.lrit._2008.Response;
import org.imo.gisis.xml.lrit.dnidrequest._2008.DNIDRequestType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import in.gov.lrit.portal.CommonUtil.ICommonUtil;
import in.gov.lrit.portal.model.vessel.ApprovedVesselRegistartionDTO;
import in.gov.lrit.portal.model.vessel.DocumentForm;
import in.gov.lrit.portal.model.vessel.GroupList;
import in.gov.lrit.portal.model.vessel.ManufacturerList;
import in.gov.lrit.portal.model.vessel.MappingNames;
import in.gov.lrit.portal.model.vessel.PortalCSODPADetails;
import in.gov.lrit.portal.model.vessel.PortalDocuments;
import in.gov.lrit.portal.model.vessel.PortalVesselCategory;
import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in.gov.lrit.portal.model.vessel.PortalManufacturerDetails;
import in.gov.lrit.portal.model.vessel.PortalModelDetail;
import in.gov.lrit.portal.model.vessel.PortalShipEquipment;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in.gov.lrit.portal.model.vessel.PortalShippingCompanyVesselRelation;
import in.gov.lrit.portal.model.user.PortalUser;
import in.gov.lrit.portal.model.vessel.PortalVesselDetail;
import in.gov.lrit.portal.model.vessel.PortalVesselDetailHistory;
import in.gov.lrit.portal.model.vessel.PortalVesselGroup;
import in.gov.lrit.portal.model.vessel.PortalVesselReportingStatusHistory;
import in.gov.lrit.portal.model.vessel.PortalVesselType;
import in.gov.lrit.portal.model.vessel.VesselLoginMappingId;
import in.gov.lrit.portal.model.vessel.VesselRegistrationDTO;
import in.gov.lrit.portal.model.vessel.ViewVesselMigratedDTO;
import in.gov.lrit.portal.service.pendingtask.PendingTaskService;
import in.gov.lrit.portal.service.vessel.CSODPADetailsService;
import in.gov.lrit.portal.service.vessel.DocumentsService;
import in.gov.lrit.portal.service.vessel.ManufacturerDetailsService;
import in.gov.lrit.portal.service.vessel.ModelService;
import in.gov.lrit.portal.service.vessel.PortalShipEquipmentService;
import in.gov.lrit.portal.service.vessel.ShippingCompanyService;
import in.gov.lrit.portal.service.vessel.UserService;
import in.gov.lrit.portal.service.vessel.VesselDetailHistoryService;
import in.gov.lrit.portal.service.vessel.VesselGroupService;
import in.gov.lrit.portal.service.vessel.VesselReportingStatusHistoryService;
import in.gov.lrit.portal.service.vessel.VesselService;
import in.gov.lrit.portal.service.vessel.VesselTypeService;
import in.gov.lrit.portal.service.vessel.VesselUserRelationService;
import in.gov.lrit.portal.webservice.config.ASPSoapConnector;
import in_.gov.lrit.session.UserSessionInterface;

@Controller
//@Transactional(rollbackFor=ConstraintViolationException.class)
@RequestMapping("/ship")
public class ShipEquipmentController {

	@Autowired
	private PortalShipEquipmentService shipEquipService;

	@Autowired
	private VesselService vesselService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private VesselGroupService vesselGroupService;

	@Autowired
	private VesselTypeService typeService;

	@Autowired
	private ShippingCompanyService companyService;

	@Autowired
	private CSODPADetailsService csodpaService;

	@Autowired
	private ManufacturerDetailsService manuService;

	@Autowired
	private UserService userService;

	@Autowired
	private VesselUserRelationService pscvrService;

	@Autowired
	private ICommonUtil utilService;

	@Autowired
	private DocumentsService docsService;

	@Autowired
	private ASPSoapConnector aspsoapConnector;

	@Value("${lrit.asp_url}")
	private String aspUrl;

	@Value("${lrit.cgid}")
	private String cgid;

	@Autowired
	private PendingTaskService pendingtaskService;

	@Autowired
	private VesselDetailHistoryService vesselHstService;

	@Autowired
	private VesselReportingStatusHistoryService reportingHstService;

	private static final Logger logger = LoggerFactory.getLogger(ShipEquipmentController.class);

	//show all company list to admin
	@RequestMapping("/showadminpage")
	public String adminCompanyShipRegistration()
	{
		return "/vessel/allcompany";
	}

	//this method is used to register vessel
	@RequestMapping(value="/registervessel", method=RequestMethod.GET)
	public ModelAndView registerVessel(HttpServletRequest request, Model model)
	{
		logger.info("inside registerVessel() GET method");
		UserSessionInterface userSession = this.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();

		if(userCategory.equals("USER_CATEGORY_SC"))    //to show register vessel form to shipping company only with default value
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String userName = portalUser.getName();
			logger.info("userName : "+userName);
			request.setAttribute("userId", loginId);
			request.setAttribute("userName", userName);
			String ownerCompanyCode = portalUser.getPortalShippingCompany().getCompanyCode();
			String ownerCompanyName = portalUser.getPortalShippingCompany().getCompanyName();
			request.setAttribute("ownerCompanyCode", ownerCompanyCode);
			request.setAttribute("ownerCompanyName", ownerCompanyName);
			logger.info("ownerCompanyCode : "+ownerCompanyCode);
			logger.info("ownerCompanyName : "+ownerCompanyName);
		}
		else
		{
			ModelAndView modelAndView = new ModelAndView("/vessel/error", "error", "LoggedIn User Category is not allowed to register ship");
			return modelAndView;
		}

		model.addAttribute("repurchase", "false");
		model.addAttribute("userCategory", userCategory);
		VesselRegistrationDTO vrdto =  new VesselRegistrationDTO();	
		this.prepareCreateShipDTO(vrdto, model);
		ModelAndView modelAndView = new ModelAndView("/vessel/vesselregistration", "vrdto", vrdto);
		return modelAndView;
	}

	//this method is used to persist vessel and shipborne equipment details
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@RequestMapping(value="/persistvessel", method=RequestMethod.POST)
	public String persistVessel(@ModelAttribute("vrdto") VesselRegistrationDTO vrdto, 
			@RequestParam("shipEq.vesselDet.vesselImage1") MultipartFile imageUploaded, 
			@RequestParam("docs.emailDoc1") MultipartFile emailDocUploaded, 
			@RequestParam("docs.tataLritDoc1") MultipartFile tataLritDocUploaded, 
			@RequestParam("docs.flagRegDoc1") MultipartFile flagRegDocUploaded, 
			@RequestParam("docs.conformanceTestCertDoc1") MultipartFile conformanceTestCertDocUploaded, 
			@RequestParam("docs.additionalDoc1") MultipartFile additionalDocUploaded, Model model, @RequestParam("repurchaseflag") String repurchaseflag, HttpServletRequest request) 
	{
		logger.info("inside persistvessel() method");

		boolean flag = false;
		byte[] vesselImage = null;
		byte[] emailDoc = null;
		byte[] tataLritDoc = null;
		byte[] flagRegDoc = null;
		byte[] conformanceTestCertDoc = null;
		byte[] additionalDoc = null;	

		//get login_id and requestor_lrit_id from session
		/*try
		{*/

		UserSessionInterface userSession = this.getCurrentSession();
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String requestor_lritId = userSession.getRequestorsLritId().getLritId();
		logger.info("requestor_lritId : "+requestor_lritId);

		logger.info("companyCode : "+vrdto.getPscvrList().get(0).getCustomId().getUserDet().getPortalShippingCompany().getCompanyCode());

		String imoNo = vrdto.getShipEq().getVesselDet().getImoNo();
		String mmsiNo = vrdto.getShipEq().getVesselDet().getMmsiNo();
		String shipEquipmentId = vrdto.getShipEq().getShipborneEquipmentId();

		logger.info("repurchaseflag : "+repurchaseflag);

		//check if imoNo, mmsiNo, shipborneequipmentId exist except sold case
		flag = vesselService.isIMONoExist(imoNo);	
		if (flag == true)
		{
			model.addAttribute("error", "IMO Number already exist");
			model.addAttribute("errorMsg", "IMO Number already exist");
			logger.error("IMO Number already exist");
		}
		else
		{
			flag = vesselService.isMMSINoExist(mmsiNo);	
			if (flag == true)
			{
				model.addAttribute("error", "MMSI Number already exist");
				model.addAttribute("errorMsg", "MMSI Number already exist");	
				logger.error("MMSI Number already exist");
			}
			else
			{
				flag = shipEquipService.isShipEquipIdExist(shipEquipmentId);
				if (flag == true)
				{
					model.addAttribute("error", "Shipborne Equipment Id already exist");
					model.addAttribute("errorMsg", "Shipborne Equipment Id already exist");
					logger.error("Shipborne Equipment Id already exist");
				}
				else
				{
					logger.info("inside saving vessel and shipborne equipment details");
					//get manufacture id by manufacure Id
					Integer manufacturerId = vrdto.getShipEq().getModelDet().getManuDet().getManufacturerId();
					logger.info("manufacturerId : " + manufacturerId);

					//set manufacturer id in vrdto
					vrdto.getShipEq().getModelDet().getManuDet().setManufacturerId(manufacturerId);

					//get model id by modelType, manufacturerName, modelName
					Integer modelId = vrdto.getShipEq().getModelDet().getModelId();
					logger.info("modelId : " + modelId);

					//set model id in vrdto
					vrdto.getShipEq().getModelDet().setModelId(modelId);
					
					//get group id
					Long groupId = vrdto.getShipEq().getVesselDet().getPortalVesselGroup().getGroupId();
					logger.info("groupId : " + groupId);
					
					//set group id in vrdto for not null
					if(groupId != null)
					{
						vrdto.getShipEq().getVesselDet().getPortalVesselGroup().setGroupId(groupId);					
					}

					//set group id in vrdto for null
					vrdto.getShipEq().getVesselDet().setPortalVesselGroup(null);
					
					//set default registration status
					vrdto.getShipEq().getVesselDet().setRegStatus("SUBMITTED");
					vrdto.getShipEq().getVesselDet().setStatus("ACTIVE");

					//set date and time
					Timestamp regDate = utilService.getCurrentTimeStamp();
					vrdto.getShipEq().getVesselDet().setRegDate(regDate);

					Timestamp regStatusDate = utilService.getCurrentTimeStamp();
					vrdto.getShipEq().getVesselDet().setRegStatusDate(regStatusDate);

					//set vessel image
					if(!imageUploaded.isEmpty())
					{
						try {
							vesselImage = imageUploaded.getBytes();
							vrdto.getShipEq().getVesselDet().setVesselImage(vesselImage);
						} catch (Exception e) {
							model.addAttribute("error", "Image Uploaded size invalid");
							model.addAttribute("errorMsg", "Image Uploaded size invalid");
							logger.error("Image Uploaded size invalid");
							e.printStackTrace();
						}
					}

					//get imo no
					logger.info("imoNo : " +imoNo);

					//set imo in shipEq
					vrdto.getShipEq().setImoNo(imoNo);

					//persisting vessel details exist inside shipborne equipment details
					PortalShipEquipment shipborneEquipment = vrdto.getShipEq();			
					PortalVesselDetail vesselDet = vrdto.getShipEq().getVesselDet();			
					shipborneEquipment.setVesselDet(vesselDet);
					PortalShipEquipment portalShipborneEquipment = shipEquipService.addShipEquip(shipborneEquipment);
					logger.info("ship equipment entity persisted successfully");

					//persisting vessel documents
					Integer vesselId = portalShipborneEquipment.getVesselDet().getVesselId();
					/*Integer vesselId = vesselService.getVesselIdByImoNo(imoNo);*/
					logger.info("vesselId : " + vesselId);
					/*Optional<PortalVesselDetail> portalVessel = vesselService.getByVesselId(vesselId);*/
					PortalVesselDetail portalVessel = portalShipborneEquipment.getVesselDet();

					List<PortalDocuments> docslist = new ArrayList<PortalDocuments>();

					try 
					{
						String tableName = "Portal_Vessel_Detail";

						logger.info("inside persisting document");

						if(!emailDocUploaded.isEmpty())
						{
							PortalDocuments email = new PortalDocuments();
							docslist.add(email);

							emailDoc = emailDocUploaded.getBytes();
							vrdto.getDocs().setEmailDoc(emailDoc);
							email.setReferenceId(vesselId.toString());
							email.setTableName(tableName);
							email.setDocument(emailDoc);
							email.setDocPurpose("Email From Owner");
						}

						/*this.setDocument(email, referenceId, tableName, document, purpose);*/
						if(!tataLritDocUploaded.isEmpty())
						{
							PortalDocuments tataLrit = new PortalDocuments();
							docslist.add(tataLrit);

							tataLritDoc = tataLritDocUploaded.getBytes();
							vrdto.getDocs().setTataLritDoc(tataLritDoc);
							tataLrit.setReferenceId(vesselId.toString());
							tataLrit.setTableName(tableName);
							tataLrit.setDocument(tataLritDoc);
							tataLrit.setDocPurpose("Tata Lrit Form");
						}

						if(!flagRegDocUploaded.isEmpty())
						{
							PortalDocuments flagReg = new PortalDocuments();
							docslist.add(flagReg);

							flagRegDoc = flagRegDocUploaded.getBytes();
							vrdto.getDocs().setFlagRegDoc(flagRegDoc);
							flagReg.setReferenceId(vesselId.toString());
							flagReg.setTableName(tableName);
							flagReg.setDocument(flagRegDoc);
							flagReg.setDocPurpose("Flag Registration Certificate");
						}

						if(!conformanceTestCertDocUploaded.isEmpty())
						{
							PortalDocuments conformanceTestCert = new PortalDocuments();
							docslist.add(conformanceTestCert);

							conformanceTestCertDoc = conformanceTestCertDocUploaded.getBytes();
							vrdto.getDocs().setConformanceTestCertDoc(conformanceTestCertDoc);
							conformanceTestCert.setReferenceId(vesselId.toString());
							conformanceTestCert.setTableName(tableName);
							conformanceTestCert.setDocument(conformanceTestCertDoc);
							conformanceTestCert.setDocPurpose("Conforence Test Certificate");
						}

						if(!additionalDocUploaded.isEmpty())
						{
							logger.info("inside persisting additional docs");
							PortalDocuments additional = new PortalDocuments();
							docslist.add(additional);

							additionalDoc = additionalDocUploaded.getBytes();
							vrdto.getDocs().setAdditionalDoc(additionalDoc);
							additional.setReferenceId(vesselId.toString());
							additional.setTableName(tableName);
							additional.setDocument(additionalDoc);
							additional.setDocPurpose("Additional Document");
						}

						Iterable<PortalDocuments> savedDocs = docsService.addAllDocuments(docslist);
						logger.info("Documents persisted successfully");

					}
					catch (Exception e) 
					{
						model.addAttribute("error", "Uploaded Document size invalid");
						model.addAttribute("errorMsg", "Uploaded Document size invalid");
						logger.error("Uploaded Document size invalid");
						e.printStackTrace();
					}

					//set list of companies
					List<PortalShippingCompanyVesselRelation> createlist = new ArrayList<PortalShippingCompanyVesselRelation>();
					PortalShippingCompanyVesselRelation owner = new PortalShippingCompanyVesselRelation();
					PortalShippingCompanyVesselRelation technicalManager = new PortalShippingCompanyVesselRelation();
					PortalShippingCompanyVesselRelation commercialManager = new PortalShippingCompanyVesselRelation();

					//segregate owner, technicalManager, commercialManager from list received from Form	
					owner= vrdto.getPscvrList().get(0);
					technicalManager = vrdto.getPscvrList().get(1);
					commercialManager = vrdto.getPscvrList().get(2);

					//set vesselid and userid for owner

					//get owner login id from session	
					logger.info("ownerloginId "+vrdto.getPscvrList().get(0).getCustomId().getUserDet().getLoginId());
					String ownerloginId = vrdto.getPscvrList().get(0).getCustomId().getUserDet().getLoginId();
					Optional<PortalUser> portalUserO = userService.getbyloginId(ownerloginId);

					owner.setCustomId(new VesselLoginMappingId(portalUserO.get(), portalVessel));

					//set vesselid and userid for technical	
					logger.info("technicalManagerloginId "+vrdto.getPscvrList().get(1).getCustomId().getUserDet().getLoginId());
					String technicalManagerloginId = vrdto.getPscvrList().get(1).getCustomId().getUserDet().getLoginId();
					Optional<PortalUser> portalUserT = userService.getbyloginId(technicalManagerloginId);

					technicalManager.setCustomId(new VesselLoginMappingId(portalUserT.get(), portalVessel));

					//set vesselid and userid for commercial
					logger.info("commercialManagerloginId "+vrdto.getPscvrList().get(2).getCustomId().getUserDet().getLoginId());
					String commercialManagerloginId = vrdto.getPscvrList().get(2).getCustomId().getUserDet().getLoginId();
					Optional<PortalUser> portalUserC = userService.getbyloginId(commercialManagerloginId);

					commercialManager.setCustomId(new VesselLoginMappingId(portalUserC.get(), portalVessel));

					//get company code and set shipping company

					//get owner company code from session
					String ownerCompanyCode = vrdto.getPscvrList().get(0).getCustomId().getUserDet().getPortalShippingCompany().getCompanyCode();
					Optional<PortalShippingCompany> shipCompO = companyService.getShipingById(ownerCompanyCode);

					owner.getCustomId().getUserDet().setPortalShippingCompany(shipCompO.get());

					String technicalManagerCompanyCode = vrdto.getPscvrList().get(1).getCustomId().getUserDet().getPortalShippingCompany().getCompanyCode();
					Optional<PortalShippingCompany> shipCompT = companyService.getShipingById(technicalManagerCompanyCode);

					technicalManager.getCustomId().getUserDet().setPortalShippingCompany(shipCompT.get());

					String commercialManagerCompanyCode = vrdto.getPscvrList().get(2).getCustomId().getUserDet().getPortalShippingCompany().getCompanyCode();
					Optional<PortalShippingCompany> shipCompC = companyService.getShipingById(commercialManagerCompanyCode);

					commercialManager.getCustomId().getUserDet().setPortalShippingCompany(shipCompC.get());

					//get and set csoid 	
					String ownerCsoId = vrdto.getPscvrList().get(0).getCcsoId();
					String technicalManagerCsoId = vrdto.getPscvrList().get(1).getCcsoId();
					String commercialManagerCsoId = vrdto.getPscvrList().get(2).getCcsoId();

					owner.setCcsoId(ownerCsoId);
					technicalManager.setCcsoId(technicalManagerCsoId);
					commercialManager.setCcsoId(commercialManagerCsoId);

					//get and set acsoId
					String ownerAcsoId = vrdto.getPscvrList().get(0).getAcsoId();
					String technicalManagerAcsoId = vrdto.getPscvrList().get(1).getAcsoId();
					String commercialManagerAcsoId = vrdto.getPscvrList().get(2).getAcsoId();

					owner.setAcsoId(ownerAcsoId);
					technicalManager.setAcsoId(technicalManagerAcsoId);
					commercialManager.setAcsoId(commercialManagerAcsoId);

					//get and set dpaId
					String ownerDpaId = vrdto.getPscvrList().get(0).getDpaId();
					String technicalManagerDpaId = vrdto.getPscvrList().get(1).getDpaId();
					String commercialManagerDpaId = vrdto.getPscvrList().get(2).getDpaId();

					owner.setDpaId(ownerDpaId);
					technicalManager.setDpaId(technicalManagerDpaId);
					commercialManager.setDpaId(commercialManagerDpaId);

					//set relationid
					if((ownerCompanyCode.equals(technicalManagerCompanyCode)) && (ownerCsoId.equals(technicalManagerCsoId)) && (ownerDpaId.equals(technicalManagerDpaId)))
					{
						if((ownerCompanyCode.equals(commercialManagerCompanyCode)) && (ownerCsoId.equals(commercialManagerCsoId)) && (ownerDpaId.equals(commercialManagerDpaId)))
						{

							logger.info("owner, technicalManager and commercialManager companies are Same");
							owner.setRel(6);
							PortalShippingCompanyVesselRelation savedEntity = pscvrService.addMapping(owner);
							logger.info("pscvr entry persisted");
						}
						else 
						{
							logger.info("owner and technicalManager Companies are Same");
							owner.setRel(4);
							commercialManager.setRel(1);
							createlist.add(commercialManager);
							createlist.add(owner);
							logger.info(owner.toString());
							logger.info(commercialManager.toString());
							Iterable<PortalShippingCompanyVesselRelation> savedEntity = pscvrService.addAllMapping(createlist);
							logger.info("pscvr entry persisted");
						}
					}
					else if((ownerCompanyCode.equals(commercialManagerCompanyCode)) && (ownerCsoId.equals(commercialManagerCsoId)) && (ownerDpaId.equals(commercialManagerDpaId)))
					{
						logger.info("owner and commercial Companies are Same");
						owner.setRel(3);
						technicalManager.setRel(2);
						createlist.add(owner);
						createlist.add(technicalManager);
						Iterable<PortalShippingCompanyVesselRelation> savedEntity = pscvrService.addAllMapping(createlist);
						logger.info("pscvr entry persisted");
					}
					else if((technicalManagerCompanyCode.equals(commercialManagerCompanyCode)) && (technicalManagerCsoId.equals(commercialManagerCsoId)) && (technicalManagerDpaId.equals(commercialManagerDpaId)))
					{
						logger.info("technical and commercial Companies are Same");
						technicalManager.setRel(5);
						owner.setRel(0);
						createlist.add(technicalManager);
						createlist.add(owner);
						Iterable<PortalShippingCompanyVesselRelation> savedEntity = pscvrService.addAllMapping(createlist);
						logger.info("pscvr entry persisted");
					}
					else
					{
						logger.info("owner, technicalManager and CommercialManager Companies all are different");
						owner.setRel(0);
						technicalManager.setRel(2);
						commercialManager.setRel(1);
						createlist.add(owner);
						createlist.add(technicalManager);
						createlist.add(commercialManager);
						Iterable<PortalShippingCompanyVesselRelation> savedEntity = pscvrService.addAllMapping(createlist);
					}

					logger.info("---------------------------------------");	
					model.addAttribute("successMsg", "Vessel Details has been registered successfully");
					logger.info("Vessel Details Persisted successfully");

					//put entry in pending task for approving vessel registration detail by admin 

					/*PortalPendingTaskMst pendingTask = new PortalPendingTaskMst();
					pendingTask.setRequestDate(utilService.getCurrentTimeStamp());
					pendingTask.setStatus("pending");
					pendingTask.setCategory("Approve_Vessel_Registration");

					//login id and reuestor lritid of user from session who is registering vessel
					pendingTask.setRequesterLoginId(loginId); 
					pendingTask.setRequestorLritid(requestor_lritId); 

					pendingTask.setReferenceId(vesselId.toString());
					pendingTask.setMessage("User has requested to approve vessel registration");
					PortalPendingTaskMst newPendingTaskEntry = pendingtaskService.addEntryInPendingMst(pendingTask);
					logger.info(newPendingTaskEntry.toString());
					logger.info("New Entry Persisted in pending task master table");*/

					return "acknowledgement";
				}
			}
		}
		/*}
		catch(Exception e)
		{
			logger.info("Url redirected to persistvessel without filling mandatory details");
			model.addAttribute("errorMsg", "Form Submitted before filling mandatory details due to some error!! Try Again");
			e.printStackTrace();
		}*/
		if(repurchaseflag.equals("true"))
		{
			return "acknowledgement";
		}
		this.setRegistrationDefaults(request, model);
		VesselRegistrationDTO vrdto1 =  new VesselRegistrationDTO();		
		this.prepareCreateShipDTO(vrdto1, model);
		model.addAttribute("vrdto", vrdto1);	
		return "/vessel/vesselregistration";
	}

	//this method is used to show approve vessel details page to admin
	@PreAuthorize(value = "hasAuthority('approvevesselregistration')")
	@RequestMapping(value="/viewapprove", method=RequestMethod.POST)
	public String viewApprove(@RequestParam Integer vesselId, Model model)
	{
		logger.info("inside viewApprove() method");
		logger.info("vesselId: "+vesselId);
		PortalVesselDetail vessel = vesselService.getByVesselId(vesselId).get();

		ApprovedVesselRegistartionDTO avrdto = new ApprovedVesselRegistartionDTO();	
		List<MappingNames> namelist = new ArrayList<MappingNames>();
		List<PortalDocuments> docs = new ArrayList<PortalDocuments>();
		this.setVesselCompanyMapping(namelist, vesselId);		
		this.setDocuments(docs, vesselId);

		/*PortalShipEquipment approvedportalvessel = shipEquipService.getByvesselDet_vesselIdAndSeidStatus(vessel.getVesselId());*/
		PortalShipEquipment approvedportalvessel = shipEquipService.getByvesselDet_vesselId(vessel.getVesselId());

		avrdto.setNameslist(namelist);
		avrdto.setDocs(docs);
		avrdto.setShipEq(approvedportalvessel);

		model.addAttribute("docs", docs);

		ArrayList<GroupList> groupNames = vesselGroupService.getAllGroup();
		model.addAttribute("groupNames", groupNames);

		List<String> vesselTypeList = typeService.getVesselTypeList();
		model.addAttribute("vesselTypeList", vesselTypeList);

		model.addAttribute("imoVesselType", avrdto.getShipEq().getVesselDet().getImoVesselType());
		model.addAttribute("vesselCategory", avrdto.getShipEq().getVesselDet().getVesselCategory());

		model.addAttribute("avrdto", avrdto);
		return "/vessel/approvevessel";
	}

	//this method is used to approve vessel registration
	@PreAuthorize(value = "hasAuthority('approvevesselregistration')")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@RequestMapping(value="/approvevessel", method=RequestMethod.POST)
	public String approveVessel(@ModelAttribute ApprovedVesselRegistartionDTO avrdto, Model model) throws Exception, JAXBException
	{
		logger.info("inside approveVessel() method");
		Response res = null;
		boolean result = false;
		/*String dnidStatus = null;*/
		String shipequiId = avrdto.getShipEq().getShipborneEquipmentId();
		String oceanRegion = avrdto.getShipEq().getOceanRegion();
		Integer memberNo = avrdto.getShipEq().getMemberNo();
		String imoNo = avrdto.getShipEq().getVesselDet().getImoNo();
		Integer dnidNo = avrdto.getShipEq().getDnidNo();
		Integer modelId = avrdto.getShipEq().getModelDet().getModelId();
		Integer manufacturerId  = avrdto.getShipEq().getModelDet().getManuDet().getManufacturerId();
		Long groupId = avrdto.getShipEq().getVesselDet().getPortalVesselGroup().getGroupId();
		Integer vesselId = avrdto.getShipEq().getVesselDet().getVesselId();
		String regStatus = avrdto.getShipEq().getVesselDet().getRegStatus();
		String imoVesselType = avrdto.getShipEq().getVesselDet().getImoVesselType();
		String vesselType = avrdto.getShipEq().getVesselDet().getVesselType();
		String vesselCategory = avrdto.getShipEq().getVesselDet().getVesselCategory();
		BigInteger timeDelay = avrdto.getShipEq().getVesselDet().getTimeDelay();
		PortalVesselGroup groupDet = null;

		logger.info("ModelId : "+modelId);
		logger.info("ManufacturerId : "+manufacturerId);
		logger.info("GroupId : "+groupId);
		logger.info("ShipEquipId : "+shipequiId);
		logger.info("VesselId : "+vesselId);
		logger.info("ImoNo : "+imoNo);
		logger.info("oceanRegion : "+ oceanRegion);
		logger.info("memberNo : "+ memberNo);
		logger.info("regStatus : "+regStatus);
		logger.info("timeDelay : "+timeDelay);
		/*logger.info("dnidStatusDate : "+ utilService.getCurrentTimeStamp());*/

		UserSessionInterface userSession = this.getCurrentSession();   //get requestor lrit id and requestor login id for pending task from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String requestor_lritId = userSession.getRequestorsLritId().getLritId();
		logger.info("requestor_lritId : "+requestor_lritId);

		PortalManufacturerDetails manuDet = manuService.findById(manufacturerId).get();
		PortalModelDetail modelDet = modelService.getById(modelId).get();
		if(groupId != null)
		{
			groupDet = vesselGroupService.findById(groupId).get();
		}
		PortalVesselDetail vesselDet = vesselService.getByVesselId(vesselId).get();
		logger.info(vesselDet.toString());
		/*PortalShipEquipment shipequipDet = shipEquipService.getByvesselDet_vesselIdAndSeidStatus(vesselId);*/
		PortalShipEquipment shipequipDet = shipEquipService.getByvesselDet_vesselId(vesselId);

		shipequipDet.setMemberNo(memberNo);   //set member no, dnid no, seid status, reg status, reg status date
		shipequipDet.setDnidNo(dnidNo);
		vesselDet.setTimeDelay(timeDelay);
		vesselDet.setImoVesselType(imoVesselType);
		vesselDet.setVesselType(vesselType);
		vesselDet.setVesselCategory(vesselCategory);
		logger.info("befor:regStatus : "+vesselDet.getRegStatus());
		/*vesselDet.setRegStatus(regStatus);
		logger.info("after:regStatus : "+vesselDet.getRegStatus());*/

		//shipequipDet.setDnidStatusDate(utilService.getCurrentTimeStamp());
		//shipequipDet.setDnidStatusDate(utilService.getCurrentTimeStamp());
		/*shipequipDet.setSeidStatus(seidStatus);*/

		if(regStatus.equals("APPROVE"))   
		{
			logger.info("inside approve vessel registration");
			final String requestType = "51";
			final String schemaVersion = "1.0";
			final String messageType= "18";
			final String test = "0";
			DNIDRequestType dnidNew = new DNIDRequestType();  //preparing dnid request packet
			/*String messageId = utilService.generateMessageID("1065");*/
			String messageId = utilService.generateMessageID(requestor_lritId);
			dnidNew.setMessageId(messageId);
			dnidNew.setIMONum(imoNo);
			dnidNew.setDNIDNo(dnidNo.toString());
			dnidNew.setMemberNo(memberNo);
			dnidNew.setMessageType(new BigInteger(messageType));
			dnidNew.setDataUserProvider(requestor_lritId);
			dnidNew.setDataUserRequestor(requestor_lritId);
			dnidNew.setOceanRegion(new BigInteger(oceanRegion));
			dnidNew.setRequestType(new BigInteger(requestType));
			dnidNew.setSchemaVersion(new BigDecimal(schemaVersion));
			dnidNew.setTest(new BigInteger(test));
			Timestamp ts = utilService.getCurrentTimeStamp();
			dnidNew.setTimeStamp(utilService.getgregorianTimeStamp(ts));

			JAXBContext jaxbContext = JAXBContext.newInstance(DNIDRequestType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(dnidNew, sw);	
			logger.info(sw.toString());
			shipequipDet.setRequestPayload(sw.toString());   	//set request payload 
			/*vesselDet.setRegStatus("DNID_DW_REQ");*/

			avrdto.setShipEq(shipequipDet);    //update models
			avrdto.getShipEq().setVesselDet(vesselDet);
			avrdto.getShipEq().getVesselDet().setPortalVesselGroup(groupDet);
			avrdto.getShipEq().setModelDet(modelDet);
			avrdto.getShipEq().getModelDet().setManuDet(manuDet);		
			logger.info(avrdto.getShipEq().toString());	
			PortalShipEquipment approvedshipeu = shipEquipService.addShipEquip(avrdto.getShipEq());
			logger.info("Vessel details are updated successfuly");

			try    //call to ASP
			{
				regStatus = "SUBMITTED";   //set registration status before calling asp

				res = aspsoapConnector.callASPWebService(aspUrl, dnidNew);
				logger.info("response after calling ASP : "+res.getResponse().toString());
				if(res.getResponse().toString().equals("SUCCESS"))
				{
					regStatus = "DNID_DW_REQ";
					/*dnidStatus = "CallToASPSucess";*/
					model.addAttribute("success", "DNID Download Request Sent To ASP Successfully for Message Id : "+ messageId);
					logger.info("DNID Download Request Sent To ASP Successfully");

				}
				else
				{
					regStatus = "DNID_DW_FAILED";
					/*dnidStatus = "CallToASPSucess";*/
					model.addAttribute("error", "Validation Error while sending DNID Download Request To ASP: " + res.getResponse().toString() + " for Message Id : "+ messageId);
					logger.info("Validation Error while sending DNID Download Request To ASP "+ res.getResponse().toString() + " for Message Id : "+ messageId);
				}

				result = shipEquipService.updateResponsePayload(res.getResponse().toString(), shipequiId);
				if(result == true)
					logger.info("response payload updated successfully");
				else 
					logger.info("response payload updation failed");

				/*result = vesselService.updateVesselRegStatus(regStatus, vesselId);
				logger.info("result : " + result);
				if(result == true)
					logger.info("dnid status updated successfully");
				else 
					logger.info("dnid status updation failed");*/

			}
			catch(Exception e)
			{
				/*logger.info("ASP webservice is Down");*/
				logger.info("Could not sent DNID Download Request To ASP for Message Id : "+messageId +" as ASP webservice is Down");
				model.addAttribute("error", "Could not sent DNID Download Request To ASP for Message Id : "+messageId +" as ASP webservice is Down");
				e.printStackTrace();		
			}
			/*result = shipEquipService.updatednidStatusAnddnidStatusDate(dnidStatus, shipequiId);*/
			result = vesselService.updateVesselRegStatus(regStatus, vesselId);
			logger.info("result : " + result);
			if(result == true)
				logger.info("dnid status updated successfully");
			else 
				logger.info("dnid status updation failed");	
		}
		else if(regStatus.equals("REJECT"))
		{
			logger.info("inside reject vessel registration");
			model.addAttribute("error", "Vessel Registration is Rejected");
			result = vesselService.updateVesselRegStatus("REJECTED", vesselDet.getVesselId());
			if(result == true)
				logger.info("Vessel details are updated successfuly");
			else 
				logger.info("Unable to update Vessel details");	
		}	
		else
		{
			logger.info("inside no option choosen for vessel registration");
			model.addAttribute("error", "Please select valid Registration Status (Approve/Reject) and Try Again !!");
		}
		return "/vessel/submittedship";
	}

	//this method is used to view vessel and shipborne equipment details on map
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/viewvessel/{vesselId}", method=RequestMethod.GET)
	public String viewVessel(@PathVariable Integer vesselId, Model model)
	{	
		logger.info("inside viewVessel() method");
		logger.info("vesselId: "+vesselId);
		PortalVesselDetail vessel = vesselService.getByVesselId(vesselId).get();
		logger.info(vessel.toString());

		Object viewObj = this.prepareViewShipDTO(vessel, model);
		if(viewObj instanceof ViewVesselMigratedDTO)
		{
			model.addAttribute("avrdto", viewObj);
			return "/vessel/viewmigratedvessel";
		}
		else
		{
			model.addAttribute("avrdto", viewObj);
			return "/vessel/viewvessel";
		}
	}

	//this method is used to view vessel and shipborne equipment details on map
	@RequestMapping(value="/viewvesselonmap/{imoNo}", method=RequestMethod.GET)
	public String viewVesselOnMap(@PathVariable String imoNo, Model model)
	{	
		logger.info("inside viewVesselOnMap() method");
		logger.info("imoNo: "+imoNo);
		PortalVesselDetail vessel = vesselService.getVesselByImoNo(imoNo);
		logger.info(vessel.toString());
		Object viewObj = this.prepareViewShipDTO(vessel, model);
		if(viewObj instanceof ViewVesselMigratedDTO)
		{
			model.addAttribute("avrdto", viewObj);
			return "/vessel/viewmigratedvessel";
		}
		else
		{
			model.addAttribute("avrdto", viewObj);
			return "/vessel/viewvessel";
		}
	}

	//Edit Ship Details Page
	//@PreAuthorize(value = "hasAuthority('editvessel')")
	@PreAuthorize(value = "hasAuthority('viewvessel')")
	@RequestMapping(value="/editvessel/{vesselId}", method=RequestMethod.GET)
	public String editVessel(@PathVariable Integer vesselId, Model model)
	{
		logger.info("inside editvessel() method");
		logger.info("vesselId: "+vesselId);
		PortalVesselDetail vessel = vesselService.getByVesselId(vesselId).get();

		/*DocumentForm approvedDocForm = null;*/
		ApprovedVesselRegistartionDTO avrdto = new ApprovedVesselRegistartionDTO();	
		List<MappingNames> namelist = new ArrayList<MappingNames>();
		List<PortalDocuments> docs = new ArrayList<PortalDocuments>();

		this.setVesselCompanyMapping(namelist, vesselId);
		this.setDocuments(docs, vesselId);

		/*PortalShipEquipment approvedportalvessel = shipEquipService.getByvesselDet_vesselIdAndSeidStatus(vessel.getVesselId());*/
		PortalShipEquipment approvedportalvessel = shipEquipService.getByvesselDet_vesselId(vessel.getVesselId());

		if(approvedportalvessel != null)
		{
			avrdto.setShipEq(approvedportalvessel);
			avrdto.setNameslist(namelist);
			avrdto.setDocs(docs);
			model.addAttribute("avrdto", avrdto);
		}
		else
		{
			ViewVesselMigratedDTO vesselDto = new ViewVesselMigratedDTO();
			vesselDto.setVesselDet(vessel);
			vesselDto.setNameslist(namelist);
			vesselDto.setDocs(docs);
			model.addAttribute("vesselDto", vesselDto);
			logger.info("size : "+docs.size());
			model.addAttribute("docs", docs);
			return "/vessel/editvesselfornoseid";
		}	
		logger.info("size : "+docs.size());
		model.addAttribute("docs", docs);

		/*ArrayList<GroupList> groupNames = vesselGroupService.getAllGroup();
		model.addAttribute("groupNames", groupNames);*/

		model.addAttribute("editmessage", "Vessel Name and Call Sign are editable fields");
		return "/vessel/editvessel";
	}


	//this method is used to update the edited vessel details
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@RequestMapping(value="/updatevessel", method=RequestMethod.POST)
	public String updateVessel(@ModelAttribute ApprovedVesselRegistartionDTO avrdto, Model model)
	{
		logger.info("inside updateVessel() method");
		Integer modelId = avrdto.getShipEq().getModelDet().getModelId();
		Integer manufacturerId  = avrdto.getShipEq().getModelDet().getManuDet().getManufacturerId();
		Long groupId = avrdto.getShipEq().getVesselDet().getPortalVesselGroup().getGroupId();
		Integer vesselId = avrdto.getShipEq().getVesselDet().getVesselId();
		String imoNo = avrdto.getShipEq().getVesselDet().getImoNo();
		String regStatus = avrdto.getShipEq().getVesselDet().getRegStatus();  //set reg status, reg status date
		String updatedVesselName = avrdto.getShipEq().getVesselDet().getVesselName();
		String updatedCallSign = avrdto.getShipEq().getVesselDet().getCallSign();
		PortalVesselGroup groupDet = null;
		
		logger.info("ModelId : "+modelId);
		logger.info("ManufacturerId : "+manufacturerId);
		logger.info("GroupId : "+groupId);
		logger.info("ShipEquipId : "+avrdto.getShipEq().getShipborneEquipmentId());
		logger.info("VesselId : "+vesselId);
		logger.info("ImoNo : "+imoNo);

		UserSessionInterface userSession = this.getCurrentSession();  //get requestor lrit id and requestor login id for pending task from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();
		logger.info("userCategory : "+userCategory);

		PortalManufacturerDetails manuDet = manuService.findById(manufacturerId).get();
		PortalModelDetail modelDet = modelService.getById(modelId).get();
		if(groupId != null)
		{
			groupDet = vesselGroupService.findById(groupId).get();
		}
		PortalVesselDetail vesselDet = vesselService.getByVesselId(vesselId).get();
		/*PortalShipEquipment shipequipDet = shipEquipService.getByvesselDet_vesselIdAndSeidStatus(vesselId);*/
		PortalShipEquipment shipequipDet = shipEquipService.getByvesselDet_vesselId(vesselId);
		Timestamp regStatusDate = vesselDet.getRegStatusDate();
		logger.info("before :regStatus :"+vesselDet.getRegStatus());
		logger.info("before regStatus :"+regStatus);
		logger.info("before : regStatusDate :"+regStatusDate);

		/*PortalPendingTaskMst newPendingTaskEntry = null;*/

		if(userCategory.equals("USER_CATEGORY_SC"))  //if login id is of shipping company
		{
			PortalPendingTaskMst pendingTask = new PortalPendingTaskMst();   //put entry in pending task for approving edited vessel detail by admin 
			pendingTask.setRequestDate(utilService.getCurrentTimeStamp());
			pendingTask.setStatus("pending");
			pendingTask.setCategory("Edit_Vessel_Details");
			pendingTask.setRequesterLoginId(loginId);   //get login id of user from session who is editing the data
			pendingTask.setRequestorLritid(this.cgid);  //get from user table after getting login id
			pendingTask.setReferenceId(vesselDet.getVesselId().toString());
			if(!(vesselDet.getVesselName().equals(updatedVesselName)))
			{
				if(!(vesselDet.getCallSign().equals(updatedCallSign)))
					pendingTask.setMessage("User has requested to change Vessel Name and Call Sign from Old Vessel Name : "+ vesselDet.getVesselName() + " to New Vessel Name : "+ avrdto.getShipEq().getVesselDet().getVesselName() + "and from Old Call Sign : " + vesselDet.getCallSign() + " to New Call Sign : " + avrdto.getShipEq().getVesselDet().getCallSign() + ".");
				else			
					pendingTask.setMessage("User has requested to change vessel name from Old Vessel Name : "+ vesselDet.getVesselName() + " to New Vessel Name : "+ avrdto.getShipEq().getVesselDet().getVesselName() + ".");	
			}
			else
			{
				if(!(vesselDet.getCallSign().equals(updatedCallSign)))		
					pendingTask.setMessage("User has requested to change Call Sign from Old Call Sign : " + vesselDet.getCallSign() + " to New Call Sign : " + avrdto.getShipEq().getVesselDet().getCallSign() + ".");	
			}
			PortalPendingTaskMst newPendingTaskEntry = pendingtaskService.addEntryInPendingMst(pendingTask);
			newPendingTaskEntry = pendingtaskService.addEntryInPendingMst(pendingTask);
			logger.info(newPendingTaskEntry.toString());
			logger.info("New Entry Persisted in pending task master table");

			/*this.persistVesselHstEntryForEditVessel(newPendingTaskEntry, updatedVesselName, updatedCallSign);*/
			PortalVesselDetailHistory vesselHist = new PortalVesselDetailHistory();   //set Vessel Detail in transition table
			vesselHist.setPendingTaskId(newPendingTaskEntry.getPendingtaskId());
			vesselHist.setVesselName(updatedVesselName);
			vesselHist.setCallSign(updatedCallSign);
			vesselHist.setUpdatedDate(utilService.getCurrentTimeStamp());
			PortalVesselDetailHistory newVesselDetHst = vesselHstService.addVesselDetailHst(vesselHist);
			logger.info(newVesselDetHst.toString());
			logger.info("New Entry Persisted in vessel detail history table");

			model.addAttribute("successMsg", "Updated Vessel Details Send for Approval Successfully");
			logger.info("Updated Vessel Details Send for Approval Successfully");

		}
		else   //if login id is of admin
		{
			/*this.persistVesselHstEntryForEditVessel(newPendingTaskEntry, vesselDet.getVesselName(), vesselDet.getCallSign());*/
			PortalVesselDetailHistory vesselHist = new PortalVesselDetailHistory();   //set Vessel Detail in history table
			vesselHist.setVesselName(vesselDet.getVesselName());
			vesselHist.setCallSign(vesselDet.getCallSign());
			vesselHist.setUpdatedDate(utilService.getCurrentTimeStamp());
			PortalVesselDetailHistory newVesselDetHst = vesselHstService.addVesselDetailHst(vesselHist);
			logger.info(newVesselDetHst.toString());
			logger.info("New Entry Persisted in vessel detail history table");

			vesselDet.setVesselName(updatedVesselName);    //set vessel detail in main table
			vesselDet.setCallSign(updatedCallSign);	
			vesselDet.setRegStatus(regStatus);
			vesselDet.setRegStatusDate(utilService.getCurrentTimeStamp());
			logger.info("ater: regStatusDate :"+vesselDet.getRegStatusDate());

			avrdto.setShipEq(shipequipDet);
			avrdto.getShipEq().setVesselDet(vesselDet);	
			avrdto.getShipEq().getVesselDet().setPortalVesselGroup(groupDet);	
			avrdto.getShipEq().setModelDet(modelDet);
			avrdto.getShipEq().getModelDet().setManuDet(manuDet);
			PortalShipEquipment approvedshipeu = shipEquipService.addShipEquip(avrdto.getShipEq());

			logger.info("Vessel Details has been updated Successfully");
			model.addAttribute("successMsg", "Vessel Details has been updated Successfully");
		}
		return "acknowledgement";
	}


	//this method is used to update the edited vessel details
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@RequestMapping(value="/updatevesselfornoseid", method=RequestMethod.POST)
	public String updateVesselForNoSeid(@ModelAttribute ViewVesselMigratedDTO vesselDto, Model model)
	{
		logger.info("inside updateVesselForNoSeid() method");
		/*Integer modelId = vesselDto.getModelDet().getModelId();
		Integer manufacturerId  = vesselDto.getModelDet().getManuDet().getManufacturerId();*/
		Long groupId = vesselDto.getVesselDet().getPortalVesselGroup().getGroupId();
		Integer vesselId = vesselDto.getVesselDet().getVesselId();
		String imoNo = vesselDto.getVesselDet().getImoNo();
		String regStatus = vesselDto.getVesselDet().getRegStatus();  //set reg status, reg status date
		String vesselName = vesselDto.getVesselDet().getVesselName();
		String callSign = vesselDto.getVesselDet().getCallSign();
		PortalVesselGroup groupDet = null;

		/*logger.info("ModelId : "+modelId);
		logger.info("ManufacturerId : "+manufacturerId);*/
		logger.info("GroupId : "+groupId);
		/*logger.info("ShipEquipId : "+avrdto.getShipEq().getShipborneEquipmentId());*/
		logger.info("VesselId : "+vesselId);
		logger.info("ImoNo : "+imoNo);

		UserSessionInterface userSession = this.getCurrentSession();  //get requestor lrit id and requestor login id for pending task from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();
		logger.info("userCategory : "+userCategory);

		/*PortalManufacturerDetails manuDet = manuService.findById(manufacturerId).get();
		PortalModelDetail modelDet = modelService.getById(modelId).get();*/
		if(groupId != null)
		{
			groupDet = vesselGroupService.findById(groupId).get();
		}
		PortalVesselDetail vesselDet = vesselService.getByVesselId(vesselId).get();
		/*PortalShipEquipment shipequipDet = shipEquipService.getByvesselDet_vesselIdAndSeidStatus(vesselId);*/
		/*PortalShipEquipment shipequipDet = shipEquipService.getByvesselDet_vesselId(vesselId);*/
		Timestamp regStatusDate = vesselDet.getRegStatusDate();
		logger.info("before :regStatus :"+vesselDet.getRegStatus());
		logger.info("before regStatus :"+regStatus);
		logger.info("before : regStatusDate :"+regStatusDate);

		if(userCategory.equals("USER_CATEGORY_SC"))  //if login id is of shipping company
		{
			PortalPendingTaskMst pendingTask = new PortalPendingTaskMst();   //put entry in pending task for approving edited vessel detail by admin 
			pendingTask.setRequestDate(utilService.getCurrentTimeStamp());
			pendingTask.setStatus("pending");
			pendingTask.setCategory("Edit_Vessel_Details");
			pendingTask.setRequesterLoginId(loginId);   //get login id of user from session who is editing the data
			pendingTask.setRequestorLritid(this.cgid);  //get from user table after getting login id
			pendingTask.setReferenceId(vesselDet.getVesselId().toString());
			if(!(vesselDet.getVesselName().equals(vesselName)))
			{
				if(!(vesselDet.getCallSign().equals(callSign)))
					pendingTask.setMessage("User has requested to change Vessel Name and Call Sign from Old Vessel Name : "+ vesselDet.getVesselName() + " to New Vessel Name : "+ vesselDto.getVesselDet().getVesselName() + "and from Old Call Sign : " + vesselDet.getCallSign() + " to New Call Sign : " + vesselDto.getVesselDet().getCallSign() + ".");
				else			
					pendingTask.setMessage("User has requested to change vessel name from Old Vessel Name : "+ vesselDet.getVesselName() + " to New Vessel Name : "+ vesselDto.getVesselDet().getVesselName() + ".");	
			}
			else
			{
				if(!(vesselDet.getCallSign().equals(callSign)))		
					pendingTask.setMessage("User has requested to change Call Sign from Old Call Sign : " + vesselDet.getCallSign() + " to New Call Sign : " + vesselDto.getVesselDet().getCallSign() + ".");	
			}
			PortalPendingTaskMst newPendingTaskEntry = pendingtaskService.addEntryInPendingMst(pendingTask);
			logger.info(newPendingTaskEntry.toString());
			logger.info("New Entry Persisted in pending task master table");

			PortalVesselDetailHistory vesselHist = new PortalVesselDetailHistory();   //set Vessel Detail in transition table
			vesselHist.setPendingTaskId(newPendingTaskEntry.getPendingtaskId());
			vesselHist.setVesselName(vesselName);
			vesselHist.setCallSign(callSign);
			vesselHist.setUpdatedDate(utilService.getCurrentTimeStamp());
			PortalVesselDetailHistory newVesselDetHst = vesselHstService.addVesselDetailHst(vesselHist);
			logger.info(newVesselDetHst.toString());
			logger.info("New Entry Persisted in vessel detail history table");

			model.addAttribute("successMsg", "Updated Vessel Details Send for Approval Successfully");
			logger.info("Updated Vessel Details Send for Approval Successfully");

		}
		else   //if login id is of admin
		{		
			PortalVesselDetailHistory vesselHist = new PortalVesselDetailHistory();   //set Vessel Detail in history table
			vesselHist.setVesselName(vesselDet.getVesselName());
			vesselHist.setCallSign(vesselDet.getCallSign());
			vesselHist.setUpdatedDate(utilService.getCurrentTimeStamp());
			PortalVesselDetailHistory newVesselDetHst = vesselHstService.addVesselDetailHst(vesselHist);
			logger.info(newVesselDetHst.toString());
			logger.info("New Entry Persisted in vessel detail history table");

			vesselDet.setVesselName(vesselName);    //set vessel detail in main table
			vesselDet.setCallSign(callSign);	
			vesselDet.setRegStatus(regStatus);
			vesselDet.setRegStatusDate(utilService.getCurrentTimeStamp());
			logger.info("ater: regStatusDate :"+vesselDet.getRegStatusDate());

			/*avrdto.setShipEq(shipequipDet);
			avrdto.getShipEq().setVesselDet(vesselDet);
			avrdto.getShipEq().getVesselDet().setPortalVesselGroup(groupDet);
			avrdto.getShipEq().setModelDet(modelDet);
			avrdto.getShipEq().getModelDet().setManuDet(manuDet);
			PortalShipEquipment approvedshipeu = shipEquipService.addShipEquip(avrdto.getShipEq());*/
			vesselDto.setVesselDet(vesselDet);
			vesselDto.getVesselDet().setPortalVesselGroup(groupDet);
			PortalVesselDetail updatedVessel = vesselService.addVessel(vesselDto.getVesselDet());

			logger.info("Vessel Details has been updated Successfully");
			model.addAttribute("successMsg", "Vessel Details has been updated Successfully");
		}
		/*return "/vessel/allvessel";*/
		return "acknowledgement";
	}


	//this method is used to show the add shipborne equipment detail page to user
	@PreAuthorize(value = "hasAuthority('addshipborneequipment')")
	@RequestMapping(value="/addshipequipment", method=RequestMethod.POST)
	public String addShipequipment(@RequestParam Integer vesselId, Model model, HttpServletRequest request)
	{	
		logger.info("inside addShipequipment() method");
		UserSessionInterface userSession = this.getCurrentSession();   //get requestor lrit id and requestor login id for pending task from session
		String userCategory = userSession.getCategory();
		logger.info("userCategory : "+userCategory);
		logger.info("vesselId : "+vesselId);

		/*PortalShipEquipment shipEquipDet = shipEquipService.findByvesselDet_vesselId(vesselId);*/
		PortalVesselDetail vesselDet = vesselService.getByVesselId(vesselId).get();
		PortalShipEquipment newShipEquipDet = new PortalShipEquipment();
		newShipEquipDet.setVesselDet(vesselDet);
		newShipEquipDet.setImoNo(vesselDet.getImoNo());
		logger.info("newShipEquipDet : "+newShipEquipDet.toString());

		List<ManufacturerList> manufacturerName = manuService.getAll();
		model.addAttribute("manufacturerName", manufacturerName);

		request.setAttribute("userCategory", userCategory);
		model.addAttribute("newShipEquipDet", newShipEquipDet);
		return "/vessel/addseid";
	}

	//this method is used to add the shipborne equipment if the old shipborne equipment is deleted
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@RequestMapping(value="/persistshipequipment", method=RequestMethod.POST)
	public String persistShipEquipment(@ModelAttribute("newShipEquipDet") PortalShipEquipment newShipEquipDet, @RequestParam("supportingDocs1") MultipartFile documentUploaded, Model model) throws Exception, JAXBException
	{	
		logger.info("inside persistShipEquipmentDetail() method");
		Response res = null;
		boolean result = false;
		byte[] supportingDOcument = null;
		/*String dnidStatus = null;*/
		String shipequiId = newShipEquipDet.getShipborneEquipmentId();
		Integer vesselId = newShipEquipDet.getVesselDet().getVesselId();
		String shipEquipId = newShipEquipDet.getShipborneEquipmentId();
		Integer manufacturerId = newShipEquipDet.getModelDet().getManuDet().getManufacturerId();
		Integer modelId = newShipEquipDet.getModelDet().getModelId();
		String imoNo = newShipEquipDet.getImoNo();
		String oceanRegion = newShipEquipDet.getOceanRegion();

		UserSessionInterface userSession = this.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String requestor_lritId = userSession.getRequestorsLritId().getLritId();
		logger.info("requestor_lritId : "+requestor_lritId);
		String userCategory = userSession.getCategory();
		logger.info("userCategory : "+userCategory);

		result = shipEquipService.isShipEquipIdExist(shipequiId);
		if (result == true)
		{
			model.addAttribute("error", "Shipborne Equipment Id already exist");
			model.addAttribute("errorMsg", "Shipborne Equipment Id already exist");
			logger.error("Shipborne Equipment Id already exist");
		}
		else
		{
			PortalVesselDetail vesselDet = vesselService.getByVesselId(vesselId).get();
			PortalManufacturerDetails manuDet = manuService.findById(manufacturerId).get();
			PortalModelDetail modelDet = modelService.getById(modelId).get();
			String regStatus = vesselDet.getRegStatus();

			/*newShipEquipDet.setSeidStatusDate(utilService.getCurrentTimeStamp());*/
			/*vesselDet.setRegStatus("DNID_DW_REQ");*/

			newShipEquipDet.setVesselDet(vesselDet);   //add new seid
			newShipEquipDet.setModelDet(modelDet);
			newShipEquipDet.getModelDet().setManuDet(manuDet);
			logger.info("newShipEquipDet : "+ newShipEquipDet.toString());
			PortalShipEquipment approvedshipequip = shipEquipService.addShipEquip(newShipEquipDet);
			logger.info("Shipborne Equipment Details added successfully");

			PortalDocuments portalDocs = new PortalDocuments();   //entry of supporting document for changing/adding Shipborne Equipment
			portalDocs.setReferenceId(shipEquipId);
			portalDocs.setTableName("Portal_Ship_Equipment");

			try {
				supportingDOcument = documentUploaded.getBytes();
				portalDocs.setDocument(supportingDOcument);
			}
			catch (Exception e) 
			{
				logger.error("Uploaded Document size invalid");
				model.addAttribute("errorMsg", "Uploaded Document size invalid");
				e.printStackTrace();
			}

			portalDocs.setDocPurpose("Change SEID Document");
			PortalDocuments docs = docsService.addDocument(portalDocs);
			logger.info("Supporting doucment persisted successfuly");

			if(userCategory.equals("USER_CATEGORY_DGS"))
			{
				Integer dnidNo = newShipEquipDet.getDnidNo();  //get dnid no and member no if admin
				Integer memberNo = newShipEquipDet.getMemberNo();
				result = shipEquipService.updateShipEquipById(dnidNo, memberNo, shipEquipId);

				if(result == true)
				{
					logger.info("dnid no and member no updated successfully");
					final String requestType = "51";
					final String schemaVersion = "1.0";
					final String messageType= "18";
					final String test = "0";
					DNIDRequestType dnidNew = new DNIDRequestType();   //preparing dnid request packet
					/*String messageId = utilService.generateMessageID("1065");*/
					String messageId = utilService.generateMessageID(requestor_lritId);
					dnidNew.setMessageId(messageId);
					dnidNew.setIMONum(imoNo);
					dnidNew.setDNIDNo(dnidNo.toString());
					dnidNew.setMemberNo(memberNo);
					dnidNew.setMessageType(new BigInteger(messageType));
					dnidNew.setDataUserProvider(requestor_lritId);     //DataUserProvider get from session
					dnidNew.setDataUserRequestor(requestor_lritId);    //DataUserRequestor get from session
					dnidNew.setOceanRegion(new BigInteger(oceanRegion));
					dnidNew.setRequestType(new BigInteger(requestType));
					dnidNew.setSchemaVersion(new BigDecimal(schemaVersion));
					dnidNew.setTest(new BigInteger(test));
					Timestamp ts = utilService.getCurrentTimeStamp();
					dnidNew.setTimeStamp(utilService.getgregorianTimeStamp(ts));

					JAXBContext jaxbContext = JAXBContext.newInstance(DNIDRequestType.class);
					Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
					jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
					StringWriter sw = new StringWriter();
					jaxbMarshaller.marshal(dnidNew, sw);	
					logger.info(sw.toString());
					/*newShipEquipDet.setRequestPayload(sw.toString());*/  //set request payload and status

					result = shipEquipService.updateRequestPayload(sw.toString(), shipequiId);
					if(result == true)
						logger.info("request payload updated successfully");
					else 
						logger.info("request payload updation failed");


					try    //call to ASP
					{
						regStatus = "DNID_DW_FAILED";

						res = aspsoapConnector.callASPWebService(aspUrl, dnidNew);
						logger.info("response after calling ASP : "+res.getResponse().toString());

						if(res.getResponse().toString().equals("SUCCESS"))
						{
							regStatus = "DNID_DW_REQ";
							/*dnidStatus = "CallToASPSucess";*/
							model.addAttribute("successMsg", "DNID Download Request Sent To ASP Successfully for Message Id : "+ messageId);
							logger.info("DNID Request Sent To ASP Successfully");
						}
						else
						{
							/*dnidStatus = "CallToASPSucess";*/
							model.addAttribute("errorMsg", "Validation Error while sending DNID Download Request To ASP: " + res.getResponse().toString() + " for Message Id : "+ messageId);
							logger.info("Validation Error while sending DNID Request To ASP");
						}
						result = shipEquipService.updateResponsePayload(res.getResponse().toString(), shipequiId);
						if(result == true)
							logger.info("response payload updated successfully");
						else 
							logger.info("response payload updation failed");

					}
					catch(Exception e)
					{
						logger.info("ASP webservice is Down");
						model.addAttribute("errorMsg", "Could not sent DNID Download Request To ASP for Message Id : "+messageId + " as ASP webservice is Down. Please Re-Download DNID.");
						e.printStackTrace();		
					}

					/*result = shipEquipService.updatednidStatusAnddnidStatusDate(dnidStatus, approvedshipequip.getShipborneEquipmentId());*/			
					result = vesselService.updateVesselRegStatus(regStatus, vesselId);
					logger.info("result : " + result);
					if(result == true)
						logger.info("registration status updated successfully");
					else 
						logger.info("registration status updation failed");	
					/*model.addAttribute("successMsg", "Shipborne equipment has been added successfully");*/

				}
				else
				{
					logger.info("dnid no and member no updated failed");
					model.addAttribute("errorMsg", "Unable to add dnid no and member no, Database Error. Try Again!!");		
				}
			}
			else
			{	
				regStatus = "SEID_ADDED";				
				result = vesselService.updateVesselRegStatus(regStatus, vesselId);
				logger.info("result : " + result);
				if(result == true)
					logger.info("registration status updated successfully");
				else 
					logger.info("registration status updation failed");	
				model.addAttribute("successMsg", "Add Shiborne Equipment Request Sent to Admin Successfully");
			}
		}

		model.addAttribute("request", "addseid");   //add request attribute required to go to showallforaction
		return "acknowledgement";
	}

	//this method is used to show vessel available for vessel action and add seid according to request variable passed
	@PreAuthorize(value = "hasAuthority('vesselaction')")
	@RequestMapping(value="/vesselaction", method=RequestMethod.GET)
	public String vesselAction(@RequestParam String request, Model model)
	{	
		logger.info("inside vesselAction() method");
		UserSessionInterface userSession = this.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();

		if(userCategory.equals("USER_CATEGORY_SC"))    //to show register vessel form to shipping company only with default value
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String ownerCompanyCode = portalUser.getPortalShippingCompany().getCompanyCode();
			logger.info("ownerCompanyCode : " + ownerCompanyCode);
			model.addAttribute("ownerCompanyCode", ownerCompanyCode);
		}
		model.addAttribute("userCategory", userCategory);
		logger.info("request : "+request);
		model.addAttribute("request", request);
		return "/vessel/showallforaction";
	}

	//this method is used to show reporting status page for vessel action menu
	@RequestMapping(value="/reportingstatus", method=RequestMethod.POST)
	public String reportingStatus(@RequestParam Integer vesselId, Model model)
	{	
		logger.info("inside reportingStatus() method");

		UserSessionInterface userSession = this.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();

		logger.info("vesselId : "+vesselId);
		PortalVesselDetail vesselDet = vesselService.getByVesselId(vesselId).get();
		String status = vesselDet.getStatus();
		logger.info("status : "+status);
		model.addAttribute("vesselDet", vesselDet);
		model.addAttribute("userCategory", userCategory);
		return "/vessel/reportingstatus";
	}

	//this method is used to update reporting status that user changed in vessel action menu
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@RequestMapping(value="/updatereportingstatus", method=RequestMethod.POST)
	public String updateReportingStaus(@RequestParam Integer vesselId, @RequestParam String status, @RequestParam String reportingStatusChangeReason, @RequestParam String reportingStatusChangeRemarks, @RequestParam String action, @RequestParam("portalDocs.document1") MultipartFile documentUploadedInactive, @RequestParam("portalDocs.document2") MultipartFile documentUploadedSell, @RequestParam("portalDocs.document3") MultipartFile documentUploadedScrap, Model model, HttpServletRequest request)
	{	
		logger.info("inside updateReportingStaus() method");

		boolean dnidseidDeletedStatus = false;
		boolean result = false;
		byte[] supportingDOcument = null;
		logger.info("id"+vesselId);
		logger.info("action"+action);
		logger.info("status" + status);
		logger.info("reportingStatusChangeReason"+reportingStatusChangeReason);
		logger.info("reportingStatusChangeRemarks"+reportingStatusChangeRemarks);
		long size=documentUploadedSell.getSize() ;
		logger.info("document upload is not null "+size);



		/*PortalShipEquipment shipEqDet = shipEquipService.getByvesselDet_vesselId(vesselId);*/
		//PortalShipEquipment shipEq = shipEquipService.findByvesselDet_vesselId(vesselId);

		UserSessionInterface userSession = this.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();
		String requestorLritId = userSession.getRequestorsLritId().getLritId();

		PortalVesselDetail vesselDet = vesselService.getByVesselId(vesselId).get();
		/*PortalShipEquipment shipEqDet = shipEquipService.getByvesselDet_vesselId(vesselId);*/	

		if(action.equals("Change Vessel Reporting Status"))
		{
			logger.info("inside change vessel reporting status ");
			//PortalShipEquipment shipEqDet = shipEquipService.getByvesselDet_vesselId(vesselId);				
			//logger.info("before setting shipEqDet: "+shipEqDet.toString());
			//logger.info("old status" + shipEqDet.getVesselDet().getStatus());			
			//if((shipEqDet.getVesselDet().getStatus().equalsIgnoreCase(status)))   //check if reporting status has been changed

			if(userCategory.equals("USER_CATEGORY_SC"))
			{
				if((vesselDet.getStatus().equalsIgnoreCase(status)))   //check if reporting status has been changed
				{
					model.addAttribute("errorMsg", "Reporting Status has not been changed. Please change the reporting status.");
					logger.error("Reporting Status has not been changed");
				}
				else
				{
					if(status.equals("INACTIVE"))
					{	
						/*PortalShipEquipment shipEqDet = shipEquipService.getByvesselDet_vesselId(vesselId);
							result = vesselService.isEligibleToInactive(shipEqDet.getShipborneEquipmentId());*/   //check if vessel is eligible for inactive status

						result = vesselService.isEligibleToInactive(vesselId);   //check if vessel is eligible for inactive status
						if(result == true)
						{				
							PortalPendingTaskMst pendingTask = new PortalPendingTaskMst();   //prepare and persist pending task entry
							/*this.preaprePendingTaskEntry(vesselId, status, pendingTask, vesselDet, action);*/
							this.preaprePendingTaskEntry(vesselId, status, pendingTask, vesselDet);
							PortalPendingTaskMst newPendingTaskEntry = pendingtaskService.addEntryInPendingMst(pendingTask);
							logger.info(newPendingTaskEntry.toString());
							logger.info("New Entry Persisted in pending task master table");

							PortalVesselReportingStatusHistory vesselReportingStatus = new PortalVesselReportingStatusHistory();   //prepare and persist new reporting entry in transition table
							this.prepareVesselReportingTransitionEntry(vesselReportingStatus, newPendingTaskEntry, status, reportingStatusChangeReason, reportingStatusChangeRemarks, documentUploadedInactive);
							PortalVesselReportingStatusHistory newEntryReportingHst = reportingHstService.addReportingStatusHst(vesselReportingStatus);
							logger.info(newEntryReportingHst.toString());
							logger.info("New Entry Persisted in vessel reporting status history table");

							model.addAttribute("successMsg", "Request has been sent to Approve for Changing Vessel Reporting Status ");
							logger.info("Request has been sent to Approve for Changing Vessel Reporting Status ");
						}
						else
						{
							model.addAttribute("errorMsg", "Vessel is not eligibe to set Inactive. Please Delete DNID & SEID First.!!");
							logger.info("Vessel is not eligibe to set Inactive. Please Delete DNID & SEID First.");
						}
					}
					else if(status.equals("ACTIVE"))
					{
						result = vesselService.isEligibleToActive(vesselId);

						if(result == true)
						{			
							PortalPendingTaskMst pendingTask = new PortalPendingTaskMst();    //prepare and persist pending task entry
							/*this.preaprePendingTaskEntry(vesselId, status, pendingTask, vesselDet, action);*/
							this.preaprePendingTaskEntry(vesselId, status, pendingTask, vesselDet);
							PortalPendingTaskMst newPendingTaskEntry = pendingtaskService.addEntryInPendingMst(pendingTask);
							logger.info(newPendingTaskEntry.toString());
							logger.info("New Entry Persisted in pending task master table");

							PortalVesselReportingStatusHistory vesselReportingStatus = new PortalVesselReportingStatusHistory();   //prepare and persist reporting history
							/*this.prepareVesselReportingTransitionEntry(vesselReportingStatus, newPendingTaskEntry, status, reportingStatusChangeReason, reportingStatusChangeRemarks, documentUploaded);*/
							vesselReportingStatus.setPendingTaskId(newPendingTaskEntry.getPendingtaskId());
							vesselReportingStatus.setReportingStatus(status);
							PortalVesselReportingStatusHistory newEntryReportingHst = reportingHstService.addReportingStatusHst(vesselReportingStatus);
							logger.info(newEntryReportingHst.toString());
							logger.info("New Entry Persisted in vessel reporting status history table");

							model.addAttribute("successMsg", "Request has been sent to Approve for Changing Vessel Reporting Status ");
							logger.info("Request has been sent to Approve for Changing Vessel Reporting Status ");
						}
						else
						{
							model.addAttribute("errorMsg", "Vessel is not eligibe to set Active!!");
							logger.info("Vessel is not eligibe to set Active");
						}
					}
					else
					{
						model.addAttribute("errorMsg", "You have not selected valid Reporting Status. Please select valid Reporting Status!!");
						logger.info("User have not selected valid Reporting Status.");
					}
				}
			}
			else
			{
				model.addAttribute("errorMsg", "LoggedIn User Category is not allowed to change reporting status of ship");
				logger.info("LoggedIn User Category is not allowed to change reporting status of ship");
			}	
		}

		else if((action.equals("Sell Ship")) || (action.equals("Scrap Ship")))
		{
			logger.info("inside sell/scrap vessel");
			//check if all dnid and seid deleted/deregistered for the selected ship
			/*dnidseidDeletedStatus = shipEquipService.getIfDnidSeidStatusDeleted(shipEqDet.getShipborneEquipmentId());*/  

			if(userCategory.equals("USER_CATEGORY_SC"))
			{
				dnidseidDeletedStatus = shipEquipService.getIfDnidSeidStatusDeleted(vesselId);
				if(dnidseidDeletedStatus == true)
				{	
					String regStatus= null;

					if(action.equals("Sell Ship"))
					{
						regStatus = "SOLD";	

						PortalPendingTaskMst pendingTask = new PortalPendingTaskMst();   //prepare and persist pending task entry
						this.preaprePendingTaskEntry(vesselId, status, pendingTask, regStatus);	
						PortalPendingTaskMst newPendingTaskEntry = pendingtaskService.addEntryInPendingMst(pendingTask);
						logger.info(newPendingTaskEntry.toString());
						logger.info("New Entry Persisted in pending task master table");

						PortalVesselReportingStatusHistory vesselReportingStatus = new PortalVesselReportingStatusHistory();    //prepare and persist reporting history
						this.prepareVesselReportingTransitionEntry(vesselReportingStatus, newPendingTaskEntry, regStatus, documentUploadedSell);
						PortalVesselReportingStatusHistory newEntryReportingHst = reportingHstService.addReportingStatusHst(vesselReportingStatus);
						logger.info(newEntryReportingHst.toString());
						logger.info("New Entry Persisted in vessel reporting status history table");

						model.addAttribute("successMsg", "Request has been sent to Approve for Changing Vessel Status");
						logger.info("Request has been sent to Approve for Changing Vessel Status");
					}
					else
					{
						regStatus = "SCRAP";

						PortalPendingTaskMst pendingTask = new PortalPendingTaskMst();   //prepare and persist pending task entry
						this.preaprePendingTaskEntry(vesselId, status, pendingTask, regStatus);	
						PortalPendingTaskMst newPendingTaskEntry = pendingtaskService.addEntryInPendingMst(pendingTask);
						logger.info(newPendingTaskEntry.toString());
						logger.info("New Entry Persisted in pending task master table");

						PortalVesselReportingStatusHistory vesselReportingStatus = new PortalVesselReportingStatusHistory();    //prepare and persist reporting history
						this.prepareVesselReportingTransitionEntry(vesselReportingStatus, newPendingTaskEntry, regStatus, documentUploadedScrap);
						PortalVesselReportingStatusHistory newEntryReportingHst = reportingHstService.addReportingStatusHst(vesselReportingStatus);
						logger.info(newEntryReportingHst.toString());
						logger.info("New Entry Persisted in vessel reporting status history table");

						model.addAttribute("successMsg", "Request has been sent to Approve for Changing Vessel Status");
						logger.info("Request has been sent to Approve for Changing Vessel Status");
					}
				}
				else
				{
					model.addAttribute("errorMsg", "Vessel cannot be Sell/Scrap. Please Delete DNID & SEID First.");
					logger.info("Vessel cannot be Sell/Scrap. Please Delete DNID & SEID First");
				}
			}			
			else
			{
				model.addAttribute("errorMsg", "LoggedIn User Category is not allowed to sell/scrap ship");
				logger.info("LoggedIn User Category is not allowed to sell/scrap ship");
			}				
		}
		else
		{
			model.addAttribute("errorMsg", "Please select appropriate Vessel Action");
			logger.info("Please select appropriate Vessel Action");
		}
		return "acknowledgement";
	}


	//this method is used to show ajax request error
	@RequestMapping(value="/repurchase", method=RequestMethod.POST)
	public String repurchaseVessel(@RequestParam Integer vesselId, HttpServletRequest request, Model model)
	{
		logger.info("inside repurchaseVessel() method");

		UserSessionInterface userSession = this.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();

		if(userCategory.equals("USER_CATEGORY_SC"))    //to show register vessel form to shipping company only with default value
		{
			PortalUser portalUser = userService.getbyloginId(loginId).get();
			String userName = portalUser.getName();
			logger.info("userName : "+userName);
			request.setAttribute("userId", loginId);
			request.setAttribute("userName", userName);
			String ownerCompanyCode = portalUser.getPortalShippingCompany().getCompanyCode();
			String ownerCompanyName = portalUser.getPortalShippingCompany().getCompanyName();
			request.setAttribute("ownerCompanyCode", ownerCompanyCode);
			request.setAttribute("ownerCompanyName", ownerCompanyName);
			logger.info("ownerCompanyCode : "+ownerCompanyCode);
			logger.info("ownerCompanyName : "+ownerCompanyName);

			if(vesselService.isVesselSold(vesselId))    //check if selected ship status is sold
			{
				PortalVesselDetail vesselrepurchase = vesselService.getByVesselId(vesselId).get();
				logger.info("vesselrepurchase : "+vesselrepurchase.toString());
				String imoNo = vesselrepurchase.getImoNo();
				String mmsiNo = vesselrepurchase.getMmsiNo();
				logger.info("imoNo " + imoNo + " mmsiNo " + mmsiNo);

				VesselRegistrationDTO vrdto = new VesselRegistrationDTO();
				PortalShipEquipment portalShipEquipment = new PortalShipEquipment();
				DocumentForm documentForm = new DocumentForm();

				portalShipEquipment.setVesselDet(vesselrepurchase);

				List<PortalShippingCompanyVesselRelation> pscvrList = new ArrayList<PortalShippingCompanyVesselRelation>();
				pscvrList.add(new PortalShippingCompanyVesselRelation());
				pscvrList.add(new PortalShippingCompanyVesselRelation());
				pscvrList.add(new PortalShippingCompanyVesselRelation());
				vrdto.setDocs(documentForm);
				vrdto.setPscvrList(pscvrList);
				vrdto.setShipEq(portalShipEquipment);
				List<ManufacturerList> manufacturerName = manuService.getAll();
				model.addAttribute("manufacturerName", manufacturerName);
				ArrayList<GroupList> groupNames = vesselGroupService.getAllGroup();
				model.addAttribute("groupNames", groupNames);
				List<String> vesselTypeList = typeService.getVesselTypeList();
				model.addAttribute("vesselTypeList", vesselTypeList);

				model.addAttribute("successMsg", "Vessel is Available To Repurchase");
				logger.info("Vessel Is Available To Repurchase");

				model.addAttribute("vrdto", vrdto);
				model.addAttribute("repurchase", "true");
				return "/vessel/showrepurchaseship";
			}
			else
			{
				model.addAttribute("errorMsg", "Vessel is Not Available To Repurchase.");
				logger.info("Vessel is Not Available To Repurchase");
			}
		}
		else
		{
			model.addAttribute("errorMsg", "LoggedIn User Category is not allowed to repurchase ship");
			logger.info("LoggedIn User Category is not allowed to repurchase ship");
		}
		return "acknowledgement";
	}


	//this method is used to show ajax request error
	@RequestMapping(value="/errorPage", method=RequestMethod.GET)
	public String showErrorPage(Model model)
	{
		logger.info("inside showErrorPage() method");
		logger.error("There is some error. Check requested url!!");
		model.addAttribute("errorMsg", "There is some error. Please refresh the page and Try Again !!");
		return "acknowledgement";
	}

	//this method is used to pending task entry values
	public void preaprePendingTaskEntry(Integer vesselId, String status, PortalPendingTaskMst pendingTask, PortalVesselDetail vesselDet)
	{
		logger.info("inside preaprePendingTaskEntry() method");
		String message = null;
		UserSessionInterface userSession = this.getCurrentSession();   //get login_id and requestor_lrit_id from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String requestLritId = userSession.getRequestorsLritId().getLritId();
		logger.info("requestLritId : "+requestLritId);

		pendingTask.setRequestDate(utilService.getCurrentTimeStamp());
		pendingTask.setStatus("pending");

		if(status.equals("INACTIVE"))
			pendingTask.setCategory("Active_to_Inactive");
		else
			pendingTask.setCategory("Inactive_to_Active");

		pendingTask.setRequesterLoginId(loginId);
		pendingTask.setRequestorLritid(requestLritId); 
		pendingTask.setReferenceId(vesselId.toString());
		/*if((action.equals("Sell Ship")) || (action.equals("Scrap Ship")))
		{
			message = "Vessel_Status_Changed_From_" + shipEqDet.getVesselDet().getRegStatus() + "_To_" + action + ".";
			message = "Vessel_Status_Changed_From_" + vesselDet.getRegStatus() + "_To_" + action + ".";
			pendingTask.setMessage(message);
		}*/
		/*else
		{*/
		/*message = "Vessel_Status_Changed_From_" + shipEqDet.getVesselDet().getStatus() + "_To_" + status + ".";*/
		message = "Vessel_Status_Changed_From_" + vesselDet.getStatus() + "_To_" + status + ".";
		pendingTask.setMessage(message);
		/*}*/
	}

	//this method is used to pending task entry values for sold/scrap case
	public void preaprePendingTaskEntry(Integer vesselId, String status, PortalPendingTaskMst pendingTask, String regStatus)
	{
		logger.info("inside preaprePendingTaskEntry() method");
		String message = null;
		UserSessionInterface userSession = this.getCurrentSession();   //get login_id and requestor_lrit_id from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String requestLritId = userSession.getRequestorsLritId().getLritId();
		logger.info("requestLritId : "+requestLritId);

		pendingTask.setRequestDate(utilService.getCurrentTimeStamp());
		pendingTask.setStatus("pending");

		if(regStatus.equals("SOLD"))
			pendingTask.setCategory("Vessel_Sold");
		else
			pendingTask.setCategory("Vessel_Scrap");

		pendingTask.setRequesterLoginId(loginId);
		pendingTask.setRequestorLritid(requestLritId); 
		pendingTask.setReferenceId(vesselId.toString());
		message = "Vessel_Status_Changed_From_" + "SEID_DELETED" + "_To_" + regStatus + ".";
		pendingTask.setMessage(message);
	}

	//this method is used to set vessel reporting transition entry values
	public void prepareVesselReportingTransitionEntry(PortalVesselReportingStatusHistory vesselReportingStatus, PortalPendingTaskMst newPendingTaskEntry, String status, String reportingStatusChangeReason, String reportingStatusChangeRemarks, MultipartFile documentUploaded)
	{
		logger.info("inside prepareVesselReportingTransitionEntry() method");
		byte [] supportingDoc = null;
		vesselReportingStatus.setPendingTaskId(newPendingTaskEntry.getPendingtaskId());
		vesselReportingStatus.setReportingStatus(status);
		vesselReportingStatus.setReason(reportingStatusChangeReason);
		vesselReportingStatus.setRemark(reportingStatusChangeRemarks);
		try {
			supportingDoc = documentUploaded.getBytes();
			vesselReportingStatus.setSupportingDocument(supportingDoc);
		} catch (Exception e) {
			logger.error("Supported Document size invalid");
			e.printStackTrace();
		}
	}

	//this method is used to set vessel reporting transition entry values
	public void prepareVesselReportingTransitionEntry(PortalVesselReportingStatusHistory vesselReportingStatus, PortalPendingTaskMst newPendingTaskEntry, String status, MultipartFile documentUploaded)
	{
		logger.info("inside prepareVesselReportingTransitionEntry() method");
		byte [] supportingDoc = null;
		vesselReportingStatus.setPendingTaskId(newPendingTaskEntry.getPendingtaskId());
		vesselReportingStatus.setReportingStatus(status);
		try {
			supportingDoc = documentUploaded.getBytes();
			vesselReportingStatus.setSupportingDocument(supportingDoc);
		} catch (Exception e) {
			logger.error("Supported Document size invalid");
			e.printStackTrace();
		}
	}

	//this method is used to get current user session
	public UserSessionInterface getCurrentSession()
	{
		logger.info("inside getCurrentSession() method");
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes(); 
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface  userSession = (UserSessionInterface) session.getAttribute("USER"); 
		return userSession;
	}

	//this method is used to create vessel DTO to register vessel
	public void prepareCreateShipDTO(VesselRegistrationDTO vrdto, Model model) 
	{
		logger.info("inside prepareCreateShipDTO() method");
		PortalShipEquipment portalShipEquipment = new PortalShipEquipment();
		PortalVesselDetail portalVessel = new PortalVesselDetail();
		DocumentForm documentForm = new DocumentForm();

		List<PortalShippingCompanyVesselRelation> pscvrList = new ArrayList<PortalShippingCompanyVesselRelation>();
		pscvrList.add(new PortalShippingCompanyVesselRelation());
		pscvrList.add(new PortalShippingCompanyVesselRelation());
		pscvrList.add(new PortalShippingCompanyVesselRelation());

		portalShipEquipment.setVesselDet(portalVessel);
		vrdto.setDocs(documentForm);
		vrdto.setPscvrList(pscvrList);
		vrdto.setShipEq(portalShipEquipment);

		List<ManufacturerList> manufacturerName = manuService.getAll();
		model.addAttribute("manufacturerName", manufacturerName);

		ArrayList<GroupList> groupNames = vesselGroupService.getAllGroup();
		model.addAttribute("groupNames", groupNames);

		List<String> vesselTypeList = typeService.getVesselTypeList();
		model.addAttribute("vesselTypeList", vesselTypeList);
	}

	//this method is used to prepare vessel DTO to view vessel details
	public Object prepareViewShipDTO(PortalVesselDetail vessel, Model model)
	{
		logger.info("inside prepareViewShipDTO() method");
		logger.info("vessel : "+ vessel.toString());
		/*DocumentForm approvedDocForm = null;*/
		ApprovedVesselRegistartionDTO avrdto = new ApprovedVesselRegistartionDTO();
		List<PortalShippingCompanyVesselRelation> pscvrList = pscvrService.getBycustomId_vesselDet_vesselId(vessel.getVesselId());
		List<MappingNames> namelist = new ArrayList<MappingNames>();
		List<PortalDocuments> docs = new ArrayList<PortalDocuments>();
		this.setVesselCompanyMapping(namelist, vessel.getVesselId());
		this.setDocuments(docs, vessel.getVesselId());
		/*this.setDocuments(approvedDocForm, vessel.getVesselId());*/
		/*PortalShipEquipment approvedportalvessel = shipEquipService.getByvesselDet_vesselIdAndSeidStatus(vessel.getVesselId());*/
		PortalShipEquipment approvedportalvessel = shipEquipService.getByvesselDet_vesselId(vessel.getVesselId());
		if(approvedportalvessel != null)
			avrdto.setShipEq(approvedportalvessel);		
		else
		{
			ViewVesselMigratedDTO viewDto = new ViewVesselMigratedDTO();
			viewDto.setVesselDet(vessel);
			viewDto.setDocs(docs);
			viewDto.setNameslist(namelist);
			return viewDto;
		}
		avrdto.setNameslist(namelist);
		/*avrdto.setDocs(approvedDocForm);*/
		return avrdto;
	}

	public void setVesselCompanyMapping(List<MappingNames> namelist, Integer vesselId)
	{
		String dpaName = "Not Available";
		List<PortalShippingCompanyVesselRelation> pscvrList = pscvrService.getBycustomId_vesselDet_vesselId(vesselId);
		PortalShippingCompanyVesselRelation owner = new PortalShippingCompanyVesselRelation();
		PortalShippingCompanyVesselRelation technical = new PortalShippingCompanyVesselRelation();
		PortalShippingCompanyVesselRelation commercial = new PortalShippingCompanyVesselRelation();

		MappingNames ownerNameList = new MappingNames();
		MappingNames technicalNameList = new MappingNames();
		MappingNames commercialNameList = new MappingNames();

		Map<Integer, PortalShippingCompanyVesselRelation> createmap = new HashMap<Integer, PortalShippingCompanyVesselRelation>();
		for (PortalShippingCompanyVesselRelation portalShippingCompanyVesselRelation : pscvrList) {
			createmap.put(portalShippingCompanyVesselRelation.getRel(), portalShippingCompanyVesselRelation);
		}

		namelist.add(ownerNameList);
		namelist.add(technicalNameList);
		namelist.add(commercialNameList);

		logger.info("listofSize" + pscvrList.size());
		if(pscvrList.size()==3)	
		{
			logger.info("relationId1"+pscvrList.get(0).getRel());
			logger.info("relationId2"+pscvrList.get(1).getRel());
			logger.info("relationId3"+pscvrList.get(2).getRel());
			if(createmap.containsKey(0))
				owner = createmap.get(0);
			if(createmap.containsKey(1))
				commercial = createmap.get(1);
			if(createmap.containsKey(2))
				technical = createmap.get(2);	
		}
		else if(pscvrList.size()==2)
		{
			logger.info("relationId1 : "+pscvrList.get(0).getRel());
			logger.info("relationId2 : "+pscvrList.get(1).getRel());
			if((createmap.containsKey(0)) && (createmap.containsKey(5)))
			{
				owner = createmap.get(0);
				technical = createmap.get(5);
				commercial = createmap.get(5);
			}
			else if((createmap.containsKey(1)) && (createmap.containsKey(4)))
			{
				commercial = createmap.get(1);
				owner = createmap.get(4);
				technical = createmap.get(4);
			}
			else if((createmap.containsKey(2)) && (createmap.containsKey(3)))
			{
				technical = createmap.get(2);
				owner = createmap.get(3);
				commercial = createmap.get(3);
			}
		}
		else
		{
			logger.info("relationId : "+pscvrList.get(0).getRel());
			if(pscvrList.get(0).getRel() == 6)
			{
				owner = pscvrList.get(0);
				technical = pscvrList.get(0);
				commercial = pscvrList.get(0);
			}
		}
		ownerNameList.setCompanyName(owner.getCustomId().getUserDet().getPortalShippingCompany().getCompanyName());
		ownerNameList.setUserName(owner.getCustomId().getUserDet().getName());
		ownerNameList.setCsoName(csodpaService.findBylritCsoIdSeq(owner.getCcsoId()).get().getName());
		ownerNameList.setAcsoName(csodpaService.findBylritCsoIdSeq(owner.getAcsoId()).get().getName());
		if(owner.getDpaId() != null)
			ownerNameList.setDpaName(csodpaService.findBylritCsoIdSeq(owner.getDpaId()).get().getName());
		else
			ownerNameList.setDpaName(dpaName);

		commercialNameList.setCompanyName(commercial.getCustomId().getUserDet().getPortalShippingCompany().getCompanyName());
		commercialNameList.setUserName(commercial.getCustomId().getUserDet().getName());
		commercialNameList.setCsoName(csodpaService.findBylritCsoIdSeq(commercial.getCcsoId()).get().getName());
		commercialNameList.setAcsoName(csodpaService.findBylritCsoIdSeq(commercial.getAcsoId()).get().getName());
		if(commercial.getDpaId() != null)
			commercialNameList.setDpaName(csodpaService.findBylritCsoIdSeq(commercial.getDpaId()).get().getName());
		else
			commercialNameList.setDpaName(dpaName);

		technicalNameList.setCompanyName(technical.getCustomId().getUserDet().getPortalShippingCompany().getCompanyName());
		technicalNameList.setUserName(technical.getCustomId().getUserDet().getName());
		technicalNameList.setCsoName(csodpaService.findBylritCsoIdSeq(technical.getCcsoId()).get().getName());
		technicalNameList.setAcsoName(csodpaService.findBylritCsoIdSeq(technical.getAcsoId()).get().getName());		
		if(technical.getDpaId() != null)
			technicalNameList.setDpaName(csodpaService.findBylritCsoIdSeq(technical.getDpaId()).get().getName());
		else
			technicalNameList.setDpaName(dpaName);
	}

	public void setDocuments(List<PortalDocuments> docs, Integer vesselId)
	{
		logger.info("inside setting document for viewing");

		/*byte[] defaultDoc = null;*/
		List<String> doc_purpose = new ArrayList<String>();
		doc_purpose.add("Email From Owner");
		doc_purpose.add("Tata Lrit Form");
		doc_purpose.add("Flag Registration Certificate");
		doc_purpose.add("Conforence Test Certificate");
		doc_purpose.add("Additional Document");
		doc_purpose.add("Sold Vessel Document");
		doc_purpose.add("Scrap Vessel Document");
		doc_purpose.add("Active To Inactive Document");
		doc_purpose.add("Change SEID Document");
		doc_purpose.add("Inactive To Active Document");
		List<PortalDocuments> approveddocs = docsService.getDocsByreferenceId(vesselId.toString(), doc_purpose);
		docs.addAll(approveddocs);
		logger.info("size of doc list : "+ docs.size());
	}

	/*public void setDocument(PortalDocuments doc, String referenceId, String tableName, byte[] document, String purpose)
	{
		logger.info("inside setDocument() method to persist document");
		doc.setReferenceId(referenceId);
		doc.setTableName(tableName);
		doc.setDocument(document);
		doc.setDocPurpose(purpose);
	}*/

	//this method is used to get vessel category list
	@RequestMapping("/getcategorylist")
	@ResponseBody
	public Set<PortalVesselCategory> getCategoryList(@RequestParam String vesselType, Model model)
	{	
		logger.info("inside getCategoryList() method");
		logger.info("vesselType : " + vesselType);
		PortalVesselType portalvesseltype = typeService.getByType(vesselType);
		Set<PortalVesselCategory> categorySet = portalvesseltype.getGetVesselCategory();
		return categorySet;
	}

	//this method is used to get modelType from manufacturerId 
	@RequestMapping("/getmodeltype")
	@ResponseBody
	public List<String> getModelType(@RequestParam Integer manufacturerId)
	{
		logger.info("inside getModelType() method");
		logger.info("manufacturerId : " + manufacturerId);
		ArrayList<String> modelTypes = modelService.getModelTypeByManufacturer(manufacturerId);
		return modelTypes;	
	}

	//this method is used to get modelName list from modelType and manufacturerId
	@RequestMapping("/getmodellist")
	@ResponseBody
	public ArrayList<PortalModelDetail> getModelList(@RequestParam String modelType, @RequestParam Integer manufacturerId)
	{	
		logger.info("inside getModelList() method");
		logger.info("modelType : " + modelType);
		logger.info("manufacturerId : " + manufacturerId);
		ArrayList<PortalModelDetail> modellist = modelService.findmodelListbymodelTypeAndManufacturerIdAndStatus(modelType, manufacturerId);
		return modellist;
	}

	//this method is used to get companies list
	@RequestMapping("/getcompanydetails")
	@ResponseBody
	public String getShippingCompanyList()
	{
		logger.info("inside getShippingCompanyList() method");
		Iterable<PortalShippingCompany> comList = companyService.getAllCompanyList();
		Gson gson = new Gson();
		String json = gson.toJson(comList);
		return json;
	}

	//this method is used to get user list from company code
	@RequestMapping("/getuserdetails")
	@ResponseBody
	public String  getUserList(@RequestParam String companyCode)
	{
		logger.info("inside getUserList() method");
		logger.info("companyCode : " + companyCode);	
		List<PortalUser> listofuser = userService.getBycompanyCode(companyCode);
		Gson gson = new Gson();
		String json = gson.toJson(listofuser);
		return json;	
	}

	//this method is used to get cso and dpa list from loginId and type
	@RequestMapping("/getcsodpalist")
	@ResponseBody
	public String  getCSODPAList(@RequestParam String loginId, @RequestParam String type)
	{
		logger.info("inside getCSODPAList() method");
		logger.info("loginId : "+loginId);
		logger.info("type : "+type);
		List<PortalCSODPADetails> listofCSODPA = new ArrayList<PortalCSODPADetails>();

		if(type.equals("cso"))
			listofCSODPA = csodpaService.getCSOByportalUser_loginIdAndType(loginId, type);
		else
			listofCSODPA = csodpaService.getDPAByportalUser_loginIdAndType(loginId, type);

		Gson gson = new Gson();
		String json = gson.toJson(listofCSODPA);
		return json;	

	}

	//this method is used to get companyName from companyCode
	@RequestMapping("getnamefromcode")
	@ResponseBody
	public String getNameFromCode(@RequestParam String code)
	{
		logger.info("inside getNameFromCode() method");
		Optional<PortalShippingCompany> company = companyService.getShipingById(code);
		String companyName = company.get().getCompanyName();
		return companyName;
	}

	//this method is used to get acso list from loginId and csoId
	@RequestMapping("/getacsolist")
	@ResponseBody
	public String  getACSOList(@RequestParam String loginId, @RequestParam String csoId)
	{
		logger.info("inside getACSOList() method");
		logger.info("loginId : "+loginId);
		logger.info("csoId : "+ csoId);
		List<PortalCSODPADetails> listofCSODPA = csodpaService.getACSOByportalUser_loginIdAndLritCsoIdSeq(loginId, csoId);
		Gson gson = new Gson();
		String json = gson.toJson(listofCSODPA);
		return json;	
	}

	//this method is used to see the uploaded document from the link given
	@RequestMapping(value="/document",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<? extends Object> getDocumentFromLink(@RequestParam Integer vesselId, @RequestParam String type)
	{
		logger.info("inside getDocumentFromLink() method");
		logger.info("vesselId : " + vesselId);
		logger.info("type : "+ type);

		List<String> doc_purpose = new ArrayList<String>();
		doc_purpose.add(type);	
		List<PortalDocuments> doclist = docsService.getDocsByreferenceId(vesselId.toString(), doc_purpose);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Access-Control-Allow-Methods", "GET, POST");
		headers.add("Access-Control-Allow-Headers", "Content-Type");

		logger.info("doclist :" + doclist.size());

		if(doclist.size() != 0)
		{	
			if(doclist.get(0).getDocument() != null)
			{
				headers.add("Content-Disposition", "inline; filename=AdditionalDoc.pdf");
				return  new ResponseEntity<InputStreamResource>(new InputStreamResource(new ByteArrayInputStream(doclist.get(0).getDocument())),headers,HttpStatus.OK);
			}
		}	
		return ResponseEntity.ok(new Gson().toJson("No document uploaded!!")); 
	}

	//this method is used to see image from link given
	@RequestMapping(value="/imagefile",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<? extends Object> getimagefile(@RequestParam Integer vesselId, HttpServletRequest req) 
	{
		logger.info("inside getimagefile() method");
		logger.info("vesselId: "+vesselId);	
		PortalVesselDetail vessel = vesselService.getByVesselId(vesselId).get();
		if(vessel.getVesselImage() != null)
		{
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("image/jpg"));
			headers.add("Access-Control-Allow-Origin", "*");
			headers.add("Access-Control-Allow-Methods", "GET, POST");
			headers.add("Access-Control-Allow-Headers", "Content-Type");
			headers.add("Content-Disposition", "inline; filename=VesselImage.jpg");
			return  new ResponseEntity<InputStreamResource>(new InputStreamResource(new ByteArrayInputStream(vessel.getVesselImage())),headers,HttpStatus.OK);
		}		
		return ResponseEntity.ok(new Gson().toJson("No image uploaded!!")); 
	}

	public void setRegistrationDefaults(HttpServletRequest request, Model model)
	{
		logger.info("inside setRegistrationDefaults() GET method");
		UserSessionInterface userSession = this.getCurrentSession();   //get login_id, requestor_lrit_id and user_category from session
		String loginId = userSession.getLoginId();
		logger.info("loginId : "+loginId);
		String userCategory = userSession.getCategory();
		PortalUser portalUser = userService.getbyloginId(loginId).get();
		String userName = portalUser.getName();
		logger.info("userName : "+userName);
		request.setAttribute("userId", loginId);
		request.setAttribute("userName", userName);
		String ownerCompanyCode = portalUser.getPortalShippingCompany().getCompanyCode();
		String ownerCompanyName = portalUser.getPortalShippingCompany().getCompanyName();
		request.setAttribute("ownerCompanyCode", ownerCompanyCode);
		request.setAttribute("ownerCompanyName", ownerCompanyName);
		logger.info("ownerCompanyCode : "+ownerCompanyCode);
		logger.info("ownerCompanyName : "+ownerCompanyName);
		model.addAttribute("userCategory", userCategory);
	}

	/*public void persistVesselHstEntryForEditVessel(PortalPendingTaskMst newPendingTaskEntry, String vesselName, String callSign)
	{
		PortalVesselDetailHistory vesselHist = new PortalVesselDetailHistory();   //set Vessel Detail in transition table
		vesselHist.setPendingTaskId(newPendingTaskEntry.getPendingtaskId());
		vesselHist.setVesselName(vesselName);
		vesselHist.setCallSign(callSign);
		vesselHist.setUpdatedDate(utilService.getCurrentTimeStamp());
		PortalVesselDetailHistory newVesselDetHst = vesselHstService.addVesselDetailHst(vesselHist);
		logger.info(newVesselDetHst.toString());
		logger.info("New Entry Persisted in vessel detail history table");
	}*/

}