package in.gov.lrit.portal.controller.pendingtask;

import java.io.ByteArrayInputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.Gson;

import in.gov.lrit.portal.CommonUtil.ICommonUtil;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in.gov.lrit.portal.service.contractinggovernment.ICGManagementService;
import in.gov.lrit.portal.service.pendingtask.PendingTaskService;
import in.gov.lrit.portal.service.request.IRequestmanagement;
import in.gov.lrit.portal.service.vessel.VesselDetailService;
import in_.gov.lrit.session.ContractingGovernmentInterface;
import in_.gov.lrit.session.UserSessionInterface;

@Controller
@RequestMapping("/pendingtask")
public class PendingTaskController {

	@Autowired
	private PendingTaskService pendingTaskService;

	@Autowired
	private ICommonUtil commonUtil;

	@Autowired
	private VesselDetailService vesselDetailService;

	@Autowired
	private IRequestmanagement requestService;

	@Autowired
	private ICGManagementService cgManagementService;

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PendingTaskController.class);
	
	/*
	 * @RequestMapping("/getDiffPendingTaskCount")
	 * 
	 * @ResponseBody public Map<String, Integer> getDiffPendingTaskCount() {
	 * Map<String, Integer> map = pendingTaskService.getCategoryWiseAlertCount();
	 * return map; }
	 */

	@RequestMapping("/getRequestPendingTaskPage")
	public String getReqeustPendingTaskPage(Model model) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		
		UserSessionInterface user = (UserSessionInterface) session.getAttribute("USER");
		String loginId = (String)user.getLoginId();
		logger.info("Inside getReqeustPendingTaskPage");
		/**Old code
		 * List<ContractingGovernmentInterface> cgList=
		 * pendingTaskService.getCGByLoginId(loginId); if(cgList==null) {
		 * logger.info("No context id found for current user "+loginId); }
		 * logger.info("Users Context list" +cgList.size());
		 * for(ContractingGovernmentInterface cg : cgList) {
		 * 
		 * logger.info(" lrit id in pending task "+cg.getLritId()); }
		 * model.addAttribute("cgList", cgList);
		 */
		List<ContractingGovernmentInterface> contextList = new ArrayList<ContractingGovernmentInterface>();
		contextList = (List<ContractingGovernmentInterface> ) session.getAttribute("context");
		if(contextList!=null)
			model.addAttribute("cgList", contextList);
		return "/pendingtasks/pendingtask_request";

	}

	@RequestMapping("/getPendingTaskCount")
	@ResponseBody
	public int getPendingTaskCount() {
		int count=0;
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		List<String> requesterLoginId = new ArrayList<String>();
		List<String> category = new ArrayList<String>();
		category = (List<String>) session.getAttribute("pendingtask");
		if(category!=null)
		{	
			/**old code
			 * UserSessionInterface user = (UserSessionInterface)
			 * session.getAttribute("USER"); PortalLritIdMaster lritIdMaster =
			 * user.getRequestorsLritId(); if
			 * (!lritIdMaster.getCountryName().equals("India"))
			 * requesterLoginId.add(lritIdMaster.getLritId()); else requesterLoginId =
			 * cgManagementService.getAllLritId();
			 */
			List<ContractingGovernmentInterface> contextList = new ArrayList<ContractingGovernmentInterface>();
			contextList = (List<ContractingGovernmentInterface> ) session.getAttribute("context");
			if(contextList!=null)
			{	
				for(ContractingGovernmentInterface context :contextList)
				{
					requesterLoginId.add(context.getLritId());
				}
				count = pendingTaskService.getPendingTaskCount(requesterLoginId, category);
			}
		}
		return count;

	}

	@RequestMapping("/pendingtaskrequestdata")
	@ResponseBody
	public String getReqeustPendingTask(@RequestParam String countryId) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		//List<String> requesterLritId = new ArrayList<String>();
		List<String> category = new ArrayList<String>();
		List<PortalPendingTaskMst> list=new ArrayList<PortalPendingTaskMst>();
		category = (List<String>) session.getAttribute("pendingtask");
		/**
		 * UserSessionInterface user = (UserSessionInterface)
		 * session.getAttribute("USER"); PortalLritIdMaster lritIdMaster =
		 * user.getRequestorsLritId(); if
		 * (!lritIdMaster.getCountryName().equals("India"))
		 * requesterLritId.add(lritIdMaster.getLritId()); else requesterLritId =
		 * cgManagementService.getAllLritId();
		 */
		//requesterLritId.add(countryId);
		/*
		 * category.add("DDP_Request"); category.add("Flag_Request");
		 * category.add("Edit_Vessel_Details"); category.add("Sold_Vessel");
		 * category.add("Delete_DNID"); requesterLoginId.add("1065");
		 * requesterLoginId.add("1136"); requesterLoginId.add("1036");
		 */
		logger.info("inside get pending task");
		// get the lritid from session and compare if it in get all the data for all the
		// requester_lrit_id
		// if not then get the data for particular lritid(country)
		// pass the granted authorities activities to method
		if(category!=null)
		 list = pendingTaskService.getRequestPendingTask(countryId, category);
		else
			list=null;
		Gson gson = new Gson();
		String json = gson.toJson(list);
		logger.info("Request pending tasks" + json);
		return json;
	}

	@RequestMapping("/pendingTaskDecision")
	@ResponseBody
	public String decisionRequest(@RequestParam("pendingtaskId") Integer pendingtaskId,
			@RequestParam("decision") String decision, @RequestParam("comment") String comment,
			@RequestParam("category") String category, @RequestParam("referenceId") String referenceId, Model model) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface user = (UserSessionInterface) session.getAttribute("USER");
		String loginId = user.getLoginId();
		Timestamp date = commonUtil.getCurrentTimeStamp();
		String message = null;
		String status = null;
		logger.info("inside making decision controller");
		logger.info("Pending Task Id :" + pendingtaskId);
		logger.info("Request Accepted(Y) or Rejected(N) :" + decision);
		logger.info("Comment :" + comment);
		logger.info("Type of Pending task :" + category);
		logger.info("Reference Id :" + referenceId);

		if ("Y".equals(decision) && (comment == null || comment.trim().length() <= 0)) {
			message = "Please enter the comment for declining the request";
		} else if ("N".equals(decision) && (comment == null || comment.trim().length() <= 0)) {
			message = "Please enter the comment for the request";
		} else {
			if ("Y".equals(decision)) {
				logger.info("Inside Pending Task request accepted");
				if (category.equals("DDP_Request") || category.equals("Flag_Request") || category.equals("Port_Request")
						|| category.equals("SARSurpic_Request") || category.equals("CoastalSurpic_Request")
						|| category.equals("SAR_Requests") || category.equals("Coastal_Requests")
						|| category.equals("SO_Request")) {
					try {
						logger.info("Inside DC Requests for message id: "+referenceId );
						status = pendingTaskService.acceptRequest(referenceId, category);
						logger.info("Response from DC: "+status);
						if (status.equalsIgnoreCase("true")) {
							message = "Request has been accepted successfully";
						} else {
							message = "Error Occurred While accepting the request";
							status = "fail";
						}
						logger.info("Message Set for user: "+message);
						logger.info("Request status update in portal_requests for message id: "+referenceId
								+ "approver_loginId: "+loginId + "response_payload : "+status + "date: "+date
								+ "status: approve");
						int resp=pendingTaskService.updateRequestStatus(referenceId, loginId, "approve", date, status);
						if(resp==1)
							logger.info("status updated in pending task master successfully");
						else
							logger.info("Error occurred while updating the pending task status");
					} catch (SOAPException | JAXBException e) {
						e.printStackTrace();
						message = "Error Occurred While accepting the request";
					} catch (Exception e) {
						e.printStackTrace();
						message = "Error Occurred While accepting the request";
					}
				} else {
					logger.info("Pending task request accept other than DC request");
					Integer vesselId = Integer.valueOf(referenceId);
					switch (category) {
					case "Edit_Vessel_Details":
						message = pendingTaskService.acceptEditVessel(vesselId, pendingtaskId);
						break;
					case "Vessel_Sold":
						message = pendingTaskService.acceptSoldScrapVessel(vesselId, "SOLD");
						break;
					case "Vessel_Scrap":
						message = pendingTaskService.acceptSoldScrapVessel(vesselId, "SCRAP");
						break;
					case "Active_to_Inactive":
						message = pendingTaskService.acceptActivetoInactiveVessel(vesselId, "INACTIVE");
						break;
					case "Inactive_to_Active": 
						message=pendingTaskService.acceptInactiveteActiveVessel(vesselId, "ACTIVE");
						break;
					case "Delete_DNID":
						message = pendingTaskService.acceptDeleteDnid(vesselId, category);
						break;
					case "Delete_SEID":
						message = pendingTaskService.acceptDeleteSeid(vesselId, category);
						break;
					case "Forceful_Delete_SEID":
						message = pendingTaskService.acceptDeleteSeid(vesselId, category);
						break;
					case "ReDownload_DNID":
						message = pendingTaskService.acceptReDownloadDnid(vesselId, category);
						break;
					}
				}
				pendingTaskService.updatePendingTaskStatus("accept", loginId, date, pendingtaskId, comment);
			} else {
				status = "reject";
				if (category.equals("DDP_Request") || category.equals("Flag_Request") || category.equals("Port_Request")
						|| category.equals("SARSurpic_Request") || category.equals("CoastalSurpic_Request")
						|| category.equals("SAR_Requests") || category.equals("Coastal_Requests")
						|| category.equals("SO_Request"))
					pendingTaskService.updateRequestStatus(referenceId, loginId, status, date, "");
				logger.info("inside reject");
				int success = pendingTaskService.updatePendingTaskStatus(status, loginId, date, pendingtaskId, comment);
				logger.info("after update request" + success);
				if (success == 1) {
					logger.info("status updated succesfully in databases");
					message = "Request rejected succesfully";
				} else {
					logger.info("fail to update status in database");
					message = "Error while sending requests";
				}

			}

		}
		return message;
	}

	@RequestMapping("/showDocument")
	@ResponseBody
	public ResponseEntity<byte[]> showDocument(@RequestParam("pendingTaskId") Integer pendingTaskId) throws Exception{

		logger.info("Getting the pending task document for pending taskid : "+pendingTaskId);
		byte[] document= pendingTaskService.getpendingTaskDocument(pendingTaskId);
		if(document != null)
		{
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_PDF);
			String filename = "DocumentUploaded.pdf";
			
			//headers.setContentDispositionFormData(filename, filename);
			headers.setContentDispositionFormData("inline", filename);
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		    ResponseEntity<byte[]> response = new ResponseEntity<>(document, headers, HttpStatus.OK);
		    return response;
			
			//headers.add("Access-Control-Allow-Origin", "*");
			//headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
			//headers.add("Access-Control-Allow-Headers", "Content-Type");
			//headers.add("Content-Disposition", "inline; filename=" + "example.pdf");
			//return  new ResponseEntity<InputStreamResource>(new InputStreamResource(new ByteArrayInputStream(document)),headers,HttpStatus.OK);
		}
		throw new Exception();
	}
}
