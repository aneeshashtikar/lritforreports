package in.gov.lrit.portal.controller.vessel;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.portal.CommonUtil.ICommonUtil;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in.gov.lrit.portal.model.pendingtask.PortalShipEquipementHst;
import in.gov.lrit.portal.model.vessel.PortalShipEquipment;
import in.gov.lrit.portal.model.vessel.ShipEquipmentAndVesselInterface;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.service.request.IRequestmanagement;
import in.gov.lrit.portal.service.vessel.VesselDetailService;
import in_.gov.lrit.session.UserSessionInterface;

@Controller
@RequestMapping("/vessel")
public class VesselController {

	@Autowired
	private VesselDetailService vesselDetailService;

	@Autowired 
	private IRequestmanagement manageRequest;

	@Autowired
	private	ICommonUtil commonUtil;

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(VesselController.class);

	@PreAuthorize(value = "hasAuthority('shipborneaction')")
	@RequestMapping("/getVesselDetailPage")
	public String getVesselDetailPage()
	{
		return "/vessel/vesseldetaillist";
	}

	@RequestMapping("/getVesselDetailList")
	@ResponseBody
	public List<ShipEquipmentAndVesselInterface> getVesselDetailList()
	{
		List<String>status=new ArrayList();
		status.add("ACTIVE");
		status.add("INACTIVE");
		List<String>regstatus=new ArrayList();
		regstatus.add("SOLD");
		regstatus.add("SCRAP");
		regstatus.add("SEID_DELETED");
		regstatus.add("INACTIVE");
		logger.info("Getting the vessel details");
		List<ShipEquipmentAndVesselInterface> listvessel=vesselDetailService.getVesselDetailList(status,regstatus);
		return listvessel;
	}

	@RequestMapping("/gettimedelay")
	@ResponseBody
	public String gettimedelay(Integer vesselId)
	{

		logger.info("in get time delay.vesselid="+vesselId);
		String vesselTimedelay =vesselDetailService.gettimedelay(vesselId);
		logger.info("in get time delay.timedelay="+vesselTimedelay);
		return vesselTimedelay;
	}

	@RequestMapping("/updatetimedelay")
	@ResponseBody
	public void updatetimedelay(String timedelay,Integer vesselId)
	{

		logger.info("in update time delay.timedelay="+timedelay);
		BigInteger bigIntegertimedelay=new BigInteger(timedelay);
		vesselDetailService.updatetimedelay(bigIntegertimedelay, vesselId);
	}

	
	@RequestMapping(value="/deleteDnid", method = RequestMethod.POST)
	@ResponseBody
	public String deleteDnid(@RequestParam("vesselId")Integer vesselId ,  Model model) throws Exception
	{
		logger.info("Inside deleteDnid method."+vesselId);
		String message=null;

		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId(); //get lritid from session 
		String requestorLritid=lritIdmaster.getLritId();

		String userCategory=userSession.getCategory();  //get user category from session
		PortalShipEquipment shipborneequipment=vesselDetailService.shipBorneEquiomentDetail(vesselId);
		logger.info("Shipborne equipmnet="+shipborneequipment.getShipborneEquipmentId());

		Timestamp ts=commonUtil.getCurrentTimeStamp();

		List<String> regStatus = new ArrayList<String>();
		regStatus.add("DNID_DOWNLOADED");
		regStatus.add("SHIP_REGISTERED");
		regStatus.add("DNID_DEL_REQ");

		List<String> shipstatus=new ArrayList<String>();
		shipstatus.add("ACTIVE");

		BigInteger messagetype=BigInteger.valueOf(18);
		BigInteger requesttype=BigInteger.valueOf(56);
		try {
			String chckvesselstatus=vesselDetailService.checkVesselStatus(vesselId, regStatus, shipstatus);
			if (chckvesselstatus!=null)
			{
				logger.info("Saving ship equipment data in Ship_equipment_hst table "+ shipborneequipment.getShipborneEquipmentId());
				PortalShipEquipementHst portalShipEquipementHst= new PortalShipEquipementHst();
				portalShipEquipementHst.setDnidNo(shipborneequipment.getDnidNo());
				portalShipEquipementHst.setImoNo(shipborneequipment.getImoNo());
				portalShipEquipementHst.setMemberNo(shipborneequipment.getMemberNo());
				portalShipEquipementHst.setModelId(shipborneequipment.getModelDet().getModelId());
				portalShipEquipementHst.setOceanRegion(shipborneequipment.getOceanRegion());
				portalShipEquipementHst.setResponsePayload(shipborneequipment.getResponsePayload());
				portalShipEquipementHst.setRequestPayload(shipborneequipment.getRequestPayload());
				portalShipEquipementHst.setShipborneEquipmentId(shipborneequipment.getShipborneEquipmentId());
				portalShipEquipementHst.setStatus("Delete_DNID");
				portalShipEquipementHst.setUpdateDate(ts);
				portalShipEquipementHst.setVesselId(vesselId);
				vesselDetailService.saveSeidHistory(portalShipEquipementHst);
				logger.info("Saved Ship equipment details in shipequipment history");

				if(userCategory.equalsIgnoreCase("USER_CATEGORY_DGS"))// This is normal request sent directly by admin inc
				{

					boolean status=vesselDetailService.manageDNID( vesselId, requestorLritid, messagetype, requesttype); //send req to ASP

					if(status) {

						vesselDetailService.updateVesselRegStatus(vesselId,"DNID_DEL_REQ"); //update vessel status
						logger.info("DNID Delete sent Successfully");
						message="DNID Delete sent Successfully.";

					}else
					{	logger.info("Delete DNID failed to sent");
					message="Delete DNID failed to sent";
					}	

				}
				else
				{
					// this request is for pending task, need to make an entry in pending task master 

					String msg = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a DNID Delete Request For SEID="+shipborneequipment.getShipborneEquipmentId()+" and DNID NO= "+shipborneequipment.getDnidNo();

					PortalPendingTaskMst portalPendingTaskMst = new PortalPendingTaskMst();
					portalPendingTaskMst.setCategory("Delete_DNID");
					portalPendingTaskMst.setRequestDate(commonUtil.getCurrentTimeStamp());
					portalPendingTaskMst.setRequesterLoginId(loginid);
					portalPendingTaskMst.setRequestorLritid(requestorLritid);
					portalPendingTaskMst.setStatus("pending");
					portalPendingTaskMst.setMessage(msg);
					portalPendingTaskMst.setReferenceId(vesselId.toString());
					portalPendingTaskMst.setRequestDate(ts);
					manageRequest.savependingtask(portalPendingTaskMst);
					message=" Delete DNID request has been sent for Approval. ";
				}
			}
			else
			{
				logger.info("DNID not qualified for Delete .");

				message="DNID Cannot be deleted.";
			}
		}
		catch (Exception e) {
			logger.info("DNID not qualified for Delete .");
			e.printStackTrace();
			message="DNID Cannot be deleted.";
		}
		return message;
	}


	@RequestMapping("/shipBorneEquipmentInfo")
	public String shipBorneEquipmentDetail( @RequestParam("vesselId") Integer vesselId, Model model)
	{
		logger.info("Getting the shipborne Equipment Details from vesselid "+vesselId);
		PortalShipEquipment shipborneequipment=vesselDetailService.shipBorneEquiomentDetail(vesselId);
		logger.info("Ship borne equipment data" +shipborneequipment);
		model.addAttribute("shipbornequipmentdetail", shipborneequipment);
		return "/vessel/shipborne_equipment_detail";
	}

	@RequestMapping(value="/deleteSeid", method = RequestMethod.POST )
	@ResponseBody
	public String deleteSeid(@RequestParam("vesselId")Integer vesselId, Model model)
	{
		logger.info("DELETE SEID for vessel Id :" +vesselId);
		logger.info("Inside deleteSeid method"+vesselId);
		String message=null;
		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId(); //get lritid from session 
		String requestorLritid=lritIdmaster.getLritId();

		String userCategory=userSession.getCategory();  //get user category from session

		Timestamp ts=commonUtil.getCurrentTimeStamp();
		List<String> regStatus = new ArrayList<String>();
		regStatus.add("DNID_DELETED");
		regStatus.add("DNID_DW_FAILED");

		List<String> shipstatus=new ArrayList<String>();
		shipstatus.add("ACTIVE");

		try {
			String chckvesselstatus=vesselDetailService.checkVesselStatus(vesselId, regStatus, shipstatus);
			if (chckvesselstatus!=null)
			{

				/*try {
			vesselDetailService.checkVesselStatus(vesselId, regStatus, shipstatus);*/
				PortalShipEquipment shipborneequipment=vesselDetailService.shipBorneEquiomentDetail(vesselId);
				logger.info("Shipborne equipmnet="+shipborneequipment.getShipborneEquipmentId());
				logger.info("Saving ship equipment data in Ship_equipment_hst table "+ shipborneequipment.getShipborneEquipmentId());
				PortalShipEquipementHst portalShipEquipementHst= new PortalShipEquipementHst();
				portalShipEquipementHst.setDnidNo(shipborneequipment.getDnidNo());
				portalShipEquipementHst.setImoNo(shipborneequipment.getImoNo());
				portalShipEquipementHst.setMemberNo(shipborneequipment.getMemberNo());
				portalShipEquipementHst.setModelId(shipborneequipment.getModelDet().getModelId());
				portalShipEquipementHst.setOceanRegion(shipborneequipment.getOceanRegion());
				portalShipEquipementHst.setResponsePayload(shipborneequipment.getResponsePayload());
				portalShipEquipementHst.setRequestPayload(shipborneequipment.getRequestPayload());
				portalShipEquipementHst.setShipborneEquipmentId(shipborneequipment.getShipborneEquipmentId());
				portalShipEquipementHst.setStatus("Delete_SEID");
				portalShipEquipementHst.setUpdateDate(ts);
				portalShipEquipementHst.setVesselId(vesselId);
				vesselDetailService.saveSeidHistory(portalShipEquipementHst);
				logger.info("Saved Ship equipment details in shipequipment history");

				logger.info("old dnid="+shipborneequipment.getDnidNo()+"old memberid="+shipborneequipment.getMemberNo());

				if(userCategory.equalsIgnoreCase("USER_CATEGORY_DGS"))// This is normal request sent directly by admin inc
				{

					int deleteseid=vesselDetailService.DeleteSeid(vesselId);
					logger.info("deleteseid"+deleteseid);
					vesselDetailService.updateVesselRegStatus(vesselId,"SEID_DELETED"); //update vessel status
					logger.info("SEID Deleted Successfully");
					message="SEID Deleted Successfully";


				}
				else
				{
					// this request is for pending task, need to make an entry in pending task master 

					String msg = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a SEID Delete Request For SEID="+shipborneequipment.getShipborneEquipmentId()+" and DNID NO= "+shipborneequipment.getDnidNo();

					PortalPendingTaskMst portalPendingTaskMst = new PortalPendingTaskMst();
					portalPendingTaskMst.setCategory("Delete_SEID");
					portalPendingTaskMst.setRequestDate(commonUtil.getCurrentTimeStamp());
					portalPendingTaskMst.setRequesterLoginId(loginid);
					portalPendingTaskMst.setRequestorLritid(requestorLritid);
					portalPendingTaskMst.setStatus("pending");
					portalPendingTaskMst.setMessage(msg);
					portalPendingTaskMst.setReferenceId(vesselId.toString());
					portalPendingTaskMst.setRequestDate(ts);
					manageRequest.savependingtask(portalPendingTaskMst);

					message=" Delete SEID request has been sent for Approval. ";

				}

			}
			else
			{
				logger.info("SEID not qualified for delete .");

				message="SEID Cannot be deleted.";
			}
		}
		catch (Exception e) {
			logger.error("SEID not qualified for delete .");
			e.printStackTrace();
			message="SEID Cannot be deleted";
		}
		return message;


	}

	@RequestMapping(value="/forceDeleteSeid", method = RequestMethod.POST )
	@ResponseBody
	public String forceDeleteSeid(@RequestParam("vesselId")Integer vesselId, Model model)
	{
		logger.info("Forceful DELETE SEID for vessel Id :" +vesselId);


		logger.info("Inside forceDeleteSeid method"+vesselId);
		String message=null;

		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId(); //get lritid from session 
		String requestorLritid=lritIdmaster.getLritId();

		String userCategory=userSession.getCategory();  //get user category from session

		Timestamp ts=commonUtil.getCurrentTimeStamp();

		try {
			//vesselDetailService.checkVesselStatus(vesselId, regStatus, shipstatus);
			PortalShipEquipment shipborneequipment=vesselDetailService.shipBorneEquiomentDetail(vesselId);
			logger.info("Shipborne equipmnet="+shipborneequipment.getShipborneEquipmentId());
			logger.info("Saving ship equipment data in Ship_equipment_hst table "+ shipborneequipment.getShipborneEquipmentId());
			PortalShipEquipementHst portalShipEquipementHst= new PortalShipEquipementHst();
			portalShipEquipementHst.setDnidNo(shipborneequipment.getDnidNo());
			portalShipEquipementHst.setImoNo(shipborneequipment.getImoNo());
			portalShipEquipementHst.setMemberNo(shipborneequipment.getMemberNo());
			portalShipEquipementHst.setModelId(shipborneequipment.getModelDet().getModelId());
			portalShipEquipementHst.setOceanRegion(shipborneequipment.getOceanRegion());
			portalShipEquipementHst.setResponsePayload(shipborneequipment.getResponsePayload());
			portalShipEquipementHst.setRequestPayload(shipborneequipment.getRequestPayload());
			portalShipEquipementHst.setShipborneEquipmentId(shipborneequipment.getShipborneEquipmentId());
			portalShipEquipementHst.setStatus("Delete_SEID");
			portalShipEquipementHst.setUpdateDate(ts);
			portalShipEquipementHst.setVesselId(vesselId);
			vesselDetailService.saveSeidHistory(portalShipEquipementHst);
			logger.info("Saved Ship equipment details in shipequipment history");

			logger.info("old dnid="+shipborneequipment.getDnidNo()+"olde memberid="+shipborneequipment.getMemberNo());

			if(userCategory.equalsIgnoreCase("USER_CATEGORY_DGS"))// This is normal request sent directly by admin inc
			{
				vesselDetailService.DeleteSeid(vesselId);
				vesselDetailService.updateVesselRegStatus(vesselId,"SEID_DELETED"); //update vessel status
				logger.info(" SEID deleted Successfully");
				message="SEID Deleted Successfully";
			}
			else
			{
				// this request is for pending task, need to make an entry in pending task master 

				String msg = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a forceful SEID Delete Request For SEID="+shipborneequipment.getShipborneEquipmentId()+" and DNID NO= "+shipborneequipment.getDnidNo();

				PortalPendingTaskMst portalPendingTaskMst = new PortalPendingTaskMst();
				portalPendingTaskMst.setCategory("Delete_SEID");
				portalPendingTaskMst.setRequestDate(commonUtil.getCurrentTimeStamp());
				portalPendingTaskMst.setRequesterLoginId(loginid);
				portalPendingTaskMst.setRequestorLritid(requestorLritid);
				portalPendingTaskMst.setStatus("pending");
				portalPendingTaskMst.setMessage(msg);
				portalPendingTaskMst.setReferenceId(vesselId.toString());
				portalPendingTaskMst.setRequestDate(ts);
				manageRequest.savependingtask(portalPendingTaskMst);

				message=" Forceful Delete SEID request has been sent for Approval. ";

			}
		}
		catch (Exception e) {
			logger.error("SEID not qualified for delete .");
			e.printStackTrace();
			message="SEID Cannot be deleted";
		}
		return message;


	}

	@RequestMapping(value="/re-DownloadDnid", method = RequestMethod.POST )
	@ResponseBody
	public String reDownloadDnid(@RequestParam("vesselId")Integer vesselId, @RequestParam("dnidNo")Integer dnidNo, @RequestParam("memberNo")Integer memberNo, Model model)
	{
		logger.info("Inside reDownloadDnid method"+vesselId);
		String message=null;

		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();       // get data from session.
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId(); //get lritid from session 
		String requestorLritid=lritIdmaster.getLritId();

		String userCategory=userSession.getCategory();  //get user category from session

		Timestamp ts=commonUtil.getCurrentTimeStamp();
		List<String> regStatus = new ArrayList();
		regStatus.add("DNID_DOWNLOADED");
		regStatus.add("SHIP_REGISTERED");
		regStatus.add("DNID_DW_REQ");
		regStatus.add("DNID_DW_FAILED");
		regStatus.add("DNID_DELETED");
		regStatus.add("SEID_ADDED");

		List<String> shipstatus=new ArrayList();
		shipstatus.add("ACTIVE");
		shipstatus.add("INACTIVE");

		BigInteger messagetype=BigInteger.valueOf(18);
		BigInteger requesttype=BigInteger.valueOf(51);
		/*
		 * try { vesselDetailService.checkVesselStatus(vesselId, regStatus, shipstatus);
		 */
		try {
			String chckvesselstatus=vesselDetailService.checkVesselStatus(vesselId, regStatus, shipstatus);
			if (chckvesselstatus!=null)
			{
				if(userCategory.equalsIgnoreCase("USER_CATEGORY_DGS"))// This is normal request sent directly by admin inc
				{

					PortalShipEquipment shipborneequipment=vesselDetailService.shipBorneEquiomentDetail(vesselId);
					logger.info("Shipborne equipmnet="+shipborneequipment.getShipborneEquipmentId());
					logger.info("Saving ship equipment data in Ship_equipment_hst table "+ shipborneequipment.getShipborneEquipmentId());
					PortalShipEquipementHst portalShipEquipementHst= new PortalShipEquipementHst();
					portalShipEquipementHst.setDnidNo(shipborneequipment.getDnidNo());
					portalShipEquipementHst.setImoNo(shipborneequipment.getImoNo());
					portalShipEquipementHst.setMemberNo(shipborneequipment.getMemberNo());
					portalShipEquipementHst.setModelId(shipborneequipment.getModelDet().getModelId());
					portalShipEquipementHst.setOceanRegion(shipborneequipment.getOceanRegion());
					portalShipEquipementHst.setResponsePayload(shipborneequipment.getResponsePayload());
					portalShipEquipementHst.setRequestPayload(shipborneequipment.getRequestPayload());
					portalShipEquipementHst.setShipborneEquipmentId(shipborneequipment.getShipborneEquipmentId());
					portalShipEquipementHst.setStatus("ReDownload_DNID");
					portalShipEquipementHst.setUpdateDate(ts);
					portalShipEquipementHst.setVesselId(vesselId);
					vesselDetailService.saveSeidHistory(portalShipEquipementHst);
					logger.info("Saved Ship equipment details in shipequipment history");

					logger.info("old dnid="+shipborneequipment.getDnidNo()+"olde memberid="+shipborneequipment.getMemberNo());
					logger.info("new dnid="+dnidNo+"new memberid="+memberNo);
					vesselDetailService.updateDnid(vesselId, dnidNo, memberNo);

					boolean status=vesselDetailService.manageDNID(vesselId,requestorLritid, messagetype,requesttype);
					if(status) {
						vesselDetailService.updateVesselRegStatus(vesselId,"DNID_DW_REQ"); //update vessel status
						logger.info("DNID Redownload sent Successfully");
						message="DNID Redownload sent Successfully";

					}else
					{	logger.info("Redownload DNID failed to sent");
					message="Redownload DNID failed to sent";
					}	

				}
				else
				{


					PortalShipEquipment shipborneequipment=vesselDetailService.shipBorneEquiomentDetail(vesselId);
					logger.info("Shipborne equipmnet="+shipborneequipment.getShipborneEquipmentId());
					logger.info("Saving ship equipment data in Ship_equipment_hst table "+ shipborneequipment.getShipborneEquipmentId());
					PortalShipEquipementHst portalShipEquipementHst= new PortalShipEquipementHst();
					portalShipEquipementHst.setDnidNo(dnidNo);
					portalShipEquipementHst.setImoNo(shipborneequipment.getImoNo());
					portalShipEquipementHst.setMemberNo(memberNo);
					portalShipEquipementHst.setModelId(shipborneequipment.getModelDet().getModelId());
					portalShipEquipementHst.setOceanRegion(shipborneequipment.getOceanRegion());
					portalShipEquipementHst.setResponsePayload(shipborneequipment.getResponsePayload());
					portalShipEquipementHst.setRequestPayload(shipborneequipment.getRequestPayload());
					portalShipEquipementHst.setShipborneEquipmentId(shipborneequipment.getShipborneEquipmentId());
					portalShipEquipementHst.setStatus("ReDownload_DNID");
					portalShipEquipementHst.setUpdateDate(ts);
					portalShipEquipementHst.setVesselId(vesselId);
					vesselDetailService.saveSeidHistory(portalShipEquipementHst);
					logger.info("Saved Ship equipment details in shipequipment history");

					logger.info("old dnid="+shipborneequipment.getDnidNo()+"olde memberid="+shipborneequipment.getMemberNo());
					logger.info("new dnid="+dnidNo+"new memberid="+memberNo);
					vesselDetailService.updateDnid(vesselId, dnidNo, memberNo);


					// this request is for pending task, need to make an entry in pending task master 

					String msg = "USER "+loginid+" of LRIT ID " + requestorLritid + " initiated a DNID Redownload Request For SEID="+shipborneequipment.getShipborneEquipmentId()+" and DNID NO= "+dnidNo;

					PortalPendingTaskMst portalPendingTaskMst = new PortalPendingTaskMst();
					portalPendingTaskMst.setCategory("ReDownload_DNID");
					portalPendingTaskMst.setRequestDate(commonUtil.getCurrentTimeStamp());
					portalPendingTaskMst.setRequesterLoginId(loginid);
					portalPendingTaskMst.setRequestorLritid(requestorLritid);
					portalPendingTaskMst.setStatus("pending");
					portalPendingTaskMst.setMessage(msg);
					portalPendingTaskMst.setReferenceId(String.valueOf(portalShipEquipementHst.getId()));
					portalPendingTaskMst.setRequestDate(ts);
					manageRequest.savependingtask(portalPendingTaskMst);

					logger.info("history id in pending task="+portalPendingTaskMst.getReferenceId());
					message=" Redownload DNID request has been sent for Approval. ";

				}

			}
			else
			{
				logger.info("DNID not qualified for redownload .");

				message="DNID Cannot be redownload.";
			}
		}
		catch (Exception e) {
			logger.error("DNID not qualified for redownload .");
			e.printStackTrace();
			message="DNID Cannot be redownload";
		}
		return message;


	}


}
