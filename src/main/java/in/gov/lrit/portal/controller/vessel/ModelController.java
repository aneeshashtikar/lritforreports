package in.gov.lrit.portal.controller.vessel;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import in.gov.lrit.portal.model.vessel.PortalModelDetail;
import in.gov.lrit.portal.service.vessel.ModelService;
import in.gov.lrit.portal.service.vessel.VesselGroupService;

@Controller
@RequestMapping("model")
public class ModelController {

	@Autowired
	private ModelService modelService;
	
	/*@Autowired
	private ManufacturerDetailsService service;*/
	
	@Autowired
	private VesselGroupService service;

	private static final Logger logger = LoggerFactory.getLogger(ModelController.class);

	//this method is used to view model
	@PreAuthorize(value = "hasAnyAuthority('editmanufacturerandmodel','viewmanufacturerandmodel')")
	@RequestMapping(value="/viewmodel/{manufacturerId}", method=RequestMethod.GET)
	public ModelAndView viewModel(@PathVariable Integer manufacturerId, Model model)
	{
		boolean flag = false;
		logger.info("inside viewModel() method");
		logger.info("manufacturerId "+manufacturerId);
		
		/*Check if no model exist under this manufacturer
		flag=modelService.ifNoModelExistForManufacturerName(manufacturerId);
		if(flag == true)
		{
			 model.addAttribute("error", "Model does not exist for this manufacturer");
			 logger.info("Model does not exist for this manufacturer");
			 return new ModelAndView("listofmanufacturer", "portalManu", new PortalManufacturerDetails());
		}*/
		
		ModelAndView modelandview = new ModelAndView("/vessel/allmodel", "command", new PortalModelDetail());
		model.addAttribute("manufacturerId", manufacturerId);
		return modelandview;
	}

	//this method is used to get list of model
	@PreAuthorize(value = "hasAnyAuthority('editmanufacturerandmodel','viewmanufacturerandmodel')")
	@RequestMapping(value="/getmodels/{manuId}", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<PortalModelDetail> getModels(@PathVariable Integer manuId, Model model)
	{
		logger.info("inside getModels() method");
		logger.info("manuId "+manuId);
		Iterable<PortalModelDetail> models = modelService.getByManuIdAndStatus(manuId);
		return models;
	}

	//this method is used to persist model
	@PreAuthorize(value = "hasAuthority('editmanufacturerandmodel')")
	@RequestMapping(value="/persistmodel", method=RequestMethod.POST)
	public String persistPortalModel(PortalModelDetail newModel, Model model)
	{
		logger.info("inside persistmodel() method");
		logger.info(newModel.toString());

		boolean flag = false;
		String modelType = newModel.getModelType();
		String modelName = newModel.getModelName();
		Integer manuId = newModel.getManuDet().getManufacturerId();
		
		/*Check if model name contains alphabet only*/
		flag=service.isStringOnlyAlphabet(modelName);
		if(flag == false)
		{
			logger.info("Model Name should contains only alphabet");
			model.addAttribute("error", "Model Name should contains only alphabet");
		}
		else 
		{
			
			/*Check if model name already present for the selected manufacturer and model type*/
			flag = modelService.isModelExist(modelType, manuId, modelName);

			if(flag == true)
			{
				logger.info("Model name already exits");
				model.addAttribute("error", "Model name already exits");
			}
			else
			{
				/*Check if no model exist under this manufacturer
				flag=modelService.ifNoModelExistForManufacturerName(manuId);
				if(flag == true)
				{
					 Integer modelId = modelService.getModelIdByManufacturerName(manuId);
					 newModel.setModelId(modelId);
					 logger.info("modelId "+modelId);
					 PortalModelDetail mod = modelService.addModel(newModel);
					 model.addAttribute("success", "Model persists successfully");
					 logger.info("Model persists successfully in portal_model_details table");
				}*/
				
				/*Persisting new model entry in portal_model_details table*/ 
				newModel.setStatus("Active");
				PortalModelDetail mod = modelService.addModel(newModel);
				logger.info("Model persists successfully in portal_model_details table");
				model.addAttribute("success", "Model has been added successfully");
			}			
		}
		model.addAttribute("command", new PortalModelDetail());
		model.addAttribute("manufacturerId", manuId);
		return "/vessel/allmodel";
	}

	//this method is used to delete model
	@PreAuthorize(value = "hasAuthority('editmanufacturerandmodel')")
	@RequestMapping(value="/deletemodel/{modelId}", method=RequestMethod.GET)
	public String deleteModel(@PathVariable Integer modelId, Model model)
	{
		boolean flag = false;
		Optional<PortalModelDetail> mod = modelService.getById(modelId);
		PortalModelDetail modDet = mod.get();
		Integer manuId = modDet.getManuDet().getManufacturerId();
		logger.info("inside deleteModel() mehtod");
		logger.info("Model Id " + modelId);
		
		flag = modelService.updateModelById(modelId);
		
		if(flag == true)
		{ 
			logger.info("Device Model deleted successfully");
			model.addAttribute("success", "Device Model has been deleted successfully");
		}
		else
		{
			logger.info("Device Model cannot be deleted");
			model.addAttribute("error", "Device Model not deleted yet due to some error");
		}
		model.addAttribute("command", new PortalModelDetail());
		model.addAttribute("manufacturerId", manuId);
		return "/vessel/allmodel";
	}

}