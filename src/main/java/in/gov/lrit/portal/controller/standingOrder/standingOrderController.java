package in.gov.lrit.portal.controller.standingOrder;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import org.imo.gisis.xml.lrit.coastalstatestandingorderupdate._2014.CoastalStateStandingOrderUpdate;
import org.imo.gisis.xml.lrit.types._2008.CoastalStateStandingOrderType;
import org.imo.gisis.xml.lrit.types._2008.CoastalStateStandingOrderType.Polygon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.gis.ddp.model.DdpVersion;
import in.gov.lrit.gis.ddp.model.LritContractGovtMst;
import in.gov.lrit.gis.ddp.repository.ContractingGovtMasterRepository;
import in.gov.lrit.gis.ddp.repository.PolygonRepository;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in.gov.lrit.portal.model.request.PortalRequest;
import in.gov.lrit.portal.model.standingOrder.*;
import in.gov.lrit.portal.service.request.IDdpMessagingManagement;
import in.gov.lrit.portal.service.request.IRequestmanagement;
import in.gov.lrit.portal.service.request.SoapConnector;
import in.gov.lrit.portal.service.standingOrder.StandingOrderService;
import in_.gov.lrit.session.UserSessionInterface;
import in.gov.lrit.portal.CommonUtil.ICommonUtil;

@Controller
public class standingOrderController {

	@Autowired
	IRequestmanagement manageRequest;
	@Autowired
	IDdpMessagingManagement iddp;
	@Autowired
	ContractingGovtMasterRepository cgMasterRepo;
	@Autowired
	PolygonRepository polygonRepo;
	@Autowired
	ICommonUtil commonutil;
	@Autowired
	SoapConnector soapConnector;
	@Autowired
	StandingOrderService sor;	

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(standingOrderController.class);

	// Function to redirect to standing order main page
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping("/standingOrder")
	public String standingOrderPage() {
		return "standingorder/standingOrder";
	}

	// Function to redirect to Create new standing order
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = { "/createStandingOrder" }, method = RequestMethod.GET)
	public String getcreateStandingOrderPage() {
		return "standingorder/createStandingOrder";
	}

	// Function to redirect to edit standing order
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/editStandingOrder", method = RequestMethod.GET)
	public String geteditStandingOrderPage(Model model, @RequestParam(value = "soId") int SOId) {

		logger.info("inside edit SO for Id: " + SOId);

		// Geting selected Ships, Countries andd Geo area of SO Id
		List<SOGeographicalAreas> GeoDetail = sor.getGeoArea(SOId);

		// Saving Data into model to display on webpage
		model.addAttribute("soId", SOId);
		model.addAttribute("GeoDetail", GeoDetail);

		logger.info("Redirecting to Edit SO page for SO Id: " + SOId);

		return "standingorder/editStandingOrder";
	}

	// Function to get countries from DDP
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/getSOCountries", method = RequestMethod.GET)
	@ResponseBody
	public List<LritContractGovtMst> getSOCountries() {

		logger.info("Geting countrylist from DDP");

		// Fetching current DDP Version
		List<DdpVersion> ddpversion = iddp.getDdpVersionview();
		String regularversion = null;

		// Getting current regular version
		if (ddpversion.size() != 0)
			regularversion = ((DdpVersion) ddpversion.get(0)).getApplicableregularversion();

		logger.info("Current Regular version=" + regularversion);

		// feating country list from DDP
		List<LritContractGovtMst> countrylist = cgMasterRepo.findByCgName(regularversion);

		logger.info("Returning " + countrylist.size() + " Countries");

		return countrylist;
	}

	// Function to get Polygons from DDP
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/getSOPolygon", method = RequestMethod.GET)
	@ResponseBody
	public List<HashMap<String, String>> getSOPolygon(String areaType) {

		logger.info("Geting " + areaType + " Polygons from DDP");

		// Fetching current DDP Version
		List<DdpVersion> ddpversion = iddp.getDdpVersionview();
		String regularversion = null;

		// Getting current regular version
		if (ddpversion.size() != 0)
			regularversion = ((DdpVersion) ddpversion.get(0)).getApplicableregularversion();

		logger.info("Regular version=" + regularversion);

		// feating polygon list from DDP
		List<String> polygon = polygonRepo.getCountrypolygons("1065", regularversion, areaType);

		List<HashMap<String, String>> poly = new ArrayList<HashMap<String, String>>();
		for (int i = 0; i < polygon.size(); i++) {
			HashMap<String, String> data = new HashMap<String, String>();
			data.put("areaid", polygon.get(i));
			poly.add(data);
		}

		logger.info("Returning " + poly.size() + " " + areaType + " Polygons");

		return poly;
	}

	// Function to get excluded Geo area of particular soid from table
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/getGeoDetail", method = RequestMethod.GET)
	@ResponseBody
	public List<SOGeographicalAreas> getGeoDetail(int SOId) {
		logger.info("Geting Geo Area List of SO Id:" + SOId);
		List<SOGeographicalAreas> GeoDetail = sor.getGeoArea(SOId);
		logger.info("Returning Geo Area List of SO Id:" + SOId);
		return GeoDetail;
	}

	// Function to get excluded countries of particular soid from table
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/getExCountry", method = RequestMethod.GET)
	@ResponseBody
	public List<SOCountriesExclusion> getExCountry(int SOId, String areacode) {
		logger.info("Geting Excluded Country List of SO Id:" + SOId);
		List<SOCountriesExclusion> exCounDetail = sor.getExCoun(SOId, areacode);
		logger.info("Returning Excluded Country List of SO Id:" + SOId);
		return exCounDetail;
	}

	// Function to get excluded ships type of particular soid from table
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/getExShip", method = RequestMethod.GET)
	@ResponseBody
	public List<SOVesselExclusion> getExShip(int SOId, String areacode) {
		logger.info("Geting Excluded Ship List of SO Id:" + SOId);
		List<SOVesselExclusion> exShipDetail = sor.getExShip(SOId, areacode);
		logger.info("Returning Excluded Ship List of SO Id:" + SOId);
		return exShipDetail;
	}

	// Function to get SO's requests from table
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/getSODetail", method = RequestMethod.GET)
	@ResponseBody
	public List<standingOrderRequest> getSODetail(String status) {

		logger.info("Fetching SO List of status " + status);

		List<standingOrderRequest> SoDetail;
		// code to fetch generated SO requests from table as per status
		if (status.equalsIgnoreCase("All"))
			SoDetail = sor.getAllSO();
		else
			SoDetail = sor.getSObyStatus(status);

		logger.info("Returning SO List of status " + status);

		return SoDetail;
	}

	// Function to update status of soid
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	@ResponseBody
	public String updateStatus(@RequestParam(value = "SOId") int SOId, @RequestParam(value = "Status") String status)
			throws Exception {

		logger.info("Updating status of SO Id " + SOId + " to " + status);

		// for open request, all previous Soid is closed
		if (status.equalsIgnoreCase("Opened"))
			sor.updateOpenedStatus(status);

		sor.updateStatus(SOId, status);

		logger.info("SO Id " + SOId + " is updated to " + status);

		return "Successfully Updated";
	}

	// Function to save SO request
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/persistSORequest", method = RequestMethod.POST)
	@ResponseBody
	public String persistportalsorequest(@RequestParam(value = "combine[]") List<String> combineData,
			@RequestParam(value = "pSOId") String pSOId) throws Exception {

		logger.info("Persisting SO Request Function");

		int soId;
		// Creating SO and Portal Request objects
		standingOrderRequest SORequest = new standingOrderRequest();
		PortalRequest portalreq = new PortalRequest();

		try {
			// code to get User Session
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpSession session = attr.getRequest().getSession();
			UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");

			PortalLritIdMaster lritIdmaster = userSession.getRequestorsLritId();

			// Fetching current DDP Version
			List<DdpVersion> ddpversion = iddp.getDdpVersionview();
			String ddpversionfinal = commonutil.getDDPversion(ddpversion);

			logger.info("Regular version=" + ddpversionfinal);

			// Getting required data to save in DB
			String requestorLritid = lritIdmaster.getLritId();
			String messageid = commonutil.generateMessageID(requestorLritid);
			String loginid = userSession.getLoginId();
			Timestamp ts = commonutil.getCurrentTimeStamp();

			logger.info("Creating SO Request Object to persist");

			// Adding data into objects to save into DB
			if (!(requestorLritid.equalsIgnoreCase("1065")))
			{
				String message = "USER " + loginid + " of LRIT ID " + requestorLritid + " initiated a SO Request";
				portalreq.setStatus("pending");
				portalreq.setStatusDate(ts);
				PortalPendingTaskMst pendingtask = new PortalPendingTaskMst();
				pendingtask.setStatus("pending");
				pendingtask.setCategory("SORequest");
				pendingtask.setMessage(message);
				pendingtask.setReferenceId(messageid);
				pendingtask.setRequestDate(ts);
				pendingtask.setRequesterLoginId(loginid);
				pendingtask.setRequestorLritid(requestorLritid);
				manageRequest.savependingtask(pendingtask);
			}
			
			portalreq.setMessageId(messageid);
			portalreq.setCurrentddpversion(ddpversionfinal);
			portalreq.setRequestCategory("SORequest");
			portalreq.setRequestorsLoginId(loginid);
			portalreq.setRequestGenerationTime(ts);
			SORequest.setPortalRequest(portalreq);
			SORequest.setLritId(requestorLritid);
			//SORequest.setStatus("Created");
			SORequest.setParentsoid(pSOId);

			// Saving data into SO request and Portal request table
			SORequest = sor.saveSO(SORequest);

			// Getting SO Id created in above step
			soId = SORequest.getStandingorderId();

			logger.info("SO Request Created with Id: " + soId);
			
			String str="", dataStr = "";
			for (int ds = 0; ds < combineData.size();) {//(String str : combineData) {
				int idx;
				do{
					System.out.println(combineData.get(ds));
					dataStr = dataStr.concat(combineData.get(ds));
					idx = dataStr.indexOf("]");
					if(idx == -1)
						dataStr = dataStr.concat(",");
					ds++;
				}while(idx == -1);

				str = dataStr;
				dataStr ="";
				//str = combineData.get(ds);
				System.out.println(str);
				
				str = str.replace("[", "");
				str = str.replace("]", "");
				
				int areaindex = str.indexOf("area:");
				int shipindex = str.indexOf("ship:");
				int countryindex = str.indexOf("country:");

				String area = str.substring(areaindex, shipindex);
				area = area.replace("area: ", "");

				String ship = str.substring(shipindex, countryindex);
				ship = ship.replace("ship: ", "");

				String country = str.substring(countryindex);
				country = country.replace("country: ", "");

				System.out.println("Area: " + area + "\nShip: " + ship + "\nCountry: " + country);

				String[] areaList = new String[0], shipList = new String[0], countryList = new String[0];
				areaList = area.split(",");
				
				if(!(ship.equals("")))
					shipList = ship.split(",");
				
				if(!(country.equals("")))
					countryList = country.split(",");
				
				logger.info("Creating excluded area Object to persist for Id: " + soId);

				// ===================Area data==================
				// Persisting Geo area Data into DB
				// ==============================================
				// loop for multiple area selected
				for (String areacode : areaList) {
					SOGeographicalAreas SOGeoArea = new SOGeographicalAreas();

					// Adding data into objects to save into DB
					SOGeoArea.setSOId(soId);
					SOGeoArea.setAreacode(areacode);

					// Inserting Geographical Area data into table
					try {
						SOGeoArea = sor.saveGeoArea(SOGeoArea);//soGeoAreasRepo.save(SOGeoArea);
					} catch (Exception e) {
					}

					logger.info("Creating excluded ship Object to persist for Id: " + soId);

					// ==================Ship data===================
					// Persisting Vessel Data into DB
					// ==============================================
					// loop for multiple ship type
					for (String shiptype : shipList) {
						SOVesselExclusion SOVesselEx = new SOVesselExclusion();

						// Separate ship code and type
						String code = shiptype.substring(shiptype.length() - 4, shiptype.length());
						String type = shiptype.substring(0, shiptype.length() - 4);

						// Adding ship data into objects to save into DB
						SOVesselEx.setSOId(soId);
						SOVesselEx.setAreacode(areacode);
						SOVesselEx.setVesselCode(code);
						SOVesselEx.setVesselType(type);

						// Inserting ship data into table
						try {
							SOVesselEx = sor.saveExVessel(SOVesselEx);
						} catch (Exception e) {
						}
						
					}

					logger.info("Creating excluded countries Object to persist for Id: " + soId);

					// =================Country data=================
					// Persisting Country Data into DB
					// ==============================================
					// loop for multiple countries selected
					for (String lritId : countryList) {
						SOCountriesExclusion SOCountryEx = new SOCountriesExclusion();

						// Adding data into objects to save into DB
						SOCountryEx.setSOId(soId);
						SOCountryEx.setAreacode(areacode);
						SOCountryEx.setExcludedCountries(lritId);

						// Inserting ship data into table
						try {
							SOCountryEx = sor.saveExCountry(SOCountryEx);
						} catch (Exception e) {
						}
						
					}
				}
			}
			System.out.println(pSOId);
			if (!pSOId.equals("")) {
				int id = Integer.parseInt(pSOId);
				List<String> GeoDetail = sor.getFlagedArea(id);
				List<String> newGeoDetail = sor.getFlagedArea(soId);
				
				for (int i = 0; i < GeoDetail.size(); i++) {
					boolean runflag = true;
					for (int j = 0; j < newGeoDetail.size(); j++) {
						//System.out.println(GeoDetail.get(i) + "   " + newGeoDetail.get(i));
						if(GeoDetail.get(i).equals(newGeoDetail.get(j))){
							runflag = false;
						}
					}
					if(runflag){
						SOGeographicalAreas SOGeoArea = new SOGeographicalAreas();

						// Adding data into objects to save into DB
						SOGeoArea.setSOId(soId);
						SOGeoArea.setAreacode(GeoDetail.get(i));
						// Inserting Geographical Area data into table
						try {
							SOGeoArea = sor.saveGeoArea(SOGeoArea);
						} catch (Exception e) {
						}

						logger.info("Creating excluded ship Object to persist for Id: " + soId);
						
						List<SOVesselExclusion> ships = sor.getExShip(id, GeoDetail.get(i));
						for (SOVesselExclusion ship : ships) {
							SOVesselExclusion SOVesselEx = new SOVesselExclusion();

							// Adding ship data into objects to save into DB
							SOVesselEx.setSOId(soId);
							SOVesselEx.setAreacode(GeoDetail.get(i));
							SOVesselEx.setVesselCode(ship.getVesselCode());
							SOVesselEx.setVesselType(ship.getVesselType());

							// Inserting ship data into table
							try {
								SOVesselEx = sor.saveExVessel(SOVesselEx);
							} catch (Exception e) {
							}
						}

						logger.info("Creating excluded countries Object to persist for Id: " + soId);

						List<String> countries = sor.getFlagedCoun(id, GeoDetail.get(i));
						for (String lritId : countries) {
							SOCountriesExclusion SOCountryEx = new SOCountriesExclusion();

							// Adding data into objects to save into DB
							SOCountryEx.setSOId(soId);
							SOCountryEx.setAreacode(GeoDetail.get(i));
							SOCountryEx.setExcludedCountries(lritId);

							// Inserting ship data into table
							try {
								SOCountryEx = sor.saveExCountry(SOCountryEx);
							} catch (Exception e) {
							}
							
						}
					}
				}
			}
			
			
		} catch (Exception e) {
			soId = SORequest.getStandingorderId();
			String msgid = SORequest.getPortalRequest().getMessageId();
			sor.deleteGeoArea(soId);
			sor.deleteExShip(soId);
			sor.deleteExCountries(soId);
			sor.deleteStandingOrder(soId);
			sor.deletePortalRequest(msgid);
			return "<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Failed to Create Standing Order</div>";
		}

		SORequest.setStatus("Created");
		SORequest = sor.saveSO(SORequest);
		return "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Standing Order Created with Id "
				+ soId + "</div>";
	}

	// Function to call DC and send SO Request
	@PreAuthorize(value = "hasAuthority('standingOrder')")
	@RequestMapping(value = "/callDC", method = RequestMethod.POST)
	@ResponseBody
	public String callDC(@RequestParam(value = "SOId") int SOId) throws Exception {

		// code to get User Session
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
		String loginid = userSession.getLoginId();
		PortalLritIdMaster lritIdmaster = userSession.getRequestorsLritId();
		
		// Creating Portal Request objects for close SO Request
		PortalRequest portalreq = new PortalRequest();

		Timestamp ts = commonutil.getCurrentTimeStamp();
		XMLGregorianCalendar xmlDate = commonutil.getgregorianTimeStamp(ts);

		String test = commonutil.getParavalues("Test");

		String schema_version = this.commonutil.getParavalues("Schema_Version");
		Double sv = new Double(schema_version);
		BigDecimal schemaversion = BigDecimal.valueOf(sv.doubleValue());

		String requestorLritid = lritIdmaster.getLritId();
		String messageid = commonutil.generateMessageID(requestorLritid);

		// Fetching SO Details to Open
		standingOrderRequest SoDetail = sor.getSO(SOId);

		// Creating Sanding order Packet to open
		CoastalStateStandingOrderType standingorder = new CoastalStateStandingOrderType();
		List<CoastalStateStandingOrderType.Polygon> polygon = standingorder.getPolygon();

		if (!(SoDetail.getStatus().equalsIgnoreCase("Opened"))) {

			List<String> GeoDetail = sor.getFlagedArea(SOId);
			for (int i = 0; i < GeoDetail.size(); i++) {
				Polygon poly = new Polygon();
				poly.setAreaID(GeoDetail.get(i));

				List<String> countries = sor.getFlagedCoun(SOId, GeoDetail.get(i));
				List<String> filterFlag = poly.getFilterFlag();
				if (filterFlag.size() == 0)
					filterFlag.addAll(countries);

				List<String> ships = sor.getFilterShips(SOId, GeoDetail.get(i));
				List<String> filtership = poly.getFilterShipType();
				if (filtership.size() == 0)
					filtership.addAll(ships);

				polygon.add(poly);
			}
			messageid = SoDetail.getPortalRequest().getMessageId();

		} else {
			// code for Closing Standing Order, packet created with blank values
			Polygon poly = new Polygon();
			poly.setAreaID(null);

			List<String> filterFlag = poly.getFilterFlag();
			if (filterFlag.size() == 0)
				filterFlag.add(null);

			List<String> filtership = poly.getFilterShipType();
			if (filtership.size() == 0)
				filtership.add(null);

			polygon.add(poly);

			portalreq.setMessageId(messageid);
			portalreq.setCurrentddpversion(SoDetail.getPortalRequest().getCurrentddpversion());
			portalreq.setRequestCategory("SORequest");
			portalreq.setRequestorsLoginId(loginid);
			portalreq.setRequestGenerationTime(ts);
			portalreq = sor.savePortalRequest(portalreq);
		}

		standingorder.setContractingGovernmentID(lritIdmaster.getLritId());

		CoastalStateStandingOrderUpdate soRequest = new CoastalStateStandingOrderUpdate();
		soRequest.setMessageType(BigInteger.valueOf(17));
		soRequest.setMessageId(messageid);
		soRequest.setStandingOrder(standingorder);
		soRequest.setDataUserRequestor(lritIdmaster.getLritId());
		soRequest.setTimeStamp(xmlDate);
		soRequest.setDDPVersionNum(SoDetail.getPortalRequest().getCurrentddpversion());
		soRequest.setTest(BigInteger.valueOf(Long.parseLong(test)));
		soRequest.setSchemaVersion(schemaversion);

		String response = soapConnector.callWebService(soRequest);

		JAXBContext jaxbContext = JAXBContext.newInstance(CoastalStateStandingOrderUpdate.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(soRequest, sw);
		
		logger.info("Sanding Order Request payload:" + sw.toString());

		if (!(SoDetail.getStatus().equalsIgnoreCase("Opened"))) {
			SoDetail.getPortalRequest().setRequestpayload(sw.toString());
			SoDetail.getPortalRequest().setResponsepayload(response);
			SoDetail = sor.saveSO(SoDetail);
		} else {
			portalreq.setRequestpayload(sw.toString());
			portalreq.setResponsepayload(response);
			portalreq = sor.savePortalRequest(portalreq);
		}

		//String response = "true";//for testing
		return response;

	}

}
