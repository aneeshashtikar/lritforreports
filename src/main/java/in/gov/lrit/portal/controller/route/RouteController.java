package in.gov.lrit.portal.controller.route;

import in.gov.lrit.gis.ddp.model.DdpVersion;
import in.gov.lrit.gis.ddp.model.LritContractGovtMst;
import in.gov.lrit.portal.CommonUtil.ICommonUtil;
import in.gov.lrit.portal.model.route.Route;
import in.gov.lrit.portal.service.country.ICountryService;
import in.gov.lrit.portal.service.request.IDdpMessagingManagement;
import in.gov.lrit.portal.service.route.IRouteService;
import in.gov.lrit.portal.service.vessel.VesselService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/route")
public class RouteController {
	@Autowired
	VesselService vesselService;
	@Autowired
	IRouteService routeService;
	@Autowired
	ICountryService countryService;
	@Autowired
	ICommonUtil commonutil;
	@Autowired
	IDdpMessagingManagement ddpobj;
	
	
	private static final Logger logger = LoggerFactory.getLogger(RouteController.class);

	
	@PreAuthorize(value = "hasAuthority('createroute')")
	@RequestMapping(value = { "/createroute" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public String newRouteForm(Model model) {
		
		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String regularversion = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			regularversion = ((DdpVersion)ddpversion.get(j)).getApplicableregularversion();
		}
		List<LritContractGovtMst> countryList = this.countryService.getCountriesList(regularversion);
		model.addAttribute("countrylist", countryList);
		model.addAttribute("route", new Route());
		return "/route/createroute";
	}

	@PreAuthorize(value = "hasAuthority('createroute')")
	@RequestMapping(value = { "/saveroute" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST })
	public String saveRoute(Model model, @ModelAttribute("route") Route route, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String regularversion = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			regularversion = ((DdpVersion)ddpversion.get(j)).getApplicableregularversion();
		}
		List<LritContractGovtMst> countryList = this.countryService.getCountriesList(regularversion);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			logger.info("string date:" + route.getStartDateString());
			Date startDateFormat = dateFormat.parse(route.getStartDateString());
			Date endDateFormat = dateFormat.parse(route.getEndDateString());
			route.setStartDate(startDateFormat);
			route.setEndDate(endDateFormat);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//logger.info("startDAte:" + route.getStartDate());
		logger.info("data form pojo=" + route.toString());
		if (bindingResult.hasErrors()) {
			model.addAttribute("errorMsg", "Please check!! Something is wrong");
			return "route/createroute";
		}
		this.routeService.SaveRoute(route);
		model.addAttribute("successMsg", "Route created Successfully");
		model.addAttribute("route", new Route());
		model.addAttribute("countrylist", countryList);

		return "route/createroute";
	}

	
	@PreAuthorize(value = "hasAnyAuthority('viewroute','editroute')")
	@RequestMapping(value = { "/viewroutes" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public ModelAndView viewVesselList(ModelAndView model) {
		List<Route> routeList = this.routeService.getAllRoutes();
		for (Route route : routeList) {
			logger.info("list" + route.getImoNo() + "countrylist" + route.getCountryList());
		}
		model = new ModelAndView("route/viewroutes");
		model.addObject("routeList", routeList);

		return model;
	}

	@PreAuthorize(value = "hasAuthority('editroute')")
	@RequestMapping(value = { "/{routeId}/update" }, method = {org.springframework.web.bind.annotation.RequestMethod.GET })
	public String updateView(ModelMap model, @PathVariable("routeId") String routeId) {
		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String regularversion = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			regularversion = ((DdpVersion)ddpversion.get(j)).getApplicableregularversion();
		}
		List<LritContractGovtMst> countryList = this.countryService.getCountriesList(regularversion);

		Route route = this.routeService.findById(routeId);
		logger.info("data loaded of id=" + route.toString());
		String assigncountry;
		List<String> assignedCountries = this.routeService.getCountriesList(route);
		for (Iterator localIterator1 = assignedCountries.iterator(); localIterator1.hasNext();) {
			assigncountry = (String) localIterator1.next();
			for (LritContractGovtMst countryDetails : countryList) {
				if (assigncountry.equals(countryDetails.getCgName()) == true) {
					countryDetails.setFlag(true);
				}
			}
		}
		route.setStartDateString(route.getStartDate().toString());
		route.setEndDateString(route.getEndDate().toString());
		model.addAttribute("startDate", route.getStartDateString());
		model.addAttribute("endDate", route.getEndDateString());
		model.addAttribute("updateForm", route);
		model.addAttribute("routeId", routeId);
		model.addAttribute("countrylist", countryList);

		return "route/editroute";
	}

	@PreAuthorize(value = "hasAuthority('editroute')")
	@RequestMapping(value = { "/{routeId}/updateroute" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST })
	public String updateRoute(Model model, @ModelAttribute("updateForm") Route route, BindingResult bindingResult,
			RedirectAttributes redirectAttributes, @PathVariable("routeId") String routeId) {
		logger.info("data while updateing=" + route.toString());
		if (bindingResult.hasErrors()) {
			model.addAttribute("errorMsg", "Please check!! Something is wrong");
			return "route/update";
		}
		this.routeService.updateRoute(route, routeId);
		model.addAttribute("successMsg", "Route updated Successfully");

		return "redirect:/route/viewroutes";
	}
	
	@RequestMapping(value = { "/getddpversion" }, method=RequestMethod.GET)
	@ResponseBody
	public String getDDPVesion(Model model)
	{
		logger.info("calling controller method");
		List<DdpVersion> ddpversion = this.ddpobj.getDdpVersionview();
		String regularversion = null;
		for (int j = 0; j < ddpversion.size(); j++) {
			regularversion = ((DdpVersion)ddpversion.get(j)).getDdpversion();
		}
		logger.info("ddp version="+regularversion);
		return regularversion;
	}
}
