package in.gov.lrit.portal.controller.vessel;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import in.gov.lrit.portal.model.vessel.PortalModelDetail;
import in.gov.lrit.portal.model.vessel.PortalVesselGroup;
import in.gov.lrit.portal.service.vessel.VesselGroupService;
import in.gov.lrit.portal.service.vessel.VesselService;

@Controller
@RequestMapping("/group")
public class VesselGroupController {

	@Autowired
	private VesselGroupService vesselGroupService;

	private static final Logger logger = LoggerFactory.getLogger(VesselGroupController.class);

	//this method is used to create vessel group
	@PreAuthorize(value = "hasAnyAuthority('viewvesselgroup','editvesselgroup')")
	@RequestMapping(value="/createvesselgroup", method=RequestMethod.GET)
	public String createVesselGroup(Model model)
	{
		logger.info("inside createvesselgroup() method");
		model.addAttribute("portalVesselGroup", new PortalVesselGroup());
		return "/vessel/createvesselgroup";
	}

	//this method is used to persist vessel group
	@PreAuthorize(value = "hasAuthority('editvesselgroup')")
	@RequestMapping(value="persistvesselgroup", method=RequestMethod.POST)
	public String persistPortalVesselGroup(@ModelAttribute("portalVesselGroup") PortalVesselGroup portalVesselGroup, Model model)
	{
		logger.info("inside persistPortalVesselGroup() method");
		logger.info(portalVesselGroup.toString());

		boolean flag = false;
		String groupName = portalVesselGroup.getGroupName();
		String shortName = portalVesselGroup.getShortName();
		String desc = portalVesselGroup.getDescription();

		/*Check if group name contains alphabet only*/
		flag=vesselGroupService.isStringOnlyAlphabet(groupName);
		if(flag == false)
		{
			model.addAttribute("error", "Group Name should contains only alphabet");
			logger.error("Group Name should contains only alphabet");
		}
		else
		{
			/*Check if group name already present*/
			flag = vesselGroupService.isGroupExist(groupName);
			if(flag == true)
			{
				model.addAttribute("error", "Group with name already exist");
				logger.error("Group with name already exist");
			}
			else
			{
				/*Check if short name contains alphabet only*/
				flag=vesselGroupService.isStringOnlyAlphabet(shortName);
				if(flag == false)
				{
					model.addAttribute("error", "Short Name should contains only alphabet");
					logger.error("Short Name should contains only alphabet");
				}
				else
				{
					/*Check if short name already present*/
					flag = vesselGroupService.isShortNameExist(groupName, shortName);
					if(flag == true)
					{
						model.addAttribute("error", "Short Name for group already exist");
						logger.error("Short Name for group already exist");

					}
					else
					{
						/*Check if description contains alphabet only*/
						flag=vesselGroupService.isStringOnlyAlphabet(desc);
						if(flag == false)
						{
							model.addAttribute("error", "Description should contains only alphabet");
							logger.error("Description should contains only alphabet");
						}
						else
						{
							/*Persisting new group entry in portal_group_details table*/
							PortalVesselGroup vesgrp = vesselGroupService.addVesselGroup(portalVesselGroup);
							model.addAttribute("successMsg", "Vessel Group has been added successfully");
							logger.info("Group persisted successfully in portal_group_details table");
						}
					}
				}
			}	
		}
		model.addAttribute("portalVesselGroup", new PortalVesselGroup());
		return "acknowledgement";
	}

	//this method is used to show list of all vessel group
	@PreAuthorize(value = "hasAnyAuthority('viewvesselgroup','editvesselgroup')")
	@RequestMapping(value="/viewvesselgroup", method=RequestMethod.GET)
	public String viewPortalVesselGroup(Model model)
	{
		logger.info("inside viewPortalVesselGroup() method");
		return "/vessel/viewallgroup";
	}

	//this method is used to get list of all vessel group
	@PreAuthorize(value = "hasAnyAuthority('viewvesselgroup','editvesselgroup')")
	@RequestMapping(value="/getgroups", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<PortalVesselGroup> getAllGroup()
	{	
		logger.info("inside getAllGroup() method");
		Iterable<PortalVesselGroup> grouplist = vesselGroupService.findAllGroup();
		return grouplist;
	}

	//this method is used to show edit vessel group
	@PreAuthorize(value = "hasAuthority('editvesselgroup')")
	@RequestMapping(value="/editgroup/{groupId}", method=RequestMethod.GET)
	public String editVesselGroup(@PathVariable("groupId") Long groupId, Model model)
	{
		logger.info("inside editVesselGroup() method");
		logger.info("groupId "+groupId);
		Optional<PortalVesselGroup> vesselGroup = vesselGroupService.findById(groupId);
		PortalVesselGroup group = vesselGroup.get();
		model.addAttribute("portalGroup", group);
		return "/vessel/editvesselgroup";
	}

	//this method is used to udate vessel group
	@PreAuthorize(value = "hasAuthority('editvesselgroup')")
	@RequestMapping(value="/updategroup", method=RequestMethod.POST)
	public String updateVesselGroup(@ModelAttribute PortalVesselGroup group, Model model)
	{
		boolean result = false;
		logger.info("inside updateVesselGroup() method");
		//logger.info(group.toString());
		String groupName = group.getGroupName();
		PortalVesselGroup grp = vesselGroupService.findBygroupName(groupName);
		Long groupId = grp.getGroupId();
		logger.info("groupId : "+groupId);
		vesselGroupService.updateGroupDetail(group.getShortName(), group.getDescription(), groupId);
		model.addAttribute("successMsg", "Group has been updated successfully");
		logger.info("Group updated successfully in portal_group_details table");
		model.addAttribute("portalVesselGroup", new PortalVesselGroup());
		return "acknowledgement";
	}

}