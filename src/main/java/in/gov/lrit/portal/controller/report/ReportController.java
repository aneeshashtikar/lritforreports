package in.gov.lrit.portal.controller.report;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import in.gov.lrit.ddp.dao.LritContractingGovtMstRepository;
import in.gov.lrit.ddp.model.LritContractGovtList;
import in.gov.lrit.gis.ddp.repository.ContractingGovtMasterRepository;
import in.gov.lrit.portal.gis.lritdb.model.CustomPolygon;
import in.gov.lrit.portal.gis.lritdb.model.CustomPolygonList;
import in.gov.lrit.portal.gis.lritdb.service.GeographicAreaFileStorageService;
import in.gov.lrit.portal.model.report.ASPShipPosition;
import in.gov.lrit.portal.model.report.AspLogInterface;
import in.gov.lrit.portal.model.report.CspLog;
import in.gov.lrit.portal.model.report.DdpLog;
import in.gov.lrit.portal.model.report.SystemStatus;
import in.gov.lrit.portal.model.report.LritLatestShipPostion;
import in.gov.lrit.portal.model.report.VwGeographicalAreaUpdate;
import in.gov.lrit.portal.model.report.VwSarSurpicRequest;
import in.gov.lrit.portal.model.report.VwShipPositionRequest;
import in.gov.lrit.portal.model.report.VwStandingOrder;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.service.report.ReportService;
import in_.gov.lrit.portal.dto.VwShipStatusLatestPositionDto;
import in_.gov.lrit.session.UserSessionInterface;

@Controller
@RequestMapping("/report")
public class ReportController {

	@Autowired
	private ReportService reportService;
	
	@Autowired
	private LritContractingGovtMstRepository cgRepository;
	
	@Autowired
	private GeographicAreaFileStorageService geographicalService;

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ReportController.class);

	// For ASP ship Position report

	@RequestMapping(value = "/getASPShipPositionReportPage", method = RequestMethod.GET)
	public String getASPShipPositionReportPage() {
		return "reports/asp_ship_position_report";
	}

	@RequestMapping(value = "/getAspShipPositionReport", method = RequestMethod.POST)
	@ResponseBody
	public Collection<ASPShipPosition> getAspShipPositionReport(Date startDate, Date endDate,
			@SessionAttribute("USER") UserSessionInterface user) {
		logger.info("Inside getAspShipPositionReport controller. startDate " + startDate + " endDate " + endDate);
		String lritId = user.getRequestorsLritId().getLritId();
		Collection<ASPShipPosition> aspShipPositionReportList = new ArrayList<ASPShipPosition>();
		if (lritId.equals("1065"))
			aspShipPositionReportList = reportService.getVessselDetails(startDate, endDate);
		// else
		for (ASPShipPosition asp : aspShipPositionReportList) {
			logger.info("asp ship position report " + asp.toString());
		}

		return aspShipPositionReportList;
	}

	@RequestMapping(value = "/getAspShipPositions", method = RequestMethod.POST)
	@ResponseBody
	public Collection<ASPShipPosition> getAspShipPositions(String imoNo, Date startDate, Date endDate) {
		/*
		 * logger.info("Inside getAspShipPositions controller. startDate " + startDate +
		 * " endDate " + endDate + " imoNo " + imoNo);
		 */
		Collection<ASPShipPosition> aspShipPositionsList = new ArrayList<ASPShipPosition>();
		aspShipPositionsList = reportService.getShipPosition(imoNo, startDate, endDate);
		logger.info("Position Responses:");
		for (ASPShipPosition aspShipPosition : aspShipPositionsList) {
			logger.info("asp ship Position " + aspShipPosition.toString());
		}

		return aspShipPositionsList;

	}

	// For CSP Log Report

	@RequestMapping(value = "/getCspLogReportPage", method = RequestMethod.GET)
	public String getCspLogReportPage() {
		return "reports/csp_log_report";
	}

	@RequestMapping(value = "/getCspLogReport", method = RequestMethod.POST)
	@ResponseBody
	public Collection<CspLog> getCspLogReport(Date startDate, Date endDate,
			@SessionAttribute("USER") UserSessionInterface user) {

		String lritId = user.getRequestorsLritId().getLritId();
		logger.info("Inside csplog controller. startDate " + startDate + " endDate " + endDate + " imoNo " + lritId);
		Collection<CspLog> cspLogList = new ArrayList<CspLog>();

		if (lritId.equals("1065"))
			cspLogList = reportService.getCspLogOfFlagVessel(startDate, endDate);
		else
			cspLogList = reportService.getCspLogofNonflagVessel(lritId, startDate, endDate);

		for (CspLog cspLog : cspLogList) {
			logger.info("CSP Log: " + cspLog);
		}
		return cspLogList;
	}

	// For sarSurpic Report
	@RequestMapping(value = "/getSarSurpicReportPage", method = RequestMethod.GET)
	public String getSarSurpicReportPage() {
		return "reports/sarsurpic_request_report";
	}

	@RequestMapping(value = "/getSarSurpicRequestReport", method = RequestMethod.POST)
	@ResponseBody
	public Collection<VwSarSurpicRequest> getSarSurpicRequestReport(String sarAuthority, Date startDate, Date endDate,
			String sarRequestType) {
		logger.info("Inside getSarSurpicRequestReport controller method.  SAR Authority " + sarAuthority
				+ " Start Date " + startDate + " End Date" + endDate);
		Collection<VwSarSurpicRequest> vwSarSurpicRequestList = new ArrayList<VwSarSurpicRequest>();
		vwSarSurpicRequestList = reportService.sarSurpicRequests(startDate, endDate, sarAuthority, sarRequestType);
		return vwSarSurpicRequestList;

	}

	@RequestMapping(value = "/getSarSurpicRequestResponse", method = RequestMethod.POST)
	@ResponseBody
	public Collection<VwSarSurpicRequest> getSarSurpicRequestResponse(String messageId) {
		logger.info("Inside getSarSurpicRequestResponse controller method. message Id=" + messageId);
		Collection<VwSarSurpicRequest> SarSurpicRequestResponseList = new ArrayList<VwSarSurpicRequest>();
		SarSurpicRequestResponseList = reportService.sarSurpicResponses(messageId);
		return SarSurpicRequestResponseList;
	}

	// For ddp log report
	@RequestMapping(value = "/getDdpLogReportPage", method = RequestMethod.GET)
	public String getDdpLogReportPage() {
		return "reports/ddp_log_report";
	}

	@RequestMapping(value = "/getDdpLog", method = RequestMethod.POST)
	@ResponseBody
	public Collection<DdpLog> getDdpLog(Date startDate, Date endDate, String messageType) {
		logger.info("Inside ddp log controller method.  SAR Authority " + messageType + " Start Date " + startDate
				+ " End Date" + endDate);
		Collection<DdpLog> ddpLogList = new ArrayList<DdpLog>();
		if (messageType.equals("9"))
			ddpLogList = reportService.getDdpRequestLogs(startDate, endDate);
		else if (messageType.equals("10"))
			ddpLogList = reportService.getDdpUpdateLogs(startDate, endDate);

		for (DdpLog ddpLog : ddpLogList) { logger.info("ddp log=" + ddpLog); }

		return ddpLogList;
	}

	@RequestMapping(value = "/getDdpSystemLog", method = RequestMethod.POST)
	@ResponseBody
	public Collection<SystemStatus> getDdpSystemLog(Date startDate, Date endDate, String messageType) {
		logger.info("Inside getDdpSystemLog controller method.  SAR Authority " + messageType + " Start Date " + startDate
				+ " End Date" + endDate);
		Collection<SystemStatus> ddpSystemLogList = new ArrayList<SystemStatus>();
		ddpSystemLogList = reportService.getDdpSystemStatusLogs(startDate, endDate);

		for (SystemStatus  ddpLog : ddpSystemLogList) { logger.info("ddp log=" + ddpLog); }

		return ddpSystemLogList;
	}


	@RequestMapping(value = "/getPayload", method = RequestMethod.POST)
	@ResponseBody
	public String getParsedPayload(String messageID, Model model) {
		String payload = reportService.getPayloadAgainstMessageId(messageID);
		logger.info("payload=" + payload);
		return payload;
	}



	//for ASP Log Report

	@RequestMapping(value = "/getAspLogReportPage", method = RequestMethod.GET)
	public String getAspLogReportPage() {
		return "reports/asp_log_report";
	}


	@RequestMapping(value = "/getAspLog", method = RequestMethod.POST)
	@ResponseBody
	public Collection<AspLogInterface> getAspLog(Date startDate, Date endDate, String messageType) {
		logger.info("Inside asp log controller method.  SAR Authority " + messageType + " Start Date " + startDate
				+ " End Date" + endDate);
		Collection<AspLogInterface> aspLogList = new ArrayList<AspLogInterface>();

		if (messageType.equals("7")) 
			aspLogList = reportService.getAspReceiptLog(startDate, endDate); 
		else if(messageType.equals("4"))
			aspLogList = reportService.getAspPositionRequestLog(startDate, endDate);
		else if(messageType.equals("1"))
			aspLogList = reportService.getAspPositionReportLog(startDate, endDate);

		for (AspLogInterface aspLog : aspLogList) {
			logger.info("asp receipt log="+aspLog.getMessageId()+"time ="+aspLog.getReceivedTime());
		}  
		return aspLogList;
	}

	//for Flag vessels at sea Report

	@RequestMapping(value = "/getFlagVesselsAtSeaReportPage", method = RequestMethod.GET)
	public String getFlagVesselsAtSeaReportPage() {
		return "reports/flag_vessels_at_sea";
	}

	@RequestMapping(value = "/getFlagVesselsAtSeaReport", method = RequestMethod.POST)
	@ResponseBody
	public Collection<VwShipStatusLatestPositionDto> getFlagVesselsAtSeaReport(String vesselStatus) {
		Collection<VwShipStatusLatestPositionDto> flagVesselAtSeaList = new ArrayList<VwShipStatusLatestPositionDto>();

		if(vesselStatus.equals("All"))
		{
			flagVesselAtSeaList = reportService.getAllFlagShipsLatestPosition();
		}
		else
		{
			flagVesselAtSeaList = reportService.getLatestPositionOfFlagVessel(vesselStatus);
		}	

		for (VwShipStatusLatestPositionDto vwShipStatusLatestPositionDto : flagVesselAtSeaList) {
			logger.info("vessels at sea="+vwShipStatusLatestPositionDto);
		}
		return flagVesselAtSeaList;
	}










	@RequestMapping(value = "/getPositionReportPage", method = RequestMethod.GET)
	public String GetPositionReportPage()
	{	
		logger.info("Getting shipspeed_report page");
		return "/reports/shipspeed_report";
	}

	@RequestMapping(value = "/showPositionReport", method = RequestMethod.POST)
	@ResponseBody
	public Collection<LritLatestShipPostion> ShowPositionReport(int shipSpeed, Model model)
	{
		logger.info("Inside ShowPositionReport");
		Collection<LritLatestShipPostion> lritLatestShipPostions= reportService.ShowPositionReport(shipSpeed);
		return lritLatestShipPostions;
	}


	@RequestMapping(value = "/getStandingOrderReportPage", method = RequestMethod.GET)
	public String GetStandingOrderReportPage()
	{	
		logger.info("Getting Standing order report page");
		return "/reports/standingorder_report";
	}

	@RequestMapping(value="getStandingOrderReport", method = RequestMethod.POST)
	@ResponseBody
	public String getStandingOrderReport(String status)
	{
		return null;
	}



	@RequestMapping(value="/getShipPositionRequestReportPage", method = RequestMethod.GET)
	public String getShipPositionRequestReportPage()
	{
		return "reports/ship_position_request_report";
	}

	@RequestMapping(value = "/getShipPositionRequestReport", method = RequestMethod.POST)
	@ResponseBody
	public Collection<VwShipPositionRequest> getShipPositionRequestReport(Date startDate, Date endDate)
	{
		logger.info("Inside getShipPositionRequestReport controller method. startDate= "+ startDate + " endDate= "+endDate);
		Collection<VwShipPositionRequest> shipPositionRequestList = new ArrayList<VwShipPositionRequest>();

		shipPositionRequestList = reportService.getShipPositionRequestByReceiveTimeFlag(startDate, endDate);  
		logger.info("Getting outside  getShipPositionRequestReport controller method");
		return shipPositionRequestList;
	}


	@RequestMapping(value="/getShipPositionReportPage", method = RequestMethod.GET)
	public String getShipPositionReportPage()
	{
		return "reports/ship_position_report";
	}

	@RequestMapping(value = "/getShipPositionReport", method = RequestMethod.POST)
	@ResponseBody
	public Collection<VwShipPositionRequest> getShipPositionReport(Date startDate, Date endDate)
	{
		logger.info("Inside getShipPositionRequestReport controller method. startDate= "+ startDate + " endDate= "+endDate);
		Collection<VwShipPositionRequest> shipPositionReportList = new ArrayList<VwShipPositionRequest>();
		shipPositionReportList = reportService.getShipPositionReportByReceiveTime(startDate, endDate);
		return shipPositionReportList;
	}

	@RequestMapping(value="/getGAUpdateToIdePage", method = RequestMethod.GET)
	public String getGAUpdateToIdeReportPage()
	{
		return "reports/ga_update_to_ide";
	}

	@RequestMapping(value = "/getGAUpdateToIde", method = RequestMethod.POST)
	@ResponseBody
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfFlagToIde(@SessionAttribute("USER") UserSessionInterface user, Date startDate, Date endDate) {
		Collection<VwGeographicalAreaUpdate> geographicalAreaToIdeList = new ArrayList<VwGeographicalAreaUpdate>();
		String lritId = user.getRequestorsLritId().getLritId();
		if(lritId.equals("1065"))
		{
			geographicalAreaToIdeList = reportService.getGeographicalAreaOfFlagToIde(startDate, endDate);
		}
		else
		{
			geographicalAreaToIdeList = reportService.getGeographicalAreaOfOtherToIde(lritId, startDate, endDate);
		}
		return geographicalAreaToIdeList;
	}


	@RequestMapping(value="/getGAUpdateToDdpPage", method = RequestMethod.GET)
	public String getGAUpdateToDdpReportPage()
	{
		return "reports/ga_update_to_ddp";
	}

	@RequestMapping(value = "/getGAUpdateToDdp", method = RequestMethod.POST)
	@ResponseBody
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfFlagToDdp(@SessionAttribute("USER") UserSessionInterface user, Date startDate, Date endDate) {

		Collection<VwGeographicalAreaUpdate> geographicalAreaToDdpList = new ArrayList<VwGeographicalAreaUpdate>();
		String lritId = user.getRequestorsLritId().getLritId();
		if(lritId.equals("1065"))
		{
			geographicalAreaToDdpList = reportService.getGeographicalAreaOfFlagToDdp(startDate, endDate);
		}
		else
		{
			geographicalAreaToDdpList = reportService.getGeographicalAreaOfOtherToDdp(lritId, startDate, endDate);
		}
		return geographicalAreaToDdpList;
	}

	@RequestMapping(value="/getOpenSoPage", method = RequestMethod.GET)
	public String getOpenSoReportPage()
	{
		return "reports/open_standing_order";
	}


	@RequestMapping(value = "/getOpenSo", method = RequestMethod.POST)

	@ResponseBody public Collection<VwStandingOrder>
	getOpenSo(@SessionAttribute("USER") UserSessionInterface user, Date
			startDate, Date endDate) {


		Collection<VwStandingOrder> openStandingOrderList = new
				ArrayList<VwStandingOrder>(); String lritId =
				user.getRequestorsLritId().getLritId(); if(lritId.equals("1065")) {
					openStandingOrderList = reportService.getOpenedStandingOrder(startDate,
							endDate); } return openStandingOrderList; }


	@RequestMapping(value = "/getOpenSoNextLevelData", method = RequestMethod.POST)
	@ResponseBody
	public  Map<String, Collection<String>> getOpenSoNextLevelData(BigInteger soId, Date startDate, Date endDate, Model model) {
		logger.info("Inside open SO controller. startDate "+startDate +" endDate "+endDate );

		Map<String, Collection<String>> excludedVal = reportService.getExcludedValues(soId, startDate, endDate);
		logger.info("Map val="+excludedVal);
		return excludedVal;

	}  
	
	@RequestMapping(value="/getDataAnalysisReportPage", method = RequestMethod.GET)
	public String getDataAnalysisReportPage(Model model)
	{
		logger.info("inside getDataAnalysisReportPage() method");
		return "reports/data_analysis_report";
	}
	
	/*@RequestMapping(value="/getDataAnalysisReportUpdate", method = RequestMethod.GET)
	public String getDataAnalysisReportUpdate(Date startDate, Date endDate, String Type)
	{
		logger.info("inside getDataAnalysisReportPage() method");
		return "reports/data_analysis_report";
	}*/

	@RequestMapping(value="/getpolygonOrboundarieslist", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, String> getPolygonOrBoundariesList(@RequestParam String type)
	{
		logger.info("inside getPolygonList() method");
		logger.info(type);
		Map<String, String> polygonOrcountrylist = new HashMap<String, String>();
		
		if(type.equals("User_Defined_Area"))
		{
			List<CustomPolygonList> areaNameList = geographicalService.findAreaNameList();
			for (CustomPolygonList customPolygonList : areaNameList) {
				polygonOrcountrylist.put(customPolygonList.getPolygonId(), customPolygonList.getPolygon_name());
			}
		}
		else if(type.equals("Geographical_Boundaries"))
		{
			List<LritContractGovtList> countryList = cgRepository.getAllContractingGovtsNames();
			for (LritContractGovtList lritContractGovtList : countryList) {
				logger.info("lritContractGovtList : "+lritContractGovtList.getCgLritid() + " : " + lritContractGovtList.getCgName());
				polygonOrcountrylist.put(lritContractGovtList.getCgLritid(), lritContractGovtList.getCgName());
			}
		}
		return polygonOrcountrylist;
	}
	
	
	@RequestMapping(value="/getvesseldetails", method=RequestMethod.GET)
	@ResponseBody
	public List<VesselDetailInterface> getVesselDetails(@RequestParam String areaName, @RequestParam Date fromdate, @RequestParam Date todate)
	{
		logger.info("areaName : "+areaName);
		logger.info("startdate : "+fromdate);
		logger.info("enddate : "+todate);
		return null;
	}

}