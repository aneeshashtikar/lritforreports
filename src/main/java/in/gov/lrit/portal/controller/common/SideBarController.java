package in.gov.lrit.portal.controller.common;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.service.common.SideBarService;

@Controller
@RequestMapping("/sidebar")
public class SideBarController {
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SideBarController.class);
	
	@Autowired
	private SideBarService sidebarService;

	/**
	 * This function returns the Flag vessels by Reporting frequency data
	 * */
	@RequestMapping("/getreportingFrequency")
	@ResponseBody
	public Map<String, Integer> getFlagVesselCountByFrequency()
	{
		logger.info("inside side bar controlller to get the vessel count by frequency");
		List<Object []> freqlist= sidebarService.getAspTerminalFrequencyCountFromVW();
		Map<String, Integer> freqCountMap = new HashMap<String, Integer>();
		if(freqlist!=null) {
		for (Object[] objects : freqlist) {
			Integer frequency = (Integer)objects[0];
			BigInteger count = (BigInteger) objects[1];
			if(frequency==15)
			freqCountMap.put("FifteenMin", count.intValue());
			if(frequency==30)
				freqCountMap.put("ThirtyMin", count.intValue());
			if(frequency==60)
				freqCountMap.put("OneHr", count.intValue());
			if(frequency==180)
				freqCountMap.put("ThreeHr", count.intValue());
			if(frequency==360)
				freqCountMap.put("SixHr", count.intValue());
			if(frequency==720)
				freqCountMap.put("TwelveHr", count.intValue());
			if(frequency==1440)
				freqCountMap.put("TwentyFourHr", count.intValue());
		}
		}
		else
		{
			freqCountMap=null;
			logger.error("No Vessel found with different frequency");
		}
		freqCountMap.entrySet().forEach(entry->{
			    logger.info(entry.getKey() + " " + entry.getValue());  
			 });
		return freqCountMap;
	}
	
	
	/***
	 * Method to retrieve the vessel detailed list by frequency rate
	 * @return
	 */
	@RequestMapping("/vesselDetailListByFrequencyRate")
	@ResponseBody
	public List<VesselDetailInterface> getVesselDetailListFromVWByFreqRate(@RequestParam Integer freqRate)
	{
		logger.info("Inside getVesselDetailListFromVWByVesselStatus ");
		List<VesselDetailInterface> vesselDetailList=sidebarService.getVesselDetailListFromVWByfreqRate(freqRate);
		for( VesselDetailInterface vesselDetail :vesselDetailList)
			logger.debug("Imo number of the vessels is"+vesselDetail.getImoNo());
			
		return vesselDetailList;
	}
	
	
	/***
	 * Method to retrieve the vessel count on the basis of user tyoe
	 * if User is Shipping company it will return that shipping company count
	 * Otherwise it will return vessel count by lritid of the country
	 * @return
	 */
	@RequestMapping("/getflagShipCountByReportingStatus")
	@ResponseBody
	public Map<String, Integer> getFlagVesselCountByReportingStatus() {
		logger.info("getting flag vessel count by reporting status");
		List<Object[]> reportStatusList =new ArrayList<Object[]>();
		reportStatusList = sidebarService.getFlagShipCountByReportingStatus();
		Map<String, Integer> reportStatusAndCount = new HashMap<String, Integer>();
		if (!reportStatusList.isEmpty()) {
			for (Object[] objects : reportStatusList) {
				String reportingStatus = (String) objects[0];
				BigInteger count = (BigInteger) objects[1];
				logger.info("reporting status : " + reportingStatus);
				logger.info("count : " + count);
				if(reportingStatus !=null)
				reportStatusAndCount.put(reportingStatus, count.intValue());
			}
		}
		int inactivecount = sidebarService.getInactiveShipCount();
		reportStatusAndCount.put("InactiveShip", inactivecount);
		logger.info("inactive count : " + inactivecount);
		return reportStatusAndCount;
	}
	
	/***
	 * Method to retrieve the vessel detailed list by reporting status
	 * @return
	 */
	@RequestMapping("/vesselDetailListByReportingStatus")
	@ResponseBody
	public List<VesselDetailInterface> getVesselDetailListFromVWByVesselStatus(@RequestParam String vesselStatus)
	{
		logger.info("Inside getVesselDetailListFromVWByVesselStatus ");
		List<VesselDetailInterface> vesselDetailLsit=sidebarService.getVesselDetailListFromVWByVesselStatus(vesselStatus);
		for( VesselDetailInterface vesselDetail :vesselDetailLsit)
			logger.debug("Imo number of the vessels is"+vesselDetail.getImoNo());
			
		return vesselDetailLsit;
	}
	
}
