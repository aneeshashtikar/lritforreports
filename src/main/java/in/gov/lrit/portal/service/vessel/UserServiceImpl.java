package in.gov.lrit.portal.service.vessel;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.UserRepository;
import in.gov.lrit.portal.model.user.PortalUser;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository repo;
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Override
	public List<PortalUser> getBycompanyCode(String companyCode) {
		logger.info("inside user method");
		return repo.findUserBycompanyCode(companyCode);
	}

	@Override
	public Optional<PortalUser> getbyloginId(String loginId) {
		logger.info("inside user method");
		return repo.findById(loginId);
	}

	@Override
	public PortalUser getByname(String name) {
		logger.info("inside getByname() method");
		return repo.findByname(name);
	}

	@Override
	public PortalUser getUserBycompanyCodeAndRelation(String companyCode) {
		logger.info("inside getUserBycompanyCodeAndRelation() method");
		return repo.findUserBycompanyCodeAndRelation(companyCode);
	}
	
}