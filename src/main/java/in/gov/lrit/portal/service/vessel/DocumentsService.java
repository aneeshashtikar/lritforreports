package in.gov.lrit.portal.service.vessel;

import java.util.List;

import in.gov.lrit.portal.model.vessel.PortalDocuments;
import in.gov.lrit.portal.model.vessel.PortalShippingCompanyVesselRelation;

public interface DocumentsService {
	
	public Iterable<PortalDocuments> addAllDocuments(Iterable<PortalDocuments> docslist);
	
	public List<PortalDocuments> getDocsByreferenceId(String referenceId, List<String> doc_purpose);
	
	public PortalDocuments addDocument(PortalDocuments document);

}
