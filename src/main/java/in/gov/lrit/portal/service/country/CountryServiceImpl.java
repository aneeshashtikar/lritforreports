package in.gov.lrit.portal.service.country;

import in.gov.lrit.gis.ddp.model.LritContractGovtMst;
import in.gov.lrit.gis.ddp.repository.ContractingGovtMasterRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryServiceImpl
  implements ICountryService
{
  @Autowired
  ContractingGovtMasterRepository cgMasterRepo;

	@Override
	public List<LritContractGovtMst> getCountriesList(String regularVersion) {
		List<LritContractGovtMst> countrylist=cgMasterRepo.findByCgName(regularVersion);
		return countrylist;
	}
}