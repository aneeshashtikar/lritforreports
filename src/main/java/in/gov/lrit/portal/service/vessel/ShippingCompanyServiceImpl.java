package in.gov.lrit.portal.service.vessel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.ShippingCompanyRepository;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
@Service
public class ShippingCompanyServiceImpl implements ShippingCompanyService{

	@Autowired
	private ShippingCompanyRepository companyRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(ShippingCompanyServiceImpl.class);
	
	/*@Override
	public Map<String, String> findCsoByCode(String companyCode) {
		System.out.println("Inside shipping company service");
		return companyRepo.findCSOByCompCode(companyCode);
	}*/

	@Override
	public Optional<PortalShippingCompany> getShipingById(String id) {
		logger.info("Inside shipping company service");
		return companyRepo.findById(id);
	}

	@Override
	public List<Object[]> shippingCompanyList() {
		logger.info("Inside shipping company service");
		return companyRepo.findShippingCompanyList();
	}

	@Override
	public String getCompanyCodeByName(String companyName) {
		logger.info("Inside shipping company service");
		return companyRepo.findCompanyCodeByName(companyName);
	}

	/*@Override
	public String getLoginIdByName(String comapnyName) {
		logger.info("Inside shipping company service");
		return companyRepo.findLoginIdByName(comapnyName);
	}*/

	@Override
	public Iterable<PortalShippingCompany> getAllCompanyList() {
		logger.info("Inside shipping company service");
		return companyRepo.findAll();
	}

	@Override
	public PortalShippingCompany getBycompanyName(String companyName) {
		logger.info("Inside shipping company service");
		return companyRepo.findBycompanyName(companyName);
	}

}