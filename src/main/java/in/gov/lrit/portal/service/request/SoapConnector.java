package in.gov.lrit.portal.service.request;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;

import org.imo.gisis.xml.lrit.coastalstatestandingorderupdate._2014.CoastalStateStandingOrderUpdate;
import org.imo.gisis.xml.lrit.ddprequest._2008.DDPRequestType;
import org.imo.gisis.xml.lrit.geographicalareaupdate._2014.GeographicalAreaUpdate;
import org.imo.gisis.xml.lrit.positionrequest._2008.ShipPositionRequestType;
import org.imo.gisis.xml.lrit.surpicrequest._2014.SURPICRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import responses.portalresponse.PortalResponse;

public class SoapConnector extends WebServiceGatewaySupport {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SoapConnector.class);
	@Value("${lrit.dc_url}")
	private String url;
	
	PortalResponse resp=null;

	String responseStatus=null;
	//call DC webservice for ddp request
	public String callWebService(DDPRequestType request) throws SOAPException,JAXBException,Exception{
		logger.info("in call web service");
		MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		WebServiceTemplate wsTemplate = getWebServiceTemplate();
		wsTemplate.setMessageFactory(saajSoapMessageFactory);
		resp =(PortalResponse) wsTemplate.marshalSendAndReceive(url, request);

		JAXBContext jaxbContext = JAXBContext.newInstance(PortalResponse.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(resp, sw);
		if ((resp.isSuccessful()))
		{
			responseStatus="true";
		}
		else
		{
			responseStatus=resp.getMessage().toString();
		}

		return responseStatus;
	}

	public PortalResponse callWebService( GeographicalAreaUpdate request) throws SOAPException{
		//String res = (String) getWebServiceTemplate().marshalSendAndReceive(url, request);
		logger.info("in call web service");
		try {
			MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
			SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
			WebServiceTemplate wsTemplate = getWebServiceTemplate();
			wsTemplate.setMessageFactory(saajSoapMessageFactory);

			resp =(PortalResponse) wsTemplate.marshalSendAndReceive(url, request);
			logger.info("after  call web service");
			logger.info("response="+resp.isSuccessful());

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return resp;
	}
	
	


	//call DC webservice for flag,port,coastal,sar request
	public String callWebService(ShipPositionRequestType request) throws SOAPException,JAXBException,Exception{
		logger.info("in call web service");
		MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		WebServiceTemplate wsTemplate = getWebServiceTemplate();
		wsTemplate.setMessageFactory(saajSoapMessageFactory);
		resp =(PortalResponse) wsTemplate.marshalSendAndReceive(url, request);

		JAXBContext jaxbContext = JAXBContext.newInstance(PortalResponse.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(resp, sw);
		if ((resp.isSuccessful()))
		{
			responseStatus="true";
		}
		else
		{
			responseStatus=resp.getMessage().toString();
		}

		return responseStatus;
	}


	//call DC webservice for surpic request
	public String callWebService(SURPICRequest request) throws SOAPException,JAXBException,Exception{
		logger.info("in call web service");
		MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		WebServiceTemplate wsTemplate = getWebServiceTemplate();
		wsTemplate.setMessageFactory(saajSoapMessageFactory);
		resp =(PortalResponse) wsTemplate.marshalSendAndReceive(url, request);

		JAXBContext jaxbContext = JAXBContext.newInstance(PortalResponse.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(resp, sw);
		if ((resp.isSuccessful()))
		{
			responseStatus="true";
		}
		else
		{
			responseStatus=resp.getMessage().toString();;
		}

		return responseStatus;
	}
	
	public String callWebService(CoastalStateStandingOrderUpdate request) throws SOAPException, JAXBException, Exception {
		logger.info("in call web service");
		MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		WebServiceTemplate wsTemplate = getWebServiceTemplate();
		wsTemplate.setMessageFactory(saajSoapMessageFactory);
		resp = (PortalResponse) wsTemplate.marshalSendAndReceive(url, request);

		JAXBContext jaxbContext = JAXBContext.newInstance(PortalResponse.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(resp, sw);
		if ((resp.isSuccessful())) {
			responseStatus = "true";
		} else {
			responseStatus = resp.getMessage().toString();
		}

		return responseStatus;
	}
	
}



