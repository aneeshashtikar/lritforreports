package in.gov.lrit.portal.service.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*import in.gov.lrit.portal.dao.role.ActivityManagementDao;*//*
import in.gov.lrit.portal.dao.role.PortalRoleActivitiesMappingDao;*/
/*import in.gov.lrit.portal.dao.role.RoleManagementDao;*/
import in.gov.lrit.portal.model.role.PortalActivities;
import in.gov.lrit.portal.model.role.PortalRoleActivitiesMapping;
import in.gov.lrit.portal.model.role.PortalRoleActivitiesMappingPK;
import in.gov.lrit.portal.model.role.PortalRoles;
import in.gov.lrit.portal.repository.role.ActivityManagementRepository;
import in.gov.lrit.portal.repository.role.PortalRoleActivitiesMappingRepository;
import in.gov.lrit.portal.repository.role.RoleManagementRepository;

@Service
public class RoleManagementService implements IRoleManagementService {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RoleManagementService.class);
	@Autowired
	ActivityManagementRepository activitymanagementRepository;
	@Autowired
	RoleManagementRepository rolemanagementRepository;
	@Autowired
	PortalRoleActivitiesMappingRepository portalRoleActivitiesMappingRepository;

	
	/**
	 * This method returns returns list of activities from database
	 * 
	 * @return List<PortalActivities>
	 */
	@Override
	// Get all activities from database
	public List<PortalActivities> getActivities() {
		logger.info("Inside getActivities service method");
		List<PortalActivities> portalActivities = new ArrayList<PortalActivities>();
		portalActivities = (List<PortalActivities>) activitymanagementRepository.findAll();
		if(portalActivities != null) {
			logger.info("size of activitites from database = "+portalActivities.size());
		}
		
		logger.info("getting outside getActivities service");
		return portalActivities;
	}

	@Override
	public void saveRole(PortalRoles portalRoles) {
		// Save portal role in database
		// String roleId = "ROLE" + System.currentTimeMillis();
		String roleId = "" + System.currentTimeMillis();
		portalRoles.setRoleId(roleId);
		logger.info(portalRoles.getRoleId() + portalRoles.getRolename() + portalRoles.getShortname()
				+ portalRoles.getStatus() + portalRoles.getActivities());
		rolemanagementRepository.save(portalRoles);
		return;
	}

	public List<PortalRoles> getRoles(String status) {
		// Get all portal role from database where status=active
		List<PortalRoles> portalRoles = new ArrayList<>();
		if(status.equals("all")) {
			portalRoles = rolemanagementRepository.getAllRoles();
		}else {
		portalRoles = rolemanagementRepository.getAllRoles(status);
		}
		return portalRoles;
	}

	public void deregisterRole(String roleId) {
		logger.info("Inside deregisterrole roleid " + roleId);
		logger.info(roleId);
		int status=rolemanagementRepository.updateStatus(roleId);
		logger.info("status "+status);
		return;
	}

	public PortalRoles getRoleDetails(String roleId) {
		logger.info("getting roles " + roleId);
		PortalRoles portalrole = rolemanagementRepository.getRoleDetails(roleId);
		logger.info("portal role value: " + portalrole);
 if(portalrole==null) {
	 portalrole=rolemanagementRepository.getRoleInfo(roleId);
 }
		return portalrole;

	}

	public void editRole(PortalRoles portalRoles) {
		logger.info("Inside edit role service " + portalRoles.toString());
		rolemanagementRepository.deleteRoleActivityMapping(portalRoles.getRoleId());
		Set<PortalActivities> portalActivities = portalRoles.getActivities();
		List<PortalRoleActivitiesMapping> portalRoleActivitiesMapping = new ArrayList<>();
		for (PortalActivities portalactivity : portalActivities) {
			PortalRoleActivitiesMappingPK portalRoleActivitiesMappingPK = new PortalRoleActivitiesMappingPK();
			portalRoleActivitiesMappingPK.setActivityId(portalactivity.getActivityId());
			portalRoleActivitiesMappingPK.setRoleId(portalRoles.getRoleId());
			PortalRoleActivitiesMapping mapping = new PortalRoleActivitiesMapping();
			mapping.setId(portalRoleActivitiesMappingPK);
			portalRoleActivitiesMapping.add(mapping);
		}
		logger.info("portalRoleActivityMapping " + portalRoleActivitiesMapping.toString());
		portalRoleActivitiesMappingRepository.saveAll(portalRoleActivitiesMapping);

		// rolemanagementDao.save(portalRoles.getActivities());

	}
	
	/**
	 * This method checks existences of rolename return boolean.
	 * 
	 * @return boolean 
	 */
	public boolean checkRoleName(String roleName) {
		boolean flag=false;
		flag=rolemanagementRepository.checkRole(roleName);
		return flag;
	}


	@Override
	public boolean checkShorName(String shortName) {
		boolean flag=false;
		flag=rolemanagementRepository.checkShorName(shortName);
		return flag;
	}
	@Override
	public List<PortalRoles> getRoleList(List<String> roleIdList) {
	
	
		return 	rolemanagementRepository.getRoleList(roleIdList);
	}
}
