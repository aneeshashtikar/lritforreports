package in.gov.lrit.portal.service.vessel;

import java.math.BigInteger;
import java.util.List;

import in.gov.lrit.portal.model.pendingtask.PortalShipEquipementHst;
import in.gov.lrit.portal.model.vessel.PortalShipEquipment;
import in.gov.lrit.portal.model.vessel.ShipEquipmentAndVesselInterface;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;

public interface VesselDetailService {
	
	public List<ShipEquipmentAndVesselInterface> getVesselDetailList(List<String> status,List<String> regstatus);
	
	public boolean manageDNID(Integer vesselId,String requestorLritid,BigInteger messagetype,BigInteger requesttype);
	
	
	public PortalShipEquipment shipBorneEquiomentDetail(Integer vesselId);
	
	public int DeleteSeid(Integer vesselId);
	
	public String getDnidStatus(Integer vesselId);
	
	public int updateDnid(int vesselId, int dnidNo, int memberNo );
	
	public String checkVesselStatus(int vesselId,List<String>registrationStatus,List<String> shipstatus) ;

	public int updateSeid(int vesselid,int memberno,int dnidno);
	
	public int updateReqResponsePayload(int vesselid,String req, String response);
	
	public int updateVesselRegStatus(int vesselid,String status);
	
	public PortalShipEquipementHst saveSeidHistory(PortalShipEquipementHst seidhst);
	
	public int getInactiveShipCount();
	
	public int updatetimedelay(BigInteger timedelay,Integer Vesselid);
	public String gettimedelay(Integer vesselId);
}

