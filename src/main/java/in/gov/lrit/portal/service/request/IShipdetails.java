package in.gov.lrit.portal.service.request;

import java.util.Collection;
import java.util.List;
import com.vividsolutions.jts.geom.Geometry;
import in.gov.lrit.portal.model.request.DcPositionRequest;
import in.gov.lrit.portal.model.request.LritLatestShipPositionFlag;
import in.gov.lrit.portal.model.request.ShipPositionNonFlag;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;

public interface IShipdetails {

	public List<VesselDetailInterface> getFlagShipDetailsList(List<String> status,List<String> regstatus);
	 
	public List<LritLatestShipPositionFlag> getShipwithinBoundry(Geometry geo);
	
	public Collection<ShipPositionNonFlag> getNonFlagShips();
	
	
	public List<DcPositionRequest> getReportingratefromDc(String imo,String status,int messagetype,List<Integer> accesstype);
	
	public int getVesselDetail(String imo);

}
