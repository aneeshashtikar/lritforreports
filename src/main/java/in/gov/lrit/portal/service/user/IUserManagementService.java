/**

 * @IUserManagementService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author   Bhavika Ninawe
 * @version 1.0
 * IUserManagementService used for exposing the method for of user management
 */
package in.gov.lrit.portal.service.user;

import java.io.Serializable;
import java.net.ConnectException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.security.crypto.bcrypt.BCrypt;

import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.user.PortalCsoDpaDetail;
import in.gov.lrit.portal.model.user.PortalUser;
import in.gov.lrit.portal.model.user.PortalUserDTO;
import in.gov.lrit.portal.model.user.PortalUserInterface;
import in.gov.lrit.portal.model.user.PortalUsersRoleMapping;
import in_.gov.lrit.session.ContractingGovernmentInterface;
import in_.gov.lrit.session.UserSessionInterface;

public interface IUserManagementService extends Serializable {

	public Collection<PortalUserInterface> getUserUsingStatus(String status);
	public Collection<PortalUserInterface> getAllUser();
	
	public boolean getUser(String lritId);
	public String persistUser(PortalUserDTO portalUserDTO,Set<PortalUsersRoleMapping> userRoleMapping);
	public List<PortalUser> getUser();
	public PortalUserDTO getUserDetails(String loginId);
	public int deregisterUser(String loginId);
	public boolean checkUserExist(String loginId);
	public void updateUser(PortalUserDTO portalUserDTO, Set<PortalUsersRoleMapping> userRoleMapping);

	public String checkCompanyCodeExist(String companyCode);
	public void updateStatus(String loginId,String status);
	public PortalUser getPortalUser(String LoginId);
	public PortalUser getActivePortalUser(String LoginId);
	public List<PortalCsoDpaDetail> getCsoList(String loginId);
	public void persistAlternateCsoId(List<PortalCsoDpaDetail> csoList);
	
	public String generateToken(String loginId);
	public String getWebPath(PortalLritIdMaster contractinggovernment);
	public void sendMail(String sender, String recipient, String subject, String body) throws ConnectException, Exception;
	public boolean checkPassword(String password, String loginId);
	public void persistUserPassword(String loginId, String password);
	public void updatePassword(String loginId, String hashedPassword);

	
	public PortalUserInterface getPortalUserInterface(String loginId);
	public UserSessionInterface getUserSessionByLoginId(String loginId);
	public void updateLoginAttempt(int noOfAttempt,String loginId);
	
	public boolean getVessel(String userId) ;
	
	//check if cso is associated with any vessel
	public boolean checkCsoIdVesselMapping(String csoId) ;
	//check if dpa is associated with any vessel
	public boolean checkDpaVesselMapping(String csoId);
	
	public PortalUser getUserDetailsByImo(String imoNo);
	public PortalCsoDpaDetail getCurrentCsoDetails(String imoNo); 
	public PortalCsoDpaDetail getAlternateCsoDetails(String imoNo);
	public List<String> getActivityListByType(String loginId, String type);
	public String getSarAuthority(String loginId);

	/*Data get for contracting government list with lritid and country name*/
	public List<String> getContextListByLoginId(String loginId);
	public List<ContractingGovernmentInterface> getCGByContext(List<String> contextList);
}
