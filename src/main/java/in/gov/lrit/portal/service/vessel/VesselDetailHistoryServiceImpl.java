package in.gov.lrit.portal.service.vessel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.model.vessel.PortalVesselDetailHistory;
import in.gov.lrit.portal.repository.vessel.VesselDetailHistoryRepository;

@Service
public class VesselDetailHistoryServiceImpl implements VesselDetailHistoryService{

	@Autowired
	private VesselDetailHistoryRepository repo;
	
	Logger logger = LoggerFactory.getLogger(VesselDetailHistoryServiceImpl.class);
	
	@Override
	public PortalVesselDetailHistory addVesselDetailHst(PortalVesselDetailHistory vesselDetHst) {
		logger.info("Inside vessel details history service");
		return repo.save(vesselDetHst);
	}

}
