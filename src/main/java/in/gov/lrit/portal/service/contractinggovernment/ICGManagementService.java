package in.gov.lrit.portal.service.contractinggovernment;

import java.util.List;

import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;

public interface ICGManagementService {
	//Related to Contracting government
		public void persistContractingGovernment(PortalLritIdMaster portalLritIdMaster);
		public List<PortalLritIdMaster> getContractingGovList();
		
		public List<PortalLritIdMaster> getSarAuthorityDetails(String countrylritId);
		public boolean checkCountryExistance (String countryName);
		public boolean checkCountryCode(String countryCode);
		public boolean checkCoastalAreaExistance(String coastalArea);
		public boolean checkLritId(String lritId);
		public PortalLritIdMaster getDetails(String lritId);
		public void deleteCgSar(String countryName);
		public void deleteCg(String lritId);
		public List<PortalLritIdMaster> getSarList(String countryName);
		public boolean checkExistenceSarLritId(String lritId);
		public void deleteSar(String lritId);
		public void saveCg(PortalLritIdMaster cgDetails);
		public void saveSar(List<PortalLritIdMaster> sarList);
		public List<String> getAllLritId();
		
}
