package in.gov.lrit.portal.service.alert;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

import in.gov.lrit.portal.model.alert.AlertActionDetailsDTO;
import in.gov.lrit.portal.model.alert.AlertDetails;


public interface IAlertService {
	
	public List<AlertDetails> findAllAlerts(List<String> roleList, String userCategory, List<String> contextList);
	
	public List<String> getRoleAgainstLoginId(String loginId);
	
	/*
	 * public List<AlertDetails> getUserSpecificAlerts(List<String> roleList, String
	 * severity, String loginId, List<String> contextIdList);
	 */
	
	public List<AlertDetails> getUserSpecificAlerts(List<String> roleList, String severity, String userCategory, List<String> contextIdList);
	
	public Map<String, Integer> getUserSpecificAlertCount(List<String> roleList, String userCategory, List<String> contextIdList);
	
	public int insertIntoAlertActionTaken(BigInteger alertTxnId, String loginId, String remark, String userCategory);
	
	public List<AlertDetails> getShippingCoAlerts(List<String> roleList, String severity, String loginId);
	
	public Map<String, Integer> getShippingCoAlertCount(List<String> roleList, String loginId);
	
	public List<AlertActionDetailsDTO> getClosedAlerts(String userCategory, List<String> contextList, Date startDate, Date endDate);
}
