package in.gov.lrit.portal.service.report;

import java.util.Date;
import java.util.HashMap;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import in.gov.lrit.portal.CommonUtil.ICommonUtil;
import in.gov.lrit.portal.model.report.ASPShipPosition;
import in.gov.lrit.portal.model.report.AspLogInterface;
import in.gov.lrit.portal.model.report.CspLog;
import in.gov.lrit.portal.model.report.DdpLog;
import in.gov.lrit.portal.model.report.SystemStatus;
import in.gov.lrit.portal.model.report.LritLatestShipPostion;
import in.gov.lrit.portal.model.report.VwGeographicalAreaUpdate;
import in.gov.lrit.portal.model.report.VwSarSurpicRequest;
import in.gov.lrit.portal.model.report.VwShipPositionRequest;
import in.gov.lrit.portal.model.report.VwStandingOrder;
//import in.gov.lrit.portal.model.report.VwStandingOrder;
import in.gov.lrit.portal.repository.report.ASPShipPositionRepository;
import in.gov.lrit.portal.repository.report.CspLogRepository;
import in.gov.lrit.portal.repository.report.DdpLogRepository;
import in.gov.lrit.portal.repository.report.ReportRepository;
import in.gov.lrit.portal.repository.report.SystemStatusRepository;
import in.gov.lrit.portal.repository.report.VwGeographicalAreaUpdateRepository;
import in.gov.lrit.portal.repository.report.VwSarSurpicRequestRepository;
import in.gov.lrit.portal.repository.report.VwShipPositionRequestRepository;
import in.gov.lrit.portal.repository.report.VwShipStatusLatestPositionRepository;
import in.gov.lrit.portal.repository.report.VwStandingOrderRepository;

import in_.gov.lrit.portal.dto.VwShipStatusLatestPositionDto;

@Service
public class ReportServiceImpl implements ReportService{

	@Autowired
	private ReportRepository reportRepository;
	
	@Autowired
	private VwSarSurpicRequestRepository vwSarSurpicRequestRepository; 
	
	@Autowired
	private VwShipPositionRequestRepository vwShipPositionRequestRepository;
	
	@Autowired
	private ASPShipPositionRepository aspShipPositionRepository;
	
	@Autowired
	private VwGeographicalAreaUpdateRepository vwGeographicalAreaUpdateRepository;
	
	@Autowired
	private VwStandingOrderRepository vwStandingOrderRepository;
	
	@Autowired 
	private CspLogRepository cspLogRepository;
	
	@Autowired 
	private DdpLogRepository ddpLogRepository;
	
	@Autowired 
	private SystemStatusRepository systemStatusRepository;
	
	@Autowired 
	private VwShipStatusLatestPositionRepository vwShipStatusRepository;
	
	@Autowired
	private ICommonUtil icommonUtil;
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ReportServiceImpl.class);
	
	@Override
	public Collection<LritLatestShipPostion> ShowPositionReport(int shipSpeed) {
		logger.info("Inside ShowPositionReport");
		Collection<LritLatestShipPostion> lritLatestShipPostions =reportRepository.showShipSpeedReport(shipSpeed);
		return lritLatestShipPostions;
	}

	@Override
	public Collection<VwSarSurpicRequest> sarSurpicRequests(Date startDate, Date endDate, String sarAuthority, String sarRequestType) {
		Collection<VwSarSurpicRequest> sarSurpicRequestList = new ArrayList<VwSarSurpicRequest>();
		logger.info("sar authority "+sarAuthority);
		logger.info("SAR REQUEST TYPE "+sarRequestType);
		if(sarAuthority.equals("Our") && sarRequestType.equals("Circular"))
			sarSurpicRequestList = vwSarSurpicRequestRepository.SarSurpicOurCircularRequests(startDate, endDate);
		else if (sarAuthority.equals("Our") && sarRequestType.equals("Rectangular"))
			sarSurpicRequestList = vwSarSurpicRequestRepository.SarSurpicOurRectangularRequests(startDate, endDate);
		else if (sarAuthority.equals("Our") && sarRequestType.equals("Both"))
			sarSurpicRequestList = vwSarSurpicRequestRepository.SarSurpicOurBothRequests(startDate, endDate);
		else if (sarAuthority.equals("Foreign") && sarRequestType.equals("Circular"))
			sarSurpicRequestList = vwSarSurpicRequestRepository.SarSurpicForeignCircularRequests(startDate, endDate);
		else if (sarAuthority.equals("Foreign") && sarRequestType.equals("Rectangular"))
			sarSurpicRequestList = vwSarSurpicRequestRepository.SarSurpicForeignRectangularRequests(startDate, endDate);
		else if(sarAuthority.equals("Foreign") && sarRequestType.equals("Both"))
			sarSurpicRequestList = vwSarSurpicRequestRepository.SarSurpicForeignBothRequests(startDate, endDate);
		else if(sarAuthority.equals("Both") && sarRequestType.equals("Circular"))
			sarSurpicRequestList = vwSarSurpicRequestRepository.SarSurpicBothCircularRequests(startDate, endDate);
		else if(sarAuthority.equals("Both") && sarRequestType.equals("Rectangular"))
			sarSurpicRequestList = vwSarSurpicRequestRepository.SarSurpicBothRectangularRequests(startDate, endDate);
		else
			sarSurpicRequestList = vwSarSurpicRequestRepository.sarSurpicAllRequest(startDate, endDate);
		
		return sarSurpicRequestList;
	}

	@Override
	public Collection<VwSarSurpicRequest> sarSurpicResponses(String messageId) {
		
		Collection<VwSarSurpicRequest> sarSurpicResponseList = new ArrayList<VwSarSurpicRequest>();
		sarSurpicResponseList = vwSarSurpicRequestRepository.SarSurpicResponses(messageId);
		
		for (VwSarSurpicRequest vwSarSurpicRequest : sarSurpicResponseList) {
			logger.info("data in service:"+vwSarSurpicRequest);
		}
		return sarSurpicResponseList;
	}

	@Override
	public Collection<VwShipPositionRequest> getShipPositionRequestByReceiveTimeFlag(Date startDate, Date endDate) {
		Collection<VwShipPositionRequest> shipPositionRequestList = new ArrayList<VwShipPositionRequest>();
		shipPositionRequestList = vwShipPositionRequestRepository.getShipPositionRequestByReceiveTimeFlag(startDate, endDate);
		return shipPositionRequestList; 
	}

	@Override
	public Collection<VwShipPositionRequest> getShipPositionReportByReceiveTime(Date startDate, Date endDate) {
		Collection<VwShipPositionRequest> shipPositionReportList = new ArrayList<VwShipPositionRequest>();
		shipPositionReportList = vwShipPositionRequestRepository.getShipPositionReportByReceiveTime(startDate, endDate);
		return shipPositionReportList; 
	}

	@Override
	public Collection<ASPShipPosition> getVessselDetails(Date startDate, Date endDate) {
		Collection<ASPShipPosition> aspShipPositionReport = new ArrayList<ASPShipPosition>();
		aspShipPositionReport = aspShipPositionRepository.getVessselDetails(startDate, endDate);
		for(ASPShipPosition aspShipPosition: aspShipPositionReport) {
			logger .info("aspShipPosition :: "+aspShipPosition.toString());
		}
		
		
		return aspShipPositionReport;
	}

	@Override
	public Collection<ASPShipPosition> getShipPosition(String imoNo, Date startDate, Date endDate) {
		Collection<ASPShipPosition> aspShipPosition = new ArrayList<ASPShipPosition>();
		aspShipPosition = aspShipPositionRepository.getShipPosition(imoNo, startDate, endDate);
		return aspShipPosition;
	}

	@Override
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfFlagToIde(Date startDate, Date endDate) {
		Collection<VwGeographicalAreaUpdate> geographicalAreaToIdeList = new ArrayList<VwGeographicalAreaUpdate>();
		geographicalAreaToIdeList = vwGeographicalAreaUpdateRepository.getGeographicalAreaOfFlagToIde(startDate, endDate);
		return geographicalAreaToIdeList;
	}

	@Override
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfOtherToIde(String lritId, Date startDate, Date endDate) {
		Collection<VwGeographicalAreaUpdate> geographicalAreaToIdeList = new ArrayList<VwGeographicalAreaUpdate>();
		geographicalAreaToIdeList = vwGeographicalAreaUpdateRepository.getGeographicalAreaOfOtherToIde(lritId, startDate, endDate);
		return geographicalAreaToIdeList;
	}

	@Override
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfFlagToDdp(Date startDate, Date endDate) {
		Collection<VwGeographicalAreaUpdate> geographicalAreaToDdpList = new ArrayList<VwGeographicalAreaUpdate>();
		geographicalAreaToDdpList = vwGeographicalAreaUpdateRepository.getGeographicalAreaOfFlagToDdp(startDate, endDate);
		return geographicalAreaToDdpList;
	}

	@Override
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfOtherToDdp(String lritId, Date startDate,
			Date endDate) {
		Collection<VwGeographicalAreaUpdate> geographicalAreaToDdpList = new ArrayList<VwGeographicalAreaUpdate>();
		geographicalAreaToDdpList = vwGeographicalAreaUpdateRepository.getGeographicalAreaOfOtherToDdp(lritId, startDate, endDate);
		return geographicalAreaToDdpList;
	}
	
	
	  @Override public Collection<VwStandingOrder> getOpenedStandingOrder(Date
	  startDate, Date endDate) { Collection<VwStandingOrder> openStandingOrders =
	  new ArrayList<VwStandingOrder>(); openStandingOrders =
	  vwStandingOrderRepository.getOpenedStandingOrder(startDate, endDate); return
	  openStandingOrders; }
	 
	
	  @Override public Map<String, Collection<String>> getExcludedValues(BigInteger
	  soId, Date startDate, Date endDate) { Map<String, Collection<String>>
	  excludedValMap = new HashMap<String, Collection<String>>(); //Excluded area
	  Collection<String> excludedAreaList = new ArrayList<String>();
	  excludedAreaList = vwStandingOrderRepository.getExcludedArea(soId, startDate,
	  endDate); //Excluded country 
	  Collection<String> excludedCountriesList = new
	  ArrayList<String>(); excludedCountriesList =
	  vwStandingOrderRepository.getExcludedCountries(soId, startDate, endDate);
	  //excluded vessels 
	  Collection<String> excludedVesselsList = new
	  ArrayList<String>(); excludedVesselsList =
	  vwStandingOrderRepository.getExcludedVessels(soId, startDate, endDate);
	  
	  
	  excludedValMap.put("ExcludedAreas", excludedAreaList);
	  excludedValMap.put("ExcludedCountries", excludedCountriesList);
	  excludedValMap.put("ExcludedVessels", excludedVesselsList);
	  
	  return excludedValMap;
	  
	  }
	 

	@Override
	public Collection<CspLog> getCspLogOfFlagVessel(Date startDate, Date endDate){
		Collection<CspLog> cspLogList = new ArrayList<CspLog>();
		cspLogList = cspLogRepository.getCspLogOfFlagVessel(startDate, endDate);
		return  cspLogList;
	}

	@Override
	public Collection<CspLog> getCspLogofNonflagVessel(String dataUserProvider, Date startDate, Date endDate){
		Collection<CspLog> cspNonflagLogList = new ArrayList<CspLog>();
		cspNonflagLogList = cspLogRepository.getCspLogOfFlagVessel(startDate, endDate);
		return  cspNonflagLogList;
	}

	@Override
	public Collection<DdpLog> getDdpRequestLogs(Date startDate, Date endDate) {
		Collection<DdpLog> ddpLogList = new ArrayList<DdpLog>();
		ddpLogList = ddpLogRepository.getDdpRequestLogs(startDate, endDate);
		return  ddpLogList;
	}

	@Override
	public String getPayloadAgainstMessageId(String messageId) {
		String payload = ddpLogRepository.getPayloadAgainstMessageId(messageId);
		return payload;
	}

	@Override
	public Collection<DdpLog> getDdpUpdateLogs(Date startDate, Date endDate) {
		Collection<DdpLog> ddpUpdateLogList = new ArrayList<DdpLog>();
		ddpUpdateLogList = ddpLogRepository.getDdpUpdateLogs(startDate, endDate);
		return  ddpUpdateLogList;
	}

	@Override
	public Collection<SystemStatus> getDdpSystemStatusLogs(Date startDate, Date endDate) {
		Collection<SystemStatus> ddpSystemStatusLogList = new ArrayList<SystemStatus>();
		ddpSystemStatusLogList = systemStatusRepository.getDdpSystemStatusLogs(startDate, endDate);
		return  ddpSystemStatusLogList;
	}

	@Override
	public Collection<AspLogInterface> getAspReceiptLog(Date startDate, Date endDate) {
		Collection<AspLogInterface> aspReceiptLogList = new ArrayList<AspLogInterface>();
		aspReceiptLogList = ddpLogRepository.getAspReceiptLog(startDate, endDate);
		return aspReceiptLogList;
	}

	@Override
	public Collection<AspLogInterface> getAspPositionRequestLog(Date startDate, Date endDate) {
		Collection<AspLogInterface> aspPositionRequestLogList = new ArrayList<AspLogInterface>();
		aspPositionRequestLogList = ddpLogRepository.getAspPositionRequestLog(startDate, endDate);
		return aspPositionRequestLogList;
	}

	@Override
	public Collection<AspLogInterface> getAspPositionReportLog(Date startDate, Date endDate) {
		Collection<AspLogInterface> aspPositionReportLogList = new ArrayList<AspLogInterface>();
		aspPositionReportLogList = ddpLogRepository.getAspPositionReportLog(startDate, endDate);
		return aspPositionReportLogList;
	}

	@Override
	public Collection<VwShipStatusLatestPositionDto> getLatestPositionOfFlagVessel(String vesselStatus) {
		Collection<VwShipStatusLatestPositionDto> flagVesselList = new ArrayList<VwShipStatusLatestPositionDto>();
		flagVesselList = vwShipStatusRepository.getLatestPositionOfFlagVessel(vesselStatus);
		return flagVesselList;
	}

	@Override
	public Collection<VwShipStatusLatestPositionDto> getAllFlagShipsLatestPosition() {
		Collection<VwShipStatusLatestPositionDto> flagVesselList = new ArrayList<VwShipStatusLatestPositionDto>();
		flagVesselList = vwShipStatusRepository.getAllFlagShipsLatestPosition();
		return flagVesselList;
	}
}
