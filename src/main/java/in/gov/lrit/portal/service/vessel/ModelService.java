package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.Optional;

import in.gov.lrit.portal.model.vessel.PortalModelDetail;

public interface ModelService {
	
 public PortalModelDetail addModel(PortalModelDetail mod);
 
 public boolean isModelExist(String modelType, Integer manufacturerId, String modelName);
 
 public boolean updateModelById(Integer modelId);
 
 public Iterable<PortalModelDetail> getByManuId(Integer manuId);
 
 public Iterable<PortalModelDetail> getByManuIdAndStatus(Integer manuId);
 
 public boolean updateModelByManuId(Integer manuId);
 
 public boolean ifNoModelExistForManufacturerName(Integer manuId);
 
 public Optional<PortalModelDetail> getById(Integer modelId);
 
 public ArrayList<String> getModelTypeByManufacturer(Integer manufacturerId);
 
 public ArrayList<String> modelListbymodelTypeAndManufacturerId(String modelType, Integer manufacturerId);
 
 public PortalModelDetail getModelByManufacturerTypeName(String modelType, Integer manufacturerId, String modelName);
 
 public ArrayList<PortalModelDetail> findmodelListbymodelTypeAndManufacturerIdAndStatus(String modelType, Integer manufacturerId);

}