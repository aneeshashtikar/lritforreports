package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.CSODPADetailsRepository;
import in.gov.lrit.portal.model.vessel.PortalCSODPADetails;

@Service
public class CSODPADetailsServiceImpl implements CSODPADetailsService{

	@Autowired
	private CSODPADetailsRepository csodpaRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(CSODPADetailsServiceImpl.class);

	@Override
	public List<PortalCSODPADetails> getCSOByportalUser_loginIdAndType(String loginId, String type) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findCSOByportalUser_loginIdAndType(loginId, type);
	}
	
	@Override
	public List<PortalCSODPADetails> getDPAByportalUser_loginIdAndType(String loginId, String type) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findDPAByportalUser_loginIdAndType(loginId, type);
	}

	@Override
	public List<PortalCSODPADetails> getACSOByportalUser_loginIdAndLritCsoIdSeq(String loginId, String lritCsoIdSeq) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findACSOByportalUser_loginIdAndLritCsoIdSeq(loginId, lritCsoIdSeq);
	}
	
	/*@Override
	public List<PortalCSODPADetails> getACSOByportalUser_loginId(String loginId) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findACSOByportalUser_loginId(loginId);
	}*/

	@Override
	public PortalCSODPADetails getByname(String name) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findByname(name);
	}

	@Override
	public Optional<PortalCSODPADetails> findBylritCsoIdSeq(String id) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findById(id);
	}

	/*@Override
	public Iterable<PortalCSODPADetails> getDPAByportalUser_loginIdAndType(String loginId, String type) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findDPAByportalUser_loginIdAndType(loginId, type);
	}*/

	/*@Override
	public Iterable<PortalCSODPADetails> getCDODPAByTypeAndLoginId(String loginId, String type) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findByportalUser_loginIdAndType(loginId, type);
				
	}*/

	

	/*@Override
	public ArrayList<String> getCurrentCsoListBycompanyCode(String companyCode) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findCurrentCsoListBycompanyCode(companyCode);
	}

	@Override
	public ArrayList<String> getAltCsoListBycompanyCode(String companyCode) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findAltCsoListBycompanyCode(companyCode);
	}

	@Override
	public ArrayList<String> getDPAListBycompanyCode(String companyCode) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findDPAListBycompanyCode(companyCode);
	}*/

	/*@Override
	public Map<String, String> getnameAndtypeBycompanyCode(String companyCode) {
		logger.info("Inside CSO DPA Service");
		return csodpaRepo.findnameAndtypeBycompanyCode(companyCode);
	}*/

	/*@Override
	public ArrayList<String> getCurrentCsoListBycompanyCode(String companyCode) {
		logger.info("Inside CSO DPA Service getAllNameByCompanyCode");
		List<CSOALTCSODPAName> listofcsodpa = csodpaRepo.findnameAndtypeBycompanyCode(companyCode);
		
		for (CSOALTCSODPAName csoaltcsodpaName : listofcsodpa) {
			System.out.println();
			System.out.println(csoaltcsodpaName.getName() + "" + csoaltcsodpaName.getType());
		}
		return csodpaRepo.findCurrentCsoListBycompanyCode(companyCode);
		return listofcsodpa;
	}
	*/
	
	/*
	public List<PortalCSODPADetails> getCSODPAList(String type,String companyCode){
		return csodpaRepo.getCSODPAList(type, companyCode);
		
		
	}*/

}