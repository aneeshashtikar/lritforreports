package in.gov.lrit.portal.service.contractinggovernment;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.ddp.model.LritContractingGovtMst;
/*import in.gov.lrit.portal.dao.user.contractingGovernmentRepository;*//*
import in.gov.lrit.portal.dao.user.PortalLritIdMasterHistoryDao;*/
/*import in.gov.lrit.portal.dao.user.PortalUserSarAuthorityDao;*/
/*import in.gov.lrit.portal.dao.user.UserManagementDao;*/
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMasterHistory;
import in.gov.lrit.portal.repository.contractinggovernment.ContractingGovernmentRepository;
import in.gov.lrit.portal.repository.contractinggovernment.PortalLritIdMasterHistoryRepository;
import in.gov.lrit.portal.repository.user.PortalUserSarAuthorityRepository;
import in.gov.lrit.portal.repository.user.UserManagementRepository;

@Service
public class CGManagementService implements  ICGManagementService {
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CGManagementService.class);
	
	@Autowired
	ContractingGovernmentRepository contractingGovernmentRepository;
	/*ContractingGovernmentRepository contractingGovernmentRepository;*/
	@Autowired
	UserManagementRepository usermanagementRepository;
	@Autowired
	PortalLritIdMasterHistoryRepository portalLritIdMasterHistoryRepository;
	@Autowired
	PortalUserSarAuthorityRepository portalUserSarAuthorityRepository;
/*	PortalUserSarAuthorityDao portalUserSarAuthorityDao;*/
	
	
	
	
	public void persistContractingGovernment(PortalLritIdMaster portalLritIdMaster) {

		logger.info("Inside persistContractingGovernment service method " + portalLritIdMaster.toString());
		PortalLritIdMaster portalLritIdMaster2 = contractingGovernmentRepository.save(portalLritIdMaster);
		logger.info("portalLritIdMaster2 " + portalLritIdMaster2.toString());
	}

	public List<PortalLritIdMaster> getContractingGovList() {
		logger.info("Inside getContractingGovList");
		List<PortalLritIdMaster> portalLritIdMastersList = new ArrayList<PortalLritIdMaster>();
		// fetch contracting gov having type as cg

		portalLritIdMastersList = contractingGovernmentRepository.getContratingGovernment();
		logger.debug("After contractingGovernmentRepository.findAll() " + portalLritIdMastersList.toString());
		return portalLritIdMastersList;
	}

	public boolean checkCountryExistance(String countryName) {
		boolean flag = contractingGovernmentRepository.checkCountryExistance(countryName);
		return flag;
	}

	public boolean checkCoastalAreaExistance(String coastalArea) {
		boolean flag = contractingGovernmentRepository.checkCoastalAreaExistance(coastalArea);
		return flag;
	}

	public boolean checkLritId(String lritId) {
		return contractingGovernmentRepository.checkLritId(lritId);
	}

	public PortalLritIdMaster getDetails(String lritId) {

		return contractingGovernmentRepository.getDetails(lritId);
	}

	public boolean getUser(String lritId) {
		logger.info("Inside getUser method ");

		return usermanagementRepository.getUser(lritId);
	}

	public void deleteCgSar(String countryName) {
		contractingGovernmentRepository.deleteCgSar(countryName);

	}

	public void deleteCg(String lritId) {
		contractingGovernmentRepository.deleteCg(lritId);
	}
	public List<PortalLritIdMaster> getSarList(String countryName) {
		return contractingGovernmentRepository.getSarList(countryName);
	}

	public List<PortalLritIdMaster> getSarAuthorityDetails(String countrylritId) {
		logger.info("Inside getSarAuthorityDetails service method " + countrylritId);
		List<PortalLritIdMaster> portalLritIdMasters = contractingGovernmentRepository.getSarAuthorities(countrylritId);
		logger.info("Out of getSarAuthorityDetails service method " + portalLritIdMasters.toString());
		return portalLritIdMasters;

	}

	public boolean checkExistenceSarLritId(String lritId) {
		return portalUserSarAuthorityRepository.checkUser(lritId);
	}

	public void deleteSar(String lritId) {
		contractingGovernmentRepository.deleteById(lritId);
	}

	public void saveCg(PortalLritIdMaster cgDetails) {

		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp ts = new Timestamp(time);
		PortalLritIdMasterHistory cgDetailHistory = new PortalLritIdMasterHistory();
		cgDetailHistory.setLritId(cgDetails.getLritId());
		cgDetailHistory.setCountryName(cgDetails.getCountryName());
		cgDetailHistory.setCountryCode(cgDetails.getCountryCode());
		cgDetailHistory.setCoastalArea(cgDetails.getCoastalArea());
		cgDetailHistory.setType(cgDetails.getType());
		cgDetailHistory.setRegDate(cgDetails.getRegDate());
		cgDetailHistory.setDeregDate(ts);

		portalLritIdMasterHistoryRepository.save(cgDetailHistory);

	}

	public void saveSar(List<PortalLritIdMaster> sarList) {

		for (PortalLritIdMaster sar : sarList) {
			Date Currentdate = new Date();
			long time = Currentdate.getTime();
			Timestamp ts = new Timestamp(time);
			PortalLritIdMasterHistory sarDetailHistory = new PortalLritIdMasterHistory();
			sarDetailHistory.setLritId(sar.getLritId());
			sarDetailHistory.setCountryName(sar.getCountryName());
			sarDetailHistory.setCountryCode(sar.getCountryCode());
			sarDetailHistory.setCoastalArea(sar.getCoastalArea());
			sarDetailHistory.setType(sar.getType());
			sarDetailHistory.setRegDate(sar.getRegDate());
			sarDetailHistory.setDeregDate(ts);
			portalLritIdMasterHistoryRepository.save(sarDetailHistory);
		}

	}
	public boolean checkCountryCode(String countryCode) {
		return contractingGovernmentRepository.checkCountryCodeExistance(countryCode);
	}

	@Override
	public List<String> getAllLritId() {
		// TODO Auto-generated method stub
		return contractingGovernmentRepository.getAllLritId();
	}

}
