package in.gov.lrit.portal.service.vessel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.model.vessel.PortalVesselReportingStatusHistory;
import in.gov.lrit.portal.repository.vessel.VesselReportingStatusHistoryRepository;

@Service
public class VesselReportingStatusHistoryServiceImpl implements VesselReportingStatusHistoryService{

	@Autowired
	private VesselReportingStatusHistoryRepository repo;
	
	Logger logger = LoggerFactory.getLogger(VesselReportingStatusHistoryServiceImpl.class);
	
	@Override
	public PortalVesselReportingStatusHistory addReportingStatusHst(PortalVesselReportingStatusHistory statushistory) {
		logger.info("Inside reporting status history service");
		return repo.save(statushistory);
	}

	
}
