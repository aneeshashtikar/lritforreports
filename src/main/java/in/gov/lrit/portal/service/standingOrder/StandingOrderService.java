package in.gov.lrit.portal.service.standingOrder;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.request.PortalRequest;
import in.gov.lrit.portal.model.standingOrder.SOCountriesExclusion;
import in.gov.lrit.portal.model.standingOrder.SOGeographicalAreas;
import in.gov.lrit.portal.model.standingOrder.SOVesselExclusion;
import in.gov.lrit.portal.model.standingOrder.standingOrderRequest;
import in.gov.lrit.portal.repository.requests.PortalRequestRepository;
import in.gov.lrit.portal.repository.standingOrder.SOCountriesExclusionRepository;
import in.gov.lrit.portal.repository.standingOrder.SOGeoAreaRepository;
import in.gov.lrit.portal.repository.standingOrder.SORequestRepository;
import in.gov.lrit.portal.repository.standingOrder.SOVesselExclusionRepository;
import in.gov.lrit.portal.service.request.IRequestmanagement;

@Service
@Transactional
public class StandingOrderService implements  IStandingOrderService {
	
	@Autowired
	IRequestmanagement manageRequest;
	@Autowired
	SORequestRepository soRequestRepo;
	@Autowired
	SOGeoAreaRepository soGeoAreasRepo;
	@Autowired
	SOCountriesExclusionRepository soCountryExlusionRepo;
	@Autowired
	SOVesselExclusionRepository soVesselExclusionRepo;
	@Autowired
	PortalRequestRepository prr;
	
	//private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(StandingOrderService.class);

	@Override
	public List<SOGeographicalAreas> getGeoArea(int soId) {
		//logger.info("Inside Delete Standing Order Service: " + soId);
		return soGeoAreasRepo.findBySOId(soId);
	}
	
	@Override
	public void deleteStandingOrder(int soId) {
		//logger.info("Inside Delete Standing Order Service: " + soId);
		soRequestRepo.deleteBySoId(soId);
	}

	@Override
	public List<SOCountriesExclusion> getExCoun(int SOId, String areacode) {
		
		return soCountryExlusionRepo.findBySOId(SOId, areacode);
	}

	@Override
	public List<SOVesselExclusion> getExShip(int SOId, String areacode) {
		// TODO Auto-generated method stub
		return soVesselExclusionRepo.findBySOId(SOId, areacode);
	}

	@Override
	public List<standingOrderRequest> getAllSO() {
		// TODO Auto-generated method stub
		return soRequestRepo.findAll();
	}

	@Override
	public List<standingOrderRequest> getSObyStatus(String status) {
		// TODO Auto-generated method stub
		return soRequestRepo.findBySOStatus(status);
	}

	@Override
	public void updateOpenedStatus(String status) {
		// TODO Auto-generated method stub
		soRequestRepo.updateOpenedStatus(status);
	}

	@Override
	public void updateStatus(int SOId, String status) {
		// TODO Auto-generated method stub
		soRequestRepo.updateByStatus(SOId, status);
	}

	@Override
	public standingOrderRequest saveSO(standingOrderRequest SORequest) {
		// TODO Auto-generated method stub
		return soRequestRepo.save(SORequest);
	}

	@Override
	public SOGeographicalAreas saveGeoArea(SOGeographicalAreas SOGeoArea) {
		// TODO Auto-generated method stub
		return soGeoAreasRepo.save(SOGeoArea);
	}

	@Override
	public SOVesselExclusion saveExVessel(SOVesselExclusion SOVesselEx) {
		// TODO Auto-generated method stub
		return soVesselExclusionRepo.save(SOVesselEx);
	}

	@Override
	public SOCountriesExclusion saveExCountry(SOCountriesExclusion SOCountryEx) {
		// TODO Auto-generated method stub
		return soCountryExlusionRepo.save(SOCountryEx);
	}

	@Override
	public List<String> getFlagedArea(int soId) {
		// TODO Auto-generated method stub
		return soGeoAreasRepo.getFlagedArea(soId);
	}

	@Override
	public List<String> getFlagedCoun(int soId, String areacode) {
		// TODO Auto-generated method stub
		return soCountryExlusionRepo.getFlagedCoun(soId, areacode);
	}

	@Override
	public standingOrderRequest getSO(int soId) {
		// TODO Auto-generated method stub
		return soRequestRepo.findBySOId(soId);
	}

	@Override
	public List<String> getFilterShips(int SOId, String areacode) {
		// TODO Auto-generated method stub
		return soVesselExclusionRepo.getFilterShips(SOId, areacode);
	}

	@Override
	public PortalRequest savePortalRequest(PortalRequest portalreq) {
		// TODO Auto-generated method stub
		return prr.save(portalreq);
	}

	@Override
	public void deletePortalRequest(String msgid) {
		// TODO Auto-generated method stub
		prr.deleteByMessageId(msgid);
	}

	@Override
	public void deleteGeoArea(int soId) {
		// TODO Auto-generated method stub
		soGeoAreasRepo.deleteBySoId(soId);
	}

	@Override
	public void deleteExCountries(int soId) {
		// TODO Auto-generated method stub
		soCountryExlusionRepo.deleteBySoId(soId);
	}

	@Override
	public void deleteExShip(int soId) {
		// TODO Auto-generated method stub
		soVesselExclusionRepo.deleteBySoId(soId);
	}
}
