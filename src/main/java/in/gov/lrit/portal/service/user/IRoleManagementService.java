package in.gov.lrit.portal.service.user;

import java.util.List;

import in.gov.lrit.portal.model.role.PortalActivities;
import in.gov.lrit.portal.model.role.PortalRoles;

public interface IRoleManagementService {
public List<PortalActivities> getActivities();
	
	//Related to Roles
	public void saveRole(PortalRoles portalRoles);
	public List<PortalRoles> getRoles(String status);
	public void deregisterRole(String roleId);
	public PortalRoles getRoleDetails(String roleId);
	public void editRole(PortalRoles portalRoles);
	public boolean checkRoleName(String roleName);
	public boolean checkShorName(String shortName);
	public List<PortalRoles> getRoleList(List<String> roleIdList);;
}
