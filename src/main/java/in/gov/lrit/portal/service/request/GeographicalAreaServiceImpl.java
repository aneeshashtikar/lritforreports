package in.gov.lrit.portal.service.request;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

import javax.activation.DataHandler;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.imo.gisis.xml.lrit._2008.Response;
import org.imo.gisis.xml.lrit.geographicalareaupdate._2014.GeographicalAreaUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.gis.ddp.model.DdpVersion;
import in.gov.lrit.portal.CommonUtil.ICommonUtil;
import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaModel;
import in.gov.lrit.portal.gis.lritdb.repository.GMLAreaCodeMappingRepository;
import in.gov.lrit.portal.gis.lritdb.repository.GeographicAreaRepository;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.request.PortalGeoraphicalAreaRequest;
import in.gov.lrit.portal.model.request.PortalRequest;
import in.gov.lrit.portal.repository.requests.GeographicalAreaRepository;
import in.gov.lrit.portal.repository.requests.GeographicalAreaRequestRepository;
import in_.gov.lrit.session.UserSessionInterface;
import responses.portalresponse.PortalResponse;

@Service
public class GeographicalAreaServiceImpl implements GeographicalAreaService {

	@Autowired
	private GeographicalAreaRepository geographicalAreaRepository;
	
	@Autowired
	private GeographicalAreaRequestRepository geoAreaRequestRepository;
	
	@Autowired
	private GeographicAreaRepository geographicAreaRepo;
	
	@Autowired
	private GMLAreaCodeMappingRepository gmlAreaCodesRepo;
	
	@Autowired
	ICommonUtil commonutil;
	
	@Autowired
	SoapConnector sc;
	
	@Autowired
	private IDdpMessagingManagement so;
	
	@Autowired
	private IRequestmanagement manageRequest;
	
	@Value("${lrit.cgid}")
	private String cgid; 
	
	@Value("${lrit.current_ddpversion_no}")
	private String current_ddpversion_no;
	
	

	
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GeographicalAreaServiceImpl.class);
	
	@Override
	public List<GeographicAreaModel> showGeographicalAreaList(String geoStatus) {
		// TODO Auto-generated method stub
		logger.info("inside showGeographicalAreaList");
		/* List<PortalGeoraphicalArea> geoArea=geographicalAreaRepository.findAll(); */
		//List<PortalGeoraphicalArea> geoArea=geographicalAreaRepository.findAllByStatus(geoStatus);
		List<GeographicAreaModel> geoArea =geographicAreaRepo.findByStatus(geoStatus);
		return geoArea;
	}

	@Override
	public String geographcalAreaUpdate(int gmlId, Integer actionType) throws IOException {
		// TODO Auto-generated method stub
		//PortalGeoraphicalArea portalGeoraphicalArea = geographicalAreaRepository.findByAreaCode(areaCode);
		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes(); //get data from session
		HttpSession session =attr.getRequest().getSession();
		UserSessionInterface userSession =	(UserSessionInterface) session.getAttribute("USER"); 
		String	loginId=userSession.getLoginId();

		PortalLritIdMaster lritIdmaster=userSession.getRequestorsLritId(); //get  lritid from session 
		String requestorLritId=lritIdmaster.getLritId();
		
		
		GeographicAreaModel geoArea = geographicAreaRepo.findByGmlId(gmlId);
		logger.info("selected Geographical Area" + geoArea);
		List<String> areaCodes=gmlAreaCodesRepo.getAreaCodes(gmlId);

		String message=null;
		DataHandler gmlFile1 = null;
		String status=null;
		
		Timestamp currentTimestamp=	commonutil.getCurrentTimeStamp();
		XMLGregorianCalendar gregorianTimeStamp=commonutil.getgregorianTimeStamp(currentTimestamp);
		String messageId=commonutil.generateMessageID(cgid);
		
		//update current ddp version code
		
		/***
		 * New DDP changes
		 * 
		 */
		List<DdpVersion> ddpversion = so.getDdpVersionview();
		String ddpversionfinal = null;
		for (int j = 0; j < ddpversion.size(); j++)
		{
			ddpversionfinal = ((DdpVersion)ddpversion.get(j)).getDdpversion();
		}
		
		
		//String currentDdpVersion=commonutil.getcurrentddpversiondate(current_ddpversion_no);
		String schemaVersionconfigPara = commonutil.getParavalues("Schema_Version");
		BigDecimal schemaVersion = new BigDecimal((schemaVersionconfigPara));
		
		PortalGeoraphicalAreaRequest portalGeoraphicalAreaRequest=new PortalGeoraphicalAreaRequest();
		PortalRequest portalRequest =new PortalRequest();
		portalRequest.setCurrentddpversion(ddpversionfinal);
		portalRequest.setMessageId(messageId);
		portalRequest.setRequestCategory("Geographical Area Update");
		portalRequest.setRequestGenerationTime(currentTimestamp);
		//Get this login id from session
		portalRequest.setRequestorsLoginId(loginId);
		//portalRequest.setRequestPayload(requestPayload);
		portalRequest.setStatus("submitted");
		portalRequest.setStatusDate(currentTimestamp);
		portalGeoraphicalAreaRequest.setPortalRequest(portalRequest);
		portalGeoraphicalAreaRequest.setGmlId(gmlId);
		//portalGeoraphicalAreaRequest.setAreaCode(areaCode);
		if(actionType==0 ||actionType ==1)
		{
		//code to get byte array
		byte[] gmlFile=	geoArea.getUploaded_file();
		 //byte[] gmlFile = java.nio.file.Files.readAllBytes( FileSystems.getDefault().getPath("C:\\Users\\nicsi\\Desktop\\Db.zip"));
		 portalGeoraphicalAreaRequest.setActualFile(gmlFile);
		 ByteArrayDataSource rawData= new ByteArrayDataSource(gmlFile); 
		 gmlFile1= new DataHandler(rawData);
		 
		}
		else
		{
			
			portalGeoraphicalAreaRequest.setActualFile(null);
		}
		BigInteger messageType=BigInteger.valueOf(16); 
		//For Testing test=1, or test =0
		String testconfigPara = commonutil.getParavalues("Test");
		BigInteger test=new BigInteger((testconfigPara));
		BigInteger actionTypedc=BigInteger.valueOf(actionType);
		//  byte[] gmlfile= portalGeoraphicalArea.getUploadedFilePath();
		
		//DC Geographical Area update Bean
		GeographicalAreaUpdate dcga=new GeographicalAreaUpdate();
		//get the data user request from session
		dcga.setDataUserRequestor(requestorLritId);
		dcga.setDDPVersionNum(ddpversionfinal);
		dcga.setGMLFile(gmlFile1);
		dcga.setMessageId(messageId);
		dcga.setMessageType(messageType);
		dcga.setSchemaVersion(schemaVersion);
		dcga.setTest(test);
		dcga.setTimeStamp(gregorianTimeStamp);
		dcga.setActionType(actionTypedc);
		
		//areaCodes.forEach{ String areaCode -> dcga.getAreaIDs().add(areaCode)}
		//String areaCode;
		//areaCodes.forEach(areaCode -> dcga.getAreaIDs().add(areaCode));
		for (String areaCode: areaCodes){
			dcga.getAreaIDs().add(areaCode);
		}
		
	//	dcga.getAreaIDs().add(areaCode);
		
		logger.info("the packet created to send to Dc" + dcga.toString());
		//This is to print the request payload in console
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(GeographicalAreaUpdate.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(dcga, sw);
			logger.info("payload:"+sw.toString());	
			portalRequest.setRequestpayload(sw.toString());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message="Error Occurred While proessing the request for messageid :" +messageId;
			return message;
		}
		/*
		 * Saving the request in portal DB
		 */
		logger.info("Saving in portal geo area request table");
		geoAreaRequestRepository.save(portalGeoraphicalAreaRequest);

		try 
		{
			PortalResponse response=sc.callWebService( dcga);
			
			if ((response.isSuccessful()))
			{
				if(actionType==0)
					status="sent_to_ide";
				else if(actionType==1)
					status="sent_to_ddp";
				else
					status= "sent_to_delete_to_ddp";
				//geographicalAreaRepository.updateGeoAreaStatus(areaCode, status);
				geographicAreaRepo.updateGeoAreaStatus(gmlId, status);
				logger.info("Geographical Area Status Updated successfully" +status);
				manageRequest.setResponsePayload("success", messageId);
				message= "Geographical Area Update Request sent successfully for messageid : "+messageId;
					return message;
			}
			else
			{
				manageRequest.setResponsePayload(response.getMessage().toString(), messageId);
				message="Error Occurred While Processing the request for messageid :" +messageId;
				return message;
			}
		
		}
		catch(Exception e)
		{
			manageRequest.setResponsePayload("SoapFault", messageId);
			e.getStackTrace();
			message="Error Occurred While Processing the request for messageid :" +messageId;
			return message;
		}
	}

	@Override
	public int deleteGeoArea(int gmlId) {
		// TODO Auto-generated method stub
		//int result=geographicAreaRepo.deleteGeoArea(gmlId);
		int result=geographicAreaRepo.updateGeoAreaStatus(gmlId,"deleted");
		//int result = geographicalAreaRepository.deleteGeoArea(areaCode);
		return result;
	}

}
