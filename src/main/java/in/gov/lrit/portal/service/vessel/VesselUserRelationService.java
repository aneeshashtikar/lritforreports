package in.gov.lrit.portal.service.vessel;

import java.util.List;

import in.gov.lrit.portal.model.vessel.PortalShippingCompanyVesselRelation;

public interface VesselUserRelationService {

	public PortalShippingCompanyVesselRelation addMapping(PortalShippingCompanyVesselRelation pscvr);
	
	public Iterable<PortalShippingCompanyVesselRelation> addAllMapping(Iterable<PortalShippingCompanyVesselRelation> pscvrlist);
	
	public List<PortalShippingCompanyVesselRelation> getBycustomId_vesselDet_vesselId(Integer vesselId);
}