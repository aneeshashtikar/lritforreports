package in.gov.lrit.portal.service.standingOrder;

import java.util.List;

import in.gov.lrit.portal.model.request.PortalRequest;
import in.gov.lrit.portal.model.standingOrder.SOCountriesExclusion;
import in.gov.lrit.portal.model.standingOrder.SOGeographicalAreas;
import in.gov.lrit.portal.model.standingOrder.SOVesselExclusion;
import in.gov.lrit.portal.model.standingOrder.standingOrderRequest;

public interface IStandingOrderService {
	
	PortalRequest savePortalRequest(PortalRequest portalreq);
	void deletePortalRequest(String msgid);
	
	/*
	 * Standing Order Repository Functions
	 */
	List<standingOrderRequest> getAllSO();
	List<standingOrderRequest> getSObyStatus(String status);
	standingOrderRequest getSO(int soId);
	void updateOpenedStatus(String status);
	void updateStatus(int SOId, String status);
	standingOrderRequest saveSO(standingOrderRequest SORequest);
	void deleteStandingOrder(int soId);
	
	
	/*
	 * Geographical Area Repository Functions
	 */
	List<SOGeographicalAreas> getGeoArea(int soId);
	List<String> getFlagedArea(int soId);
	SOGeographicalAreas saveGeoArea(SOGeographicalAreas SOGeoArea);
	void deleteGeoArea(int soId);
		
	
	/*
	 * Countries Exclusion Repository Functions
	 */
	List<SOCountriesExclusion> getExCoun(int SOId, String areacode);
	List<String> getFlagedCoun(int soId, String areacode);
	SOCountriesExclusion saveExCountry(SOCountriesExclusion SOCountryEx);
	void deleteExCountries(int soId);
	
	
	/*
	 * Vessel Exclusion Repository Functions
	 */
	List<SOVesselExclusion> getExShip(int SOId, String areacode);
	List<String> getFilterShips(int SOId, String areacode);
	SOVesselExclusion saveExVessel(SOVesselExclusion SOVesselEx);
	void deleteExShip(int soId);
	
}
