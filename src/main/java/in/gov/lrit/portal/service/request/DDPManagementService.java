package in.gov.lrit.portal.service.request;



import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.gis.ddp.repository.ContractingGovtMasterRepository;
import in.gov.lrit.gis.ddp.repository.DdpHistoryRepository;
import in.gov.lrit.gis.ddp.repository.DdpVersionRepository;
import in.gov.lrit.gis.ddp.repository.PolygonRepository;
import in.gov.lrit.gis.ddp.repository.PortDetailsRepository;
import in.gov.lrit.gis.ddp.repository.PortFacilityRepository;
import in.gov.lrit.gis.ddp.repository.SOFetchRepository;
import in.gov.lrit.gis.ddp.model.DdpVersion;
import in.gov.lrit.gis.ddp.model.LritContractGovtMst;
import in.gov.lrit.gis.ddp.model.LritPolygon;
import in.gov.lrit.gis.ddp.model.LritPort;
import in.gov.lrit.gis.ddp.model.LritPortFacilityType;

import in.gov.lrit.portal.model.request.DcPositionReport;
import in.gov.lrit.portal.model.request.DcSarsurpicrequest;
import in.gov.lrit.portal.repository.requests.DcDdpNotificationRepository;
import in.gov.lrit.portal.repository.requests.DcPositionReportRepository;
import in.gov.lrit.portal.repository.requests.DcSarsurpicRepository;

@Service
public class DDPManagementService implements IDdpMessagingManagement {

	@Autowired
	SOFetchRepository so;

	@Autowired
	PolygonRepository pr;
	@Autowired
	PortDetailsRepository pd;

	@Autowired
	PortFacilityRepository pf;
	@Autowired
	DdpVersionRepository ddp;

	@Autowired
	DcSarsurpicRepository sar;
	@Autowired
	ContractingGovtMasterRepository cg;

	@Autowired
	DcDdpNotificationRepository dcddp;

	@Autowired
	DdpHistoryRepository ddphst;

	@Autowired
	DcPositionReportRepository dcposition;
	LritPolygon sodetails=null;
	@Override
	public LritPolygon getCountryPolygon(String cgid,String regularversion) {
		// TODO Auto-generated method stub

		sodetails=pr.findBycgLritidAndRegularversionNo(cgid,regularversion);
		return sodetails;
	}

	@Override
	public List<LritPort> getPortList(String cgid, String regularVersion) {
		// TODO Auto-generated method stub

		List<LritPort> portdetail=pd.findBycgLritidAndRegularversionNo(cgid, regularVersion);
		return portdetail;
	}

	@Override
	public List<LritPortFacilityType> getPortFacilityList(String cgid, String regularVersion) {
		// TODO Auto-generated method stub
		List<LritPortFacilityType> portdetail=pf.findBycgLritidAndRegularversionNo(cgid, regularVersion);

		return portdetail;
	}

	@Override
	public List<DdpVersion> getDdpVersionview() {

		//DdpVersion regularversion=ddp.findByapplicableregularversion();
		List<DdpVersion> regularversion=ddp.findAll();
		return regularversion;
	}

	@Override
	public List<LritContractGovtMst> getCountrylist(String regularVersion) {
		// TODO Auto-generated method stub
		List<LritContractGovtMst> countrylist=cg.findByCgName(regularVersion);
		return countrylist;
	}

	@Override
	public List<DcSarsurpicrequest> getSurpicarea(int accesstype) {
		// TODO Auto-generated method stub
		List<DcSarsurpicrequest> surpicarea=sar.findbyMessageIdAndAccessType(accesstype);

		return surpicarea;
	}

	@Override
	public List<DcPositionReport> getShipsinSurpic(List<String>  referenceid) {
		// TODO Auto-generated method stub
		List<DcPositionReport> shipsinsurpic=dcposition.findbyReferenceId(referenceid);
		return shipsinsurpic;
	}

	@Override
	public String getDdpNotificationMessageid() {
		// TODO Auto-generated method stub
		String messageid=dcddp.findbyMessageId();
		return messageid;
	}

	@Override
	public Timestamp getArchivedDdpversionTimestamp(String ddpversion) {
		// TODO Auto-generated method stub
		Timestamp arrchivedversiontimestamp=ddphst.findByUpdatedVersion(ddpversion);
		return arrchivedversiontimestamp;
	}

	@Override
	public String getDataUserProviderlritid(String datauserprovidercountry) {
		// TODO Auto-generated method stub
		String datauserproviderlritid=cg.findByCgLritid(datauserprovidercountry);
		return datauserproviderlritid;
	}





}
