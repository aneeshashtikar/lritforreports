package in.gov.lrit.portal.service.vessel;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import in.gov.lrit.portal.model.vessel.ManufacturerList;
import in.gov.lrit.portal.model.vessel.PortalManufacturerDetails;

public interface ManufacturerDetailsService {

	public PortalManufacturerDetails addManufacturer(PortalManufacturerDetails manu);
	
	public Iterable<PortalManufacturerDetails> getAllManufacturer();
	
	public boolean isManufacturerExist(String manufacturerName);
	
	public boolean updateManuById(Integer manufacturerId);
	
	public Optional<PortalManufacturerDetails> findById(Integer manufacturerId);
	
	public ArrayList<String> getmanufacturerName();
	
	public PortalManufacturerDetails getBymanufacturerName(String manufacturerName);
	
	public List<ManufacturerList> getAll();

}