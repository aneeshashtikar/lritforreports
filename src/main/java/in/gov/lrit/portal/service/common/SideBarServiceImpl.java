package in.gov.lrit.portal.service.common;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.repository.common.AspTerminalFrequencyRepository;
import in.gov.lrit.portal.repository.vessel.VesselDetailRepository;
import in_.gov.lrit.session.UserSessionInterface;

@Service
public class SideBarServiceImpl implements SideBarService {

	@Autowired
	private AspTerminalFrequencyRepository aspTerminalFrequency;

	@Autowired
	private VesselDetailRepository vesselDetailRepository;

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SideBarServiceImpl.class);

	@Override
	public List<Object[]> getFlagShipCountByReportingStatus() {
		logger.info("inside service getting the vessel count ");
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface user = (UserSessionInterface) session.getAttribute("USER");
		String category = (String) user.getCategory();
		logger.info("logged in user category is : " + category);
		List<Object[]> listOfReportingStatus = null;
		if (category.equals("USER_CATEGORY_SC")) {
			String loginId = user.getLoginId();
			listOfReportingStatus = vesselDetailRepository.getVesselStatusCountByLoginIdfromVW(loginId);
		} else {
			PortalLritIdMaster lritIdMaster = user.getRequestorsLritId();
			String lritId = lritIdMaster.getLritId();
			listOfReportingStatus = vesselDetailRepository.getVesselStatusCountByLritIdfromVW(lritId);
		}
		return listOfReportingStatus;
	}

	@Override
	public List<VesselDetailInterface> getVesselDetailListFromVWByVesselStatus(String vesselStatus) {
		// TODO Auto-generated method stub
		logger.info("Getting vessel Detail list from view by Reporting status for vessel status" + vesselStatus);
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface user = (UserSessionInterface) session.getAttribute("USER");
		
		String category = (String) user.getCategory();
		logger.info("logged in user category is : " + category);
		List<VesselDetailInterface>  vesselDetailList = null;
		if (category.equals("USER_CATEGORY_SC")) {
			String loginId = user.getLoginId();
			if(vesselStatus.equals("InactiveShip"))
				vesselDetailList=vesselDetailRepository.getInactiveShipDetailsbyLoginId(loginId);
			else
				vesselDetailList = vesselDetailRepository.getVesselDetailFromVWByVesselStatusAndLoginId(vesselStatus,loginId);
		} else {
			PortalLritIdMaster lritIdMaster = user.getRequestorsLritId();
			String lritId = lritIdMaster.getLritId();
			if(vesselStatus.equals("InactiveShip"))
				vesselDetailList= vesselDetailRepository.getInactiveShipDetailsbyLritId(lritId);
			else
				vesselDetailList = vesselDetailRepository.getVesselDetailFromVWByVesselStatusAndLritId(vesselStatus,lritId);
		}
		return vesselDetailList;
	}

	@Override
	public List<Object[]> getAspTerminalFrequencyCountFromVW() {
		// TODO Auto-generated method stub
		logger.info("inside service getting the vessel count by frequency");
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface user = (UserSessionInterface) session.getAttribute("USER");
		String category = (String) user.getCategory();
		logger.info("logged in user category is : " + category);
		List<Object[]> listByReportingStatus = null;
		if (category.equals("USER_CATEGORY_SC")) {
			String loginId = user.getLoginId();
			listByReportingStatus = vesselDetailRepository.getFrequencyCountByLoginIdfromVW(loginId);
		} else {
			PortalLritIdMaster lritIdMaster = user.getRequestorsLritId();
			String lritId = lritIdMaster.getLritId();
			listByReportingStatus = vesselDetailRepository.getFrequencyCountByLritIdfromVW(lritId);
		}
		return listByReportingStatus;
	}

	@Override
	public List<VesselDetailInterface> getVesselDetailListFromVWByfreqRate(Integer frequecyRate) {
		// TODO Auto-generated method stub
		logger.info("Inside service getting vessel list by frequency rate");
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface user = (UserSessionInterface) session.getAttribute("USER");
		
		String category = (String) user.getCategory();
		logger.info("logged in user category is : " + category);
		List<VesselDetailInterface>  vesselDetailList = null;
		if (category.equals("USER_CATEGORY_SC")) {
			String loginId = user.getLoginId();
			vesselDetailList = vesselDetailRepository.getVesselDetailFromVWByFrequencyAndLoginId(frequecyRate,loginId);
		} else {
			PortalLritIdMaster lritIdMaster = user.getRequestorsLritId();
			String lritId = lritIdMaster.getLritId();
			vesselDetailList = vesselDetailRepository.getVesselDetailFromVWByFrequencyAndLritId(frequecyRate,lritId);
		}
		return vesselDetailList;
	}

	public int getInactiveShipCount()
	{
		logger.info("inside service getting inactive ship count");
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		UserSessionInterface user = (UserSessionInterface) session.getAttribute("USER");
		String category = (String) user.getCategory();
		int count;
		logger.info("logged in user category is : " + category);
		if (category.equals("USER_CATEGORY_SC")) {
			String loginId = user.getLoginId();
			count =vesselDetailRepository.getInactiveShipCountbyLoginId(loginId);
		} else {
			PortalLritIdMaster lritIdMaster = user.getRequestorsLritId();
			String lritId = lritIdMaster.getLritId();
			count =vesselDetailRepository.getInactiveShipCountbyLritId(lritId);
		}
		return count;
	}
}
