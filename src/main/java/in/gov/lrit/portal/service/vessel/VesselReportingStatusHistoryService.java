package in.gov.lrit.portal.service.vessel;

import in.gov.lrit.portal.model.vessel.PortalVesselReportingStatusHistory;

public interface VesselReportingStatusHistoryService {

	public PortalVesselReportingStatusHistory addReportingStatusHst(PortalVesselReportingStatusHistory statushistory);
}
