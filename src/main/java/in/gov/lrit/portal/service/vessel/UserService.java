package in.gov.lrit.portal.service.vessel;

import java.util.List;
import java.util.Optional;

import in.gov.lrit.portal.model.user.PortalUser;

public interface UserService {

	public List<PortalUser> getBycompanyCode(String companyCode);
	
	public Optional<PortalUser> getbyloginId(String loginId);
	
	public PortalUser getByname(String name);
	
	public PortalUser getUserBycompanyCodeAndRelation(String companyCode);
}