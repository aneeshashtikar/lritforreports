package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import in.gov.lrit.portal.model.vessel.PortalVesselDetail;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.model.vessel.VesselListInterface;

public interface VesselService {

	public Optional<PortalVesselDetail> getByVesselId(int vesselId);
	
	public PortalVesselDetail addVessel(PortalVesselDetail pvd);
	
	public Iterable<PortalVesselDetail> getAllVessel();

	public PortalVesselDetail getVesselByImoNo(String imoNo);
	
	public Integer getVesselIdByImoNo(String imoNo);
	
	public ArrayList<VesselListInterface> getByRegStatusAndStatus(String regStatus, String status);
	
	public ArrayList<VesselListInterface> getByStatus(String status);
	
	public ArrayList<PortalVesselDetail> getVesselForCompanyByLoginId(String loginId);
	
	public ArrayList<VesselListInterface> getByRegStatus();
	
	public ArrayList<VesselListInterface> getAllVesselList();
	
	public ArrayList<PortalVesselDetail> getVesselForCompany(String companyCode);
	
	public ArrayList<PortalVesselDetail> getVesselForCompanyByStatus(String companyCode, String status);
	
	/*public ArrayList<PortalVesselDetail> getVesselForCompanyByRegStatus(String companyCode);*/
	public ArrayList<VesselListInterface> getVesselForCompanyByRegStatus(String companyCode);
	
	public ArrayList<VesselListInterface> getVesselForCompanyByRegStatusAndLoginId(String loginId);
	
	/*public ArrayList<PortalVesselDetail> getVesselForCompanyByStatusAndRegStatus(String companyCode, String status, String regStatus);*/
	public ArrayList<VesselListInterface> getVesselForCompanyByStatusAndRegStatus(String companyCode, String status, String regStatus);
	
	public ArrayList<VesselListInterface> getVesselForCompanyByStatusAndRegStatusAndLoginId(String loginId, String status, String regStatus);
	
	public ArrayList<VesselListInterface> getAllActiveVessel(String status, List<String> regStatus);
	
	public ArrayList<VesselListInterface> getAllActiveVesselForCompany(String companyCode, String status, List<String> regStatus);
	
	public ArrayList<VesselListInterface> getAllActiveVesselForCompanyByLoginId(String loginId, String status, List<String> regStatus);
	
	public ArrayList<VesselListInterface> getRepurchaseVesselList(String companyCode);
	
	/*public ArrayList<VesselListInterface> getRepurchaseVesselListByLoginId(String loginId);*/
	
	public boolean updateVesselRegStatus(String regStatus, Integer vesselId);
	
	public boolean updateVesselStatustoInactive(String status, String regStatus, Integer vesselId, String reportingStatusChangeReason, String reportingStatusChangeRemarks);
	
	public boolean updateVesselStatusToActive(String status, String regStatus, Integer vesselId);
	
	public boolean isIMONoExist(String imoNo);
	
	public boolean isMMSINoExist(String mmsiNo);
	
	public boolean isVesselSold(Integer vesselId);
	
	/*public boolean isEligibleToInactive(String shipequipId);*/
	
	public boolean isEligibleToInactive(Integer vesselId);
	
	public boolean isEligibleToActive(Integer vesselId);
	
}