package in.gov.lrit.portal.service.vessel;

import java.util.List;
import java.util.Optional;

import in.gov.lrit.portal.model.user.PortalShippingCompany;

public interface ShippingCompanyService {
	
	/*public Map<String,String> findCsoByCode(String companyCode);*/

	public Optional<PortalShippingCompany> getShipingById(String id);
	
	public List<Object[]> shippingCompanyList();
	
	public String getCompanyCodeByName(String companyName);
	
	/*public String getLoginIdByName(String comapnyName);*/
	
	public Iterable<PortalShippingCompany> getAllCompanyList();
	
	public PortalShippingCompany getBycompanyName(String companyName);
}