package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.Optional;

import in.gov.lrit.portal.model.vessel.GroupList;
import in.gov.lrit.portal.model.vessel.PortalVesselGroup;

public interface VesselGroupService {
	
	public PortalVesselGroup addVesselGroup(PortalVesselGroup vesselGroup);
	
	public ArrayList<String> groupNameList();
	
	public Iterable<PortalVesselGroup> findAllGroup();
	
	public Long findGroupIdBygrpName(String groupName);
	
	public boolean updateGroupDetail(String shortName, String description, Long groupId);

	public boolean isGroupExist(String groupName);
	
	public Optional<PortalVesselGroup> findById(Long groupId);
	
	public PortalVesselGroup findBygroupName(String groupName);
	
	public boolean isShortNameExist(String groupName, String shortName);
	
	public ArrayList<GroupList> getAllGroup();
	
	public boolean isStringOnlyAlphabet(String str);
}