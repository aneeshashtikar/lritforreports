//this interface is used to save the request.
package in.gov.lrit.portal.service.request;

import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in.gov.lrit.portal.model.request.PortalCoastalRequest;
import in.gov.lrit.portal.model.request.PortalDdpRequest;
import in.gov.lrit.portal.model.request.PortalFlagRequest;
import in.gov.lrit.portal.model.request.PortalPortRequest;
import in.gov.lrit.portal.model.request.PortalSarRequest;
import in.gov.lrit.portal.model.request.PortalSurpicRequest;

public interface IRequestmanagement {


	void saveddprequest(PortalDdpRequest portalddprequest);
	void saveflagrequest(PortalFlagRequest portalflagrequest);
	void saveportrequest(PortalPortRequest portalportrequest);
	void savesurpicrequest(PortalSurpicRequest portalsurpicrequest);
	void savesarrequest(PortalSarRequest portalsarrequest);
	void savecoastalrequest(PortalCoastalRequest portalcoastalrequest);
	void savependingtask(PortalPendingTaskMst paramPortalPendingTaskMst);
	void setResponsePayload(String response,String messageid);
}
