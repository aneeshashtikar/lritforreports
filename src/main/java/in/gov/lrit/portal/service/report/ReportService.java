package in.gov.lrit.portal.service.report;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.Collection;

import in.gov.lrit.portal.model.report.ASPShipPosition;
import in.gov.lrit.portal.model.report.AspLogInterface;
import in.gov.lrit.portal.model.report.CspLog;
import in.gov.lrit.portal.model.report.DdpLog;
import in.gov.lrit.portal.model.report.SystemStatus;
import in.gov.lrit.portal.model.report.LritLatestShipPostion;
import in.gov.lrit.portal.model.report.VwGeographicalAreaUpdate;
import in.gov.lrit.portal.model.report.VwSarSurpicRequest;
import in.gov.lrit.portal.model.report.VwShipPositionRequest;
import in.gov.lrit.portal.model.report.VwStandingOrder;
//import in.gov.lrit.portal.model.report.VwStandingOrder;
import in_.gov.lrit.portal.dto.VwShipStatusLatestPositionDto;
public interface ReportService {

public Collection<LritLatestShipPostion> ShowPositionReport(int shipSpeed);
	
	public Collection<VwSarSurpicRequest> sarSurpicRequests(Date startDate, Date endDate, String sarAuthority, String sarRequestType);
	
	public Collection<VwSarSurpicRequest> sarSurpicResponses(String messageId);
	
	public Collection<VwShipPositionRequest> getShipPositionRequestByReceiveTimeFlag(Date startDate, Date endDate);
	
	public Collection<VwShipPositionRequest> getShipPositionReportByReceiveTime(Date startDate, Date endDate);
	
	public Collection<ASPShipPosition> getVessselDetails(Date startDate, Date endDate);
	
	public Collection<ASPShipPosition> getShipPosition(String imoNo, Date startDate, Date endDate);
	
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfFlagToIde(Date startDate, Date endDate);
	
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfOtherToIde(String lritId, Date startDate, Date endDate);
	
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfFlagToDdp(Date startDate, Date endDate);
	
	public Collection<VwGeographicalAreaUpdate> getGeographicalAreaOfOtherToDdp(String lritId, Date startDate, Date endDate);
	
	public Collection<VwStandingOrder> getOpenedStandingOrder(Date startDate, Date endDate);
	
	public Map<String, Collection<String>> getExcludedValues(BigInteger soId, Date startDate, Date endDate);
	
	public Collection<CspLog> getCspLogOfFlagVessel(Date startDate, Date endDate);
	
	public Collection<CspLog> getCspLogofNonflagVessel(String dataUserProvider, Date startDate, Date endDate);
	
	public Collection<DdpLog> getDdpRequestLogs(Date startDate, Date endDate);
	
	public String getPayloadAgainstMessageId(String messageId);
	
	public Collection<DdpLog> getDdpUpdateLogs(Date startDate, Date endDate);
	
	public Collection<SystemStatus> getDdpSystemStatusLogs(Date startDate, Date endDate);
	
	public Collection<AspLogInterface> getAspReceiptLog(Date startDate, Date endDate);
	
	public Collection<AspLogInterface> getAspPositionRequestLog(Date startDate, Date endDate);
	
	public Collection<AspLogInterface> getAspPositionReportLog(Date startDate, Date endDate);
	
	public Collection<VwShipStatusLatestPositionDto> getLatestPositionOfFlagVessel(String vesselStatus);
	
	public Collection<VwShipStatusLatestPositionDto> getAllFlagShipsLatestPosition();
}
