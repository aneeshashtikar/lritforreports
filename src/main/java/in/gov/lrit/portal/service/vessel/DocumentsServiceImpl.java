package in.gov.lrit.portal.service.vessel;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.DocumentsRepository;
import in.gov.lrit.portal.model.vessel.PortalDocuments;

@Service
public class DocumentsServiceImpl implements DocumentsService{

	@Autowired
	private DocumentsRepository docsRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(DocumentsServiceImpl.class);
	
	@Override
	public Iterable<PortalDocuments> addAllDocuments(Iterable<PortalDocuments> docslist) {
		logger.info("inside documents service");
		return docsRepo.saveAll(docslist);
	}

	@Override
	public List<PortalDocuments> getDocsByreferenceId(String referenceId, List<String> doc_purpose) {
		logger.info("inside documents service");
		return docsRepo.findByreferenceId(referenceId, doc_purpose);
	}

	@Override
	public PortalDocuments addDocument(PortalDocuments document) {
		logger.info("inside documents service");
		return docsRepo.save(document);
	}

}
