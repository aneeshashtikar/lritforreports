package in.gov.lrit.portal.service.route;

import in.gov.lrit.portal.dao.route.RouteDao;
import in.gov.lrit.portal.model.route.Route;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RouteServiceImpl  implements IRouteService {

	@Autowired
	  RouteDao routeRepository;
	  private static final Logger logger = LoggerFactory.getLogger(RouteServiceImpl.class);
	  
	  public Route SaveRoute(Route route)
	  {
	    return (Route)this.routeRepository.save(route);
	  }
	  
	  public List<Route> getAllRoutes()
	  {
	    List<Route> routeList = this.routeRepository.findAll();
	    return routeList;
	  }
	  
	/*
	 * public String getRouteId(String imoNo) { return
	 * this.routeRepository.getRouteIDByIMONo(imoNo); }
	 */
	  
	  public Route findById(String Id)
	  {
	    return (Route)this.routeRepository.findById(Id).get();
	  }
	  
	public void updateRoute(Route route, String routeId) {
		// String id = this.routeRepository.getRouteIDByIMONo(imoNo);
		Route routeEntity = (Route) this.routeRepository.findById(routeId).get();
		logger.info("data in update route=" + routeEntity.toString());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			logger.info("string date:" + route.getStartDateString());
			Date startDateFormat = dateFormat.parse(route.getStartDateString());
			Date endDateFormat = dateFormat.parse(route.getEndDateString());
			routeEntity.setStartDate(startDateFormat);
			routeEntity.setEndDate(endDateFormat);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		routeEntity.setRouteDescription(route.getRouteDescription());
		routeEntity.setRouteStatus(route.getRouteStatus());
		routeEntity.setCountryList(route.getCountryList());
		this.routeRepository.save(routeEntity);
	}
	  
	  public List<String> getCountriesList(Route route)
	  {
	    String countriesList = route.getCountryList();
	    List<String> finalCountryList = Arrays.asList(countriesList.split(",", -1));
	    return finalCountryList;
	  }
	
}
