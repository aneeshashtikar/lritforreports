package in.gov.lrit.portal.service.alert;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.dao.alert.IAlertDao;
import in.gov.lrit.portal.dao.alert.IClosedAlertsDao;
import in.gov.lrit.portal.model.alert.AlertDetails;/*
import in.gov.lrit.portal.model.alert.ClosedAlertDetails;*/
import in.gov.lrit.portal.model.alert.AlertActionDetails;
import in.gov.lrit.portal.model.alert.AlertActionDetailsDTO;

@Service
public class AlertServiceImpl implements IAlertService{

	@Autowired 
	IAlertDao alertDao;
	
	@Autowired
	IClosedAlertsDao closedAlertDao;
	
	  private static final org.slf4j.Logger logger =
	  org.slf4j.LoggerFactory.getLogger(AlertServiceImpl.class);
	 
	
	@Override
	public List<AlertDetails> findAllAlerts(List<String> roleList, String userCategory, List<String> contextList) {
		return alertDao.findAlerts(roleList, userCategory, contextList);
	}
	@Override
	public List<String> getRoleAgainstLoginId(String loginId) {
		return alertDao.getRoleAgainstLoginId(loginId);
	}
	
	/*
	 * @Override public List<AlertDetails> getUserSpecificAlerts(List<String>
	 * roleList, String severity, String loginId, List<String> contextIdList) {
	 * return alertDao.getUserSpecificAlerts(roleList, severity,loginId,
	 * contextIdList); }
	 */
	
	@Override
	public List<AlertDetails> getUserSpecificAlerts(List<String> roleList, String severity, String userCategory, List<String> contextIdList) {
		return alertDao.getUserSpecificAlerts(roleList, severity,userCategory, contextIdList);
	}
	
	
	@Override
	public Map<String, Integer> getUserSpecificAlertCount(List<String> roleList, String userCategory, List<String> contextIdList) {
		logger.info("inside other user method");
		List<Object[]> alertCountList = alertDao.getUserSpecificAlertCount(roleList, userCategory, contextIdList);
		logger.info("contextList in alert service imple="+contextIdList);
		Map<String, Integer> alertCountMap = new HashMap<String, Integer>();
		int totalCount=0;
		for (Object[] objects : alertCountList) {
			String severity = (String)objects[0];
			BigInteger count = (BigInteger) objects[1];
			totalCount +=count.intValue();
			alertCountMap.put(severity, count.intValue());
			//logger.info("String:="+severity);
		}
		alertCountMap.put("totalCount", totalCount);
		logger.info("totalCount:"+totalCount);
		
		return alertCountMap;
	}
	@Override
	public int insertIntoAlertActionTaken(BigInteger alertTxnId, String loginId, String remark, String userCategory) {
		int flag = alertDao.insertIntoAlertActionTaken(alertTxnId, loginId, remark, userCategory);
		return flag;
	}
	
	@Override
	public List<AlertDetails> getShippingCoAlerts(List<String> roleList, String severity, String loginId) {
		return alertDao.getShippingCoAlerts(roleList, severity, loginId);
	}
	@Override
	public Map<String, Integer> getShippingCoAlertCount(List<String> roleList, String loginId) {
		List<Object[]> alertCountList = alertDao.getShippingCoAlertCount(roleList, loginId);
		Map<String, Integer> alertCountMap = new HashMap<String, Integer>();
		int totalCount=0;
		for (Object[] objects : alertCountList) {
			String severity = (String)objects[0];
			BigInteger count = (BigInteger) objects[1];
			totalCount +=count.intValue();
			alertCountMap.put(severity, count.intValue());
			logger.info("String:="+severity);
		}
		alertCountMap.put("totalCount", totalCount);
		logger.info("totalCount:"+totalCount);
		
		return alertCountMap;
	}
	@Override
	public List<AlertActionDetailsDTO> getClosedAlerts(String userCategory, List<String> contextList, Date startDate, Date endDate){
		logger.info("inside closedAlerts in alertserviceImpl");
		return closedAlertDao.getClosedAlerts(userCategory, contextList, startDate, endDate);
	}
	
	
}
