package in.gov.lrit.portal.service.pendingtask;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;

import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in_.gov.lrit.session.ContractingGovernmentInterface;

public interface PendingTaskService {
	
	public Map<String, Integer> getCategoryWiseAlertCount();

	public List<PortalPendingTaskMst> getRequestPendingTask(String countryLritId, List<String> category );

	public String acceptRequest(String messageId,String category)throws SOAPException, JAXBException, Exception;
	
	public int updatePendingTaskStatus(String status, String approverLoginId, Timestamp date, Integer pendinTaskId, String comment);

	public int updateRequestStatus(String messageId, String approversLoginId, String status, Timestamp statusDate,
			String responsePayload);

	public String acceptEditVessel(Integer vesselId, Integer pendingTaskId);
	
	public String acceptSoldScrapVessel(Integer vesselId, String status);
	
	public String acceptActivetoInactiveVessel(Integer vesselId, String status); 
	
	public PortalPendingTaskMst addEntryInPendingMst(PortalPendingTaskMst pendingtask);
	
	public String acceptInactiveteActiveVessel(Integer vesselId, String status) ;
	
	public String acceptDeleteDnid(Integer vesselId, String status) ;
	
	public String acceptDeleteSeid(Integer vesselId, String status) ;
	
	public String acceptReDownloadDnid(Integer vesselId, String status) ;
	
	public int getPendingTaskCount(List<String> requesterLoginId, List<String> category);
	
	public byte[] getpendingTaskDocument(Integer pendingTaskId);
	
}
