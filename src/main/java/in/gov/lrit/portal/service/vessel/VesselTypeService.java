package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.Optional;

import in.gov.lrit.portal.model.vessel.PortalVesselType;

public interface VesselTypeService {
	
	public ArrayList<String> getVesselTypeList();
	
	public PortalVesselType getByType(String vesselType);
	
	public Iterable<PortalVesselType> getAllVesselType();
	
	public Optional<PortalVesselType> getVesselTypeById(String vesselTypeId);
	
	public String getIdByType(String vesselType);

}