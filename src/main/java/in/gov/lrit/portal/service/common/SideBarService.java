package in.gov.lrit.portal.service.common;

import java.util.List;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;


public interface SideBarService {

	
	public List<Object []> getAspTerminalFrequencyCountFromVW();
	
	public List<VesselDetailInterface> getVesselDetailListFromVWByVesselStatus(String vesselStatus);
	
	public List<Object []> getFlagShipCountByReportingStatus();
	
	public List<VesselDetailInterface> getVesselDetailListFromVWByfreqRate(Integer frequecyRate);
	
	public int getInactiveShipCount();
}
