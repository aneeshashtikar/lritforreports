package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.VesselCategoryRepository;

@Service
public class VesselCategoryServiceImpl implements VesselCategoryService{

	@Autowired
	private VesselCategoryRepository categoryRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(VesselCategoryServiceImpl.class);
	
	@Override
	public ArrayList<String> getCategoryList() {
		logger.info("Inside vessel category service");
		return categoryRepo.findCategoryList();
	}

}