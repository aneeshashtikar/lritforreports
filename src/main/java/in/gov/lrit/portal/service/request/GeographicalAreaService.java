package in.gov.lrit.portal.service.request;

import java.io.IOException;
import java.util.List;

import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaModel;
import in.gov.lrit.portal.gis.lritdb.model.GeographicalAreaInterface;
import in.gov.lrit.portal.model.request.PortalGeoraphicalArea;

public interface GeographicalAreaService {

	public List<GeographicAreaModel> showGeographicalAreaList(String geoStatus);
	
	//public boolean geographcalAreaUpdate(String areaCode, Integer actionType) throws IOException;
	
	public int deleteGeoArea(int gmlId);

	String geographcalAreaUpdate(int gmlId, Integer actionType) throws IOException;
	
	
}
