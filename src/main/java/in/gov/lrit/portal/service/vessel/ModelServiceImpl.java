package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.ModelRepository;
import in.gov.lrit.portal.model.vessel.PortalModelDetail;

@Service
public class ModelServiceImpl implements ModelService{
	
	@Autowired
	private ModelRepository modelRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(ModelServiceImpl.class);
	
	@Override
	public PortalModelDetail addModel(PortalModelDetail mod)
	{
		logger.info("inside addModel() method");
		return modelRepo.save(mod);
	}

	@Override
	public boolean isModelExist(String modelType, Integer manufacturerId, String modelName) {
		logger.info("inside isModelExist() method");
		int result = modelRepo.isModelExist(modelType, manufacturerId, modelName);
		return result >= 1 ? true : false;
	}

	@Override
	public boolean updateModelById(Integer modelId) {
		logger.info("inside updateModelById() method");
		int result = modelRepo.updateModelById(modelId);
		return result >= 1 ? true : false;
	}

	@Override
	public Iterable<PortalModelDetail> getByManuId(Integer manuId) {
		logger.info("inside getByManuId() method");
		return modelRepo.findBymanuDet_manufacturerId(manuId);
	}

	@Override
	public Iterable<PortalModelDetail> getByManuIdAndStatus(Integer manuId) {
		String status = "Active";
		logger.info("inside getByManuIdAndStatus() method");
		return modelRepo.findBymanuDet_manufacturerIdAndStatus(manuId, status);
	}

	@Override
	public boolean updateModelByManuId(Integer manuId) {
		logger.info("inside updateModelByManuId() method");
		int result =  modelRepo.updateModelByManuId(manuId);
		return result >= 1 ? true : false;
	}

	@Override
	public boolean ifNoModelExistForManufacturerName(Integer manuId) {
		logger.info("inside ifNoModelExistForManufacturerName() method");
		int result =  modelRepo.ifNoModelExistForManufacturerId(manuId);
		return result >= 1 ? false : true;
	}

	@Override
	public Optional<PortalModelDetail> getById(Integer modelId) {
		logger.info("inside getById() method");
		return modelRepo.findById(modelId);
	}
	
	@Override
	public ArrayList<String> getModelTypeByManufacturer(Integer manufacturerId) {
		logger.info("inside getModelTypeByManufacturer() method");
		return modelRepo.findModelTypeByManufacturer(manufacturerId);
	}
	
	@Override
	public ArrayList<String> modelListbymodelTypeAndManufacturerId(String modelType, Integer manufacturerId) {
		logger.info("inside modelListbymodelTypeAndManufacturerId() method");
		return modelRepo.findmodelListbymodelTypeAndManufacturerId(modelType, manufacturerId);
	}
	
	@Override
	public PortalModelDetail getModelByManufacturerTypeName(String modelType, Integer manufacturerId, String modelName) {
		logger.info("inside getModelByManufacturerTypeName() method");
		return modelRepo.findModelByManufacturerTypeName(modelType, manufacturerId, modelName);
	}
	
	@Override
	public ArrayList<PortalModelDetail> findmodelListbymodelTypeAndManufacturerIdAndStatus(String modelType,
			Integer manufacturerId) {
		logger.info("inside findmodelListbymodelTypeAndManufacturerIdAndStatus() method");
		return modelRepo.findByModelTypeAndManuDet_ManufacturerIdAndStatus(modelType, manufacturerId, "Active");
	}
	
}