package in.gov.lrit.portal.service.request;

import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.vividsolutions.jts.geom.Geometry;
import in.gov.lrit.portal.model.request.DcPositionRequest;
import in.gov.lrit.portal.model.request.LritLatestShipPositionFlag;
import in.gov.lrit.portal.model.request.LritLatestShipPositionNonFlag;
import in.gov.lrit.portal.model.request.ShipPositionNonFlag;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.repository.requests.DcPositionRequestRepository;
import in.gov.lrit.portal.repository.requests.LatestShipFlagRepository;
import in.gov.lrit.portal.repository.requests.LatestShipNonFlagRepository;
import in.gov.lrit.portal.repository.vessel.VesselDetailRepository;

@Service
public class ShipdetailService implements IShipdetails{

	@Autowired
	VesselDetailRepository shipdao;

	@Autowired
	LatestShipFlagRepository latestposition;

	@Autowired
	LatestShipNonFlagRepository latestshippositionnonflag;
	
	@Autowired
	DcPositionRequestRepository dcposition;

	@Autowired
	VesselDetailRepository shipdetailsdao;

	@Override
	public List<VesselDetailInterface> getFlagShipDetailsList(List<String> status,List<String> regstatus) {
		List<VesselDetailInterface> vesselList=null;
		vesselList=shipdao.checkStatusflag(status, regstatus);
		return vesselList;
	}

	@Override
	public List<LritLatestShipPositionFlag> getShipwithinBoundry(Geometry geo) {
		// TODO Auto-generated method stub
		List<LritLatestShipPositionFlag> latestlist=latestposition.findByImoNOAndMmsiNOAndShipName(geo);
		return latestlist;
	}

	@Override
	public List<DcPositionRequest> getReportingratefromDc(String imo,String status,int messagetype,List<Integer> accesstype) {
		// TODO Auto-generated method stub

		List<DcPositionRequest> dcpositionreq=dcposition.findByimoNoAndRequestStatusAndMessageTypeAndAccessType(imo,status,messagetype,accesstype); 
		return dcpositionreq;
	}

	@Override
	public int getVesselDetail(String imo) {
		// TODO Auto-generated method stub
		int vesselsid=shipdetailsdao.findByImoNoAndStatus(imo,"ACTIVE"); 
		return vesselsid;

	}

	@Override
	public Collection<ShipPositionNonFlag> getNonFlagShips() {
		// TODO Auto-generated method stub
		Collection<ShipPositionNonFlag> latestvessellistNonflag=latestshippositionnonflag.getimonofromnonflag();
		return latestvessellistNonflag;
	}


}
