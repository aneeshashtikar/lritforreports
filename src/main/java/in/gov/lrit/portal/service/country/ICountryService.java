package in.gov.lrit.portal.service.country;

import in.gov.lrit.gis.ddp.model.LritContractGovtMst;
import java.util.List;

public abstract interface ICountryService
{
  public abstract List<LritContractGovtMst> getCountriesList(String ddpVersion);
		  
}
