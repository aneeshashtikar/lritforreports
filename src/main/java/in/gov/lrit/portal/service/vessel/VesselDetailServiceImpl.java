package in.gov.lrit.portal.service.vessel;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.imo.gisis.xml.lrit._2008.Response;
import org.imo.gisis.xml.lrit.dnidrequest._2008.DNIDRequestType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.CommonUtil.CommonUtilService;
import in.gov.lrit.portal.model.pendingtask.PortalShipEquipementHst;
import in.gov.lrit.portal.model.vessel.PortalShipEquipment;
import in.gov.lrit.portal.model.vessel.ShipEquipmentAndVesselInterface;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.repository.pendingtask.ShipequipmentHistoryRepository;
import in.gov.lrit.portal.repository.vessel.ShipEquipmentRepository;
import in.gov.lrit.portal.repository.vessel.VesselDetailRepository;
import in.gov.lrit.portal.webservice.config.ASPSoapConnector;

@Service
public class VesselDetailServiceImpl implements VesselDetailService {

	@Autowired
	private VesselDetailRepository vesselDetailRepository;
	
	@Autowired
	private ShipEquipmentRepository shipEquipmentRepository;
	
	@Autowired
	private CommonUtilService commonUtil;
	
	@Autowired
	ShipequipmentHistoryRepository shipequipmentHistoryRepository;
	
	@Value("${lrit.asp_url}")
	private String asp_url;
	
	
	@Autowired
	private ASPSoapConnector aspsoapConnector;
	
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(VesselDetailServiceImpl.class);
	
	
	@Override
	public List<ShipEquipmentAndVesselInterface> getVesselDetailList(List<String> status,List<String> regstatus) {
		// TODO Auto-generated method stub
		//List<VesselDetailInterface>	vessellist =vesselDetailRepository.checkStatus(status,regstatus);
		List<ShipEquipmentAndVesselInterface> vesselShipbornInterface=shipEquipmentRepository.getShipEquipmentAndVesselDetails(status,regstatus);
		return vesselShipbornInterface;
	}

	@Override
	public boolean manageDNID(Integer vesselId,String requestorLritid,BigInteger messagetype,BigInteger requesttype) {
		// TODO Auto-generated method stub
		//Getting the ship borne equipement details from vesselid
		PortalShipEquipment shipborneequipment= shipEquipmentRepository.findByvesselId(vesselId);
				
		logger.info("Ship borne equipment details from vesselid " +shipborneequipment);
		
		int dnid = shipborneequipment.getDnidNo(); 
		String dnidno=Integer.toString(dnid);
		Timestamp ts=commonUtil.getCurrentTimeStamp();
		String oceanRegion=shipborneequipment.getOceanRegion();
		BigInteger oceanregion=new BigInteger(oceanRegion);
		String test = commonUtil.getParavalues("Test");    

		DNIDRequestType DnidRequestType=new DNIDRequestType();
		//get the data from session
		DnidRequestType.setDataUserProvider(requestorLritid);
		DnidRequestType.setDataUserRequestor(requestorLritid);
		//change when change in asp service
		DnidRequestType.setDNIDNo(dnidno);
		DnidRequestType.setIMONum(shipborneequipment.getImoNo());
		DnidRequestType.setMemberNo(shipborneequipment.getMemberNo());
		//get the data from session
		DnidRequestType.setMessageId(commonUtil.generateMessageID(requestorLritid));
		DnidRequestType.setMessageType(messagetype);
		DnidRequestType.setOceanRegion(oceanregion);
		DnidRequestType.setRequestType(requesttype);
		DnidRequestType.setSchemaVersion(commonUtil.getSchemaVersion());
		DnidRequestType.setTest(BigInteger.valueOf(Long.parseLong(test)));
		DnidRequestType.setTimeStamp(commonUtil.getgregorianTimeStamp(ts));
		
		JAXBContext jaxbContext;
		String reqPayload=null;
		try {
			jaxbContext = JAXBContext.newInstance(DNIDRequestType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(DnidRequestType, sw);
			reqPayload=sw.toString();
			logger.info("payload:"+reqPayload);	
			
		} catch (JAXBException e) {
			e.printStackTrace();
			return false;
		}
		try 
		{
			Response response=aspsoapConnector.callASPWebService(asp_url, DnidRequestType);
			response.getResponse();
			logger.info("response in controller:"+	response.getResponse());
			String res=response.getResponse().toString();
			if (res=="SUCCESS")
			{
				updateReqResponsePayload(vesselId,reqPayload , res);
				//STATUS CHANGE FROM SUBMITTED TO SEDN TO IDE
			    	logger.info("Delete DNID sent successfully");
					return true;
			}
			else
			{
				logger.info("Error Sending Delete DNID");
				return false;
			}
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}
		return false;
	}
	
	public PortalShipEquipment shipBorneEquiomentDetail(Integer vesselId)
	{
		PortalShipEquipment shipborneequipment= shipEquipmentRepository.findByvesselId(vesselId);
		return shipborneequipment;
	}

	@Override
	public int DeleteSeid(Integer vesselId) {
		// TODO Auto-generated method stub
		int status=shipEquipmentRepository.deleteSeid(vesselId);
		return status;
	}

	@Override
	public String getDnidStatus(Integer vesselId) {
		// TODO Auto-generated method stub
		String dnidStatus=shipEquipmentRepository.findDnidStatusByvesselId(vesselId);
		return dnidStatus;
	}

	@Override
	public int updateDnid(int vesselId, int dnidNo,int memberNo) {
		// TODO Auto-generated method stub
		int updateStatus=shipEquipmentRepository.updateDnid(vesselId, dnidNo, memberNo);
		return updateStatus;
	}

	

	@Override
	public String checkVesselStatus(int vesselId,List<String>registrationStatus,List<String> shipstatus) {
		// TODO Auto-generated method stub
		String vesselid=vesselDetailRepository.findByVesselId(vesselId, registrationStatus, shipstatus);
		return vesselid;
	}

	@Override
	public int updateVesselRegStatus(int vesselid,String status) {
		// TODO Auto-generated method stub
		int seid=vesselDetailRepository.updateVesselRegistrationStatus(vesselid, status);
		return seid;
	}

	@Override
	public int updateSeid(int vesselid,int memberno,int dnidno) {
		// TODO Auto-generated method stub
		int seid=shipEquipmentRepository.updateDnidAndMemberNo(vesselid, memberno, dnidno);
		return seid;
	}

	@Override
	public PortalShipEquipementHst saveSeidHistory(PortalShipEquipementHst seidhst) {
		// TODO Auto-generated method stub
		PortalShipEquipementHst seidhistory=shipequipmentHistoryRepository.save(seidhst);
		return seidhistory;
		
	}

	@Override
	public int updateReqResponsePayload(int vesselid,String req, String response) {
		// TODO Auto-generated method stub
		int vessel=shipEquipmentRepository.updateRequestpayload(vesselid, req, response);
		return vessel;
	}

	@Override
	public int getInactiveShipCount() {
		// TODO Auto-generated method stub
		int count=vesselDetailRepository.getInactiveShipCount();
		return count;
	}

	@Override
	public int updatetimedelay(BigInteger timedelay, Integer Vesselid) {
		// TODO Auto-generated method stub
		int timedelayinput=vesselDetailRepository.updatetimedelay(timedelay,Vesselid);
		return timedelayinput;
	}

	@Override
	public String gettimedelay(Integer vesselId) {
     String vesseltimedelay=vesselDetailRepository.gettimedelay(vesselId);
		return vesseltimedelay;
	}

	


	

}
