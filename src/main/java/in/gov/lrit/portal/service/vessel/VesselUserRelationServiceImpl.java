package in.gov.lrit.portal.service.vessel;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.VesselUserRelationRepository;
import in.gov.lrit.portal.model.vessel.PortalShippingCompanyVesselRelation;

@Service
public class VesselUserRelationServiceImpl implements VesselUserRelationService{

	@Autowired
	private VesselUserRelationRepository repo;
	
	private static final Logger logger = LoggerFactory.getLogger(VesselCategoryServiceImpl.class);
	
	@Override
	public PortalShippingCompanyVesselRelation addMapping(PortalShippingCompanyVesselRelation pscvr) {
		logger.info("Inside Vessel User Relation Service");
		return repo.save(pscvr);
	}

	@Override
	public Iterable<PortalShippingCompanyVesselRelation> addAllMapping(Iterable<PortalShippingCompanyVesselRelation> pscvrlist) {
		logger.info("Inside Vessel User Relation Service");
		return repo.saveAll(pscvrlist);
	}

	@Override
	public List<PortalShippingCompanyVesselRelation> getBycustomId_vesselDet_vesselId(Integer vesselId) {
		logger.info("Inside Vessel User Relation Service");
		return repo.findBycustomId_vesselDet_vesselId(vesselId);
	}

}