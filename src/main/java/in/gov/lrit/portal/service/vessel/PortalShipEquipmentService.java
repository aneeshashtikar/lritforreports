package in.gov.lrit.portal.service.vessel;


import in.gov.lrit.portal.model.vessel.PortalShipEquipment;

public interface PortalShipEquipmentService {
	
	public PortalShipEquipment addShipEquip(PortalShipEquipment pse);
	
	/*public PortalShipEquipment getByvesselDet_vesselIdAndSeidStatus(Integer vesselId);*/
	public PortalShipEquipment getByvesselDet_vesselId(Integer vesselId);
	
	public boolean isShipEquipIdExist(String shipEquipmentId);
	
	public Integer updateShipEquipByImoNo(String requestPayload, String responsePayload, String shipequiId);
	
	/*public boolean updatednidStatusAnddnidStatusDate(String dnidStatus, String shipequiId);*/
	/*public PortalShipEquipment saveBy_shipborneEquipmentId(String y);*/
	
	public boolean getIfDnidSeidStatusDeleted(Integer vesselId);
	
	public boolean updateShipEquipById(Integer dnidNo, Integer memberNo, String shipequiId);
	
	public boolean updateResponsePayload(String responsePayload, String shipequiId);
	
	public boolean updateRequestPayload(String requestPayload, String shipequiId);

}