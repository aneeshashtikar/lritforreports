package in.gov.lrit.portal.service.vessel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.ShipEquipmentRepository;
import in.gov.lrit.portal.model.vessel.PortalShipEquipment;
@Service
public class PortalShipEquipmentServiceImpl implements PortalShipEquipmentService{

	@Autowired
	private ShipEquipmentRepository shipEquipRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(PortalShipEquipmentServiceImpl.class);

	@Override
	public PortalShipEquipment addShipEquip(PortalShipEquipment pse) {
		logger.info("inside addShipEquip() method");
		return shipEquipRepo.save(pse);
	}

	/*@Override
	public PortalShipEquipment getByvesselDet_vesselIdAndSeidStatus(Integer vesselId) {
		logger.info("inside ship euip service");
		logger.info("Vessel No : " + vesselId);
		return shipEquipRepo.findByvesselDet_vesselIdAndSeidStatus(vesselId, "Active");
	}*/
	
	@Override
	public PortalShipEquipment getByvesselDet_vesselId(Integer vesselId) {
		logger.info("inside getByvesselDet_vesselId() method");
		logger.info("Vessel No : " + vesselId);
		return shipEquipRepo.findByvesselDet_vesselId(vesselId);
	}

	@Override
	public boolean isShipEquipIdExist(String shipEquipmentId) {
		logger.info("inside isShipEquipIdExist() method");
		int result = shipEquipRepo.isShipEquipIdExist(shipEquipmentId);
		return result >=1 ? true : false;
	}

	@Override
	public Integer updateShipEquipByImoNo(String requestPayload, String responsePayload, String shipequiId) {
		logger.info("inside updateShipEquipByImoNo() method");
		return shipEquipRepo.updateShipEquipByImoNo(requestPayload, responsePayload, shipequiId);
	}

	/*@Override
	public boolean updatednidStatusAnddnidStatusDate(String dnidStatus, String shipequiId) {
		logger.info("inside ship euip service");
		Integer result =  shipEquipRepo.updatednidStatusAnddnidStatusDate(dnidStatus, shipequiId);
		return result >=1 ? true : false;
	}*/

	@Override
	public boolean getIfDnidSeidStatusDeleted(Integer vesselId) {
		logger.info("inside getIfDnidSeidStatusDeleted() method");
		Integer result =  shipEquipRepo.findIfDnidSeidStatusDeleted(vesselId);
		return result >=1 ? true : false;
	}

	@Override
	public boolean updateShipEquipById(Integer dnidNo, Integer memberNo, String shipequiId) {
		logger.info("inside updateShipEquipById() method");
		Integer result =  shipEquipRepo.updateShipEquipById(dnidNo, memberNo, shipequiId);
		return result >=1 ? true : false;
	}

	@Override
	public boolean updateResponsePayload(String responsePayload, String shipequiId) {
		logger.info("inside updateResponsePayload() method");
		Integer result =  shipEquipRepo.updateResponsePayload(responsePayload, shipequiId);
		return result >=1 ? true : false;
	}

	@Override
	public boolean updateRequestPayload(String requestPayload, String shipequiId) {
		logger.info("inside updateRequestPayload() method");
		Integer result =  shipEquipRepo.updateRequestPayload(requestPayload, shipequiId);
		return result >=1 ? true : false;
	}
	
}