package in.gov.lrit.portal.service.request;

import java.sql.Timestamp;
import java.util.List;

import in.gov.lrit.gis.ddp.model.DdpVersion;
import in.gov.lrit.gis.ddp.model.LritContractGovtMst;
import in.gov.lrit.gis.ddp.model.LritPolygon;
import in.gov.lrit.gis.ddp.model.LritPort;
import in.gov.lrit.gis.ddp.model.LritPortFacilityType;
import in.gov.lrit.portal.model.request.DcPositionReport;
import in.gov.lrit.portal.model.request.DcSarsurpicrequest;

public interface IDdpMessagingManagement {
	 public LritPolygon getCountryPolygon(String cgid,String regularversion);
	 public List<LritPort> getPortList(String cgid, String regularVersion);
	 public List<LritPortFacilityType> getPortFacilityList(String cgid, String regularVersion);
	 public List<DdpVersion> getDdpVersionview();
	 public List<LritContractGovtMst> getCountrylist(String regularVersion); 
	 public List<DcSarsurpicrequest> getSurpicarea(int accesstype);  
	 public List<DcPositionReport> getShipsinSurpic(List<String>  referenceid);
	 public String getDdpNotificationMessageid();
	 public Timestamp getArchivedDdpversionTimestamp(String ddpversion);
	 public String getDataUserProviderlritid(String datauserprovidercountry); 
}
