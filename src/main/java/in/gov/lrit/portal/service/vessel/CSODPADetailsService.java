package in.gov.lrit.portal.service.vessel;

import java.util.List;
import java.util.Optional;

import in.gov.lrit.portal.model.vessel.PortalCSODPADetails;

public interface CSODPADetailsService {
	
public List<PortalCSODPADetails> getCSOByportalUser_loginIdAndType(String loginId, String type);

public List<PortalCSODPADetails> getDPAByportalUser_loginIdAndType(String loginId, String type);
	
public List<PortalCSODPADetails> getACSOByportalUser_loginIdAndLritCsoIdSeq(String loginId, String lritCsoIdSeq);

/*public List<PortalCSODPADetails> getACSOByportalUser_loginId(String loginId);*/
	
	public PortalCSODPADetails getByname(String name);
	
	public Optional<PortalCSODPADetails> findBylritCsoIdSeq(String id);
	
	
	/*public ArrayList<String> getCurrentCsoListBycompanyCode(String companyCode);
	
	public ArrayList<String> getAltCsoListBycompanyCode(String companyCode);
	
	public ArrayList<String> getDPAListBycompanyCode(String companyCode);*/
	
	/*public List<CSOALTCSODPAName> getnameAndtypeBycompanyCode(String companyCode)*/;
	
	/*public Map<String, String> getnameAndtypeBycompanyCode(String companyCode);*/
	
	/*public List<PortalCSODPADetails> getCSODPAList(String type,String companyCode);*/
	
	/*public Iterable<PortalCSODPADetails> getCDODPAByTypeAndLoginId(String loginId, String type);*/
	
	/*public Iterable<PortalCSODPADetails> getDPAByportalUser_loginIdAndType(String loginId, String type);*/
	
}