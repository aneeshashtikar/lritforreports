package in.gov.lrit.portal.service.route;

import in.gov.lrit.portal.model.route.Route;
import java.util.List;

public abstract interface IRouteService
{
  public abstract Route SaveRoute(Route paramRoute);
  
  public abstract List<Route> getAllRoutes();
  
	/* public abstract String getRouteId(String paramString); */
  
  public abstract Route findById(String paramString);
  
  public abstract List<String> getCountriesList(Route paramRoute);
  
  public abstract void updateRoute(Route paramRoute, String paramString);
}