package in.gov.lrit.portal.service.logging;

import in.gov.lrit.portal.model.logging.PortalLoginAudit;

public interface PortalLoggingService {

	public PortalLoginAudit saveLoginStatus(PortalLoginAudit portalLoginAudit);
	
	public int UpdateGeoLocation(Integer id,String latitude,String longitude);
	
	public void UpdateLogoutTime(Integer id);
}
