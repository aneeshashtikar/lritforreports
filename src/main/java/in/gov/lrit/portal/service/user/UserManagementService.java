/**

 * @UserManagementService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author   Bhavika Ninawe
 * @version 1.0
 * UserManagementService used for implementation of method exposed in IUserManagementService interface. 
 */
package in.gov.lrit.portal.service.user;

import java.net.ConnectException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.portal.CommonUtil.UUIDGeneratorUtil;
import in.gov.lrit.portal.CommonUtil.Utility;
import in.gov.lrit.portal.model.contractinggovernment.PortalContext;
import in.gov.lrit.portal.model.contractinggovernment.PortalContextPK;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.user.PortalCsoDpaDetail;
import in.gov.lrit.portal.model.user.PortalCsoDpaDetailsTransit;
import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in.gov.lrit.portal.model.user.PortalUser;
import in.gov.lrit.portal.model.user.PortalUserDTO;

import in.gov.lrit.portal.model.user.PortalUserInterface;
import in.gov.lrit.portal.model.user.PortalUserSarAuthority;

import in.gov.lrit.portal.model.user.PortalUserTransit;
import in.gov.lrit.portal.model.user.PortalUsersPassword;
import in.gov.lrit.portal.repository.contractinggovernment.ContractingGovernmentRepository;
import in.gov.lrit.portal.repository.contractinggovernment.PortalContextRepository;
import in.gov.lrit.portal.repository.user.PortalCsoDpaDetailRepository;
import in.gov.lrit.portal.repository.user.PortalCsoDpaDetailTransitRepository;
import in.gov.lrit.portal.repository.user.PortalUserPasswordRepository;
import in.gov.lrit.portal.repository.user.PortalUserSarAuthorityRepository;
import in.gov.lrit.portal.repository.user.PortalUserTransitRepository;
import in.gov.lrit.portal.repository.user.PortalUsersRoleMappingRepository;
import in.gov.lrit.portal.repository.user.ShipingCompanyRepository;
import in.gov.lrit.portal.repository.user.UserManagementRepository;
import in_.gov.lrit.session.ContractingGovernmentInterface;
import in_.gov.lrit.session.UserSessionInterface;
import in.gov.lrit.portal.model.user.PortalUsersRoleMapping;

@Service
public class UserManagementService implements IUserManagementService {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(UserManagementService.class);

	@Autowired
	private UserManagementRepository usermanagementRepository;
	@Autowired
	private PortalUsersRoleMappingRepository portalUserRoleMappingRepository;
	@Autowired
	private PortalUserSarAuthorityRepository portalUserSarAuthorityRepository;
	@Autowired
	private PortalUserTransitRepository portalUserTransitRepository;

	@Autowired
	ShipingCompanyRepository shipingCompanyRepository;
	@Autowired
	PortalCsoDpaDetailRepository portalCsoDpaDetailRepository;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private PortalUserPasswordRepository portalUserPasswordRepository;
	@Autowired
	private PortalCsoDpaDetailTransitRepository portalCsoDpaDetailTransitRepository;
	@Autowired
	private ContractingGovernmentRepository contractingGovernmentRepository;
	@Autowired
	private PortalContextRepository portalContextRepository;

	public boolean getVessel(String userId) {
		return shipingCompanyRepository.getVessel(userId);
	}

	@Override
	public boolean getUser(String lritId) {
		// TODO Auto-generated method stub
		logger.info("Inside getUser service method ");
		return usermanagementRepository.getUser(lritId);
	}

	public Collection<PortalUserInterface> getAllUser() {
		logger.info("Inside getAllUser service method ");
		Collection<PortalUserInterface> portalUserList = usermanagementRepository.getAllUsersList();
		return portalUserList;
	}

	
	public Collection<PortalUserInterface> getUserUsingStatus(String status) {
		logger.info("Inside getUserUsingStatus service method. status = " + status);

		Collection<PortalUserInterface> portalUserList = usermanagementRepository.getUsersList(status);
		return portalUserList;
	}

	
	@Transactional
	public String persistUser(PortalUserDTO portalUserDTO, Set<PortalUsersRoleMapping> userRoleMapping) {
		logger.info("Inside persistUser service method");
		PortalUser portalUser = portalUserDTO.getPortaluser();
		Timestamp timestamp = Utility.getCurrentTimeStamp();
		logger.info("Current date and time :" + timestamp);
		portalUser.setRegisterDate(timestamp);
		portalUser.setNoOfPasswordAttempt(4);
		portalUser.setStatus("registered");
		portalUserDTO.setPortaluser(portalUser);
		if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_SC")) {
			logger.info("User is shipping company");

			// check existences of shipping company

			boolean flag = shipingCompanyRepository
					.checkExistences(portalUser.getPortalShippingCompany().getCompanyCode());
			logger.info("check flag " + flag);
			// if flag is false shipping company is not present. persist shipping company
			if (flag == false) {
				// set relation as owner
				logger.info("shipping company is not registered");
				portalUser.setRelation("owner");
				shipingCompanyRepository.save(portalUser.getPortalShippingCompany());
			} else {
				String companyName = shipingCompanyRepository
						.getCompanyNameByCode(portalUser.getPortalShippingCompany().getCompanyCode());
				PortalShippingCompany shippingCompany = portalUser.getPortalShippingCompany();
				shippingCompany.setCompanyName(companyName);
				portalUser.setPortalShippingCompany(shippingCompany);
			}

			// persist portaluser
			portalUser.setStatus("incomplete");
			usermanagementRepository.save(portalUser);
			// persist cso
			List<PortalCsoDpaDetail> csoList = portalUserDTO.getCurrentcsoList();
			logger.info("******************");
			for (PortalCsoDpaDetail csoDetails : csoList) {
				logger.info(csoDetails.toString());
			}
			logger.info("******************");
			List<PortalCsoDpaDetail> persistingCsoList = new ArrayList<>();
			for (PortalCsoDpaDetail csoDetails : csoList) {
				if (csoDetails.getFlag() != null && csoDetails.getFlag().equals("new")
						&& csoDetails.getName() != null) {
					csoDetails.setCompanyCode(portalUser.getPortalShippingCompany().getCompanyCode());
					csoDetails.setLoginId(portalUser.getLoginId());
					persistingCsoList.add(csoDetails);
				}
			}
			// portalCsoDpaDetailRepository.saveAll(persistingCsoList);
			List<PortalCsoDpaDetail> dpaList = portalUserDTO.getDpaList();
			logger.info("******************");
			for (PortalCsoDpaDetail dpaDetails : dpaList) {
				logger.info(dpaDetails.toString());
			}
			logger.info("******************");
			// List<PortalCsoDpaDetail> persistingDpaList=new ArrayList<>();
			for (PortalCsoDpaDetail dpaDetails : dpaList) {
				if (dpaDetails.getFlag() != null && dpaDetails.getFlag().equals("new")
						&& dpaDetails.getName() != null) {
					dpaDetails.setCompanyCode(portalUser.getPortalShippingCompany().getCompanyCode());
					dpaDetails.setLoginId(portalUser.getLoginId());
					persistingCsoList.add(dpaDetails);
				}
			}

			portalCsoDpaDetailRepository.saveAll(persistingCsoList);
			logger.info("after persisting in cso dpa details");
			portalUserRoleMappingRepository.saveAll(userRoleMapping);

		} else if (portalUserDTO.getPortaluser().getCategory().equals("USER_CATEGORY_CG")) {
			
			PortalUserSarAuthority portalUserSarAuthority = portalUserDTO.getPortalUserSarAuthority();
			 logger.debug("portalUserSar authority " + portalUserSarAuthority.toString());
			portalUserSarAuthority.setLoginId(portalUser.getLoginId());
			logger.debug("portalUserSar authority2 " + portalUserSarAuthority.toString());
			usermanagementRepository.save(portalUser);
			logger.debug("After saving in portalusers");
			portalUserSarAuthorityRepository.save(portalUserSarAuthority);
			logger.debug("After saving in portalusersarauthority");
			portalUserRoleMappingRepository.saveAll(userRoleMapping);
		} else {
			logger.info("category of user " + portalUser.getCategory());
			usermanagementRepository.save(portalUser);
			portalUserRoleMappingRepository.saveAll(userRoleMapping);
		}
		persistContext(portalUser);
		// persist roles
		// portalUsersRoleMappingDao.saveAll(userRoleMappings);
		return null;
	}

	public void persistContext(PortalUser portalUser) {
		logger.info("Inside persistContext service method");
		String loginId = portalUser.getLoginId();

		/*
		 * PortalContext portalContext = null; PortalContextPK portalContextPK = null;
		 */
		PortalContext portalContext = new PortalContext();
		PortalContextPK portalContextPK = new PortalContextPK();
		if (portalUser.getCategory().equals("USER_CATEGORY_DGS")
				&& portalUser.getRequestorsLritId().getCountryName().equalsIgnoreCase("India")) { // Get all lritId of
																									// cg
			List<PortalContext> contextlist = new ArrayList<>();
			List<PortalLritIdMaster> cglist = contractingGovernmentRepository.getContratingGovernment();

			for (PortalLritIdMaster cg : cglist) {
				logger.info("inside cg list " + cg.toString());
				portalContext = new PortalContext();
				portalContextPK = new PortalContextPK();
				portalContextPK.setLoginId(loginId);
				portalContextPK.setContextId(cg.getLritId());
				portalContext.setId(portalContextPK);
				contextlist.add(portalContext);
			}
			logger.info("size of contexr list " + contextlist.size());
			for (PortalContext cg : contextlist) {

				logger.info("cg " + cg.toString());
			}
			portalContextRepository.saveAll(contextlist);

		} else if (portalUser.getCategory().equals("USER_CATEGORY_SC")) {

			portalContextPK.setLoginId(loginId);
			portalContextPK.setContextId(portalUser.getPortalShippingCompany().getCompanyCode());
			portalContext.setId(portalContextPK);
			portalContextRepository.save(portalContext);
		} else {

			portalContextPK.setLoginId(loginId);
			portalContextPK.setContextId(portalUser.getRequestorsLritId().getLritId());
			portalContext.setId(portalContextPK);
			portalContextRepository.save(portalContext);
		}
		return;
	}

	public List<PortalUser> getUser() {
		logger.info("Inside getUser service method");
		return usermanagementRepository.getUsersList();
	}

	public PortalUserDTO getUserDetails(String loginId) {
		logger.info("Inside getUserDetails service method");
		PortalUserDTO portalUserDTO = new PortalUserDTO();
		PortalUser portalUser = usermanagementRepository.getUserDetails(loginId);

		if (portalUser == null) {
			logger.info("User is not present in database having login id = "+loginId);
			return portalUserDTO;
		}
		logger.info("portaluser  " + portalUser.toString());

		logger.info(portalUser.getCategory());
		if (portalUser.getCategory().equals("USER_CATEGORY_SC")) {


			List<PortalCsoDpaDetail> csoList = portalCsoDpaDetailRepository.getCsoList(portalUser.getLoginId());
			List<PortalCsoDpaDetail> dpaList = portalCsoDpaDetailRepository.getDpaList(portalUser.getLoginId());
			portalUserDTO.setCurrentcsoList(csoList);
			portalUserDTO.setDpaList(dpaList);
			portalUserDTO.setPortaluser(portalUser);

		} else if (portalUser.getCategory().equals("USER_CATEGORY_CG")) {
			PortalUserSarAuthority sarAuthority = portalUserSarAuthorityRepository.getSarDetails(loginId);
			portalUserDTO.setPortalUserSarAuthority(sarAuthority);
			logger.info("User has category other than ShipingCompany");
			portalUserDTO.setPortaluser(portalUser);
		} else {
			portalUserDTO.setPortaluser(portalUser);
		}

		return portalUserDTO;
	}

	public int deregisterUser(String loginId) {
		logger.info("Inside deregisterUser service method. LoginId = "+loginId);
		int flag = usermanagementRepository.deregisterUser(loginId);
		logger.info("flag " + flag);
		return flag;
	}

	public boolean checkUserExist(String loginId) {
		return usermanagementRepository.checkUserExist(loginId);
	}

	public void updateUser(PortalUserDTO portalUserDTO, Set<PortalUsersRoleMapping> userRoleMapping) {
		logger.info("Inside update User " + portalUserDTO.toString());

		// get previous data entry of login id
		PortalUser previousPortalUser = usermanagementRepository
				.getUserDetails(portalUserDTO.getPortaluser().getLoginId());

		PortalUser portalUser = portalUserDTO.getPortaluser();
		if (portalUserDTO.getPortaluser().getUploadedFilePath() == null) {
			logger.info("uploaded file is null");
			portalUser.setUploadedFilePath(previousPortalUser.getUploadedFilePath());
			portalUserDTO.setPortaluser(portalUser);
		}
		if (portalUser.getCategory().equals("USER_CATEGORY_SC")) {
			// if shiping company
			// MAke entry in transient
			PortalUserTransit portalUserTransit;
			portalUserTransit = setPortalUserTransient(previousPortalUser);
			portalUserTransitRepository.save(portalUserTransit);

			// cso and dpa entry using loginid
			List<PortalCsoDpaDetail> csoDpaList = portalCsoDpaDetailRepository.getCsoDpaList(portalUser.getLoginId());
			List<PortalCsoDpaDetailsTransit> portalCsoDpaDetailsTransitsList = new ArrayList<>();
			for (PortalCsoDpaDetail csoDpa : csoDpaList) {
				PortalCsoDpaDetailsTransit portalCsoDpaDetailsTransit = setPortalDpaCsoDetailsTransist(csoDpa);
				portalCsoDpaDetailsTransitsList.add(portalCsoDpaDetailsTransit);
			}

			portalCsoDpaDetailTransitRepository.saveAll(portalCsoDpaDetailsTransitsList);

			usermanagementRepository.updateUser(portalUserDTO.getPortaluser());
			if (userRoleMapping != null && userRoleMapping.size() > 0) {
				// Mapping of role and user
				portalUserRoleMappingRepository.deleteByLoginId(portalUserDTO.getPortaluser().getLoginId());
				portalUserRoleMappingRepository.saveAll(userRoleMapping);
			}

			List<PortalCsoDpaDetail> csoList = portalUserDTO.getCurrentcsoList();
			if (csoList != null) {
				if (csoList.size() != 0) {
					for (PortalCsoDpaDetail cso : csoList) {
						if (cso.getFlag() != null) {
							if (cso.getFlag().equals("new")) {
								logger.info("Inside new ");
								cso.setCompanyCode(portalUser.getPortalShippingCompany().getCompanyCode());
								cso.setLoginId(portalUser.getLoginId());
								portalCsoDpaDetailRepository.save(cso);
							}else if (cso.getCsoidseq() != null && cso.getName() == null) {
								logger.info("Inside remove ");
								portalCsoDpaDetailRepository.updateDeregsiterdate(cso.getCsoidseq());
							} else if (cso.getFlag().equals("update")) {
								logger.info("Inside update ");
								portalCsoDpaDetailRepository.updateCsoDpaDetails(cso);
							}
						}
					}
				}
		}
		List<PortalCsoDpaDetail> dpaList = portalUserDTO.getDpaList();
		if (dpaList != null) {
			if (dpaList.size() != 0) {
				for (PortalCsoDpaDetail dpa : dpaList) {
					logger.info("dpa " + dpa.toString());
					if (dpa.getFlag() != null) {
						if (dpa.getFlag().equals("new")) {
							logger.info("Inside dpa new ");
							dpa.setCompanyCode(portalUser.getPortalShippingCompany().getCompanyCode());
							dpa.setLoginId(portalUser.getLoginId());
							portalCsoDpaDetailRepository.save(dpa);
						} else if (dpa.getCsoidseq() != null && dpa.getName() == null) {
							logger.info("Inside dpa remove ");
							portalCsoDpaDetailRepository.updateDeregsiterdate(dpa.getCsoidseq());
						} else if (dpa.getFlag().equals("update")) {
							logger.info("Inside dpa update ");
							portalCsoDpaDetailRepository.updateCsoDpaDetails(dpa);
						}
					}
				}
			}
		}

	}else

	{
		PortalUserTransit portalUserTransit;
		portalUserTransit = setPortalUserTransient(previousPortalUser);
		portalUserTransitRepository.save(portalUserTransit);

		usermanagementRepository.updateUser(portalUserDTO.getPortaluser());

		if (userRoleMapping != null && userRoleMapping.size() > 0) {
			// Mapping of role and user
			portalUserRoleMappingRepository.deleteByLoginId(portalUserDTO.getPortaluser().getLoginId());
			portalUserRoleMappingRepository.saveAll(userRoleMapping);
		}
	}
	}

	public PortalCsoDpaDetailsTransit setPortalDpaCsoDetailsTransist(PortalCsoDpaDetail previousPortalCsoDpaDetail) {
		logger.info("Inside setPortalDpaCsoDetailsTransist service method. ");
		PortalCsoDpaDetailsTransit portalCsoDpaDetail = new PortalCsoDpaDetailsTransit();
		portalCsoDpaDetail.setCompanyCode(previousPortalCsoDpaDetail.getCompanyCode());
		portalCsoDpaDetail.setDeregisterDate(Utility.getCurrentTimeStamp());
		portalCsoDpaDetail.setEmailid(previousPortalCsoDpaDetail.getEmailid());
		portalCsoDpaDetail.setLoginId(previousPortalCsoDpaDetail.getLoginId());
		portalCsoDpaDetail.setMobileNo(previousPortalCsoDpaDetail.getMobileNo());
		portalCsoDpaDetail.setName(previousPortalCsoDpaDetail.getName());
		portalCsoDpaDetail.setParentCsoid(previousPortalCsoDpaDetail.getParentCsoid());
		portalCsoDpaDetail.setRegisterDate(previousPortalCsoDpaDetail.getRegisterDate());
		portalCsoDpaDetail.setTelephoneOffice(previousPortalCsoDpaDetail.getTelephoneResidence());
		portalCsoDpaDetail.setTelephoneResidence(previousPortalCsoDpaDetail.getTelephoneResidence());
		portalCsoDpaDetail.setType(previousPortalCsoDpaDetail.getType());
		logger.info("Getting Outside setPortalDpaCsoDetailsTransist service method. PortalCsoDpaDetailsTransit = "+portalCsoDpaDetail.toString());
		return portalCsoDpaDetail;
	}

	public PortalUserTransit setPortalUserTransient(PortalUser portalUser) {
		logger.info("Inside setPortalUserTransient service method. ");
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		
		UserSessionInterface user = (UserSessionInterface) session.getAttribute("USER");
		String loginId = (String)user.getLoginId();
		logger.info("session loginId = "+loginId);
		
		
		
		PortalUserTransit portalUserTransit = new PortalUserTransit();
		portalUserTransit.setAddress1(portalUser.getAddress1());
		portalUserTransit.setAddress2(portalUser.getAddress2());
		portalUserTransit.setAddress3(portalUser.getAddress3());
		portalUserTransit.setCity(portalUser.getCity());
		portalUserTransit.setCategory(portalUser.getCategory());
		portalUserTransit.setCountry(portalUser.getCountry());
		portalUserTransit.setDeregisterDate(Utility.getCurrentTimeStamp());
		portalUserTransit.setDistrict(portalUser.getDistrict());
		portalUserTransit.setEmailid(portalUser.getEmailid());
		portalUserTransit.setAlternateemailid(portalUser.getAlternateemailid());
		portalUserTransit.setFax(portalUser.getFax());
		portalUserTransit.setLandmark(portalUser.getLandmark());
		portalUserTransit.setLoginId(portalUser.getLoginId());
		portalUserTransit.setMobileno(portalUser.getMobileno());
		portalUserTransit.setName(portalUser.getName());
		portalUserTransit.setNoOfPasswordAttempt(portalUser.getNoOfPasswordAttempt());
		portalUserTransit.setPassword(portalUser.getPassword());
		portalUserTransit.setPendingtaskId(null);
		portalUserTransit.setPincode(portalUser.getPincode());
		portalUserTransit.setRegisterDate(portalUser.getRegisterDate());
		portalUserTransit.setRelation(portalUser.getRelation());
		portalUserTransit.setRequestorsLritId(portalUser.getRequestorsLritId().getLritId());
		portalUserTransit.setSarAreaView(portalUser.getSarAreaView());
		portalUserTransit.setState(portalUser.getState());
		portalUserTransit.setStatus("deregistered");
		portalUserTransit.setTelephone(portalUser.getTelephone());
		portalUserTransit.setTokenvalidupto(portalUser.getTokenvalidupto());
		portalUserTransit.setUpdateDate(portalUser.getUpdateDate());
		portalUserTransit.setUpdatedBy(portalUser.getUpdatedBy());
		portalUserTransit.setUploadedFilePath(portalUser.getUploadedFilePath());
		portalUserTransit.setValidateToken(portalUser.getValidateToken());
		portalUserTransit.setWebsite(portalUser.getWebsite());
		portalUserTransit.setUpdateDate(Utility.getCurrentTimeStamp());
		portalUserTransit.setUpdatedBy(loginId); // corresponding login ID will be taken from session

		if (portalUser.getCategory().equals("USER_CATEGORY_SC")) {
			logger.info("PortalUser category is USER_CATEGORY_SC");
			portalUserTransit.setCompanyCode(portalUser.getPortalShippingCompany().getCompanyCode());
		}
		logger.debug("Getting Outside of setPortalUserTransient service method. portalUserTransit = "+portalUserTransit.toString());
		return portalUserTransit;
	}

	public String checkCompanyCodeExist(String companyCode) {
		logger.info("Inside checkCompanyCodeExist service method. companyCode ="+companyCode);
		
		return shipingCompanyRepository.checkCompanyCodeExist(companyCode);
	}

	public List<PortalCsoDpaDetail> getCsoList(String loginId) {
		logger.info("Inside getCsoList service method. loginId " + loginId);
		List<PortalCsoDpaDetail> csoList = portalCsoDpaDetailRepository.getCsoList(loginId);
		for (PortalCsoDpaDetail cso : csoList) {
			logger.debug("cso " + cso.toString());
		}

		return portalCsoDpaDetailRepository.getCsoList(loginId);
	}

	public void persistAlternateCsoId(List<PortalCsoDpaDetail> csoList) {
		logger.info("Inside persistAlternateCsoId service method " + csoList.size());
		for (PortalCsoDpaDetail cso : csoList) {
			logger.info("cso :::" + cso.toString());
			portalCsoDpaDetailRepository.updateAlternateCsoId(cso);
		}
	}

	public void updateStatus(String loginId, String status) {
		usermanagementRepository.updateStatus(loginId, status);
	}

	// Generate token and set in database
	public String generateToken(String loginId) {
		String token = UUIDGeneratorUtil.getUUID();
		logger.info("Generated token " + token);
		java.util.Date now = new java.util.Date();
		java.sql.Timestamp OneDaytimestamp = new java.sql.Timestamp(now.getTime());
		long oneDay = 1 * 24 * 60 * 60 * 1000;
		OneDaytimestamp.setTime(OneDaytimestamp.getTime() + oneDay);
		logger.info("OneDaytimestamp = " + OneDaytimestamp);
		int i = usermanagementRepository.updateToken(token, OneDaytimestamp, loginId);
		logger.info("Status of update token " + i);

		return token;
	}

	// get the path of user where it is deployed
	public String getWebPath(PortalLritIdMaster contractinggovernment) {
		String path = null;

		if (contractinggovernment.getCountryName().equalsIgnoreCase("India")) {
			path = usermanagementRepository.getWebPathNDC();
		} else {
			path = usermanagementRepository.getWebPathRDC();
		}
		return path;
	}

	// Send mail
	public void sendMail(String sender, String recipient, String subject, String body) throws ConnectException, Exception {
		Utility.sendMail(sender, recipient, subject, body, mailSender);
		
	}

	public PortalUser getPortalUser(String loginId) {
		logger.info("Inside getPortalUser service method . loginId " + loginId);
		PortalUser portalUser = usermanagementRepository.getUserDetails(loginId);
		// logger.info("portalUser " + portalUser.toString());
		return portalUser;

	}
	public PortalUser getActivePortalUser(String loginId) {
		logger.info("Inside getActivePortalUser service method . loginId " + loginId);
		PortalUser portalUser = usermanagementRepository.getActiveUserDetails(loginId);
		// logger.info("portalUser " + portalUser.toString());
		return portalUser;

	}

	// Check last 3 password
	public boolean checkPassword(String password, String loginId) {
		logger.info("Inside check password method password " + password + " loginId " + loginId);
		boolean flag = false;
		List<String> listpasswords = usermanagementRepository.getPassword(loginId);
		/* for(String listpassword:listpasswords) { */
		for (int i = 0; listpasswords != null && i < listpasswords.size(); i++) {
			/* if(listpassword.equals(password)) */
			logger.info("Password in database " + listpasswords.get(i));
			if (BCrypt.checkpw(password, listpasswords.get(i))) {
				logger.info("password from database " + listpasswords.get(i));
				flag = true;
				break;

			}
		}
		return flag;
	}

	// persist in portal_users_password table
	public void persistUserPassword(String loginId, String password) {
		logger.info("setUserPassword loginId " + loginId + " password " + password);
		PortalUsersPassword userpassword = new PortalUsersPassword();
		userpassword.setLoginId(loginId);
		userpassword.setPassword(password);
		userpassword.setExpiryDate(Utility.getCurrentTimeStamp());
		portalUserPasswordRepository.save(userpassword);

	}

	// update password set validatetoken to null and tokenvalidupto to null
	public void updatePassword(String loginId, String hashedPassword) {
		logger.info("Inside updatePassword method .loginId " + loginId + " Password " + hashedPassword);
		usermanagementRepository.updatePassword(loginId, hashedPassword);
	}

	/***
	 * get the basic user deatils to verify the user
	 */
	@Override
	public PortalUserInterface getPortalUserInterface(String loginId) {
		// TODO Auto-generated method stub
		PortalUserInterface portalInterface = usermanagementRepository.findByLoginId(loginId);
		logger.info("No of password attempt " + portalInterface.getNoOfPasswordAttempt() + "and Deregisterdate"
				+ portalInterface.getDeregisterDate());
		logger.info("User Status is " + portalInterface.getStatus());
		return portalInterface;
	}

	/***
	 * Get the data needs to be set in session
	 */
	@Override
	public UserSessionInterface getUserSessionByLoginId(String loginId) {
		// TODO Auto-generated method stub
		logger.info("inside get user session by loginid");
		UserSessionInterface userSessionInterface = usermanagementRepository.findUserSessionDataByLoginId(loginId);
		logger.info("Portal User from DB " + userSessionInterface);
		return userSessionInterface;
	}

	/**
	 * Update the login attempt of the user if the user tries to access the account
	 * with wrong password
	 */
	@Override
	public void updateLoginAttempt(int noOfAttempt, String loginId) {
		// TODO Auto-generated method stub
		int flag = usermanagementRepository.updateLoginAttempt(noOfAttempt, loginId);
		if (flag == 1)
			logger.info("Login Attempt updated successfully");
		else
			logger.info("Failed to update login attempt");
	}

	// check if cso is associated with any vessel
	public boolean checkCsoIdVesselMapping(String csoId) {
		return shipingCompanyRepository.getCsoVesselRelation(csoId);

	}

	// check if dpa is associated with any vessel
	public boolean checkDpaVesselMapping(String csoId) {
		return shipingCompanyRepository.getDpaVesselRelation(csoId);

	}

	@Override
	public PortalUser getUserDetailsByImo(String imoNo) {
		return usermanagementRepository.getUserDetailsByImo(imoNo);
	}

	@Override
	public PortalCsoDpaDetail getCurrentCsoDetails(String imoNo) {
		return portalCsoDpaDetailRepository.getCurrentCsoDetails(imoNo);
	}

	@Override
	public PortalCsoDpaDetail getAlternateCsoDetails(String imoNo) {
		return portalCsoDpaDetailRepository.getAlternateCsoDetails(imoNo);
	}

	@Override
	public List<String> getActivityListByType(String loginId, String type) {
		// TODO Auto-generated method stub

		return usermanagementRepository.getActivityListByType(loginId, type);
	}

	@Override
	public String getSarAuthority(String loginId) {
		// TODO Auto-generated method stub
		String sarid = portalUserSarAuthorityRepository.getSarAuthority(loginId);
		return sarid;
	}

	/*
	 * public List<PortalRoles> getAssignedRole(List<String> roleIdList){ return
	 * roleManagementRepository.getAllRole(roleIdList); }
	 */
	public List<String> getContextListByLoginId(String loginId)
	{
		return contractingGovernmentRepository.getContextListByLoginId(loginId);
	}
	public List<ContractingGovernmentInterface> getCGByContext(List<String> contextList){
		return contractingGovernmentRepository.getCGByContext(contextList);
	}
}
