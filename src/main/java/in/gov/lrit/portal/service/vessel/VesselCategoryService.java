package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;

public interface VesselCategoryService {

	public ArrayList<String> getCategoryList();
	 
}