package in.gov.lrit.portal.service.logging;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;
import in.gov.lrit.portal.model.logging.PortalLoginAudit;
import in.gov.lrit.portal.repository.logging.PortalLoggingRepository;
import in.gov.lrit.security.config.LoginSuccessHandler;



@Service
public class PortalLoggingServiceImpl implements PortalLoggingService {
@Autowired
private PortalLoggingRepository loggingRepository;
	
private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PortalLoggingServiceImpl.class);


	public PortalLoginAudit saveLoginStatus(PortalLoginAudit portalLoginAudit) {
		
		return loggingRepository.save(portalLoginAudit);
	}

	@Override
	public int UpdateGeoLocation(Integer id, String latitude, String longitude) {
		// TODO Auto-generated method stub
		String longlat= latitude+" , "+longitude;
		int flag=loggingRepository.updateGeoLocation(id, longlat);
		if(flag==1)
			logger.info("User Location updated successfully"+flag);
		else
			logger.info("User Location failed to update"+flag);
		return flag;
	}
	
	public void UpdateLogoutTime(Integer loggingid)
	{
		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp ts = new Timestamp(time);
		logger.info("Current time"+ts);
		logger.info("Current time"+loggingid);
		int flag =loggingRepository.updateLogOutTime(loggingid,ts,"logout");
		if(flag==1)
			logger.info("User Logout Time updated successfully :"+flag);
		else
			logger.info("Failed to update user logout time :"+flag);
	}
}
