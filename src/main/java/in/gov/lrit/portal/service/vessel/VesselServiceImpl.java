package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import in.gov.lrit.portal.repository.vessel.VesselRepository;
import in.gov.lrit.portal.model.vessel.PortalVesselDetail;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.model.vessel.VesselListInterface;

@Service
public class VesselServiceImpl implements VesselService{
	
	@Autowired
	private VesselRepository vesselRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(VesselServiceImpl.class);

	@Override
	public Optional<PortalVesselDetail> getByVesselId(int vesselId) {
		logger.info("inside getByVesselId() method");
		return vesselRepository.findById(vesselId);	
	}

	@Override
	public PortalVesselDetail addVessel(PortalVesselDetail pvd) {
		logger.info("inside addVessel() method");
		return vesselRepository.save(pvd);
	}

	@Override
	public Iterable<PortalVesselDetail> getAllVessel() {
		logger.info("inside getAllVessel() method");
		return vesselRepository.findAll();
	}

	@Override
	public PortalVesselDetail getVesselByImoNo(String imoNo) {
		logger.info("inside getVesselByImoNo() method");
		return vesselRepository.findVesselByImoNo(imoNo);
	}

	@Override
	public Integer getVesselIdByImoNo(String imoNo) {
		logger.info("inside getVesselIdByImoNo() method");
		return vesselRepository.findVesselIdByImoNo(imoNo);
	}

	@Override
	public boolean isIMONoExist(String imoNo) {
		logger.info("inside isIMONoExist() method");
		int result = vesselRepository.isIMONoExist(imoNo);
		return result >= 1 ? true : false;
	}

	@Override
	public boolean isMMSINoExist(String mmsiNo) {
		logger.info("inside isMMSINoExist() method");
		int result = vesselRepository.isMMSINoExist(mmsiNo);
		return result >= 1 ? true : false;
	}

	@Override
	public ArrayList<VesselListInterface> getByRegStatusAndStatus(String regStatus, String status) {
		logger.info("inside getByRegStatusAndStatus() method");
		return vesselRepository.findByRegStatusAndStatus(regStatus, status);
	}

	@Override
	public boolean isVesselSold(Integer vesselId) {
		logger.info("inside isVesselSold() method");
		int result = vesselRepository.isVesselSold(vesselId);
		return result >= 1 ? true : false;
	}

	@Override
	public boolean updateVesselRegStatus(String regStatus, Integer vesselId) {
		logger.info("inside updateVesselRegStatus() method");
		int result = vesselRepository.updateVesselRegStatus(regStatus, vesselId);
		return result >= 1 ? true : false;
	}
	
	@Override
	public boolean updateVesselStatustoInactive(String status, String regStatus, Integer vesselId, String reportingStatusChangeReason, String reportingStatusChangeRemarks) {
		logger.info("inside updateVesselStatus() method");
		int result = vesselRepository.updateVesselStatustoInactive(status, regStatus, vesselId, reportingStatusChangeReason, reportingStatusChangeRemarks);
		return result >= 1 ? true : false;
	}
	
	@Override
	public boolean updateVesselStatusToActive(String status, String regStatus, Integer vesselId) {
		logger.info("inside updateVesselStatus() method");
		int result = vesselRepository.updateVesselStatusToActive(status, regStatus, vesselId);
		return result >= 1 ? true : false;
	}

	/*@Override
	public boolean isEligibleToInactive(String shipequipId) {
		logger.info("inside isEligibleToInactive() method");
		int result = vesselRepository.isEligibleToInactive(shipequipId);
		logger.info("result "+ result);
		return result >= 1 ? true : false;
	}*/
	
	@Override
	public boolean isEligibleToInactive(Integer vesselId) {
		logger.info("inside isEligibleToInactive() method");
		int result = vesselRepository.isEligibleToInactive(vesselId);
		logger.info("result "+ result);
		return result >= 1 ? true : false;
	}

	@Override
	public boolean isEligibleToActive(Integer vesselId) {
		logger.info("inside isEligibleToActive() method");
		int result = vesselRepository.isEligibleToActive(vesselId);
		return result >= 1 ? true : false;
	}

	@Override
	public ArrayList<VesselListInterface> getByStatus(String status) {
		logger.info("inside getByStatus() method");
		return vesselRepository.findByStatus(status);
	}

	@Override
	public ArrayList<VesselListInterface> getAllVesselList() {
		logger.info("inside getAllVesselList() method");
		return vesselRepository.findAllByStatus();
	}

	@Override
	public ArrayList<PortalVesselDetail> getVesselForCompany(String companyCode) {
		logger.info("inside getVesselForCompany() method");
		return vesselRepository.findVesselForCompany(companyCode);
	}

	@Override
	public ArrayList<PortalVesselDetail> getVesselForCompanyByStatus(String companyCode, String status) {
		logger.info("inside getVesselForCompanyByStatus() method");
		return vesselRepository.findVesselForCompanyByStatus(companyCode, status);
	}
	
	/*@Override
	public ArrayList<PortalVesselDetail> getVesselForCompanyByStatusAndRegStatus(String companyCode, String status, String regStatus) {
		logger.info("inside getVesselForCompanyByStatusAndRegStatus() method");
		return vesselRepository.findVesselForCompanyByStatusAndRegStatus(companyCode, status, regStatus);

	}

	@Override
	public ArrayList<PortalVesselDetail> getVesselForCompanyByRegStatus(String companyCode) {
		logger.info("inside getVesselForCompanyByRegStatus() method");
		return vesselRepository.findVesselForCompanyByRegStatus(companyCode);
	}*/
	
	@Override
	public ArrayList<VesselListInterface> getVesselForCompanyByStatusAndRegStatus(String companyCode, String status, String regStatus) {
		logger.info("inside getVesselForCompanyByStatusAndRegStatus() method");
		return vesselRepository.findVesselForCompanyByStatusAndRegStatus(companyCode, status, regStatus);

	}

	@Override
	public ArrayList<VesselListInterface> getVesselForCompanyByRegStatus(String companyCode) {
		logger.info("inside getVesselForCompanyByRegStatus() method");
		return vesselRepository.findVesselForCompanyByRegStatus(companyCode);
	}

	@Override
	public ArrayList<VesselListInterface> getByRegStatus() {
		logger.info("inside getByRegStatus() method");
		return vesselRepository.findByRegStatus();
	}

	@Override
	public ArrayList<PortalVesselDetail> getVesselForCompanyByLoginId(String loginId) {
		logger.info("inside getVesselForCompanyByLoginId() method");
		return vesselRepository.findVesselForCompanyByLoginId(loginId);
	}


	@Override
	public ArrayList<VesselListInterface> getVesselForCompanyByStatusAndRegStatusAndLoginId(String loginId,
			String status, String regStatus) {
		logger.info("inside getVesselForCompanyByStatusAndRegStatusAndLoginId() method");
		return vesselRepository.findVesselForCompanyByStatusAndRegStatusAndLoginId(loginId, status, regStatus);
	}

	@Override
	public ArrayList<VesselListInterface> getAllActiveVessel(String status, List<String> regStatus) {
		logger.info("inside getAllActiveVessel() method");
		return vesselRepository.findAllActiveVessel(status, regStatus);
	}

	@Override
	public ArrayList<VesselListInterface> getAllActiveVesselForCompany(String companyCode,
			String status, List<String> regStatus) {
		logger.info("inside getAllActiveVesselForCompany() method");
		return vesselRepository.findAllActiveVesselForCompany(companyCode, status, regStatus);
	}

	@Override
	public ArrayList<VesselListInterface> getAllActiveVesselForCompanyByLoginId(String loginId,
			String status, List<String> regStatus) {
		logger.info("inside getAllActiveVesselForCompanyByLoginId() method");
		return vesselRepository.findAllActiveVesselForCompanyByLoginId(loginId, status, regStatus);
	}

	@Override
	public ArrayList<VesselListInterface> getVesselForCompanyByRegStatusAndLoginId(String loginId) {
		logger.info("inside getVesselForCompanyByRegStatusAndLoginId() method");
		return vesselRepository.findVesselForCompanyByRegStatusAndLoginId(loginId);
	}

	@Override
	public ArrayList<VesselListInterface> getRepurchaseVesselList(String companyCode) {
		logger.info("inside getRepurchaseVesselList() method");
		return vesselRepository.findRepurchaseVesselList(companyCode);
	}

	/*@Override
	public ArrayList<VesselListInterface> getRepurchaseVesselListByLoginId(String loginId) {
		logger.info("inside findRepurchaseVesselListByLoginId() method");
		return vesselRepository.findRepurchaseVesselListByLoginId(loginId);
	}*/
	
}