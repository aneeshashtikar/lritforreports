package in.gov.lrit.portal.service.vessel;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.VesselTypeRepository;
import in.gov.lrit.portal.model.vessel.PortalVesselType;

@Service
public class VesselTypeServiceImpl implements VesselTypeService{

	@Autowired
	private VesselTypeRepository typeRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(VesselTypeServiceImpl.class);
	
	@Override
	public ArrayList<String> getVesselTypeList() {
		logger.info("Inside vessel type service method");
		return typeRepo.findAllVesselType();
	}

	@Override
	public PortalVesselType getByType(String vesselType) {
		logger.info("Inside vessel type service method");
		return typeRepo.findByType(vesselType);
	}

	@Override
	public Iterable<PortalVesselType> getAllVesselType() {
		logger.info("Inside vessel type service method");
		return typeRepo.findAll();
	}

	@Override
	public Optional<PortalVesselType> getVesselTypeById(String vesselTypeId) {
		logger.info("Inside vessel type service method");
		return typeRepo.findById(vesselTypeId);
	}

	@Override
	public String getIdByType(String vesselType) {
		logger.info("Inside vessel type service method");
		return typeRepo.findIdByType(vesselType);
	}

}