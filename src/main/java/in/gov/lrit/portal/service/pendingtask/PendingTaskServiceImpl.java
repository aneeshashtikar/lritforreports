package in.gov.lrit.portal.service.pendingtask;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;

import org.imo.gisis.xml.lrit.ddprequest._2008.DDPRequestType;
import org.imo.gisis.xml.lrit.positionrequest._2008.ShipPositionRequestType;
import org.imo.gisis.xml.lrit.surpicrequest._2014.SURPICRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.gov.lrit.portal.CommonUtil.ICommonUtil;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in.gov.lrit.portal.model.pendingtask.PortalShipEquipementHst;
import in.gov.lrit.portal.model.pendingtask.ShipEquipmentHstInterface;
import in.gov.lrit.portal.model.request.PortalSurpicRequest;
import in.gov.lrit.portal.model.vessel.EditVesselInterface;
import in.gov.lrit.portal.model.vessel.PortalShipEquipment;
import in.gov.lrit.portal.model.vessel.PortalVesselDetailHistory;
import in.gov.lrit.portal.model.vessel.ShipEquipmentIInterface;
import in.gov.lrit.portal.repository.contractinggovernment.ContractingGovernmentRepository;
import in.gov.lrit.portal.repository.pendingtask.PendingTaskMasterRepository;
import in.gov.lrit.portal.repository.pendingtask.ShipequipmentHistoryRepository;
import in.gov.lrit.portal.repository.requests.PortalRequestRepository;
import in.gov.lrit.portal.repository.requests.SurpicRequestRepository;
import in.gov.lrit.portal.repository.vessel.ShipEquipmentRepository;
import in.gov.lrit.portal.repository.vessel.VesselDetailHistoryRepository;
import in.gov.lrit.portal.repository.vessel.VesselDetailRepository;
import in.gov.lrit.portal.repository.vessel.VesselReportingStatusHistoryRepository;
import in.gov.lrit.portal.service.request.IRequestmanagement;
import in.gov.lrit.portal.service.request.SoapConnector;
import in.gov.lrit.portal.service.user.IUserManagementService;
import in.gov.lrit.portal.service.vessel.VesselDetailService;
import in_.gov.lrit.session.ContractingGovernmentInterface;
import in_.gov.lrit.session.UserSessionInterface;

@Service
public class PendingTaskServiceImpl implements PendingTaskService {

	@Autowired
	private PendingTaskMasterRepository pendingTaskMasterRepository;

	@Autowired
	private PortalRequestRepository portalRequestRepository;

	@Autowired
	private SoapConnector soapConnector;

	@Autowired
	private VesselDetailRepository vesselDetailRepository;

	@Autowired
	private VesselDetailHistoryRepository vesselDetailHistoryRepository;

	@Autowired
	private IRequestmanagement manageRequest;

	@Autowired
	private ShipEquipmentRepository shipEquipmentRepository;

	@Autowired
	private VesselDetailService vesselDetailService;

	@Autowired
	private ShipequipmentHistoryRepository shipequipmentHistoryRepository;
	
	@Autowired
	private SurpicRequestRepository surpicRequestRepository;

	@Autowired
	private ICommonUtil commonutil;
	
	@Autowired
	private IUserManagementService iUserManagementService;
	
	@Autowired
	private VesselReportingStatusHistoryRepository vesselReportingStatusHistoryRepository;
	
	@Autowired
	private ContractingGovernmentRepository contractingGovernmentRepository;
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PendingTaskServiceImpl.class);

	public Map<String, Integer> getCategoryWiseAlertCount() {
		List<Object[]> list = pendingTaskMasterRepository.getCategoryWiseAlertCount();
		Integer totalCount = 0;
		Map<String, Integer> map = new HashMap<>();
		for (Object[] obj : list) {
			++totalCount;
			String category = (String) obj[0];
			BigInteger count = (BigInteger) obj[1];
			logger.info("category " + category);
			logger.info("count" + count.intValue());
			map.put(category, count.intValue());
		}
		logger.info(" " + totalCount);
		map.put("totalCount", totalCount);
		return map;
	}

	public int getPendingTaskCount(List<String> requesterLoginId, List<String> category) {
		int count = pendingTaskMasterRepository.getPendingTaskCountForLoginUser(requesterLoginId, category);
		return count;
	}

	public List<PortalPendingTaskMst> getRequestPendingTask(String countryLritId, List<String> category) {
		logger.info("inside pending task Service");
		List<PortalPendingTaskMst> list = pendingTaskMasterRepository.getRequestPendingTask(countryLritId, category);
		return list;
	}

	@Override
	public String acceptRequest(String messageId, String category) throws SOAPException, JAXBException, Exception {
		// TODO Auto-generated method stub
		logger.info("Accepting portal request for messageid : " + messageId);
		String requestPayload = portalRequestRepository.findrequestPayloadById(messageId);
		logger.info("Request packet : " + requestPayload);
		logger.info("REQUEST CATEGORY" + category);

		String response = null;
		switch (category) {
		case "DDP_Request":
			DDPRequestType ddprequest = new DDPRequestType();
			ddprequest = genericUnmarshll(DDPRequestType.class, requestPayload);
			response = soapConnector.callWebService(ddprequest);
			break;
		case "Flag_Request":
			ShipPositionRequestType flagrequest = new ShipPositionRequestType();
			flagrequest = genericUnmarshll(ShipPositionRequestType.class, requestPayload);
			response = soapConnector.callWebService(flagrequest);
			break;
		case "Port_Request":
			ShipPositionRequestType portRequest = new ShipPositionRequestType();
			portRequest = genericUnmarshll(ShipPositionRequestType.class, requestPayload);
			response = soapConnector.callWebService(portRequest);
			break;
		case "SARSurpic_Request":
			/*
			 * SURPICRequest sarSurpicRequest = new SURPICRequest(); sarSurpicRequest =
			 * genericUnmarshll(SURPICRequest.class, requestPayload); response =
			 * soapConnector.callWebService(sarSurpicRequest);
			 */
			response = sarSurpicRequestGenerate(messageId);
			break;
		case "CoastalSurpic_Request":
			/*
			 * SURPICRequest coastalSurpicRequest = new SURPICRequest();
			 * coastalSurpicRequest = genericUnmarshll(SURPICRequest.class, requestPayload);
			 * response = soapConnector.callWebService(coastalSurpicRequest);
			 */
			response = sarSurpicRequestGenerate(messageId);
			break;
		case "SAR_Requests":
			ShipPositionRequestType sarRequest = new ShipPositionRequestType();
			sarRequest = genericUnmarshll(ShipPositionRequestType.class, requestPayload);
			response = soapConnector.callWebService(sarRequest);
			break;
		case "Coastal_Requests":
			ShipPositionRequestType coastalRequest = new ShipPositionRequestType();
			coastalRequest = genericUnmarshll(ShipPositionRequestType.class, requestPayload);
			response = soapConnector.callWebService(coastalRequest);
			break;
		}

		// pendingTaskMasterRepository.updatePendingTaskStatus("approve",
		// approverLoginId, date, pendinTaskId)
		return response;
	}

	@Override
	public PortalPendingTaskMst addEntryInPendingMst(PortalPendingTaskMst pendingtask) {
		logger.info("Inside Pensing Task Master Service");
		return pendingTaskMasterRepository.save(pendingtask);
	}

	/***
	 * This is a generic method to unmarshall a string type to class type
	 * 
	 * @param requestClass
	 * @param requestString
	 * @return
	 * @throws JAXBException
	 */
	public static <T> T genericUnmarshll(Class<T> requestClass, String requestString) {
		logger.info("generic string to class converter ");

		T requestPayload = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(requestClass);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// Overloaded methods to unmarshal from different xml sources
			requestPayload = requestClass.cast(jaxbUnmarshaller.unmarshal(new StringReader(requestString)));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return requestPayload;

	}

	@Override
	public int updatePendingTaskStatus(String status, String approverLoginId, Timestamp date, Integer pendinTaskId,
			String comment) {
		// TODO Auto-generated method stub

		int flag = pendingTaskMasterRepository.updatePendingTaskStatus(status, approverLoginId, date, pendinTaskId,
				comment);
		return flag;
	}

	@Override
	public int updateRequestStatus(String messageId, String approversLoginId, String status, Timestamp statusDate,
			String responsePayload) {
		// TODO Auto-generated method stub
		return portalRequestRepository.updateRequestStatus(messageId, approversLoginId, status, statusDate,
				responsePayload);
	}

	@Override
	public String acceptEditVessel(Integer vesselId, Integer pendingTaskId) {
		// TODO Auto-generated method stub
		try {
			logger.info("Edit vessel pending task");
			EditVesselInterface vessel = vesselDetailRepository.findByVesselId(vesselId);
			String vesselCallSignOld = vessel.getCallSign();
			String vesselNameOld = vessel.getVesselName();
			logger.info("Vessel Details in portal_vessel_details - callsign" + vesselCallSignOld
					+ "vessel name " + vesselNameOld);
			
			PortalVesselDetailHistory vesselHistory = vesselDetailHistoryRepository.findByPendingTaskId(pendingTaskId);
			String vesselCallSignNew = vesselHistory.getCallSign();
			String vesselvesselNameNew = vesselHistory.getVesselName();
			logger.info("Vessel Details in portal_vessel_details_history - callsign" + vesselCallSignOld
					+ "vessel name " + vesselvesselNameNew);
			int flag =vesselDetailRepository.updateEditVessel(vesselId, vesselvesselNameNew, vesselCallSignNew);
			if(flag==1)
			{
				logger.debug("New vessel details updated in vessel_details");	
				flag= vesselDetailHistoryRepository.updatevesselDetail(pendingTaskId, vesselNameOld, vesselCallSignOld);
				if(flag==1) {
					logger.debug("old vessel detail updated in history");
				    return "Vessel Details updated Successfully. ";
				}else{	
					logger.debug("failed to update vessel detail in history");
					return "Failed to Accept vessel details. ";
				}
			}
			else
				return "Failed to Accept vessel details. ";
				
			
		} catch (NullPointerException e) {
			e.printStackTrace();
			return "No vessel Found with vesselId" + vesselId;
			// TODO: handle exception
		}
	}

	@Override
	public String acceptSoldScrapVessel(Integer vesselId, String status) {
		String message = null;
		logger.info("updating vessel registration status to: "+status);
		int flag = vesselDetailRepository.updateVesselRegistrationStatus(vesselId, status);
		if (flag == 1)
		{
			logger.debug("vessel registration status updated succefully to: "+status);
			message = "Vesssel status updated succefully to " + status;
		}
		else
		{	
			logger.debug("Failed to update vessel registration status to: "+status);
			message = "Failed to update the vessel to " + status;
		}
		return message;
	}

	@Override
	public String acceptActivetoInactiveVessel(Integer vesselId, String status) {
		String message;
		logger.info("updating vessel reporting status to: "+status);
		int flag = vesselDetailRepository.updateVesselStatusAndRegistrationStatus(vesselId, status);
		if (flag == 1) 
		{
			logger.debug("vessel reporting status active to inactive updated succefully to: "+status);
			message = "Vessel Status Changed Active to Inactive Successfully";
		}
		else
		{	
			logger.debug("vessel reporting status updated succefully to: "+status);
			message = "Failed to update Vessel Status";
		}
		return message;
	}

	@Override
	public String acceptInactiveteActiveVessel(Integer vesselId, String status) {
		String message;
		logger.info("updating vessel reporting status to: "+status);
		int flag = vesselDetailRepository.updateVesselStatus(vesselId, status);
		if (flag == 1)
		{
			logger.debug("vessel reporting status inactive to active updated succefully to: "+status);
			message = "Vessel Status Changed Inactive to Active Successfully";
		}
		else
		{
			logger.debug("vessel reporting status updated succefully to: "+status);
			message = "Failed to update Vessel Status";
		}
			return message;
	}

	@Override
	public String acceptDeleteDnid(Integer vesselId, String status) {
		// TODO Auto-generated method stub
		String message;
		logger.info("Accepting Delete DNID request for vesselid: " + vesselId);
		// saveInShipEquipmentHistory(vesselId, status);
		List<String> notInRegStatus = new ArrayList<String>();
		notInRegStatus.add("SEID_DELETED");
		notInRegStatus.add("SOLD");
		notInRegStatus.add("SCRAPPED");
		notInRegStatus.add("INACTIVE");
		notInRegStatus.add("DNID_DELETED");
		boolean state = vesselDetailRepository.checkNotInStatusInRegistrationStatus(notInRegStatus, vesselId);
		if (state) {
			logger.info("Cannot perform delete dnid on vesselId: "+vesselId+" because dnid is already deleted for the vessel");
			logger.info("Status of the vessel can be sold, scrapped, seid deleted, inactive, delete dnid");
			message = "This action cannot be performed DNID is already deleted for the ship";
		} else {
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes(); // get
			// session.
			HttpSession session = attr.getRequest().getSession();
			UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
			PortalLritIdMaster lritIdmaster = userSession.getRequestorsLritId(); // get lritid from session
			String requestorLritid = lritIdmaster.getLritId();

			BigInteger messagetype = BigInteger.valueOf(18);
			BigInteger requesttype = BigInteger.valueOf(56);
			logger.debug("Calling manage DNID with messagetype: " + messagetype + "requesttype : " + requesttype + ":"
					+ "requestorLritid: " + requestorLritid + "for vesselid: " + vesselId);
			boolean flag = vesselDetailService.manageDNID(vesselId, requestorLritid, messagetype, requesttype); // take
																												// lrit
																												// from
			if (flag) {
				logger.info("DNID Delete Request sent to ASP succefully.");
				message = "DNID Delete Request Sent Successsfully";
				// shipEquipmentRepository.updateDnidAndMemberNo(vesselId, null, null);
				int updateFlag = vesselDetailRepository.updateVesselRegistrationStatus(vesselId, "DNID_DEL_REQ");
				if (updateFlag == 1)
					logger.info("Vessel Registration status updated to : DNID_DEL_REQ Succesfully");
			} else
				message = "Error Occured while sending the request";
		}
		return message;
	}

	@Override
	public String acceptDeleteSeid(Integer vesselId, String status) {
		// TODO Auto-generated method stub
		String message;
		List<String> notInRegStatus = new ArrayList<String>();
		notInRegStatus.add("SEID_DELETED");
		notInRegStatus.add("SOLD");
		notInRegStatus.add("SCRAPPED");
		notInRegStatus.add("INACTIVE");
		boolean state = vesselDetailRepository.checkNotInStatusInRegistrationStatus(notInRegStatus, vesselId);
		if (state) {
			logger.info("Cannot perform delete SEID on vesselId: "+vesselId+" because SEID is already deleted for the vessel");
			logger.info("Status of the vessel can be sold, scrapped, seid deleted, inactive, delete dnid");
			message = "This action cannot be performed SEID is already deleted for the ship";
		} else {
		// saveInShipEquipmentHistory(vesselId, status);
		logger.info("Ship equipment data deleted for vesselid " + vesselId);
		try {
		shipEquipmentRepository.deleteShipEquipmentDetails(vesselId);
		vesselDetailRepository.updateVesselRegistrationStatus(vesselId, "SEID_DELETED");
		message = "SEID Deleted Successfully";
		}		
		catch(Exception e)
		{
			e.printStackTrace();
			message="Error Occurred while deleting SEID";
		}
		}
	
		return message;
	}

	public void saveInShipEquipmentHistory(Integer vesselId, String status) {

		PortalShipEquipment shipEquipment = shipEquipmentRepository.findByvesselId(vesselId);
		logger.info("Saving ship equipment data in Ship_equipment_hst table " + shipEquipment);
		PortalShipEquipementHst portalShipEquipementHst = new PortalShipEquipementHst();
		portalShipEquipementHst.setDnidNo(shipEquipment.getDnidNo());
		portalShipEquipementHst.setImoNo(shipEquipment.getImoNo());
		portalShipEquipementHst.setMemberNo(shipEquipment.getMemberNo());
		portalShipEquipementHst.setModelId(shipEquipment.getModelDet().getModelId());
		portalShipEquipementHst.setOceanRegion(shipEquipment.getOceanRegion());
		portalShipEquipementHst.setResponsePayload(shipEquipment.getResponsePayload());
		portalShipEquipementHst.setRequestPayload(shipEquipment.getRequestPayload());
		portalShipEquipementHst.setShipborneEquipmentId(shipEquipment.getShipborneEquipmentId());
		portalShipEquipementHst.setStatus(status);
		portalShipEquipementHst.setVesselId(vesselId);
		shipequipmentHistoryRepository.save(portalShipEquipementHst);
		logger.info("Saved Ship equipment details in shipequipment history");
	}

	@Override
	public String acceptReDownloadDnid(Integer vesselId, String status) {
		// TODO Auto-generated method stub
		/** Here VesselId is pk of portal_ship_equipement_hst table 
		 * which is refereced in portal_pending_task_mst
		 * 
		 */
		String message = null;
		try {

			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes(); // get
																														// session.
			HttpSession session = attr.getRequest().getSession();
			UserSessionInterface userSession = (UserSessionInterface) session.getAttribute("USER");
			PortalLritIdMaster lritIdmaster = userSession.getRequestorsLritId(); // get lritid from session
			String requestorLritid = lritIdmaster.getLritId();
			
			ShipEquipmentIInterface shipEquipmentInterface = shipEquipmentRepository.findShipIntefaceByVesselId(vesselId);
			Integer memberNoOld = shipEquipmentInterface.getMemberNo();
			Integer dnidNoOld = shipEquipmentInterface.getDnidNo();

			ShipEquipmentHstInterface shipEquipmentHstInterface = shipequipmentHistoryRepository
					.getDnidMemberNo(vesselId);
			Integer memberNoNew = shipEquipmentHstInterface.getMemberNo();
			Integer dnidNoNew = shipEquipmentHstInterface.getDnidNo();

			shipEquipmentRepository.updateDnidAndMemberNo(vesselId, memberNoNew, dnidNoNew);
			shipequipmentHistoryRepository.updateDnidMemberNo(vesselId, memberNoOld, dnidNoOld);

			// calling function for dnid download function
			BigInteger messagetype = BigInteger.valueOf(18);
			BigInteger requesttype = BigInteger.valueOf(51);

			boolean flag = vesselDetailService.manageDNID(vesselId, requestorLritid, messagetype, requesttype);
			if (flag) {
				vesselDetailRepository.updateVesselRegistrationStatus(vesselId, "DNID_DW_REQ");
				message = "DNID Download Request Sent Successfully";
			} else {
				message = "Error Occured while processing the requesrt";
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			message = "Error Occurred while processing the request";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Error Occurred while processing the request";
		}
		return message;
	}
	
	public String sarSurpicRequestGenerate(String messageId)
	{
		String responseStatus=null;
		SURPICRequest  dcSurpicRequest= new SURPICRequest();
		BigInteger messageType = BigInteger.valueOf(6L);
		BigInteger accessType = BigInteger.valueOf(1L);
		String configParaTest = commonutil.getParavalues("Test");
		String configParaSchemaVersion = commonutil.getParavalues("Schema_Version");

		BigInteger test=new BigInteger(configParaTest);
		BigDecimal schemaVersion=new BigDecimal(configParaSchemaVersion);
		
		PortalSurpicRequest portalSurpicRequest=	surpicRequestRepository.findByMessageId(messageId);
		
		String dataUserRequesterLoginid=portalSurpicRequest.getPortalRequest().getRequestorsLoginId();
		String sarAuthority=iUserManagementService.getSarAuthority(dataUserRequesterLoginid);
		
		Timestamp time= portalSurpicRequest.getPortalRequest().getRequestGenerationTime();
		XMLGregorianCalendar gregorianTimeStamp=commonutil.getgregorianTimeStamp(time);
		dcSurpicRequest.setCircularArea(portalSurpicRequest.getCircularArea());
		dcSurpicRequest.setRectangularArea(portalSurpicRequest.getRectangularArea());
		dcSurpicRequest.setTest(test);
		dcSurpicRequest.setSchemaVersion(schemaVersion);
		dcSurpicRequest.setNumberOfPositions(portalSurpicRequest.getNumberOfPosition());
		dcSurpicRequest.setMessageId(messageId);
		dcSurpicRequest.setMessageType(messageType);
		dcSurpicRequest.setAccessType(accessType);
		dcSurpicRequest.setDataUserRequestor(sarAuthority);
		dcSurpicRequest.getShipTypes();
		dcSurpicRequest.setDataUserProvider(portalSurpicRequest.getDataUserProvider());
		dcSurpicRequest.setTimeStamp(gregorianTimeStamp);
		dcSurpicRequest.setDDPVersionNum(portalSurpicRequest.getPortalRequest().getCurrentddpversion());
		
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(SURPICRequest.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(dcSurpicRequest, sw);
			logger.info("surpic request payload:"+sw.toString());	
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			responseStatus="Error Occurred in marshalling";
			e.printStackTrace();
			return responseStatus;
		}
	
		try {
			responseStatus = soapConnector.callWebService( dcSurpicRequest);
			if (responseStatus.equalsIgnoreCase("true"))
				manageRequest.setResponsePayload("success", messageId);
			if  (!(responseStatus.equalsIgnoreCase("true")))
				manageRequest.setResponsePayload(responseStatus, messageId);
		} catch (Exception e) {
			responseStatus="Error Occurred calling DC";
			e.printStackTrace();
			return responseStatus;
		}
		
		return responseStatus;
		
	}

	@Override
	public byte[] getpendingTaskDocument(Integer pendingTaskId) {
		// TODO Auto-generated method stub
		return vesselReportingStatusHistoryRepository.findsupportingDocumentbypendingTaskId(pendingTaskId);
	}

	

}
