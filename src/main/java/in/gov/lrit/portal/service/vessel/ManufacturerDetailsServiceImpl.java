package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.ManufacturerDetailsRepository;
import in.gov.lrit.portal.model.vessel.ManufacturerList;
import in.gov.lrit.portal.model.vessel.PortalManufacturerDetails;

@Service
public class ManufacturerDetailsServiceImpl implements ManufacturerDetailsService{

	@Autowired
	private ManufacturerDetailsRepository repo;
	
	private static final Logger logger = LoggerFactory.getLogger(ManufacturerDetailsServiceImpl.class);
	
	@Override
	public PortalManufacturerDetails addManufacturer(PortalManufacturerDetails manu) {
		logger.info("inside addManufacturer() method");
		return repo.save(manu);
	}

	@Override
	public Iterable<PortalManufacturerDetails> getAllManufacturer() {
		String status = "Active";
		logger.info("inside getAllManufacturer() method");
		return repo.findBystatus(status);
	}
	
	@Override
	public boolean isManufacturerExist(String manufacturerName) {
		logger.info("inside model method");
		int result =  repo.isManufacturerExist(manufacturerName);
		return result >= 1 ? true : false;
	}

	@Override
	public boolean updateManuById(Integer manufacturerId) {
		logger.info("inside updateManuById() method");
		int result =  repo.updateManuById(manufacturerId);
		return result >= 1 ? true : false;
	}

	@Override
	public Optional<PortalManufacturerDetails> findById(Integer manufacturerId) {
		logger.info("inside findById() method");
		return repo.findBymanufacturerId(manufacturerId);
	}
	
	@Override
	public ArrayList<String> getmanufacturerName() {
		logger.info("inside getmanufacturerName() method");
		return repo.findmanufacturerName();
	}
	
	@Override
	public PortalManufacturerDetails getBymanufacturerName(String manufacturerName) {
		logger.info("inside getBymanufacturerName() method");
		return repo.findBymanufacturerName(manufacturerName);
	}

	@Override
	public List<ManufacturerList> getAll() {
		logger.info("inside getAll() method");
		return repo.findByStatus("Active");
	}

}