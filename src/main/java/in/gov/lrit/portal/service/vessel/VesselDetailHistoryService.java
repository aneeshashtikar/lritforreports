package in.gov.lrit.portal.service.vessel;

import in.gov.lrit.portal.model.vessel.PortalVesselDetailHistory;
import in.gov.lrit.portal.model.vessel.PortalVesselReportingStatusHistory;

public interface VesselDetailHistoryService {
	
	public PortalVesselDetailHistory addVesselDetailHst(PortalVesselDetailHistory vesselDetHst);

}
