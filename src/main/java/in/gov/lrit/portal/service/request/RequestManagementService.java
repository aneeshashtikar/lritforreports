//this class is used to save the request.
package in.gov.lrit.portal.service.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;
import in.gov.lrit.portal.model.request.PortalCoastalRequest;
import in.gov.lrit.portal.model.request.PortalDdpRequest;
import in.gov.lrit.portal.model.request.PortalFlagRequest;
import in.gov.lrit.portal.model.request.PortalPortRequest;
import in.gov.lrit.portal.model.request.PortalSarRequest;
import in.gov.lrit.portal.model.request.PortalSurpicRequest;
import in.gov.lrit.portal.repository.pendingtask.PendingTaskMasterRepository;
import in.gov.lrit.portal.repository.requests.CoastalRequestRepository;
import in.gov.lrit.portal.repository.requests.DdpRequestRepository;
import in.gov.lrit.portal.repository.requests.FlagRequestRepository;
import in.gov.lrit.portal.repository.requests.PortalRequestRepository;
import in.gov.lrit.portal.repository.requests.PortRequestRepository;
import in.gov.lrit.portal.repository.requests.SarRequestRepository;
import in.gov.lrit.portal.repository.requests.SurpicRequestRepository;


@Service
public class RequestManagementService implements IRequestmanagement {
	@Autowired
	DdpRequestRepository ddprequest;
	@Autowired
	PortalRequestRepository portalrequest;
	@Autowired
	FlagRequestRepository flagrequest;

	@Autowired
	PortRequestRepository portrequest;

	@Autowired
	SurpicRequestRepository surpic;

	@Autowired
	SarRequestRepository sar;

	@Autowired
	CoastalRequestRepository coastal;

	@Autowired
	PendingTaskMasterRepository pendingtask;
	
	@Autowired
	PortalRequestRepository portalreq;

	@Override
	public void saveddprequest(PortalDdpRequest portalddprequest) {
		ddprequest.save(portalddprequest);

	}

	@Override
	public void saveflagrequest(PortalFlagRequest portalflagrequest) {
		flagrequest.save(portalflagrequest);
	}

	@Override
	public void saveportrequest(PortalPortRequest portalportrequest) {
		portrequest.save(portalportrequest);
	}

	@Override
	public void savesurpicrequest(PortalSurpicRequest portalsurpicrequest) {
		surpic.save(portalsurpicrequest);
	}

	@Override
	public void savesarrequest(PortalSarRequest portalsarrequest) {
		sar.save(portalsarrequest);
	}

	@Override
	public void savecoastalrequest(PortalCoastalRequest portalcoastalrequest) {
		coastal.save(portalcoastalrequest);
	}

	public void savependingtask(PortalPendingTaskMst portalpendingtask)
	{
		pendingtask.save(portalpendingtask);
	}

	@Override
	public void setResponsePayload(String response, String messageid) {
		// TODO Auto-generated method stub
		portalreq.setResponsePayload(response, messageid);
	}


}
