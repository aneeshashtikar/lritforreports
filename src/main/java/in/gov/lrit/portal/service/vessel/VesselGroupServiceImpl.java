package in.gov.lrit.portal.service.vessel;

import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.portal.repository.vessel.VesselGroupRepository;
import in.gov.lrit.portal.model.vessel.GroupList;
import in.gov.lrit.portal.model.vessel.PortalVesselGroup;

@Service
public class VesselGroupServiceImpl implements VesselGroupService{

	@Autowired
	private VesselGroupRepository vesselGroupRepo;

	private static final Logger logger = LoggerFactory.getLogger(VesselGroupServiceImpl.class);

	@Override
	public PortalVesselGroup addVesselGroup(PortalVesselGroup vesselGroup) {

		logger.info("inside addVesselGroup() method");
		logger.info(vesselGroup.toString());
		return vesselGroupRepo.save(vesselGroup);
	}

	@Override
	public Iterable<PortalVesselGroup> findAllGroup() {
		logger.info("inside findAllGroup() method");
		return vesselGroupRepo.findAll();
	}

	@Override
	public Long findGroupIdBygrpName(String groupName) {
		logger.info("inside findGroupIdBygrpName() method");
		return vesselGroupRepo.findGroupIdByName(groupName);
	}

	@Override
	public boolean updateGroupDetail(String shortName, String description, Long groupId) {
		logger.info("inside updateGroupDetail() method");
		int result = vesselGroupRepo.updateVesselGroup(shortName, description, groupId);
		return result >=1 ? true : false;
	}

	@Override
	public boolean isGroupExist(String groupName) {
		logger.info("inside isGroupExist() method");
		int result = vesselGroupRepo.countBygroupName(groupName);
		return result >=1 ? true : false;
	}

	@Override
	public Optional<PortalVesselGroup> findById(Long groupId) {
		logger.info("inside findById() method");
		return vesselGroupRepo.findBygroupId(groupId);
	}

	@Override
	public PortalVesselGroup findBygroupName(String groupName) {
		logger.info("inside findBygroupName() method");
		return vesselGroupRepo.findBygroupName(groupName);
	}

	@Override
	public boolean isShortNameExist(String groupName, String shortName) {
		logger.info("inside isShortNameExist() method");
		int result = vesselGroupRepo.countBygroupNameAndShortName(groupName, shortName);
		return result >= 1 ? true : false;
	}

	@Override
	public ArrayList<String> groupNameList() {
		logger.info("inside groupNameList() method");
		return vesselGroupRepo.findAllGroupName();
	}

	@Override
	public ArrayList<GroupList> getAllGroup() {
		logger.info("inside getAllGroup() method");
		return vesselGroupRepo.findAllBy();
	}

	@Override
	public boolean isStringOnlyAlphabet(String str) 
	{ 
		logger.info("inside isStringOnlyAlphabet() method");
		return ((!str.equals("")) 
				&& (str != null) 
				&& (str.matches("^[a-zA-Z]*$"))); 
	}
}