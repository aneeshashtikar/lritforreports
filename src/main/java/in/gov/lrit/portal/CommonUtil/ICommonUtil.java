package in.gov.lrit.portal.CommonUtil;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import in.gov.lrit.gis.ddp.model.DdpVersion;

public interface ICommonUtil {


	public String generateMessageID(String cglrit) ;

	public Timestamp getCurrentTimeStamp();

	public XMLGregorianCalendar getgregorianTimeStamp(Timestamp ts);

	public BigDecimal getSchemaVersion();

	public Timestamp getDatetoTimestamp(Date date);

	public String getParavalues(String paraname);

	public String getDDPversion(List<DdpVersion> ddpversion);

	public String getDDPversionDate(List<DdpVersion> ddpversion); 
	
	public String getDDPRegularversion(List<DdpVersion> ddpversion); 
}
