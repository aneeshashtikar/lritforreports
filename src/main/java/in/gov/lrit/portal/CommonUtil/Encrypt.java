package in.gov.lrit.portal.CommonUtil;

public class Encrypt {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger( Encrypt.class);
		public static String encryptString(String plainText){
			logger.info("Inside encryptString method");
			String encryptedPassword=null;
			try {
					encryptedPassword = org.apache.commons.codec.digest.DigestUtils.sha256Hex(plainText);  
					logger.info(" encryptedPassword ="+encryptedPassword);
			} catch (Exception e) {
				logger.error("Error"+e);
			}
			return encryptedPassword;
		}
}
