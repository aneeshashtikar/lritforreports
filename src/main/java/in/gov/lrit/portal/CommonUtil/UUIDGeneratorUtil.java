/**
 * @UUIDGeneratorUtil.java
 * @copyright 2017 CDAC Mumbai. All rights reserved.
 * @author  eSangam Team
 * @version 1.0
 */
package in.gov.lrit.portal.CommonUtil;

import org.safehaus.uuid.UUIDGenerator;

public class UUIDGeneratorUtil {

	public static String getUUID(){
		UUIDGenerator uuid = UUIDGenerator.getInstance();

		String id = uuid.generateRandomBasedUUID().toString() ;

		String uuid1 = "" ;
		for(int i=0;i<id.length();i++){
			if( id.charAt(i) == '-' ){
				continue;
			}else{
				uuid1 += id.charAt(i);	
			}	
		}	
		return uuid1;
	}

}

