package in.gov.lrit.portal.CommonUtil;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.lrit.gis.ddp.model.DdpVersion;
import in.gov.lrit.portal.repository.requests.ConfigParaRepository;


@Service
public class CommonUtilService implements ICommonUtil {

	@Autowired
	ConfigParaRepository config;


	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CommonUtilService.class);

	// TODO Auto-generated method stub
	@Override
	public String generateMessageID(String cglrit)  
	{

		LocalDateTime ldt = LocalDateTime.now();
		int date = ldt.getDayOfMonth();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		String instance = dtf.format(ldt);
		String random=config.getNextSeriesId();
		String id = cglrit + instance +random;
		return id;
	}


	@Override
	public Timestamp getCurrentTimeStamp() {
		// TODO Auto-generated method stub
		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp ts = new Timestamp(time);
		logger.info("Current Time Stamp : "+ts); 
		return ts;
	}

	@Override
	public XMLGregorianCalendar getgregorianTimeStamp(Timestamp ts) {
		// TODO Auto-generated method stub
		Date date = ts;
		XMLGregorianCalendar xmlDate=null;
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date); 
		try{
			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			logger.info("Gregorian Time Stamp : "+xmlDate); 
		}catch(Exception e){
			e.printStackTrace();
		}
		return xmlDate;
	}

	public BigDecimal getSchemaVersion()
	{
		Double d = new Double("1.0"); 
		BigDecimal schemaVersion = BigDecimal.valueOf(d); 
		return schemaVersion;
	}

	@Override
	public String getParavalues(String paraname) {

		String paramvalue=config.getParavalue(paraname);
		return paramvalue;
	}

	@Override
	public Timestamp getDatetoTimestamp(Date date) {
		// TODO Auto-generated method stub
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);
		return ts;
	}

	@Override
	public String getDDPversion(List<DdpVersion> ddpversion) {
		String ddpversionfinal = null; 
		for (int j = 0; j < ddpversion.size(); j++)
		{ 
			ddpversionfinal =  ((DdpVersion)ddpversion.get(j)).getDdpversion(); 
		}

		return ddpversionfinal;
	}

	@Override
	public String getDDPversionDate(List<DdpVersion> ddpversion) {

		String ddpversiondate = null;
		for (int j = 0; j < ddpversion.size(); j++) 
		{ 

			ddpversiondate = ((DdpVersion)ddpversion.get(j)).getDdpimplementationtime();
		}

		return ddpversiondate;
	}


	@Override
	public String getDDPRegularversion(List<DdpVersion> ddpversion) {
		String regularversion = null; 
		for (int j = 0; j < ddpversion.size(); j++)
		{ 
			regularversion =  ((DdpVersion)ddpversion.get(j)).getApplicableregularversion(); 
		}

		return regularversion;
	}

}
