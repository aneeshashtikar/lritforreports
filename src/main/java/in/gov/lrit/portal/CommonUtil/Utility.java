package in.gov.lrit.portal.CommonUtil;

import java.sql.Timestamp;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Utility {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Utility.class);
	public static void sendMail(String sender, String recipient, String title, String message,JavaMailSender mailSender) throws MailException, MessagingException{
		
	      try {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
		mimeMessage.setContent(message, "text/html");
		helper.setTo(recipient);
		helper.setSubject(title);
		helper.setFrom(sender);
		mailSender.send(mimeMessage);
	      }
	      catch (MessagingException ex) {
	            logger.debug("Error While Sending Mail");
	            logger.error("Error "+ex.getMessage(),ex);
	            ex.printStackTrace();
	        }
	}
	public static String bcryptPassword(String password){
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		return hashedPassword;
	}
	public static Timestamp getCurrentTimeStamp() {
		Date Currentdate = new Date();
		long time = Currentdate.getTime();
		Timestamp timestamp = new Timestamp(time);
		return timestamp;
	}
	
}
