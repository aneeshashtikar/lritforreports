package in.gov.lrit.portal.repository.report;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.report.CspLog;

@Repository
public interface CspLogRepository extends JpaRepository<CspLog, String> {
	
	@Query(value = ReportQueries.CSP_LOG_REPORT_FLAG, nativeQuery = true)
	public List<CspLog> getCspLogOfFlagVessel(Date startDate, Date endDate);

	@Query(value = ReportQueries.CSP_LOG_REPORT_NONFLAG, nativeQuery = true)
	public List<CspLog> getCspLogofNonflagVessel(String dataUserProvider, Date startDate, Date endDate);
	
}
