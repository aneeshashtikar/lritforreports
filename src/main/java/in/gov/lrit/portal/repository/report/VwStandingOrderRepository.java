package in.gov.lrit.portal.repository.report;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.portal.model.report.VwStandingOrder;

public interface VwStandingOrderRepository extends JpaRepository<VwStandingOrder, BigInteger>{

	@Query(value = ReportQueries.OPEN_STANDING_ORDER, nativeQuery = true)
	public List<VwStandingOrder> getOpenedStandingOrder(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.GET_EXCLUDED_AREA, nativeQuery = true)
	public List<String> getExcludedArea(BigInteger soId, Date startDate, Date endDate);

	@Query(value = ReportQueries.GET_EXCLUDED_COUNTRIES, nativeQuery = true)
	public List<String> getExcludedCountries(BigInteger soId, Date startDate, Date endDate);
	
	@Query(value = ReportQueries.GET_EXCLUDED_VESSELS, nativeQuery = true)
	public List<String> getExcludedVessels(BigInteger soId, Date startDate, Date endDate);
}
