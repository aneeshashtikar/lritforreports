package in.gov.lrit.portal.repository.requests;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.portal.model.request.DcPositionRequest;

//@Repository
public interface DcPositionRequestRepository extends JpaRepository<DcPositionRequest, String> {
	
	
	@Query(value="select * from dc_position_request where imo_no=:imo and request_status=:status and message_type=:messagetype and access_type IN (:accesstype)",nativeQuery = true)
	  List<DcPositionRequest> findByimoNoAndRequestStatusAndMessageTypeAndAccessType(String imo,String status,int messagetype,List<Integer> accesstype);
	
	
	
	  
}
