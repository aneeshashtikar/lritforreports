package in.gov.lrit.portal.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.portal.model.user.PortalCsoDpaDetailsTransit;

public interface PortalCsoDpaDetailTransitRepository extends JpaRepository<PortalCsoDpaDetailsTransit, String> {

}
