package in.gov.lrit.portal.repository.contractinggovernment;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.portal.model.contractinggovernment.PortalContext;



public interface PortalContextRepository extends JpaRepository<PortalContext, String>{

}
