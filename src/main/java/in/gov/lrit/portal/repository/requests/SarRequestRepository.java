package in.gov.lrit.portal.repository.requests;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalDdpRequest;
import in.gov.lrit.portal.model.request.PortalFlagRequest;
import in.gov.lrit.portal.model.request.PortalPortRequest;
import in.gov.lrit.portal.model.request.PortalSarRequest;
import in.gov.lrit.portal.model.request.PortalSurpicRequest;

@Repository
public interface SarRequestRepository extends JpaRepository<PortalSarRequest, Integer> {
	

}
