package in.gov.lrit.portal.repository.vessel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.vessel.ManufacturerList;
import in.gov.lrit.portal.model.vessel.PortalManufacturerDetails;

@Repository
public interface ManufacturerDetailsRepository extends JpaRepository<PortalManufacturerDetails, Integer>{

	@Query("SELECT count(*) FROM PortalManufacturerDetails p where p.manufacturerName = ?1")
	public int isManufacturerExist(String manufacturerName);
	
	public Iterable<PortalManufacturerDetails> findBystatus(String status);
	
	@Modifying
	@Transactional
	@Query("Update PortalManufacturerDetails p set p.status= 'Deactive', p.deRegDate = now() where p.manufacturerId = ?1")
	public Integer updateManuById(Integer manufacturerId);
	
	public Optional<PortalManufacturerDetails> findBymanufacturerId(Integer manufacturerId);
	
	@Query("SELECT DISTINCT p.manufacturerName FROM PortalManufacturerDetails p where p.status = 'Active' ")
	public ArrayList<String> findmanufacturerName();
	
	public PortalManufacturerDetails findBymanufacturerName(String manufacturerName);
	
	public List<ManufacturerList> findByStatus(String status);
	
}