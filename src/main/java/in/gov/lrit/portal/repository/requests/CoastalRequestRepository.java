package in.gov.lrit.portal.repository.requests;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalCoastalRequest;

@Repository
public interface CoastalRequestRepository extends JpaRepository<PortalCoastalRequest, Integer> {
	

}
