package in.gov.lrit.portal.repository.report;


public class ReportQueries {

	public static final String SHIPSPEED_POSITION_REPORT = "SELECT * FROM lrit_latest_ship_postion t WHERE t.speed < ?1";
	
	public static final String SARSURPIC_OUR_CIRCULAR_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE sar_rec_area IS NULL AND originator IN ('3065', '2031', '2032', '1065') AND req_timestmp between ? AND ? "; 
		 
	public static final String SARSURPIC_OUR_RECTANGULAR_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE sar_cir_area IS NULL AND originator IN ('3065', '2031', '2032', '1065') AND req_timestmp between ? AND ? "; 
	
	public static final String SARSURPIC_OUR_BOTH_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE originator IN ('3065', '2031', '2032','1065') AND req_timestmp between ? AND ? "; 
	
	public static final String SARSURPIC_FOREIGN_CIRCULAR_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE sar_rec_area IS NULL AND originator Not IN ('3065', '2031', '2032', '1065') AND req_timestmp between ?  AND ? ";
	
	public static final String SARSURPIC_FOREIGN_RECTANGULAR_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE sar_cir_area IS NULL AND originator Not IN ('3065', '2031', '2032', '1065') AND req_timestmp between ?  AND ?";
	
	public static final String SARSURPIC_FOREIGN_BOTH_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE originator NOT IN ('3065', '2031', '2032','1065') AND req_timestmp between ? AND ?"; 

	public static final String SARSURPIC_BOTH_CIRCULAR_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE sar_rec_area IS NULL AND req_timestmp between ? AND ?";
	
	public static final String SARSURPIC_BOTH_RECTANGULAR_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE sar_cir_area IS NULL AND req_timestmp between ?  AND ?";
	
	public static final String SARSURPIC_ALL_REQUEST = "select distinct on (message_id) vw_sar_surpic_request.* from vw_sar_surpic_request WHERE req_timestmp between ?  AND ?";

	public static final String SARSURPIC_RESPONSES = "select * from vw_sar_surpic_request where message_id= ? ";
	
	public static final String SHIP_POSITION_REQUEST_REPORT_FLAG = "select * from vw_asp_log_position_request where receive_time between ? and ?";
	
	public static final String SHIP_POSITION_REPORT = "select * from vw_asp_log_position_report where receive_time between ? and ?";
	
	public static final String ASP_SHIP_POSITION_REPORT = "select distinct on (imo_no) asp_shipposition.* from asp_shipposition where timestamp3 between ? and ? ";
	
	public static final String ASP_SHIP_POSITION = "select * from asp_shipposition where imo_no = ? AND timestamp3 between ? and ?";
	
	public static final String GEOGRAPHICAL_AREA_UPDATE_TO_IDE_FLAG = "select * from vw_geographical_area_update where action_type = 0 and request_timestamp between ? and ? ";
	
	public static final String GEOGRAPHICAL_AREA_UPDATE_TO_IDE_OTHER = "select * from vw_geographical_area_update where action_type = 0 AND data_user_requestor = ? and request_timestamp between ? and ?";
	
	public static final String GEOGRAPHICAL_AREA_UPDATE_TO_DDP_FLAG = "select * from vw_geographical_area_update where action_type = 1 and request_timestamp between ? and ?";
	
	public static final String GEOGRAPHICAL_AREA_UPDATE_TO_DDP_OTHER = "select * from vw_geographical_area_update where action_type = 1 AND data_user_requestor = ? and request_timestamp between ? and ?";
	
	public static final String OPEN_STANDING_ORDER = "select distinct on (so_id) vw_lrit_standing_orders.* from vw_lrit_standing_orders where raised_date between ? and ? AND status = 'Opened'";
	
	public static final String GET_EXCLUDED_AREA = "select distinct excluded_areas from vw_lrit_standing_orders where so_id = ? and raised_date between ? and ?";
	
	public static final String GET_EXCLUDED_COUNTRIES = "select distinct excluded_countries from vw_lrit_standing_orders where so_id = ? and raised_date between ? and ? ";
	
	public static final String GET_EXCLUDED_VESSELS = "select distinct excluded_vessels from vw_lrit_standing_orders where so_id = ? and raised_date between ? and ?";
	
	public static final String CSP_LOG_REPORT_FLAG = "select * from dc_position_report where data_user_provider in ('1065', '1136') AND timestamp1 between ? AND ? order by timestamp1 desc";
	
	public static final String CSP_LOG_REPORT_NONFLAG = "select * from dc_position_report where data_user_provider= ? AND timestamp1 between ? AND ? order by timestamp1 desc";
	
	public static final String DDP_REQUEST_LOGS = "select * from dc_ddprequest where lrit_timestamp between ? and ? order by lrit_timestamp desc";
	
	public static final String DDP_UPDATE_LOGS = "select * from dc_ddpupdate where lrit_timestamp between ? and ? order by lrit_timestamp desc";

	public static final String GET_PAYLOAD_AGAINST_MESSAGEID = "select payload from dc_message_log where message_id = ?";
	
	public static final String ASP_RECEIPT_LOG = "select message_id as messageId, timestamp as receivedTime from asp_receipt_txn where timestamp between ? and ? order by timestamp desc"; 
	
	public static final String ASP_POSITION_REQUEST_LOG = "select message_id as messageId, transmit_timestamp as receivedTime from asp_position_request where transmit_timestamp between ? and ? order by transmit_timestamp desc";
	
	public static final String ASP_POSITION_REPORT_LOG = "select message_id as messageId, timestamp2 as receivedTime from asp_shipposition where timestamp2 between ? and ? order by timestamp2 desc";
	
	
}