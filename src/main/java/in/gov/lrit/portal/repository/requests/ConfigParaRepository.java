package in.gov.lrit.portal.repository.requests;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.Configpara;


@Repository
public interface ConfigParaRepository extends JpaRepository<Configpara, Integer> {
	
	  @Query(value="SELECT cp.paravalue from config_para cp where cp.paraname=:paraname" , nativeQuery = true) 
	  public String getParavalue(String paraname);
	 
	  
	  @Query(value = "select nextval('lrit_messageid_seq')", nativeQuery = true) 
	  String getNextSeriesId();
}
