package in.gov.lrit.portal.repository.standingOrder;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.standingOrder.SOVesselExclusion;

@Repository
@Transactional
public interface SOVesselExclusionRepository extends JpaRepository<SOVesselExclusion, String> {
	
	@Query(value = "Select * from portal_standingorders_vessel_exclusion where standingorder_id= :SOId and area_code= :areacode ", nativeQuery = true)
	List<SOVesselExclusion> findBySOId(int SOId, String areacode);
	
	@Query(value = "Select vessel_category_id from portal_standingorders_vessel_exclusion where standingorder_id= :SOId and area_code= :areacode", nativeQuery = true)
	List<String> getFilterShips(int SOId, String areacode);
	
	void deleteBySoId(int soid);

}
