package in.gov.lrit.portal.repository.vessel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.vessel.PortalVesselDetailHistory;

@Repository
@Transactional
public interface VesselDetailHistoryRepository extends JpaRepository<PortalVesselDetailHistory, Long>{

	
	public PortalVesselDetailHistory findByPendingTaskId(Integer pendingTaskID);
	
	@Modifying
	@Query(value="update portal_vessel_details_history set vessel_name=:vesselName, call_sign=:callSign where pendingtask_id=:pendingTaskId ", nativeQuery=true)
	public int updatevesselDetail(Integer pendingTaskId, String vesselName, String callSign);
}
