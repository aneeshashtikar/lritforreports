package in.gov.lrit.portal.repository.requests;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vividsolutions.jts.geom.Geometry;

import in.gov.lrit.portal.model.request.LritLatestShipPositionFlag;

//@Repository
public interface LatestShipFlagRepository extends JpaRepository<LritLatestShipPositionFlag, String> {
	
	
	
	
	  @Query(value="select * from(select *,ST_Covers(?,ST_SetSRID(ST_MakePoint(lrit_latest_ship_position_flag.longitude,lrit_latest_ship_position_flag.latitude), 4326))as test from lrit_latest_ship_position_flag) as c where test='t'",nativeQuery = true)
	  List<LritLatestShipPositionFlag> findByImoNOAndMmsiNOAndShipName(Geometry geo);
}
