package in.gov.lrit.portal.repository.report;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.report.AspLogInterface;
import in.gov.lrit.portal.model.report.DdpLog;

@Repository
public interface DdpLogRepository extends JpaRepository<DdpLog, String> {

	@Query(value = ReportQueries.DDP_REQUEST_LOGS, nativeQuery = true)
	public List<DdpLog> getDdpRequestLogs(Date startDate, Date endDate);
	
	@Query(value =ReportQueries.GET_PAYLOAD_AGAINST_MESSAGEID, nativeQuery = true)
	public String getPayloadAgainstMessageId(String messageId);
	
	@Query(value = ReportQueries.DDP_UPDATE_LOGS, nativeQuery = true)
	public List<DdpLog> getDdpUpdateLogs(Date startDate, Date endDate);
	
	//Queries for ASP log
	
	@Query(value = ReportQueries.ASP_RECEIPT_LOG, nativeQuery = true)
	public List<AspLogInterface> getAspReceiptLog(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.ASP_POSITION_REQUEST_LOG, nativeQuery = true)
	public List<AspLogInterface> getAspPositionRequestLog(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.ASP_POSITION_REPORT_LOG, nativeQuery = true)
	public List<AspLogInterface> getAspPositionReportLog(Date startDate, Date endDate);
	
}
