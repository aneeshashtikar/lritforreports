package in.gov.lrit.portal.repository.pendingtask;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.pendingtask.PortalPendingTaskMst;

@Repository
@Transactional
public interface PendingTaskMasterRepository extends JpaRepository<PortalPendingTaskMst, Long> {

	public static final String GET_CATEGORYWISE_PENDINGTASK="SELECT category, COUNT(*) FROM \r\n" + 
			"	portal_pending_task_mst WHERE category='approve_delete_dnid' and pendingtask_id  in\r\n" + 
			"	 (select pendingtask_id from portal_pending_task_mst where requestor_lritid='1065') \r\n" + 
			"	 GROUP BY category";
	
	@Query(value=GET_CATEGORYWISE_PENDINGTASK,nativeQuery = true)
	public List<Object[]> getCategoryWiseAlertCount();
	
	
	public static final String GET_PENDING_TASK_COUNT="select count(*) from portal_pending_task_mst  where category IN (:category) and"
			+ " requestor_lritid IN (:requesterLoginId) and status='pending'";
	@Query(value=GET_PENDING_TASK_COUNT,nativeQuery = true)
	public int getPendingTaskCountForLoginUser(List<String> requesterLoginId, List<String> category );
	
	
	
	public static final String GET_REQUEST_PENDING_TASK="select * from portal_pending_task_mst  where category IN (:category) and"
			+ " requestor_lritid = :countryLritId and status='pending'";
	@Query(value=GET_REQUEST_PENDING_TASK,nativeQuery = true)
	public List<PortalPendingTaskMst> getRequestPendingTask(String countryLritId, List<String> category);

	public static final String UPDATE_STATUS_PENDIND_TASK ="UPDATE portal_pending_task_mst SET STATUS = :status,"
			+ " approval_login_id= :approverLoginId, action_date=:date, remark=:comment where pendingtask_id=:pendingTaskId ";
	@Query(value=UPDATE_STATUS_PENDIND_TASK, nativeQuery=true)
	@Modifying
	public int updatePendingTaskStatus(String status, String approverLoginId, Timestamp date, Integer pendingTaskId, String comment);
	
	
}
