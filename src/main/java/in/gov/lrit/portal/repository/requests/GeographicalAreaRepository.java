package in.gov.lrit.portal.repository.requests;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalGeoraphicalArea;

@Repository
@Transactional
public interface GeographicalAreaRepository extends JpaRepository<PortalGeoraphicalArea, String > {
	
	@Query("select t from PortalGeoraphicalArea t where status = :geoStatus  ")
	public List<PortalGeoraphicalArea> findAllByStatus(String geoStatus);

	
	@Modifying
	@Query("update PortalGeoraphicalArea t set t.status = :status where t.areaCode = :areaCode")
	int updateGeoAreaStatus(String areaCode, String status);
	
	
	@Modifying
	@Query("update PortalGeoraphicalArea t set t.status = 'Deleted' where t.areaCode = :areaCode")
	int deleteGeoArea(String areaCode);
	
	
	PortalGeoraphicalArea findByAreaCode(String areaCode);
}
