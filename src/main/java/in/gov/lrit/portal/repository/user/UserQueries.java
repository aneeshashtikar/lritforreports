package in.gov.lrit.portal.repository.user;

public class UserQueries {
	
	public static final String GET_USER_ACTIVITIES="SELECT  a.activityname FROM portal_users u "
			+ "INNER JOIN portal_role_user_mapping ur on u.login_id=ur.login_id " + 
			"		 INNER JOIN portal_roles r on ur.role_id=r.role_id "
			+"			INNER JOIN portal_role_activities_mapping ram "
			+ "			on ur.role_id= ram.role_id "
			+ " INNER JOIN portal_activities a on a.activity_id=ram.activity_id where u.login_id=:loginId";
	
	public static final String GET_PENDING_TASK_CATEGORY="SELECT  a.activityname FROM portal_users u "
			+ "INNER JOIN portal_role_user_mapping ur on u.login_id=ur.login_id " + 
			"		 INNER JOIN portal_roles r on ur.role_id=r.role_id "
			+"			INNER JOIN portal_role_activities_mapping ram "
			+ "			on ur.role_id= ram.role_id "
			+ " INNER JOIN portal_activities a on a.activity_id=ram.activity_id where u.login_id=:loginId"
			+ " and type=:type";

}
