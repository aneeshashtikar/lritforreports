package in.gov.lrit.portal.repository.vessel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import in.gov.lrit.portal.model.vessel.PortalVesselDetail;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in.gov.lrit.portal.model.vessel.VesselListInterface;

@Repository
public interface VesselRepository extends JpaRepository<PortalVesselDetail, Integer> {

	public ArrayList<VesselListInterface> findByRegStatusAndStatus(String regStatus, String status);

	/*
	 * @Query(
	 * value="select vessel_id as vesselId, imo_no as imoNo, vessel_name as vesselName, mmsi_no as mmsiNo, call_sign as callSign, status, registration_status as regStatus from portal_vessel_details where status = :status and registration_status not in ('SUBMITTED','SCRAP') "
	 * , nativeQuery=true) public ArrayList<VesselDetailInterface>
	 * findByStatus(String status);
	 */

	@Query(value = "select vessel_id as vesselId, imo_no as imoNo, vessel_name as vesselName, mmsi_no as mmsiNo, call_sign as callSign, status, registration_status as regStatus from portal_vessel_details where status = :status ", nativeQuery = true)
	public ArrayList<VesselListInterface> findByStatus(String status);

	@Query(value = "select vessel_id as vesselId, imo_no as imoNo, vessel_name as vesselName, mmsi_no as mmsiNo, call_sign as callSign, status, registration_status as regStatus from portal_vessel_details where registration_status not in ('SUBMITTED','SCRAP', 'SOLD') ", nativeQuery = true)
	public ArrayList<VesselListInterface> findByRegStatus();

	/*
	 * @Query(
	 * value="select * from portal_vessel_details where vessel_id in (select vessel_id from portal_shippingcompany_vessel_relation where user_id in (select login_id as loginId from portal_users where company_code = :companyCode and relation = 'owner'))"
	 * , nativeQuery=true) public ArrayList<PortalVesselDetail>
	 * findVesselForCompany(String companyCode);
	 */

	@Query(value = "select * from ((portal_shippingcompany_vessel_relation map Inner Join portal_users users On users.login_id = map.user_id) Inner Join portal_vessel_details vessel ON map.vessel_id = vessel.vessel_id) where users.company_code = :companyCode and users.status = 'registered' and map.relation in (0, 3, 4, 6)", nativeQuery = true)
	public ArrayList<PortalVesselDetail> findVesselForCompany(String companyCode);

	@Query(value = "select * from ( portal_shippingcompany_vessel_relation mapping Inner Join portal_vessel_details vessel ON mapping.vessel_id=vessel.vessel_id ) where mapping.user_id = :loginId and mapping.relation in (0, 3, 4, 6)", nativeQuery = true)
	public ArrayList<PortalVesselDetail> findVesselForCompanyByLoginId(String loginId);

	/*
	 * @Query(
	 * value="select * from portal_vessel_details where vessel_id in (select vessel_id from portal_shippingcompany_vessel_relation where user_id in (select login_id as loginId from portal_users where company_code = :companyCode and relation = 'owner')) and status = :status and registration_status not in ('SUBMITTED','SCRAP') union select * from portal_vessel_details where registration_status = 'SOLD' "
	 * , nativeQuery=true) public ArrayList<PortalVesselDetail>
	 * findVesselForCompanyByStatus(String companyCode, String status);
	 */

	@Query(value = "select * from portal_vessel_details where vessel_id in (select vessel_id from portal_shippingcompany_vessel_relation where user_id in (select login_id as loginId from portal_users where company_code = :companyCode and relation = 'owner')) and status = :status ", nativeQuery = true)
	public ArrayList<PortalVesselDetail> findVesselForCompanyByStatus(String companyCode, String status);

	/*@Query(value = "select * from portal_vessel_details where vessel_id in (select vessel_id from portal_shippingcompany_vessel_relation where user_id in (select login_id as loginId from portal_users where company_code = :companyCode and relation = 'owner')) and registration_status not in ('SUBMITTED','SCRAP') union select * from portal_vessel_details where registration_status = 'SOLD'", nativeQuery = true)
	public ArrayList<PortalVesselDetail> findVesselForCompanyByRegStatus(String companyCode);*/
	
	@Query(value = "select vessel.vessel_id as vesselId, vessel.imo_no as imoNo, vessel.mmsi_no as mmsiNo, vessel.vessel_name as vesselName, vessel.call_sign as callSign, vessel.registration_status as regStatus from ((portal_shippingcompany_vessel_relation map Inner Join portal_users users On users.login_id = map.user_id) Inner Join portal_vessel_details vessel On map.vessel_id = vessel.vessel_id) where users.company_code = :companyCode and users.status = 'registered' and map.relation in (0, 3, 4, 6) and vessel.registration_status not in ('SUBMITTED', 'SCRAP', 'SOLD') ", nativeQuery = true)
	public ArrayList<VesselListInterface> findVesselForCompanyByRegStatus(String companyCode);
	
	@Query(value = "select vessel.vessel_id as vesselId, vessel.imo_no as imoNo, vessel.mmsi_no as mmsiNo, vessel.vessel_name as vesselName, vessel.call_sign as callSign, vessel.registration_status as regStatus from ( portal_shippingcompany_vessel_relation mapping Inner Join portal_vessel_details vessel ON mapping.vessel_id=vessel.vessel_id ) where mapping.user_id = :loginId and mapping.relation in (0, 3, 4, 6) and registration_status not in ('SUBMITTED','SCRAP','SOLD') ", nativeQuery = true)
	public ArrayList<VesselListInterface> findVesselForCompanyByRegStatusAndLoginId(String loginId);

/*	@Query(value = "select * from portal_vessel_details where vessel_id in (select vessel_id from portal_shippingcompany_vessel_relation where user_id in (select login_id as loginId from portal_users where company_code = :companyCode and relation = 'owner')) and status = :status and registration_status = :regStatus", nativeQuery = true)
	public ArrayList<PortalVesselDetail> findVesselForCompanyByStatusAndRegStatus(String companyCode, String status, String regStatus);*/
	
	@Query(value = "select vessel.vessel_id as vesselId, vessel.imo_no as imoNo, vessel.mmsi_no as mmsiNo, vessel.vessel_name as vesselName, vessel.call_sign as callSign, vessel.registration_status as regStatus from ((portal_shippingcompany_vessel_relation map Inner Join portal_users users On users.login_id = map.user_id) Inner Join portal_vessel_details vessel On map.vessel_id = vessel.vessel_id) where users.company_code = :companyCode and users.status = 'registered' and map.relation in (0, 3, 4, 6) and vessel.status = :status and vessel.registration_status = :regStatus ", nativeQuery = true)
	public ArrayList<VesselListInterface> findVesselForCompanyByStatusAndRegStatus(String companyCode, String status, String regStatus);
	
	@Query(value = "select vessel.vessel_id as vesselId, vessel.imo_no as imoNo, vessel.mmsi_no as mmsiNo, vessel.vessel_name as vesselName, vessel.call_sign as callSign, vessel.registration_status as regStatus from ( portal_shippingcompany_vessel_relation mapping Inner Join portal_vessel_details vessel ON mapping.vessel_id=vessel.vessel_id ) where mapping.user_id = :loginId and mapping.relation in (0, 3, 4, 6) and vessel.status = :status and vessel.registration_status = :regStatus", nativeQuery = true)
	public ArrayList<VesselListInterface> findVesselForCompanyByStatusAndRegStatusAndLoginId(String loginId, String status, String regStatus);
	
	@Query(value = "select vessel_id as vesselId, imo_no as imoNo, vessel_name as vesselName, mmsi_no as mmsiNo, call_sign as callSign, status, registration_status as regStatus from portal_vessel_details where status is not null", nativeQuery = true)
	public ArrayList<VesselListInterface> findAllByStatus();
	
	@Query(value = "select vessel_id as vesselId, imo_no as imoNo, vessel_name as vesselName, mmsi_no as mmsiNo, call_sign as callSign, status, registration_status as regStatus from portal_vessel_details where status = :status and registration_status not in :regStatus", nativeQuery = true)
	public ArrayList<VesselListInterface> findAllActiveVessel(String status, List<String> regStatus);
	
	@Query(value = "select vessel.vessel_id as vesselId, vessel.imo_no as imoNo, vessel.mmsi_no as mmsiNo, vessel.vessel_name as vesselName, vessel.call_sign as callSign, vessel.registration_status as regStatus from ((portal_shippingcompany_vessel_relation map Inner Join portal_users users On users.login_id = map.user_id) Inner Join portal_vessel_details vessel On map.vessel_id = vessel.vessel_id) where users.company_code = :companyCode and users.status = 'registered' and map.relation in (0, 3, 4, 6) and vessel.status = :status and vessel.registration_status not in :regStatus ", nativeQuery = true)
	public ArrayList<VesselListInterface> findAllActiveVesselForCompany(String companyCode, String status, List<String> regStatus);
	
	@Query(value = "select vessel.vessel_id as vesselId, vessel.imo_no as imoNo, vessel.mmsi_no as mmsiNo, vessel.vessel_name as vesselName, vessel.call_sign as callSign, vessel.registration_status as regStatus from ( portal_shippingcompany_vessel_relation mapping Inner Join portal_vessel_details vessel ON mapping.vessel_id=vessel.vessel_id ) where mapping.user_id = :loginId and mapping.relation in (0, 3, 4, 6) and vessel.status = :status and vessel.registration_status not in :regStatus", nativeQuery = true)
	public ArrayList<VesselListInterface> findAllActiveVesselForCompanyByLoginId(String loginId, String status, List<String> regStatus);
	
	@Query(value="select vessel.vessel_id as vesselId, vessel.imo_no as imoNo, vessel.mmsi_no as mmsiNo, vessel.vessel_name as vesselName, vessel.call_sign as callSign, vessel.registration_status as regStatus from portal_vessel_details vessel where vessel.vessel_id in (select vessel_id from portal_shippingcompany_vessel_relation map where map.user_id in (select login_id  from portal_users where category='USER_CATEGORY_SC' and status = 'registered' and company_code != :companyCode ) and relation in (0,3,4,6)) and vessel.registration_status = 'SOLD'", nativeQuery=true)
	public ArrayList<VesselListInterface> findRepurchaseVesselList(String companyCode);
	
	/*@Query(value="select vessel.vessel_id as vesselId, vessel.imo_no as imoNo, vessel.mmsi_no as mmsiNo, vessel.vessel_name as vesselName, vessel.call_sign as callSign, vessel.registration_status as regStatus from portal_vessel_details vessel where vessel.vessel_id in (select map.vessel_id from portal_shippingcompany_vessel_relation map where map.user_id != :loginId ) and vessel.registration_status = 'SOLD'", nativeQuery=true)
	public ArrayList<VesselListInterface> findRepurchaseVesselListByLoginId(String loginId);*/

	@Modifying
	@Transactional
	@Query("UPDATE PortalVesselDetail p SET p.regStatus = ?1, p.regStatusDate = now() WHERE p.vesselId = ?2")
	public Integer updateVesselRegStatus(String regStatus, Integer vesselId);

	@Modifying
	@Transactional
	@Query("UPDATE PortalVesselDetail p SET p.status = ?1, p.regStatus = ?2, p.regStatusDate = now(), p.reportingStatusChangeReason = ?4, p.reportingStatusChangeRemarks = ?5 WHERE p.vesselId = ?3")
	public Integer updateVesselStatustoInactive(String status, String regStatus, Integer vesselId,
			String reportingStatusChangeReason, String reportingStatusChangeRemarks);

	@Modifying
	@Transactional
	@Query("UPDATE PortalVesselDetail p SET p.status = ?1, p.regStatus = ?2, p.regStatusDate = now() WHERE p.vesselId = ?3")
	public Integer updateVesselStatusToActive(String status, String regStatus, Integer vesselId);

	@Query("SELECT count(*) FROM PortalVesselDetail p WHERE p.vesselId = ?1 AND p.regStatus = 'SOLD' ")
	public Integer isVesselSold(Integer vesselId);

	@Query("SELECT p FROM PortalVesselDetail p WHERE p.imoNo = ?1 and p.regStatus not in ('SOLD')")
	public PortalVesselDetail findVesselByImoNo(String imoNo);

	@Query("SELECT p.vesselId FROM PortalVesselDetail p WHERE p.imoNo = ?1")
	public Integer findVesselIdByImoNo(String imoNo);

	/*
	 * @Query("SELECT count(*) FROM PortalVesselDetail p WHERE p.imoNo = ?1 AND (regStatus != 'SOLD' OR regStatus != 'SCRAP' OR status != 'INACTIVE') "
	 * ) public Integer isIMONoExist(String imoNo);
	 */

	@Query("SELECT count(*) FROM PortalVesselDetail p WHERE (p.regStatus != 'SOLD' OR p.regStatus = 'SCRAP' OR p.status = 'INACTIVE') AND imo_no = ?1")
	public Integer isIMONoExist(String imoNo);

	/*
	 * @Query("SELECT count(*) FROM PortalVesselDetail p WHERE p.vesselId in (SELECT q.vesselDet.vesselId FROM PortalShipEquipment q WHERE q.shipborneEquipmentId = ?1 ) AND p.status = 'ACTIVE' AND p.regStatus = 'SEID_DELETED' "
	 * ) public Integer isEligibleToInactive(String shipequipId);
	 */

	/*
	 * @Query("SELECT count(*) FROM PortalVesselDetail p WHERE p.vesselId = ?1 AND p.status = 'ACTIVE' AND p.regStatus = 'SEID_DELETED' "
	 * ) public Integer isEligibleToInactive(Integer vesselId);
	 * 
	 * @Query("SELECT count(*) FROM PortalVesselDetail p WHERE p.vesselId = ?1 AND p.status = 'INACTIVE' AND p.regStatus = 'SUBMITTED' "
	 * ) public Integer isEligibleToActive(Integer vesselId);
	 */

	@Query("SELECT count(*) FROM PortalVesselDetail p WHERE p.vesselId = ?1 AND p.status = 'ACTIVE' AND p.regStatus = 'DNID_DELETED' ")
	public Integer isEligibleToInactive(Integer vesselId);

	@Query("SELECT count(*) FROM PortalVesselDetail p WHERE p.vesselId = ?1 AND p.status = 'INACTIVE' AND p.regStatus = 'INACTIVE' ")
	public Integer isEligibleToActive(Integer vesselId);

	/*
	 * @Query("SELECT count(*) FROM PortalVesselDetail p WHERE p.mmsiNo = ?1 AND (regStatus != 'SOLD' OR regStatus != 'SCRAP' OR status != 'INACTIVE') "
	 * ) public Integer isMMSINoExist(String mmsiNo);
	 */

	@Query("SELECT count(*) FROM PortalVesselDetail p WHERE (p.regStatus != 'SOLD' OR p.regStatus = 'SCRAP' OR p.status = 'INACTIVE') AND mmsi_no = ?1")
	public Integer isMMSINoExist(String mmsiNo);

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param vesselId
	 * @return
	 */
	public PortalVesselDetail findByVesselId(Integer vesselId);
}