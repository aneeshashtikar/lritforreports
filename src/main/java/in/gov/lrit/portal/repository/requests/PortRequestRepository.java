package in.gov.lrit.portal.repository.requests;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalDdpRequest;
import in.gov.lrit.portal.model.request.PortalFlagRequest;
import in.gov.lrit.portal.model.request.PortalPortRequest;

@Repository
public interface PortRequestRepository extends JpaRepository<PortalPortRequest, Integer> {
	

}
