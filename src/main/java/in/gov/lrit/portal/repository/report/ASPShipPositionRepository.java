package in.gov.lrit.portal.repository.report;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.portal.model.report.ASPShipPosition;

public interface ASPShipPositionRepository extends JpaRepository<ASPShipPosition, String>{
	
	@Query(value = ReportQueries.ASP_SHIP_POSITION_REPORT, nativeQuery = true)
	public List<ASPShipPosition> getVessselDetails(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.ASP_SHIP_POSITION, nativeQuery = true)
	public List<ASPShipPosition> getShipPosition(String imoNo, Date startDate, Date endDate);

}
