package in.gov.lrit.portal.repository.contractinggovernment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMasterHistory;
@Repository
public interface PortalLritIdMasterHistoryRepository extends JpaRepository<PortalLritIdMasterHistory,String> {

	/*
	 *  @Modifying
    @Query(value = "insert into Logger (redirect,user_id) VALUES (:insertLink,:id)", nativeQuery = true)
    @Transactional
	 */
	 /*@Modifying
	 @Transactional
	@Query("INSERT INTO PortalLritIdMasterHistory (lritId,coastalArea,countryCode,countryName,regDate,deregDate,type) VALUES (:)")
	public void saveCG(PortalLritIdMaster cgDetails);*/
}
