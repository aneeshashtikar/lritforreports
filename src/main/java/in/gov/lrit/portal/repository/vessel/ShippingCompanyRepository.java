package in.gov.lrit.portal.repository.vessel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in_.gov.lrit.ddp.dto.LritNameDto;

@Repository
public interface ShippingCompanyRepository extends JpaRepository<PortalShippingCompany, String> {

	/*
	 * @Query("SELECT p.currentCsoId, p.alternateCsoId FROM PortalShippingCompany p WHERE p.companyCode = ?1"
	 * ) public Map<String, String> findCSOByCompCode(String companyCode);
	 */

	@Query("SELECT p.companyName, p.companyCode FROM PortalShippingCompany p")
	public List<Object[]> findShippingCompanyList();

	@Query("SELECT p.companyCode FROM PortalShippingCompany p WHERE p.companyName = ?1")
	public String findCompanyCodeByName(String companyName);

	public PortalShippingCompany findBycompanyName(String companyName);

	/*
	 * @Query("SELECT p.loginId FROM PortalShippingCompany p WHERE p.companyName = ?1"
	 * ) public String findLoginIdByName(String companyName);
	 */

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @return
	 */
	@Query("Select new in_.gov.lrit.ddp.dto.LritNameDto(ship.companyCode, ship.companyName) from PortalShippingCompany ship")
	public List<LritNameDto> findAllShippingCompanyDto();

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param agencyCode
	 * @return
	 */
	@Query("Select ship.companyName from PortalShippingCompany ship where ship.companyCode = :companyCode")
	public String getShippingCompanyName(@Param("companyCode") String agencyCode);
}