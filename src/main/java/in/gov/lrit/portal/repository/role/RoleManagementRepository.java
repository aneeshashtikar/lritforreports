package in.gov.lrit.portal.repository.role;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.role.PortalRoles;

@Repository
public interface RoleManagementRepository extends JpaRepository<PortalRoles, Integer> {

	@Transactional
	@Modifying
	@Query(value = "UPDATE PortalRoles roles set roles.status = 'inactive' where roles.roleId = :roleId ")
	public int updateStatus(String roleId);

	// @Query(value="SELECT r.activity from roles_activity r where
	// r.role=:role",nativeQuery = true)

	// public static final String LOAD_USER_BY_USERNAME="SELECT u FROM org_user_mst
	// u where u.username=:username";where roles.status='active' 
	@Query(value = "SELECT roles from PortalRoles roles ")
	public List<PortalRoles> getAllRoles();

	@Query(value = "SELECT roles from PortalRoles roles where roles.status= :status ")
	public List<PortalRoles> getAllRoles(String status);

	@Query(value = "SELECT roles from PortalRoles roles JOIN roles.activities a where  roles.roleId = :roleId ")
	public PortalRoles getRoleDetails(String roleId);

	@Transactional
	@Modifying
	@Query(value = "Delete from portal_role_activities_mapping mapping where mapping.role_id = :roleId", nativeQuery = true)
	public void deleteRoleActivityMapping(String roleId);



	@Query(value = "Select case when count(role)> 0 then true else false end from PortalRoles role where role.rolename = :roleName")
	public boolean checkRole(String roleName);
	
	@Query(value = "Select case when count(role)> 0 then true else false end from PortalRoles role where role.shortname = :shortName")
	public boolean checkShorName(String shortName);
	
	
	
	
	
	@Query(value = "SELECT roles from PortalRoles roles  where  roles.roleId = :roleId ")
	public PortalRoles getRoleInfo(String roleId);
	
	
	//@Query(value="select vessel_id from portal_vessel_details where vessel_id=:vesselId and status IN (:shipstatus) and registration_status IN (:registrationStatus)",nativeQuery = true)
		@Query(value="select roles from portal_roles roles where roles.role_id IN (:roleIdList) ",nativeQuery = true)
		public List<PortalRoles> getRoleList(List<String> roleIdList);
	
	
	
}
