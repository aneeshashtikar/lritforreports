package in.gov.lrit.portal.repository.standingOrder;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.standingOrder.SOGeographicalAreas;

@Repository
@Transactional
public interface SOGeoAreaRepository extends JpaRepository<SOGeographicalAreas, String> {
	
	@Query(value = "Select * from portal_standingorders_geographicalareas where standingorder_id= :SOId ", nativeQuery = true)
	List<SOGeographicalAreas> findBySOId(int SOId);
	
	@Query(value = "Select area_code from portal_standingorders_geographicalareas where standingorder_id= :SOId ", nativeQuery = true)
	List<String> getFlagedArea(int SOId);
	
	void deleteBySoId(int SOId);

}
