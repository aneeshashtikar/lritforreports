package in.gov.lrit.portal.repository.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.common.AspTerminalFrequency;
@Repository
public interface AspTerminalFrequencyRepository extends JpaRepository<AspTerminalFrequency, String>{
	
	@Query(value="select min(frequency_rate) as frequency_rate,count(frequency_rate) from "
			+ " asp_terminal_frequency  where status='ACTIVE' group by frequency_rate " 
		, nativeQuery=true)
	public List<Object[]> getAspTerminalFrequencyCount();

}
