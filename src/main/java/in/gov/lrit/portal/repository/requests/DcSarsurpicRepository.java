package in.gov.lrit.portal.repository.requests;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.DcSarsurpicrequest;


@Repository
public interface DcSarsurpicRepository extends JpaRepository<DcSarsurpicrequest, Integer> {
	
	@Query(value = "select * from dc_sarsurpicrequest where access_type=:accesstype and lrit_timestamp BETWEEN NOW() - INTERVAL '24 HOURS' AND NOW()", nativeQuery = true)
	List<DcSarsurpicrequest> findbyMessageIdAndAccessType(int accesstype); 
	
}
