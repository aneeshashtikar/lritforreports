package in.gov.lrit.portal.repository.vessel;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.vessel.PortalVesselCategory;

@Repository
public interface VesselCategoryRepository extends JpaRepository<PortalVesselCategory, String>{
	
	@Query("SELECT DISTINCT p.categoryName FROM PortalVesselCategory p")
	public ArrayList<String> findCategoryList();
	
	

}