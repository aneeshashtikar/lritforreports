package in.gov.lrit.portal.repository.vessel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionUsageException;

import in.gov.lrit.portal.model.vessel.PortalDocuments;

@Repository
public interface DocumentsRepository extends JpaRepository<PortalDocuments, Integer>{
	
	@Query(value="select * from portal_documents where ref_id = :referenceId and doc_purpose in :doc_purpose", nativeQuery=true)
	public List<PortalDocuments> findByreferenceId(String referenceId, List<String> doc_purpose);

}