package in.gov.lrit.portal.repository.requests;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalGeoraphicalAreaRequest;

@Repository
public interface GeographicalAreaRequestRepository extends JpaRepository<PortalGeoraphicalAreaRequest, String> {

}
