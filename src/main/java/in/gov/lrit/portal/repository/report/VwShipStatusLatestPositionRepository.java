package in.gov.lrit.portal.repository.report;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.report.VwShipStatusLatestPosition;
import in_.gov.lrit.portal.dto.VwShipStatusLatestPositionDto;

@Repository
public interface VwShipStatusLatestPositionRepository extends JpaRepository<VwShipStatusLatestPosition, String> {
	
	
	@Query(value = "select new in_.gov.lrit.portal.dto.VwShipStatusLatestPositionDto(vw.imoNo, vw.mmsiNo, vw.shipName, vw.callSign, vw.latitude," + 
			" vw.longitude, vw.timestamp1, vw.status, vw.portalUser.name) from VwShipStatusLatestPosition vw inner join vw.portalUser pu  " + 
			" where vw.status = :vesselStatus and vesselFlag = 'flag'")
	public List<VwShipStatusLatestPositionDto> getLatestPositionOfFlagVessel(String vesselStatus);
	
	
	@Query(value = "select new in_.gov.lrit.portal.dto.VwShipStatusLatestPositionDto(vw.imoNo, vw.mmsiNo, vw.shipName, vw.callSign, vw.latitude," + 
			" vw.longitude, vw.timestamp1, vw.status, vw.portalUser.name) from VwShipStatusLatestPosition vw inner join vw.portalUser pu  " + 
			" where vesselFlag = 'flag'")
	public List<VwShipStatusLatestPositionDto> getAllFlagShipsLatestPosition();

}
