package in.gov.lrit.portal.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.user.PortalShippingCompany;
import in.gov.lrit.portal.model.user.PortalUser;

@Repository
public interface ShipingCompanyRepository extends JpaRepository<PortalShippingCompany,String> {

	//@Query(value="select case when count(login_id)> 0 then true else false end  from portal_users  where requestors_lrit_id = :lritId and status = 'active'",nativeQuery=true)
	//@Query(value="Select shipingdetails from PortalShippingCompany shipingdetails where shipingdetails.loginId= :portalUser.loginId",nativeQuery=true)
	//"SELECT * FROM lrit_latest_ship_postion t WHERE t.speed < ?1"
	@Query(value="SELECT * FROM portal_shipping_company shipingcompany WHERE shipingcompany.login_id = :loginId",nativeQuery=true)
	public PortalShippingCompany getDetails(String loginId);
	
	//select case when count(login_id)> 0 then true else false end  from portal_users  where requestors_lrit_id = :lritId and status = 'active'
	@Query(value="select case when count(user_id)> 0 then true else false end  from portal_shippingcompany_vessel_relation where user_id= :userId ",nativeQuery=true)
	public boolean getVessel(String userId);
	
	@Query(value="select case when count(vessel_id)> 0 then true else false end  from portal_shippingcompany_vessel_relation where current_cso_id= :csoId and de_reg_date is null",nativeQuery=true)
	public boolean getCsoVesselRelation(String csoId);
	
	@Query(value="select case when count(vessel_id)> 0 then true else false end  from portal_shippingcompany_vessel_relation where dpa_id= :csoId and de_reg_date is null",nativeQuery=true)
	public boolean getDpaVesselRelation(String csoId);
	

	@Query(value="Select case when count(shippingCompany)> 0 then true else false end  from portal_shipping_company shippingCompany where shippingCompany.company_code = :companyCode",nativeQuery=true)
	public boolean checkExistences(String companyCode);
	
	@Query(value="Select company_name  from portal_shipping_company shippingCompany where shippingCompany.company_code = :companyCode",nativeQuery=true)
	public String checkCompanyCodeExist(String companyCode);
	
	@Query(value = "select * from portal_shipping_company where company_code = :companyCode", nativeQuery = true)
	public String getCompanyNameByCode(String companyCode);
	
}
