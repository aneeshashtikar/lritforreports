package in.gov.lrit.portal.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import in.gov.lrit.portal.model.user.PortalUserSarAuthority;
@Repository
public interface PortalUserSarAuthorityRepository  extends JpaRepository<PortalUserSarAuthority, String > {
	@Query(value="select case when count(sarauthority)> 0 then true else false end from portal_user_sarauthority sarauthority where sarauthority.sar_authority = :lritId",nativeQuery=true)
	public boolean checkUser(String lritId);
	
	@Query(value="Select * from portal_user_sarauthority sarauthority where sarauthority.login_id = :loginId",nativeQuery=true)
	public PortalUserSarAuthority getSarDetails(String loginId);
	
	
	@Query(value="select sar_authority from portal_user_sarauthority where login_id=:loginId", nativeQuery=true)
	public String getSarAuthority(String loginId);
}
