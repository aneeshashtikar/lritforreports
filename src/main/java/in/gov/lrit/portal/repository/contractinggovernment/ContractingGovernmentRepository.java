package in.gov.lrit.portal.repository.contractinggovernment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.ddp.model.LritContractingGovtMst;
import in.gov.lrit.portal.model.contractinggovernment.PortalLritIdMaster;
import in_.gov.lrit.session.ContractingGovernmentInterface;


public interface ContractingGovernmentRepository extends JpaRepository<PortalLritIdMaster, String> {

	@Query(value="SELECT cg from PortalLritIdMaster cg where lower(cg.type)= 'cg' ")
	public List<PortalLritIdMaster> getContratingGovernment();
	
	
	
	@Query(value="SELECT lritid from PortalLritIdMaster lritid where lritid.countryName in (SELECT lritmaster.countryName from PortalLritIdMaster lritmaster where lritmaster.lritId = :countrylritId ) and lritid.type='sar'")
	public List<PortalLritIdMaster> getSarAuthorities(String countrylritId);
	
	@Query("select case when count(portallrit)> 0 then true else false end from PortalLritIdMaster portallrit where lower(portallrit.countryName) = lower(:countryName) and type='cg'")
	public boolean checkCountryExistance (String countryName);
	
	@Query("select case when count(portallrit)> 0 then true else false end from PortalLritIdMaster portallrit where lower(portallrit.countryCode) = lower(:countryCode) and type='cg'")
	public boolean checkCountryCodeExistance (String countryCode);
	
	
	@Query("select case when count(portallrit)> 0 then true else false end from PortalLritIdMaster portallrit where lower(portallrit.coastalArea) = lower(:coastalArea) and type='sar'")
	public boolean checkCoastalAreaExistance(String coastalArea);
	
	
	@Query("select case when count(portallrit)> 0 then true else false end from PortalLritIdMaster portallrit where portallrit.lritId = :lritId ")
	public boolean checkLritId (String lritId);
	
	@Query("SELECT portal from PortalLritIdMaster portal where  portal.lritId = :lritId")
	public PortalLritIdMaster getDetails(String lritId);
	
	@Modifying
    @Transactional
	@Query("DELETE from PortalLritIdMaster portal where lower(portal.countryName) = lower(:countryName) and lower(portal.type) = 'sar'")
	public void deleteCgSar(String countryName);
	
	
	
	@Modifying
    @Transactional
	@Query("DELETE from PortalLritIdMaster portal where lower(portal.lritId) = lower(:lritId) and lower(portal.type) = 'cg'")
	public void deleteCg (String lritId);
	
	@Query("SELECT portallrit from PortalLritIdMaster portallrit where portallrit.countryName = :countryName and lower(portallrit.type)='sar' ")
	public List<PortalLritIdMaster> getSarList(String countryName);
	
	@Query(value="select lrit_id from portal_lrit_id_master",nativeQuery=true)
	public List<String> getAllLritId();
	
	 @Query(value= "select context_id from portal_context where login_id=:loginId", nativeQuery = true)
	 public List<String> getContextListByLoginId(String loginId);
	 
	 
	 @Query(value= "select new in_.gov.lrit.session.ContractingGovernmentInterface"
	 		+ " (cg.lritId , cg.countryName  ) "
	 		+ " from PortalLritIdMaster cg where cg.lritId in (:contextList)")
	 public List<ContractingGovernmentInterface> getCGByContext(List<String> contextList);
	
}
