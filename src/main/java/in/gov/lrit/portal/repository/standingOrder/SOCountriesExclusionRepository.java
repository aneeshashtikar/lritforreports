package in.gov.lrit.portal.repository.standingOrder;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.standingOrder.SOCountriesExclusion;

@Repository
@Transactional
public interface SOCountriesExclusionRepository extends JpaRepository<SOCountriesExclusion, String> {
	
	//@Query(value = "Select * from portal_standingorders_countries_exclusion where standingorder_id= :SOId ", nativeQuery = true)
	//List<SOCountriesExclusion> findBySOId(int SOId);
	
	@Query(value = "Select * from portal_standingorders_countries_exclusion where standingorder_id= :SOId and area_code= :areacode", nativeQuery = true)
	List<SOCountriesExclusion> findBySOId(int SOId, String areacode);
	
	
	@Query(value = "Select excluded_countries from portal_standingorders_countries_exclusion where standingorder_id= :SOId and area_code= :areacode", nativeQuery = true)
	List<String> getFlagedCoun(int SOId, String areacode);
	
	void deleteBySoId(int SOId);

}
