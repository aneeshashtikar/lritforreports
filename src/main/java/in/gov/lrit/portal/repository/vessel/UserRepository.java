package in.gov.lrit.portal.repository.vessel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.user.PortalUser;

@Repository						
public interface UserRepository extends JpaRepository<PortalUser, String>{

	@Query("SELECT p FROM PortalUser p WHERE p.portalShippingCompany.companyCode = ?1 AND p.status = 'registered' AND p.category = 'USER_CATEGORY_SC' ")
	public List<PortalUser> findUserBycompanyCode(String companyCode);
	
	@Query("SELECT p FROM PortalUser p WHERE p.portalShippingCompany.companyCode = ?1 AND p.status = 'registered' AND p.category = 'USER_CATEGORY_SC' AND p.relation = 'owner'")
	public PortalUser findUserBycompanyCodeAndRelation(String companyCode);
	
	public PortalUser findByname(String name);

}