package in.gov.lrit.portal.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.user.PortalUserTransit;

@Repository
public interface PortalUserTransitRepository  extends JpaRepository<PortalUserTransit, String>{

}
