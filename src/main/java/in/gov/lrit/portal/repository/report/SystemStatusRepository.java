package in.gov.lrit.portal.repository.report;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.report.SystemStatus;

@Repository
public interface SystemStatusRepository extends JpaRepository<SystemStatus, String> {
	
	@Query(value = "select * from dc_systemstatus where originator = '3065' and receiver ='0001' and lrit_timestamp between ? and ? order by lrit_timestamp desc ", nativeQuery = true)
	public List<SystemStatus> getDdpSystemStatusLogs(Date startDate, Date endDate);

}
