package in.gov.lrit.portal.repository.report;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.portal.model.report.VwShipPositionRequest;

public interface VwShipPositionRequestRepository extends JpaRepository<VwShipPositionRequest, String>{
	
	@Query(value = ReportQueries.SHIP_POSITION_REQUEST_REPORT_FLAG, nativeQuery = true)
	public List<VwShipPositionRequest> getShipPositionRequestByReceiveTimeFlag(Date startDate, Date endDate);
	
	@Query(value =ReportQueries.SHIP_POSITION_REPORT, nativeQuery = true)
	public List<VwShipPositionRequest> getShipPositionReportByReceiveTime(Date startDate, Date endDate);
	
}
