package in.gov.lrit.portal.repository.requests;

import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.portal.model.request.LritLatestShipPositionNonFlag;
import in.gov.lrit.portal.model.request.ShipPositionNonFlag;


//@Repository
public interface LatestShipNonFlagRepository extends JpaRepository<LritLatestShipPositionNonFlag, String> {
	
	
	@Query(value="Select imo_no as imoNo ,mmsi_no as mmsiNo ,ship_name as vesselName ,data_user_provider as dataUserProvider from lrit_latest_ship_position_non_flag",nativeQuery = true)
	Collection<ShipPositionNonFlag> getimonofromnonflag();
}

