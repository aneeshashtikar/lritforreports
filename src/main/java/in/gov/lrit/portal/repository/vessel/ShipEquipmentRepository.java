package in.gov.lrit.portal.repository.vessel;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.vessel.PortalShipEquipment;
import in.gov.lrit.portal.model.vessel.ShipEquipmentAndVesselInterface;
import in.gov.lrit.portal.model.vessel.ShipEquipmentIInterface;

@Repository
@Transactional
public interface ShipEquipmentRepository extends JpaRepository<PortalShipEquipment,String>  {

	@Query(value="select * from portal_ship_equipement where vessel_id= :vesselId",nativeQuery=true)
	PortalShipEquipment findByvesselId(Integer vesselId);

	@Modifying
	@Transactional
	@Query(value="DELETE from portal_ship_equipement where vessel_id=:vesselId", nativeQuery=true)
	public Integer deleteSeid(int vesselId);

	@Query(value="select dnid_status from portal_ship_equipement where vessel_id=:vesselId", nativeQuery=true)
	String findDnidStatusByvesselId(Integer vesselId);

	@Query(value="update portal_ship_equipement  set dnid_no=:dnidNo, member_no=:memberNo where vessel_id=:vesselId", nativeQuery=true)
	@Modifying
	int updateDnid(int vesselId, int dnidNo, int memberNo );

	/*@Query("SELECT count(*) FROM PortalShipEquipment p WHERE p.shipborneEquipmentId = ?1 AND p.seidStatus = 'SEID_DELETED' AND p.dnidStatus = 'DNID_DELETED' ")
	public Integer findIfDnidSeidStatusDeleted(String shipEquipId);*/
	/*@Query("SELECT count(*) FROM PortalShipEquipment p WHERE p.vesselDet.vesselId in (SELECT q.vesselId FROM PortalVesselDetail q WHERE q.regStatus = 'SEID_DELETED') AND p.shipborneEquipmentId = ?1 ")
	public Integer findIfDnidSeidStatusDeleted(String shipEquipmentId);*/
	@Query("SELECT count(*) FROM PortalVesselDetail q WHERE q.regStatus = 'SEID_DELETED' AND q.vesselId = ?1 ")
	public Integer findIfDnidSeidStatusDeleted(Integer vesselId);

	/*public PortalShipEquipment findByvesselDet_vesselIdAndSeidStatus(Integer vesselId, String seidStatus);*/
	@Query("SELECT p FROM PortalShipEquipment p WHERE p.vesselDet.vesselId in (SELECT q.vesselId FROM PortalVesselDetail q WHERE q.regStatus != 'SEID_DELETED') AND p.vesselDet.vesselId = ?1 ")
	public PortalShipEquipment findByvesselDet_vesselId(Integer vesselId);

	/*@Query("SELECT count(*) FROM PortalShipEquipment p WHERE p.shipborneEquipmentId = ?1 AND p.seidStatus = 'Active' ")
	public Integer isShipEquipIdExist(String shipEquipmentId);*/

	@Query("SELECT count(*) FROM PortalShipEquipment p WHERE p.vesselDet.vesselId in (SELECT q.vesselId FROM PortalVesselDetail q WHERE q.regStatus != 'SEID_DELETED') AND p.shipborneEquipmentId = ?1 ")
	public Integer isShipEquipIdExist(String shipEquipmentId);

	/*	@Modifying
	@Transactional
	@Query("UPDATE PortalShipEquipment p SET p.dnidStatus = ?1, p.dnidStatusDate = now(), p.seidStatusDate = now() WHERE p.shipborneEquipmentId = ?2")
	public Integer updatednidStatusAnddnidStatusDate(String dnidStatus, String shipequiId);*/

	/*public PortalShipEquipment saveBy_shipborneEquipmentId(String y);*/

	@Modifying
	@Transactional
	@Query("UPDATE PortalShipEquipment p SET p.requestPayload = ?1, p.responsePayload = ?2 WHERE p.shipborneEquipmentId = ?3")
	public Integer updateShipEquipByImoNo(String requestPayload, String responsePayload, String shipequiId);

	@Modifying
	@Transactional
	@Query("UPDATE PortalShipEquipment p SET p.responsePayload = ?1 WHERE p.shipborneEquipmentId = ?2")
	public Integer updateResponsePayload(String responsePayload, String shipequiId);

	@Modifying
	@Transactional
	@Query("UPDATE PortalShipEquipment p SET p.requestPayload = ?1 WHERE p.shipborneEquipmentId = ?2")
	public Integer updateRequestPayload(String requestPayload, String shipequiId);
	
	@Modifying
	@Transactional
	@Query("UPDATE PortalShipEquipment p SET p.dnidNo = ?1, p.memberNo = ?2 WHERE p.shipborneEquipmentId = ?3")
	public Integer updateShipEquipById(Integer dnidNo, Integer memberNo, String shipequiId);

	//@Query(value="select PortalShipEquipment from PortalShipEquipment where vesselId= :vesselId")
	//public PortalShipEquipment getShipEquipmentDetails(Integer vesselId);
	@Modifying
	@Query(value="delete from portal_ship_equipement where vessel_id= :vesselId", nativeQuery=true)
	public void deleteShipEquipmentDetails(Integer vesselId);

	@Query(value="update portal_ship_equipement set dnid_no = :dnidNo, member_no= :memberNo where vessel_id= :vesselId", nativeQuery=true)
	@Modifying 
	public int updateDnidAndMemberNo(Integer vesselId, Integer memberNo, Integer dnidNo);

	 @Query(value="select dnid_no as dnidNo, member_no as memberNo from portal_ship_equipement where vessel_id=:vesselId", nativeQuery=true) 
	 public ShipEquipmentIInterface findShipIntefaceByVesselId(Integer vesselId);

	/*
	 * @Query(
	 * value="select dnid_no, member_no from portal_ship_equipement where vessel_id=:vesselId "
	 * , nativeQuery=true) public ShipEquipmentIInterface getDnidMemberNo(Integer
	 * vesselId);
	 */

	 @Query(value="update portal_ship_equipement set request_payload = :req, response_payload= :response where vessel_id= :vesselId", nativeQuery=true)
	@Modifying 
	public int updateRequestpayload(Integer vesselId, String req, String response);

	 @Query("select shipb from PortalShipEquipment shipb where shipb.vesselDet.status IN (:status) and shipb.vesselDet.regStatus NOT IN(:regstatus)")
	public List<ShipEquipmentAndVesselInterface> getShipEquipmentAndVesselDetails(List<String> status,List<String> regstatus);
}
