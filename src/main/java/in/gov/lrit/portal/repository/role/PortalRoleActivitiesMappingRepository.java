package in.gov.lrit.portal.repository.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.role.PortalRoleActivitiesMapping;
@Repository
public interface PortalRoleActivitiesMappingRepository extends JpaRepository<PortalRoleActivitiesMapping,String> {

}
