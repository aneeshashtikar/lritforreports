package in.gov.lrit.portal.repository.report;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.portal.model.report.VwGeographicalAreaUpdate;

public interface VwGeographicalAreaUpdateRepository extends JpaRepository<VwGeographicalAreaUpdate, String>{
	
	
	@Query(value = ReportQueries.GEOGRAPHICAL_AREA_UPDATE_TO_IDE_FLAG, nativeQuery = true)
	public List<VwGeographicalAreaUpdate> getGeographicalAreaOfFlagToIde(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.GEOGRAPHICAL_AREA_UPDATE_TO_IDE_OTHER, nativeQuery = true)
	public List<VwGeographicalAreaUpdate> getGeographicalAreaOfOtherToIde(String lritId, Date startDate, Date endDate);

	@Query(value = ReportQueries.GEOGRAPHICAL_AREA_UPDATE_TO_DDP_FLAG, nativeQuery = true)
	public List<VwGeographicalAreaUpdate> getGeographicalAreaOfFlagToDdp(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.GEOGRAPHICAL_AREA_UPDATE_TO_DDP_OTHER, nativeQuery = true)
	public List<VwGeographicalAreaUpdate> getGeographicalAreaOfOtherToDdp(String lritId, Date startDate, Date endDate);
}
