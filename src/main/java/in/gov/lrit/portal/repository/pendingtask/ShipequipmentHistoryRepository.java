package in.gov.lrit.portal.repository.pendingtask;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.pendingtask.PortalShipEquipementHst;
import in.gov.lrit.portal.model.pendingtask.ShipEquipmentHstInterface;

@Repository
@Transactional
public interface ShipequipmentHistoryRepository extends JpaRepository<PortalShipEquipementHst, Integer>{
	
	
	@Query(value="select dnid_no as dnidNo, member_no as memberNo from portal_ship_equipement_hst where id=:vesselId", nativeQuery=true)
	public ShipEquipmentHstInterface getDnidMemberNo(Integer vesselId);
	
	@Modifying
	@Query(value="update portal_ship_equipement_hst set member_no=:memberNo, dnid_no=:dnidNo where id=:vesselId",nativeQuery=true)
	public int updateDnidMemberNo(Integer vesselId, Integer memberNo, Integer dnidNo);

}
