package in.gov.lrit.portal.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.contractinggovernment.PortalContext;
import in.gov.lrit.portal.model.user.PortalCsoDpaDetail;

public interface PortalCsoDpaDetailRepository extends JpaRepository<PortalCsoDpaDetail, String> {

	@Query(value = "Select * from portal_cso_dpa_details csodpadetails where csodpadetails.login_id = :loginId and csodpadetails.type =:type and csodpadetails.deregister_date is null ", nativeQuery = true)
	public List<PortalCsoDpaDetail> getCurrentCsoDpaDetails(String loginId, String type);

	@Modifying
	@Transactional
	@Query(value = "Update PortalCsoDpaDetail csoDpaDetails set csoDpaDetails.deregisterDate= now() where csoDpaDetails.csoidseq= :csoId")
	public int updateDeregsiterdate(String csoId);

	@Modifying
	@Transactional
	@Query(value = "Update PortalCsoDpaDetail csoDpaDetails set csoDpaDetails.deregisterDate= now() where csoDpaDetails.parentCsoid= :CsoId")
	public void setDeregsiterdateAlternateCso(String CsoId);

	// csoDpaDetails.type = :#{#updateCsoDpa.type}
	@Modifying
	@Transactional

	@Query(value = "Update PortalCsoDpaDetail csoDpaDetails set csoDpaDetails.name = :#{#updateCsoDpa.name},"
			+ " csoDpaDetails.telephoneOffice = :#{#updateCsoDpa.telephoneOffice},csoDpaDetails.emailid = :#{#updateCsoDpa.emailid},"
			+ "csoDpaDetails.telephoneResidence = :#{#updateCsoDpa.telephoneResidence}, csoDpaDetails.mobileNo = :#{#updateCsoDpa.mobileNo} "
			+ " where csoDpaDetails.csoidseq= :#{#updateCsoDpa.csoidseq}")
	public void updateCsoDpaDetails(PortalCsoDpaDetail updateCsoDpa);

	/*
	 * @Modifying
	 * 
	 * @Transactional
	 * 
	 * @Query(
	 * value="Update PortalCsoDpaDetail csoDpaDetails set csoDpaDetails.name = :#{#updateCsoDpa.name},"
	 * +
	 * " csoDpaDetails.telephoneOffice = :#{#updateCsoDpa.telephoneOffice},csoDpaDetails.emailid = :#{#updateCsoDpa.emailid},"
	 * +
	 * "csoDpaDetails.telephoneResidence = :#{#updateCsoDpa.telephoneResidence}, csoDpaDetails.mobileNo = :#{#updateCsoDpa.mobileNo} "
	 * + " where csoDpaDetails.parentCsoid= :#{#updateCsoDpa.parentCsoid}" ) public
	 * void updateAlternateCsoDetails(PortalCsoDpaDetail updateCsoDpa);
	 */

	@Query(value = "Select csoDpaDetails from PortalCsoDpaDetail csoDpaDetails where csoDpaDetails.csoidseq = :csoId ")
	public PortalCsoDpaDetail getCsoDpaDetails(String csoId);

	@Query(value = "Select csodetails from PortalCsoDpaDetail csodetails where csodetails.loginId = :loginId ")
	public List<PortalCsoDpaDetail> getCsoDetails(String loginId);

	@Query(value = "Select csodetails from PortalCsoDpaDetail csodetails where csodetails.type='cso' and csodetails.loginId = :loginId  and csodetails.deregisterDate is null ")
	public List<PortalCsoDpaDetail> getCsoList(String loginId);

	@Query(value = "Select csodetails from PortalCsoDpaDetail csodetails where csodetails.type='dpa' and csodetails.loginId = :loginId and csodetails.deregisterDate is null ")
	public List<PortalCsoDpaDetail> getDpaList(String loginId);

	
	@Modifying
	@Transactional
	@Query(value = "Update PortalCsoDpaDetail csoDpaDetails set csoDpaDetails.parentCsoid= :#{#portalCsoDpaDetail.parentCsoid} where csoDpaDetails.csoidseq= :#{#portalCsoDpaDetail.csoidseq}")
	public void updateAlternateCsoId(PortalCsoDpaDetail portalCsoDpaDetail);

	
	@Query(value="select csodetails from PortalCsoDpaDetail csodetails where csodetails.loginId = :loginId and deregisterDate is null")
	public List<PortalCsoDpaDetail> getCsoDpaList(String loginId);
	
	@Query(value="select * from portal_cso_dpa_details where lrit_csoid_seq = (select current_cso_id from portal_shippingcompany_vessel_relation where relation in (0,3,4,6)" + 
			"AND vessel_id = (select vessel_id from portal_vessel_details where imo_no = :imoNo and status = 'ACTIVE'))", nativeQuery = true)
	public PortalCsoDpaDetail getCurrentCsoDetails(String imoNo);
	
	@Query(value="select * from portal_cso_dpa_details where lrit_csoid_seq = (select alternate_cso_id from portal_shippingcompany_vessel_relation where relation in (0,3,4,6) " + 
			"			AND vessel_id = (select vessel_id from portal_vessel_details where imo_no = :imoNo and status = 'ACTIVE'));", nativeQuery = true)
	public PortalCsoDpaDetail getAlternateCsoDetails(String imoNo);
	

}
