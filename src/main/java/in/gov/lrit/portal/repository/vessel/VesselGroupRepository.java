package in.gov.lrit.portal.repository.vessel;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.vessel.GroupList;
import in.gov.lrit.portal.model.vessel.PortalVesselGroup;

@Repository
public interface VesselGroupRepository extends JpaRepository<PortalVesselGroup, Long>{
	
	@Query("SELECT p.groupName FROM PortalVesselGroup p")
	public ArrayList<String> findAllGroupName();
	
	public ArrayList<GroupList> findAllBy();
	
	@Query("SELECT p.groupId FROM PortalVesselGroup p WHERE p.groupName = ?1")
	public Long findGroupIdByName(String groupName);
	
	@Transactional
	@Modifying
	@Query("UPDATE PortalVesselGroup p SET p.shortName = ?1, p.description=?2 WHERE p.groupId = ?3")
	public Integer updateVesselGroup(String shortName, String description, Long groupId);

	public int countBygroupName(String groupName);
	
	public Optional<PortalVesselGroup> findBygroupId(Long groupId);
	
	public PortalVesselGroup findBygroupName(String groupName);
	
	public int countBygroupNameAndShortName(String groupName, String shortName);
}