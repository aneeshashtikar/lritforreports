package in.gov.lrit.portal.repository.vessel;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.vessel.ModelList;
import in.gov.lrit.portal.model.vessel.PortalModelDetail;

@Repository
public interface ModelRepository extends JpaRepository<PortalModelDetail, Integer>{
	
	
	public Iterable<PortalModelDetail> findBymanuDet_manufacturerIdAndStatus(Integer manuId, String status);
	
	@Query("SELECT count(*) FROM PortalModelDetail p where p.modelType = ?1 and p.manuDet.manufacturerId = ?2 and p.modelName = ?3")
	public int isModelExist(String modelType, Integer manufacturerId, String modelName);
	
	@Modifying
	@Transactional
	@Query("Update PortalModelDetail p set p.status= 'Deactive', p.deRegDate = now() where p.modelId = ?1")
	public Integer updateModelById(Integer modelId);

	public Iterable<PortalModelDetail> findBymanuDet_manufacturerId(Integer manuId);
	
	@Modifying
	@Transactional
	@Query("Update PortalModelDetail p set p.status= 'Deactive', p.deRegDate = now() where p.manuDet.manufacturerId = ?1")
	public Integer updateModelByManuId(Integer manuId);
	
	@Query("SELECT count(*) FROM PortalModelDetail p where p.manuDet.manufacturerId = ?1")
	public int ifNoModelExistForManufacturerId(Integer manufacturerId);
	
	@Query("SELECT p.modelType FROM PortalModelDetail p WHERE p.manuDet.manufacturerId = ?1 AND p.status = 'Active' AND p.deRegDate IS NULL ")
	public ArrayList<String> findModelTypeByManufacturer(Integer manufacturerId);
	
	@Query("SELECT p.modelName FROM PortalModelDetail p WHERE p.modelType = ?1 AND p.manuDet.manufacturerId = ?2 AND p.status = 'Active' ")
	public ArrayList<String> findmodelListbymodelTypeAndManufacturerId(String modelType, Integer manufacturerId);
	
	public ArrayList<PortalModelDetail> findByModelTypeAndManuDet_ManufacturerIdAndStatus(String modelType, Integer manufacturerId, String status);
	
	@Query("SELECT p FROM PortalModelDetail p WHERE p.modelType = ?1 AND p.manuDet.manufacturerId = ?2 AND p.modelName = ?3 AND p.status = 'Active' ")
	public PortalModelDetail findModelByManufacturerTypeName(String modelType, Integer manufacturerId, String modelName);

}