package in.gov.lrit.portal.repository.user;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import in.gov.lrit.portal.model.user.PortalCsoDpaDetail;
import in.gov.lrit.portal.model.user.PortalUser;
import in.gov.lrit.portal.model.user.PortalUserInterface;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;
import in_.gov.lrit.session.UserSessionInterface;

@Repository
@Transactional
public interface UserManagementRepository extends JpaRepository<PortalUser, String> {


	@Query(value = "select case when count(login_id)> 0 then true else false end  from portal_users  where requestors_lrit_id = :lritId and status = 'registered'", nativeQuery = true)
	public boolean getUser(String lritId);

	@Query(value = "select users from PortalUser users ")
	public List<PortalUser> getUsersList();

	@Query(value = "select login_id as loginId, name, category, status , country_name AS country from portal_users users INNER JOIN portal_lrit_id_master cg On users.requestors_lrit_id = cg.lrit_id where status=:status ", nativeQuery = true)
	public List<PortalUserInterface> getUsersList(String status);

	@Query(value = "SELECT login_id as loginId, name, category, status, country_name AS country "
			+ " FROM portal_users users INNER JOIN portal_lrit_id_master cg On users.requestors_lrit_id = cg.lrit_id", nativeQuery = true)
	public List<PortalUserInterface> getAllUsersList();

	public List<PortalUserInterface> findByStatus(String status);


	@Query(value = "Select user from PortalUser user where user.loginId = :loginId ")
	public PortalUser getUserDetails(String loginId);

	@Query(value = "Select user from PortalUser user where user.loginId = :loginId and status in ('registered','incomplete') ")
	public PortalUser getActiveUserDetails(String loginId);

	@Transactional
	@Modifying
	@Query(value = "Update PortalUser user set user.status='deregistered', user.deregisterDate='now()' where user.loginId = :loginId ")
	public int deregisterUser(String loginId);

	@Transactional
	@Modifying
	@Query(value = "Update PortalUser user set user.name = :#{#portalUser.name}, user.address1 = :#{#portalUser.address1}, "
			+ "user.address2 = :#{#portalUser.address2}, user.address3 = :#{#portalUser.address3}, user.city = :#{#portalUser.city}, "
			+ " user.district = :#{#portalUser.district}, user.landmark = :#{#portalUser.landmark}, user.state = :#{#portalUser.state}, "
			+ " user.country = :#{#portalUser.country}, user.telephone = :#{#portalUser.telephone}, user.mobileno = :#{#portalUser.mobileno}, "
			+ "user.fax = :#{#portalUser.fax},  user.emailid = :#{#portalUser.emailid},user.alternateemailid = :#{#portalUser.alternateemailid},  user.website = :#{#portalUser.website}, "
			+ " user.sarAreaView = :#{#portalUser.sarAreaView} ,user.pincode = :#{#portalUser.pincode},user.uploadedFilePath = :#{#portalUser.uploadedFilePath}  where user.loginId = :#{#portalUser.loginId} ")
	public int updateUser(@Param("portalUser") PortalUser portalUser);

	@Transactional
	@Modifying
	@Query(value = "Update PortalUser user set user.validateToken = :token , user.tokenvalidupto = :oneDaytimestamp where user.loginId = :loginId")
	public int updateToken(String token, Timestamp oneDaytimestamp, String loginId);

	@Query(value = "Select config.paravalue from config_para config where config.paraname='LritportalpathNDC'", nativeQuery = true)
	public String getWebPathNDC();

	@Query(value = "Select config.paravalue from config_para config where config.paraname='LritportalpathRDC'", nativeQuery = true)
	public String getWebPathRDC();

	@Query(value = "Select password from portal_users_password  where login_id= :loginId ORDER BY expiry_date desc limit 2 ", nativeQuery = true)
	public List<String> getPassword(String loginId);

	@Transactional
	@Modifying
	@Query(value = "Update PortalUser user set user.validateToken=null, user.password = :password, user.noOfPasswordAttempt = 4 , user.tokenvalidupto =null where user.loginId = :loginId  ")
	public void updatePassword(String loginId, String password);

	@Query(value = "Select case when count(user)> 0 then true else false end  from PortalUser user  where user.loginId=:loginId")
	public boolean checkUserExist(String loginId);

	@Transactional
	@Modifying
	@Query(value = "Update PortalUser user set user.status= :status where user.loginId=:loginId")
	public void updateStatus(String loginId, String status);

	// Spring Security related methods
	@Query("select users from PortalUser users where users.loginId=:loginId")
	PortalUser findLoginId(String loginId);

	/*
	 * @Override Optional<PortalUser> findById(String loginId);
	 */

	@Query(value = UserQueries.GET_USER_ACTIVITIES, nativeQuery = true)
	public List<String> getAllActivitiesByLoginId(String loginId);

	/* Getting deregister date and number of attempt */
	PortalUserInterface findByLoginId(String loginId);

	// get from session id
	@Query(value = "select password from portal_users where login_id=:loginId", nativeQuery = true)
	String getPasswordByLoginId(String loginId);

	@Modifying
	@Query(value = "update PortalUser users set users.noOfPasswordAttempt = :noOfAttempt where users.loginId= :loginId")
	int updateLoginAttempt(int noOfAttempt, String loginId);

	@Query("select new in_.gov.lrit.session.UserSessionInterface"
			+ " ( p.name, p.emailid, p.category,  p.loginId, p.requestorsLritId) "
			+ " from PortalUser p where p.loginId=:loginId")
	UserSessionInterface findUserSessionDataByLoginId(String loginId);

	@Query(value = "select * from portal_users where login_id = (select user_id from portal_shippingcompany_vessel_relation where relation in (0,3,4,6) AND "
			+ "vessel_id = (select vessel_id from portal_vessel_details where imo_no = :imoNo and status = 'ACTIVE'));", nativeQuery = true)
	public PortalUser getUserDetailsByImo(String imoNo);

	@Query(value = UserQueries.GET_PENDING_TASK_CATEGORY, nativeQuery = true)
	public List<String> getActivityListByType(String loginId, String type);

}
