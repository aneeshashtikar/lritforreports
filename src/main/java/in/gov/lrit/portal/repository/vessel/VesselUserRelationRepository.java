package in.gov.lrit.portal.repository.vessel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.vessel.PortalShippingCompanyVesselRelation;
import in.gov.lrit.portal.model.vessel.PortalVesselDetail;
import in.gov.lrit.portal.model.vessel.VesselLoginMappingId;

@Repository
public interface VesselUserRelationRepository
		extends JpaRepository<PortalShippingCompanyVesselRelation, VesselLoginMappingId> {

	/*
	 * public List<PortalShippingCompanyVesselRelation>
	 * saveAll(List<PortalShippingCompanyVesselRelation> pscvrlist);
	 */

	public List<PortalShippingCompanyVesselRelation> findBycustomId_vesselDet_vesselId(Integer vesselId);

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param companyCode
	 * @return
	 */
	@Query("Select sr.customId.vesselDet.vesselId from PortalShippingCompanyVesselRelation sr left join sr.customId.userDet user where user.portalShippingCompany.companyCode = :companyCode and sr.rel = 0")
	public List<Integer> findVesselsByCompanyCode(@Param("companyCode") String companyCode);

	// commenting following query due to querysyntax error saying " Expecting OPEN
	// found "." near line 1 column 69"
	// @Query("Select psvr.vesselId from PortalUser p,
	// PortalShippingcompanyVesselRelation psvr where ((p.companyCode =
	// :companyCode) and (p.loginId = psvr.userId) and (p.category like
	// 'USER_CATEGORY_SC'))")
	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param companyCode
	 * @return
	 */
	@Query(value = "Select psvr.vessel_id from portal_users pu, portal_shippingcompany_vessel_relation psvr where ((pu.company_code = :companyCode) and (pu.login_id = psvr.user_id) and (pu.category like 'USER_CATEGORY_SC'))", nativeQuery = true)
//	@Query(value = "Select sr.rel from PortalShippingCompanyVesselRelation sr")
	public List<Integer> findVesselsIdsbyCompanyCode(@Param("companyCode") String companyCode);

	/**
	 * 
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param companyCode
	 * @return
	 */
	@Query("Select sr.customId.vesselDet from PortalShippingCompanyVesselRelation sr left join sr.customId.userDet user where user.portalShippingCompany.companyCode = :companyCode and sr.rel = 0")
//	@Query("Select sr.customId.vesselDet from PortalShippingcompanyVesselRelation sr")
	public List<PortalVesselDetail> findVesselsDetailsByCompanyCode(@Param("companyCode") String companyCode);

}