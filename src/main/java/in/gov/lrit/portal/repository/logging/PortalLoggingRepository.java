package in.gov.lrit.portal.repository.logging;


import java.sql.Timestamp;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.logging.PortalLoginAudit;

@Repository
@Transactional
public interface PortalLoggingRepository extends JpaRepository<PortalLoginAudit,Integer > {
	
	
	@Modifying
	@Query("update PortalLoginAudit pla set pla.geolocation = :longlat where pla.id = :id")
	int updateGeoLocation(Integer id, String longlat);
	
	@Modifying
	@Query("update PortalLoginAudit pla set pla.logoutTime = :logoutTime , pla.loginStatus = :status where pla.id= :id")
	int updateLogOutTime(Integer id,Timestamp logoutTime, String status );
	
}
