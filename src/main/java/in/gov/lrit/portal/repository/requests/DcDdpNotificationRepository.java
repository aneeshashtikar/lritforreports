package in.gov.lrit.portal.repository.requests;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.DcDdpnotification;


@Repository
public interface DcDdpNotificationRepository extends JpaRepository<DcDdpnotification, Integer> {

	@Query(value = "select message_id from dc_ddpnotification order by lrit_timestamp desc limit 1 ", nativeQuery = true)
	String  findbyMessageId(); 

}
