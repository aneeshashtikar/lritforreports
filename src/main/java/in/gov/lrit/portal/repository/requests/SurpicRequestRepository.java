package in.gov.lrit.portal.repository.requests;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalSurpicRequest;

@Repository
public interface SurpicRequestRepository extends JpaRepository<PortalSurpicRequest, Integer> {
	
	@Query("select sr from PortalSurpicRequest sr where sr.portalRequest.messageId=:messageId" )
	PortalSurpicRequest findByMessageId(String messageId);
	
}
