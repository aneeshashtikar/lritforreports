package in.gov.lrit.portal.repository.vessel;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.vessel.PortalVesselType;
@Repository
public interface VesselTypeRepository extends JpaRepository<PortalVesselType, String>{
	
	@Query("SELECT DISTINCT p.vesselType FROM PortalVesselType p")
	public ArrayList<String> findAllVesselType();
	
	@Query("SELECT  p FROM PortalVesselType p WHERE p.vesselType = ?1")
	public PortalVesselType findByType(String vesselType);
	
	@Query("SELECT  p.vesselTypeId FROM PortalVesselType p WHERE p.vesselType = ?1")
	public String findIdByType(String vesselType);

}