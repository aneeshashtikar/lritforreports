package in.gov.lrit.portal.repository.requests;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.DcPositionReport;
import in.gov.lrit.portal.model.request.DcSarsurpicrequest;


@Repository
public interface DcPositionReportRepository extends JpaRepository<DcPositionReport, Integer> {
	
	@Query(value = "select * from dc_position_report where reference_id in ?1 ", nativeQuery = true)
	List<DcPositionReport> findbyReferenceId(List<String> referenceid); 
	
}
