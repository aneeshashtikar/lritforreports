package in.gov.lrit.portal.repository.requests;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalDdpRequest;

@Repository
public interface DdpRequestRepository extends JpaRepository<PortalDdpRequest, Integer> {
	

}
