package in.gov.lrit.portal.repository.report;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vividsolutions.jts.geom.Geometry;

import in.gov.lrit.gis.ddp.model.SOFetchModel;
import in.gov.lrit.portal.model.report.LritLatestShipPostion;
import in.gov.lrit.portal.model.report.VwSarSurpicRequest;

//@Repository
public interface ReportRepository extends JpaRepository<LritLatestShipPostion, String> {
	
	@Query(
			  value = ReportQueries.SHIPSPEED_POSITION_REPORT, 
			  nativeQuery = true)
	List<LritLatestShipPostion> showShipSpeedReport(int shipSpeed);
	List<LritLatestShipPostion> findAll();
	
	
}
