package in.gov.lrit.portal.repository.requests;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalDdpRequest;
import in.gov.lrit.portal.model.request.PortalFlagRequest;

@Repository
public interface FlagRequestRepository extends JpaRepository<PortalFlagRequest, Integer> {
	

}
