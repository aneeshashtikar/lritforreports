package in.gov.lrit.portal.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.portal.model.user.PortalUsersPassword;

public interface PortalUserPasswordRepository extends JpaRepository<PortalUsersPassword, String> {

}
