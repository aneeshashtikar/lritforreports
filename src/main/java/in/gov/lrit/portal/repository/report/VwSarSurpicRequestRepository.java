package in.gov.lrit.portal.repository.report;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.portal.model.report.VwSarSurpicRequest;

public interface VwSarSurpicRequestRepository  extends JpaRepository<VwSarSurpicRequest, String> {

	@Query(value = ReportQueries.SARSURPIC_OUR_CIRCULAR_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicOurCircularRequests(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.SARSURPIC_OUR_RECTANGULAR_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicOurRectangularRequests(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.SARSURPIC_OUR_BOTH_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicOurBothRequests(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.SARSURPIC_FOREIGN_CIRCULAR_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicForeignCircularRequests(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.SARSURPIC_FOREIGN_RECTANGULAR_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicForeignRectangularRequests(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.SARSURPIC_FOREIGN_BOTH_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicForeignBothRequests(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.SARSURPIC_BOTH_CIRCULAR_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicBothCircularRequests(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.SARSURPIC_BOTH_RECTANGULAR_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicBothRectangularRequests(Date startDate, Date endDate);
	
	@Query(value = ReportQueries.SARSURPIC_ALL_REQUEST, nativeQuery = true)
	List<VwSarSurpicRequest> sarSurpicAllRequest(Date startDate, Date endDate);

	@Query(value = ReportQueries.SARSURPIC_RESPONSES, nativeQuery = true)
	List<VwSarSurpicRequest> SarSurpicResponses(String messagId);
	
}
