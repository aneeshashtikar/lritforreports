package in.gov.lrit.portal.repository.requests;

import java.sql.Timestamp;

import javax.transaction.Transactional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.request.PortalRequest;

@Repository
@Transactional
public interface PortalRequestRepository extends JpaRepository<PortalRequest, Integer> {
	
	@Query(value="update portal_requests set approvers_login_id= :approversLoginId, status= :status , status_date= :statusDate, "
			+ "response_payload= :responsePayload where message_id = :messageId", nativeQuery=true)
	@Modifying
	public int updateRequestStatus(String messageId, String approversLoginId, String status, Timestamp statusDate,
			String responsePayload);


	@Modifying
	@Transactional
	@Query("Update PortalRequest portalreq set portalreq.responsepayload=:response where portalreq.messageId=:messageid")
	int  setResponsePayload(String response,String messageid); 
	
	@Query("select requestpayload from  PortalRequest where messageId=:messageId")
	public String findrequestPayloadById(String messageId);
	
	void deleteByMessageId(String msgId);
}
