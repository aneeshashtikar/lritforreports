package in.gov.lrit.portal.repository.standingOrder;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.standingOrder.standingOrderRequest;

@Repository
@Transactional
public interface SORequestRepository extends JpaRepository<standingOrderRequest, String> {
	
	@Query(value = "Select * from portal_standingorders_request where standingorder_id= :soId ", nativeQuery = true)
	standingOrderRequest findBySOId(int soId);
	
	@Query(value = "Select * from portal_standingorders_request where status= :status ", nativeQuery = true)
	List<standingOrderRequest> findBySOStatus(String status);
	
	@Modifying
	@Query(value ="UPDATE portal_standingorders_request SET status= :status where standingorder_id= :soId", nativeQuery = true)
	void updateByStatus(int soId, String status);
	
	@Modifying
	@Query(value ="UPDATE portal_standingorders_request SET status= 'Closed' where status= :status", nativeQuery = true)
	void updateOpenedStatus(String status);
	
	@Modifying
	@Query(value ="DELETE from portal_standingorders_request WHERE standingorder_id = :soId", nativeQuery = true)
	void deleteBySoId(int soId);
}