package in.gov.lrit.portal.repository.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.role.PortalActivities;


@Repository
public interface ActivityManagementRepository extends JpaRepository<PortalActivities, String > {

	 
	//@Query(value="SELECT a.activityname from portal_activities a",nativeQuery = true)
	//public List<String> getActivities() ;
	//@Query(value="SELECT u FROM portal_activities u ",nativeQuery = true)
//	public List<PortalActivities> f() ;
	
}
