package in.gov.lrit.portal.repository.vessel;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.vessel.PortalCSODPADetails;

@Repository
public interface CSODPADetailsRepository extends JpaRepository<PortalCSODPADetails, String>{
	
	@Query("SELECT p FROM PortalCSODPADetails p WHERE p.portalUser.loginId = ?1 AND p.deRegDate IS NULL AND p.type = ?2 AND p.alternateCsoId IS NOT NULL")
	public List<PortalCSODPADetails> findCSOByportalUser_loginIdAndType(String loginId, String type);
	
	@Query("SELECT p FROM PortalCSODPADetails p WHERE p.portalUser.loginId = ?1 AND p.deRegDate IS NULL AND p.type = ?2 ")
	public List<PortalCSODPADetails> findDPAByportalUser_loginIdAndType(String loginId, String type);
	
	@Query("SELECT p FROM PortalCSODPADetails p WHERE p.lritCsoIdSeq in (SELECT q.alternateCsoId FROM PortalCSODPADetails q WHERE q.lritCsoIdSeq = ?2) AND p.portalUser.loginId = ?1 AND p.deRegDate IS NULL AND p.type = 'cso' ")
	public List<PortalCSODPADetails> findACSOByportalUser_loginIdAndLritCsoIdSeq(String loginId, String lritCsoIdSeq);
	
	/*@Query("SELECT p FROM PortalCSODPADetails p WHERE p.portalUser.loginId = ?1 AND p.deRegDate IS NULL AND p.type = 'cso' AND p.alternateCsoId IS NULL ")
	public List<PortalCSODPADetails> findACSOByportalUser_loginId(String loginId);*/
	
	public PortalCSODPADetails findByname(String name);
	
	/*@Query("SELECT p.name FROM PortalCSODPADetails p WHERE p.companyCode = ?1 AND p.type = 'current_cso' ")
	public ArrayList<String> findCurrentCsoListBycompanyCode(String companyCode);
	
	@Query("SELECT p.name FROM PortalCSODPADetails p WHERE p.companyCode = ?1 AND p.type = 'alt_cso'")
	public ArrayList<String> findAltCsoListBycompanyCode(String companyCode);
	
	@Query("SELECT p.name FROM PortalCSODPADetails p WHERE p.companyCode = ?1 AND p.type = 'dpa'")
	public ArrayList<String> findDPAListBycompanyCode(String companyCode);*/
	
	/*@Query("SELECT p.name, p.type FROM PortalCSODPADetails p where p.companyCode = ?1")
	public List<CSOALTCSODPAName> findnameAndtypeBycompanyCode(String companyCode);*/
	
	/*@Query("SELECT p.name, p.type FROM PortalCSODPADetails p where p.companyCode = ?1")
	public Map<String, String> findnameAndtypeBycompanyCode(String companyCode);*/
	
	/*@Query(value = "SELECT * FROM  PortalCSODPADetails p where p.type= ?1 And p.shipCom.companyCode=?2", nativeQuery=true)
	public List<PortalCSODPADetails> getCSODPAList(String type,String companyCode);*/
	
	/*@Query("SELECT p FROM PortalDPADetails p WHERE p.portalUser.loginId = ?1 AND p.type = ?2 AND p.deRegDate IS NULL ")
	public Iterable<PortalCSODPADetails> findDPAByportalUser_loginIdAndType(String loginId, String type);*/
	
	
}