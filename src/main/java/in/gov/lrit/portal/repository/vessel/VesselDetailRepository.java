package in.gov.lrit.portal.repository.vessel;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.gov.lrit.portal.model.vessel.EditVesselInterface;
import in.gov.lrit.portal.model.vessel.PortalVesselDetail;
import in.gov.lrit.portal.model.vessel.VesselDetailInterface;

@Repository
@Transactional
public interface VesselDetailRepository extends JpaRepository<PortalVesselDetail,Integer> {

	@Query(value="select vessel_id as vesselId,imo_no as imoNo,mmsi_no as mmsiNo,call_sign as callSign,vessel_name as vesselName  from portal_vessel_details where status IN (:status) and registration_status NOT IN(:regstatus) ",nativeQuery = true)
	public List<VesselDetailInterface> checkStatus(List<String> status,List<String> regstatus);

	@Query(value="select vessel_id as vesselId,imo_no as imoNo,mmsi_no as mmsiNo,call_sign as callSign,vessel_name as vesselName  from portal_vessel_details where status IN (:status) and registration_status IN(:regstatus) ",nativeQuery = true)
	public List<VesselDetailInterface> checkStatusflag(List<String> status,List<String> regstatus);

	@Query(value="select vessel_id from portal_vessel_details where imo_no=:imo and status IN (:status) and registration_status IN ('SHIP_REGISTERED','DNID_DOWNLOADED')",nativeQuery = true)
	public int findByImoNoAndStatus(String imo,String status);
    
	public List<VesselDetailInterface> findByStatus(String status);
	
	@Query(value="select vessel_id from portal_vessel_details where vessel_id=:vesselId and status IN (:shipstatus) and registration_status IN (:registrationStatus)",nativeQuery = true)
	public String findByVesselId(int vesselId,List<String>registrationStatus,List<String> shipstatus);
	
	public EditVesselInterface findByVesselId(Integer vesselId) ;
	
	@Query(value="update portal_vessel_details set vessel_name=:vesselName, call_sign=:callSign, reg_status_date=now() where vessel_id=:vesselId", nativeQuery=true)
	@Modifying
	public int updateEditVessel(Integer vesselId, String vesselName,String callSign);

	@Query(value="update portal_vessel_details set registration_status=:status, reg_status_date=now() where vessel_id=:vesselId",nativeQuery=true)
	@Modifying
	public int updateVesselRegistrationStatus(Integer vesselId, String status);

	@Query(value="update portal_vessel_details set status=:status, reg_status_date=now() where vessel_id=:vesselId",nativeQuery=true)
	@Modifying
	public int updateVesselStatus(Integer vesselId, String status);
	
	@Query(value="select count(*) from portal_vessel_details where status='INACTIVE'",  nativeQuery=true)
	public int getInactiveShipCount();
	
	@Query(value="update portal_vessel_details set status=:status, registration_status=:status , reg_status_date=now() where vessel_id=:vesselId",nativeQuery=true)
	@Modifying
	public int updateVesselStatusAndRegistrationStatus(Integer vesselId, String status);


	@Query(value="update portal_vessel_details set time_delay=:timedelay where vessel_id=:vesselId", nativeQuery=true)
	@Modifying
	public int updatetimedelay(BigInteger timedelay, Integer vesselId);
	
	
	@Query(value="select time_delay  from portal_vessel_details where vessel_id=:vesselId ",nativeQuery = true)
	public String gettimedelay(Integer vesselId);
	
	@Query(value="select vesselstatus, count(vesselstatus)"
			+ " from vw_shipstatus_latestposition where login_id=:loginId group by vesselstatus ",nativeQuery = true)
	public List<Object[]>  getVesselStatusCountByLoginIdfromVW(String  loginId);
	
	@Query(value="select vesselstatus, count(vesselstatus)"
			+ " from vw_shipstatus_latestposition where data_user_provider=:lritId group by vesselstatus ",nativeQuery = true)
	public List<Object[]>  getVesselStatusCountByLritIdfromVW(String  lritId);
	
	@Query(value="select vessel_id as vesselId,"
			+ " imo_no as imoNo,"
			+ " mmsi_no as mmsiNo,"
			+ " call_sign as callSign,"
			+ " ship_name as vesselName"
			+ " from vw_shipstatus_latestposition where vesselstatus= :vesselStatus and data_user_provider=:lritId ",nativeQuery = true)
	public List<VesselDetailInterface> getVesselDetailFromVWByVesselStatusAndLritId(String  vesselStatus, String lritId);
	
	@Query(value="select vessel_id as vesselId,"
			+ " imo_no as imoNo,"
			+ " mmsi_no as mmsiNo,"
			+ " call_sign as callSign,"
			+ " ship_name as vesselName"
			+ " from vw_shipstatus_latestposition where vesselstatus= :vesselStatus and login_id=:loginId ",nativeQuery = true)
	public List<VesselDetailInterface> getVesselDetailFromVWByVesselStatusAndLoginId(String  vesselStatus, String loginId);
	
	@Query(value="select frequency_rate, count(frequency_rate)"
			+ " from vw_vesselimo_lritid_aspterminalactivefrequency where login_id=:loginId group by frequency_rate ",nativeQuery = true)
	public List<Object[]>  getFrequencyCountByLoginIdfromVW(String  loginId);
	
	@Query(value="select frequency_rate, count(frequency_rate)"
			+ " from vw_vesselimo_lritid_aspterminalactivefrequency  where contracting_government_lritid=:lritId group by frequency_rate ",nativeQuery = true)
	public List<Object[]>  getFrequencyCountByLritIdfromVW(String  lritId);

	@Query(value="select vessel_id as vesselId,"
			+ " imo_no as imoNo,"
			+ " mmsi_no as mmsiNo,"
			+ " call_sign as callSign,"
			+ " vessel_name as vesselName"
			+ " from vw_vesselimo_lritid_aspterminalactivefrequency "
			+ " where frequency_rate= :frequency "
			+ " and contracting_government_lritid=:lritId ",nativeQuery = true)
	public List<VesselDetailInterface> getVesselDetailFromVWByFrequencyAndLritId(Integer frequency, String lritId);

	@Query(value="select vessel_id as vesselId,"
			+ " imo_no as imoNo,"
			+ " mmsi_no as mmsiNo,"
			+ " call_sign as callSign,"
			+ " vessel_name as vesselName"
			+ " from vw_vesselimo_lritid_aspterminalactivefrequency "
			+ " where frequency_rate= :frequency "
			+ " and login_id=:loginId ",nativeQuery = true)
	public List<VesselDetailInterface> getVesselDetailFromVWByFrequencyAndLoginId(Integer frequency, String loginId);
	
	@Query(value="select count(*) from portal_vessel_details "
			+ " where vessel_id in (select vessel_id from  portal_shippingcompany_vessel_relation "
			+ "	where relation in('0','3','4','6') and user_id=:loginId) "
			+ " and status='INACTIVE'",  nativeQuery=true)
	public int getInactiveShipCountbyLoginId(String loginId);
	
	@Query(value="select count(*) "
			+ " from portal_vessel_details "
			+ " where vessel_id in "
			+ "	( select vessel_id "
			+ "		from  portal_shippingcompany_vessel_relation "
			+ "     where relation in('0','3','4','6')  "
			+ " and user_id in "
			+ " ( select login_id "
			+ "		from portal_users "
			+ "		where requestors_lrit_id=:lritId and category='USER_CATEGORY_SC')) "
			+ " and status='INACTIVE'" ,  nativeQuery=true)
	public int getInactiveShipCountbyLritId(String lritId);

	@Query(value="select vessel_id as vesselId, " 
			+  " imo_no as imoNo,"  
			+  " mmsi_no as mmsiNo,"  
			+  " call_sign as callSign,"  
			+  " vessel_name as vesselName"
			+ " from portal_vessel_details "
			+ " where vessel_id in (select vessel_id from  portal_shippingcompany_vessel_relation "
			+ "	where relation in('0','3','4','6') and user_id=:loginId) "
			+ " and status='INACTIVE'",  nativeQuery=true)
	public List<VesselDetailInterface> getInactiveShipDetailsbyLoginId(String loginId);
	
	@Query(value="select vessel_id as vesselId, " 
			+  " imo_no as imoNo,"  
			+  " mmsi_no as mmsiNo,"  
			+  " call_sign as callSign,"  
			+  " vessel_name as vesselName"
			+ " from portal_vessel_details "
			+ " where vessel_id in "
			+ "	( select vessel_id "
			+ "		from  portal_shippingcompany_vessel_relation "
			+ "     where relation in('0','3','4','6')  "
			+ " and user_id in "
			+ " ( select login_id "
			+ "		from portal_users "
			+ "		where requestors_lrit_id=:lritId and category='USER_CATEGORY_SC')) "
			+ " and status='INACTIVE'" ,  nativeQuery=true)
	public List<VesselDetailInterface> getInactiveShipDetailsbyLritId(String lritId);
	
	@Query(value="SELECT CASE WHEN count(vessel_id) > 0 THEN true ELSE false END "
			+ " FROM portal_vessel_details where "
			+ " vessel_id = :vesselId and "
			+ " registration_status in (:regStatus)", nativeQuery=true)
	public boolean checkNotInStatusInRegistrationStatus(List<String> regStatus, Integer vesselId );
}
