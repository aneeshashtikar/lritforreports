package in.gov.lrit.portal.repository.vessel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import in.gov.lrit.portal.model.vessel.PortalVesselReportingStatusHistory;

@Repository
public interface VesselReportingStatusHistoryRepository extends JpaRepository<PortalVesselReportingStatusHistory, Long>{

	
	@Query(value=" select supporting_docs from portal_vessel_reporting_status_history where pendingtask_id=:pendingTaskId", nativeQuery=true)
	byte[] findsupportingDocumentbypendingTaskId(Integer pendingTaskId);
}
