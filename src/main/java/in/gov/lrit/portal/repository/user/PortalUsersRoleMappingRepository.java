package in.gov.lrit.portal.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/*
import in.gov.lrit.portal.service.user.PortalUsersRoleMapping;*/

import in.gov.lrit.portal.model.user.PortalUsersRoleMapping;



@Repository
public interface PortalUsersRoleMappingRepository  extends JpaRepository<PortalUsersRoleMapping, String > {
	 @Transactional
	 @Modifying
	@Query(value="Delete from portal_role_user_mapping mapping where mapping.login_id = :loginId",nativeQuery=true)
	public void deleteByLoginId(String loginId);
	
	
	/*
	 * @Transactional
	 @Modifying
	@Query(value="Delete from portal_role_activities_mapping mapping where mapping.role_id = :roleId",nativeQuery=true)
	public void deleteRoleActivityMapping(String roleId);
	 */
}
