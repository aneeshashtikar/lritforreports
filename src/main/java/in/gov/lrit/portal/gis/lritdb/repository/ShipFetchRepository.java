/**
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.portal.gis.lritdb.repository;


import java.math.BigInteger;
import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vividsolutions.jts.geom.Geometry;

import in.gov.lrit.portal.gis.lritdb.model.ShipFetchModel;
import in.gov.lrit.portal.gis.lritdb.model.shipDetailsForDisplay;
import in.gov.lrit.portal.gis.lritdb.model.shipHistoryDetails;

@Repository
public interface ShipFetchRepository extends CrudRepository<ShipFetchModel, Long> {

		
	//Ship Details for first time load 

	//@Query(value = "Select * from vw_lrit_latest_ship_position "  // ,course
	//		+ "where (data_user_provider = ?1 OR data_user_requestor = ?1 ); ", nativeQuery = true) 
	
	/*@Query(value = "Select a.*, portal_vessel_details.registration_status from ((SELECT * from vw_shipstatus_latestposition where data_user_provider in ?1 OR "
			+ "data_user_requestor in ?1 ) as a LEFT JOIN portal_vessel_details ON a.imo_no = portal_vessel_details.imo_no) "
			+ "where portal_vessel_details.registration_status in ('SHIP_REGISTERED', 'DNID_DW_REQ', 'DNID_DEL_REQ')  or "
			+ "portal_vessel_details.registration_status IS NULL ;", nativeQuery = true)*/
	
	 @Query(value = "Select DISTINCT aaa.imo_no, aaa.message_id, aaa.reference_id, aaa.latitude, aaa.longitude, aaa.ship_borne_equipment_id, aaa.asp_id, aaa.mmsi_no, aaa.dc_id, " +
	" aaa.timestamp4, aaa.ship_name, aaa.data_user_provider, aaa.course, aaa.speed, aaa.vesselstatus, portal_vessel_details.registration_status  " +
	" from (Select vw_shipstatus_latestposition.* from vw_shipstatus_latestposition left join " +
	" (Select imo_no, max(timestamp4) as timestamp4 from vw_shipstatus_latestposition where data_user_provider in ?1 OR " +
	" data_user_requestor in ?1 group by imo_no)a on a.imo_no =  vw_shipstatus_latestposition.imo_no and  a.timestamp4 = vw_shipstatus_latestposition.timestamp4 ) aaa " +
	" LEFT JOIN portal_vessel_details ON aaa.imo_no = portal_vessel_details.imo_no where portal_vessel_details.registration_status in ('SHIP_REGISTERED', 'DNID_DW_REQ', 'DNID_DEL_REQ') " +  
	" or portal_vessel_details.registration_status IS NULL And aaa.imo_no  IS NOT NULL;", nativeQuery = true)	
	 List<shipDetailsForDisplay> findAll(List<String> cg_lritid);
	 
	/*@Query(value = "Select DISTINCT aaa.imo_no, aaa.message_id, aaa.reference_id, aaa.latitude, aaa.longitude, aaa.ship_borne_equipment_id, aaa.asp_id, aaa.mmsi_no, aaa.dc_id, " +
	 " aaa.timestamp4, aaa.ship_name, aaa.data_user_provider, aaa.course, aaa.speed, aaa.vesselstatus  " +
	 " from (Select vw_shipstatus_latestposition.* from vw_shipstatus_latestposition left join " +
	 " (Select imo_no, max(timestamp4) as timestamp4 from vw_shipstatus_latestposition where data_user_provider in ('1065','1136') OR " +
	 " data_user_requestor in ('1065','1136') group by imo_no)a on a.imo_no =  vw_shipstatus_latestposition.imo_no and  a.timestamp4 = vw_shipstatus_latestposition.timestamp4 ) aaa " +
	 " where aaa.imo_no  IS NOT NULL", nativeQuery = true)*/
		
	
		//List<ShipFetchModel> findAll();
	//Active ships means: portal_vessel_details table's column registration_status hs value 'SHIP_REGISTERED', 'DNID_DW_REQ', 'DNID_DEL_REQ' and for non flag ships its registration_status IS NULL 
	//Select a.* from
	//((SELECT * from vw_shipstatus_latestposition where data_user_provider = '1065' OR data_user_requestor = '1065') as a
	//LEFT JOIN portal_vessel_details ON a.imo_no = portal_vessel_details.imo_no) where portal_vessel_details.registration_status in ('SHIP_REGISTERED', 'DNID_DW_REQ', 'DNID_DEL_REQ')  or portal_vessel_details.registration_status IS NULL ;
	
		// Search History of Ship in provided start and end date 
		@Query(value = "SELECT * FROM dc_position_report where imo_no = :imoNo and timestamp4 BETWEEN :startDate AND :endDate ORDER BY timestamp4", nativeQuery=true)
		List<shipHistoryDetails> findbyimoNo(@Param("imoNo") String imoNo, @Param("startDate") java.sql.Timestamp startDate, @Param("endDate") java.sql.Timestamp endDate);
		
		// Search Ship by ship name
		/*@Query(value = "Select DISTINCT a.imo_no, a.message_id, a.reference_id, a.latitude, a.longitude, a.ship_borne_equipment_id, a.asp_id, a.mmsi_no, a.dc_id, \r\n" + 
				"	 a.timestamp4, a.ship_name, a.data_user_provider, a.course, a.speed, a.vesselstatus, portal_vessel_details.registration_status  from vw_shipstatus_latestposition as a, portal_vessel_details " 
				+ "where a.imo_no = portal_vessel_details.imo_no AND  UPPER(a.ship_name) like UPPER(CONCAT('%',:searchVal,'%')) and (data_user_provider = :cg_lritid OR data_user_requestor=:cg_lritid); ", nativeQuery = true)*/
		@Query(value = "select ss.imo_no, ss.message_id, ss.reference_id, ss.latitude, ss.longitude, ss.ship_borne_equipment_id, ss.asp_id, ss.mmsi_no, ss.dc_id, \r\n" + 
				" ss.timestamp4, ss.ship_name, ss.data_user_provider, ss.course, ss.speed, ss.registration_status,ss.vesselstatus, portal_ship_equipement.dnid_no, portal_ship_equipement.member_no from portal_ship_equipement right join(Select vw_shipstatus_latestposition.*, aa.registration_status \r\n " +
				" from vw_shipstatus_latestposition left join ((select portal_vessel_details.imo_no,portal_vessel_details.registration_status, portal_vessel_details.reg_date  from portal_vessel_details right join \r\n" + 
				" (select imo_no, max(reg_date) reg_date from portal_vessel_details group by (imo_no)order by imo_no) a on \r\n" + 
				" portal_vessel_details.imo_no = a.imo_no and portal_vessel_details.reg_date = a.reg_date order by imo_no)) aa \r\n" + 
				" on vw_shipstatus_latestposition.imo_no = aa.imo_no where (UPPER(vw_shipstatus_latestposition.ship_name) like  UPPER(CONCAT('%',:searchVal,'%'))) and (data_user_provider in :cg_lritid OR data_user_requestor in :cg_lritid) ) ss \r\n" + 
				" on portal_ship_equipement.imo_no = ss.imo_no; ", nativeQuery = true)
		List<ShipFetchModel> findShipDetailsOnShipName( @Param("searchVal") String searchVal, @Param("cg_lritid") List<String> cg_lritid);

		// Search Ship by imo no
		/*@Query(value = "Select DISTINCT a.imo_no, a.message_id, a.reference_id, a.latitude, a.longitude, a.ship_borne_equipment_id, a.asp_id, a.mmsi_no, a.dc_id, \r\n" + 
				"	 a.timestamp4, a.ship_name, a.data_user_provider, a.course, a.speed, a.vesselstatus, portal_vessel_details.registration_status  from vw_shipstatus_latestposition as a, portal_vessel_details " 
				+ "where a.imo_no = portal_vessel_details.imo_no AND  (a.imo_no like CONCAT(?1,'%')) and (data_user_provider = ?2 OR data_user_requestor=?2 ); ", nativeQuery = true)*/
		
		
		@Query(value = "select ss.imo_no, ss.message_id, ss.reference_id, ss.latitude, ss.longitude, ss.ship_borne_equipment_id, ss.asp_id, ss.mmsi_no, ss.dc_id, \r\n" + 
				" ss.timestamp4, ss.ship_name, ss.data_user_provider, ss.course, ss.speed, ss.registration_status, ss.vesselstatus, portal_ship_equipement.dnid_no, portal_ship_equipement.member_no from portal_ship_equipement right join(Select vw_shipstatus_latestposition.*, aa.registration_status \r\n " +
				" from vw_shipstatus_latestposition left join ((select portal_vessel_details.imo_no,portal_vessel_details.registration_status, portal_vessel_details.reg_date  from portal_vessel_details right join \r\n" + 
				" (select imo_no, max(reg_date) reg_date from portal_vessel_details group by (imo_no)order by imo_no) a on \r\n" + 
				" portal_vessel_details.imo_no = a.imo_no and portal_vessel_details.reg_date = a.reg_date order by imo_no)) aa \r\n" + 
				" on vw_shipstatus_latestposition.imo_no = aa.imo_no where (vw_shipstatus_latestposition.imo_no like CONCAT(?1,'%')) and (data_user_provider in ?2 OR data_user_requestor in ?2) ) ss \r\n" + 
				" on portal_ship_equipement.imo_no = ss.imo_no; ", nativeQuery = true)
		List<ShipFetchModel> findShipDetailsOnIMONo(String searchVal, List<String> cg_lritid);

		// Search Ship by mmsi no.
		/*@Query(value = "Select DISTINCT a.imo_no, a.message_id, a.reference_id, a.latitude, a.longitude, a.ship_borne_equipment_id, a.asp_id, a.mmsi_no, a.dc_id, \r\n" + 
				"	 a.timestamp4, a.ship_name, a.data_user_provider, a.course, a.speed, a.vesselstatus, portal_vessel_details.registration_status  from vw_shipstatus_latestposition as a, portal_vessel_details " 
				+ "where a.imo_no = portal_vessel_details.imo_no AND  UPPER(a.mmsi_no) like UPPER(CONCAT(?1,'%')) and (data_user_provider = ?2 OR data_user_requestor=?2 ); ", nativeQuery = true)*/
		@Query(value = "select ss.imo_no, ss.message_id, ss.reference_id, ss.latitude, ss.longitude, ss.ship_borne_equipment_id, ss.asp_id, ss.mmsi_no, ss.dc_id, \r\n" + 
				" ss.timestamp4, ss.ship_name, ss.data_user_provider, ss.course, ss.speed, ss.registration_status, ss.vesselstatus, portal_ship_equipement.dnid_no, portal_ship_equipement.member_no from portal_ship_equipement right join(Select vw_shipstatus_latestposition.*, aa.registration_status \r\n " +
				" from vw_shipstatus_latestposition left join ((select portal_vessel_details.imo_no, portal_vessel_details.registration_status, portal_vessel_details.reg_date  from portal_vessel_details right join \r\n" + 
				" (select imo_no, max(reg_date) reg_date from portal_vessel_details group by (imo_no)order by imo_no) a on \r\n" + 
				" portal_vessel_details.imo_no = a.imo_no and portal_vessel_details.reg_date = a.reg_date order by imo_no)) aa \r\n" + 
				" on vw_shipstatus_latestposition.imo_no = aa.imo_no where (UPPER(vw_shipstatus_latestposition.mmsi_no) like  UPPER(CONCAT(?1,'%'))) and (data_user_provider in ?2 OR data_user_requestor in ?2) ) ss \r\n" + 
				" on portal_ship_equipement.imo_no = ss.imo_no; ", nativeQuery = true)
		List<ShipFetchModel> findShipDetailsOnMMSINo(String searchVal, List<String> cg_lritid);
		
		 // Search Ship by SEID no.
		@Query(value = "select ss.imo_no, ss.message_id, ss.reference_id, ss.latitude, ss.longitude, ss.ship_borne_equipment_id, ss.asp_id, ss.mmsi_no, ss.dc_id, \r\n" + 
				" ss.timestamp4, ss.ship_name, ss.data_user_provider, ss.course, ss.speed, ss.registration_status, ss.vesselstatus, portal_ship_equipement.dnid_no, portal_ship_equipement.member_no from portal_ship_equipement right join(Select vw_shipstatus_latestposition.*, aa.registration_status \r\n " +
				" from vw_shipstatus_latestposition left join ((select portal_vessel_details.imo_no,portal_vessel_details.registration_status, portal_vessel_details.reg_date  from portal_vessel_details right join \r\n" + 
				" (select imo_no, max(reg_date) reg_date from portal_vessel_details group by (imo_no)order by imo_no) a on \r\n" + 
				" portal_vessel_details.imo_no = a.imo_no and portal_vessel_details.reg_date = a.reg_date order by imo_no)) aa \r\n" + 
				" on vw_shipstatus_latestposition.imo_no = aa.imo_no) ss \r\n" + 
				" on portal_ship_equipement.imo_no = ss.imo_no  where (UPPER(ss.ship_borne_equipment_id) like  UPPER(CONCAT(?1,'%'))) and (data_user_provider in ?2 OR data_user_requestor in ?2); ", nativeQuery = true)
			List<ShipFetchModel> findShipDetailsOnSEIDNo(String searchVal, List<String> cg_lritid);
			
			// Search Ship by DNID no.
		@Query(value = "select ss.imo_no, ss.message_id, ss.reference_id, ss.latitude, ss.longitude, ss.ship_borne_equipment_id, ss.asp_id, ss.mmsi_no, ss.dc_id, \r\n" + 
				" ss.timestamp4, ss.ship_name, ss.data_user_provider, ss.course, ss.speed, ss.registration_status, ss.vesselstatus, portal_ship_equipement.dnid_no, portal_ship_equipement.member_no from portal_ship_equipement right join(Select vw_shipstatus_latestposition.*, aa.registration_status \r\n " +
				" from vw_shipstatus_latestposition left join ((select portal_vessel_details.imo_no,portal_vessel_details.registration_status, portal_vessel_details.reg_date  from portal_vessel_details right join \r\n" + 
				" (select imo_no, max(reg_date) reg_date from portal_vessel_details group by (imo_no)order by imo_no) a on \r\n" + 
				" portal_vessel_details.imo_no = a.imo_no and portal_vessel_details.reg_date = a.reg_date order by imo_no)) aa \r\n" + 
				" on vw_shipstatus_latestposition.imo_no = aa.imo_no ) ss \r\n" + 
				" on portal_ship_equipement.imo_no = ss.imo_no where (cast(portal_ship_equipement.dnid_no as text) like CONCAT(?1,'%')) and (data_user_provider in ?2 OR data_user_requestor in ?2); ", nativeQuery = true)
			List<ShipFetchModel> findShipDetailsOnDnidNo(String searchVal, List<String> cg_lritid); 
	
		// Search Ship by messageId
		@Query(value = "SELECT * FROM dc_position_report where reference_id in :reference_id", nativeQuery=true)
		List<shipHistoryDetails> findShipByMessageId(@Param("reference_id") List<String> reference_id);
		
		// search ship by company imo list
		@Query(value = "SELECT a.*, portal_vessel_details.registration_status from vw_shipstatus_latestposition as a, portal_vessel_details where a.imo_no in :imo_no_list", nativeQuery=true)
		List<ShipFetchModel> findShipByImoforComaony(@Param("imo_no_list") List<String> imo_no_list);
		
		@Query(value = " SELECT  Distinct a.imo_no\r\n" + 
				"FROM (Select imo_no, ST_MakePoint(longitude, latitude) from dc_position_report where timestamp1 between ?2 And ?3)a\r\n" + 
				"WHERE ST_Contains( (ST_SetSRID(?1,4326)), (ST_SetSRID(a.st_makepoint,4326)))\r\n" + 
				"", nativeQuery=true)
		List<String> findShipByPolygon(@Param("polygon") Geometry polygon,  @Param("startDate") java.sql.Timestamp startDate, @Param("endDate") java.sql.Timestamp endDate );
		
		
		@Query(value = " SELECT  Distinct a.imo_no\r\n" + 
				"FROM (Select imo_no, ST_MakePoint(longitude, latitude) from dc_position_report where timestamp1 between ?2 And ?3) a\r\n" + 
				"WHERE ST_Contains((SELECT ST_MakePolygon(ST_GeomFromText(?1))), a.st_makepoint);", nativeQuery=true)
		List<String> findShipByCustomPolygon(@Param("polygon") String polygon,  @Param("startDate") java.sql.Timestamp startDate, @Param("endDate") java.sql.Timestamp endDate );
}
