package in.gov.lrit.portal.gis.lritdb.model;

public interface CustomPolygonList {

	String getPolygonId();
	String getPolygon_name();
}
