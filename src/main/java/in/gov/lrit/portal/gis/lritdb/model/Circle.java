/**
 * @(#)Circle.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */
package in.gov.lrit.portal.gis.lritdb.model;


public class Circle extends PolygonShape {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4562814260017542771L;
	public Circle() {
		super("circle");
		// TODO Auto-generated constructor stub
	}
	
	private double lat;
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	private double lon; //long
	private double radius;
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	/*
	public Circle(CustomPolygon custompolygonObj) {
		// TODO Auto-generated constructor stub
		super("circle");
		userId = custompolygonObj.getCreated_by();
		polygonId = custompolygonObj.getPolygonId();
		polygonName = custompolygonObj.getPolygon_name();
		polygonId = custompolygonObj.getCreated_by();		
		String[] points=custompolygonObj.getPolygon_points().split(",");
		try {
			lat= Double.parseDouble(points[0]);
			lon= Double.parseDouble(points[1]);
			radius = Double.parseDouble(points[2]);
		} catch (NumberFormatException e) {
		    
		}
	}*/
	
}
