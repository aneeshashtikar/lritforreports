package in.gov.lrit.portal.gis.lritdb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.gov.lrit.portal.gis.lritdb.model.ShipFrequencyModel;

public interface ShipFrequencyRepository extends CrudRepository<ShipFrequencyModel,Long> {
	
	@Query(value = "select now() as currentTimeStamp, imo_no, max(frequency_rate) as request_type\r\n" + 
			" from (select imo_no, CASE\r\n" + 
			"            WHEN request_type = 2 THEN 15\r\n" + 
			"            WHEN request_type = 3 THEN 30\r\n" + 
			"            WHEN request_type = 4 THEN 60\r\n" + 
			"            WHEN request_type = 5 THEN 180\r\n" + 
			"            WHEN request_type = 6 THEN 360\r\n" + 
			"            WHEN request_type = 10 THEN 720\r\n" + 
			"            WHEN request_type = 11 THEN 1440\r\n" + 
			"            ELSE 360\r\n" + 
			"            end as frequency_rate\r\n" + 
			"            from\r\n" + 
			" (select imo_no, request_type  from \r\n" + 
			" (select dc_position_request.imo_no, dc_position_request.request_type  from dc_position_request right join\r\n" + 
			" (select imo_no, max(req_duration_start) as req_duration_start from dc_position_request where request_status = 'active' and (req_duration_start is not null and req_duration_stop is not null ) \r\n" + 
			" and (req_duration_start < now() and req_duration_stop >= now()) group by imo_no) as a\r\n" + 
			" on dc_position_request.imo_no = a.imo_no and a.req_duration_start =  dc_position_request.req_duration_start where dc_position_request.request_status = 'active' and req_duration_stop >= now()) as aaa\r\n" + 
			" union\r\n" + 
			" (select tt.imo_no, tt.request_type as request_type from \r\n" + 
			" (select dc_position_request.imo_no, dc_position_request.request_type from dc_position_request right join \r\n" + 
			" (select imo_no, max(position_request_timestamp) as time from dc_position_request where request_status = 'active' and ((req_duration_start is null and req_duration_stop is null)or (req_duration_stop is null)) and imo_no not in \r\n" + 
			" (select aaa.imo_no from (select imo_no, max(req_duration_start) as request_type from dc_position_request where request_status = 'active' and (req_duration_start is not null and req_duration_stop is not null) and \r\n" + 
			" (req_duration_start < now() and req_duration_stop >= now()) group by imo_no)aaa)group by imo_no) as ss on dc_position_request.imo_no = ss.imo_no and dc_position_request.position_request_timestamp = ss.time and \r\n" + 
			" dc_position_request.request_status = 'active' and ((dc_position_request.req_duration_start is null and dc_position_request.req_duration_stop is null) or (dc_position_request.req_duration_stop is null))) as tt)\r\n" + 
			" order by imo_no) as sss) as ssss group by imo_no order by imo_no;",  nativeQuery = true)
	List<ShipFrequencyModel> findFreuencyOfShips();

}
