package in.gov.lrit.portal.gis.lritdb.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "portal_geographical_area")
@NamedQuery(name="GeographicAreaModel.findAll", query="SELECT p FROM GeographicAreaModel p")
public class GeographicAreaModel implements Serializable {
	
	@Override
	public String toString() {
		return "GeographicAreaModel [gmlId=" + gmlId + ", area_type=" + area_type + ", creation_date=" + creation_date
				+ ", login_id=" + login_id + ", rejection_date=" + rejection_date + ",  status=" + status + ", areaCodes=" + areaCodes + "]";
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "gml_id",columnDefinition = "serial")
	private int gmlId;
	
	public int getGmlId() {
		return gmlId;
	}
	public void setGmlId(int gmlId) {
		this.gmlId = gmlId;
	}
	@Column(name = "area_type")
	private String area_type;
	@Column(name = "creation_date")
	private Date creation_date;
	@Column(name = "login_id")
	private String login_id;
	@Column(name = "rejection_date")
	private Date rejection_date;	
	@Column(name = "uploaded_file")
	private byte[] uploaded_file;
	@Column(name = "status")
	private String status;
	
	
	public String getArea_type() {
		return area_type;
	}
	public void setArea_type(String area_type) {
		this.area_type = area_type;
	}
	public Date getCreation_date() {
		return creation_date;
	}
	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public Date getRejection_date() {
		return rejection_date;
	}
	public void setRejection_date(Date rejection_date) {
		this.rejection_date = rejection_date;
	}
	
	public byte[] getUploaded_file() {
		return uploaded_file;
	}
	public void setUploaded_file(byte[] uploaded_file) {
		this.uploaded_file = uploaded_file;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	 @OneToMany(mappedBy = "gmlId", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	    List<GMLAreaCodeMappingModel> areaCodes;

	
	
	public List<GMLAreaCodeMappingModel> getAreaCodes() {
		return areaCodes;
	}
	public void setAreaCodes(List<GMLAreaCodeMappingModel> areaCodes) {
		this.areaCodes = areaCodes;
	}
	public GeographicAreaModel() {
		super();
	}
	
	
}
