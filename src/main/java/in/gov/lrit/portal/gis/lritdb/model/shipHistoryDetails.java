package in.gov.lrit.portal.gis.lritdb.model;

import java.util.Date;

public interface shipHistoryDetails {

	
	String getMessage_id();
	String getReference_id();
	float getLatitude();
	float getLongitude();
	String getShip_borne_equipment_id();
	String getAsp_id();
	String getImo_no();
	String getMmsi_no();
	String getDc_id();
	Date getTimestamp4();
	String getData_user_requestor();
	String getShip_name();
	String getData_user_provider();
	String getDdpversion_no();
	
	 void setData_user_provider(String str);
}
