package in.gov.lrit.portal.gis.lritdb.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaModel;
import in.gov.lrit.portal.gis.lritdb.model.GeographicalAreaInterface;



@Repository
@Transactional
public interface GeographicAreaRepository  extends JpaRepository<GeographicAreaModel,String>{
	List<GeographicAreaModel> findAll();
	
	GeographicAreaModel findByGmlId(int id);
	
	@Query(value = "SELECT * FROM portal_geographical_area where status ='pending_for_ide_approval'; ", nativeQuery = true)
	List<GeographicAreaModel> getGeaographicAreaList();

	@Query(value = "SELECT * FROM portal_geographical_area where status ='pending_for_ide_approval' and login_id = ?1 ; ", nativeQuery = true)
	List<GeographicAreaModel> getGeaographicAreaListPerUser(String loginid);
	
//	@Query(value="SELECT max(cast(substring(area_id,10) as integer)) FROM lrit_test_db.lrit_polygons where area_id like 'GACA1065%';", nativeQuery = true)
	/*
	 * @Query(
	 * value="SELECT max(cast(substring(area_id,10) as integer)) FROM lrit_polygons where area_id like ?1% ;"
	 * , nativeQuery = true) int getNextAreacodeFromDDP(String watertype);
	 */
	
	@Query(value="SELECT max(cast(substring(portal_gmlid_areacode_mapping.area_code,10) as integer))" + 
			"	FROM portal_gmlid_areacode_mapping inner join portal_geographical_area on portal_gmlid_areacode_mapping.gml_id = portal_geographical_area.gml_id  where area_code like ?1% and portal_geographical_area.status IN ('pending_for_ide_approval','sent_to_ide','approved_from_ide','sent_to_ddp');", nativeQuery = true)

	int getNextAreacodeFromDraft(String watertype);

	
	//@Query(value="select * from portal_geographical_area  where status = :geoStatus", nativeQuery = true)
	//@Query(value="select GeographicalAreaInterface from GeographicAreaModel t  where t.status = :geoStatus")
	//public List<GeographicalAreaInterface> findByArea_type( String geoStatus);
	
	
	public List<GeographicAreaModel> findByStatus(String status);
	
	/*
	 * @Modifying
	 * 
	 * @Query("update GeographicAreaModel t set t.status = 'Deleted' where t.gmlId = :gmlId"
	 * ) int deleteGeoArea(int gmlId);
	 */
	
	@Modifying
	@Query("update GeographicAreaModel t set t.status = :status where t.gmlId = :gmlId")
	int updateGeoAreaStatus(int gmlId, String status);
	
	
	
	
}
