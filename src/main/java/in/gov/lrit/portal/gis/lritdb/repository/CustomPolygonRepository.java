/**
 * @(#)CustomPolygonRepository.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */
package in.gov.lrit.portal.gis.lritdb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.gis.lritdb.model.CustomPolygon;
import in.gov.lrit.portal.gis.lritdb.model.CustomPolygonList;

@Repository
public interface CustomPolygonRepository extends CrudRepository<CustomPolygon,String>{
	List<CustomPolygon> findAll();
	
	@Query(value = "SELECT * FROM portal_custom_polygon where upper(polygon_name)=?1 ; ", nativeQuery = true)
	List<CustomPolygon> findBypolygonname(String name);

	List<CustomPolygon> findByPolygonIdIn(List<String> id);
	
	@Query(value = "SELECT polygon_id as polygonId, polygon_name FROM portal_custom_polygon ", nativeQuery = true)
	List<CustomPolygonList> findAreaNameList();
	
}
