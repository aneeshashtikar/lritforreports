/**
 * @(#)ShipFetchModel.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.portal.gis.lritdb.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name = "lrit_latest_ship_position_flag")
public class ShipFetchModel {
	

	   
	    private String message_id;   
	    private String reference_id;
		private float latitude;
		private float longitude;
		private String ship_borne_equipment_id;
		private String asp_id;
		@Id
		private String imo_no;
		private String mmsi_no;
		private String dc_id;
		private Date timestamp4;		
		//private String data_user_requestor;
		private String ship_name;		
		private String data_user_provider;
		//private String ddpversion_no;	
		private String course;
		private String speed;
		private String vesselstatus;
		private String registration_status;
		
		private String dnid_no;
		private String member_no;
		
		public ShipFetchModel() {			
		}
		
		
		
		


		public String getVesselstatus() {
			return vesselstatus;
		}


		public void setVesselstatus(String vesselstatus) {
			this.vesselstatus = vesselstatus;
		}






		public String getDnid_no() {
			return dnid_no;
		}

		public void setDnid_no(String dnid_no) {
			this.dnid_no = dnid_no;
		}

		public String getMember_no() {
			return member_no;
		}

		public void setMember_no(String member_no) {
			this.member_no = member_no;
		}

		public String getRegistration_status() {
			return registration_status;
		}

		public void setRegistration_status(String registration_status) {
			this.registration_status = registration_status;
		}

		/*public String getVesselstatus() {
			return vesselstatus;
		}

		public void setVesselstatus(String vesselstatus) {
			this.vesselstatus = vesselstatus;
		}
*/
		public String getSpeed() {
			return speed;
		}

		public void setSpeed(String speed) {
			this.speed = speed;
		}		
				
		public String getMessage_id() {
			return message_id;
		}
		public void setMessage_id(String message_id) {
			this.message_id = message_id;
		}
		public float getLatitude() {
			return latitude;
		}
		public void setLatitude(float latitude) {
			this.latitude = latitude;
		}
		public float getLongitude() {
			return longitude;
		}
		public void setLongitude(float longitude) {
			this.longitude = longitude;
		}
		public String getShip_borne_equipment_id() {
			return ship_borne_equipment_id;
		}
		public void setShip_borne_equipment_id(String ship_borne_equipment_id) {
			this.ship_borne_equipment_id = ship_borne_equipment_id;
		}
		public String getAsp_id() {
			return asp_id;
		}
		public void setAsp_id(String asp_id) {
			this.asp_id = asp_id;
		}
		public String getImo_no() {
			return imo_no;
		}
		public void setImo_no(String imo_no) {
			this.imo_no = imo_no;
		}
		public String getMmsi_no() {
			return mmsi_no;
		}
		public void setMmsi_no(String mmsi_no) {
			this.mmsi_no = mmsi_no;
		}
		public String getDc_id() {
			return dc_id;
		}
		public void setDc_id(String dc_id) {
			this.dc_id = dc_id;
		}
		public Date getTimestamp4() {
			return timestamp4;
		}
		public void setTimestamp4(Date timestamp4) {
			this.timestamp4 = timestamp4;
		}
		
		public String getShip_name() {
			return ship_name;
		}
		public void setShip_name(String ship_name) {
			this.ship_name = ship_name;
		}
		
		/*public String getDdpversion_no() {
			return ddpversion_no;
		}
		public void setDdpversion_no(String ddpversion_no) {
			this.ddpversion_no = ddpversion_no;
		}*/

		public String getCourse() {
			return course;
		}

		public void setCourse(String course) {
			this.course = course;
		}

		public String getReference_id() {
			return reference_id;
		}

		public void setReference_id(String reference_id) {
			this.reference_id = reference_id;
		}

		/*public String getData_user_requestor() {
			return data_user_requestor;
		}

		public void setData_user_requestor(String data_user_requestor) {
			this.data_user_requestor = data_user_requestor;
		}*/

		public String getData_user_provider() {
			return data_user_provider;
		}

		public void setData_user_provider(String data_user_provider) {
			this.data_user_provider = data_user_provider;
		}

		
	
		
}
