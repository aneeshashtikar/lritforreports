/**
 * @(#)Rectangle.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */
package in.gov.lrit.portal.gis.lritdb.model;


public class Rectangle extends PolygonShape {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public double getLatOffset() {
		return latOffset;
	}
	public void setLatOffset(double latOffset) {
		this.latOffset = latOffset;
	}
	public double getLongOffset() {
		return longOffset;
	}
	public void setLongOffset(double longOffset) {
		this.longOffset = longOffset;
	}
	public Rectangle() {
		super("rectangle");
		// TODO Auto-generated constructor stub
	}
	/*public Rectangle(CustomPolygon custompolygonObj) {
		// TODO Auto-generated constructor stub
		super("rectangle");
		userId = custompolygonObj.getCreated_by();
		polygonId = custompolygonObj.getPolygonId();
		polygonName = custompolygonObj.getPolygon_name();
		polygonId = custompolygonObj.getCreated_by();		
		String[] points=custompolygonObj.getPolygon_points().split(",");
		try {
			lat= Double.parseDouble(points[0]);
			lon= Double.parseDouble(points[1]);
			latOffset = Double.parseDouble(points[2]);
			longOffset=Double.parseDouble(points[3]);	
		} catch (NumberFormatException e) {
		    
		}
	}*/
	private double lat;
	private double lon; //long 
	private double latOffset;
	private double longOffset;
	
}
