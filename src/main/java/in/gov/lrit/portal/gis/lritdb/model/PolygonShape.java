/**
 * @(#)PolygonShape.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */
package in.gov.lrit.portal.gis.lritdb.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "polygontype")
@JsonSubTypes(value = {
    @JsonSubTypes.Type(name = "rectangle", value = Rectangle.class),
    @JsonSubTypes.Type(name = "polygon", value = Freedraw.class),
    @JsonSubTypes.Type(name = "circle", value = Circle.class)
})

public abstract class PolygonShape implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String polygontype;
	String polygonName;
	String polygonId;
	String createdBy;
	public String getPolygonName() {
		return polygonName;
	}

	public void setPolygonName(String polygonName) {
		this.polygonName = polygonName;
	}

	public String getPolygonId() {
		return polygonId;
	}

	public void setPolygonId(String polygonId) {
		this.polygonId = polygonId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	String userId;
	
	PolygonShape(String polygontype){
		this.polygontype=polygontype;
	}	

//	
//	public Object getMaapData(String type) {
//		return this;
//	}
//	
//	public void save(Object object,String role,String type ) {
//		
//	}
	public String getPolygontype() {
		return polygontype;
	}
//
	public void setPolygontype(String polygontype) {
		this.polygontype = polygontype;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
	/*
	public static PolygonShape getInstance(CustomPolygon custompolygonObj) {
		if (custompolygonObj.getPolygon_type().equals("polygon")) {
			return new Freedraw(custompolygonObj);
		} else if (custompolygonObj.getPolygon_type().equals("rectangle")) {
			return new Rectangle(custompolygonObj);
		} else if (custompolygonObj.getPolygon_type().equals("circle")) {
			return new Circle(custompolygonObj);
		}
		return null;
	} */

}
