package in.gov.lrit.portal.gis.lritdb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "portal_shipping_company")
public class ShipCompanyFetchModel {
	
	@Id
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "company_code")
	private String companyCode;

	//@Column(name = "login_id")
	//private String loginId;
	
	
	public ShipCompanyFetchModel() {		
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	/*
	 * public String getCurrentCsoId() { return loginId; }
	 * 
	 * public void setCurrentCsoId(String currentCsoId) { this.loginId =
	 * currentCsoId; }
	 */

	@Override
	public String toString() {
		return "ShipCompanyFetchModel [companyName=" + companyName + ", companyCode=" + companyCode + "]";
	}

	
	
}
