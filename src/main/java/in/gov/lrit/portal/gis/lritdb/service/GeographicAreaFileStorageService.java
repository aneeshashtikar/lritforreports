/**
 * @(#)GeographicAreaFileStorageService.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.portal.gis.lritdb.service;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;
import in.gov.lrit.gis.ddp.repository.SOFetchRepository;
import in.gov.lrit.portal.gis.lritdb.model.CustomPolygon;
import in.gov.lrit.portal.gis.lritdb.model.CustomPolygonList;
import in.gov.lrit.portal.gis.lritdb.model.GMLAreaCodeMappingModel;
import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaData;
import in.gov.lrit.portal.gis.lritdb.model.GeographicAreaModel;
import in.gov.lrit.portal.gis.lritdb.repository.CustomPolygonRepository;
import in.gov.lrit.portal.gis.lritdb.repository.GeographicAreaRepository;



@Service
public class GeographicAreaFileStorageService {
	
	@Autowired
	GeographicAreaRepository geographicAreaRepository;
	
	@Autowired
	SOFetchRepository SOFetchRepositoryObj;	
	
	@Autowired
	CustomPolygonRepository customPolygonRepository;
	
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(GeographicAreaFileStorageService.class);
	 
	public GeographicAreaModel storeFile(GeographicAreaData file,String loginId) {
    	String status ="pending_for_ide_approval";
 		GeographicAreaModel geographicAreaObj = new GeographicAreaModel();
 		if(file.getGmlId()!=0) geographicAreaObj.setGmlId(file.getGmlId());		 			 		
 		geographicAreaObj.setArea_type(file.getAreaType());		 		
 		geographicAreaObj.setCreation_date(new Date());
 		geographicAreaObj.setUploaded_file(zipBytes("test.gml",file.getData().getBytes()));
 		geographicAreaObj.setStatus(status);
 		geographicAreaObj.setLogin_id(loginId);
 		geographicAreaObj = geographicAreaRepository.save(geographicAreaObj);
 		
 		List<GMLAreaCodeMappingModel> areaIdMapList =new ArrayList<>();
 		for (GMLAreaCodeMappingModel areaCodeItem : file.getPolygonAreas()) {
 			GMLAreaCodeMappingModel polygonareaTemp = new GMLAreaCodeMappingModel(areaCodeItem,geographicAreaObj); 	
 			
 			areaIdMapList.add(polygonareaTemp);
		}	
 		
 		geographicAreaObj.setAreaCodes(areaIdMapList);
 		
 		
        return geographicAreaRepository.save(geographicAreaObj); 
 
} 
	
	 public GeographicAreaModel getFile(int fileId) {
	        return geographicAreaRepository.findByGmlId(fileId);
	    }
	 
	 
	 public GeographicAreaData getGeographicArea(int fileId) {
		 GeographicAreaData geographicAreaData = new GeographicAreaData();
		 GeographicAreaModel geographicAreaObj = geographicAreaRepository.findByGmlId(fileId);
		 geographicAreaData.setAreaType(geographicAreaObj.getArea_type());
		 geographicAreaData.setData( new String(geographicAreaObj.getUploaded_file()));
		 return geographicAreaData;
	 } 
	
	 
	 
	 
	 public List<GeographicAreaModel> getGeographicAreaList() {			
		 return geographicAreaRepository.getGeaographicAreaList();
	 }
	 
	 public List<GeographicAreaModel> getGeographicAreaListPerUser(String loginid) {		 
		 List<GeographicAreaModel> ret = geographicAreaRepository.getGeaographicAreaListPerUser(loginid);
		 System.out.println(loginid);
		 for(GeographicAreaModel geographicAreaModel:ret) {
			 byte[] unzipped =null;
			 try {
				 unzipped = unzipBytes(geographicAreaModel.getUploaded_file());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
			 if(unzipped != null) {
				 geographicAreaModel.setUploaded_file(unzipped);
				 
			 }
		 }
		 return ret;
	 }
	 
	 
	 
	 public GeographicAreaModel setGeographicAreaList(GeographicAreaModel geographicAreaModel) {		 
		 return geographicAreaRepository.save(geographicAreaModel);
	 }
	 
	 
	 public int getNextPolygonID(String prefix) {
		// return geographicAreaRepository.getNextAreacodeFromDDP("GACA1065");
		 int retval =-1;
		 int retvalDDP = 0;
		 int retvalDraft =0;
		 try {
			 retvalDDP = SOFetchRepositoryObj.getNextAreacodeFromDDP(prefix);
		 }catch(Exception e) {			 
		 }
		 try {
			 retvalDraft = geographicAreaRepository.getNextAreacodeFromDraft(prefix);
		 }catch(Exception e) {			 
		 }
		 if (retvalDDP > retvalDraft) {
			 retval = retvalDDP;
		 } else {
			 retval = retvalDraft;
		 }
		// System.out.println(prefix);
		 return retval;
	 }
	 
	 public static byte[] zipBytes(String filename, byte[] input)  {
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    ZipOutputStream zos = new ZipOutputStream(baos);
		    ZipEntry entry = new ZipEntry(filename);
		    entry.setSize(input.length);
		    try {
				zos.putNextEntry(entry);
				 zos.write(input);
				  zos.closeEntry();
				  zos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
		    return baos.toByteArray();
		}
	 
	 
		 
	 private byte[] unzipBytes( byte[] input_data ) throws Exception
	 {
	 	ZipInputStream zipStream = new ZipInputStream( new ByteArrayInputStream( input_data ) );
	 	ZipEntry entry = null;

	 	byte[] result = null;

	 	while ( (entry = zipStream.getNextEntry()) != null )
	 	{
	 		ByteArrayOutputStream baos = new ByteArrayOutputStream();

	 		byte[] buff = new byte[1024];

	 		int count = 0, loop = 0;

	 		while ( (count = zipStream.read( buff )) != -1 )
	 		{
	 			baos.write( buff, 0, count );
	 			// NLog.i("[OfflineFileParser] unzip read loop : " + loop);
	 			if ( loop++ > 1048567 )
	 			{
	 				throw new Exception();
	 			}
	 		}

	 		result = baos.toByteArray();

	 		zipStream.closeEntry();
	 	}

	 	zipStream.close();

	 	return result;
	 }
	 
	 public List<CustomPolygonList> findAreaNameList()
	 {
		 logger.info("inside findAreaNameList() method");
		 return customPolygonRepository.findAreaNameList();
	 }
	 
}
