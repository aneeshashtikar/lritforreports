/**
 * @(#)Freedraw.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */
package in.gov.lrit.portal.gis.lritdb.model;

import java.util.List;

public class Freedraw extends PolygonShape {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Freedraw(){
		super("polygon");	
		
	}
	
	/*
	public Freedraw(CustomPolygon custompolygonObj){
		super("polygon");
		userId = custompolygonObj.getCreated_by();
		polygonId = custompolygonObj.getPolygonId();
		polygonName = custompolygonObj.getPolygon_name();
		String[] points=custompolygonObj.getPolygon_points().split(",");
		lat= new ArrayList<Double>();
		lon= new ArrayList<Double>();
		for(int i=0;i<points.length;i++) {			
				try {
					lat.add(Double.parseDouble(points[i]));
					lat.add(Double.parseDouble(points[i++]));
				} catch (NumberFormatException e) {
				    
				}			
		}
		
	}*/
	
	public List<Double> getLat() {
		return lat;
	}


	public void setLat(List<Double> lat) {
		this.lat = lat;
	}


	public List<Double> getLon() {
		return lon;
	}


	public void setLon(List<Double> lon) {
		this.lon = lon;
	}

	private List<Double> lat;
	

	private List<Double> lon; //long
	
}
