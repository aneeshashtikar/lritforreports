package in.gov.lrit.portal.gis.lritdb.model;

import java.util.Date;

public interface GeographicalAreaInterface {

	
	public int getGmlId();
	public String getArea_type();
	public Date getCreation_date();
}
