/**
 * @(#)GMLAreaCodeMappingModel.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.portal.gis.lritdb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name = "portal_gmlid_areacode_mapping" )
@IdClass(GMLAreaCodeMappingModel.IdClass.class)
public class GMLAreaCodeMappingModel {
	
	

	@Override
	public String toString() {
		return "GMLAreaCodeMappingModel [areaCode=" + areaCode + ", gmlId=" + gmlId + "]";
	}


	public GMLAreaCodeMappingModel() {
		super();
	}


	@Id
	@Column(name = "area_code")
	private String areaCode;
	
	public GMLAreaCodeMappingModel(String areaCode, GeographicAreaModel geographicAreaModel) {
		super();
		this.areaCode = areaCode;
		this.geographicAreaModel = geographicAreaModel;
		this.gmlId = geographicAreaModel.getGmlId();
	}

	public GMLAreaCodeMappingModel(GMLAreaCodeMappingModel gMLAreaCodeMappingModel, GeographicAreaModel geographicAreaModel) {
		super();
		this.areaCode = gMLAreaCodeMappingModel.getAreaCode();
		this.left_bottom_latitude =gMLAreaCodeMappingModel.getLeft_bottom_latitude();
		this.left_bottom_longitude =gMLAreaCodeMappingModel.getLeft_bottom_longitude();
		this.right_top_latitude =gMLAreaCodeMappingModel.getRight_top_latitude();
		this.right_top_longitude =gMLAreaCodeMappingModel.getRight_top_longitude();
		
		
		
		this.geographicAreaModel = geographicAreaModel;
		this.gmlId = geographicAreaModel.getGmlId();
	}
	
	public String getAreaCode() {
		return areaCode;
	}


	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}


	@Id
	@Column(name = "gml_id")
	private int gmlId;
	
	
	public int getGmlId() {
		return gmlId;
	}


	public void setGmlId(int gmlId) {
		this.gmlId = gmlId;
	}


	@ManyToOne
    //@MapsId("gmlId")
    @JoinColumn(name = "gml_id", nullable = false,insertable=false,updatable=false)
	@JsonIgnore
	GeographicAreaModel geographicAreaModel;
	
	public GeographicAreaModel getGeographicAreaModel() {
		return geographicAreaModel;
	}


	public void setGeographicAreaModel(GeographicAreaModel geographicAreaModel) {
		this.geographicAreaModel = geographicAreaModel;
	} 
	
	
	Float left_bottom_latitude;
	Float left_bottom_longitude;
	Float right_top_latitude;
	Float right_top_longitude;	
	
	 public Float getLeft_bottom_latitude() {
		return left_bottom_latitude;
	}


	public void setLeft_bottom_latitude(Float left_bottom_latitude) {
		this.left_bottom_latitude = left_bottom_latitude;
	}


	public Float getLeft_bottom_longitude() {
		return left_bottom_longitude;
	}


	public void setLeft_bottom_longitude(Float left_bottom_longitude) {
		this.left_bottom_longitude = left_bottom_longitude;
	}


	public Float getRight_top_latitude() {
		return right_top_latitude;
	}


	public void setRight_top_latitude(Float right_top_latitude) {
		this.right_top_latitude = right_top_latitude;
	}


	public Float getRight_top_longitude() {
		return right_top_longitude;
	}


	public void setRight_top_longitude(Float right_top_longitude) {
		this.right_top_longitude = right_top_longitude;
	}
	
	
	
	
	 static class IdClass implements Serializable {
	        /**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			private int gmlId;			
			private String areaCode;			
			
	    }
}
