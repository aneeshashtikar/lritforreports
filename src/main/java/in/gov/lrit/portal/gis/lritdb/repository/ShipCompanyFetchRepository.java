package in.gov.lrit.portal.gis.lritdb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.gis.lritdb.model.ShipCompanyFetchModel;
import in.gov.lrit.portal.gis.lritdb.model.ShipFetchModel;



@Repository
public interface ShipCompanyFetchRepository extends CrudRepository<ShipCompanyFetchModel, Long> {
	
	@Query(value = "SELECT DISTINCT portal_shipping_company.company_name, portal_shipping_company.company_code FROM portal_shipping_company, portal_users where portal_users.company_code = portal_shipping_company.company_code and portal_users.requestors_lrit_id in ?1 ", nativeQuery = true)
	List<ShipCompanyFetchModel> findAllCompanyNameByLritId(List<String> requestorsLritId);
	
	  @Query(value =  "SELECT vw_lrit_latest_ship_position.imo_no FROM vw_lrit_latest_ship_position,portal_vessel_details, \r\n" +
	  "portal_shippingcompany_vessel_relation,portal_shipping_company, portal_users\r\n"	  +
	  " where portal_vessel_details.imo_no = vw_lrit_latest_ship_position.imo_no\r\n"  +
	  " and portal_vessel_details.vessel_id = portal_shippingcompany_vessel_relation.vessel_id\r\n" +
	  " and portal_shippingcompany_vessel_relation.user_id = portal_users.login_id\r\n"  +
	  " and (portal_shippingcompany_vessel_relation.relation = '0' or\r\n"  + //owner
	  "	portal_shippingcompany_vessel_relation.relation = '3'or\r\n"  + // owner and technical
	  "	portal_shippingcompany_vessel_relation.relation = '4'or\r\n"  + // owner and commercial
	  "	portal_shippingcompany_vessel_relation.relation = '6')\r\n"  + // All
	  " and portal_users.company_code = portal_shipping_company.company_code\r\n"  +
	  "	and portal_shipping_company.company_code = ?1",  nativeQuery = true) 
	  List<String> findByCompanyName(String company_code);
	  
	  @Query(value =  "SELECT company_name, company_code FROM portal_shipping_company, portal_vessel_details\r\n" + 
	  		"where portal_vessel_details.imo_no = ?1 and\r\n" + 
	  		"portal_vessel_details.comm_companycode = company_code",  nativeQuery = true) 
	  ShipCompanyFetchModel findCompanyByImo(String imo_no);
	  
	  @Query(value =  "SELECT company_name, company_code FROM portal_shipping_company, portal_vessel_details\r\n" + 
		  		"where portal_vessel_details.imo_no = ?1 and\r\n" + 
		  		"portal_vessel_details.owner_companycode = company_code",  nativeQuery = true) 
		  ShipCompanyFetchModel findOwnerCompanyByImo(String imo_no);
	  
	  @Query(value =  "SELECT company_name, company_code FROM portal_shipping_company, portal_vessel_details\r\n" + 
		  		"where portal_vessel_details.imo_no = ?1 and\r\n" + 
		  		"portal_vessel_details.manager_companycode = company_code",  nativeQuery = true) 
		  ShipCompanyFetchModel findManagerCompanyByImo(String imo_no);
}
