package in.gov.lrit.portal.gis.lritdb.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.gov.lrit.portal.gis.lritdb.model.SURPICModel;



public interface SURPICFetchRepository extends CrudRepository<SURPICModel, Long>{
	
	  // and access_type in :accessType , @Param("accessType") List<Integer> accessType
  		@Query(value = "Select * from dc_sarsurpicrequest where (data_user_requestor in :cg_lritid and access_type in :accessType and sarsurpicrequest_timestamp BETWEEN :startDate AND :endDate) ORDER BY sarsurpicrequest_timestamp",  nativeQuery = true)
  		List<SURPICModel> findSurpicArea(@Param("cg_lritid") List<String> cg_lritid, @Param("startDate") java.sql.Timestamp startDate, @Param("endDate") java.sql.Timestamp endDate, @Param("accessType") List<Integer> accessType);	
  		
  		@Query(value = "Select * from dc_sarsurpicrequest where (message_id in :message_idList)",  nativeQuery = true)
  		List<SURPICModel> findSurpicAreaByMessageID(@Param("message_idList") List<String> message_idList);	
  		
}
