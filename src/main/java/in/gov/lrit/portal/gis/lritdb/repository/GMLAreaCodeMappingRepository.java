package in.gov.lrit.portal.gis.lritdb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.lrit.portal.gis.lritdb.model.GMLAreaCodeMappingModel;

public interface GMLAreaCodeMappingRepository extends JpaRepository<GMLAreaCodeMappingModel, String>{

	@Query(value="select area_code from portal_gmlid_areacode_mapping where gml_id=:gmlId",nativeQuery=true)
	public List<String> getAreaCodes(int gmlId);
}
