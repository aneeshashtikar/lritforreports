package in.gov.lrit.portal.gis.lritdb.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dc_sarsurpicrequest")
public class SURPICModel {

	   @Id
	    private String message_id;  
	    private int message_type;	     
	    private int access_type;
	   	private String ship_type;
	   	private String circular_area; 
	   	private String rectangular_area; 
	   	private int  no_of_positions; 
	   	private String data_user_requestor;
	   	private Date sarsurpicrequest_timestamp; 
	   
	   	private String ddpversion_no;
	   	private int test;
	   	private int schema_version;
	   	
	   	
	   	
		public SURPICModel() {			
			// TODO Auto-generated constructor stub
		}



		public int getMessage_type() {
			return message_type;
		}



		public void setMessage_type(int message_type) {
			this.message_type = message_type;
		}



		public String getMessage_id() {
			return message_id;
		}



		public void setMessage_id(String message_id) {
			this.message_id = message_id;
		}



		public int getAccess_type() {
			return access_type;
		}



		public void setAccess_type(int access_type) {
			this.access_type = access_type;
		}



		public String getShip_type() {
			return ship_type;
		}



		public void setShip_type(String ship_type) {
			this.ship_type = ship_type;
		}



		public String getCircular_area() {
			return circular_area;
		}



		public void setCircular_area(String circular_area) {
			this.circular_area = circular_area;
		}



		public String getRectangular_area() {
			return rectangular_area;
		}



		public void setRectangular_area(String rectangular_area) {
			this.rectangular_area = rectangular_area;
		}



		public int getNo_of_positions() {
			return no_of_positions;
		}



		public void setNo_of_positions(int no_of_positions) {
			this.no_of_positions = no_of_positions;
		}



		public String getData_user_requestor() {
			return data_user_requestor;
		}



		public void setData_user_requestor(String data_user_requestor) {
			this.data_user_requestor = data_user_requestor;
		}




		public String getDdpversion_no() {
			return ddpversion_no;
		}



		public void setDdpversion_no(String ddpversion_no) {
			this.ddpversion_no = ddpversion_no;
		}



		public int getTest() {
			return test;
		}



		public void setTest(int test) {
			this.test = test;
		}



		public int getSchema_version() {
			return schema_version;
		}



		public void setSchema_version(int schema_version) {
			this.schema_version = schema_version;
		}



		public Date getSarsurpicrequest_timestamp() {
			return sarsurpicrequest_timestamp;
		}



		public void setSarsurpicrequest_timestamp(Date sarsurpicrequest_timestamp) {
			this.sarsurpicrequest_timestamp = sarsurpicrequest_timestamp;
		}



		@Override
		public String toString() {
			return "SURPICModel [message_id=" + message_id + ", message_type=" + message_type + ", access_type="
					+ access_type + ", ship_type=" + ship_type + ", circular_area=" + circular_area
					+ ", rectangular_area=" + rectangular_area + ", no_of_positions=" + no_of_positions
					+ ", data_user_requestor=" + data_user_requestor + ", sarsurpicrequest_timestamp="
					+ sarsurpicrequest_timestamp + ", ddpversion_no=" + ddpversion_no + ", test=" + test
					+ ", schema_version=" + schema_version + "]";
		}


		
	   	
	   	

}
