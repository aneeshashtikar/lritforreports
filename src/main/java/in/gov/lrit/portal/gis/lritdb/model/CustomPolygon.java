/**
 * @(#)CustomPolygon.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */

package in.gov.lrit.portal.gis.lritdb.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


import javax.persistence.Id;

@Entity
@Table(name = "portal_custom_polygon")
public class CustomPolygon {
	@Id
	@Column(name = "polygon_id")
	private String polygonId;
	
	@Column(name = "polygon_name")
	private String polygon_name;
	
	@Column(name = "polygon_type")
	private String polygon_type;
	
	public String getPolygon_name() {
		return polygon_name;
	}
	public void setPolygon_name(String polygon_name) {
		this.polygon_name = polygon_name;
	}
	@Column(name = "polygon_points")
	private String polygon_points;
	
	@Column(name = "created_by")
	private String created_by;
	
	@Column(name = "creation_date")
	private Date creation_date;	
	
	
	
	public Date getCreation_date() {
		return creation_date;
	}
	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}
	public String getPolygon_type() {
		return polygon_type;
	}
	public String getPolygonId() {
		return polygonId;
	}
	public void setPolygonId(String polygonId) {
		this.polygonId = polygonId;
	}
	public void setPolygon_type(String polygon_type) {
		this.polygon_type = polygon_type;
	}
	public String getPolygon_points() {
		return polygon_points;
	}
	public void setPolygon_points(String polygon_points) {
		this.polygon_points = polygon_points;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	

}
