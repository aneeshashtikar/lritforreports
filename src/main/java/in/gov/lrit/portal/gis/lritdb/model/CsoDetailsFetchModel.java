package in.gov.lrit.portal.gis.lritdb.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "portal_cso_dpa_details")
public class CsoDetailsFetchModel {
	
	String name;
	String telephone_office;
	String emailid;
	String telephone_residence;
	String mobile_no;
	@Id
	String lrit_csoid_seq;
	
	public CsoDetailsFetchModel() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelephone_office() {
		return telephone_office;
	}

	public void setTelephone_office(String telephone_office) {
		this.telephone_office = telephone_office;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getTelephone_residence() {
		return telephone_residence;
	}

	public void setTelephone_residence(String telephone_residence) {
		this.telephone_residence = telephone_residence;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getLrit_csoid_seq() {
		return lrit_csoid_seq;
	}

	public void setLrit_csoid_seq(String lrit_csoid_seq) {
		this.lrit_csoid_seq = lrit_csoid_seq;
	}

	


	

}
