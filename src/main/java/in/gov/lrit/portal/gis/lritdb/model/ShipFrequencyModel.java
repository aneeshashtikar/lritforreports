package in.gov.lrit.portal.gis.lritdb.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ShipFrequencyModel {

	@Id
	private String imo_no;
	
	private String request_type;
	
	private Date currentTimeStamp;

	public String getImo_no() {
		return imo_no;
	}

	public void setImo_no(String imo_no) {
		this.imo_no = imo_no;
	}

	public String getRequest_type() {
		return request_type;
	}

	public void setRequest_type(String request_type) {
		this.request_type = request_type;
	}

	public Date getCurrentTimeStamp() {
		return currentTimeStamp;
	}

	public void setCurrentTimeStamp(Date currentTimeStamp) {
		this.currentTimeStamp = currentTimeStamp;
	}
	
	
	
}
