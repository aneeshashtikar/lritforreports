package in.gov.lrit.portal.gis.lritdb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.gov.lrit.portal.gis.lritdb.model.CsoDetailsFetchModel;




public interface CsoDetailsFetchRepository  extends CrudRepository<CsoDetailsFetchModel, Long>{
	
	@Query(value = "SELECT DISTINCT ON (lrit_csoid_seq) lrit_csoid_seq, name, telephone_office, emailid, telephone_residence, mobile_no FROM portal_cso_dpa_details, \r\n" + 
			"portal_shippingcompany_vessel_relation, portal_vessel_details\r\n" + 
			"where portal_shippingcompany_vessel_relation.vessel_id = portal_vessel_details.vessel_id and\r\n" + 
			"portal_vessel_details.imo_no = ?1 and\r\n" + 
			"portal_vessel_details.cso_id = lrit_csoid_seq", nativeQuery = true)
	CsoDetailsFetchModel findCSODetails(String imo_no);
	
	
	@Query(value = "SELECT DISTINCT ON (lrit_csoid_seq) lrit_csoid_seq, name, telephone_office, emailid, telephone_residence, mobile_no FROM portal_cso_dpa_details, \r\n" + 
			"portal_shippingcompany_vessel_relation, portal_vessel_details\r\n" + 
			"where portal_vessel_details.imo_no =  ?1 and\r\n" + 
			"portal_shippingcompany_vessel_relation.vessel_id = portal_vessel_details.vessel_id and\r\n" + 
			"portal_shippingcompany_vessel_relation.alternate_cso_id = lrit_csoid_seq", nativeQuery = true)
	CsoDetailsFetchModel findAlternateCSODetails(String imo_no);

}
