/**
 * @(#)GeographicAreaData.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-team
 * @version 1.0
 */
package in.gov.lrit.portal.gis.lritdb.model;

import java.util.List;

public class GeographicAreaData {	
	
	
	
	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	
	public String getAreaType() {
		return areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	private int gmlId;

    public int getGmlId() {
		return gmlId;
	}

	public void setGmlId(int gmlId) {
		this.gmlId = gmlId;
	}

	private String fileType;
    
    private String data;    
   

    private List<GMLAreaCodeMappingModel> polygonAreas;
	
    
    public List<GMLAreaCodeMappingModel> getPolygonAreas() {
		return polygonAreas;
	}

	public void setPolygonAreas(List<GMLAreaCodeMappingModel> polygonAreas) {
		this.polygonAreas = polygonAreas;
	}

    
    public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	private String areaType;
    
    private String caption;
    

}
