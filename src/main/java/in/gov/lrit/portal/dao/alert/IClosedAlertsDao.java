package in.gov.lrit.portal.dao.alert;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.alert.AlertActionDetails;
import in.gov.lrit.portal.model.alert.AlertActionDetailsDTO;

@Repository
public interface IClosedAlertsDao extends JpaRepository<AlertActionDetails, BigInteger>{
	
	@Query(value= "select a from AlertActionDetails a where a.status = 'Close' AND a.userCategory = :userCategory"+
					"  AND a.alertDetails.contextId in (:contextList) AND a.closedTimestamp between :startDate AND :endDate order by a.closedTimestamp desc")
	public List<AlertActionDetailsDTO> getClosedAlerts(String userCategory, List<String> contextList, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
}


  
 