package in.gov.lrit.portal.dao.route;

import in.gov.lrit.portal.model.route.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface RouteDao extends JpaRepository<Route, String> {

	
	  
	/*
	 * @Query(
	 * value="select max(p.route_id) from portal_vessel_route p where p.imo_no =:imoNo"
	 * , nativeQuery=true) public abstract String getRouteIDByIMONo(String
	 * paramString);
	 */
	 
}