package in.gov.lrit.portal.dao.alert;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.lrit.portal.model.alert.AlertDetails;

@Repository
public interface IAlertDao extends JpaRepository<AlertDetails, Integer> {

	@Query(value="select pt.alert__txn_id, pt.alert_id, pt.message, pt.created_date, pt.context_id, pt.status, pt.created_by, pt.login_id from portal_alert_txn pt inner join portal_alert_mst pm" + 
			"									on pt.alert_id = pm.alert_id" + 
			"									where pt.alert__txn_id not in (select alert__txn_id from portal_alert_action pa join portal_users pu on  pu.login_id = pa.user_id where pu.category = :userCategory )" + 
			"									AND pm.alert_name in" + 
			"									(select pa.activityname from portal_role_activities_mapping pm inner join portal_activities pa on pa.activity_id = pm.activity_id where pm.role_id in :roleList AND pa.type = 'alert')" + 
			"								    AND pt.context_id in :contextIdList order by pt.created_date desc", nativeQuery=true)
	public List<AlertDetails> findAlerts(List<String> roleList, String userCategory, List<String> contextIdList);
	
	@Query(value="select role_id from portal_role_user_mapping where login_id = ?",nativeQuery=true)
	public List<String> getRoleAgainstLoginId(String loginId);
	
	@Query(value = "select pt.alert__txn_id, pt.alert_id, pt.message, pt.created_date, pt.context_id, pt.status, pt.created_by, pt.login_id from portal_alert_txn pt inner join portal_alert_mst pm" + 
			"		on pt.alert_id = pm.alert_id" + 
			"		where pt.alert__txn_id not in (select alert__txn_id from portal_alert_action pa join portal_users pu on  pu.login_id = pa.user_id where pu.category = :userCategory )" + 
			"		AND pm.alert_name in" + 
			"		(select pa.activityname from portal_role_activities_mapping pm inner join portal_activities pa on pa.activity_id = pm.activity_id where pm.role_id in :roleList AND pa.type = 'alert')" + 
			"		AND pm.servelity = :severity AND pt.context_id in :contextIdList order by pt.created_date desc", nativeQuery = true)
	public List<AlertDetails> getUserSpecificAlerts(List<String> roleList, String severity, String userCategory, List<String> contextIdList);
	
	
	@Query(value = "select pm.servelity, count(alert__txn_id) from portal_alert_txn pt inner join portal_alert_mst pm" + 
			"		on pt.alert_id = pm.alert_id " + 
			"		where pt.alert__txn_id not in (select alert__txn_id from portal_alert_action pa inner join portal_users pu on pu.login_id = pa.user_id where pu.category = :userCategory)" + 
			"		AND pm.alert_name in" + 
			"		(select pa.activityname from portal_role_activities_mapping pm inner join portal_activities pa on pa.activity_id = pm.activity_id where pm.role_id in :roleList AND pa.type = 'alert' )" + 
			"		AND pt.context_id in :contextIdList group by pm.servelity", nativeQuery = true)
	public List<Object[]> getUserSpecificAlertCount(List<String> roleList, String userCategory, List<String> contextIdList);
	
	 
	 @Transactional
	 @Modifying
	 @Query(value="insert into portal_alert_action(alert__txn_id, user_id, timestamp, status, remark, user_category) values (:alertTxnId, :loginId, NOW(), 'Close', :remark, :userCategory)"
	  , nativeQuery=true) 
	  public int insertIntoAlertActionTaken(BigInteger alertTxnId, String loginId, String remark, String userCategory);
	 
	
	 @Query(value = "select pt.alert__txn_id, pt.alert_id, pt.message, pt.created_date, pt.context_id, pt.status, pt.created_by, pt.login_id from portal_alert_txn pt inner join portal_alert_mst pm" + 
	 		"						on pt.alert_id = pm.alert_id" + 
	 		"						where pt.alert__txn_id not in (select alert__txn_id from portal_alert_action where user_id = :loginId)" + 
	 		"						AND pm.alert_name in" + 
	 		"						(select pa.activityname from portal_role_activities_mapping pm inner join portal_activities pa on pa.activity_id = pm.activity_id where pm.role_id in :roleList AND pa.type = 'alert')" + 
	 		"						AND pm.servelity = :severity AND pt.login_id = :loginId order by pt.created_date desc", nativeQuery = true)
	 public List<AlertDetails> getShippingCoAlerts(List<String> roleList, String severity, String loginId);
	 
	 
	 
	 @Query(value ="select pm.servelity, count(alert__txn_id) from portal_alert_txn pt inner join portal_alert_mst pm" + 
	 		"						on pt.alert_id = pm.alert_id" + 
	 		"						where pt.alert__txn_id not in (select alert__txn_id from portal_alert_action where user_id = :loginId )" + 
	 		"						AND pm.alert_name in" + 
	 		"						(select pa.activityname from portal_role_activities_mapping pm inner join portal_activities pa on pa.activity_id = pm.activity_id where pm.role_id in :roleList AND pa.type = 'alert' ) "+ 
	 		"						AND pt.login_id = :loginId" + 
	 		"						group by pm.servelity", nativeQuery = true)
	 public List<Object[]> getShippingCoAlertCount(List<String> roleList, String loginId); 
	 
	 
	
	
	
	
}

