package in.gov.lrit.ide.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.lrit.ide.model.JournalExtract;
import in.gov.lrit.ide.model.JournalExtractLog;

public interface JournalExctractRepository extends JpaRepository<JournalExtractLog, Integer>{
	//void saveAll(List<JournalExtract> journalExtracts);
}
