package in.gov.lrit.ide.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import in.gov.lrit.ide.model.JournalExtractLog;

public interface JournalExctractLogRepository extends JpaRepository<JournalExtractLog, Integer>{
	//void saveAll(List<JournalExtractLog> journalExtractLogs);
	List<JournalExtractLog> findByFromDateBetween(Date fromDate, Date toDate);
}
