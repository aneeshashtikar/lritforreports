package in.gov.lrit.ide.controller;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import in.gov.lrit.ide.exception.FileStorageException;
import java.text.ParseException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import in.gov.lrit.ide.model.JournalExtractLog;
import in.gov.lrit.ide.service.JournalExtractService;

@Controller
@RequestMapping("/ide")
public class JournalExtractController {

	/*
	 * Journal extract service to manage uploading and viewing of journal extracts 
	 */
	@Autowired
	private JournalExtractService journalExtractService;

	// logger for JournalExtractController class 
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(JournalExtractController.class);
	
	/* 
	 * Get request controller for /ide/uploadJournalExtract 
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping("/uploadJournalExtract")
	// @ResponseBody
	public String uploadJournalExtract(final Model model) {

		return "ide/uploadJournalExtract";
	}

	/*
	 * Post request controller for /ide/uploadJournalExtractPost
	 */
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@PostMapping("/uploadJournalExtractPost")
	// @ResponseBody
	public ModelAndView uploadJournalExtractPost(@RequestParam("journalExtractZipFile") final MultipartFile file, final Model model) {

		String status = "";
		ModelAndView modelAndView = new ModelAndView("/ide/uploadJournalExtract");
		String fileName = file.getOriginalFilename();
		try { 
			journalExtractService.uploadExtractAndLoadJournalExtract(file);
			modelAndView.addObject("successMsg", "Journal Extract has been uploaded successfully.");
			return modelAndView;
		} catch( FileStorageException fse) {
			status = "Uploading of " + fileName + " have failed<br>";
			status += "Please select correct file ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = "Uploading of " + fileName + " have failed<br>";
			status += "Please select zip file with correct name format ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = "Uploading of " + fileName + " have failed<br>";
			status += "Unable to find the required files and directories ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = "Uploading of " + fileName + " have failed<br>";
			status += "File is not found or required permissions are not available for the file ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		} catch (DataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = "Uploading of " + fileName + " have failed<br>";
			status += "The contents of zip file are not in the required format ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		} catch (PSQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = "Uploading of " + fileName + " have failed<br>";
			status += "Database is unavailable ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		} catch (ConstraintViolationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = "Uploading of " + fileName + " have failed<br>";
			status += "Part of the journal extract data already present in the database ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = "Uploading of " + fileName + " have failed<br>";
			status += "Database is unavailable ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		} catch (Exception e) {
			e.printStackTrace();
			status = "Uploading of " + fileName + " have failed<br>";
			status += "Unhandled exception occured. Exception Msg: ";
			modelAndView.addObject("errorMsg", status);
			return modelAndView;
		}
					
		
		
	}

	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping("/viewJournalExtract")
	// @ResponseBody
	public String viewJournalExtract(Model model) {
		model.addAttribute("msg", "Upload Journal Extractor");
		logger.info("View Journal Extract Controller ");
		return "ide/viewJournalExtract";
	}

	// /getreportingratesar	 
	@PreAuthorize(value = "hasAuthority('lritaccount')")
	@RequestMapping(value="/getJournalExtractLogs",method=RequestMethod.POST)
	@ResponseBody public List<JournalExtractLog> getJournalExtractLogs( 
			@RequestParam(name="from",required =false)   String from,
			@RequestParam(name = "to", required = false) String to) {
		logger.info("From " + from + " To " + to);
		List<JournalExtractLog> journalExtractLogs = null;
		if ( from == null || from.equals("") || to == null || to.equals("")) {
			journalExtractLogs = journalExtractService.findAllJournalExtractLog();
		} else {
			java.sql.Date fromDate = java.sql.Date.valueOf(from);
			java.sql.Date toDate = java.sql.Date.valueOf(to);
			journalExtractLogs = journalExtractService
				.findAllJournalExtractLogBetweenDates(fromDate, toDate);
		}
		return journalExtractLogs;
		// return null;
	}
//	@RequestMapping("/getjournalextractlogs")
//	@ResponseBody 
//	public String getJournalExtractLogs() {
//		
//		return journalExtractService.findAllJournalExtractLog().toString();
//	}
}
