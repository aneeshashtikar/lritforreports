package in.gov.lrit.ide.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import in.gov.lrit.ide.exception.FileStorageException; 
//import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import in.gov.lrit.ddp.dao.LritDatacentreMstRepository;
//import in.gov.lrit.config.LritDbEntityManager;
import in.gov.lrit.ide.dao.JournalExctractLogRepository;
import in.gov.lrit.ide.model.JournalExtractLog;

@Service
public class JournalExtractServiceImpl implements JournalExtractService {

	@Autowired
	private FileStorageService fileStorageService;

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(JournalExtractServiceImpl.class);

	@Autowired
	private Unzipper unzipper;

	/*
	 * @Autowired private LritDbEntityManager lritDbEntityManager;
	 */

	// @PersistenceContext(unitName = "lritDbEntityManagerFactory")
	@PersistenceContext(unitName = "portalEntityManagerFactory")
	private EntityManager entityManager;

	/*
	 * @PersistenceContext(unitName = "ddpDbEntityManagerFactory") private
	 * EntityManager ddpDbEntityManager;
	 */

	@Autowired
	private LritDatacentreMstRepository lritDataCentreMstRepo;

	@Autowired
	private CSVLoader csvLoader;

	@Autowired
	private JournalExctractLogRepository journalExtractLogRepo;

	// Extract multipart file(zip file)
	// Dump the zip file in database

	// Get source, destination, from date and to Date from zip file
	private Map<String, Object> parseFileName(String fileName) throws ParseException {
		// Map to store key value pairs where keys will be source, destination, from
		// date and to date
		Map<String, Object> map = new HashMap<String, Object>();

		// Split file name
		String[] strings = fileName.split("_");
		// Sample output of printing using foreach for strings
		// journal
		// 3065
		// All
		// 010119
		// 063019.zip
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddyy");
		Date localDateTime = null;
		String dcName = null;
		for (int i = 0; i < strings.length; i++) {
			logger.info(strings[i]);
		}
		if (strings == null || !strings[0].equals("journal") || strings.length != 5 ) {
			logger.info("Filename should start with journal");
			return null;
		}
		if (strings[1].equals("All")) {
			logger.info("Keeping All as it is");
			map.put("Source", strings[1]);
		} else {
			dcName = lritDataCentreMstRepo.getNameByDCLritid(strings[1]);
			logger.info("LritID " + strings[1] + " Name " + dcName);
			map.put("Source", dcName);
		}
		if (strings[2].equals("All")) {
			map.put("Destination", strings[2]);
		} else {
			dcName = lritDataCentreMstRepo.getNameByDCLritid(strings[2]);
			logger.info("LritID " + strings[2] + " Name " + dcName);
			map.put("Destination", dcName);
		}

		localDateTime = new Date(simpleDateFormat.parse(strings[3]).getTime());
		logger.info("Date is: " + localDateTime);
		map.put("FromDate", localDateTime);
		String lastStrings[] = strings[4].split("\\.");
		logger.info("lastStrings[0]: " + lastStrings[0]);
		localDateTime = new Date(simpleDateFormat.parse(lastStrings[0]).getTime());
		logger.info("Date is: " + localDateTime);
		map.put("ToDate", localDateTime);

		return map;
	}

	// Extract multipart file(zip file)
	// Dump the zip file in database

	@Transactional(value = "portalTransactionManager")
	@Override
	public void uploadExtractAndLoadJournalExtract(MultipartFile file) throws ParseException, FileNotFoundException, IOException, DataException, SQLException, FileStorageException{
		String fileName = null;


		// Upload zip file
		String zipFileName = file.getOriginalFilename(); 
		fileName = fileStorageService.storeFile(file);
		logger.info("File uploaded at " + fileStorageService.getFileAsString(fileName));

		Map<String, Object> map = parseFileName(zipFileName);

		if ( map == null ) {
			throw new ParseException(zipFileName, 0);  
		}
		// Collect inputs for zip extraction
		String zipFile = fileStorageService.getFileAsString(fileName);
		String csvDir = fileStorageService.getCsvDir();

		// Extract zip file
		String extractedCsvFile = unzipper.unzip(zipFile, csvDir);
		logger.info("Extracted file from zip file is " + extractedCsvFile);

		// get Entity Manager
		// EntityManager em = lritDbEntityManager.getLritDbEntityManager();

		// Hard coded value

		String tableName = "billing_journal_extract";
		String columnOrder = "(journal_id,journalled_by,message_id,message_type,"
				+ "originator_data_centre,originator_data_user,destination_data_centre,"
				+ "destination_data_user,reference_message_id,receive_time,send_start_time,"
				+ "send_time,test_indicator,sar_indicator,delivered,delivery_notes,delivery_retries)";

		// Load csv file
		csvLoader.loadCSV(entityManager, extractedCsvFile, tableName, columnOrder);

		logJournalExtract(zipFile, extractedCsvFile, map);


	}

	// log metadata(zip file and csv file along with location and time of loading
	// zip file) about the journal extract

	private void logJournalExtract(String journalExtractZipFile, String journalExtractCsvFile,
			Map<String, Object> map) {
		// TODO Auto-generated method stub
		JournalExtractLog journalExtractLog = new JournalExtractLog();
		int journalExtractZipFileLength = journalExtractZipFile.length();
		int lastIndex = journalExtractZipFile.lastIndexOf("/");
		String journalExtractZipFileName = journalExtractZipFile.substring(lastIndex + 1, journalExtractZipFileLength);
		int journalExtractCsvFileLength = journalExtractCsvFile.length();
		lastIndex = journalExtractCsvFile.lastIndexOf("/");
		String journalExtractCsvFileName = journalExtractCsvFile.substring(lastIndex + 1, journalExtractCsvFileLength);

		// java.util.Date now = new java.util.Date();
		// Timestamp loadedOn = new Timestamp(now.getTime());
		journalExtractLog.setJournalExtractZip(journalExtractZipFileName);
		journalExtractLog.setJournalExtractCsv(journalExtractCsvFileName);



		// Source
		String source = (String) map.get("Source");
		journalExtractLog.setSource(source);

		// Destination
		String destination = (String) map.get("Destination");
		journalExtractLog.setDestination(destination);

		// FromDate
		Date fromDate = (Date) map.get("FromDate");

		// ToDate
		Date toDate = (Date) map.get("ToDate");
		try {
			// journalExtractLog.setLoadedOn(now);
			journalExtractLog.setFromDate(fromDate);
			journalExtractLog.setToDate(toDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		journalExtractLogRepo.save(journalExtractLog);

	}

	// Get list of journal extract logs
	@Override
	public List<JournalExtractLog> findAllJournalExtractLog() {
		// TODO Auto-generated method stub
		List<JournalExtractLog> journalExtractLogs = journalExtractLogRepo.findAll();

		return journalExtractLogs;
	}

	/*
	 * @Override public List<JournalExtractLog>
	 * findAllJournalExtractLogBetweenDates(Date fromDate, Date toDate) { // TODO
	 * Auto-generated method stub
	 * 
	 * List<JournalExtractLog> journalExtractLogs =
	 * journalExtractLogRepo.findByFromDateBetween(fromDate, toDate); return
	 * journalExtractLogs; }
	 */

	/**
	 * @author Yogendra Yadav
	 */
	@Override
	public List<JournalExtractLog> findAllJournalExtractLogBetweenDates(java.sql.Date fromDate, java.sql.Date toDate) {
		// TODO Auto-generated method stub
		List<JournalExtractLog> journalExtractLogs = journalExtractLogRepo.findByFromDateBetween(fromDate, toDate);
		return journalExtractLogs;
	}

}
