package in.gov.lrit.ide.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.hibernate.exception.DataException;
import org.springframework.web.multipart.MultipartFile;

import in.gov.lrit.ide.model.JournalExtractLog;

public interface JournalExtractService {
	// Upload zip file, extract it and load it into the database
	void uploadExtractAndLoadJournalExtract(MultipartFile file) throws ParseException, FileNotFoundException, IOException, DataException, SQLException;

	// To find all journal logs
	List<JournalExtractLog> findAllJournalExtractLog();

	// To find journal extract logs between given dates
	List<JournalExtractLog> findAllJournalExtractLogBetweenDates(Date fromDate, Date toDate);

	/**
	 * 
	 * 
	 * @author Yogendra Yadav
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	//List<JournalExtractLog> findAllJournalExtractLogBetweenDates(java.util.Date fromDate, java.util.Date toDate);

}
