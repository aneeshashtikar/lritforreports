package in.gov.lrit.ide.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CSVLoader {
	//@Transactional(value = "portalTransactionManager")
	public void loadCSV(EntityManager em, String csvFile, String tableName, String columnOrder)
			throws SQLException, FileNotFoundException, IOException {
		Session session = em.unwrap(Session.class);

		session.doWork(new Work() {

			@Override
			public void execute(Connection con) throws SQLException {
				// do whatever you need to do with the connection
				CopyManager copyManager = new CopyManager(con.unwrap(BaseConnection.class));
				FileReader fileReader;

				try {
					fileReader = new FileReader(csvFile);
					copyManager.copyIn("COPY " + tableName + columnOrder + " FROM STDIN with csv header", fileReader);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					// throw e;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					// throw e;
				}
//					catch (PSQLException e) {
//						throw e;
//					}
			}
		});
	}

	
}
