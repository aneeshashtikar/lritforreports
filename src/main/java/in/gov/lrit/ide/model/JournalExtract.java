package in.gov.lrit.ide.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the journal_extract database table.
 * 
 */
@Entity
@Table(name="billing_journal_extract")
@NamedQuery(name="JournalExtract.findAll", query="SELECT j FROM JournalExtract j")
public class JournalExtract implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id

	@SequenceGenerator(name="billing_journal_extract_seq_gen",sequenceName="billing_journal_extract_seq", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="billing_journal_extract_seq_gen")

	@Column(name = "journal_extract_id") 
	private Integer journalExtractId;



	@Column(name="journal_id")
	private Integer journalId;

	@Column(name="journalled_by")
	private Integer journalledBy;

	@Column(name="message_id",  columnDefinition="varchar(25)")
	private String messageId;

	@Column(name="message_type")
	private Integer messageType;

	@Column(name="originator_data_centre")
	private String originatorDataCentre;

	@Column(name="originator_data_user")
	private String originatorDataUser;

	@Column(name="destination_data_centre")
	private String destinationDataCentre;

	@Column(name="destination_data_user")
	private String destinationDataUser;

	@Column(name="reference_message_id",  columnDefinition="varchar(25)")
	private String referenceMessageId;

	@Column(name="receive_time")
	private Timestamp receiveTime;

	@Column(name="send_start_time")
	private Timestamp sendStartTime;

	@Column(name="send_time")
	private Timestamp sendTime;

	@Column(name="test_indicator")
	private String testIndicator;

	@Column(name="sar_indicator")
	private String sarIndicator;

	private String delivered;

	@Column(name="delivery_notes")
	private String deliveryNotes;

	@Column(name="delivery_retries")
	private Integer deliveryRetries;

	public JournalExtract() {
	}

	public Integer getJournalId() {
		return this.journalId;
	}

	public void setJournalId(Integer journalId) {
		this.journalId = journalId;
	}

	public String getDelivered() {
		return this.delivered;
	}

	public void setDelivered(String delivered) {
		this.delivered = delivered;
	}

	public String getDeliveryNotes() {
		return this.deliveryNotes;
	}

	public void setDeliveryNotes(String deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	public Integer getDeliveryRetries() {
		return this.deliveryRetries;
	}

	public void setDeliveryRetries(Integer deliveryRetries) {
		this.deliveryRetries = deliveryRetries;
	}

	public String getDestinationDataCentre() {
		return this.destinationDataCentre;
	}

	public void setDestinationDataCentre(String destinationDataCentre) {
		this.destinationDataCentre = destinationDataCentre;
	}

	public String getDestinationDataUser() {
		return this.destinationDataUser;
	}

	public void setDestinationDataUser(String destinationDataUser) {
		this.destinationDataUser = destinationDataUser;
	}

	public Integer getJournalledBy() {
		return this.journalledBy;
	}

	public void setJournalledBy(Integer journalledBy) {
		this.journalledBy = journalledBy;
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Integer getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public String getOriginatorDataCentre() {
		return this.originatorDataCentre;
	}

	public void setOriginatorDataCentre(String originatorDataCentre) {
		this.originatorDataCentre = originatorDataCentre;
	}

	public String getOriginatorDataUser() {
		return this.originatorDataUser;
	}

	public void setOriginatorDataUser(String originatorDataUser) {
		this.originatorDataUser = originatorDataUser;
	}

	public Timestamp getReceiveTime() {
		return this.receiveTime;
	}

	public void setReceiveTime(Timestamp receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getReferenceMessageId() {
		return this.referenceMessageId;
	}

	public void setReferenceMessageId(String referenceMessageId) {
		this.referenceMessageId = referenceMessageId;
	}

	public String getSarIndicator() {
		return this.sarIndicator;
	}

	public void setSarIndicator(String sarIndicator) {
		this.sarIndicator = sarIndicator;
	}

	public Timestamp getSendStartTime() {
		return this.sendStartTime;
	}

	public void setSendStartTime(Timestamp sendStartTime) {
		this.sendStartTime = sendStartTime;
	}

	public Timestamp getSendTime() {
		return this.sendTime;
	}

	public void setSendTime(Timestamp sendTime) {
		this.sendTime = sendTime;
	}


	public String getTestIndicator() {
		return this.testIndicator;
	}

	public void setTestIndicator(String testIndicator) {
		this.testIndicator = testIndicator;
	}


	public Integer getJournalExtractId() { return journalExtractId; }

	public void setJournalExtractId(Integer journalExtractId) {
		this.journalExtractId = journalExtractId; 
	}




}