package in.gov.lrit.ide.model;

import java.io.Serializable;
import java.text.ParseException;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The persistent class for the journal_query database table.
 * 
 */
@Entity
@Table(name = "billing_journal_extract_log")
@NamedQuery(name = "JournalExtractLog.findAll", query = "SELECT j FROM JournalExtractLog j")
public class JournalExtractLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="billing_journal_extract_log_seq_gen",sequenceName="billing_journal_extract_log_seq", initialValue=1, allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="billing_journal_extract_log_seq_gen")
	@Column(name = "journal_extract_log_id")
	private int journalExtractLogId;

	@Column(name = "journal_extract_zip")
	private String journalExtractZip;

	@Column(name = "journal_extract_csv")
	private String journalExtractCsv;

	private String source;

	private String destination;

	@Column(name = "from_date")
	// @Temporal(TemporalType.TIMESTAMP)
	// @JsonFormat(pattern="yyyy-MM-dd")
	private Date fromDate;

	@Column(name = "to_date")
	// @JsonFormat(pattern="yyyy-MM-dd")
	private Date toDate;

	@Column(name = "loaded_on", columnDefinition = "TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP")
	// @Column(name = "loaded_on", columnDefinition= "TIMESTAMP WITH TIME ZONE")
	/*
	 * //@Temporal(TemporalType.DATE)
	 * 
	 * @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Kolkata")
	private java.util.Date loadedOn = new java.util.Date();

	public int getJournalExtractLogId() {
		return journalExtractLogId;
	}

	public void setJournalExtractLogId(int journalExtractLogId) {
		this.journalExtractLogId = journalExtractLogId;
	}

	public String getJournalExtractZip() {
		return journalExtractZip;
	}

	public void setJournalExtractZip(String journalExtractZip) {
		this.journalExtractZip = journalExtractZip;
	}

	public String getJournalExtractCsv() {
		return journalExtractCsv;
	}

	public void setJournalExtractCsv(String journalExtractCsv) {
		this.journalExtractCsv = journalExtractCsv;
	}

	public java.util.Date getLoadedOn() throws ParseException {
		/*
		 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); return
		 * dateFormat.parse(dateFormat.format(loadedOn));
		 */
		return loadedOn;
	}

	public void setLoadedOn(java.util.Date loadedOn) throws ParseException {
		/*
		 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); this.loadedOn =
		 * dateFormat.parse(dateFormat.format(loadedOn));
		 */
		this.loadedOn = loadedOn;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getFromDate() throws ParseException {
		// DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// return dateFormat.parse(dateFormat.format(fromDate));
		return fromDate;
	}

	public void setFromDate(Date fromDate) throws ParseException {
		/*
		 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); this.fromDate =
		 * dateFormat.parse(dateFormat.format(fromDate));
		 */
		this.fromDate = fromDate;
	}

	public Date getToDate() throws ParseException {
		/*
		 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); return
		 * dateFormat.parse(dateFormat.format(toDate));
		 */
		return toDate;
	}

	public void setToDate(Date toDate) throws ParseException {
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");		
//		this.toDate = dateFormat.parse(dateFormat.format(toDate));
		this.toDate = toDate;
	}

	@Override
	public String toString() {
		return "JournalExtractLog [journalExtractLogId=" + journalExtractLogId + ", journalExtractZip="
				+ journalExtractZip + ", journalExtractCsv=" + journalExtractCsv + ", source=" + source
				+ ", destination=" + destination + ", fromDate=" + fromDate + ", toDate=" + toDate + ", loadedOn="
				+ loadedOn + "]";
	}

}