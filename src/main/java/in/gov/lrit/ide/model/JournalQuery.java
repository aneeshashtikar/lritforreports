package in.gov.lrit.ide.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the journal_query database table.
 * 
 */
@Entity
@Table(name="journal_query")
@NamedQuery(name="JournalQuery.findAll", query="SELECT j FROM JournalQuery j")
public class JournalQuery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="message_id")
	private float messageId;

	private String delivered;

	@Column(name="delivery_notes")
	private String deliveryNotes;

	@Column(name="delivery_retries")
	private Integer deliveryRetries;

	@Column(name="destination_data_centre")
	private String destinationDataCentre;

	@Column(name="destination_data_user")
	private String destinationDataUser;

	@Column(name="journalled_by")
	private Integer journalledBy;

	@Column(name="message_type")
	private Integer messageType;

	@Column(name="originator_data_centre")
	private String originatorDataCentre;

	@Column(name="originator_data_user")
	private String originatorDataUser;

	@Column(name="receive_time")
	private Timestamp receiveTime;

	@Column(name="reference_message_id")
	private float referenceMessageId;

	@Column(name="sar_indicator")
	private String sarIndicator;

	@Column(name="send_time")
	private Timestamp sendTime;

	@Column(name="test_indicator")
	private String testIndicator;

	public JournalQuery() {
	}

	public float getMessageId() {
		return this.messageId;
	}

	public void setMessageId(float messageId) {
		this.messageId = messageId;
	}

	public String getDelivered() {
		return this.delivered;
	}

	public void setDelivered(String delivered) {
		this.delivered = delivered;
	}

	public String getDeliveryNotes() {
		return this.deliveryNotes;
	}

	public void setDeliveryNotes(String deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	public Integer getDeliveryRetries() {
		return this.deliveryRetries;
	}

	public void setDeliveryRetries(Integer deliveryRetries) {
		this.deliveryRetries = deliveryRetries;
	}

	public String getDestinationDataCentre() {
		return this.destinationDataCentre;
	}

	public void setDestinationDataCentre(String destinationDataCentre) {
		this.destinationDataCentre = destinationDataCentre;
	}

	public String getDestinationDataUser() {
		return this.destinationDataUser;
	}

	public void setDestinationDataUser(String destinationDataUser) {
		this.destinationDataUser = destinationDataUser;
	}

	public Integer getJournalledBy() {
		return this.journalledBy;
	}

	public void setJournalledBy(Integer journalledBy) {
		this.journalledBy = journalledBy;
	}

	public Integer getMessageType() {
		return this.messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public String getOriginatorDataCentre() {
		return this.originatorDataCentre;
	}

	public void setOriginatorDataCentre(String originatorDataCentre) {
		this.originatorDataCentre = originatorDataCentre;
	}

	public String getOriginatorDataUser() {
		return this.originatorDataUser;
	}

	public void setOriginatorDataUser(String originatorDataUser) {
		this.originatorDataUser = originatorDataUser;
	}

	public Timestamp getReceiveTime() {
		return this.receiveTime;
	}

	public void setReceiveTime(Timestamp receiveTime) {
		this.receiveTime = receiveTime;
	}

	public float getReferenceMessageId() {
		return this.referenceMessageId;
	}

	public void setReferenceMessageId(float referenceMessageId) {
		this.referenceMessageId = referenceMessageId;
	}

	public String getSarIndicator() {
		return this.sarIndicator;
	}

	public void setSarIndicator(String sarIndicator) {
		this.sarIndicator = sarIndicator;
	}

	public Timestamp getSendTime() {
		return this.sendTime;
	}

	public void setSendTime(Timestamp sendTime) {
		this.sendTime = sendTime;
	}

	public String getTestIndicator() {
		return this.testIndicator;
	}

	public void setTestIndicator(String testIndicator) {
		this.testIndicator = testIndicator;
	}

}