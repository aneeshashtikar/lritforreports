/**
 * @(#)Utility.java
 * @copyright 2019 CDAC Mumbai. All rights reserved.
 * @author  lrit-portal
 * @version 1.0
 * @description This class has some basic utility function like generate messaging password, send mail
 * 
 * Change Details - 
 * sha1() - Changed the encryption algorithm from sha1 to sha-256
 */
package in.gov.lrit;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Random;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Class for utility methods
 * @author lrit-portal
 *
 */
public class Utility {

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Utility.class);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		logger.info(bcryptPassword("Welcome123"));
	}
	
	/*This method generates the passworsd
	 * This method is used to generate the captcha and password
	 * 
	 * */
	public static String generatePassword(int length){
		String saltChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		while(sb.length() < length){
			int index = (random.nextInt(saltChars.length()));
			sb.append(saltChars.substring(index, index+1));
		}
		return sb.toString();
	}
	
	/**
	 * Returns a salt to be added while calculating the hash value for password. 
	 * @return	byte array of 32 size
	 * */
	public static byte[] getRandomSalt() {		
		SecureRandom sr = new SecureRandom();
		byte[] salt = new byte[32];
		sr.nextBytes(salt);			
		return salt;
	}
	
	public static void sendMail(String sender, String recipient, String title, String message,JavaMailSender mailSender) throws MailException, MessagingException{
	
	      try {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
		mimeMessage.setContent(message, "text/html");
		helper.setTo(recipient);
		helper.setSubject(title);
		helper.setFrom(sender);
		mailSender.send(mimeMessage);
	      }
	      catch (MessagingException ex) {
	            logger.debug("Error While Sending Mail");
	            logger.error("Error "+ex.getMessage(),ex);
	            ex.printStackTrace();
	        }
	}
	
	public static String getFormattedTime(Timestamp ts) {
		
		Calendar time = Calendar.getInstance();
		time.setTimeInMillis(ts.getTime());
		
		int hour = time.get(Calendar.HOUR_OF_DAY);
		int minute = time.get(Calendar.MINUTE);
		
		String formattedTime="";
		
		if(hour<=9) {
			formattedTime = "0";
		}
		formattedTime += hour;
		
		if(minute<=9) {
			formattedTime += "0";
		}
		formattedTime += minute;
		
		return formattedTime;
	}
	
	public static String bcryptPassword(String password){
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		return hashedPassword;
	}
	/**
	 * For generating clear type of password messaging password 
	 */
	public static String getNewClearPassword()
	{
		Random rand = new Random();
		Random rand1 = new Random();
		int j = rand.nextInt();
		int m = 20;
		j = rand1.nextInt(m+1);
		byte[] bytes1 = new byte[6];
		rand1.nextBytes(bytes1);
		String clearpassw =""+bytes1;
		return clearpassw;
	}
	/**
	 * For generating sha256 type of password of messaging password
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String sha2(String input) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA-256"); // We have upgraded from sha-1 to sha-256.
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	/**
	 * For generating sha1 type of password of messaging password
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String sha1(String input) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	
	
}
