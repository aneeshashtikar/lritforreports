

function createGrid(gridNo){
	var squareGrid = []  	  
	  var bbox;
	  var poly;
	  if (gridNo ==1 )
	  {
		  bbox = [-180, -85, 180, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
	  }
	  else if (gridNo ==2 )
	  {
		  bbox = [-180, -85, 0, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, -85, 180, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
	  }
	  
	  else if (gridNo ==4 )
	  {
		  bbox = [-180, -85, 0, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-180, 0, 0, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, -85, 180, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, 0, 180, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
	  }
	 
	  else if (gridNo ==8 )
	  {
		  bbox = [-180, -85, -90, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-180, 0, -90, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-90, -85, 0, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-90, 0, 0, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, -85, 90, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, 0, 90, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [90, -85, 180, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [90, 0, 180, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
	  }
	  else if (gridNo == 16 )
	  {
		  bbox = [-180, -85, -135, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-180, 0, -135, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-135, -85, -90, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-135, 0, -90, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-90, -85, -45, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-90, 0, -45, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-45, -85, 0, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-45, 0, 0, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, -85, 45, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, 0, 45, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [45, -85, 90, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [45, 0, 90, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [90, -85, 135, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [90, 0, 135, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [135, -85, 180, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [135, 0, 180, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
	  }
	  else if (gridNo == 32 )
	  {
		  bbox = [-180, -85, -135, -45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-180, -45, -135, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-180, 0, -135, 45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-180, 45, -135, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-135, -85, -90, -45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-135, -45, -90, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-135, 0, -90, 45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-135, 45, -90, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-90, -85, -45, -45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-90, -45, -45, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-90, 0, -45, 45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-90, 45, -45, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-45, -85, 0, -45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-45, -45, 0, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-45, 0, 0, 45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [-45, 45, 0, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, -85, 45, -45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, -45, 45, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, 0, 45, 45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [0, 45, 45, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [45, -85, 90, -45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [45, -45, 90, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [45, 0, 90, 45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [45, 45, 90, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [90, -85, 135, -45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [90, -45, 135, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [90, 0, 135, 45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [90, 45, 135, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [135, -85, 180, -45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [135, -45, 180, 0];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [135, 0, 180, 45];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
		  bbox = [135, 45, 180, 85];
		  poly = turf.bboxPolygon(bbox);
		  squareGrid.push(poly);
	  }
	  return squareGrid;
}