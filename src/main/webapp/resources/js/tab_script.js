


/**
 * Created a script for the category field to open a new tab while registering
 * user
 */

$(function() {
	/*$('#category').click(function() {
		var _selected = $('#category').val();
		if(_selected == 'Shipping Company')
			{
				var _newtab = '<li id="tab3"><a href="#tab_3" data-toggle="tab">CSO Details</a></li>';
				$('div.nav-tabs-custom ul').append(_newtab);
				$('li#tab3').addClass('active').siblings().removeClass('active');
			}
		else
			{
				$('li#tab3').hide();
			}
		});*/
	
	/*$(document).on('dblclick', '#category', function() {
		if($('#category').val() == 'Shipping Company')
			{
				$('li#tab3').show().addClass('active').siblings().removeClass('active');
			}
		});*/
	$(document).on('click', '.btn-prev', function() {
  		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
  	});
  	
  	$(document).on('click', '.btn-next', function() {
  		 var active = $('.nav-tabs > .active').attr( 'id' );
  		alert("active id "+active);
  		if(active=='userdetailstab'){
  			alert("Inside if of userdetail tab");
  			//validation of userdetail tab
  		var flag=validationOfUserDetails();	
  			//if validation is sucess make entry in database
  		}
  		
  		alert("Outside if of userdetail tab"+flag);
  		
  		/*
		 * console.info("ssssssssss");
		 * 
		 * var uservalidationflag= userDetailsValidation();
		 * 
		 * console.info(uservalidationflag); if(uservalidationflag==true) {
		 */
  		if(flag==true) {
  			alert("click next");
  		$('.nav-tabs > .active').next('li').find('a').trigger('click');
  		}
  	/* } */
  	});
	});
		
function validationOfUserDetails(){
	var nameFlag=NameValidation();
	var loginIdFlag=LoginIdValidation();
	var roleFlag=RoleValidation();
	var fileFlag=FileValidation();
	if(nameFlag==true && loginIdFlag==true && roleFlag==true && fileFlag==true ) {
		console.log("value is true");
		//check loginid already exist in database
		var loginId = document.getElementById("loginid").value;
		var flag =checkLoginIdExist();
		alert("user id already exist "+ flag.val);
		/*if(checkLoginIdExist()){
			 document.getElementById("loginIderror").innerHTML="";
			return true;
		}else {
			 document.getElementById("loginIderror").innerHTML="LoginId already exist";
		}*/
		
	}
	return false;
}



function checkLoginIdExist() {

	var loginId = document.getElementById("loginid").value;

	  $
				.ajax({
					url : '/lrit/users/checkLoginId',async : false,
					type : 'GET',
					data : {
						loginId : loginId
					},
					contentType : 'application/json; charset=utf-8',
					success : function(data) {

						//alert
						alert("Inside success "+data);
						if(data == 'true'){
							alert("inside  true of ajax call");
						return true;
							
						}else {
							alert("inside false of ajax call");
							return false;
						}
					},
					error : function(error) {
						alert("Error AJAX not working: " + error);
						return false;
					}
				});



}
































function userDetailsValidation(formId) {
	console.log("userDetailsValidation");
	
	var nameFlag=NameValidation();
	console.log(nameFlag);
	var loginIdFlag=LoginIdValidation();
	console.log("loginIdFlag"+loginIdFlag);
	var roleFlag=RoleValidation();
	console.log("roleFlag"+roleFlag);
	
	var addressFlag= addressValidation();
	console.log("addressFlag"+addressFlag);
	var validation=Validation();
	console.log("validation"+validation);
	var mobilenoFlag=MobileNoValidation();
	console.log("mobilenoFlag"+mobilenoFlag);
	var faxFlag=FaxValidation();
	console.log("faxFlag"+faxFlag);
	var emailFlag=EmailValidation();
	console.log("emailFlag"+emailFlag);
	var websiteFlag=WebsiteValidation();
	console.log("websiteFlag"+websiteFlag);
	var pincodeFlag=pincodeValidation();
	console.log("pincodeFlag"+pincodeFlag);
	var telephoneFlag=telephoneValidation();
	console.log("telephoneFlag"+telephoneFlag);
	if(formId=='editshipingcompanyuserform')
		{
		
		console.log("FormId "+formId);
		if(nameFlag==true && loginIdFlag==true && roleFlag==true && addressFlag==true && validation==true && mobilenoFlag==true
				&& faxFlag== true && emailFlag==true && websiteFlag ==true && pincodeFlag==true && telephoneFlag==true) {
			console.log("value is true");
			return true;
		}
		return false;
		}else if(formId=='adduserform') {
			var fileFlag=FileValidation();
			console.log("fileFlag"+fileFlag);
			if(nameFlag==true && loginIdFlag==true && roleFlag==true && fileFlag==true && addressFlag==true && validation==true && mobilenoFlag==true
					&& faxFlag== true && emailFlag==true && websiteFlag ==true && pincodeFlag==true && telephoneFlag==true) {
				console.log("value is true");
				return true;
			}
			return false;
		}
	
}
function isEmpty(str){
    return !str.replace(/\s+/, '').length;
}

function NameValidation(){
	var name = document.getElementById("name").value;
	var error=null;
	console.log("error "+error);
	  var letters = /^[A-Za-z]{1,50}$/;
	   
	if( isEmpty(name) ) {
	    error="Name is Empty";
	  }
	else if(!name.match(letters)){
		  error="Name should contain only alphabets between 1 to 50";
	}
		
	
	if(error!=null) {
		 document.getElementById("nameerror").innerHTML=error;
		 return false;
		 
	}else {
		document.getElementById("nameerror").innerHTML="";
		 return true;
	}
		
		
		
}
function LoginIdValidation() {
	var loginId = document.getElementById("loginid").value;
	var alphanumberic = /^[0-9A-Za-z]{5,50}$/;
	var error=null;
	if( isEmpty(loginId) ) {
	    error="loginId is Empty";
	    }
	else if(!loginId.match(alphanumberic)){
		  error="LoginId should contain only alphanumeric between 5 to 50";
	}
	if(error!=null) {
	 document.getElementById("loginIderror").innerHTML=error;
	 return false;
	} else {
		 document.getElementById("loginIderror").innerHTML="";
		 return true;
	}
}
function RoleValidation() {
	var roles=document.getElementById("roles").value;
	if( isEmpty(roles) ) {
	    
	     document.getElementById("roleerror").innerHTML="roles is Empty";
	     return false;
	  } else {
		  document.getElementById("roleerror").innerHTML="";  
		  return true;
	  }
}

function FileValidation(){
	var file=document.getElementById("scannedforms").value;
	var error=null;
	
	if( isEmpty(file) ) {
	   error="upload file";
	    
	  } else {
		
	 var Extension = file.substring(file.lastIndexOf('.') + 1).toLowerCase();
	 console.log("Extension "+Extension);
	 if (Extension == "pdf") {
		
	 }else {
		 error="Upload pdf file";
	 }
	  }
	if(error!=null){
		 document.getElementById("formuploaderror").innerHTML=error;
		 return false;
	}else {
		 document.getElementById("formuploaderror").innerHTML="";
		 return true;
	}
}

function addressValidation(){
	var address =/^[a-zA-Z0-9\s,.'-]{3,100}$/ ;
	var plotvalidationerror=null;
	var areavalidationerror=null;
	var streetvalidationerror=null;
	var landmarkvalidationerror=null;
	var address1=document.getElementById("address1").value;
	if(isEmpty(address1)) {
	
		plotvalidationerror="Plot no is Empty";
	} else
	if(!address1.match(address)) {
	
		plotvalidationerror="Address should be alphanumeric having length between 3 and 100";
	}
	var address2=document.getElementById("address2").value;
	if(isEmpty(address2)) {
		
		areavalidationerror="Area/Locality field is Empty";
	} else
	if(!address2.match(address)) {
		
		areavalidationerror="Area/Locality should be alphanumeric having length between 3 and 100";
	}
	var address3=document.getElementById("address3").value;
	if(isEmpty(address3)) {
		
		streetvalidationerror="Street field is Empty";
	} else
	if(!address3.match(address)) {
	
		streetvalidationerror="Street should be alphanumeric having length between 3 and 100";
	}
	var landmark=document.getElementById("landmark").value;
	if(isEmpty(landmark)) {
		landmarkvalidationerror="Street field is Empty";
	}else if(!landmark.match(address)) {
		
			landmarkvalidationerror="landmark should be alphanumeric having length between 3 and 100";
		}
	
	
	
	
	if(plotvalidationerror!=null){
		 document.getElementById("address1error").innerHTML=plotvalidationerror;
		 
	}else {
		 document.getElementById("address1error").innerHTML="";
		
	}
	if(areavalidationerror!=null){
		 document.getElementById("address2error").innerHTML=areavalidationerror;
		
	}else {
		 document.getElementById("address2error").innerHTML="";
		
	}if(streetvalidationerror!=null){
		 document.getElementById("address3error").innerHTML=streetvalidationerror;
		
	}else {
		 document.getElementById("address3error").innerHTML="";
		
	}if(landmarkvalidationerror!=null){
		 document.getElementById("landmarkerror").innerHTML=landmarkvalidationerror;
		
	}else {
		 document.getElementById("landmarkerror").innerHTML="";
		
	}
	if(plotvalidationerror==null && areavalidationerror==null && streetvalidationerror==null && landmarkvalidationerror==null) 
		{
		return true;
		}
	return false;
}

function Validation() {
	var city=document.getElementById("city").value;
	var district=document.getElementById("district").value;
	var state=document.getElementById("state").value;
	var country=document.getElementById("country").value;
	var letters = /^[A-Za-z]{1,50}$/;
	var cityerror=null;
	var districterror=null;
	var stateerror=null;
	var countryerror=null;
	if( isEmpty(city) ) {
		cityerror="City Field is Empty";
	  }
	else if(!city.match(letters)){
		cityerror="City field should contain only alphabets between 1 to 50";
	}
	if( isEmpty(district) ) {
		districterror="District Field is Empty";
	  }
	else if(!district.match(letters)){
		districterror="District field should contain only alphabets between 1 to 50";
	}
	if( isEmpty(state) ) {
		stateerror="State Field is Empty";
	  }
	else if(!state.match(letters)){
		stateerror="State field should contain only alphabets between 1 to 50";
	}
	if( isEmpty(country) ) {
		countryerror="Country Field is Empty";
	  }
	else if(!country.match(letters)){
		countryerror="Country field should contain only alphabets between 1 to 50";
	}
	if(cityerror!=null) {
		 document.getElementById("cityerror").innerHTML=cityerror; 
	}else {
		document.getElementById("cityerror").innerHTML="";
		
	}
	if(districterror!=null) {
		 document.getElementById("districterror").innerHTML=districterror; 
	}else {
		document.getElementById("districterror").innerHTML="";
		
	}
	if(stateerror!=null) {
		 document.getElementById("stateerror").innerHTML=stateerror; 
	}else {
		document.getElementById("stateerror").innerHTML="";
		
	}
	if(countryerror!=null) {
		 document.getElementById("countryerror").innerHTML=countryerror; 
	}else {
		document.getElementById("countryerror").innerHTML="";
		
	}
	
	if(cityerror!=null && districterror!=null && stateerror!=null && countryerror!=null)
		{
		return false;
		}
	else {
		return true;
	}
}
function MobileNoValidation(){
	var mobileno = document.getElementById("mobileno").value;
	/*var pattern = /^(\+\d{1,3}[- ]?)?\d{10}$/;*/
	var pattern = /^[0]?[789]\d{9}$/;
	
	 var error=null;  
	if( isEmpty(mobileno) ) {
	    error="mobileno field is Empty";
	  }else if(!pattern.test(mobileno)) {
		error="mobileno be of 10 digits ";
		
	  }
	if(error!=null) {
		 document.getElementById("mobilenoerror").innerHTML=error;
		 return false;
		 
	}else {
		document.getElementById("mobilenoerror").innerHTML="";
		 return true;
	}
}

function FaxValidation(){
	var fax = document.getElementById("fax").value;
	var pattern = /^\\+[0-9]{1,3}-[0-9]{3}-[0-9]{7}$/;
	
	 var error=null;  
	if( isEmpty(fax) ) {
	    error="fax field is Empty";
	  }else if(pattern.test(fax)) {
		error="Fax should contain +countrycode-areacode-fax pattern";
		
	  }
	if(error!=null) {
		 document.getElementById("faxerror").innerHTML=error;
		 return false;
		 
	}else {
		document.getElementById("faxerror").innerHTML="";
		 return true;
	}
}

function EmailValidation(){
	var email = document.getElementById("emailid").value;

	 var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	 var error=null; 
	
	if( isEmpty(email) ) {
	    error="email field is Empty";
	  }else if(!pattern.test(email)) {
		error="Invaild email pattern ";
		
	  }
	 
	if(error!=null) {
		 document.getElementById("emailiderror").innerHTML=error;
		 return false;
		 
	}else {
		document.getElementById("emailiderror").innerHTML="";
		 return true;
	}
}
function WebsiteValidation(){
	var website = document.getElementById("website").value;
var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ //port
        '(\\?[;&amp;a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i');
	
	 var error=null;  
	if( isEmpty(website) ) {
		
	    error="website field is Empty";
	  }else if(!pattern.test(website)) {
		
		error="Invaild website pattern ";
		
	  }
	
	if(error!=null) {
		 document.getElementById("websiteerror").innerHTML=error;
		 return false;
		 
	}else {
		document.getElementById("websiteerror").innerHTML="";
		 return true;
	}
}
function pincodeValidation(){
	/*var pattern="/^[0-9]{6}$/";*/
	var pattern=/^\d{6}$/;
	var pincode = document.getElementById("pincode").value;
	 var error=null;  
		if( isEmpty(pincode) ) {
		    error="pincode field is Empty";
		  }else if(!pattern.test(pincode)) {
			error="Invaild valid pincode pattern ";
			
		  }
		if(error!=null) {
			 document.getElementById("pincodeerror").innerHTML=error;
			 return false;
			 
		}else {
			document.getElementById("pincodeerror").innerHTML="";
			 return true;
		}
}
function telephoneValidation(){
	console.log("telephoneValidation");
	/*var pattern=/^(?!0+$)\\d{8,15}$/;*/
	 const pattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	var telephone = document.getElementById("telephone").value;
	 var error=null;  
		if( isEmpty(telephone) ) {
		    error="telephone field is Empty";
		  }else if(!pattern.test(telephone)) {
			error="Enter telephone number with STD code and should have 10 digit number";
			
		  }
		if(error!=null) {
			 document.getElementById("telephoneerror").innerHTML=error;
			 return false;
			 
		}else {
			document.getElementById("telephoneerror").innerHTML="";
			 return true;
		}
		/*https://stackoverflow.com/a/18376010 telephone number validation
		 * title="Enter Phone number with STD code with Min '8' and Max '15'
		 * digit number"
		 */
}
/*
 * function mobileNoValiadtion(){ var pattern="/[789][0-9]{9}/" title="Enter a
 * valid mobile number with Min '10' and Max '13' digit number"; var mobileno =
 * document.getElementById("mobileno").value; var error=null; if(
 * isEmpty(mobileno) ) { error="mobileno field is Empty"; }else
 * if(pattern.test(pincode)) { error="Enter a valid mobile number with Min '10'
 * and Max '13' digit number";
 *  } if(error!=null) {
 * document.getElementById("mobilenoerror").innerHTML=error; return false;
 * 
 * }else { document.getElementById("mobilenoerror").innerHTML=""; return true; } }
 * 
 */




function validatePortalUser(form) {
	console.log("Inside validatePortalUser "+form.id);
	var uservalidationflag= userDetailsValidation(form.id);
		
		console.info(uservalidationflag);
		alert("validation flag "+uservalidationflag);
		if(uservalidationflag==true) {
	 return true;
		}
		else {
			
			 document.getElementById("formerror").innerHTML="Validation failed kindly check all tabs ";
			return false;
		}
}








/*
 * $("#addressdetailstab").tab('show');
 * 
 * 
 * var num_tabs = $('div.nav-tabs-custom ul li').length + 1; alert(num_tabs);
 * 
 * $('div.nav-tabs-custom ul').append( '<li><a href="#tab_' + num_tabs + '"
 * data-toggle="tab">Shipping Company Details</a></li>' );
 * 
 * $('div.nav-tabs-custom').tabs("refresh"); }
 */
