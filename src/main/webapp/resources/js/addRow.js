/**
 * 
 */
function addRow(tableID) {
	var table = document.getElementById(tableID);
//	alert("length " + document.getElementById('csolength').value);
	var rowLength = table.rows.length;
	var rowCount = document.getElementById('csolength').value;
	var length = document.getElementById('csolength').value;
	var row = table.insertRow(rowLength);
	var counts = rowCount;
	
	
	var cell1 = row.insertCell(0);
	var name = document.createElement("input");
	name.type = "text";
	name.name = "currentcsoList[" + counts + "].name";
	cell1.appendChild(name);
	// path="currentcsoList[${status.index}].telephoneOffice"
	var cell2 = row.insertCell(1);
	var telephoneOffice = document.createElement("input");
	telephoneOffice.type = "text";
	telephoneOffice.id="csotelephoneoffice";
	telephoneOffice.name = "currentcsoList[" + counts + "].telephoneOffice";
	cell2.appendChild(telephoneOffice);

	var cell3 = row.insertCell(2);
	var telephoneResidence = document.createElement("input");
	telephoneResidence.type = "text";
	telephoneResidence.name = "currentcsoList[" + counts
			+ "].telephoneResidence";
	cell3.appendChild(telephoneResidence);

	var cell4 = row.insertCell(3);
	var mobileNo = document.createElement("input");
	mobileNo.type = "text";
	mobileNo.name = "currentcsoList[" + counts + "].mobileNo";
	mobileNo.setAttribute("required", "");

	cell4.appendChild(mobileNo);

	var cell5 = row.insertCell(4);
	var emailid = document.createElement("input");
	emailid.type = "text";
	emailid.name = "currentcsoList[" + counts + "].emailid";
	emailid.setAttribute("required", "");

	cell5.appendChild(emailid);
	var cell6 = row.insertCell(5);
	var remove = document.createElement("input");
	remove.id = "csoremove_edituser";
	remove.type = "button";
	remove.value = "Remove";
	remove.setAttribute("onclick", "Remove(this)");
	remove.setAttribute('class', 'btn btn-primary  btn-flat');
	var size = document.createElement("input");
	size.setAttribute("type", "hidden");
	size.setAttribute("value", length);
	size.id = "csosize";
	cell6.appendChild(remove);
	cell6.appendChild(size);

	var cell7 = row.insertCell(6);
	var flag = document.createElement("input");
	flag.setAttribute("type", "hidden");
	flag.setAttribute("value", "new");
/*	 flag.type = "text"; */
	flag.id = "currentcsoList" + counts + ".flag";
	flag.name = "currentcsoList[" + counts + "].flag";
	cell7.appendChild(flag);
	
	
	var cell8 = row.insertCell(7);
	var type = document.createElement("input");
	type.setAttribute("type", "hidden");
	type.setAttribute("value", "cso");
/*	 flag.type = "text"; */
	type.id = "currentcsoList" + counts + ".type";
	type.name = "currentcsoList[" + counts + "].type";
	cell8.appendChild(type);
	
	length++;
	document.getElementById('csolength').value = length;
}


function addRowDpaTable(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	var dpaLength = document.getElementById('dpalength').value;
	var length = document.getElementById('dpalength').value;
	var row = table.insertRow(rowCount);
	var counts = rowCount - 1;
	/*
	 * var cell = row.insertCell(0); var type = document.createTextNode('Current
	 * CSO'); var text = 'Current CSO'; cell.appendChild(type);
	 */
	var cell1 = row.insertCell(0);
	var name = document.createElement("input");
	name.type = "text";
	/* name.name = "dpaList[" + counts + "].name"; */
	name.name = "dpaList[" + dpaLength + "].name";
	cell1.appendChild(name);

	var cell2 = row.insertCell(1);
	var telephoneOffice = document.createElement("input");
	telephoneOffice.type = "text";
	/* telephoneOffice.name = "dpaList[" + counts + "].telephoneOffice"; */
	telephoneOffice.name = "dpaList[" + dpaLength + "].telephoneOffice";
	cell2.appendChild(telephoneOffice);

	var cell3 = row.insertCell(2);
	var telephoneResidence = document.createElement("input");
	telephoneResidence.type = "text";
	/* telephoneResidence.name = "dpaList[" + counts + "].telephoneResidence"; */
	telephoneResidence.name = "dpaList[" + dpaLength + "].telephoneResidence";
	cell3.appendChild(telephoneResidence);

	var cell4 = row.insertCell(3);
	var mobileNo = document.createElement("input");
	mobileNo.type = "text";
	/* mobileNo.name = "dpaList[" + counts + "].mobileNo"; */
	mobileNo.name = "dpaList[" + dpaLength + "].mobileNo";
	mobileNo.setAttribute("required", "");

	cell4.appendChild(mobileNo);

	var cell5 = row.insertCell(4);
	var emailid = document.createElement("input");
	emailid.type = "text";
	/* emailid.name = "dpaList[" + counts + "].emailid"; */
	emailid.name = "dpaList[" + dpaLength + "].emailid";
	emailid.setAttribute("required", "");
	cell5.appendChild(emailid);
	var cell6 = row.insertCell(5);
	var remove = document.createElement("input");
	remove.id = "dparemove_edituser";
	remove.type = "button";
	remove.value = "Remove";
	remove.setAttribute("onclick", "Remove(this)");
	remove.setAttribute('class', 'btn btn-primary  btn-flat');
	var size = document.createElement("input");
	size.setAttribute("type", "hidden");
	size.setAttribute("value", length);
	size.id = "dpasize";
	cell6.appendChild(remove);
	cell6.appendChild(size);

	var cell7 = row.insertCell(6);
	var flag = document.createElement("input");
	flag.setAttribute("type", "hidden");
	flag.setAttribute("value", "new");
	/* flag.type = "text"; */
	flag.id = "dpaList" + dpaLength + ".flag";
	/* flag.name = "dpaList[" + counts + "].flag"; */
	flag.name = "dpaList[" + dpaLength + "].flag";
	cell7.appendChild(flag);
	
	var cell8 = row.insertCell(7);
	var type = document.createElement("input");
	type.setAttribute("type", "hidden");
	type.setAttribute("value", "dpa");
/*	 flag.type = "text"; */
	type.id = "dpaList" + counts + ".type";
	type.name = "dpaList[" + counts + "].type";
	cell8.appendChild(type);
	
	
	length++;
	document.getElementById('dpalength').value = length;

}

function update(obj) {
//	alert("Inside update"+obj);
	var text = null;
	var row = obj.parentNode.parentNode;
	var getParentNode = obj.parentNode.parentNode;
	//alert("getParentNode " + getParentNode);
	 var text=getParentNode.getElementsByTagName("td")[5]; 
	//	alert("text " + text.innerHTML);
	var value = text.getElementsByTagName("input")[1].value;
	//alert("value " + value);
	var type_element= "currentcsoList" + value + ".type";
	//alert("type_element "+type_element);
	var type=document.getElementById(type_element).value ;
//	alert("type "+type);
	if(type == 'cso') {
	var element = "currentcsoList" + value + ".flag";
//	alert("cso*** "+element);
	document.getElementById(element).value = 'update';
	var chk = document.getElementById(element).value;
//	alert("chk " + chk);
	} else if (type== 'dpa') {
		
		var element = "dpaList" + value + ".flag";
	//	alert("dpa*** "+element);
		document.getElementById(element).value = 'update';
		var chk = document.getElementById(element).value;
	//	alert("chk1111111111 " + chk);
		
		
	}




}
function updatedpa(obj) {
	//alert("Inside update"+obj);
	var text = null;
	var row = obj.parentNode.parentNode;
	var getParentNode = obj.parentNode.parentNode;
	//alert("getParentNode " + getParentNode);
	 var text=getParentNode.getElementsByTagName("td")[5]; 
	//	alert("text " + text.innerHTML);
	var value = text.getElementsByTagName("input")[1].value;
	//alert("value " + value);
	var type_element= "dpaList" + value + ".type";
	//alert("type_element "+type_element);
	var type=document.getElementById(type_element).value ;
//	alert("type "+type);
	if(type == 'cso') {
	var element = "currentcsoList" + value + ".flag";
//	alert("cso*** "+element);
	document.getElementById(element).value = 'update';
	var chk = document.getElementById(element).value;
//	alert("chk " + chk);
	} else if (type== 'dpa') {
		
		var element = "dpaList" + value + ".flag";
	//	alert("dpa*** "+element);
		document.getElementById(element).value = 'update';
		var chk = document.getElementById(element).value;
	//	alert("chk1111111111 " + chk);
		
		
	}




}

function Remove (button) {
	//alert("Id "+button.id);
	var buttonId=button.id
	var table = null;
	var rowLengthcso = 0;
	var rowLengthdpa = 0;	
	var getParentNode = button.parentNode;
	var row = button.parentNode.parentNode;
	//alert("getParentNode " +getParentNode);
	if(buttonId=='Csoremove_createuser'){
		
		//alert("Inside csoremove_createuser");
		var count = row.rowIndex;
		//alert("count "+count);
		table = document.getElementById("csoTable");
		// Delete the Table row using it's Index.
		table.deleteRow(count);
	}else if(buttonId=='csoremove_edituser') {
		//alert("Inside csoremove_edituser");
		var count = row.rowIndex;
	//	alert("count "+count);
		table = document.getElementById("csoTable");
		// Delete the Table row using it's Index.
		table.deleteRow(count);
		var csolength=document.getElementById("csolength").value;
		var csolen=csolength-1;
		document.getElementById("csolength").value=csolen;
	}else if(buttonId=='Dparemove_createuser') {
		//alert("Inside Dparemove_createuser");
		var count = row.rowIndex;
		//alert("count "+count);
		table = document.getElementById("dpaTable");
		// Delete the Table row using it's Index.
		table.deleteRow(count);
	}else if(buttonId=='dparemove_edituser') {
		//alert("Inside dparemove_edituser");
		var count = row.rowIndex;
	//alert("count "+count);
		table = document.getElementById("dpaTable");
		// Delete the Table row using it's Index.
		table.deleteRow(count);
		var dpalength=document.getElementById("dpalength").value;
		var dpalen=dpalength-1;
		document.getElementById("dpalength").value=dpalen;
	}
	
}
