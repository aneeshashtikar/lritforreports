/**
 * 
 */
$(document).ready(function() {
	
	var _imoVesselType = $('#imoVesselType').val();
	
	if(_imoVesselType=='0100')
		$('#imoVesselType').val('Passenger');
	else if(_imoVesselType=='0200')
		$('#imoVesselType').val('Cargo');
	else if(_imoVesselType=='0300')
		$('#imoVesselType').val('Tanker');
	else if(_imoVesselType=='0400')
		$('#imoVesselType').val('Mobile offshore drilling unit');
	else if(_imoVesselType=='9900')
		$('#imoVesselType').val('Other');
	else
		$('#imoVesselType').val('');
	
});
