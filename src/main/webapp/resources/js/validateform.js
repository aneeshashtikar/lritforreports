/**
 * 
 */

var numPattern = /^[0-9s]+$/;

var numAlphaPattern = /^[0-9a-zA-Z \-'_]+$/;

var alphaPattern = /^[a-zA-Z]+$/;

//var decimalPattern = /^[-+]?[0-9]+\.[0-9]+$/;

function validateForm() { 

	//alert("inside validate form");

	var arr = [];
	
	arr[0] = validateIMO();
	arr[1] = validateMMSI();
	arr[2] = validateVesselName();
	arr[3] = validateCallSign();
	arr[4] = validateYOB();
	arr[5] = validateGrossTonnane();
	arr[6] = validateDeadWeight();
	arr[7] = validateRegistrationNo();
	//arr[8] = validateTimeDelay();
	arr[8] = validateImoVesselType();
	arr[9] = validateVesselType();
	//arr[11] = validateVesselCategory();
	//arr[10] = validateVesselGroup();
	
	arr[10] = validateDoc1();
	arr[11] = validateDoc2();
	arr[12] = validateDoc3();
	arr[13] = validateDoc4();
	
	arr[14] = validateCompanyCode1();
	arr[15] = validateUserId1();
	arr[16] = validateCSOId1();
	arr[17] = validateACSOId1();
	arr[18] = validateDPAId1();
	arr[19] = validateCompanyCode2();
	arr[20] = validateUserId2();
	arr[21] = validateCSOId2();
	arr[22] = validateACSOId2();
	arr[23] = validateDPAId2();
	arr[24] = validateCompanyCode3();
	arr[25] = validateUserId3();
	arr[26] = validateCSOId3();
	arr[27] = validateACSOId3();
	arr[28] = validateDPAId3();

	arr[29] = validateShipEquipId();
	arr[30] = validateManufacturerName();
	arr[31] = validateModelType();
	arr[32] = validateModelName();
	arr[33] = validateOceanRegion();

	arr[34] = validatePIClub();
	arr[35] = validateVesselImage();

	arr[36] = validateDoc5();
	
	/*alert(arr.length);*/
	
	for(var i = 0 ; i < arr.length ; i++)
	{
		/*alert(arr[i]);*/
		if(!arr[i])
			{
				document.getElementById("formerror").innerHTML = "Validation failed kindly check all tabs ";
				/*$('#showError').append(
						'<div class="alert alert-danger colose" data-dismiss="alert">'+
						'<a href="#" class="close" data-dismiss="alert"'+
							'aria-label="close">&times;</a> Validation Failed! Kindly check the Form again. </div>'	
				);*/
				return false;
			}
	}
	
	return true;

}


function approvedForm() { 

	var arr = [];
	
	arr[0] = validateTimeDelay();
	arr[1] = validateImoVesselType();
	arr[2] = validateVesselType();
	//arr[3] = validateVesselGroup();
	arr[3] = validateDNIDNo();
	arr[4] = validateMemberNo();
	arr[5] = validateRegStatus();
	
	//arr[7] = validateVesselCategory();
	/*arr[2] = validateSEIDStatus();*/
	//arr[3] = validateRegStatus();
	
	for(var i = 0 ; i < arr.length ; i++)
	{
		/*alert(arr[i]);*/
		if(!arr[i]){
			$('#showError').append(
					'<div class="alert alert-danger colose" data-dismiss="alert">'+
					'<a href="#" class="close" data-dismiss="alert"'+
						'aria-label="close">&times;</a> Validation Failed! Kindly check the Form again. </div>'	
			);		
			return false;
		}
			
	}
	
	return true;
	
}

function editForm()
{
var arr = [];
	
	arr[0] = validateVesselName();
	arr[1] = validateCallSign();
	//arr[2] = validateVesselGroup();
	
	for(var i = 0 ; i < arr.length ; i++)
	{
		/*alert(arr[i]);*/
		if(!arr[i]){
			$('#showError').append(
					'<div class="alert alert-danger colose" data-dismiss="alert">'+
					'<a href="#" class="close" data-dismiss="alert"'+
						'aria-label="close">&times;</a> Validation Failed! Kindly check the Form again. </div>'	
			);	
			return false;
		}
	}
	
	return true;
}

function validateApproveSEIDForm()
{
	var arr = [];
	
	arr[0] = validateDNIDNoSEID();
	arr[1] = validateMemberNoSEID();
		
	for(var i = 0 ; i < arr.length ; i++)
	{
		//alert(arr[i]);
		if(!arr[i]){	
			return false;
		}
	}
	
	return true;
}

//validation for imo number

function validateIMO() {	

	var _imoNo = $('#imoNo').val();

	if ((_imoNo == "") || (!numPattern.test(_imoNo)) || (_imoNo.length != 7)) {
		$('#imoError').text("IMO No should be 7 digit mandatory field");
		return false;
	}
	else {
		$('#imoError').text("");
		return true;
	}
}

//validation for mmsi number

function validateMMSI() {

	var _mmsiNo = $('#mmsiNo').val();

	if ((_mmsiNo == "") || (!numPattern.test(_mmsiNo)) || (_mmsiNo.length != 9)) {
		$('#mmsiError').text("MMSI No should be 9 digit mandatory field");
		return false;
	} 
	else {
		$('#mmsiError').text("");
		return true;
	}

}

//validation for ship name

function validateVesselName() {

	var _vesselName = $('#vesselName').val();

	if ((_vesselName == "")|| (!numAlphaPattern.test(_vesselName)) || (_vesselName.length > 50)) {
		$('#vesselNameError').text("Vessel Name should be of maximam 50 characters long alphanumeric mandatory field");
		return false;
	} 
	else {
		$('#vesselNameError').text("");
		return true;
	}

}

//validation for call Sign

function validateCallSign() {

	var _callSign = $('#callSign').val();

	if(((!numAlphaPattern.test(_callSign)) || (_callSign.length > 20)) && (_callSign != "")) {
		$('#callSignError').text("Call Sign should be of maximam 20 characters long alphanumeric field");
		return false;
	}
	else{
		$('#callSignError').text("");
		return true;
	}
}

//validation for year of built

function validateYOB() {

	var _yearOfBuilt = $('#yearOfBuilt').val();
	
	var d = new Date();
	var n = d.getFullYear();
	
	if ((_yearOfBuilt == "") || (!numPattern.test(_yearOfBuilt)) || (_yearOfBuilt.length != 4 ) || (_yearOfBuilt > n )) {
		$('#yearOfBuiltError').text("Year of built should be 4 digit mandatory field cannot exceed current year");
		return false;
	} 
	else {
		$('#yearOfBuiltError').text("");
		return true;
	}

}


//validation for gross tonnage

function validateGrossTonnane() {

	var _grossTonnage = $('#grossTonnage').val();

	if ((_grossTonnage == "") || (!numPattern.test(_grossTonnage)) || (_grossTonnage.length > 10)) {
		$('#grossTonnageError').text("Gross Tonnage should be of maximam 10 digit mandatory field");
		return false;
	} 
	else {
		$('#grossTonnageError').text("");
		return true;
	}

}

//validation for dead weight

function validateDeadWeight() {

	var _deadWeight = $('#deadWeight').val();

	if ((_deadWeight == "") || (!numPattern.test(_deadWeight)) || (_deadWeight.length > 7)) {
		$('#deadWeightError').text("Dead Weight should be of maximam 7 digit mandatory field");
		return false;
	} 
	else {
		$('#deadWeightError').text("");
		return true;
	}

}


//validation for registration no

function validateRegistrationNo() {

	var _registrationNo = $('#registrationNo').val();

	if ((_registrationNo == "") || (!numPattern.test(_registrationNo)) || (_registrationNo.length > 10)) {
		$('#registrationNoError').text("Registration No should be of maximam 10 digit mandatory field");
		return false;
	}
	else {
		$('#registrationNoError').text("");
		return true;
	}

}


//validation for time delay

function validateTimeDelay() {

	var _timeDelay = $('#timeDelay').val();

	if ((_timeDelay == "") || (!numPattern.test(_timeDelay)) || (_timeDelay.length > 4) || (_timeDelay > 1440) || (_timeDelay < 0)) {
		$('#timeDelayError').text("Time Delay should be of maximam 4 digit mandatory field from 0 to 1440");
		return false;
	} 
	else {
		$('#timeDelayError').text("");
		return true;
	}

}

//validation for imo vessel type

function validateImoVesselType() {

	var _imoVesselType = $('#imoVesselType').val();

	if (_imoVesselType == "") {
		$('#imoVesselTypeError').text("IMO Vessel Type is mandatory");
		return false;
	}
	else {
		$('#imoVesselTypeError').text("");
		return true;
	}

}

//validation for vessel type

function validateVesselType() {

	var _vesselType = $('#vesselType').val();

	if (_vesselType == "") {
		$('#vesselTypeError').text("Vessel Type is mandatory");
		return false;
	}
	else {
		$('#vesselTypeError').text("");
		return true;
	}

}

//validation for vessel category

function validateVesselCategory() {

	var _vesselCategory = $('#vesselCategory').val();

	if (_vesselCategory == "") {
		$('#vesselCategoryError').text("Vessel Category is mandatory");
		return false;
	}
	else {
		$('#vesselCategoryError').text("");
		return true;
	}

}

//validation for vessel group

function validateVesselGroup() {

	var _vesselGroup = $('#vesselGroup').val();

	if (_vesselGroup == "") {
		$('#vesselGroupError').text("Vessel Group is mandatory");
		return false;
	}
	else {
		$('#vesselGroupError').text("");
		return true;
	}

}


//validation for pi Club

function validatePIClub() {

	var _piClub = $('#piClub').val();

	if((_piClub != "") && ((!numAlphaPattern.test(_piClub)) || (_piClub.length > 50))) {
		$('#piClubError').text("PI Club should be of maximam 50 characters long alphanumeric field");
		return false;
	}
	else {
		$('#piClubError').text("");
		return true;
	}

}

//validation for vessel image

function validateVesselImage() {

	var _vesselImage = document.getElementById('vesselImage');

	if((_vesselImage.value != "") && ((_vesselImage.files[0].size < 2097153) && ((_vesselImage.value.endsWith(".png")) || (_vesselImage.value.endsWith(".PNG")) || (_vesselImage.value.endsWith(".jpg")) || (_vesselImage.value.endsWith(".JPG")) || (_vesselImage.value.endsWith(".jpeg")) || (_vesselImage.value.endsWith(".JPEG"))))) {
		$('#vesselImageError').text("");
		return true;
	} 
	else if(_vesselImage.value == "")
	{
		$('#vesselImageError').text("");
		return true;
	}
	else {	
		$('#vesselImageError').text("Only jpg or png accepted along with size should not exceed 2MB");
		return false;
	}

}

//validation for ship equip id

function validateShipEquipId() {

	var _shipborneEquipmentId = $('#shipborneEquipmentId').val();

	if ((_shipborneEquipmentId == "") || (!numPattern.test(_shipborneEquipmentId)) || (_shipborneEquipmentId.length != 9)) {
		$('#shipborneEquipmentIdError').text("Ship Equipment Id should be 9 digit mandatory field");
		return false;
	}
	else {
		$('#shipborneEquipmentIdError').text("");
		return true;
	}

}


//validation for manufacturer Name

function validateManufacturerName() {

	var _manufacturerName = $('#manufacturerName').val();

	if (_manufacturerName == "") {
		$('#manufacturerNameError').text("ManufacturerName is mandatory");
		return false;
	}
	else {
		$('#manufacturerNameError').text("");
		return true;
	}

}

//validation for model type

function validateModelType() {

	var _modelType = $('#modelType').val();

	if (_modelType == "") {
		$('#modelTypeError').text("Model Type is mandatory");
		return false;
	}
	else {
		$('#modelTypeError').text("");
		return true;
	}

}

//validation for model name

function validateModelName() {

	var _modelName = $('#modelName').val();

	if (_modelName == "") {
		$('#modelNameError').text("Model Name is mandatory");
		return false;
	}
	else {
		$('#modelNameError').text("");
		return true;
	}

}


//validation for add seid for dnid No

function validateDNIDNoSEID() {

	var _dnidNo = $('#dnidNo').val();

	if(((_dnidNo != "") && ((!numPattern.test(_dnidNo)) || (_dnidNo.length != 5))) || (_dnidNo == "")) {
		$('#dnidNoError').text("DNID No should be 5 digit mandatory field");
		return false;
	}
	else {
		$('#dnidNoError').text("");
		return true;
	}

}

//validation for approve registration for dnid No

function validateDNIDNo() {

	var _dnidNo = $('#dnidNo').val();
	
	var _regStatus = $('#regStatus').val();
	
	if(_regStatus == "APPROVE")
	{
		if(((_dnidNo != "") && ((!numPattern.test(_dnidNo)) || (_dnidNo.length != 5))) || (_dnidNo == "")) {
			$('#dnidNoError').text("DNID No should be 5 digit mandatory field");
			return false;
		}
		else {
			$('#dnidNoError').text("");
			return true;
		}
	}
	else
	{
		if((_dnidNo != "") && ((!numPattern.test(_dnidNo)) || (_dnidNo.length != 5))) {
			$('#dnidNoError').text("DNID No should be 5 digit mandatory field");
			return false;
		}
		else {
			$('#dnidNoError').text("");
			return true;
		}
	}
	

}


//validation for add seid for member No

function validateMemberNoSEID() {

	var _memberNo = $('#memberNo').val();

	if(((_memberNo != "") && ((!numPattern.test(_memberNo)) || (_memberNo.length > 3) || (_memberNo < 100) || (_memberNo > 999))) || (_memberNo == "")) {
		$('#memberNoError').text("Member No should be 3 digit mandatory field");
		return false;
	}
	else {
		$('#memberNoError').text("");
		return true;
	}

}

//validation for approve registration for member No

function validateMemberNo() {

	var _memberNo = $('#memberNo').val();
	
	var _regStatus = $('#regStatus').val();
	
	if(_regStatus == "APPROVE")
	{
		if(((_memberNo != "") && ((!numPattern.test(_memberNo)) || (_memberNo.length > 3) || (_memberNo < 100) || (_memberNo > 999))) || (_memberNo == "")) {
			$('#memberNoError').text("Member No should be 3 digit mandatory field");
			return false;
		}
		else {
			$('#memberNoError').text("");
			return true;
		}
	}
	else
	{
		if((_memberNo != "") && ((!numPattern.test(_memberNo)) || (_memberNo.length > 3) || (_memberNo < 100) || (_memberNo > 999))) {
			$('#memberNoError').text("Member No should be 3 digit mandatory field");
			return false;
		}
		else {
			$('#memberNoError').text("");
			return true;
		}
	}

}


//validation for ocean region

function validateOceanRegion() {

	var _oceanRegion = $('#oceanRegion').val();

	if (_oceanRegion == "") {
		$('#oceanRegionError').text("Ocean Region is mandatory");
		return false;
	}
	else {
		$('#oceanRegionError').text("");
		return true;
	}

}

//validation for seid status for add seid

function validateSEIDStatusSEID() {

	var _seidStatus= $('#seidStatus').is(':checked');
	
	if (_seidStatus == false) {
		$('#seidStatusError').text("SEID Status is mandatory");
		return false;
	}
	else {
		$('#seidStatusError').text("");
		return true;
	}
}


//validation for seid status

function validateSEIDStatus() {

	var _seidStatus= $('#seidStatus').is(':checked');

	var _regStatus = $('#regStatus').val();
	
	if(_regStatus == 'APPROVE')
	{
		if (_seidStatus == false) {
			$('#seidStatusError').text("SEID Status is mandatory");
			return false;
		}
		else {
			$('#seidStatusError').text("");
			return true;
		}
	}
	else
	{
		$('#seidStatusError').text("");
		return true;
	}
}


//validation for company code

function validateCompanyCode1() {

	var _companyCode1 = $('#companyCode1').val();

	if (_companyCode1 == "") {
		$('#companyCode1Error').text("Company Code is mandatory");
		return false;
	}
	else {
		$('#companyCode1Error').text("");
		return true;
	}

}

function validateUserId1() {

	var _userId1 = $('#userId1').val();
	
	if (_userId1 == "") {
		$('#userId1Error').text("Login Id is mandatory");
		return false;
	}
	else {
		$('#userId1Error').text("");
		return true;
	}

}	

function validateCSOId1() {

	var _csoId1 = $('#csoId1').val();
	
	if (_csoId1 == "") {
		$('#csoId1Error').text("CSO Id is mandatory");
		return false;
	}
	else {
		$('#csoId1Error').text("");
		return true;
	}

}

function validateACSOId1() {

	var _acsoId1 = $('#acsoId1').val();
	
	if (_acsoId1 == "") {
		$('#acsoId1Error').text("ACSO Id is mandatory");
		return false;
	}
	else {
		$('#acsoId1Error').text("");
		return true;
	}

}

function validateDPAId1() {

	var _dpaId1 = $('#dpaId1').val();
	
	if (_dpaId1 == "") {
		$('#dpaId1Error').text("DPA Id is mandatory");
		return false;
	}
	else {
		$('#dpaId1Error').text("");
		return true;
	}

}

function validateCompanyCode2() {

	var _companyCode2 = $('#companyCode2').val();
	
	if (_companyCode2 == "") {
		$('#companyCode2Error').text("Company Code is mandatory");
		return false;
	}
	else {
		$('#companyCode2Error').text("");
		return true;
	}

}

function validateUserId2() {

	var _userId2 = $('#userId2').val();
	
	if (_userId2 == "") {
		$('#userId2Error').text("Login Id is mandatory");
		return false;
	}
	else {
		$('#userId2Error').text("");
		return true;
	}

}

function validateCSOId2() {

	var _companyCode1 = $('#companyCode1').val();
	var _companyCode2 = $('#companyCode2').val();

	var _userId1 = $('#userId1').val();
	var _userId2 = $('#userId2').val();

	var _csoId1 = $('#csoId1').val();
	var _csoId2 = $('#csoId2').val();
	
	if (_csoId2 == "") {
		$('#csoId2Error').text("CSO Id is mandatory");
		return false;
	}
	else {
		
		if(_companyCode1 == _companyCode2)
		{
			if(_userId1 == _userId2)
			{
				if(_csoId1 != _csoId2)
				{
					$('#csoId2Error').text("Please select same cso as for owner");
					return false;
				}
					
			}
		}
		
		$('#csoId2Error').text("");
		return true;
	}

}

function validateACSOId2() {

	var _acsoId2 = $('#acsoId2').val();
	
	if (_acsoId2 == "") {
		$('#acsoId2Error').text("ACSO Id is mandatory");
		return false;
	}
	else {
		$('#acsoId2Error').text("");
		return true;
	}

}

function validateDPAId2() {

	var _companyCode1 = $('#companyCode1').val();
	var _companyCode2 = $('#companyCode2').val();

	var _userId1 = $('#userId1').val();
	var _userId2 = $('#userId2').val();

	var _csoId1 = $('#csoId1').val();
	var _csoId2 = $('#csoId2').val();

	var _dpaId1 = $('#dpaId1').val();
	var _dpaId2 = $('#dpaId2').val();
	
	if (_dpaId2 == "") {
		$('#dpaId2Error').text("DPA Id is mandatory");
		return false;
	}
	else {
		
		if(_companyCode1 == _companyCode2)
		{
			if(_userId1 == _userId2)
			{
				if(_csoId1 == _csoId2)
				{
					if(_dpaId1 != _dpaId2)
					{
						$('#dpaId2Error').text("Please select same dpa as for owner");
						return false;
					}
				}
					
			}
		}
		
		$('#dpaId2Error').text("");
		return true;
	}

}

function validateCompanyCode3() {

	var _companyCode3 = $('#companyCode3').val();
	
	if (_companyCode3 == "") {
		$('#companyCode3Error').text("Company Code is mandatory");
		return false;
	}
	else {
		$('#companyCode3Error').text("");
		return true;
	}

}


//validation for user id/ login id

function validateUserId3() {

	var _userId3 = $('#userId3').val();

	if (_userId3 == "") {
		$('#userId3Error').text("Login Id is mandatory");
		return false;
	}
	else {
		$('#userId3Error').text("");
		return true;
	}

}


//validation for csoid

function validateCSOId3() {

	var _companyCode1 = $('#companyCode1').val();
	var _companyCode2 = $('#companyCode2').val();
	var _companyCode3 = $('#companyCode3').val();

	var _userId1 = $('#userId1').val();
	var _userId2 = $('#userId2').val();
	var _userId3 = $('#userId3').val();

	var _csoId1 = $('#csoId1').val();
	var _csoId2 = $('#csoId2').val();
	var _csoId3 = $('#csoId3').val();

	if (_csoId3 == "") {
		$('#csoId3Error').text("CSO Id is mandatory");
		return false;
	}
	else {
		
		if(_companyCode1 == _companyCode3)
		{
			if(_userId1 == _userId3)
			{
				if(_csoId1 != _csoId3)
				{
					$('#csoId3Error').text("Please select same cso as for owner");
					return false;
				}
					
			}
		}
		else if(_companyCode2 == _companyCode3)
		{
			if(_userId2 == _userId3)
			{
				if(_csoId2 != _csoId3)
				{
					$('#csoId3Error').text("Please select same cso as for technical");
					return false;
				}
					
			}
		}
		
		$('#csoId3Error').text("");
		return true;
	}

}

//validation for acso id

function validateACSOId3() {

	var _acsoId3 = $('#acsoId3').val();

	if (_acsoId3 == "") {
		$('#acsoId3Error').text("ACSO Id is mandatory");
		return false;
	}
	else {
		$('#acsoId3Error').text("");
		return true;
	}

}

//validation for dpa id

function validateDPAId3() {

	var _companyCode1 = $('#companyCode1').val();
	var _companyCode2 = $('#companyCode2').val();
	var _companyCode3 = $('#companyCode3').val();

	var _userId1 = $('#userId1').val();
	var _userId2 = $('#userId2').val();
	var _userId3 = $('#userId3').val();

	var _csoId1 = $('#csoId1').val();
	var _csoId2 = $('#csoId2').val();
	var _csoId3 = $('#csoId3').val();

	var _dpaId1 = $('#dpaId1').val();
	var _dpaId2 = $('#dpaId2').val();
	var _dpaId3 = $('#dpaId3').val();

	if (_dpaId3 == "") {
		$('#dpaId3Error').text("DPA Id is mandatory");
		return false;
	}
	else {
		
		if(_companyCode1 == _companyCode3)
		{
			if(_userId1 == _userId3)
			{
				if(_csoId1 == _csoId3)
				{
					if(_dpaId1 != _dpaId3)
					{
						$('#dpaId3Error').text("Please select same dpa as for owner");
						return false;
					}
				}
					
			}
		}
		
		else if(_companyCode2 == _companyCode3)
		{
			if(_userId2 == _userId3)
			{
				if(_csoId2 == _csoId3)
				{
					if(_dpaId2 != _dpaId3)
					{
						$('#dpaId3Error').text("Please select same dpa as for technical");
						return false;
					}
				}
					
			}
		}
		
		$('#dpaId3Error').text("");
		return true;
	}

}

//validation for doc 1

function validateDoc1() {

	var _emailDoc = document.getElementById('emailDoc');

	if ((_emailDoc.value != "") && ((_emailDoc.files[0].size > 2097152) || (_emailDoc.value.substring(_emailDoc.value.lastIndexOf('.') + 1) != 'pdf'))) {
		$('#emailDocError').text("Mandatory PDF Document size should not exceed 2MB");
		return false;
	}
	else {
		$('#emailDocError').text("");
		return true;
	}

}

//validation for doc 2

function validateDoc2() {

	var _tataLritDoc = document.getElementById('tataLritDoc');

	if ((_tataLritDoc.value != "") && ((_tataLritDoc.files[0].size > 2097152) || (_tataLritDoc.value.substring(_tataLritDoc.value.lastIndexOf('.') + 1) != 'pdf'))) {
		$('#tataLritDocError').text("Mandatory PDF Document size should not exceed 2MB");
		return false;
	}
	else {
		$('#tataLritDocError').text("");
		return true;
	}

}

//validation for doc 3

function validateDoc3() {

	var _flagRegDoc = document.getElementById('flagRegDoc');

	if ((_flagRegDoc.value != "") && ((_flagRegDoc.files[0].size > 2097152) || (_flagRegDoc.value.substring(_flagRegDoc.value.lastIndexOf('.') + 1) != 'pdf'))) {
		$('#flagRegDocError').text("Mandatory PDF Document size should not exceed 2MB");
		return false;
	}
	else {
		$('#flagRegDocError').text("");
		return true;
	}

}

//validation for doc 4

function validateDoc4() {

	var _conformanceTestCertDoc = document.getElementById('conformanceTestCertDoc');

	if ((_conformanceTestCertDoc.value != "") && ((_conformanceTestCertDoc.files[0].size > 2097152) || (_conformanceTestCertDoc.value.substring(_conformanceTestCertDoc.value.lastIndexOf('.') + 1) != 'pdf'))) {
		$('#conformanceTestCertDocError').text("Mandatory PDF Document size should not exceed 2MB");
		return false;
	}
	else {
		$('#conformanceTestCertDocError').text("");
		return true;
	}

}

//validation for doc 5

function validateDoc5() {

	var _additionalDoc = document.getElementById('additionalDoc');

	if((_additionalDoc.value != "") && ((_additionalDoc.files[0].size > 2097152) || (_additionalDoc.value.substring(_additionalDoc.value.lastIndexOf('.') + 1) != 'pdf'))) {
		$('#additionalDocError').text("PDF Document size should not exceed 2MB");
		return false;
	} 
	else {
		$('#additionalDocError').text("");
		return true;
	}

}

//validation for seid status

/*function validateSEIDStatus() {

	var _seidStatus= $('#seidStatus').val();

	if (_seidStatus != "Active") {
		$('#seidStatusError').text("SEID Status is mandatory");
		return false;
	}
	else {
		$('#seidStatusError').text("");
		return true;
	}

}*/


//validation for reporting status

function validateStatus()
{
	var _status = $('#status').val();

	if(_status == "") {
		$('#statusError').text("Please select Vessel Reporting Status");
		return false;
	}
	else{
		$('#statusError').text("");
		return true;
	}
}


//validation for reporting status change reason 

function validateReason()
{
	var _reason = $('#reason').val();

	if(_reason == "") {
		$('#reasonError').text("Please select valid reason to change vessel reporting status");
		return false;
	}
	else{
		$('#reasonError').text("");
		return true;
	}
}


//validation for reporting status change remarks

function validateRemarksForInactive()
{
	var _remarks = $('#remarksforinactive').val();

	if((_remarks != "") && ((!numAlphaPattern.test(_remarks)) || (_remarks.length > 500))) {
		$('#remarksforinactiveError').text("Remarks should be of maximam 500 characters long alphanumeric field");
		return false;
	}
	else{
		$('#remarksforinactiveError').text("");
		return true;
	}
}

//validation for reporting status change remarks

function validateRemarksForActive()
{
	var _remarks = $('#remarksforactive').val();

	if((_remarks != "") && ((!numAlphaPattern.test(_remarks)) || (_remarks.length > 500))) {
		$('#remarksforactiveError').text("Remarks should be of maximam 500 characters long alphanumeric field");
		return false;
	}
	else{
		$('#remarksforactiveError').text("");
		return true;
	}
}


//validation for reporting status change remarks

function validateRemarksForSell()
{
	var _remarks = $('#remarksforsell').val();

	if((_remarks != "") && ((!numAlphaPattern.test(_remarks)) || (_remarks.length > 500))) {
		$('#remarksforsellError').text("Remarks should be of maximam 500 characters long alphanumeric field");
		return false;
	}
	else{
		$('#remarksforsellError').text("");
		return true;
	}
}

//validation for reporting status change remarks

function validateRemarksForScrap()
{
	var _remarks = $('#remarksforscrap').val();

	if((_remarks != "") && ((!numAlphaPattern.test(_remarks)) || (_remarks.length > 500))) {
		$('#remarksforscrapError').text("Remarks should be of maximam 500 characters long alphanumeric field");
		return false;
	}
	else{
		$('#remarksforscrapError').text("");
		return true;
	}
}

//validation for reporting status change document 

function validateReportingDocumentForInactive()
{
	var _document = document.getElementById('documentforinactive');

	if ((_document.value == "") || (_document.files[0].size > 512000) || (_document.value.substring(_document.value.lastIndexOf('.') + 1) != 'pdf')) {
		$('#documentforinactiveError').text("Mandatory PDF Document size should not exceed 500kb");
		return false;
	} 
	else {
		$('#documentforinactiveError').text("");
		return true;
	}
}

//validation for reporting status change document 

function validateReportingDocumentForActive()
{
	var _document = document.getElementById('documentforactive');

	if ((_document.value == "") || (_document.files[0].size > 512000) || (_document.value.substring(_document.value.lastIndexOf('.') + 1) != 'pdf')) {
		$('#documentforactiveError').text("Mandatory PDF Document size should not exceed 500kb");
		return false;
	} 
	else {
		$('#documentforactiveError').text("");
		return true;
	}
}

//validation for reporting status change document 

function validateReportingDocumentForSell()
{
	var _document = document.getElementById('documentforsell');

	if ((_document.value == "") || (_document.files[0].size > 512000) || (_document.value.substring(_document.value.lastIndexOf('.') + 1) != 'pdf')) {
		$('#documentforsellError').text("Mandatory PDF Document size should not exceed 500kb");
		return false;
	} 
	else {
		$('#documentforsellError').text("");
		return true;
	}
}

//validation for reporting status change document 

function validateReportingDocumentForScrap()
{
	var _document = document.getElementById('documentforscrap');

	if ((_document.value == "") || (_document.files[0].size > 512000) || (_document.value.substring(_document.value.lastIndexOf('.') + 1) != 'pdf')) {
		$('#documentforscrapError').text("Mandatory PDF Document size should not exceed 500kb");
		return false;
	} 
	else {
		$('#documentforscrapError').text("");
		return true;
	}
}

//validation for vessel action

function validateAction()
{
	var _action = document.getElementById('action');

	if (_action.value == "") {
		$('#actionError').text("Please select valid vessel action.");
		return false;
	} 
	else {
		$('#actionError').text("");
		return true;
	}
}


//validate regStatus for approval

function validateRegStatus()
{
	var _regStatus = $('#regStatus').val();
	
	if((_regStatus == "Submitted")||(_regStatus == "")){
		$('#regStatusError').text("Registration Status is Mandatory. Please select APPROVE/REJECT");
		return false;
	}
	else{
		$('#regStatusError').text("");
		return true;
	}
}

//validation for supporting document for adding shipborne equipment 

function validateSEIDDocument()
{
	var _document = document.getElementById('supportingDocs');

	if ((_document.value == "") || (_document.files[0].size > 512000) || (_document.value.substring(_document.value.lastIndexOf('.') + 1) != 'pdf')) {
		$('#supportingDocsError').text("Mandatory PDF Document size should not exceed 500kb");
		return false;
	} 
	else {
		$('#supportingDocsError').text("");
		return true;
	}
}

