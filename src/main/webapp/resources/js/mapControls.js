

var flagPPRightClick=0; // for searching ship lat long if path projection option selected From vertical menu
var searchTypeChecked;	 
var searchShipPortShipData;
var countryBoundaryList;
var companyNameList;
var waterPolygonNameList;
var lritIdDisplay;
var lritNameDisplay;
var portLritIds;
var rightClickShipData;
var rightClickShipRequestType;
var sarRegionButtonFlag = false;
var archivedSurpicButtonFlag = false;
var distanceBearingFlag = false;
var selectedOceanRegionList = [];
var selectedGeographicalLayersList = [];
var pathProjectionStartFlag = false;
var pathProjectionLength; // =  $("#pathLengthComboID :selected").text();
var dataTableObj;
var zoomChangedFlag =0;
var layerPresent ="";
var fullScreenFlag=false;
var viewSurpic24HoursFlag = true;
function displayWaterPolygon(){
	try{
		
		var table = jq('#waterPolygonsNameTable').DataTable();
		table.search('').draw();
		 var len = countryListIds.split(',');
		 var map1 = new Map();
		
		 var objStatus = false;
		 var objStatusPort = false;
		 $.each( len, function( key, value ) {
			 var favorite = [];
			 var favoritePort = [];
			 
	        	if($("#" + value.trim() + "_1").prop("checked") == true)
	        		 favorite.push('InternalWaters');
				 
	        	if($("#" + value.trim() + "_2").prop("checked") == true)
	        		 favorite.push('TerritorialSea');							 
				 
	        	if($("#" + value.trim() + "_3").prop("checked") == true)
	        		 favorite.push('CustomCoastal');				 
	        		 
	        	if($("#" + value.trim() + "_5").prop("checked") == true)
	        		 favorite.push('CoastalSea');
	        	
	        	if($("#" + value.trim() + "_4").prop("checked") == true)
	        		 favorite.push('OpenSO');
				
	        	if($("#" + value.trim() + "_6").prop("checked") == true)
	        	{ 
	        		favorite.push('Port');
	        		favoritePort.push('Port');
	        	}
	        	map1.set(value.trim(),favorite);
	        	if(favorite.length>0)
	        		objStatus=true;
	        		
	        	if(favoritePort.length>0)
	        		objStatusPort=true;
	     });
		 
		
	     // Making JS Map compatible for JSON.Stringify
	     const out = Object.create(null)
	     map1.forEach(function(value, key){
	    	 
	      if (value instanceof Map) {
	        out[key] = JSON.stringify(map_to_object(value))
	
	      }
	      else {
	        out[key] = JSON.stringify(value)
	      }
	    })
		
	   // var objStatus = (Object.keys(out).length === 0); 
	      
		 document.getElementById("selectedWaterPolygonsName").value =JSON.stringify(out);
	     document.getElementById("selectedPortsName").value =JSON.stringify(out);
	    // $('input:checkbox').removeAttr('checked');
	     //$( "#LayerForm" ).trigger("reset");
	     if(objStatus===true) {
			 $.ajax({
				    type: "POST",
				    traditional: true,
				    url: "/lrit/map/SelectedWaterPolygons",
				    data: { 'selectedWaterPolygonsName': JSON.stringify(out) },
				    success:function(response) {		
				    	try{
				            generateWaterPolygonLayer(response);
				    	}
				    	catch(err)
				    	{
				    		console.info('error in display Water Polygon: ' + err)
				    	}
				        
				      }		 		
				});	
	     }
	     if(objStatusPort===true) {
			 $.ajax({
				    type: "POST",
				    traditional: true,
				    url: "/lrit/map/SelectedPorts",
				    data: { 'selectedPortsName': JSON.stringify(out) },
				    success:function(response) {		
				    	try{
				    		if(response!=null)
				    			generatePortLayer(response);
				    	}
				    	catch(err)
				    	{
				    		console.info('error in display Water Polygon: ' + err)
				    	}
				        
				      }		 		
				});	
	   }
	}
	catch(err)
	{
		console.info('error in display Water Polygon: ' + err)
	}
}

function clearWaterPolygonFrom()
{
	 removeWaterPolygonLayer();
	 removeLayersFromMap('selectedPortLayer','name','POINT');
	 map.addLayer(vectorLayer_selectedPort);
	 var len = countryListIds.split(',');
	
	$.each( len, function( key, value ) {		   		  
		 $("#" + value.trim() + "_1").prop("checked", false);
		 $("#" + value.trim() + "_2").prop("checked", false);
		 $("#" + value.trim() + "_3").prop("checked", false);
		 $("#" + value.trim() + "_4").prop("checked", false);
		 $("#" + value.trim() + "_5").prop("checked", false);
 		 $("#" + value.trim() + "_6").prop("checked", false);
     });
	
	 $("#checkbox1").prop("checked", false);
	 $("#checkbox2").prop("checked", false);
	 $("#checkbox3").prop("checked", false);
	 $("#checkbox4").prop("checked", false);
	 $("#checkbox5").prop("checked", false);
	 $("#checkbox6").prop("checked", false);
}

function displayWaterPolygonFrom()
{	
	try{
		if ($('#waterPolygonsNameTableBody').children().length == 0){
		for(var i=0; i<lritIdDisplay.length;i++)
		{
			$('#waterPolygonsNameTableBody').append('<tr> <td> '+ lritNameDisplay[i] + ' (' + lritIdDisplay[i].trim() +')' +'</td>'+							
			'<td> <input type=\"checkbox\" name=\"Layer\" id = \"' + lritIdDisplay[i].trim() + '_1\" /> </td>' +
			'<td> <input type=\"checkbox\" name=\"Layer\" id = \"' + lritIdDisplay[i].trim() + '_2\" /> </td>' +
			'<td> <input type=\"checkbox\" name=\"Layer\" id = \"' + lritIdDisplay[i].trim() + '_3\" /> </td>' +			
			'<td> <input type=\"checkbox\" name=\"Layer\" id = \"' + lritIdDisplay[i].trim() + '_5\" /> </td>' +
			'<td> <input type=\"checkbox\" name=\"Layer\" id = \"' + lritIdDisplay[i].trim() + '_4\" /> </td> ' +
			'<td> <input type=\"checkbox\" name=\"Layer\" id = \"' + lritIdDisplay[i].trim() + '_6\" /> </td> ' +
			'</tr>');
		}	
		 var len = lritIdDisplay;			
		
		  $.each( len, function( key, value ) {			  		  
			  
			  $("#" + value.trim() + "_1").attr('disabled', true);
			  $("#" + value.trim() + "_2").attr('disabled', true);
			  $("#" + value.trim() + "_3").attr('disabled', true);
			  $("#" + value.trim() + "_4").attr('disabled', true);
			  $("#" + value.trim() + "_5").attr('disabled', true);
			  $("#" + value.trim() + "_6").attr('disabled', true);
			
				 var uniqueNames = [];
				
				 for(var j = 0; j<waterPolygonNameList.length; j++)
				 {
					 if (waterPolygonNameList[j].cglritID.trim() == value.trim())
						 uniqueNames.push(waterPolygonNameList[j].type);
				 }
				 				 
				 $.each(uniqueNames, function(val, text) {
					 
					 if(text.toLowerCase() === ('InternalWaters').toLowerCase())
						 $("#" + value.trim() + "_1").attr('disabled', false);
					 
					 if(text.toLowerCase() === ('TerritorialSea').toLowerCase())
						 $("#" + value.trim() + "_2").attr('disabled', false);							 
					 
					 if(text.toLowerCase() === ('CustomCoastal').toLowerCase())
						 $("#" + value.trim() + "_3").attr('disabled', false);
					 /*
					 if(text.toLowerCase() === ('OT').toLowerCase())
						 $("#" + value.trim() + "_4").attr('disabled', false);*/
					 							 
					if(text.toLowerCase() === ('CoastalSea').toLowerCase())
						 $("#" + value.trim() + "_5").attr('disabled', false);
					
					 if(text.toLowerCase() === ('SO').toLowerCase())
						 $("#" + value.trim() + "_4").attr('disabled', false);
					
					 if(text.toLowerCase() === ('Port').toLowerCase())
						 $("#" + value.trim() + "_6").attr('disabled', false);
					
				 });				
			
			});
		}
	}
	catch(err)
	{
		console.info('error in display Water Polygon Form: ' + err)
	}
}

function displayCountriesShips()
{	
	try{
		
		var table = jq('#searchShipByCountryTable').DataTable();
		table.search('').draw();
		
		 var countryName = [];
		 for(var i=0; i<countryBoundaryList.length;i++)
		 {
			 if($("#" + countryBoundaryList[i].cg_lritid + "_country").prop("checked") == true) 
	       	{	
	       		countryName.push(countryBoundaryList[i].cg_lritid) ;
	       	
	       	}
		 }
			 
		generateSelectedShipLayer(countryName);
	}
	catch(err)
	{
		console.info('error in display Countries Ships: ' + err)
	}
}

function displayCountriesShipsInBoundary()
{	
	try{
		
		var table = jq('#searchShipByCountryBoundaryTable').DataTable();
		table.search('').draw();
		
		 var countryName = [];
		 for(var i=0; i<countryBoundaryList.length;i++)
		 {
			 if($("#" + countryBoundaryList[i].cg_lritid + "_countryBoundary").prop("checked") == true) 
	       	{	
	       		countryName.push(countryBoundaryList[i].cg_lritid) ;
	       	
	       	}
		 }
		
		generateCountryBoundaryLayer(countryName);
	}
	catch(err)
	{
		console.info('error in display Ships in boundary: ' + err)
	}
}

function clearCountrySelction()
{	
	 for(var i=0; i<countryBoundaryList.length;i++)
	 {	
       	 $("#" +countryBoundaryList[i].cg_lritid + "_country").prop("checked", false);       	
       	
	 }	
	
	 $("#checkboxCountryList").prop("checked", false);
	 removeLayersFromMap('SelectedCountryShipLayer','type','SelectedCountryShip');
	 map.addLayer(vectorLayer_SelectedCountryShipPosition);
}

function clearCountryBoundarySelction()
{	
	 for(var i=0; i<countryBoundaryList.length;i++)
	 {	
       	 $("#" +countryBoundaryList[i].cg_lritid + "_countryBoundary").prop("checked", false);       	
       	
	 }	
	
	 $("#checkboxCountryBoundaryList").prop("checked", false);
	 removeLayersFromMap('countryBoundrayLayer','type','CountryBoundary');
	 map.addLayer(vectorLayer_SelectedCountryBoundaryShipPosition);
}

function displayCountriesShipsFrom()
{	
	try{
		
		if ($('#searchShipByCountryTableBody').children().length == 0){
			for(var i=0; i<countryBoundaryList.length;i++)
			{
				
				for(var j=0; j<lritIdDisplay.length;j++)
				{
					if(lritIdDisplay[j].trim() == countryBoundaryList[i].cg_lritid.trim())
					{						
						$('#searchShipByCountryTableBody').append('<tr><td>' + lritNameDisplay[j] + ' (' + lritIdDisplay[j].trim() +')' + ' </td><td> <input type=\"checkbox\" name=\"Layer\" id = \"' +
								countryBoundaryList[i].cg_lritid  + '_country\"/>  </td> '+
								'</tr>'); // <td style=\"Width:100px\">' + lritNameDisplay[j] + '</td> searchShipByCountryTable
					}		
				}			
			}
		}	
	}
	catch(err)
	{
		console.info('error in display Countries Ships From: ' + err)
	} 
}

function displayCountriesBoundaryShipsFrom()
{	
	try{
		if ($('#searchShipByCountryBoundaryTableBody').children().length == 0){
			for(var i=0; i<countryBoundaryList.length;i++)
			{
				
				for(var j=0; j<lritIdDisplay.length;j++)
				{
					if(lritIdDisplay[j].trim() == countryBoundaryList[i].cg_lritid.trim())
					{
						$('#searchShipByCountryBoundaryTableBody').append('<tr><td>' + lritNameDisplay[j] + ' (' + lritIdDisplay[j].trim() +')' + '</td><td> <input type=\"checkbox\" name=\"Layer\" id = \"' +
								countryBoundaryList[i].cg_lritid  + '_countryBoundary\"/>  </td> '+
								'</tr>'); //'<td style=\"Width:100px\">' + lritNameDisplay[j] + '</td>
					}		
				}			
			}
		}
	}
	catch(err)
	{
		console.info('error in display Countries Boundary Ships From: ' + err)
	} 
}


function clearCompanyShipsSelction()
{	
	 for(var i=0; i<companyNameList.length;i++)
	 {	
		 if(companyNameList[i].companyName.trim() !="")
			{
			 $("#" +companyNameList[i].companyCode.trim() + "_company").prop("checked", false);       	
			}
	 }	
	
	 $("#checkboxCompanyName").prop("checked", false);
	 removeLayersFromMap('SelectedCompanyShipLayer','type','SelectedCompanyShip');
	 map.addLayer(vectorLayer_SelectedCompanyShipPosition);
}
function displayCompanyShipsFrom()
{	
	try{
		if ($('#searchShipByCompanyTableBody').children().length == 0){
			for(var i=0; i<companyNameList.length;i++)
			{
				if(companyNameList[i].companyName.trim() !="")
				{
					$('#searchShipByCompanyTableBody').append('<tr><td>' + companyNameList[i].companyName + '</td><td> <input type=\"checkbox\" name=\"Layer\" id = \"' +
						companyNameList[i].companyCode.trim()  + '_company\"/>  </td> '+
						'</tr>');	 //'<td style=\"Width:100px\">' + companyNameList[i].companyName + '</td>
				}
			}	
		}
	}
	catch(err)
	{
		console.info('error in display Company Ships From: ' + err)
	} 
}

function displayCompanyShips()
{
	try{
		var table = jq('#searchShipByCompanyTable').DataTable();
		table.search('').draw();
		
		 var company = [];
		 
		 for(var i=0; i<companyNameList.length;i++)
		 {
			 if($("#" + companyNameList[i].companyCode.trim()  + '_company').prop("checked") == true)
				 company.push(companyNameList[i].companyCode.trim());	
		 }		 
		 //document.getElementById("companyName").value =JSON.stringify(company);	   
		 $.ajax({
			    type: "POST",
			    traditional: true,
			    url: "/lrit/map/SearchShipByCompany",
			    data: { 'companyCode': JSON.stringify(company) },
			    success:function(response) {	
			    	try{
			    		generateSelectedShipCompnyLayer(response);			   
			    	}
			    	catch(err)
			    	{
			    		console.info('error in display Company Ships: ' + err)
			    	} 
			      }		 		
			});
	}
	catch(err)
	{
		console.info('error in display Company Ships: ' + err)
	} 
   
}

// display Path Projection

function pathProjectionfunc()
{
	try{
		var text = document.getElementById("PPimoNoVal").value;	
	//	pathProjectionLength =  $("#pathLengthComboID :selected").text();
		var textLength = document.getElementById("pathLengthComboID").value;	
		 $("#idRingDistance").val(textLength.trim());
	  pathProjectionLength = $("#idRingDistance").val();
	 
	  if(flagPPRightClick==0)
		{
			for(i =0; i<shipData.length; i++)
			{		    	 
				if ( text == shipData[i].imo_no)
				{	currentShipLong = shipData[i].longitude
					currentShipLat = shipData[i].latitude;
					currentShipCourse = shipData[i].course;			
					currentShipSpeed = shipData[i].speed;
				}
			}	 
		}
	  
		//var dist = ("#pathLengthComboID").val(); //$("#pathLengthComboID :selected").text();
		
		if(currentShipCourse == null || currentShipLat  == null || currentShipLong == null || currentShipSpeed == null)
		{
		 // alert('Not enough information for Predicting Path')
			pathProjectionStartFlag = true;
		}
		else
		{
			//showPathProjection(currentShipLong, currentShipLat, currentShipCourse, dist*100000);
			pathProjectionAfterThisHour = pathProjectionLength;
			if(currentShipSpeed>0 && currentShipCourse>0)
				showPathProjection(currentShipLat, currentShipLong, currentShipCourse, currentShipSpeed*1850*pathProjectionAfterThisHour);
			else
				alert('Not enough information for Predicting Path');
		}
	}
	catch(err)
	{
		console.info('error in pathProjectionfunc: ' + err)
	} 
   
}

// 

//Search ship/port button controls


function sarSurpicfunc(){
	
	optionSurpicValue = document.getElementsByName('sarSurpicFormRadios');
	var selectedSurpic;
	 for(i = 0; i < optionSurpicValue.length; i++) { 
        if(optionSurpicValue[i].checked) 
   	 {
        	selectedSurpic = optionSurpicValue[i].value;
   	 }
	 }	
	 rightClickCoodinate = "";
	 rightClickSurpicRadius = "";
	 rightClickSurpicCenter = "";
	 surpicInteractionOnFlag = 0;
	if (selectedSurpic == 'Rectangle')
	{		
		/*if(sarSurpicAllowed==='true')					
			window.open("../requests/surpicrequest", '_blank');	
		else if(coastalSurpicAllowed==='true')
			window.open("../requests/coastalsurpicrequest", '_blank');*/
		addInteractionSurpicRectangle();
	}
	else
		addInteractionSurpicRing();
}


//Search Ship Port

//---------------------------------------------------------------------------
//Function to populate data in the search ship and port form
//---------------------------------------------------------------------------

var radioBtnsSearchShipPort = document.getElementsByName("searchType");

// populate combo box for selecting port
function populateCombo()
{
	try{
		var searchType;
		var searchValue;
		var searchCountry;	
		radioBtnsSearchShipPort = document.getElementsByName("searchType");
		var lblSearch = document.getElementById("lblSearchCriteria");
		var lblValue = document.getElementById("lblSearchValue");
		
		$("#searchCriteriaCombo").empty();
	
		for(var i=0;i<radioBtnsSearchShipPort.length; i++){
		    if(radioBtnsSearchShipPort[i].checked && radioBtnsSearchShipPort[i].value == "ship"){	  
		    	$("input[name='searchType']:checked").val("ship");
		    	lblSearch.innerHTML = "Search Criteria";
		    	lblValue.innerHTML = "Value";
		    	$("#idSearchValue").attr("disabled", false);
		    	/*	$("#lblSearchValue").css("color", "black");*/
		    		    	
		        var opt1 = document.createElement("option");
		        opt1.value = "Ship Name";
		        opt1.text = "Ship Name";
		       //	searchCriteriaCombo.add(opt1, null);
		        $("#searchCriteriaCombo").append(opt1);
		       	
		        var opt2 = document.createElement("option");
		        opt2.value = "IMO No.";
		        opt2.text = "IMO No.";
		       	//searchCriteriaCombo.add(opt2, null);
		        $("#searchCriteriaCombo").append(opt2);
		        
		        var opt3 = document.createElement("option");
		        opt3.value ="MMSI No.";
		        opt3.text = "MMSI No.";
		       	//searchCriteriaCombo.add(opt3, null);
		        $("#searchCriteriaCombo").append(opt3);
		        
		        var opt4 = document.createElement("option");
		        opt4.value ="SEID No.";
		        opt4.text = "SEID No.";
		       	//searchCriteriaCombo.add(opt3, null);
		        $("#searchCriteriaCombo").append(opt4);
		        
		        var opt5 = document.createElement("option");
		        opt5.value ="DNID No.";
		        opt5.text = "DNID No.";
		       	//searchCriteriaCombo.add(opt3, null);
		        $("#searchCriteriaCombo").append(opt5);
				        
		    }
		    else if (radioBtnsSearchShipPort[i].checked && radioBtnsSearchShipPort[i].value == "port")
	    	{	    	
		    	$("input[name='searchType']:checked").val("port");
		    	lblSearch.innerHTML = "Country";
		    	lblValue.innerHTML = "Search";
		    	$("#idSearchValue").attr("disabled", true);
		    	//$("#lblSearchValue").css("color", "gray");	    
		    	var elementId = "searchButton";
		    	hideElementFunction(elementId);
		    	var opt = document.createElement("option");
    			opt.value = "Select";
    			opt.text = "Select";
    			//searchCriteriaCombo.add(opt, null);
    			 $("#searchCriteriaCombo").append(opt);		    	
		    	for(var i=0; i<portLritIds.length; i++)
	    		{
	    			for(var j=0; j<lritIdDisplay.length; j++)
		    		{
		    			if((portLritIds[i].cg_lritid).trim()== lritIdDisplay[j].trim())
		    			{		    		
				    		var opt = document.createElement("option");
			    			opt.value = lritIdDisplay[i];
			    			opt.text = lritNameDisplay[i];
			    			//searchCriteriaCombo.add(opt, null);
			    			 $("#searchCriteriaCombo").append(opt);
		    			}
		    		}
	    		}
		    	
	    	}
		}
	}
	catch(err)
	{
		console.info('error in populateCombo: ' + err)
	}
	
}



var portData;
var portcheckBoxSelectAll;

function searchShipPortLayer(response)
{
	try{
		
		portData = response; 
		
		if (jq.fn.DataTable.isDataTable( '#searchPortTable' ) ) {
			
			$('#searchPortTableBody').html("");
			$('#searchPortTable').html("");					 	
			jq('#searchPortTable').DataTable().clear().draw();		
			jq('#searchPortTable').DataTable().destroy();	
			$('#searchPortTable').html("");
			
			 var tabkeHeader = "<thead><tr><th><input type=\"checkbox\"id=\"checkBoxSelectAll\"  name=\"checkBoxSelectAllName\" \" /></th>" +
				"<th>CG Name</th><th>CG LRITID</th><th>regularversionno</th><th>lcode</th><th>Name</th><th>Latitude</th><th>Longitude</th>" +
				"</tr></thead><tbody id=\"searchPortTableBody\"></tbody>"
		
				$('#searchPortTable').append(tabkeHeader);
				
				for(var i = 0; i <= response.length-1; i++) {
					
					var portCountryName = getCgNameByCgLritId(response[i].cglritid);						
					
					$('#searchPortTableBody').append('<tr><td><input type=\"checkbox\" name=\"checkbox\" id = \"checkboxShipPort_' + i  + '\"/></td><td>'+ portCountryName +'</td>' + 
				    		'<td>'+ response[i].cglritid +'</td>' + '<td>'+ response[i].regularversionno +'</td>' + '<td>'+ response[i].locode +'</td>' + 
				    		'<td>'+ response[i].name +'</td>'+ '<td>'+ response[i].position.coordinates[0] +'</td>' +
				    		'<td>'+ response[i].position.coordinates[1] +'</td>'+				    		
							'</tr>');	 				
				}
			
				var table=  jq('#searchPortTable').DataTable( {   	
		   			 "columnDefs": [
		                 {className: "dt-center", "targets": '_all'}  
		             ],
		   			 	"scrollY":        "150px",
			 	        "scrollCollapse": true,
			 	        "paging":         false,
			 	        "ordering": false,
			 	        "info":     false,
			 	       // "searching": false,
			 	        searching: true,
			 	        dom: 't'
			 	       
			 	    } );	
			 $("#idSearchValue").attr("disabled", false);	
		   	 var input = document.getElementById("idSearchValue");
			 input.addEventListener("keyup", function(event) {
				 table.search($(this).val()).draw() ;
			 });
		}
		else
		{
			var tabkeHeader = "<thead><tr><th><input type=\"checkbox\"id=\"checkBoxSelectAll\"  name=\"checkBoxSelectAllName\" \" /></th>" +
			"<th>CG Name</th><th>CG LRITID</th><th>regularversionno</th><th>lcode</th><th>Name</th><th>Latitude</th><th>Longitude</th>" +
			"</tr></thead><tbody id=\"searchPortTableBody\"></tbody>"
	
			$('#searchPortTable').append(tabkeHeader);
			
			for(var i = 0; i <= response.length-1; i++) {
				
				var portCountryName = getCgNameByCgLritId(response[i].cglritid);	  
										
				$('#searchPortTableBody').append('<tr><td><input type=\"checkbox\" name=\"checkbox\" id = \"checkboxShipPort_' + i  + '\"/></td><td>'+ portCountryName +'</td>' + 
			    		'<td>'+ response[i].cglritid +'</td>' + '<td>'+ response[i].regularversionno +'</td>' + '<td>'+ response[i].locode +'</td>' + 
			    		'<td>'+ response[i].name +'</td>'+ '<td>'+ response[i].position.coordinates[0] +'</td>' +
			    		'<td>'+ response[i].position.coordinates[1] +'</td>'+			    		
						'</tr>');	 				
			}
		
			var table = jq('#searchPortTable').DataTable( {   	
	   			/* "columnDefs": [
	                 {className: "dt-center", "targets": '_all'}  
	             ],*/
	   			 	"scrollY":        "150px",
		 	        "scrollCollapse": true,
		 	        "paging":         false,
		 	        "ordering": false,
		 	        "info":     false,
		 	        /*"searching": false,*/
		 	      	searching: true,
		 	        dom: 't'
		 	       
		 	    } );	
			$("#idSearchValue").attr("disabled", false);
	   		var input = document.getElementById("idSearchValue");
			 input.addEventListener("keyup", function(event) {
				 table.search($(this).val()).draw() ;
			 });
		}
	}
	catch(err)
	{
		console.info('error in searchShipPortLayer: ' + err)
	}
}//function searchShipPortLayer(response)



function checkUncheckComboBoxes(flagAllSelect){
	
	try{
		if(flagAllSelect)
			{
			for(var i=0; i<portData.length; i++)
				$("#checkboxShipPort_"+i).prop("checked", true);			  				
			}
		else
		{
			for(var i=0; i<portData.length; i++)
				$("#checkboxShipPort_"+i).prop("checked", false);		
		}
	}
	catch(err)
	{
		console.info('error in checkUncheckComboBoxes: ' + err)
	}
}

//To enable or disable displaySerachButton when atleast one combobox is selected 	
function checkComboSelect()
{ 
	try{
		var selectedCombo = 0;	
		if (portData != undefined)
		{
			for(var i=0; i<portData.length; i++)
			{
				if($("#checkboxShipPort_"+i).prop("checked") == true)			
				{	
				 	selectedCombo = selectedCombo + 1;
				}	
			}
		
			if(selectedCombo>0)
				document.getElementById("displaySerachButton").disabled = false;
			else
				document.getElementById("displaySerachButton").disabled = true;
		}
	}
	catch(err)
	{
		console.info('error in checkComboSelect: ' + err)
	}
}

function searchShipDetails()
{
	try{
	var searchCriteria = document.getElementById("searchCriteriaCombo").value;
		//console.info(searchCriteria)
		searchCriteria = searchCriteria.replace(/\s/g,''); 	
		var searchVal = document.getElementById("idSearchValue").value;
		
		$.ajax({ 
			type: "POST",
			traditional: true,			    
			url: "/lrit/map/ShipDetailsOnSearchCriteria?searchCriteria="+ searchCriteria + '&searchVal='+searchVal + '&cg_lritId='+countryListIdsShips,			    
			success:function(response) {
			    //invoke your function 
				try{
					searchShipPortShipData = response;					
		    		searchShipLayer(response);		
				}
				catch(err)
				{
					console.info('error in searchShipDetails: ' + err)
				}
	    	}		 		
		});		
	}
	catch(err)
	{
		console.info('error in searchShipDetails: ' + err)
	}
}


function searchShipLayer(response)
{
	try{
		portData = response; 
		
		if (jq.fn.DataTable.isDataTable( '#searchShipTable' ) ) {
				//$('#searchShipTable').html("");
				$('#searchShipTableBody').html("");
				//jq('#searchShipTable').DataTable().destroy();			 	
			
				/*var tabkeHeader = "<thead><tr><th><input type=\"checkbox\"id=\"checkBoxSelectAll\"  name=\"checkBoxSelectAllName\" \" /></th>" +
				"<th> IMO No </th><th> MMSI No </th><th>SEID No </th><th> DNID No </th><th> Member No </th><th> Ship Name </th><th> Country </th><th> Latitude </th><th> Longitude </th><th> Status </th>" +
				"</tr></thead><tbody id=\"searchShipTableBody\"></tbody>"
		
				$('#searchShipTable').append(tabkeHeader);*/
				for(var i = 0; i <= response.length-1; i++) {
					var ShipCurrentStatus = "In Active";
					
					if ( (response[i].registration_status==='SHIP_REGISTERED') || 
							(response[i].registration_status==='DNID_DW_REQ') ||
							(response[i].registration_status==='DNID_DEL_REQ') ||
							(response[i].registration_status==='NULL'))
						ShipCurrentStatus = "Active"
					
							$('#searchShipTableBody').append('<tr><td><input type=\"checkbox\" name=\"checkbox\" id = \"checkboxShipPort_' + i  + '\"/></td><td>'+ response[i].imo_no +'</td>' + 
				    		'<td>'+ response[i].mmsi_no +'</td>' + '<td>'+ response[i].ship_borne_equipment_id +'</td>' +'<td>'+ response[i].dnid_no +'</td>' +
				    		'<td>'+ response[i].member_no +'</td>' + '<td>'+ response[i].ship_name +'</td>' + 
				    		'<td>'+ getCgNameByCgLritId(response[i].data_user_provider.trim()) +'</td>'+ '<td>'+ response[i].latitude +'</td>' +
				    		'<td>'+ response[i].longitude +'</td>'+
				    		'<td>'+ ShipCurrentStatus +'</td>'+
							'</tr>');					
					
				}			
				
				jq(jq.fn.dataTable.tables(true)).DataTable()
			      .columns.adjust();
			 
				/*var table = jq('#searchPortTable').DataTable( {  
			   			"columnDefs": [
			                {className: "dt-center", "targets": '_all'},               
			            ],							
			   			 	"scrollX": "100%",
			   			 	"scrollX": true,
			   			 	"scrollY":        "150px",
				 	        "scrollCollapse": true,
				 	        "paging":         false,
				 	        "ordering": false,
				 	        "info":     false,
				 	        "searching": false,
				 	    } );					
		*/
		}
		else{
			var tabkeHeader = "<thead><tr><th><input type=\"checkbox\"id=\"checkBoxSelectAll\"  name=\"checkBoxSelectAllName\" \" /></th>" +
			"<th> IMO No </th><th> MMSI No </th><th>SEID No </th><th> DNID No </th><th> Member No </th><th> Ship Name </th><th> Country </th><th> Latitude </th><th> Longitude </th><th> Status </th>" +
			"</tr></thead><tbody id=\"searchShipTableBody\"></tbody>"
	
			$('#searchShipTable').append(tabkeHeader);	
			
				for(var i = 0; i <= response.length-1; i++) {
					
					var ShipCurrentStatus = "In Active";
					
					if ( (response[i].registration_status==='SHIP_REGISTERED') || 
							(response[i].registration_status==='DNID_DW_REQ') ||
							(response[i].registration_status==='DNID_DEL_REQ') ||
							(response[i].registration_status==='NULL'))
						ShipCurrentStatus = "Active"
							
					$('#searchShipTableBody').append('<tr><td><input type=\"checkbox\" name=\"checkbox\" id = \"checkboxShipPort_' + i  + '\"/></td><td>'+ response[i].imo_no +'</td>' + 
							'<td>'+ response[i].mmsi_no +'</td>' + '<td>'+ response[i].ship_borne_equipment_id +'</td>' +'<td>'+ response[i].dnid_no +'</td>' +
				    		'<td>'+ response[i].member_no +'</td>' + '<td>'+ response[i].ship_name +'</td>' +  
				    		'<td>'+ getCgNameByCgLritId(response[i].data_user_provider.trim()) +'</td>'+ '<td>'+ response[i].latitude +'</td>' +
				    		'<td>'+ response[i].longitude +'</td>'+
				    		'<td>'+ ShipCurrentStatus +'</td>'+
							'</tr>');
				}
		
	   		 jq('#searchShipTable').DataTable( { 
	   			
	   			"scrollX": "100%",
   			 	"scrollX": true,
	   			 	"scrollY":        "150px",
		 	        "scrollCollapse": true,
		 	        "paging":         false,
		 	        "ordering": false,
		 	        "info":     false,
		 	        "searching": false,		 	       
		 	       
		 	    } );	
	   		jq(jq.fn.dataTable.tables(true)).DataTable()
		      .columns.adjust();
		}
	}
	catch(err)
	{
		console.info('error in searchShipLayer: ' + err)
	}
}//function searchShipPortLayer(response)



function resetSearchShipPortForm()
{
	try{
		  //  $("#searchShipPortTable").children().remove(); 
		    
			document.getElementById("idSearchValue").value="";
			
			
				var elementId = "searchButton"
				hideElementFunction(elementId)
			
	 	    elementId = "idSearchValue"
	 	    //hideElementFunction(elementId) 
	 	    elementId = "lblSearchValue"
	 	  //  hideElementFunction(elementId)	 	   
	 	    
	 	    populateCombo();
	 	    
	 	    flagSearchShipPortButtonClick = false;
	 	  
	}
	catch(err)
	{
		console.info('error in resetSearchShipPortForm: ' + err)
	}
}

function hideElementFunction(elementId) {	
  try{ 
	   if (searchTypeChecked == "port") {
		   $("#"+elementId).hide();
		   if (jq.fn.DataTable.isDataTable('#searchShipTable') ) {
				 
				 
				    $('#searchShipTable').html("");
					$('#searchShipTableBody').html("");
					
					// jq('#searchShipTable').dataTable().fnClearTable();
					// jq('#searchShipTable').dataTable().fnDestroy();
					
					jq('#searchShipTable').DataTable().clear().draw();		
					jq('#searchShipTable').DataTable().destroy();	
					$('#searchShipTable').html("");
					
			 }
			 
		   
	  } else {
		  $("#"+elementId).show();
		  if (jq.fn.DataTable.isDataTable('#searchPortTable') ) {
				 
				 
			    $('#searchPortTable').html("");
				$('#searchPortTableBody').html("");
				
				// jq('#searchShipTable').dataTable().fnClearTable();
				// jq('#searchShipTable').dataTable().fnDestroy();
				
				jq('#searchPortTable').DataTable().clear().draw();		
				jq('#searchPortTable').DataTable().destroy();	
				$('#searchPortTable').html("");
				
		 }
	  }
	   document.getElementById("displaySerachButton").disabled = true;
  }
	catch(err)
	{
		console.info('error in hideElementFunction: ' + err)
	}	 
}

var messageIdsSurpicArea = [];
function displayArchievedSURPICAreaView()
{
	try{   	
    	
		var accessType = [];
		
			accessType.push(1);		
			accessType.push(6);
		
     	$.ajax({	     		
		    type: "POST",
		    traditional: true,
		    url: "/lrit/map/SurpicArea",
		    data: { 'cg_lritid': lritIdDisplay, 'timestamp1': startDate, 'timestamp2':endDate, 'accessType':  accessType },
		    success:function(response) {
	    	try{	
			    
			     jsondata =response; 
			     messageIdsSurpicArea = [];
				  for(var i =0; i<jsondata.length; i++)
				  {				 
					  var messageId_SurpicIndex = 0;
					  messageId_SurpicIndex = (jsondata[i].message_id).indexOf(":");
					  var messageId_Surpic = jsondata[i].message_id;
					  
					  if(messageId_SurpicIndex>0)
						  messageId_Surpic = (jsondata[i].message_id).substring(0,messageId_SurpicIndex);					  
					  
					  messageIdsSurpicArea.push(messageId_Surpic.trim());				
					
				  }	 
			
				// find unique values
					function onlyUnique(value, index, self) { 
					    return self.indexOf(value) === index;
					}
					
					var uniqueMessageIds = messageIdsSurpicArea.filter(onlyUnique); 			
					 messageIdsSurpicArea = [];
					 messageIdsSurpicArea = uniqueMessageIds;	
					// console.info(uniqueMessageIds);
					displayArchievedSURPICAreaViewTable(uniqueMessageIds);	
					
					document.getElementById("displaySarAreaViewApplyButton").disabled = false;
		    }
			catch(err)
			{
				console.info('error in SurpicArea: ' + err)
			}			
	    }
	  });				      

	}
	catch(err)
	{
		console.info('error in archieved SURPIC Area View: ' + err)
	}
}

function displaySar24Hours()
{		
	var layersToRemove = [];
	  map.getLayers().forEach(function (layer) {
	      if (layer.get('name') != undefined && (layer.get('name')).match("SURPIC24HoursLayer")) {		    	 
	    	  layer.getSource().clear();
	    	  layersToRemove.push(layer);		         
	      }
	  });

	  var len = layersToRemove.length;
	 
	  for(var i = 0; i < len; i++) {
		  
		  var features = layersToRemove[i].getSource().getFeatures();
		  	features.forEach(function(feature) {		  
	    		layersToRemove[i].getSource().removeFeature(feature);					    	
		    });					
		  	map.removeLayer(layersToRemove[i]);
	      }
	  
	  map.addLayer(vectorLayerSURPIC24Hours);
	var today = new Date();
	var day = today.getUTCDate() + "";
	var month = (today.getUTCMonth() + 1) + "";
	var year = today.getUTCFullYear() + "";
	var hour = today.getUTCHours() + "";
	var minutes = today.getUTCMinutes() + "";
	var seconds = today.getUTCSeconds() + "";
	var endDateTimeToDisplay = year + "-" +month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;

	var today1 = new Date();
	var day1 = today1.getUTCDate()-1 + "";
	var month1 = (today1.getUTCMonth() + 1) + "";
	var year1 = today1.getUTCFullYear() + "";
	var hour1 = today1.getUTCHours() + "";
	var minutes1 = today1.getUTCMinutes() + "";
	var seconds1 = today1.getUTCSeconds() + "";
	var startDateTimeToDisplay = year1 + "-" +month1 + "-" + day1 + " " + hour1 + ":" + minutes1 + ":" + seconds1;
	//console.info('startDateTimeToDisplay: ' + startDateTimeToDisplay);
	//console.info('endDateTimeToDisplay: ' + endDateTimeToDisplay);
	var accessType = [];
	
	if (coastalSurpicAllowed === 'true')
		accessType.push(1);
	else
		accessType.push(6);
	
	try{   	
    	
     	$.ajax({	     		
		    type: "POST",
		    traditional: true,
		    url: "/lrit/map/SurpicArea",
		    data: { 'cg_lritid': countryListIdsShips, 'timestamp1': startDateTimeToDisplay, 'timestamp2':endDateTimeToDisplay, 'accessType':accessType  },
		    success:function(response) {
	    	try{	
	    		
			     jsondata =response; 
			     if(jsondata.length>0)
			     {
			    	 var messageIdsSurpicAreaTemp = [];
					  for(var i =0; i<jsondata.length; i++)
					  {				 
						  var messageId_SurpicIndex = 0;
						  messageId_SurpicIndex = (jsondata[i].message_id).indexOf(":");
						  var messageId_Surpic = jsondata[i].message_id;
						  
						  if(messageId_SurpicIndex>0)
							  messageId_Surpic = (jsondata[i].message_id).substring(0,messageId_SurpicIndex);					  
						  
						  messageIdsSurpicAreaTemp.push(messageId_Surpic.trim());				
							
					  }
					// find unique values
						function onlyUnique(value, index, self) { 
						    return self.indexOf(value) === index;
						}
						var uniqueMessageIds = null;						 					
						uniqueMessageIds = messageIdsSurpicAreaTemp.filter(onlyUnique);
						displaySelectedSurpicArea(uniqueMessageIds,vectorLayerSURPIC24Hours);
						
					}
					
					/* if(uniqueMessageIds===undefined || currentSurpicArea=== null)
						{
						  $("#hideViewSurpicLayerButton").prop("disabled",true);
						}
					  else
						  $("#hideViewSurpicLayerButton").prop("disabled",false);*/
					return uniqueMessageIds;
		    }
			catch(err)
			{
				console.info('error in SurpicArea: ' + err)
			}		
	    }
	  });				      

	}
	catch(err)
	{
		console.info('error in archieved SURPIC Area View: ' + err)
	}
	
	
	
	
}


function hideViewSurpic24Hours()
{
	
	if (viewSurpic24HoursFlag==false)
	{ 
		vectorLayerSURPIC24Hours.setVisible(false);		
		surpicButtonText = "View Active Surpic";
		$("#hideViewSurpicLayerButton").html(surpicButtonText);
		viewSurpic24HoursFlag = true;
	}
	else
	{						
		vectorLayerSURPIC24Hours.setVisible(true);		
		surpicButtonText = "Hide Active Surpic";
		 $("#hideViewSurpicLayerButton").html(surpicButtonText);
		//$("#hideViewSurpicLayerButton").prop("value", "Hide Active Surpic");
		viewSurpic24HoursFlag = false;
	}
}

function displayArchievedSURPICAreaViewTable(messageIdsSurpicArea)
{
	try{		
		 
		if (jq.fn.DataTable.isDataTable( '#sarAreaViewRightTable' ) ) {
			
			 jq('#sarAreaViewRightTable').DataTable().destroy();
			 $('#sarAreaViewRightTable').html("");
			 $('#sarAreaViewRightTableBody').html("");
			 
			 var tabkeHeader = "<thead><tr><th style=\"padding-left:10px;\"><input type=\"checkbox\"id=\"checkBoxSelectAllMessageIds\"  name=\"checkBoxSelectAllMessageName\" \" /></th>" +
				"<th>Message Id</th>" +
				"</tr></thead><tbody id=\"sarAreaViewRightTableBody\"></tbody>"
		
				$('#sarAreaViewRightTable').append(tabkeHeader);
				
				for(var i = 0; i <= messageIdsSurpicArea.length-1; i++) {					
									
					$('#sarAreaViewRightTableBody').append('<tr><td><input type=\"checkbox\" name=\"checkbox\" id = \"' + messageIdsSurpicArea[i].trim() +
							'_sarAreaMsgId\"/></td><td>'+ messageIdsSurpicArea[i] +'</td>' +		    						    		
							'</tr>');	 				
				}
			
				var table=  jq('#sarAreaViewRightTable').DataTable( {  	
						
					'columnDefs': [
	    				  {
	    				      "targets": [0,1], // your case first column
	    				      "className": "text-left",	
	    				       
	    				 },	    				
	    				 ],
	    			"scrollY":        "150px",
		 	        "scrollCollapse": true,
		 	        "paging":         false,		 	       
		 	        "info":     false,
		 	         "ordering": false,		 	       
			 	    } );
		}
		else
		{			
			
			 var tabkeHeader = "<thead><tr><th style=\"padding-left:10px;\"><input type=\"checkbox\"id=\"checkBoxSelectAllMessageIds\"  name=\"checkBoxSelectAllMessageName\" \" /></th>" +
				"<th>Message Id</th>" +
				"</tr></thead><tbody id=\"sarAreaViewRightTableBody\"></tbody>"
		
				$('#sarAreaViewRightTable').append(tabkeHeader);
			
			 for(var i = 0; i <= messageIdsSurpicArea.length-1; i++) {					
					
					$('#sarAreaViewRightTableBody').append('<tr><td><input type=\"checkbox\" name=\"checkbox\" id = \"' + messageIdsSurpicArea[i].trim() +
							'_sarAreaMsgId\"/></td><td>'+ messageIdsSurpicArea[i] +'</td>' +		    						    		
							'</tr>');	 				
				}
			
				var table=  jq('#sarAreaViewRightTable').DataTable( {   	
					 'columnDefs': [
	    				  {
	    				      "targets": [0,1], // your case first column
	    				      "className": "text-left",	
	    				       
	    				 },	    				
	    				 ],
	    			"scrollY":        "150px",
		 	        "scrollCollapse": true,
		 	        "paging":         false,		 	       
		 	        "info":     false,
		 	        "ordering": false,	 	        
			 	       
			 	    } );			
				
				
		}
		selectAllSarAreaViewMessageIds();
	}
	catch(err)
	{
		console.info('error in displayArchievedSURPICAreaViewTable: ' + err)
	}
}//function searchShipPortLayer(response)




function selectAllSarAreaViewMessageIds()
{
	try{
	// handle select all check box controls for sar area view/archived countrty List
		$("#checkBoxSelectAllMessageIds").change(function() {		
			
		    if(this.checked) {	        
		       	 $.each( messageIdsSurpicArea, function( key, value ) {		   		  
		   		  if ($("#" + value.trim() + "_sarAreaMsgId").is(':enabled'))
		   		  	$("#" + value.trim() + "_sarAreaMsgId").prop("checked", true);
		       	 });
		            
	            $(this).prop("checked", true);	            
	        }
	        else
        	{
	        	$.each( messageIdsSurpicArea, function( key, value ) {	   		  	  
			   		  $("#" + value.trim() + "_sarAreaMsgId").prop("checked", false);
			       	 });
			            
	            $(this).prop("checked", false);	        	
        	}	               
	    });   
	}
	catch(err)
	{
		console.info('error in selectAllSarAreaViewCountry: ' + err)
	}
}


function displaySarAreaViewForm() {	
	try{   
		var element = document.createElement('div');	          	
		element.id = "sarAreaViewInnerRight";
		
		element.innerHTML = "<div id= \"mainSarAreaDiv\" class=\"form-group\">"+  					
	    "<label>Select Country</label>" +	
	    "<table  class=\"table table-condensed\">" +
	    "<tr> <th> <input type=\"checkbox\" id=\"checkboxSarAreaViewCountryList\" />  </th>" + 
	    "<th style=\"Width:90px\">Select All </th> </tr> </table>" +   
	    "</div>";  
	  
		var element2 = document.createElement('div');
		element2.className = "sarAreaTableBodyStyle";
		element2.innerHTML = "<table  class=\"table table-condensed\">" +   
	    "<tbody id=\"sarAreaViewTableBody\" > </tbody></table>";
	    
	   document.getElementById("sarAreaViewRight").appendChild(element);
	   document.getElementById("mainSarAreaDiv").appendChild(element2);
	 
	   for(var j=0; j<lritIdDisplay.length;j++)
		{			
		   		$('#sarAreaViewTableBody').append('<tr><td> <input type=\"checkbox\" name=\"Layer\" id = \"' +
				   lritIdDisplay[j].trim()  + '_sarAreacountry\"/>  </td> '+
					'<td style=\"Width:100px\">' + lritNameDisplay[j] + '</td></tr>');
		}		
	}	
	catch(err)
	{
		console.info('error in displaySarAreaViewForm: ' + err)
	}
}



function distanceBearingfunc()
{
	optionDistanceValue = document.getElementsByName('distanceBearingOptionsRadios');		
	var selectedDistanceOption;
	 for(i = 0; i < optionDistanceValue.length; i++) { 
        if(optionDistanceValue[i].checked) 
	   	 {
        	selectedDistanceOption = optionDistanceValue[i].value;
	   	 }
	 }	
	 if(selectedDistanceOption==="distance")
	 {
		 
			 enableMeasuringTool();
			 distanceBearingFlag = true;		
	 }
	 else
	 {
		 
		 var setDiv = document.getElementById("ringDistanceInfo"); //pattern=\"[0-9][0-9]{1,}\"
		  setDiv.innerHTML = "Ring Distance in NM &nbsp; <input type=\"text\" value= \"1\"   maxlength=\"5\" class = \"textRingDistance\" name=\"textRingDistance\" id=\"idRingDistance\">";				

		 
		 
		 $('#ringDistanceInfo').show();
		 addInteractionRangeRing();
		 distanceBearingFlag = true;
		 
	 }
}

function getCgNameByCgLritId(lritId)
{
	try{
		for(var j=0; j<lritIdDisplay.length; j++)
		{
			if(lritId.trim()== lritIdDisplay[j].trim())
			{	
				return lritNameDisplay[j];
			}
		}
	}
	catch(err)
	{
		console.info('error in getCgNameByCgLritId: ' + err)
	}
}

function adjustMapHeight()
{
	try{
	
		 var viewHeight = $(window).height();		
		 var offsetHeightMapTitleBox = document.getElementById('mapTitleBox').offsetHeight;
		 var offsetHeightFooter = document.getElementById('MainFooter').offsetHeight;
		 var offsetWidthcontent = document.getElementById('MainContent').offsetWidth;
		 var viewWidth = $(window).width();
		 
		 var offsetHeightHeader = document.getElementById('header-img').offsetHeight;
		 var offsetHeightLatLonLabel = document.getElementById('lonLatInfo').offsetHeight;
		 var offsetHeightHorzMapButton = document.getElementById('HorzButtonMap').offsetHeight;
		 var offsetHeightScaleLine = document.getElementById('scale-line').offsetHeight;
		 var offsetHeightOpenStreet = document.getElementById('creditOpenStreetMap').offsetHeight;
		 var offsetHeightLatLonSearch = document.getElementById('latlonSearch').offsetHeight;
		 
		 var offsetWidthSideBar = document.getElementById('sidebar-img').offsetWidth;						
		 var offsetWidthrightSideBar = document.getElementById('rightSideBar').offsetWidth;
		 
		if(fullScreenFlag==true)
		 {
			 offsetHeightHeader = 0;
			 offsetHeightFooter = 0;
			 offsetWidthSideBar = 0;						
			 offsetWidthrightSideBar = 0;
		 }
		 
		 var mapHeight = viewHeight-offsetHeightHeader-offsetHeightMapTitleBox-offsetHeightFooter-offsetHeightHorzMapButton-offsetHeightScaleLine-offsetHeightOpenStreet-offsetHeightLatLonSearch;
		 var mapWidth = viewWidth - offsetWidthSideBar - offsetWidthrightSideBar;				 
		 var mapCurrentSize = map.getSize();				
	     map.setSize([mapWidth, mapHeight]);			   
	     var offset = offsetHeightHeader-offsetHeightMapTitleBox;
	     $('.ol-custom:visible').css('top',0);
	     $('.ol-custom:visible').css('height',mapHeight-5);
	     var mapDivHeight = viewHeight-offsetHeightHeader-offsetHeightFooter;			     		    
	     setTimeout(function(){ map.updateSize(); }, 300); 
	     //$('#fullscreen').width(mapWidth);
	       }
	catch(err)
	 {
		console.info('error in adjustMapHeight: ' + err)
	 }
}

function searchLatLonPoint()
{
	vectorSource_Temp.clear();
	
	if(($('#idLonDegSearch').val().length >0) && ($('#idLonMinSearch').val().length >0) && ($('#idLonSecSearch').val().length >0)) {			
		var str1= $('#idLonDegSearch').val()+String.fromCharCode(176);
		var convertedValueLon = DegMinSecToLatLon($('#idLonDegSearch').val(),$('#idLonMinSearch').val(),$('#idLonSecSearch').val(),$("#lonSign").val());
		var convertedValueLat = DegMinSecToLatLon($('#idLatDegSearch').val(),$('#idLatMinSearch').val(),$('#idLatSecSearch').val(),$("#latSign").val());
		
		var featuresSelectedPort =  new ol.Feature({
			name: "POINT",
	    	geometry: new ol.geom.Point([convertedValueLon, convertedValueLat] ),
		});
		
		var focusPositionStyle = new ol.style.Style({	    
		   	image: new ol.style.Circle({		             
		   		 radius: 1,
		   		 stroke: new ol.style.Stroke({
		   	     color: 'blue',
		   	     width: 1
		   		 }),
		   		 fill: new ol.style.Fill({
		   	      color: 'blue'
		   	       }),	    	     
		   	 })
		    });		 
		
		featuresSelectedPort.setStyle(focusPositionStyle);
		vectorLayer_SelectedCountryShipPosition.getSource().addFeature(featuresSelectedPort.clone());
		 vectorSource_Temp.addFeature(featuresSelectedPort.clone());
		 
		 
		
		 
		 
		 var extent = vectorSource_Temp.getExtent();
			//console.info(extent);
			if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
			 { 
	    		 var diffExtent = extent[2]- extent[0];
	    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
	    		 {
	    			 var center = ol.extent.getCenter(extent);							
	    			 map.getView().setCenter(center);
	    		 }
	    		 else
	    			 map.getView().fit(extent, map.getSize());	    		
			 }
	}
	
}

function enableFVStatusOptionStatus(){
	var option = $("input[name='FVStatusOptionVesselType']:checked").val();
	
	if(option === 'foreignVessels')		
	{
		$("input[id^=FVStatusOptionStatus]:radio").attr('disabled',true);
		
	}
	else
	{
		$('input:radio[id^=FVStatusOptionStatus]:nth(1)').attr('checked',true);	
		$("input[id^=FVStatusOptionStatus]:radio").attr('disabled',false);
	}
}


// function to disable checkboxes whose values does not exit in db
$(document).ready(function(){	   
	
	countryListIds = countryListIds.replace("[","");
	countryListIds = countryListIds.replace("]","");
	 lritIdDisplay = countryListIds.split(',');
	 
	 countryListName = countryListName.replace("[","");
	 countryListName = countryListName.replace("]","");
	 lritNameDisplay = countryListName.split(',');	
	for(var x=0; x<lritNameDisplay.length;x++)
		{
		lritNameDisplay[x] = lritNameDisplay[x].replace("*",",");
		}
	 
	
	
	
	$('#start').daterangepicker({
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		parentEl: '#shipHistoryForm',
		singleDatePicker : true
	}

	);
	
	$('#end').daterangepicker({
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		parentEl: '#shipHistoryForm',
		singleDatePicker : true
	});


	$('#start').on('apply.daterangepicker', function(ev, picker) {
		
		  startDate = picker.startDate.format('YYYY-MM-DD H:mm:ss');				  
		 
		 
		});
	$('#end').on('apply.daterangepicker', function(ev, picker) {				
		 
		  endDate = picker.endDate.format('YYYY-MM-DD H:mm:ss');
		  
		  if(archivedSurpicButtonFlag === true)
		  {  displayArchievedSURPICAreaView();
		  	 
		  }
		 
		});
	
	$('#startSar').daterangepicker({
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		parentEl: '#sarAreaViewModal',
		singleDatePicker : true
	}

	);
	
	$('#endSar').daterangepicker({
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		parentEl: '#sarAreaViewModal',
		singleDatePicker : true
	});


	$('#startSar').on('apply.daterangepicker', function(ev, picker) {
		
		  startDate = picker.startDate.format('YYYY-MM-DD H:mm:ss');				  
		 
		 
		});
	$('#endSar').on('apply.daterangepicker', function(ev, picker) {				
		 
		  endDate = picker.endDate.format('YYYY-MM-DD H:mm:ss');
		  
		  if(archivedSurpicButtonFlag === true)
		  {  displayArchievedSURPICAreaView();
		  	 
		  }
		 
		});
	
	
	// displaySarAreaViewForm();	
	 $('#ringDistanceInfo').hide();
	// $("#distanceTableId").draggable();	 
	// $("#distanceTableId").hide();
	
	/* var elementHistory = document.createElement('div');	          	
	 elementHistory.id = "historyDateRange";
	 elementHistory.className = "form-group";
		
	 elementHistory.innerHTML = "<label>Date and time range:</label>"+  					
	    "<div class=\"input-group\">" +	
	    "<div class=\"input-group-addon\">" +
	    " <i class=\"fa fa-clock-o\"></i> </div>" + 
	    "<input type=\"text\" class=\"form-control pull-right\" name=\"datetimes\" id=\"historytime\">" +   
	    "</div>";  */
	 
	//-------------------------------------------------------------------------------
	// handle select all check box controls
	//-------------------------------------------------------------------------------
	 
	var len = countryListIds.split(',');
	var dvjQuery = $("#sarAreaViewInnerRight").detach();
	 // DESELCT ALL ON START
	 function deselectAllOnStart() {
		 try{
	       	 $.each( countryBoundaryList, function( key, value ) {		   		  
	   		  if ($("#" + countryBoundaryList[key].cg_lritid + "_country").is(':enabled'))
	   		  	$("#" + countryBoundaryList[key].cg_lritid + "_country").prop("checked", false);
	       	 });	
		 }
		 catch(err)
		{
			console.info('error in deselectAllOnStart: ' + err)
		}
    };
	 
	   // SAR Area View Checkbox Selection 
	    $("#sarRegionArea").change(function() {
	    	 try{
		        if(this.checked) {
		        	$("#startSar").val("");
	        		$("#endSar").val("");
		        	if($("#sarRegionAreaVessel").prop("checked") == false)
	        		{
		        		//$("#sarAreaViewRight").append(dvjQuery);	
		        	 	//document.getElementById("sarAreaViewInnerRight").appendChild(document.getElementById("historyDateRange"));
		        		archivedSurpicButtonFlag = true;
		        		$('#starttimeSar').show();
						$('#endtimeSar').show();
						$(this).prop("checked", true); 
		        		//document.getElementById("sarAreaViewRight").appendChild(document.getElementById("historyDateRange"));
		        	 	//selectAllSarAreaViewCountry();
		        	 	
	        		}
		        	       
		        }
		        else
	        	{ 
		        	/*if($("#sarRegionAreaVessel").prop("checked") == false)
		        		dvjQuery = $("#sarAreaViewInnerRight").detach();*/
		        	if($("#sarRegionAreaVessel").prop("checked") == false)
	        		{
		        		$('#starttimeSar').hide();
						$('#endtimeSar').hide();
						archivedSurpicButtonFlag = false;
	        		}
					 
		        	$(this).prop("checked", false);	        	
	        	}
	    	 }
			 catch(err)
			 {
				console.info('error in sarRegionArea change: ' + err)
			 }  
	    });  
	    
	    $("#sarRegionAreaVessel").change(function() {
	    	 try{
		        if(this.checked) {
		        	$("#startSar").val("");
	        		$("#endSar").val("");
		        	if($("#sarRegionArea").prop("checked") == false)
		        	{
		        	//	$("#sarAreaViewRight").html(dvjQuery);	
		        	//	document.getElementById("sarAreaViewInnerRight").appendChild(document.getElementById("historyDateRange"));
		        		archivedSurpicButtonFlag = true;		        		
		        		$('#starttimeSar').show();
						$('#endtimeSar').show();
		        		//document.getElementById("sarAreaViewRight").appendChild(document.getElementById("historyDateRange"));
		        		//selectAllSarAreaViewCountry();
		        	}
		        	 
		            $(this).prop("checked", true);	            
		        }
		        else
	        	{ 
		        	//if($("#sarRegionArea").prop("checked") == false)
		        	//	dvjQuery = $("#sarAreaViewInnerRight").detach();
		        	if($("#sarRegionArea").prop("checked") == false)
	        		{
		        		$('#starttimeSar').hide();
						$('#endtimeSar').hide();
						archivedSurpicButtonFlag = false;	
	        		}
					 
	            	$(this).prop("checked", false);	        	
	        	}
	    	 }
			 catch(err)
			 {
				console.info('error in sarRegionAreaVessel change: ' + err)
			 }         
	    });  
	    
	 // End of SAR Area View Checkbox Selection  	    
	    
	    // ocean Region Selection Function
	    $("#indianOceanRegionId").change(function() {
	    	 try{
		    	 if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	   
		    		 selectedOceanRegionList.push("Indian");
		        }
		    	 else
	    		 {
		    		 $(this).prop("checked", false);
		    		 var index = selectedOceanRegionList.indexOf("Indian");
		    		 if (index > -1) {
		    			 selectedOceanRegionList.splice(index, 1);
		    		 }
	    		 }
	    	 }
			 catch(err)
			 {
				console.info('error in indianOceanRegionId change: ' + err)
			 }   
	    });
	    
	    $("#pacificOceanRegionId").change(function() {
	    	 try{
	    		if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	   
		    		 selectedOceanRegionList.push("Pacific");
		        }
		    	 else
		   		 {
		    		 $(this).prop("checked", false);	   
		    		 var index = selectedOceanRegionList.indexOf("Pacific");
		    		 if (index > -1) {
		    			 selectedOceanRegionList.splice(index, 1);
		    		 }
		   		 }
	    	 }
			 catch(err)
			 {
				console.info('error in pacificOceanRegionId change: ' + err)
			 }  
	    });
	    
	    $("#atlanticOceanRegionEastId").change(function() {
	    	try{
		    	if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	 
		    		 selectedOceanRegionList.push("AtlanticEast");
		        }
		    	 else
		    	 {
		    		 $(this).prop("checked", false);	
		    		 var index = selectedOceanRegionList.indexOf("AtlanticEast");
		    		 if (index > -1) {
		    			 selectedOceanRegionList.splice(index, 1);
		    		 }
		    	 }
	    	 }
			 catch(err)
			 {
				console.info('error in atlanticOceanRegionEastId change: ' + err)
			 }  
	    });
	    
	    $("#atlanticOceanRegionWestId").change(function() {
	    	try{
		    	if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	   
		    		 selectedOceanRegionList.push("AtlanticWest");
		        }
		    	else
		    	{
		    		 $(this).prop("checked", false);	
		    		 var index = selectedOceanRegionList.indexOf("AtlanticWest");
		    		 if (index > -1) {
		    			 selectedOceanRegionList.splice(index, 1);
		    		 }
		    	}
	    	 }
			 catch(err)
			 {
				console.info('error in atlanticOceanRegionWestId change: ' + err)
			 }  
	    });
	    
	    
	    //Geographical Layer Selection Function	    
	    $("#displayLakes").change(function() {
	    	try{
		    	 if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	   
		    		 selectedGeographicalLayersList.push("Lakes");
		        }
		    	 else
		    	 {
		    		 $(this).prop("checked", false);
		    		 var index = selectedGeographicalLayersList.indexOf("Lakes");
		    		 if (index > -1) {
		    			 selectedGeographicalLayersList.splice(index, 1);
		    		 }
		    	 }
	    	}
			 catch(err)
			 {
				console.info('error in displayLakes change: ' + err)
			 }  
	    });
	    
	    $("#displaySeas").change(function() {
	    	try{
		    	if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	   
		    		 selectedGeographicalLayersList.push("Seas");
		        }
		    	else
		    	{
		    		 $(this).prop("checked", false);
		    		 var index = selectedGeographicalLayersList.indexOf("Seas");
		    		 if (index > -1) {
		    			 selectedGeographicalLayersList.splice(index, 1);
		    		 }
		    	}
	    	}
			 catch(err)
			 {
				console.info('error in displaySeas change: ' + err)
			 }  
	    });
	    
	    $("#displayBays").change(function() {
	    	try{
		    	if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	   
		    		 selectedGeographicalLayersList.push("Bays");
		        }
		    	else
		    	{
		    		 $(this).prop("checked", false);
		    		 var index = selectedGeographicalLayersList.indexOf("Bays");
		    		 if (index > -1) {
		    			 selectedGeographicalLayersList.splice(index, 1);
		    		 }
		    	}
	    	}
			 catch(err)
			 {
				console.info('error in displayBays change: ' + err)
			 }  
	    });
	    
	    $("#displayLightHouse").change(function() {
	    	try{
		    	if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	   
		    		 selectedGeographicalLayersList.push("LightHouse");
		        }
		    	else
		    	{
		    		 $(this).prop("checked", false);
		    		 var index = selectedGeographicalLayersList.indexOf("LightHouse");
		    		 if (index > -1) {
		    			 selectedGeographicalLayersList.splice(index, 1);
		    		 }
		    	}
	    	}
			 catch(err)
			 {
				console.info('error in displayLightHouse change: ' + err)
			 }  
	    });
	    
	    $("#displayShipWreks").change(function() {
	    	try{
		    	 if(this.checked) 
		    	 {
		    		 $(this).prop("checked", true);	   
		    		 selectedGeographicalLayersList.push("ShipWreks");
		    	 }
		    	 else
		    	 {
		    		 $(this).prop("checked", false);
		    		 var index = selectedGeographicalLayersList.indexOf("ShipWreks");
		    		 if (index > -1) {
		    			 selectedGeographicalLayersList.splice(index, 1);
		    		 }
			 	}
	    	}
			 catch(err)
			 {
				console.info('error in displayShipWreks change: ' + err)
			 }  
	    });
	    
	    $("#displayBethyLines").change(function() {
	    	try{
		    	 if(this.checked) 
		    	 {
		    		 $(this).prop("checked", true);	   
		    		 selectedGeographicalLayersList.push("BethyLines");
	       		 }
		    	 else
		    	 {
		    		 $(this).prop("checked", false);
		    		 var index = selectedGeographicalLayersList.indexOf("BethyLines");
		    		 if (index > -1) {
		    			 selectedGeographicalLayersList.splice(index, 1);
		    		 }
		    	 }
	    	}
			 catch(err)
			 {
				console.info('error in displayBethyLines change: ' + err)
			 }  
	    });
	    
	    $("#displayOceans").change(function() {
	    	try{
		    	 if(this.checked) 
		         {
		    		 $(this).prop("checked", true);	   
		    		 selectedGeographicalLayersList.push("Oceans");
		         }
		    	 else
		    	 {
		    		 $(this).prop("checked", false);
		    		 var index = selectedGeographicalLayersList.indexOf("Oceans");
		    		 if (index > -1) {
		    			 selectedGeographicalLayersList.splice(index, 1);
		    		 }
		    	 }
	    	 }
			 catch(err)
			 {
				console.info('error in displayOceans change: ' + err)
			 }  
	    });
	    
	    $("#displayGeographicalPorts").change(function() {
	    	try{
		    	if(this.checked) 
		        {
		    		 $(this).prop("checked", true);	   
		    		 selectedGeographicalLayersList.push("GeographicalPorts");
		        }
		    	else
		    	{
		    		 $(this).prop("checked", false);
		    		 var index = selectedGeographicalLayersList.indexOf("GeographicalPorts");
		    		 if (index > -1) {
		    			 selectedGeographicalLayersList.splice(index, 1);
		    		 }
		    	}
	    	}
			 catch(err)
			 {
				console.info('error in displayGeographicalPorts change: ' + err)
			 }  
	    });
	    
	    
	// handle select all check box controls for countrty List
	 $("#checkboxCountryList").change(function() {
		 try{
		        if(this.checked) 
		        {
		             $.each( countryBoundaryList, function( key, value ) {		   		  
			   		  if ($("#" + countryBoundaryList[key].cg_lritid+ "_country").is(':enabled'))
			   		  	$("#" + countryBoundaryList[key].cg_lritid + "_country").prop("checked", true);
			       	 });
			            
			            $(this).prop("checked", true);	            
		        }
		        else
	        	{
		        	$.each( countryBoundaryList, function( key, value ) {		   		  
				   		 $("#" + countryBoundaryList[key].cg_lritid + "_country").prop("checked", false);
			       	 });
				            
		        		$(this).prop("checked", false);	        	
	        	}
		 }
		 catch(err)
		 {
			console.info('error in checkboxCountryList change: ' + err)
		 }         
	    });
	 
	// handle select all check box controls for countrty List
	 $("#checkboxCountryBoundaryList").change(function() {
		 try{
	        if(this.checked) 
	        {
	             $.each( countryBoundaryList, function( key, value ) {		   		  
		   		  if ($("#" + countryBoundaryList[key].cg_lritid+ "_countryBoundary").is(':enabled'))
		   		  	$("#" + countryBoundaryList[key].cg_lritid + "_countryBoundary").prop("checked", true);
		       	 });
		            
		            $(this).prop("checked", true);	            
	        }
	        else
        	{
	        	$.each( countryBoundaryList, function( key, value ) {		   		  
			   		 $("#" + countryBoundaryList[key].cg_lritid + "_countryBoundary").prop("checked", false);
		       	 });
			            
	        		$(this).prop("checked", false);	        	
        	}
		 }
		 catch(err)
		 {
			console.info('error in checkboxCountryBoundaryList change: ' + err)
		 }           
	    });
	 
	 
	// handle select all check box controls for Shipping Company List
	 $("#checkboxCompanyName").change(function() {
		 try{
		        if(this.checked) {
		        
		       	 $.each( companyNameList, function( key, value ) {		   		  
		   		  if ($("#" + companyNameList[key].companyCode.trim() + "_company").is(':enabled'))
		   		  	$("#" + companyNameList[key].companyCode.trim() + "_company").prop("checked", true);
		       	 });
		            
		            $(this).prop("checked", true);	            
		        }
		        else
	        	{
		        	$.each( companyNameList, function( key, value ) {		  
			   		  	$("#" + companyNameList[key].companyCode.trim() + "_company").prop("checked", false);
			       	 });			            
		            	$(this).prop("checked", false);	        	
	        	}
		 }
		 catch(err)
		 {
			console.info('error in checkboxCompanyName change: ' + err)
		 }           
	    });
	
	// handle select all check box controls for waterpolygon List
	$("#checkbox1").change(function() {
		try{
	        if(this.checked) {
	        
	       	 $.each( len, function( key, value ) {		   		  
	   		  if ($("#" + value.trim() + "_1").is(':enabled'))
	   		  	$("#" + value.trim() + "_1").prop("checked", true);
	       	 });
	            
	            $(this).prop("checked", true);            
	        }
        	else
        	{
        	$.each( len, function( key, value ) {	   		  
	   		  	$("#" + value.trim() + "_1").prop("checked", false);
	       	 });		            
	            $(this).prop("checked", false);
        	
        	}
		}
		 catch(err)
		 {
			console.info('error in checkbox1 waterpolygon change: ' + err)
		 }        
    });
	
	$("#checkbox2").change(function() {
		try{
	        if(this.checked) {          
	         $.each( len, function( key, value ) {
	   		  
	        	 if ($("#" + value.trim() + "_2").is(':enabled'))
	   		  		$("#" + value.trim() + "_2").prop("checked", true);
	       	 });
	            
	            $(this).prop("checked", true);
	            
	        }
	        else
	    	{
	    	$.each( len, function( key, value ) {		   		  
		   		  
		   		  $("#" + value.trim() + "_2").prop("checked", false);
		       	 });
		            
		            $(this).prop("checked", false);
	    	
	    	}
		}
		 catch(err)
		 {
			console.info('error in checkbox2 waterpolygon change: ' + err)
		 }        
    });
	
	$("#checkbox3").change(function() {
		try{
	        if(this.checked) {
	        	
	       	 $.each( len, function( key, value ) {
	   		  
	       		 if ($("#" + value.trim() + "_3").is(':enabled'))
	   		  		$("#" + value.trim() + "_3").prop("checked", true);
	       	 });
	            
	            $(this).prop("checked", true);
	            
	        }
	        else
	    	{
	    	$.each( len, function( key, value ) {		   		  
		   		  
		   		  $("#" + value.trim() + "_3").prop("checked", false);
		       	 });
		            
		            $(this).prop("checked", false);	    	
	    	}
		 }
		 catch(err)
		 {
			console.info('error in checkbox3 waterpolygon change: ' + err)
		 }         
    });
	
	$("#checkbox4").change(function() {
		try{
	        if(this.checked) {
	         
	       	 $.each( len, function( key, value ) {
	   		  
	       		 if ($("#" + value.trim() + "_4").is(':enabled'))
	   		  		$("#" + value.trim() + "_4").prop("checked", true);
	       	 });
	            
	            $(this).prop("checked", true);
	            
	        }
	        else
	    	{
	    	$.each( len, function( key, value ) {		   		  
		   		  
		   		  $("#" + value.trim() + "_4").prop("checked", false);
		       	 });
		            
		            $(this).prop("checked", false);	    	
	    	}
		}
		 catch(err)
		 {
			console.info('error in checkbox4 waterpolygon change: ' + err)
		 }      
    });
	
	$("#checkbox5").change(function() {
		try{
	        if(this.checked) {
	       
	       	 $.each( len, function( key, value ) {
	   		  
	       		 if ($("#" + value.trim() + "_5").is(':enabled'))
	   		  		$("#" + value.trim() + "_5").prop("checked", true);
	       	 });
	            
	            $(this).prop("checked", true);
	            
	        }
	        else
	    	{
	    	$.each( len, function( key, value ) {		   		  
		   		  
		   		  $("#" + value.trim() + "_5").prop("checked", false);
		       	 });
		            
		            $(this).prop("checked", false);
	    	
	    	}
		}
		 catch(err)
		 {
			console.info('error in checkbox5 waterpolygon change: ' + err)
		 }       
    });
	
	
	$("#checkbox6").change(function() {
		try{
	        if(this.checked) {
	       
	       	 $.each( len, function( key, value ) {
	   		  
	       		 if ($("#" + value.trim() + "_6").is(':enabled'))
	   		  		$("#" + value.trim() + "_6").prop("checked", true);
	       	 });
	            
	            $(this).prop("checked", true);
	            
	        }
	        else
	    	{
	    	$.each( len, function( key, value ) {		   		  
		   		  
		   		  $("#" + value.trim() + "_6").prop("checked", false);
		       	 });
		            
		            $(this).prop("checked", false);
	    	
	    	}
		}
		 catch(err)
		 {
			console.info('error in checkbox6 port change: ' + err)
		 }       
    });
	
	//selectAllSarAreaViewMessageIds();


	//-------------------------------------------------------------------------------
	// End of handle select all check box controls
	//-------------------------------------------------------------------------------
	
	
	//-------------------------------------------------------------------------------
	//Get the value of the selected option button(search Ship or Port)	
	
	
	//var searchTypeChecked;	 
	//$("#searchType1").prop("checked", true);
	//$("#searchType2").prop("checked", false);
	
	document.getElementsByName('searchType')[0].checked = true;
	document.getElementsByName('searchType')[1].checked = false;
	$(document).on('change','#searchType', function(){ 	
		try{
			
			 var optionSearchShipPort = document.getElementsByName('searchType');	    	 
			
			 for(var i = 0; i < optionSearchShipPort.length; i++) { 
		           if(optionSearchShipPort[i].checked)	           
		           	searchTypeChecked = optionSearchShipPort[i].value;
			 } 		 
			 resetSearchShipPortForm();
		 }
		 catch(err)
		 {
			console.info('error in searchType change: ' + err)
		 }   
	});

	//Get the value on change in combobox check button	
	var flagAllSelect;

	$(document).on('click','#checkBoxSelectAll', function(){ 
		try{
		    if(this.checked) {    	   	   	
	   	   	 flagAllSelect = 1;
	   	   	 checkUncheckComboBoxes(flagAllSelect);	
	   	    }
	   	    else
   	    	{
	   	    	flagAllSelect = 0;	   	    	
	   	    	checkUncheckComboBoxes(flagAllSelect);			    	    	
   	    	}		
		 }
		 catch(err)
		 {
			console.info('error in checkBoxSelectAll click: ' + err)
		 }   
	})



	
	
    //Display the details of the port in table of the selected country
	$(document).on('change', "#searchCriteriaCombo", function(){
		try{	 
			
			 document.getElementById("idSearchValue").value="";		
			 if(searchTypeChecked == "port"){
				 
				 if ($.fn.DataTable.isDataTable('#searchShipTable') ) {
					 console.info('insidw $.fn.DataTable.isDataTable() ');
				 }
				 
				 
				 var portSearchValue = $("#searchCriteriaCombo :selected").val();
				 if(portSearchValue!="Select")
				 { $.ajax({
					    type: "POST",
					    traditional: true,
					    url: "/lrit/map/Port?cg_lritId="+ portSearchValue,				   
					    success:function(response) {	
					    	
					    	try{
					    		searchShipPortLayer(response);			
					    	 }
							 catch(err)
							 {
								console.info('error in searchCriteriaCombo change: ' + err)
							 }   
					      }			 		
					});	
				 }
				 else
				 {
					 if (jq.fn.DataTable.isDataTable( '#searchPortTable' ) ) {
						 $('#searchPortTable').html("");
							$('#searchPortTableBody').html("");
							jq('#searchPortTable').DataTable().destroy();			 	
							jq('#searchPortTable').empty();
					 }
					
				 }
			 }		
			 else
				 {
				 
				 if (jq.fn.DataTable.isDataTable( '#searchPortTable' ) ) {
					 $('#searchPortTable').html("");
						$('#searchPortTableBody').html("");
						jq('#searchPortTable').DataTable().destroy();			 	
						
				 }
				 }
			// $("#searchShipPortModal").modal('show');
		 }
		 catch(err)
		 {
			console.info('error in searchCriteriaCombo change: ' + err)
		 }   
	    });
	    
	//-------------------------------------------------------------------------------  
    //Search Ship and Port ends	 
	//-------------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------------
	// handle vertical tool box controls on map
	//-------------------------------------------------------------------------------
	var h5 = document.createElement("h5");
	h5.className = "modal-title";
	
	// Show & Hide vertical tool box Button 
	 $("#hideButton").click(function(){ 	
		 try{
			 $("#demo").toggle();
			 if($("#imageArrow").attr('src') === '\\lrit\\resources\\icons\\upArrow.png')
				 $("#imageArrow").attr("src","\\lrit\\resources\\icons\\downArrow.png");
			 else
				 $("#imageArrow").attr("src","\\lrit\\resources\\icons\\upArrow.png");
		 }
		 catch(err)
		 {
			console.info('error in hideButton click: ' + err)
		 }   
	 });
	
	
	// Tool Tip Button 
	 $("#toolTipButton").click(function(){ 
		 try{
			 stopPathProjection();
			 disableMeasuringTool();
			/* map.updateSize();
			 getAllCountries1000NMShipData();*/
			 
		 }
		 catch(err)
		 {
			console.info('error in toolTipButton click: ' + err)
		 }   
	 });
	 
	// Custom Polygon Button 
	 $("#customPolygonButton").click(function(){ 
		 try{
			 stopPathProjection();
			 disableMeasuringTool();			
			 openCustomPolygonForm();
		 }
		 catch(err)
		 {
			console.info('error in customPolygonButton click: ' + err)
		 }   
	 });
	 
	// Search Ship/Port Button 
	// displaySearchShipPortLayer
	
	 var input = document.getElementById("idSearchValue");
	 input.addEventListener("keypress", function(event) {	
		 try{
			 if(searchTypeChecked == "port")
				{	/*if (jq.fn.DataTable.isDataTable( '#searchShipPortTable' ) ) {
						jq('#searchShipPortTable').DataTable().search($(this).val()).draw() ;
					}*/	
				 	if (event.key === 'Enter')
				 	{
				 		event.preventDefault();
				 	}
				}
			 else{
				  if (event.key === 'Enter' && input.value.length>2) {					 
					  event.preventDefault();
					  document.getElementById("searchButton").click();
				  }
				  else if  (event.key === 'Enter')
				  {
					  event.preventDefault();
					  alert('Inavalid Entery');
				  }
			 }
		 }
		 catch(err)
		 {
			console.info('error in keypress for search ship/port: ' + err)
		 }   
	 });
	 
	 input.addEventListener("keyup", function(event) {
		 try{
			 
				 if(searchTypeChecked == "port")
					{	/*if (jq.fn.DataTable.isDataTable( '#searchShipPortTable' ) ) {
							jq('#searchShipPortTable').DataTable().search($(this).val()).draw() ;
						}	*/
					}
				 else{
					  if(input.value.length>2)
					  { 
						  document.getElementById("searchButton").disabled = false;
					  }
					  else
					  {
						 document.getElementById("searchButton").disabled = true;
						 /*if (jq.fn.DataTable.isDataTable( '#searchShipTable' ) ) {
							 $('#searchShipTable').html("");
								$('#searchShipTableBody').html("");
								jq('#searchShipTable').DataTable().destroy();			 	
								jq('#searchShipTable').empty();
						}*/
					  }
				 }
			 
		 }
		 catch(err)
		 {
			console.info('error in keyup for search ship/port: ' + err)
		 }  
	 });
	 var flagSearchShipPortButtonClick  = false;
	 $(document).on('click', 'input[type="checkbox"]', function(){  
   		{
		 	checkComboSelect();   		
   		}
	   });
	 
	 $("#searchShipPortButton").click(function(){ 
		 try{
			 stopPathProjection();
			 disableMeasuringTool();			
			// resetSearchShipPortForm();
			// flagSearchShipPortButtonClick = true;
			 $.ajax({
				    type: "POST",
				    traditional: true,
				    url: "/lrit/map/PortCountries",			    
				    success:function(response) {	
				    	try{
				    		portLritIds = response;		
				    	}
						 catch(err)
						 {
							console.info('error in searchShipPortButton click: ' + err)
						 }  
				      }		 		
				});
			 
			 $("#searchShipPortModal").modal('show');
		 }
		 catch(err)
		 {
			console.info('error in searchShipPortButton click: ' + err)
		 }  
	 });
	 
	//Country List Button 
	 var table;
	 $("#countryListButton").click(function(){ 
		 try{
			 disableMeasuringTool();
			 stopPathProjection();
			 $.ajax({
				    type: "POST",
				    traditional: true,
				    url: "/lrit/map/CountryBoundary",
				    data: {},
				    success:function(response) {
				        /* invoke your function */	
				    	 try{
				    		//jq('#searchShipByCountryTable').DataTable().destroy();
						   // $('#searchShipByCountryTableBody').html("");
					    	countryBoundaryList = response;
					    	
					    	displayCountriesShipsFrom();					    						    	
					    	if ( ! jq.fn.DataTable.isDataTable( '#searchShipByCountryTable' ) ) {
					    		 table = jq('#searchShipByCountryTable').DataTable( {	
					    			 
					    			// order: [[ 0, 'asc' ]],		
					    			 'columnDefs': [
					    				  {
					    				      "targets": [0,1], // your case first column
					    				      "className": "text-left",	
					    				       
					    				 }],
					    			"scrollY":        "150px",
						 	        "scrollCollapse": true,
						 	        "paging":         false,
						 	        "ordering": false,
						 	        "info":     false,
						 	        /*searching: true,
						 	        dom: 'tr'*/
						 	    } );	
					    	}
				    	 }
						 catch(err)
						 {
							console.info('error in countryListButton click: ' + err)
						 }  
				        // $( "#LayerForm" ).reset();
				      }			 		
				});
			
			 $("#countryList").modal('show');
			
		 }
		 catch(err)
		 {
			console.info('error in countryListButton click: ' + err)
		 }  
	 });
	
	 $('#countryList').on('shown.bs.modal', function () {
		 jq(jq.fn.dataTable.tables(true)).DataTable()
	      .columns.adjust();
	   });
	 
	 // Layer List Button: Display Countrywise Water Polygon
		 $("#waterPolyButton").click(function(){ 
			 try{
				 disableMeasuringTool();
				 stopPathProjection();				 
				// removeWaterPolygonLayer();
				// removeLayersFromMap('selectedPortLayer','name','POINT');
				// map.addLayer(vectorLayer_selectedPort);
				 $.ajax({
					    type: "POST",
					    traditional: true,
					    url: "/lrit/map/WaterPolygons",
					    data: {},
					    success:function(response) {
					    	 try{
						    	waterPolygonNameList = response;				    	
						    	displayWaterPolygonFrom();	
						    	if ( ! jq.fn.DataTable.isDataTable( '#waterPolygonsNameTable' ) ) {
						    		 table = jq('#waterPolygonsNameTable').DataTable( {	
						    			 
						    			 //order: [[ 0, 'asc' ]],		
						    			 'columnDefs': [
						    				  {
						    				      "targets": [0,1,2,3,4,5,6], // your case first column
						    				      "className": "text-left",	
						    				       
						    				 },
						    				 {
						    				      "targets": [0,1,2,3,4,5,6], // your case first column
						    				      "className": "select-checkbox",	
						    				       
						    				 }],
						    			"scrollY":        "150px",
							 	        "scrollCollapse": true,
							 	        "paging":         false,
							 	        "ordering": false,
							 	        "info":     false,
							 	       select: {
							 	            style:    'multi',
							 	            selector: 'td:first-child'
							 	        },
							 	        /*searching: true,
							 	        dom: 'tr'*/
							 	    } );	
						    	}
					    	 }
					    	 catch(err )
					    	 {
					    		 console.info('error in waterPolyButton click: ' + err)
					    	 }
					      }			 		
					});
				 				 
				 $("#waterPolygonList").modal('show');
			 }
			 catch(err)
			 {
				console.info('error in waterPolyButton click: ' + err)
			 }  
		 });
		 
		 $('#waterPolygonList').on('shown.bs.modal', function () {
			 jq(jq.fn.dataTable.tables(true)).DataTable()
		      .columns.adjust();
		   });
		 
		 //Display SAR Region
	     $("#sarRegionButton").click(function(){ 
	    	 try{
		    	 disableMeasuringTool();
		    	 stopPathProjection();
		    	 $(this).toggleClass("down");		    	 
		    	// getAllCountries1000NMShipData();
		    	 if(sarRegionButtonFlag==false)
	    		 {
		    		 showSARRegions();
		    		 sarRegionButtonFlag = true;
	    		 }
		    	 else
	    		 {
		    		 hideSARRegions();	    		 
		    		 sarRegionButtonFlag = false;
	    		 }
	    	 }
			 catch(err)
			 {
				console.info('error in sarRegionButton click: ' + err)
			 }  
		 });
		 
		// Distance and bearing tool	
	     
		 $("#distanceBearingButton").click(function(){ 	
			 
			 try{
				// disableMeasuringTool();
				 stopPathProjection();
				  if(distanceBearingFlag==true)
				 { 
					 disableMeasuringTool();
					 //distanceBearingFlag = false;
				 }
				 else
				{
					 $("#distanceBearingModal").modal('show');
					// enableMeasuringTool();
					 //distanceBearingFlag = true;
				 }
					 
			 }
			 catch(err)
			 {
				console.info('error in distanceBearingButton click: ' + err)
			 }  
		 });
		 
		// SAR Surpic View		 
		 $("#sarViewButton").click(function(){ 	
			 try{
				 disableMeasuringTool();
				 stopPathProjection();				
				// createSurpicRequest();	 
				 rightClickCoodinate = "";
				 rightClickSurpicRadius = "";
				 rightClickSurpicCenter = "";
				// window.open("../requests/surpicrequest", '_blank');		
				 $("#sarSurpicModal").modal('show');	
				// addInteractionSurpicRing();
			 }
			 catch(err)
			 {
				console.info('error in sarViewButton click: ' + err)
			 }  	
		 });
		 
		// Geographical Layer	 
		 $("#geographicalLayerButton").click(function(){
			 try{
				 disableMeasuringTool();
				 stopPathProjection();				
				 $("#displayGeographicalLayerssModalId").modal('show');
			 }
			 catch(err)
			 {
				console.info('error in geographicalLayerButton click: ' + err)
			 }  
		 });
		 
		// Grid List 
		 $("#gridListButton").click(function(){ 	
			 try{
				 disableMeasuringTool();
				 stopPathProjection();							 
				 $("#gridList").modal('show');
			 }
			 catch(err)
			 {
				console.info('error in gridListButton click: ' + err)
			 }  
		 });
		 
		// Vessel Status
		 $("#vesselStatusButton").click(function(){ 
			 try{
				 disableMeasuringTool();
				 stopPathProjection();
				 $("#filterVesselStatusModalId").modal('show');
				// resetFVStatusModal();
			 }
			 catch(err)
			 {
				console.info('error in vesselStatusButton click: ' + err)
			 }  
		 });
		 
		// Satellite Ocean Region
		 $("#oceanRegionButton").click(function(){ 		
			 try{
				 disableMeasuringTool();	
				 stopPathProjection();
			 	 removeSatelliteOceanRegionLayer();
				 $("#showSatelliteOceanRegionModal").modal('show');				 
			 }
			 catch(err)
			 {
				console.info('error in oceanRegionButton click: ' + err)
			 }  
		 });
		 
		// ship in country boundary
		 $("#shipInCountryBoundaryButton").click(function(){
			 try{
				 disableMeasuringTool();
				 stopPathProjection();
				 $.ajax({
					    type: "POST",
					    traditional: true,
					    url: "/lrit/map/CountryBoundary",
					    data: {},
					    success:function(response) {	
					    	 try{
						    	countryBoundaryList = response;				    	
						    	displayCountriesBoundaryShipsFrom();	
						    	if ( ! jq.fn.DataTable.isDataTable( '#searchShipByCountryBoundaryTable' ) ) {
						    		 table = jq('#searchShipByCountryBoundaryTable').DataTable( {							    			 
						    			  order: [],
						    			 'columnDefs': [
						    				// { "targets":[0, 1], "orderable": false, },
						    				 {
						    				      "targets": [0,1], 
						    				      "className": "text-left ",	
						    				 },						    				
						    				 ],
						    			"scrollY":        "150px",
							 	        "scrollCollapse": true,
							 	        "paging":         false,
							 	        "ordering": false,							 	       
							 	        "info":     false,
							 	        /*searching: true,
							 	        dom: 'tr'*/
							 	    } );	
						    	}
					    	 }
							 catch(err)
							 {
								console.info('error in shipInCountryBoundaryButton click: ' + err)
							 }  
					      }				 		
					});			 
				
				 $("#countryBoundaryList").modal('show');
			 }
			 catch(err)
			 {
				console.info('error in shipInCountryBoundaryButton click: ' + err)
			 }  
			
		 });
		 $('#countryBoundaryList').on('shown.bs.modal', function () {
			 jq(jq.fn.dataTable.tables(true)).DataTable()
		      .columns.adjust();
		   });	 
		// shipping Company
		 $("#shippingCompanyButton").click(function(){ 	
			 try{
				 disableMeasuringTool();
				 stopPathProjection();				 
				 $.ajax({
					    type: "POST",
					    traditional: true,
					    url: "/lrit/map/ShipCompany",
					    data: {'requestorsLritId': countryListIdsShips},
					    
					    success:function(response) {	
					    	try{
						    	companyNameList = response;			
						    	displayCompanyShipsFrom();		
						    	if ( ! jq.fn.DataTable.isDataTable( '#searchShipByCompanyTable' ) ) {						    		 
									
						    		 table = jq('#searchShipByCompanyTable').DataTable( {	
						    			 
						    			// order: [[ 0, 'asc' ]],		
						    			 'columnDefs': [
						    				  {
						    				      "targets": [1], // your case first column
						    				      "className": "text-left",	
						    				       
						    				 }],
						    			"scrollY":        "150px",
							 	        "scrollCollapse": true,
							 	        "paging":         false,
							 	        "ordering": false,
							 	        "info":     false,
							 	        /*searching: true,
							 	        dom: 'tr'*/
							 	    } );	
						    	}
					    	 }
							 catch(err)
							 {
								console.info('error in shippingCompanyButton click: ' + err)
							 }  
					      }
				 		
					});
				 $("#shipCompanyList").modal('show');
			 }
			 catch(err)
			 {
				console.info('error in shippingCompanyButton click: ' + err)
			 }  
		 });
		 $('#shipCompanyList').on('shown.bs.modal', function () {
			 jq(jq.fn.dataTable.tables(true)).DataTable()
		      .columns.adjust();
		   });
		
		// sar area view
		 $("#sarAreaViewButton").click(function(){ 	
			 try{
				 disableMeasuringTool();
				 stopPathProjection();
				 archivedSurpicButtonFlag = true;
				 $('#sarAreaViewRightTable').html("");
				 $('#sarAreaViewRightTableBody').html("");
				 
				 if (jq.fn.DataTable.isDataTable( '#sarAreaViewRightTable' ) ) {
					 jq('#sarAreaViewRightTable').DataTable().destroy();
					 $('#sarAreaViewRightTable').html("");
					 $('#sarAreaViewRightTableBody').html("");
				 }
				// $("#sarAreaViewRightTable").children().remove(); 
				 document.getElementById("displaySarAreaViewApplyButton").disabled = true;
				 
				 $("#startSar").val("");
	        	 $("#endSar").val("");				

				 $("#sarAreaViewModal").modal('show');
				// $("#starttimeSar").hide();
				// $("#endtimeSar").hide();
				 
			 }
			 catch(err)
			 {
				console.info('error in sarAreaViewButton click: ' + err)
			 }  
		 });
		 
		 //Display Ship History
	     $("#shipHistoryButton").click(function(){ 
	    	 try{
		    	 disableMeasuringTool();
		    	 stopPathProjection();		
		    	 archivedSurpicButtonFlag = false;
			  // to hide previous shown history
				//hideHistory('showHistoryLayer');
				historyShownFlag = false;
				
				//document.getElementById("historyTimeForm").appendChild(document.getElementById("historyDateRange"))
				//document.getElementById("shipHistoryMainForm").appendChild(document.getElementById("historyTimeForm"));
				document.getElementById("imoNumber").value = '';
				// $("#imoNumber").removeAttr("disabled"); 
				
				$("#start").val("");
	        	$("#end").val("");		
				$('#starttime').show();
				$('#endtime').show();
				$("#shipHistoryForm").modal('show'); 
	    	 }
			 catch(err)
			 {
				console.info('error in shipHistoryButton click: ' + err)
			 }  	
	     });
		 
	   //Display Path Projection
	     $("#pathProjectionButton").click(function(){
	    	 try{
		    	 $(this).toggleClass("down");
		    	 disableMeasuringTool();
		    	
				if (pathProjectionStartFlag==false)
				{ 
				  document.getElementById("PPimoNoVal").disabled = false;
				  document.getElementById("PPimoNoVal").value = "";
				  document.getElementById("pathLengthComboID").value = "5";
				  
			      $("#pathProjectionModal").modal('show');
			      flagPPRightClick = 0;	    
			      var setDiv = document.getElementById("ringDistanceInfo");
				  setDiv.innerHTML = "Time (in Hour) &nbsp; <input type=\"text\" value= \""+$("#pathLengthComboID").val()+"\" pattern=\"[0-9]{1,}\"  maxlength=\"5\" class = \"textRingDistance\" name=\"textRingDistance\" id=\"idRingDistance\">";				
				  pathProjectionLength =  $("#idRingDistance").val();
				 
				  $("#ringDistanceInfo").show();
			      pathProjectionStartFlag = true;
				}
				else
				{
					$("#ringDistanceInfo").hide();
					pathProjectionStartFlag = false;
					map.removeInteraction(draw);
					flagCircleInteraction =false;
				}
	    	 }
			 catch(err)
			 {
				console.info('error in pathProjectionButton click: ' + err)
			 }  	
	     });     
	     
	  // geographical Area
		 $("#geographicalAreaButton").click(function(){ 	
			 try{
				 disableMeasuringTool();
				 stopPathProjection();
				 addGeographicalArea();
			 }
			 catch(err)
			 {
				console.info('error in geographicalAreaButton click: ' + err)
			 }  
		 });
		
		 $("#fullScreenButton").click(function(){ 	
			 try{
				 if(fullScreenFlag==false)
				 {	
					// map.getView().setZoom(zoom-1);
					// map.getView().setCenter( [0, -15]);
					 fullScreenFlag = true;
				 }
				 else
				 {
					// map.getView().setZoom(zoom);
					// map.getView().setCenter(centerFocus);
					 fullScreenFlag = false;
				 }
			 }
			 catch(err)
			 {
				console.info('error in fullScreenButton click: ' + err)
			 }  
		 });
	   
	//-------------------------------------------------------------------------------
	// End of handle vertical tool box controls on map
	//-------------------------------------------------------------------------------	
		 
	//-------------------------------------------------------------------------------
	// handle Right click on map and ship
	//-------------------------------------------------------------------------------
	
	     	// Show history Selection on Right Click
			$("#rightClickDiv").on('click', 'li.showHistClass', function () {
				 try{
					historyShownFlag = false;
					document.getElementById("imoNumber").value = '';
					 $("#imoNumber").removeAttr("disabled");
					 
					$("#rightClickDiv").hide();
					document.getElementById("imoNumber").value = currentShip;
					hideHistory('showHistoryLayer'+currentShip);
					$("#imoNumber").attr("disabled", "disabled"); 
					$("#start").val("");
		        	$("#end").val("");		
					$('#starttime').show();
					$('#endtime').show();
					$("#shipHistoryForm").modal('show'); 			  	
					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Show history: ' + err)
				 }  
			});
			
			// Hide history Selection on Right Click
			$("#rightClickDiv").on('click', 'li.hideHistClass', function () {
				 try{
					hideHistory('showHistoryLayer');
					historyShownFlag = false;
					document.getElementById("imoNumber").value = '';
					 $("#imoNumber").removeAttr("disabled"); 
					$("#rightClickDiv").hide();
					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Hide history: ' + err)
				 }  
			});
			
			// Show Path Projection on Right Click
			$("#rightClickDiv").on('click', 'li.showPathClass', function () {
				try{			  	 
					flagPPRightClick = 1;
				      $("#pathProjectionModal").modal('show');			      
					  document.getElementById("PPimoNoVal").value = currentShip;
					  hideHistory("pathProjectionLayer" + currentShip)
					  $("#PPimoNoVal").attr("disabled", "disabled");
					  
					  //pathProjectionStartFlagfunc();			  	  
					$("#rightClickDiv").hide();				 			  	
					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Show Path Projection: ' + err)
				 }  
			});
			
			// Hide Path Projection on Right Click
			$("#rightClickDiv").on('click', 'li.hidePathClass', function () {	
				try{
					$("#rightClickDiv").hide();
					hideHistory("pathProjectionLayer")
					pathProjectionFlag = false;
					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Hide Path Projection: ' + err)
				 }  
			});
			var windowObjectReference;
			var strWindowFeatures = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";

			
			// Flag Selection on Right Click
			$("#rightClickDiv").on('click', 'li.flagClass', function () {	
				try{
					$("#rightClickDiv").hide();					
					rightClickShipData = AllShipStatusInfo.get(currentShip);	
					rightClickShipRequestType = 'Flag';
					//rightClickShipData = getShipInfoFromloadedShipData(currentShip);		
					
					
					/*function openRequestedPopup() {
					  windowObjectReference = window.open("http://www.cnn.com/", "CNN_WindowName", strWindowFeatures);
					}
					*/
					window.open("../requests/flagrequest", '_blank');	
					//window.open("../requests/flagrequest", "LRIT_WindowName", strWindowFeatures);		
				 }
				 catch(err)
				 {
					console.info('error in Right Click Flag Request: ' + err)
				 }  
			});
			
			
			// Flag Stop FSelection on Right Click
			$("#rightClickDiv").on('click', 'li.stopFlagClass', function () {		
				try{
					$("#rightClickDiv").hide();
					
					rightClickShipRequestType = 'Stop';
					rightClickShipData = AllShipStatusInfo.get(currentShip);	
					
					window.open("../requests/flagrequest", '_blank');	
					//window.open("../requests/flagrequest", "LRIT_WindowName", strWindowFeatures);
				 }
				 catch(err)
				 {
					console.info('error in Right Click Stop Request: ' + err)
				 }  
			});
			
			// Flag Poll Selection on Right Click
			$("#rightClickDiv").on('click', 'li.pollFlagClass', function () {
				try{
					$("#rightClickDiv").hide();
					
					rightClickShipRequestType = 'Poll';
					rightClickShipData = AllShipStatusInfo.get(currentShip);	
					
					window.open("../requests/flagrequest", '_blank');	
				 }
				 catch(err)
				 {
					console.info('error in Right Click Poll Request: ' + err)
				 }  
			});
			
			// SAR SURPIC Request Selection on Right Click
			$("#rightClickDiv").on('click', 'li.sarsurpicClass', function () {	
				try{
					$("#rightClickDiv").hide();		
					if(sarSurpicAllowed==='true')					
						window.open("../requests/surpicrequest", '_blank');	
					else if(coastalSurpicAllowed==='true')
						window.open("../requests/coastalsurpicrequest", '_blank');
				 }
				 catch(err)
				 {
					console.info('error in Right Click SAR SURPIC Request: ' + err)
				 }  
			});
			
			// Close SAR Selection on Right Click
			$("#rightClickDiv").on('click', 'li.closeSarClass', function () {		
				try{
					$("#rightClickDiv").hide();
					hideSurpicPolygon();
					surpicRequestFlag = false;					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Close SAR SURPIC Request: ' + err)
				 }  
			});
			
			// Port Selection on Right Click
			$("#rightClickDiv").on('click', 'li.portFClass', function () {		
				try{
					$("#rightClickDiv").hide();					
					rightClickShipData = AllShipStatusInfo.get(currentShip);	
					rightClickShipRequestType = 'Port';
					//rightClickShipData = getShipInfoFromloadedShipData(currentShip);				
					window.open("../requests/portrequest", '_blank');
				 }
				 catch(err)
				 {
					console.info('error in Right Click Port Request: ' + err)
				 }  
			});
			
			// Coastal Selection on Right Click
			$("#rightClickDiv").on('click', 'li.costalFClass', function () {
				try{
					$("#rightClickDiv").hide();					
					rightClickShipData = AllShipStatusInfo.get(currentShip);	
					rightClickShipRequestType = 'Coastal';
					//rightClickShipData = getShipInfoFromloadedShipData(currentShip);				
					window.open("../requests/coastalrequest", '_blank');

				 }
				 catch(err)
				 {
					console.info('error in Right Click Coastal Request: ' + err)
				 }  
			});
		
			// Stop Selection on Right Click
			$("#rightClickDiv").on('click', 'li.stopClass', function () {		
				try{
					$("#rightClickDiv").hide();
					
					rightClickShipRequestType = 'Stop';
					rightClickShipData = AllShipStatusInfo.get(currentShip);
					
					window.open("../requests/coastalrequest", '_blank');	
				 }
				 catch(err)
				 {
					console.info('error in Right Click Stop Request: ' + err)
				 }  
			});
			
			// Poll Selection on Right Click
			$("#rightClickDiv").on('click', 'li.pollClass', function () {
				try{
					$("#rightClickDiv").hide();					
					rightClickShipRequestType = 'Poll';
					rightClickShipData = AllShipStatusInfo.get(currentShip);					
					window.open("../requests/coastalrequest", '_blank');	
				 }
				 catch(err)
				 {
					console.info('error in Right Click Poll Request: ' + err)
				 }  
			});
			
			// Archive Selection on Right Click
			$("#rightClickDiv").on('click', 'li.archiveFClass', function () {		
				try{
					$("#rightClickDiv").hide();				
					rightClickShipData = AllShipStatusInfo.get(currentShip);	
					rightClickShipRequestType = 'Archive';
					//rightClickShipData = getShipInfoFromloadedShipData(currentShip);				
					window.open("../requests/coastalrequest", '_blank');

				}
				 catch(err)
				 {
					console.info('error in Right Click Archive Request: ' + err)
				 }  
			});		
			
			// SAR Poll Selection on Right Click
			$("#rightClickDiv").on('click', 'li.sarPollFClass', function () {		
				try{
					$("#rightClickDiv").hide();					
					rightClickShipData = AllShipStatusInfo.get(currentShip);	
					rightClickShipRequestType = 'sarPoll';
					//rightClickShipData = getShipInfoFromloadedShipData(currentShip);				
					window.open("../requests/sarrequest", '_blank');
				 }
				 catch(err)
				 {
					console.info('error in Right Click SAR Poll Request: ' + err)
				 } 
			});
	//-------------------------------------------------------------------------------
	// End of handle Right click on map and ship
	//-------------------------------------------------------------------------------	
			// Hide on Right Click Custom Polygon
			$("#rightClickDiv").on('click', 'li.customPolygonHideClass', function () {
				try{
					$("#rightClickDiv").hide();
					hideCustomPolygon();					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Hide Custom Polygon: ' + err)
				 } 
			});
			
			// Hide All on Right Click
			$("#rightClickDiv").on('click', 'li.customPolygonHideAllClass', function () {		
				try{
					$("#rightClickDiv").hide();
					hideAllCustomPolygons();				
				 }
				 catch(err)
				 {
					console.info('error in Right Click Hide All Custom Polygon: ' + err)
				 } 
			});
			
			// Discard Polygon on Right Click
			$("#rightClickDiv").on('click', 'li.customPolygonDiscardClass', function () {	
				try{
					$("#rightClickDiv").hide();
					discardPolygon();					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Discard Polygon: ' + err)
				 } 
			});
			
			// Modify Polygon on Right Click
			$("#rightClickDiv").on('click', 'li.customPolygonModifyClass', function () {	
				try{
					$("#rightClickDiv").hide();
					modifypolygon();					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Modify Polygon: ' + err)
				 }
			});
			
			// Save Polygon on Right Click
			$("#rightClickDiv").on('click', 'li.customPolygonSaveClass', function () {		
				try{
					$("#rightClickDiv").hide();
					savePolygon();					
				 }
				 catch(err)
				 {
					console.info('error in Right Click Save Polygon: ' + err)
				 }
			});
		
								 
			window.onresize = function()
			{
				adjustMapHeight();
			}
			 var displaySurpicDivText = "";
			 var surpicButtonText = "Hide Active Surpic";
			 if(sarSurpicAllowed==='true' || coastalSurpicAllowed==='true')
				{
				 displaySurpicDivText = "<div class=\"pull-right\" id=\"hideViewSurpicLayerDiv\" style=\"padding:0px; padding-top:5px;padding-right:150px;\"> <button type=\"button\" title=\"hide/view Active Surpic\" " +
				  " id=\"hideViewSurpicLayerButton\" class=\"btn-xs btn-default pull-right\" style=\" background-color: #c3c5c6; padding:0px; padding-left:3px;padding-right:3px; border-color: black; border-block-width: 1px; \"> "+surpicButtonText+ "</button></div>";
				}
			  function refreshShipData() {
				  getShipPositionOnPageLoad(countryListIdsShips);
				  var currentSurpicArea;
				  if(sarSurpicAllowed==='true' || coastalSurpicAllowed==='true')
					{
					  currentSurpicArea =  displaySar24Hours();					 
					}
				  var d = new Date();
				  var t = d.toLocaleTimeString();
				 
				  document.getElementById("refreshDataTime").innerHTML = "Data refreshed at "+ t + "<a Style=\"text-decoration: underline #3c8dbc; padding-right:10px;\" href=# id=\"refreshLink\"> (force refresh)</a> " +
				  displaySurpicDivText;
				  	//	"<a class = \"btn btn-default\" title=\"Snapshot\" id=\"snapShot\" href=\"#\" download=\"map.jpeg\" Style=\"background-color: #e6eceb4d;; padding:0px; padding-left:3px;padding-right:3px;\"> <i class=\"fa  fa-camera\"></i> </a>";
				  //"<a class = \"btn btn-default\" title=\"Snapshot\" id=\"snapShot\"  Style=\"background-color: #e6eceb4d;; padding:0px; padding-left:3px;padding-right:3px;\"><i class=\"fa  fa-camera\"></i></a> <a id=\"image-download\" download=\"map.png\"></a>";
				  
				  if(sarSurpicAllowed==='true' || coastalSurpicAllowed==='true')	  
				  {
					  $("#hideViewSurpicLayerButton").click(function(){
						  //console.info('hideViewSurpicLayerButton clicked.')
					    	 try{				 
						    	 hideViewSurpic24Hours();								
					    	 }
							 catch(err)
							 {
								console.info('error in hideViewSurpicLayerButton click: ' + err)
							 }  					 	
				     });    
					 
					  					 
				  }
				
				 
				  var exportOptions = {
						  filter: function(element) {
						    return element.className ? element.className.indexOf('ol-control') === -1 : true;
						  }
						};
				  
			/*	  var exportPNGElement = document.getElementById('snapShot');

				     if ('download' in exportPNGElement) {
				       exportPNGElement.addEventListener('click', function(e) {
				         map.once('postcompose', function(event) {
				           var canvas = event.context.canvas;
				           //exportPNGElement.crossorigin="anonymous";
				           exportPNGElement.href = canvas.toDataURL('image/jpeg');
				           
				         });
				    	   
				    	   map.once('rendercomplete', function() {
				    		    htmltoimage.toPng(map.getTargetElement(), exportOptions)
				    		      .then(function(dataURL) {
				    		        //var link = document.getElementById('image-download');
				    				        var link = document.getElementById('snapShot');

				    		        link.href = dataURL;
				    		       // link.click();
				    		      });
				    		  });
				    	   
				         map.renderSync();
				       }, false);
				     }  */ 
				  
				 /* document.getElementById('snapShot').addEventListener('click', function() {
					  map.once('rendercomplete', function() {
					    htmltoimage.toPng(map.getTargetElement())
					      .then(function(dataURL) {
					        //var link = document.getElementById('image-download');
							        var link = document.getElementById('image-download');

					        link.href = dataURL;
					        link.click();
					      });
					  });
					  map.renderSync();
					});*/
				  
				  
			}						
			
			refreshShipData();
			hideViewSurpic24Hours();
			currentFilterShipStatus = shipData;
			var interval = 1000 * 60 * 15; // where X is your every X minutes

			var currentRefresh = setInterval(refreshShipData, interval);
			
			 var refreshClick  = document.getElementById("refreshLink");			
			 $("#refreshDataTime").on('click','#refreshLink', function() {	
			// $("#refreshClick").click( 			    	
			 		clearInterval(currentRefresh);			 		
			 		refreshShipData();
			 		currentRefresh = setInterval(refreshShipData, interval);
				});
			
			$(document).on('expanded.pushMenu',function (event) {  			
				setTimeout(function(){ adjustMapHeight(); }, 300);
				adjustMapHeight();
			 });
			
			$(document).on('collapsed.pushMenu',function (event) {  						
				setTimeout(function(){ adjustMapHeight(); }, 300);
				
			 });
			
			map.on('moveend', function(e) {
				  var newZoom = map.getView().getZoom();
				  
					
				  if (currZoom != newZoom) 
				  {
					  zoomChangedFlag = 1
					  layerPresent = "";
					  map.getLayers().forEach(function (layer)
					  {
					      if (layer.get('name') != undefined && (layer.get('name')).match("layerOcean")) {
					    	  	layerPresent = "layerOcean";
				    	  		progressID = "progressIDOcean";
				    	  		addprogress(sourceLayerOcean, progressID);				    	  		
					      }
					  });
					  if(layerPresent == "")
						  {
						  map.getLayers().forEach(function (layer)
								  {
								      if (layer.get('name') != undefined && (layer.get('name')).match("layerBethyLines")) {
								    	  	layerPresent = "layerBethyLines";
							    	  		progressID = "progressIDBethyLines";
							    	  		addprogress(sourceBethyLines, progressID);							    	  		
								      }
								  });
						  }
					  else if (layerPresent == "")
					  {
						  map.getLayers().forEach(function (layer)
								  {
								      if (layer.get('name') != undefined && (layer.get('name')).match("layerSeas")) {
								    	  	layerPresent = "layerSeas";
							    	  		progressID = "progressIDLayerSeas";
							    	  		addprogress(sourceLayerSeas, progressID);							    	  		
								      }
								  });
					  }
					  else if(layerPresent == "")
						  {
						  map.getLayers().forEach(function (layer)
								  {
								      if (layer.get('name') != undefined && (layer.get('name')).match("layerBays")) {
								    	  	layerPresent = "layerBays";
							    	  		progressID = "progressIDLayerBays";
							    	  		addprogress(sourceLayerBays, progressID);							    	  		
								      }
								  });
						  
						  }
					  else if(layerPresent == "")
						  {
						  map.getLayers().forEach(function (layer)
								  {
								      if (layer.get('name') != undefined && (layer.get('name')).match("layerLakes")) {
								    	  	layerPresent = "layerLakes";
							    	  		progressID = "progressIDLayerLakes";
							    	  		addprogress(sourceLayerLakes, progressID);							    	  		
								      }
								  });
						  
						  }
					  
				  }
				});
				currZoom = map.getView().getZoom();
						
				$(function(){
										 
					 var duration = 3000;
					 function flash(feature) {
					   var start = new Date().getTime();					   
					   var listenerKey = map.on('postcompose', animate);
					   function animate(event) {
					     var vectorContext = event.vectorContext;
					     var frameState = event.frameState;
					     var flashGeom = feature.getGeometry().clone();
					     var elapsed = frameState.time - start;
					     var elapsedRatio = elapsed / duration;
					     // radius will be 5 at start and 30 at end.
					     var radius = ol.easing.easeOut(elapsedRatio) * 25 + 5;
					     var opacity = ol.easing.easeOut(1 - elapsedRatio);
					   					    					     
					     var flashStyle = new ol.style.Style({
					         image: new ol.style.Circle({
					           radius: radius,
					           stroke: new ol.style.Stroke({
					             color: 'rgba(255, 0, 0, ' + opacity + ')',
					             width: 0.25 + opacity
					           })
					         })
					       });
					    vectorContext.setStyle(flashStyle);
					    vectorContext.drawGeometry(flashGeom);
					    					    
					     //vectorContext.drawPointGeometry(flashGeom, null);
					     if (elapsed > duration) {
					       ol.Observable.unByKey(listenerKey);
					       return;
					     }
					     // tell OL3 to continue postcompose animation
					     frameState.animate = true;
					   }
					 //  listenerKey = map.on('postcompose', animate);
					 }

					  vectorSource_Temp.on('addfeature', function(e) {
					   flash(e.feature);
					 });									
				})				
				
				function loadDataMap() {
					 getDDPVersion();
					 flagShipByReportingStatusFunction();
					 frequencyCountFunction();
					// inactiveShipCount();
					 displayRightSideBarData()
					 adjustMapHeight();
					 
				}
			
			window.onload = loadDataMap;

						
}); // end of document ready
		
		