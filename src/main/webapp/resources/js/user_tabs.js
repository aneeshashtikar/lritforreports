/*
 * When user provide company code .check company code is existing .make company name ineditable
 * 
 */
$("#companycode")
		.change(
				function() {

					var companycode = document.getElementById("companycode").value;

					url = '/lrit/users/checkCompanyCode';
					var data = {
						companyCode : companycode
					}

					$
							.ajax({
								url : url,
								type : 'POST',
								data : data,
								dataType : 'text',
								dataSrc : "",
								success : function(data) {
									// alert("after success " + data);
									if (data == 'null') {
										// alert("Company code is not present");
										$("#companyname").prop("readonly",
												false);
										document.getElementById("companyname").value = "";

									} else {
										// alert("Company code is already
										// present");
										if (confirm("Users for this company are already registered in the system . Do you still want to register another user? ")) {
											data = data.trim();

											$('#companyname').val(data);

											$("#companyname").prop("readonly",
													true);
										} else {
											document
													.getElementById("companyname").value = "";
											document
													.getElementById("companycode").value = "";
										}
									}

								},
								error : function(error) {
									alert("Error Ajax not working: " + error);
								}

							});

				});

/**
 * Created a script for the category field to open a new tab while registering
 * user
 */

$(function() {

	$(document).on('click', '.btn-prev', function() {
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	});

	$(document).on('click', '.btn-next', function() {
		var active = $('.nav-tabs > .active').attr('id');

		$('.nav-tabs > .active').next('li').find('a').trigger('click');

	});
});

// Main validation page
function validatePortalUser(form) {
	console.log("Inside validatePortalUser " + form.id);
	var uservalidationflag = userDetailsValidation(form.id);

	console.info("validation flag " + uservalidationflag);
	// alert("validation flag "+uservalidationflag);
	if (uservalidationflag == true) {
		return true;
	} else {

		document.getElementById("formerror").innerHTML = "Validation failed kindly check all tabs ";
		return false;
	}
}

// validation as per formid
function userDetailsValidation(formId) {
	console.log("userDetailsValidation");
	// alert("formId "+formId);
	var nameFlag = NameValidation();
	// alert("nameFlag "+nameFlag);
	console.log(nameFlag);
	// alert("roleFlag "+roleFlag);
	var addressFlag = addressValidation();
	console.log("addressFlag" + addressFlag);
	// alert("addressFlag "+addressFlag);
	var validation = Validation();
	console.log("validation" + validation);
	// alert("validation "+validation);
	var mobilenoFlag = MobileNoValidation();
	console.log("mobilenoFlag" + mobilenoFlag);
	// alert("mobilenoFlag "+mobilenoFlag);
	var faxFlag = FaxValidation();
	// alert("faxFlag "+faxFlag);
	console.log("faxFlag" + faxFlag);
	var emailFlag = EmailValidation();
	// alert("emailFlag "+emailFlag);
	console.log("emailFlag" + emailFlag);
	var websiteFlag = WebsiteValidation();
	// alert("websiteFlag "+websiteFlag);
	console.log("websiteFlag" + websiteFlag);
	var pincodeFlag = pincodeValidation();
	// alert("pincodeFlag "+pincodeFlag);
	console.log("pincodeFlag" + pincodeFlag);
	var telephoneFlag = telephoneValidation();
	// alert("telephoneFlag "+telephoneFlag);
	console.log("telephoneFlag" + telephoneFlag);

	if (formId == 'adduserform') {
		var loginIdFlag = LoginIdValidation();
		var roleFlag = RoleValidation();
		console.log("roleFlag" + roleFlag);
		// alert("loginIdFlag "+loginIdFlag);
		console.log("loginIdFlag" + loginIdFlag);
		var fileFlag = FileValidation();
		// alert("fileFlag "+fileFlag);
		if (loginIdFlag == true) {
			existLoginId = checkLoginIdExist();
			// alert("existLoginId "+existLoginId);
			if (existLoginId == 'true') {
				document.getElementById("loginIderror").innerHTML = "LoginId already exist.";
			} else {
				document.getElementById("loginIderror").innerHTML = "";
			}

		}
		console.log("fileFlag" + fileFlag);
		var category = document.getElementById("category").value;
		// alert("category yoo" + category);
		if (category == 'USER_CATEGORY_SC') {

			// alert("Inside category == 'USER_CATEGORY_SC' ");
			// check validation of companycode and comapnyname
			var companycodeCompanynameFlag = CompanyCodeCompanyNameValidation();

			// Check validation of CsoDpa
			var csoDpaFlag = CsoDpaValidation();
			// alert(" csoDpaFlag " + csoDpaFlag);
			if (nameFlag == true && loginIdFlag == true && roleFlag == true
					&& fileFlag == true && addressFlag == true
					&& validation == true && mobilenoFlag == true
					&& faxFlag == true && emailFlag == true
					&& websiteFlag == true && pincodeFlag == true
					&& telephoneFlag == true && existLoginId == 'false'
					&& csoDpaFlag == true) {

				console.log("value is true");
				return true;
			} else {

				document.getElementById("formerror").innerHTML = "Validation failed kindly check all tabs ";
				return false;
			}
		} else if (nameFlag == true && loginIdFlag == true && roleFlag == true
				&& fileFlag == true && addressFlag == true
				&& validation == true && mobilenoFlag == true
				&& faxFlag == true && emailFlag == true && websiteFlag == true
				&& pincodeFlag == true && telephoneFlag == true
				&& existLoginId == 'false') {
			console.log("value is true");
			return true;
		}
		return false;
	} else if (formId == 'edituserform') {
	//	alert("edituserform");
		// alert("Inside formId "+formId);
		console.log("formId" + formId);
		var roleFlag = RoleValidation();
		var fileFlag = FileTypeAndSizeValidation();
		// alert("fileFlag "+fileFlag);
		console.log("roleFlag" + roleFlag);
		if (nameFlag == true && roleFlag == true && addressFlag == true
				&& validation == true && mobilenoFlag == true
				&& faxFlag == true && emailFlag == true && websiteFlag == true
				&& pincodeFlag == true && telephoneFlag == true
				&& fileFlag == true) {
			console.log("value is true");

			// alert("validation is true");

			// alert("loginIdFlag "+loginIdFlag);
			return true;
		}
		return false;
	} else if (formId == 'editshipingcompanyuserform') {
		 //alert("Inside editshipingcompanyuserform this");
		console.log("FormId " + formId);
		/* var loginIdFlag = LoginIdValidation(); */
		var roleFlag = RoleValidation();
	//	alert("role flag "+roleFlag);
		
		var fileFlag = FileTypeAndSizeValidation();
		//alert("fileFlag "+fileFlag);
		var csoDpaFlag = CsoDpaValidation();
		// alert(" csoDpaFlag " + csoDpaFlag);
		if (nameFlag == true && roleFlag == true && addressFlag == true
				&& validation == true && mobilenoFlag == true
				&& faxFlag == true && emailFlag == true && websiteFlag == true
				&& pincodeFlag == true && telephoneFlag == true
				&& fileFlag == true && csoDpaFlag == true) {

			console.log("value is true");
			return true;
		} else {

			document.getElementById("formerror").innerHTML = "Validation failed kindly check all tabs ";
			return false;
		}
		/*
		 * }else { if (nameFlag == true && loginIdFlag == true && addressFlag ==
		 * true && validation == true && mobilenoFlag == true && faxFlag == true &&
		 * emailFlag == true && websiteFlag == true && pincodeFlag == true &&
		 * telephoneFlag == true ) {
		 * 
		 * console.log("value is true"); return true; } else {
		 * 
		 * document.getElementById("formerror").innerHTML = "Validation failed
		 * kindly check all tabs "; return false; } }
		 */

	}

	else if (formId == 'userprofileform') {
		// alert("userprofileform");
		/*
		 * var loginIdFlag = LoginIdValidation();
		 * 
		 * console.log("loginIdFlag" + loginIdFlag);
		 */
		console.log("FormId " + formId);
		// alert("userprofileform");
		var category = document.getElementById("category").value;
		// alert("category yoo" + category);

		var fileFlag = FileTypeAndSizeValidation();
		// alert("fileFlag "+fileFlag);

		if (category == 'Shipping Company') {

			var csoDpaFlag = CsoDpaValidation();
			// alert(" csoDpaFlag " + csoDpaFlag);
			// && loginIdFlag == true
			if (nameFlag == true && addressFlag == true && fileFlag == true
					&& validation == true && mobilenoFlag == true
					&& faxFlag == true && emailFlag == true
					&& websiteFlag == true && pincodeFlag == true
					&& telephoneFlag == true && csoDpaFlag == true) {

				console.log("value is true");
				return true;
			} else {

				document.getElementById("formerror").innerHTML = "Validation failed kindly check all tabs ";
				return false;
			}
		} else {
			if (nameFlag == true && addressFlag == true && fileFlag == true
					&& validation == true && mobilenoFlag == true
					&& faxFlag == true && emailFlag == true
					&& websiteFlag == true && pincodeFlag == true
					&& telephoneFlag == true) {

				console.log("value is true");
				return true;
			} else {

				document.getElementById("formerror").innerHTML = "Validation failed kindly check all tabs ";
				return false;
			}
		}

	}

}

function validationOfUserDetails() {
	var nameFlag = NameValidation();
	var loginIdFlag = LoginIdValidation();
	var roleFlag = RoleValidation();
	var fileFlag = FileValidation();
	if (nameFlag == true && loginIdFlag == true && roleFlag == true
			&& fileFlag == true) {
		console.log("value is true");
		// check loginid already exist in database
		var loginId = document.getElementById("loginid").value;
		var flag = checkLoginIdExist();

	}
	return false;
}

function CompanyCodeCompanyNameValidation() {
	var companycode = document.getElementById("companycode").value;
	var companyname = document.getElementById("companyname").value;
	var companynametestbox = document.getElementById("companyname");
	var numeric = /^[0-9]{5,7}$/;
	var error = null;
	var companynameerror = null;
	var letters = /^[A-Za-z\s]{1,50}$/;
	if (isEmpty(companycode)) {
		error = "Companycode can not be blank.";
	} else if (!companycode.match(numeric)) {
		error = "companycode must be numeric between 5 and 7.";
	}

	if (isEmpty(companyname)) {
		companynameerror = "companyname can not be blank.";
	} else if (!companyname.match(letters)) {
		companynameerror = "companyname must be contain aphabets between 1 to 50 .";
	}

	if (error != null) {
		document.getElementById("companycodeerror").innerHTML = error;
		// return false;

	} else {
		document.getElementById("companycodeerror").innerHTML = "";
		// return true;
	}
	if (companynameerror != null) {
		document.getElementById("companynameerror").innerHTML = companynameerror;
		// return false;

	} else {
		document.getElementById("companynameerror").innerHTML = "";
		// return true;
	}
	if (error != null && companynameerror != null) {
		return true;
	}
	return false;

}

/*
 * var txt = document.getElementById("text_id").readOnly;
 * document.getElementById("GFG").innerHTML = txt;
 */
/*
 * function validateDoc1() {
 * 
 * //var _emailDoc = document.getElementById('emailDoc');
 * 
 * if ((_emailDoc.value == "") || (_emailDoc.files[0].size > 512000) ||
 * (_emailDoc.value.substring(_emailDoc.value.lastIndexOf('.') + 1) != 'pdf')) {
 * $('#emailDocError').text("Mandatory PDF Document size should not exceed
 * 500kb"); return false; } else { $('#emailDocError').text(""); return true; }
 *  }
 */

function checkLoginIdExist() {

	var loginId = document.getElementById("loginid").value;
	var data = {
		loginId : loginId
	}
	var errorFlag = $.ajax({
		url : '/lrit/users/checkLoginId',
		async : false,
		type : 'POST',
		data : data,
		success : function(data) {

			if (data == 'true') {
				// alert("inside true of ajax call");
				return true;

			} else {
				// alert("inside false of ajax call");
				return false;
			}
		},
		error : function(error) {
			alert("Error AJAX not working: " + error);
			return true;
		}
	});

	// alert("errorFlag"+errorFlag.val+errorFlag+errorFlag.responseText);
	return errorFlag.responseText;

}

function isEmpty(str) {
	return !str.replace(/\s+/, '').length;
}

function NameValidation() {
	var name = document.getElementById("name").value;
	var error = null;
	console.log("error " + error);
	// validation for alphanumeric values
	/* var letters = /^[A-Za-z]{1,50}$/; */

	// validation allows: space , . ' - but allows . and special character in
	// start
	// var letters = /^[a-zA-Z\s,.'-]{1,50}$/;

	/* ~^(?!\.)[\w.] */

	// validation allows: space , . ' - but allows . and special character not
	// allowed at start
	var letters = /^[^\s,.'-][a-zA-Z\s,.'-]{1,50}$/;
	//alert("pattern " + letters);
	if (isEmpty(name)) {
		error = "Name can not be blank.";
	} else if (!name.match(letters)) {
		error = "Name should contain alphabets between 1 to 50.";
	}

	if (error != null) {
		document.getElementById("nameerror").innerHTML = error;
		return false;

	} else {
		document.getElementById("nameerror").innerHTML = "";
		return true;
	}

}
function LoginIdValidation() {
	var loginId = document.getElementById("loginid").value;
	// alert("loginID "+loginId);
	var alphanumberic = /^[0-9A-Za-z]{5,50}$/;
	var error = null;
	if (isEmpty(loginId)) {
		error = "loginId can not be blank.";
	} else if (!loginId.match(alphanumberic)) {
		error = "LoginId should contain only alphanumeric between 5 to 50";
	}
	if (error != null) {
		document.getElementById("loginIderror").innerHTML = error;
		return false;
	} else {
		document.getElementById("loginIderror").innerHTML = "";
		return true;
	}
}
function RoleValidation() {
	var roles = document.getElementById("roles").value;
	if (isEmpty(roles)) {

		document.getElementById("roleerror").innerHTML = "roles can not be blank.";
		return false;
	} else {
		document.getElementById("roleerror").innerHTML = "";
		return true;
	}
}

function FileValidation() {
	var file = document.getElementById("scannedforms").value;
	var error = null;

	if (isEmpty(file)) {
		error = "file needs to be uploaded.";

	} else {

		var Extension = file.substring(file.lastIndexOf('.') + 1).toLowerCase();
		console.log("Extension " + Extension);
		if (Extension == "pdf") {

		} else {
			error = "Upload file must have extension .pdf.";
		}
		var filedata = document.getElementById("scannedforms");
		var fileSize = filedata.files[0].size / (1024 * 1024);
		// alert("file.size" +fileSize);
		if (fileSize > 2) {
			alert("fileSize > 2");
			error = "Mandatory PDF Document size should not exceed 2MB";

		}

	}
	if (error != null) {
		document.getElementById("formuploaderror").innerHTML = error;
		return false;
	} else {
		document.getElementById("formuploaderror").innerHTML = "";
		return true;
	}

}
function FileTypeAndSizeValidation() {
	var file = document.getElementById("scannedforms").value;
	var error = null;

	if (isEmpty(file)) {

	} else {

		var Extension = file.substring(file.lastIndexOf('.') + 1).toLowerCase();
		console.log("Extension " + Extension);
		if (Extension == "pdf") {

		} else {
			error = "Upload file must have extension .pdf.";
		}
		var filedata = document.getElementById("scannedforms");
		var fileSize = filedata.files[0].size / (1024 * 1024);
		// alert("file.size" +fileSize);
		if (fileSize > 2) {
			// alert("fileSize > 2");
			error = "Mandatory PDF Document size should not exceed 2MB";

		}

	}
	if (error != null) {
		document.getElementById("formuploaderror").innerHTML = error;
		return false;
	} else {
		document.getElementById("formuploaderror").innerHTML = "";
		return true;
	}

}
function addressValidation() {
	var address = /^[a-zA-Z0-9\s,.'-]{3,100}$/;
	var plotvalidationerror = null;
	var areavalidationerror = null;
	var streetvalidationerror = null;
	var landmarkvalidationerror = null;
	var address1 = document.getElementById("address1").value;
	if (isEmpty(address1)) {

		plotvalidationerror = "Plot no can not be blank.";
	} else if (!address1.match(address)) {

		plotvalidationerror = "Address should be alphanumeric having length between 3 and 100";
	}
	var address2 = document.getElementById("address2").value;
	if (isEmpty(address2)) {

		areavalidationerror = "Area/Locality can not be blank.";
	} else if (!address2.match(address)) {

		areavalidationerror = "Area/Locality should be alphanumeric having length between 3 and 100";
	}
	var address3 = document.getElementById("address3").value;
	if (isEmpty(address3)) {

		streetvalidationerror = "Street field can not be blank.";
	} else if (!address3.match(address)) {

		streetvalidationerror = "Street should be alphanumeric having length between 3 and 100";
	}
	var landmark = document.getElementById("landmark").value;
	if (isEmpty(landmark)) {
		landmarkvalidationerror = "Landmark field can not be blank.";
	} else if (!landmark.match(address)) {

		landmarkvalidationerror = "Landmark should be alphanumeric having length between 3 and 100";
	}

	if (plotvalidationerror != null) {
		document.getElementById("address1error").innerHTML = plotvalidationerror;

	} else {
		document.getElementById("address1error").innerHTML = "";

	}
	if (areavalidationerror != null) {
		document.getElementById("address2error").innerHTML = areavalidationerror;

	} else {
		document.getElementById("address2error").innerHTML = "";

	}
	if (streetvalidationerror != null) {
		document.getElementById("address3error").innerHTML = streetvalidationerror;

	} else {
		document.getElementById("address3error").innerHTML = "";

	}
	if (landmarkvalidationerror != null) {
		document.getElementById("landmarkerror").innerHTML = landmarkvalidationerror;

	} else {
		document.getElementById("landmarkerror").innerHTML = "";

	}
	if (plotvalidationerror == null && areavalidationerror == null
			&& streetvalidationerror == null && landmarkvalidationerror == null) {
		return true;
	}
	return false;
}

function Validation() {
	var city = document.getElementById("city").value;
	var district = document.getElementById("district").value;
	var state = document.getElementById("state").value;
	var country = document.getElementById("country").value;
	var letters = /^[A-Za-z]{1,50}$/;
	var cityerror = null;
	var districterror = null;
	var stateerror = null;
	var countryerror = null;
	if (isEmpty(city)) {
		cityerror = "City Field can not be blank.";
	} else if (!city.match(letters)) {
		cityerror = "City field should contain only alphabets between 1 to 50";
	}
	if (isEmpty(district)) {
		districterror = "District Field can not be blank.";
	} else if (!district.match(letters)) {
		districterror = "District field should contain only alphabets between 1 to 50";
	}
	if (isEmpty(state)) {
		stateerror = "State Field can not be blank.";
	} else if (!state.match(letters)) {
		stateerror = "State field should contain only alphabets between 1 to 50";
	}
	if (isEmpty(country)) {
		countryerror = "Country Field can not be blank.";
	} else if (!country.match(letters)) {
		countryerror = "Country field should contain only alphabets between 1 to 50";
	}
	if (cityerror != null) {
		document.getElementById("cityerror").innerHTML = cityerror;
	} else {
		document.getElementById("cityerror").innerHTML = "";

	}
	if (districterror != null) {
		document.getElementById("districterror").innerHTML = districterror;
	} else {
		document.getElementById("districterror").innerHTML = "";

	}
	if (stateerror != null) {
		document.getElementById("stateerror").innerHTML = stateerror;
	} else {
		document.getElementById("stateerror").innerHTML = "";

	}
	if (countryerror != null) {
		document.getElementById("countryerror").innerHTML = countryerror;
	} else {
		document.getElementById("countryerror").innerHTML = "";

	}

	if (cityerror != null && districterror != null && stateerror != null
			&& countryerror != null) {
		return false;
	} else {
		return true;
	}
}
function MobileNoValidation() {
	var mobileno = document.getElementById("mobileno").value;
	/* var pattern = /^(\+\d{1,3}[- ]?)?\d{10}$/; */
	var pattern = /^[0]?[789]\d{9}$/;

	var error = null;
	/*
	 * if (isEmpty(mobileno)) { error = "mobileno can not be blank."; } else
	 */

	if (!isEmpty(mobileno)) {
		if (!pattern.test(mobileno)) {
			error = "mobileno should contain 10 digits.";

		}
	}
	if (error != null) {
		document.getElementById("mobilenoerror").innerHTML = error;
		return false;

	} else {
		document.getElementById("mobilenoerror").innerHTML = "";
		return true;
	}
}

function FaxValidation() {
	// alert("Inside fax validation ");
	console.log("Inside fax validation ********");
	var fax = document.getElementById("fax").value;
	// var pattern = /^\\+[0-9]{1,3}-[0-9]{3}-[0-9]{7}$/;
	var regex = new RegExp("^\\+[0-9]{1,3}-[0-9]{3}-[0-9]{7}$");
	var error = null;
	/*
	 * if( isEmpty(fax) ) { // alert("Inside fax validation is empty ");
	 * console.log("Inside fax validation is empty ********");
	 * 
	 * error="fax can not be blank."; }else
	 */
	if (!isEmpty(fax)) {
		if (!regex.test(fax)) {
			// alert("Inside fax validation pattern miss match ");
			console.log("Inside fax validation pattern miss match  ********");
			error = "Fax should contain +countrycode-areacode-faxnumber of 7 digit";

		}
	}
	if (error != null) {
		// alert("error raised "+error);
		document.getElementById("faxerror").innerHTML = error;
		return false;

	} else {
		document.getElementById("faxerror").innerHTML = "";
		return true;
	}
}

function EmailValidation() {
	var email = document.getElementById("emailid").value;
	var alternateemaiid = document.getElementById("alternateemailid").value;
//alert("alternateemaiid "+alternateemaiid);
	var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	var error = null;
	var alternateemailiderror = null;

	if (isEmpty(email)) {
		error = "emailid can not be blank.";
	} else if (!pattern.test(email)) {
		error = "Invaild emailid ";
	}
	if (isEmpty(alternateemaiid)) {
		alternateemailiderror = "alternateEmailId can not be blank.";
	} else if (!pattern.test(alternateemaiid)) {
		//alert("pattern doesnot match in alternate email id")
		alternateemailiderror = "Invaild alternateEmailId ";
	}

	if (error != null ) {
		document.getElementById("emailiderror").innerHTML = error;
	} else {
		document.getElementById("emailiderror").innerHTML = "";
	}
	if (alternateemailiderror != null ) {
	//	alert("alternateemailiderror "+alternateemailiderror );
		document.getElementById("alternateemailiderror").innerHTML = alternateemailiderror;
	} else {
		document.getElementById("alternateemailiderror").innerHTML = "";
	}
	
	if (error == null && alternateemailiderror == null) {
		 
	
		return true;

	} else {
	//	alert("error raised "+error+ " alternateemailiderror "+ alternateemailiderror);
		return false;
	}
}
function WebsiteValidation() {
	var website = document.getElementById("website").value;
	var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
	'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
	'((\\d{1,3}\\.){3}\\d{1,3}))' + // ip (v4) address
	'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port
	'(\\?[;&amp;a-z\\d%_.~+=-]*)?' + // query string
	'(\\#[-a-z\\d_]*)?$', 'i');

	var error = null;

	if (!isEmpty(website)) {
		if (!pattern.test(website)) {

			error = "Website should contain http://ip_or_domainname:port/value:";

		}
	}

	if (error != null) {
		document.getElementById("websiteerror").innerHTML = error;
		return false;

	} else {
		document.getElementById("websiteerror").innerHTML = "";
		return true;
	}
}
function pincodeValidation() {

	var pattern = /^\d{6}$/;
	var pincode = document.getElementById("pincode").value;
	var error = null;
	if (isEmpty(pincode)) {
		error = "pincode can not be blank.";
	} else if (!pattern.test(pincode)) {
		error = "pincode should contain 6 digits ";

	}
	if (error != null) {
		document.getElementById("pincodeerror").innerHTML = error;
		return false;

	} else {
		document.getElementById("pincodeerror").innerHTML = "";
		return true;
	}
}
function telephoneValidation() {
	console.log("telephoneValidation");

	const pattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/;
	var telephone = document.getElementById("telephone").value;
	var error = null;

	if (isEmpty(telephone)) {
		error = "telephone no can not be blank.";
	} else if (!isEmpty(telephone)) {
		if (!pattern.test(telephone)) {
			error = "telephone number with STD code and should have 11 digit number";

		}
	}
	if (error != null) {
		document.getElementById("telephoneerror").innerHTML = error;
		return false;

	} else {
		document.getElementById("telephoneerror").innerHTML = "";
		return true;
	}

}

function CsoDpaValidation() {
	var category = document.getElementById("category").value;
	var error = false;
	// alert("category");

	if (category == 'Shipping Company' || category == 'USER_CATEGORY_SC') {
		// alert("category is USER_CATEGORY_SC");
		var csolength = document.getElementById("csolength").value;
	//	alert("csolength " + csolength);

		if (csolength < 2) {
			// alert("csolength < 2 "+csolength);
			document.getElementById("csotableerror").innerHTML = "No of rows in Cso  table cannot be less than 2.";
			error = true;
		} else {
			document.getElementById("csotableerror").innerHTML = "";
		}
		var dpalength = document.getElementById("dpalength").value;
		// alert("dpalength "+dpalength);
		if (dpalength < 1) {
			// alert("dpalength < 1 "+dpalength);
			document.getElementById("dpaerror").innerHTML = "No of rows in Dpa  table cannot be less than 1.";
			error = true;
		} else {
			document.getElementById("dpaerror").innerHTML = "";
		}
		if (error == true) {
		//	alert("false");
			return false;
		} else {
			return CsoNameValidation();

		}
	}
}

function CsoNameValidation() {
//	alert("start of CsoNameValidation");
	var error = false;
	var count = 0;
	var alphabets = /^[a-zA-Z\s]{3,50}$/;
	/*
	 * var rows = document.getElementById("csoTable").tBodies[0].rows;
	 * alert("rows "+rows);
	 */
	var flag = false;
	$('#csoTable tr')
			.each(
					function() {
						//alert("count " + count);
						// alert("#csoTable tr");
						if (count > 0) {
						//	alert("count>0 " + count);
							var keval = $(this).find("input").val();
						//	alert("value " + keval);
							var csotelephoneoffice = $(this).find(
									"[id='csotelephoneoffice']").val();
						//	alert("csotelephoneoffice" + csotelephoneoffice);

							if (keval == null || keval == '') {
								document.getElementById("csotableerror").innerHTML = "Name in cso table cannot be null.";
								error = true;
								// break;
							} else if (!alphabets.test(keval)) {
								document.getElementById("csotableerror").innerHTML = "Name in cso table should contain alphabets between 3 to 50";
								error = true;
							}
							console.log(keval);
						}
						count = count + 1;
					});
	$('#dpaTable tr')
			.each(
					function() {
						//alert("#dpaTable tr");
						var keval = $(this).find("input").val();
						//alert("value " + keval);
						if (keval == null || keval == '') {
							document.getElementById("dpaerror").innerHTML = "Name in dpa table cannot be null.";
							error = true;
							// break;
						} else if (!alphabets.test(keval)) {
							document.getElementById("dpaerror").innerHTML = "Name in dpa table should contain alphabets between 3 to 50";
							error = true;
						}
						console.log(keval);
					});
	if (error == true) {
		return false;
	} else {
		return true;
	}
}