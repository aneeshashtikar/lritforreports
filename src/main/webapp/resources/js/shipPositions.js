  var flagShipRespondNormalIndex=0;
  var flagShipMissPositionIndex=0;
  var flagShipNotRespondIndex=0;
  var flagShipErrorIndex=0;

  var foreignShipRespondNormalIndex=0;
  var foreignShipMissPositionIndex=0;
  var foreignShipNotRespondIndex=0;
  var mapCountryWiseShipData = new Map();
  var mapFlagShipDataWorldWide = new Map();

function getShipPositionOnPageLoad(countryListIdsShips)     	
	{
		console.info('getShipPositionOnPageLoad called');
		//console.info(countryListIdsShips);	
		
		var urlShipPosition = "/lrit/map/SearchCompanyShipByImo";
		var dataToPass = { 'companyCode': JSON.stringify(companyUser)};
		if (companyUser=="none")
		{
			urlShipPosition ="/lrit/map/ShipPosition";
			dataToPass = { 'cg_lritid': countryListIdsShips };
		}
		//console.info(flagCountry);
		try{
		$.ajax({
		    type: "POST",
		    traditional: true,
		    url: urlShipPosition, //"/lrit/map/ShipPosition",
		    data: dataToPass, //{ 'cg_lritid': flagCountry },
		    success:function(response) {	
		    	try{
		    	  jsondata = response;
		    	  var shipRespondNormalStyle = new ol.style.Style({
				    	 image: new ol.style.Circle({		             
					    		 radius: 3,
					    		 stroke: new ol.style.Stroke({
					    	     color: 'green',
					    	     width: 1
				    		 }),
				    		 fill: new ol.style.Fill({
				    			 color: 'green'
			    	          })
				    	 }),
				    	 text: new ol.style.Text({
				             offsetY: 20,	
				             font: 'bold 12px Calibri,sans-serif',
				             fill: new ol.style.Fill({
				               color: '#000'
				             }),
				             stroke: new ol.style.Stroke({
				               //color: '#fff',
				            	color: 'yellow',
				               width: 3
				             })
				           })
				     });		


		    	  var shipMissPositionStyle = new ol.style.Style({
				    	 image: new ol.style.Circle({		             
				    		 radius: 3,
					    		 stroke: new ol.style.Stroke({
					    	     color: 'Orange',
					    	     width: 1
				    		 }),
				    		 fill: new ol.style.Fill({
				    			 color: 'Orange'
			    	          })
				    	 }),
				    	 text: new ol.style.Text({
				             offsetY: 20,	
				             font: 'bold 12px Calibri,sans-serif',
				             fill: new ol.style.Fill({
				               color: '#000'
				             }),
				             stroke: new ol.style.Stroke({
				               //color: '#fff',
				            	color: 'yellow',
				               width: 3
				             })
				           })
				     });
		    	  
		    	  var shipErrorPositionStyle = new ol.style.Style({
				    	 image: new ol.style.Circle({		             
				    		 radius: 3,
					    		 stroke: new ol.style.Stroke({
					    	     color: 'yellow',
					    	     width: 1
				    		 }),
				    		 fill: new ol.style.Fill({
				    			 color: 'yellow'
			    	          })
				    	 }),
				    	 text: new ol.style.Text({
				             offsetY: 20,	
				             font: 'bold 12px Calibri,sans-serif',
				             fill: new ol.style.Fill({
				               color: '#000'
				             }),
				             stroke: new ol.style.Stroke({
				               //color: '#fff',
				            	color: 'yellow',
				               width: 3
				             })
				           })
				     });
		     
	     
			     var shipNotRespondStyle = new ol.style.Style({
			    	 image: new ol.style.Circle({		             
				    		 radius: 3,
				    		 stroke: new ol.style.Stroke({
				    	     color: 'red',
				    	     width: 1
			    		 }),
			    		 fill: new ol.style.Fill({
			    			 color: 'red'
		    	          })
			    	 }),
			    	 text: new ol.style.Text({
			             offsetY: 20,	
			             font: 'bold 12px Calibri,sans-serif',
			             fill: new ol.style.Fill({
			               color: '#000'
			             }),
			             stroke: new ol.style.Stroke({
			               //color: '#fff',
			            	color: 'yellow',
			               width: 3
			             })
			           })
			     });
			     
			     
			     var forignshipStyle = new ol.style.Style({
			    	 image: new ol.style.Circle({		             
				    		 radius: 3,
				    		 stroke: new ol.style.Stroke({
				    	     color: 'darkviolet',
				    	     width: 1
			    		 }),
			    		 fill: new ol.style.Fill({
			    			 color: 'darkviolet'
		    	          })
			    	 }),
			    	 text: new ol.style.Text({
			             offsetY: 20,	
			             font: 'bold 12px Calibri,sans-serif',
			             fill: new ol.style.Fill({
			               color: '#000'
			             }),
			             stroke: new ol.style.Stroke({
			               //color: '#fff',
			            	color: 'yellow',
			               width: 3
			             })
			           })
			     });
			     

	     
			    shipData = 	jsondata;
			    var flagShipData = [];
			    var foreignShipData = [];
			    
			    flagShipRespondNormalIndex=0;
			    flagShipMissPositionIndex=0;
			    flagShipNotRespondIndex=0;

			    foreignShipRespondNormalIndex=0;
			    foreignShipMissPositionIndex=0;
			    foreignShipNotRespondIndex=0;

			    vectorLayerFlagShipMissPosition.getSource().clear();
				vectorLayerFlagShipNoRespons.getSource().clear();
				vectorLayerFlagShipRespondNormal.getSource().clear();
				vectorLayerForeignShipRespondNormal.getSource().clear();
				vectorLayerForeignShipMissPosition.getSource().clear();
				vectorLayerForeignShipNoRespons.getSource().clear();

			    var flagIndex = 0;
			    var foreignIndex = 0;
			    var flagCountryToCompare;
			    var featureToCompare;
			    if(countryListIdsShips.length>1)
		    	{
			    	featureToCompare = 'dc_id';
			    	flagCountryToCompare = '3065';
		    	}
			    else if(countryListIdsShips.length==1)
		    	{
			    	featureToCompare = 'dataUserProvider';
			    	flagCountryToCompare = countryListIdsShips[0].trim();;
		    	}
			    	
			     for(var i =0; i<jsondata.length; i++)
			     {		    	 
				      var features = new ol.Feature({
				    	  name: "POINT" + i,
				    	  geometry: new ol.geom.Point([jsondata[i].longitude, jsondata[i].latitude] ),	
				    	  message_id: jsondata[i].message_id,
				    	  ship_borne_equipment_id: jsondata[i].ship_borne_equipment_id,
				    	  asp_id: jsondata[i].asp_id,
				    	  imo_no: jsondata[i].imo_no,
				    	  mmsi_no: jsondata[i].mmsi_no,
				    	  dc_id: jsondata[i].dc_id,
				    	  timestamp4: jsondata[i].timestamp4,
				    	  dataUserRequestor: jsondata[i].data_user_requestor,
				    	  ship_name: jsondata[i].ship_name,
				    	  dataUserProvider: jsondata[i].data_user_provider,
				    	  ddpversion_no: jsondata[i].ddpversion_no,
				    	  course:jsondata[i].course,
				    	  speed:jsondata[i].speed,
				    	  vesselstatus: jsondata[i].vesselstatus,
				    	  registration_status: jsondata[i].registration_status
				      });
						      
				      shipData[i].pathProjectionStatus = false;
				      shipData[i].historyStatus = false;
				      shipData[i].popupStatus = false;
				      shipData[i].popupResizeStatus = false;
				      shipData[i].popupResizeHeight = 0;
				      shipData[i].popupResizeWidth = 0;
				      
				      AllShipStatusInfo.set(jsondata[i].imo_no.trim(), shipData[i]);
				      if(filterdShipData===undefined)
				    	  filterdShipData = shipData;
				      
				      var timeInterval = 6;
				      if(features.get(featureToCompare) != flagCountryToCompare)
			    	  { 
				    	  	if( hrDiff(new Date(jsondata[i].timestamp4)) <=timeInterval)
				    	  // if(jsondata[i].vesselstatus==='Responding Normally')
			    	  		{
			    	  			foreignShipRespondNormal[foreignShipRespondNormalIndex] =  jsondata[i];
			    	  			//features.setStyle(shipRespondNormalStyle);
			    	  			/*features.setStyle(function(features,resolution) {
			    	  				shipRespondNormalStyle.getText().getStroke().setColor('yellow');
			    	  				shipRespondNormalStyle.getText().setText(getText(features,resolution));
									return shipRespondNormalStyle;
						        });*/
			    	  			
			    	  			features.setStyle(function(features,resolution) {
			    	  				forignshipStyle.getText().setText(getText(features,resolution));
									return forignshipStyle;
						        });
			    	  			foreignShipData[foreignIndex] = jsondata[i];
			    	  			foreignIndex = foreignIndex + 1;
			    	  			foreignShipRespondNormalIndex = foreignShipRespondNormalIndex+1;
			    	  			vectorLayerForeignShipRespondNormal.getSource().addFeature(features.clone());
			    	  			shipData[i].status = 'Responding';
			    	  		}
				    	  	else if(hrDiff(new Date(jsondata[i].timestamp4))>=timeInterval && hrDiff(new Date(jsondata[i].timestamp4))<24)
				    	  // else if(jsondata[i].vesselstatus==='Missing Position')
				    	  	{
				    	  		foreignShipMissPosition[foreignShipMissPositionIndex] =  jsondata[i];
			    	  			//features.setStyle(shipMissPositionStyle);
				    	  		features.setStyle(function(features,resolution) {
			    	  				forignshipStyle.getText().setText(getText(features,resolution));
									return forignshipStyle;
						        });

			    	  			foreignShipData[foreignIndex] = jsondata[i];
			    	  			foreignIndex = foreignIndex + 1;
			    	  			foreignShipMissPositionIndex = foreignShipMissPositionIndex+1;
			    	  			vectorLayerForeignShipMissPosition.getSource().addFeature(features.clone());
			    	  			shipData[i].status = 'Missing';
			    	  		}
				    	   else //if(jsondata[i].vesselstatus==='Not Responding')
				    	  	{
				    	  		foreignShipMissPosition[foreignShipMissPositionIndex] =  jsondata[i];
			    	  			//features.setStyle(shipMissPositionStyle);
				    	  		features.setStyle(function(features,resolution) {
			    	  				forignshipStyle.getText().setText(getText(features,resolution));
									return forignshipStyle;
						        });

			    	  			foreignShipData[foreignIndex] = jsondata[i];
			    	  			foreignIndex = foreignIndex + 1;
			    	  			foreignShipMissPositionIndex = foreignShipMissPositionIndex+1;
			    	  			vectorLayerForeignShipMissPosition.getSource().addFeature(features.clone());
			    	  			shipData[i].status = 'NotResponding';
			    	  		}
				    	  	/*else
				    	  	{
			    	  			foreignShipNotRespond[foreignShipNotRespondIndex] =  jsondata[i];
			    	  			//features.setStyle(shipNotRespondStyle);
			    	  			features.setStyle(function(features,resolution) {
			    	  				forignshipStyle.getText().setText(getText(features,resolution));
									return forignshipStyle;
						        });

			    	  			foreignShipData[foreignIndex] = jsondata[i];
			    	  			foreignIndex = foreignIndex + 1;
			    	  			foreignShipNotRespondIndex = foreignShipNotRespondIndex+1;
			    	  			vectorLayerForeignShipNoRespons.getSource().addFeature(features.clone());
			    	  			shipData[i].status = 'Transmitting Errorneously';
			    	  		}*/
				    	  	
				    	  }
					      else //if(features.get(featureToCompare) == flagCountryToCompare)
				    	  {
					    	  	/*features.setStyle(flagShipPositionStyle);
					    	  	foreignShipData[foreignIndex] = jsondata[i];
					    	  	foreignIndex = foreignIndex + 1;*/
					    	//if( hrDiff(new Date(jsondata[i].timestamp4)) <=timeInterval)
					    	  if(jsondata[i].vesselstatus==='Responding_Normally')
				  			{
				    	  			flagShipRespondNormal[flagShipRespondNormalIndex] =  jsondata[i];
				    	  			//features.setStyle(shipRespondNormalStyle);
				    	  			features.setStyle(function(features,resolution) {
				    	  				shipRespondNormalStyle.getText().getStroke().setColor(' #d5f5e3');
				    	  				shipRespondNormalStyle.getText().setText(getText(features,resolution));
										return shipRespondNormalStyle;
							        });
				    	  			flagShipData[flagIndex] = jsondata[i];
				    	  			flagIndex = flagIndex + 1;
				    	  			flagShipRespondNormalIndex = flagShipRespondNormalIndex+1;
				    	  			vectorLayerFlagShipRespondNormal.getSource().addFeature(features.clone());
				    	  			shipData[i].status = 'Responding';
			    	  		}
			    	  		//else if(hrDiff(new Date(jsondata[i].timestamp4))>=timeInterval && hrDiff(new Date(jsondata[i].timestamp4))<24)
					    	  else if(jsondata[i].vesselstatus==='Missing_Position')
				    	  	{
					  			flagShipMissPosition[flagShipMissPositionIndex] =  jsondata[i];
					  			//features.setStyle(shipMissPositionStyle);

					  			features.setStyle(function(features,resolution) {
					  				shipMissPositionStyle.getText().getStroke().setColor(' #d5f5e3');
					  				shipMissPositionStyle.getText().setText(getText(features,resolution));
									return shipMissPositionStyle;
						        });
					  			flagShipData[flagIndex] = jsondata[i];
					  			flagIndex = flagIndex + 1;
					  			flagShipMissPositionIndex = flagShipMissPositionIndex+1;
					  			vectorLayerFlagShipMissPosition.getSource().addFeature(features.clone());
					  			shipData[i].status = 'Missing';
				    	  	}
					    	  else if(jsondata[i].vesselstatus==='Not_Responding')
				    	  	{
					  			flagShipNotRespond[flagShipNotRespondIndex] =  jsondata[i];
					  			//features.setStyle(shipNotRespondStyle);
					  			features.setStyle(function(features,resolution) {
					  				shipNotRespondStyle.getText().getStroke().setColor(' #d5f5e3');
									shipNotRespondStyle.getText().setText(getText(features,resolution));
									return shipNotRespondStyle;
						        });
					  			flagShipData[flagIndex] = jsondata[i];
					  			flagIndex = flagIndex + 1;
					  			flagShipNotRespondIndex = flagShipNotRespondIndex+1;
					  			vectorLayerFlagShipNoRespons.getSource().addFeature(features.clone());
					  			shipData[i].status = 'Not Responding';
				    	  	}
					    	 else
					    	  {
						  			flagShipNotRespond[flagShipNotRespondIndex] =  jsondata[i];
						  			//features.setStyle(shipNotRespondStyle);
						  			features.setStyle(function(features,resolution) {
						  				shipErrorPositionStyle.getText().getStroke().setColor(' #d5f5e3');
						  				shipErrorPositionStyle.getText().setText(getText(features,resolution));
										return shipErrorPositionStyle;
							        });
						  			flagShipData[flagIndex] = jsondata[i];
						  			flagIndex = flagIndex + 1;
						  			flagShipErrorIndex = flagShipErrorIndex+1;
						  			vectorLayerFlagShipNoRespons.getSource().addFeature(features.clone());
						  			shipData[i].status = 'Transmitting Errorneously';
					    	  	}			    
				    	  }		    
	      
			     		}			     
			     		// sessionStorage.setItem("lritIdDisplayRightSideBar", lritIdDisplay);
			     		// sessionStorage.setItem("lritNameDisplayRightSideBar", lritNameDisplay);
			     		 
			     		 sessionStorage.setItem("shipDataRightSideBar", shipData);
			     		 getShipStatus();
						 getAllCountries1000NMShipData();	
						 getShipCountWorldWide(); 					 
			    }
				catch(err)
				{
					console.info('error in getting Ship Position: ' + err)
				}
		    }
	      });	
		}
		catch(err)
		{
			console.info('error in getting Ship Position: ' + err)
		}
	}
	

	function getShipStatus()
	{	
		try{
			
			sessionStorage.setItem("flagShipNotRespondIndexRightSideBar", flagShipNotRespondIndex);
			sessionStorage.setItem("flagShipRespondNormalIndexRightSideBar", flagShipRespondNormalIndex);
			sessionStorage.setItem("flagShipMissPositionIndexRightSideBar", flagShipMissPositionIndex);
			
/*
			$("#flagShipNotRespondIndex").html(flagShipNotRespondIndex);
			$('#flagShipRespondNormalIndex').html(flagShipRespondNormalIndex);
			$('#flagShipMissPositionIndex').html(flagShipMissPositionIndex);*/

		}
		catch(err)
		{
			console.info('error in getShipStatus: ' + err)
		}
	}

	
	
	function getShipCountWorldWide()
	{
		try{
			if (lritIdDisplay!=undefined){
				for(var i =0; i<lritIdDisplay.length; i++)
			     {	
					 var shipInfoDetails = [];
			   		 for (var j=0; j<shipData.length; j++)
					 {
			   			 if(shipData[j].data_user_provider !=null)
					 	{
			   				 if ( shipData[j].data_user_provider.trim() == lritIdDisplay[i].trim())
					 		{
						 		shipInfoDetails.push(shipData[j]);
					 		}		
					 	}
					 }
			   		mapFlagShipDataWorldWide.set(lritNameDisplay[i].trim(),shipInfoDetails);
			     }		
				
				
				
				// Making JS Map compatible for JSON.Stringify
			     const outmapFlagShipDataWorldWide = Object.create(null)
			     mapFlagShipDataWorldWide.forEach(function(value, key){
			    	 
			      if (value instanceof Map) {
			        outmapFlagShipDataWorldWide[key] = JSON.stringify(map_to_object(value))
			
			      }
			      else {
			        outmapFlagShipDataWorldWide[key] = JSON.stringify(value)
			      }
			    })
			  
				var tempmapFlagShipDataWorldWide = JSON.stringify(outmapFlagShipDataWorldWide);
				sessionStorage.setItem("mapFlagShipDataWorldWideRightSideBar", tempmapFlagShipDataWorldWide);
				var retrievedData = sessionStorage.getItem("mapFlagShipDataWorldWideRightSideBar");
				var movies2 = JSON.parse(retrievedData);
				/*Object.entries(movies2).forEach(function([key,value]){
					console.info(key +' : ' + value);
					if(key==="India")
					{var tempValue = JSON.parse(value);
					console.info(tempValue );
					console.info(tempValue.length);
					}
				});*/
				
				
				// $('#flagWorldWide').innerHTML = "";	
				 
				 $('#flagWorldWide').html("");						
				 countLoop = 0;
				 
				 $('#flagWorldWide').addClass("table table-condensed");
				 $('#flagWorldWide').append('<tbody><tr><th style="width: 10px">S. No.</th> <th>Country</th><th>No. of ships</th></tr>');
				 
				 var countLoop = 0;
				 mapFlagShipDataWorldWide.forEach(function(value, key){
			    	 countLoop = countLoop + 1;					    	 
			    	/*$('#flagWorldWide').append('<tr> <td> '+ countLoop +'</td>'+							
								'<td>'+ key +' </td>' +
								'<td> <span class="badge">'+ value.length +'</span> </td>' +								
								'</tr>');*/
			    	 //var tempValue = JSON.parse(value);
					 	$('#flagWorldWide').append('<tr> <td> '+ countLoop +'. </td>'+							
					 				'<td>'+ key +' </td>' +
					 				'<td>' + '<a href="#" id="' +key+'-vesselww"'+
					 				' data-target="#detailmodel" data-toggle="modal">' +
					 				 '<span class="badge">' + value.length  + '</a> ' +' </span></td>' +								
					 				'</tr>'); 
					 	var vesselClick  = document.getElementById(key+"-vesselww");
							//console.log("vessel clisck value"+vesselClick);
							if(vesselClick !=null)
							{
			 				vesselClick.onclick = function() {    
							    showData(value);
							}
							}
			      
			    });
			}
		}
		catch(err)
		{
			console.info('error in getShipCountWorldWide: ' + err)
		}
	}
	
	
	function getAllCountries1000NMShipData()
    {
		try{    	
    	var allCountryBoundaryLritId;  	
    	    	
    	$.ajax({
		    type: "POST",
		    traditional: true,
		    url: "/lrit/map/CountryBoundary",
		    data: {},
		    success:function(response) {		
	    	try{	
		    	
		    	for(var i =0; i<response.length; i++)
			     {
		    		
		    		var shipCoordinate=[[]];
		     		
		   		 for(var j =0; j<shipData.length; j++)
		   	     {
		   			 	shipCoordinate[j] = [shipData[j].longitude, shipData[j].latitude];
		   	     }			 
		   		 	
		   		 var points = turf.points(shipCoordinate);
		   		 var findvalueX = 0;
		   	     var findvalueY = 0;
		        	function findShipData(shipDataTofind) {
		        		var coord = [shipDataTofind.longitude ,shipDataTofind.latitude ];		     		
		        		return coord[0] ==findvalueX && coord[1]==findvalueY;
		       	}
		        	
		    		
		    		var searchWithin = turf.polygon(response[i].poslist.coordinates);		    		
		    		var ptsWithin = turf.pointsWithinPolygon(points, searchWithin); 
		    		var turfFeature = ptsWithin.features;					    
		    		
			     	 var shipInfoDetails = [];
				     for(var k =0; k<turfFeature.length; k++)
				     {		   
				    	 var geom = turf.getGeom(turfFeature[k]);
				    	 var shipInBoundaryCoordinates = turf.getCoords(geom)
				    	 findvalueX = shipInBoundaryCoordinates[0];
				    	 findvalueY = shipInBoundaryCoordinates[1];
				    	 
				    	 var foundIndex = shipData.findIndex(findShipData);		
				    	 shipInfoDetails.push(shipData[foundIndex]);				      	    	    	  		      
			    	 }	   
				  
				     for(var j =0; j<lritIdDisplay.length; j++)
				     {
					     if ( response[i].cg_lritid.trim() == lritIdDisplay[j].trim())
				 		 {
					    	 mapCountryWiseShipData.set(lritNameDisplay[j].trim(),shipInfoDetails);  
				 		 }
				     }
				     //console.info(response[i].cg_lritid.trim());
				    // console.info(shipInfoDetails.length);
			     }
		    }
		    catch(err)
			{
				console.info('error in get All Countries 1000NM Ship Data: ' + err)
			}
		    
		   
		    
		     const outmapCountryWiseShipData = Object.create(null)
		     mapCountryWiseShipData.forEach(function(value, key){
		    	 
		      if (value instanceof Map) {
		    	  outmapCountryWiseShipData[key] = JSON.stringify(map_to_object(value))
		
		      }
		      else {
		    	  outmapCountryWiseShipData[key] = JSON.stringify(value)
		      }
		    })
	  
		var tempmapCountryWiseShipData = JSON.stringify(outmapCountryWiseShipData);
		sessionStorage.setItem("mapCountryWiseShipDataRightSideBar", tempmapCountryWiseShipData);
		    
		 //   sessionStorage.setItem("mapCountryWiseShipDataRightSideBar", mapCountryWiseShipData);
		   // $('#within1000NM').innerHTML = "";
		$('#within1000NM').html("");	
		 $('#within1000NM').addClass("table table-condensed");
		 $('#within1000NM').append('<tbody><tr><th style="width: 10px">S. No.</th> <th>Country</th><th>No. of ships</th></tr>');
			 var countLoop = 0;
		     mapCountryWiseShipData.forEach(function(value, key){			
		    	 countLoop = countLoop + 1;					    	 
		    	/*$('#within1000NM').append('<tr> <td> '+ countLoop +'</td>'+							
							'<td>'+ key +' </td>' +
							'<td> <span class="badge"> '+ value.length +' </span></td>' +								
							'</tr>');*/
		    	//  var tempValue = JSON.parse(value);
		    	 $('#within1000NM').append('<tr> <td> '+ countLoop +'. </td>'+							
	 		 				'<td>'+ key +' </td>' +
	 		 				'<td>' + '<a href="#" id="' +key+'-vesselnm"'+
	 		 				' data-target="#detailmodel" data-toggle="modal">' +
	 		 				 '<span class="badge">' + value.length  + '</a> ' +' </span></td>' +								
	 		 				'</tr>'); 

	 				var vesselClick  = document.getElementById(key+"-vesselnm");
	 				//console.log("vessel clisck value"+vesselClick);
	 				if(vesselClick !=null)
	 				{
		 				vesselClick.onclick = function() {    
		 				var aa=	document.getElementById("SidebarDetailData");
		 				showData(value);
	 				}
	 				}
		      
		    });
		    
		    
		      } 
		});
		}
		catch(err)
		{
			console.info('error in getAllCountries1000NMShipData: ' + err)
		}
    
    }
    
	function getShipDataByCustomPolygonName(id,startDate,endDate)
	{
		alert('4 inside getShipDataByCustomPolygonName');
		alert("5 id :"+id+" startdate: "+startDate+" enddate : "+endDate);
		var coordsFormatted = "LINESTRING(";
		$.ajax({
		 	type: "GET",
		    traditional: true,
		    url: "/lrit/map/getselecteduserarea?id="+id,		   
		    success: function(result){ 
		    console.log("6 "+result);
		    userareas = result;	 
		    for(var i =0 ;i<userareas.length;i++){
	    		 var coords =[];
	    	 	 var polygonshape = userareas[i];
				//var coordsFormatted = "LINESTRING(";
	    	 	if(polygonshape.polygontype ==="polygon"){ 
					var coords = [];
		    		 for(var j =0 ;j<polygonshape.lon.length;j++){
		    			 coords.push([polygonshape.lon[j],polygonshape.lat[j]])
		    			 if(j===polygonshape.lon.length-1)
		    				 coordsFormatted = coordsFormatted + " " + polygonshape.lon[j] + " " + polygonshape.lat[j] + ")";
		    			 else
		    				 coordsFormatted = coordsFormatted + " " + polygonshape.lon[j] + " " + polygonshape.lat[j] + ",";
		    		 }		 
		    		 
		    	 	console.info('polygon coords: ' + coords);
		    	 	console.info('coordsFormatted: ' + coordsFormatted);
		    	 	alert("7 coords : "+coords);
		    	 	alert('8 coordsFormatted: ' + coordsFormatted);
		   	 	}
		   	 	else if(polygonshape.polygontype ==="rectangle")
		   	 	{
		   	 		var coords = [[polygonshape.lon,polygonshape.lat],[polygonshape.lon,polygonshape.latOffset],[polygonshape.longOffset,polygonshape.latOffset],[polygonshape.longOffset,polygonshape.lat],[polygonshape.lon,polygonshape.lat]]
		   	 		
		   	 	    coordsFormatted = "ST_GeomFromText('LINESTRING(" +  polygonshape.lon + " " + polygonshape.lat + "," +  polygonshape.lon + " " + polygonshape.latOffset + "," +
		   	 	                       polygonshape.longOffset + " " + polygonshape.latOffset + "," + polygonshape.longOffset + " " + polygonshape.lat + ")')";
		   	 	   
		   	 		console.info('rectangle coords: ' + coords);
		   	 		console.info('coordsFormatted: ' + coordsFormatted);
		   	 	alert("7 coords : "+coords);
	    	 	alert('8 coordsFormatted: ' + coordsFormatted);
		   	 	}
		   	 	else if(polygonshape.polygontype ==="circle"){ 
		   	 	 		 
				  var options = {steps: 360, units: 'miles', options: {}}; //degrees
				  polygonshape.radius = polygonshape.radius * 1.1507;
				  
				  var circle = turf.circle([polygonshape.lon,polygonshape.lat], polygonshape.radius, options);
				  var coordsTemp = (circle.geometry.coordinates).split(",");
				  for(var j =0 ;j<coordsTemp.length;j=j+2){
					  if(j===coordsTemp.length-2)
		    				 coordsFormatted = coordsFormatted + " " + coordsTemp[j] + " " + coordsTemp[j+1] + ")'";
		    			 else
		    				 coordsFormatted = coordsFormatted + " " + coordsTemp[j] + " " + coordsTemp[j+1] + ",";
				  }
				   console.info('circle coords: ' + circle.geometry.coordinates);
				   console.info('coordsFormatted: ' + coordsFormatted);
				   alert("7 coords : "+coords);
				   alert('8 coordsFormatted: ' + coordsFormatted);
		   	 	}
	 
	    	 }		 
		    
		    }
		});
		return coordsFormatted;
	}
	