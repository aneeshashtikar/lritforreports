/*
 * billing.js
 */

/**
 * @author Yogendra Yadav
 */

/**
 * THIS WILL RETURN JSON DATA FROM PASSED
 * 
 * @URL AND BASED ON GIVEN
 * @DATA
 * 
 * 
 * @author Yogendra Yadav
 * @param url
 * @param data
 * @returns
 */
function getJSONData(url, data) {

	var jsonData = {};
	$.ajax({
		url : url,
		async : false,
		data : data
		
	}).done(function(result) {
		jsonData = result;
	});
	return jsonData;
}

function clearMessages() {
	$('#messageTable tbody').empty();
}

function addAgency(url, data){
	var result = getJSONData(url, data);

	console.log("addAgency");
	console.log(result);

	$('#agency').find('option').remove();

	$('#agency').append(
			$("<option></option>").attr("value", "")
					.text("-- Select Agency --"));
	
	$.each(result, function(key, value) {
		$('#agency').append(
				$("<option></option>").attr("value", value.lritid).text(
						value.name));
	});
}

/**
 * THIS WILL FILL SELECT OPTION OF AGENCY WITH AGENCY NAME AND LRIT ID COMES
 * WITHIN SELECTED DATE
 * 
 * 
 * @author Yogendra Yadav
 * @param url
 * @param data
 * @returns
 */
function addAspDcAgency(url, data) {

	var result = getJSONData(url, data);

	console.log("addAspDcAgency");
	console.log(result);

	$('#agency').find('option').remove();

	$('#agency').append(
			$("<option></option>").attr("value", "")
					.text("-- Select Agency --"));
	$.each(result, function(key, value) {
		$('#agency').append(
				$("<option></option>").attr("value", value.lritid).text(
						value.name));
	});
}

/**
 * TO ADD SHIPPING COMPANY DETAILS COMPANY CODE AND SHIPPING COMPANY NAME COMES
 * WITHIN SELECTED DATE
 * 
 * 
 * @author Yogendra Yadav
 * @param url
 * @param data
 * @returns
 */
function addShippingAgency(url, data) {
	var result = getJSONData(url, data);

	console.log("addShippingAgency");
	console.log(result);

	$('#agency').find('option').remove();

	$('#agency').append(
			$("<option></option>").attr("value", "").text(
					"-- Select Shipping Company --"));
	$.each(result, function(key, value) {
		$('#agency').append(
				$("<option></option>").attr("value", value.companyCode).text(
						value.companyName));
	});
}

/**
 * ADD CONTRACT BASED ON AGENCY OR SHIPPING COMPANY SELECTION
 * 
 * 
 * @author Yogendra Yadav
 * @param url
 * @param data
 * @returns
 */
function addContracts(url, data) {

	var result = getJSONData(url, data);

	console.log("addContracts");
	console.log(result);

	$('#billingContract').find('option').remove();

	$('#billingContract').append(
			$("<option></option>").attr("value", "").text(
					"-- Select Contract --"));
	$.each(result, function(key, value) {
		$('#billingContract').append(
				$("<option></option>").attr("value", value.contractId).text(
						value.contractNumber));
	});
}

/**
 * BASED ON CONTRACT SELECTED DETAILS OF CONTRACTING GOVERNMENT OR VESSEL CAN BE
 * VIEWED
 * 
 * 
 * @author Yogendra Yadav
 * @param url
 * @param data
 * @returns
 */
function addBillableMessages(url, data) {

	var result = getJSONData(url, data);

	console.log("addMessages");
	console.log(result);

	var subTotal = 0;

	$('#messageTable tbody').empty();

	$.each(result, function(key, value) {
		subTotal = subTotal + parseFloat(value.cost);
		$("#messageTable tbody").append(
				'<tr><td>' + value.provider + '</td><td>' + value.consumer
						+ '</td><td>'
						+ value.billingPaymentBillableItemCategory.itemName
						+ '</td><td>'
						+ value.billingPaymentBillableItemCategory.rate
						+ '</td><td>' + value.systemCount + '</td><td>'
						+ value.cost + '</td></tr>');
	});
	$('#subTotal').val(subTotal.toFixed(2));
	console.log(subTotal);
	grandTotalFn();
}

/**
 * 
 * 
 * 
 * @author Yogendra Yadav
 * @param url
 * @param data
 * @returns
 */
function addBillableMergeMessages(url, data) {

	var result = getJSONData(url, data);

	console.log("addMessages");
	//console.log(result);

	var subTotal = 0;

	$('#messageTable tbody').empty();

	Object.keys(result).forEach(function(producer) {
		var producerCount = 0;
		
		// to make rowspan for producer
		Object.keys(result[producer]).forEach(function(consumer) {
			producerCount += result[producer][consumer].length;
		});

		var producerSpan = "<td rowspan='" + producerCount + "'>" + producer + "</td>";

		Object.keys(result[producer]).forEach(function(consumer) {
			var consumerCount = result[producer][consumer].length;
			var consumerSpan = "<td rowspan='" + consumerCount + "'>" + consumer + "</td>";
			
			result[producer][consumer].forEach(function(value) {
				subTotal = subTotal + parseFloat(value.cost);
				$("#messageTable tbody").append(
						'<tr>'+ producerSpan + consumerSpan +'<td>'
								+ value.billingPaymentBillableItemCategory.itemName
								+ '</td><td>'
								+ value.billingPaymentBillableItemCategory.rate
								+ '</td><td>' + value.systemCount + '</td><td>'
								+ value.cost + '</td></tr>');
				consumerSpan = '';
				producerSpan = '';
			});
		});
		//console.log(producerCount);
	});
	$('#subTotal').val(subTotal.toFixed(2));
	console.log(subTotal);
	grandTotalFn();
}

/**
 * 
 * 
 * 
 * @author Yogendra Yadav
 * @param url
 * @param data
 * @returns
 */
function addInvoiceBillableItems(url, data) {

	var result = getJSONData(url, data);

	console.log("addMessages");
	//console.log(result);

	var subTotal = 0;
	var key = 0;

	$('#messageTable tbody').empty();

	Object.keys(result).forEach(function(producer) {
		var producerCount = 0;
		
		// to make rowspan for producer
		Object.keys(result[producer]).forEach(function(consumer) {
			producerCount += result[producer][consumer].length;
		});

		var producerSpan = "<td rowspan='" + producerCount + "'>" + producer + "</td>";

		Object.keys(result[producer]).forEach(function(consumer) {
			var consumerCount = result[producer][consumer].length;
			var consumerSpan = "<td rowspan='" + consumerCount + "'>" + consumer + "</td>";
			
			result[producer][consumer].forEach(function(value) {
				subTotal = subTotal + parseFloat(value.cost);
				var provider = `<input type="hidden" name="billingBillableItems[${key}].provider" value="${value.provider}" />`;
				var consumer = `<input type="hidden" name="billingBillableItems[${key}].consumer" value="${value.consumer}" />`;
				var itemId = `<input type="hidden" name="billingBillableItems[${key}].billingPaymentBillableItemCategory.billableItemCategoryId" value="${value.billingPaymentBillableItemCategory.billableItemCategoryId}" />`;
				var systemCount = `<input type="hidden" name="billingBillableItems[${key}].systemCount" value="${value.systemCount}" />`;
				var manualCount = `<input type="hidden"  name="billingBillableItems[${key}].manualCount" value="${value.manualCount}" />`;
				var cost = `<input type="hidden" name="billingBillableItems[${key}].cost" value="${value.cost}" />`;
				$("#messageTable tbody").append(
						`<tr>${producerSpan}${consumerSpan}
						${provider}${consumer}${itemId}${systemCount}${manualCount}${cost}
						<td>${value.billingPaymentBillableItemCategory.itemName}</td>
						<td>${value.rate}</td>
						<td>${value.systemCount}</td>
						<td>${value.cost}</td>
						</tr>`);
				consumerSpan = '';
				producerSpan = '';
				
				key = parseInt(key)+1;
				console.log(key);
			});
		});
	});
	$('#subTotal').val(subTotal.toFixed(2));
	console.log(subTotal);
	grandTotalFn();
}

/**
 * TO UPDATE GRAND TOTAL OF INVOICE OR BILL
 * 
 * 
 * @author Yogendra Yadav
 * @returns
 */
function grandTotalFn() {
	var grandTotal = parseFloat($('#subTotal').val()) + parseFloat($('#levies').val()) + parseFloat($('#taxes').val());
	console.log($('#subTotal').val()+"()()()()");
	console.log($('#levies').val()+"{}{}{}{}");
	console.log($('#taxes').val()+"[][][][]");
	$('#grandTotal').val(grandTotal.toFixed(2));
}

function grandTotalForCSPFn() {
	var annualManagementCharges = parseFloat($('#annualManagementCharges').val());
	$('#grandTotal').val(0.00);
	console.log(annualManagementCharges + "===============");
	var grandTot = parseFloat($('#subTotal').val())
			+ parseFloat($('#levies').val()) + parseFloat($('#taxes').val()) + parseFloat(annualManagementCharges);
	console.log("grandTotalForCSPFn"+grandTot);
	if(isNaN(grandTot)){
		$('#grandTotal').val(0.00);
	}
	else{
		$('#grandTotal').val(grandTot.toFixed(2));
	}
	
}

/**
 * WHEN UPDATING MESSAGE COUNT THIS METHOD WILL UPDATE OVERALL COST OF THE
 * MESSAGE
 * 
 * 
 * @author Yogendra Yadav
 * @returns
 */
function subTotalVerify() {
	var subtotal = 0;
	$(".bcost").each(function() {
		subtotal = subtotal + parseFloat($(this).val());
	});
	$('#subTotal').val(subtotal.toFixed(2));
	return subtotal;
}

/**
 * CALCULATE THE COST OF BILLABLEITEM PER CATEGORY AS PER COUNT CHANGE
 * 
 * 
 * @author Yogendra Yadav
 * @returns
 */
function calculateMessageCost() {
	var alt = $(this).attr("alt");
	var cost = 0.0;
	cost = parseFloat($("#rate_" + alt).val()) * parseFloat($(this).val());
	$('#cost_' + alt).val(cost.toFixed(2));
	console.log(cost);
	subTotalVerify();
	grandTotalFn();
}

function calculateMessageCostForCSP() {
	var alt = $(this).attr("alt");
	var cost = 0.0;
	cost = (parseFloat($("#rate_" + alt).val()) * parseFloat($(this).val())) * ($('#dollarRate').val());
	//alert(cost);
	$('#cost_' + alt).val(cost.toFixed(2));
	console.log(cost);
	subTotalVerify();
	$('#grandTotal').val(0.00);
	grandTotalForCSPFn();
}

function calculateMessageCostForCSPByDollarRate() {
	
	var alt = $(".count").attr("alt");
	//var alt = 0;
	var count = $('.count').length;
	var cost = 0.0;
	for(var i = 0; i < count ; i++){
		cost = (parseFloat($("#rate_" + i).val()) * parseFloat($("#manualcount_" + i).val())) * ($('#dollarRate').val());
		$('#cost_' + i).val(cost.toFixed(2));
		console.log(cost);
	}
	subTotalVerify();
	$('#grandTotal').val(0.00);
	grandTotalForCSPFn();
}

function setBillableItemsDataTable(url, billingServiceId) {
	$('#contractingGovernment').css("display", "");
	var dtable = $('#contractingGovernment').DataTable({
		// dom: 'lBrtip',
		"bDestroy" : true,
		dom : 'lBfrtip',
		"ajax" : {
			"url" : url,
			"type" : "POST",
			async : false,
			"data" : {
				"billingServiceId" : billingServiceId
			},
			"dataSrc" : ""
		},
		"columns" : [ {
			"data" : "providerName"
		}, {
			"data" : "consumerName"
		}, {
			"data" : "billingPaymentBillableItemCategory.itemName"
		}, {
			"data" : "rate"
		}, {
			"data" : "systemCount"
		}, {
			"data" : "cost"
		} ],
		"rowsGroup" : [// Always the array (!) of the column-selectors in
		// specified order to which rows groupping is applied
		// (column-selector could be any of specified in
		// https://datatables.net/reference/type/column-selector)
		0, 1 ],
		"responsive" : {
			"details" : {
				"type" : 'column',
				"target" : 'tr'
			}
		},
		"columnDefs" : [],
		"scrollY":        "500px",
        "scrollCollapse": true,
        "paging":         false
	});
}
