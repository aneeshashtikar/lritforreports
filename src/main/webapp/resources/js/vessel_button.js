$(document).ready(function(){

	$('#seidhidden').hide();
	
	$(document).on('click', '.btn-prev', function() {
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	});

	$(document).on('click', '.btn-next', function() {
		$('.nav-tabs > .active').next('li').find('a').trigger('click');
	});

	/*$('#shipborneTable').find('tbody tr.newRow').hide();*/

	$(document).on('click', '.btn-add', function() {
		/*$('tbody#shipborne_table_body').append($('tr#new_shipborne_row:first').clone(true));*/
		/*$('#new_shipborne_row').show();*/
		/*var _row = $('#newRow').html();
		console.log(_row); */
		/*$('#shipborne_table_body').html();*/

		/*var _row = $('.newRow').html();
		console.log(_row);
		$('#shipborne_table_body').append('<tr>' + _row + '</tr>');*/

		var table = document.getElementById("shipborneTable");

		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);

		var colCount = table.rows[1].cells.length;

		for(var i=0; i<colCount; i++) {

			var newcell	= row.insertCell(i);

			newcell.innerHTML = table.rows[1].cells[i].innerHTML;
			//alert(newcell.childNodes);
			switch(newcell.childNodes[0].type) {
			case "text":
				newcell.childNodes[0].value = "";
				break;
			case "radiobutton":
				newcell.childNodes[0].selected = false;
				break;
			case "select-one":
				newcell.childNodes[0].selectedIndex = 0;
				break;
			}
		}

	});

	$(document).on('click', '.btn-delete', function() {
		/*$(this).closest('tr').remove();*/
		//var table = document.getElementById("shipborneTable");
		if($('#shipborneTable tr').length == 2)
			alert('Single row cannot be deleted');
		else
			$('#shipborneTable').find('tbody tr:last').remove();
	});

});

var _prev = '<button type="button" class="btn btn-info pull-left btn-prev">Previous</button>';
var _submit = '<input type="submit" class="btn btn-primary pull-right btn-submit" value="Submit" />';
var _next = '<button type="button" class="btn btn-info pull-right btn-next">Next</button>';
var id;

function tabChange(id, _prev, _next, _submit){

	$('#addbtn').html(_prev+_next+_submit);

		if(id.includes("first"))
		{
			$('.btn-prev').hide();
			$('.btn-next').show();
			$('.btn-submit').hide();
		}
		else if (id.includes("last"))
		{
			$('.btn-prev').show();
			$('.btn-next').hide();
			$('.btn-submit').show();
		}
		else
		{
			$('.btn-prev').show();
			$('.btn-next').show();
			$('.btn-submit').hide();
		}
}

function findModelName()
{
	var _modelType = $('#modelType').val();
	var _manufacturerName = $('#manufacturerName').val();
	$.get("getmodellist?modelType="+_modelType+"&manufacturerId="+_manufacturerName, function( modellist ) {
		console.log(modellist);
		var _modelName="";
		$.each(modellist, function(i, obj) {
			_modelName += '<option value= "' + obj.modelId + '">' + obj.modelName + '</option>';
		});
		console.log(_modelName);
		$('#modelName').html('<option value="">Select</option>'+_modelName);
	});
}

function findCategorySet() {
	var _vesselType = $('#vesselType').val();
	$.get("getcategorylist?vesselType="+_vesselType, function( categorySet ) {
		//console.log(categorySet);
		var _categoryName = "";
		$.each(categorySet, function (i, obj) {
			_categoryName += '<option value= "' + obj.categoryName + '">' + obj.categoryName + '</option>';
		});
		//console.log(_categoryName);
		if(_categoryName == "")
		{
			_categoryName += '<option value= "" disabled="true">No Category Exist For Selected Vessel Type</option>';
		}
		$('#vesselCategory').html('<option value="">Select</option>'+_categoryName);
	});
}

function findModelType()
{
	var _manufacturerName = $('#manufacturerName').val();
	$.get("getmodeltype?manufacturerId="+_manufacturerName, function(modeltypelist) {
		console.log(modeltypelist);	
		var _modelType = "";
		$.each(modeltypelist, function (i, value) {
			_modelType += '<option value= "' + value + '">' + value + '</option>';
		});
		console.log(_modelType);
		$('#modelType').html('<option value="">Select</option>'+_modelType);
	});
}

function getName(id)
{
	//alert(id);
	_getId='#' + id;
	_code = $(getId).val();
	$.get("getnamefromcode?code="+_code, function(name) {
		$(getId).val(name);
	});
}

function getAcsoName(id)
{
	//alert(id);
	var buttonId = "#csoId" + id;
	var buttonId2 = "#userId" + id;
	var buttonId3 = '#acsoId'+id;
	var buttonId4 = '#acsoName'+id;
	var _csoId = $(buttonId).val();	
	var _loginId = $(buttonId2).val();
    console.log(_csoId);
    $.get("getacsoname?loginId="+_loginId+"&type=acso&alternateCsoId="+_csoId, function(list) {
    	console.log("inside loop");
    	/*$.each(list, function (i, value) {	
    		$(buttonId3).val(value);
		});*/
    	$(buttonId3).val(list.get(0));
    	$(buttonId4).val(list.get(1));
	});
}