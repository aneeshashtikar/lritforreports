 	 var india = [76.2711, 10.8505]; // kerla longitude and lattitude
     var sriLanka = [80.7718, 7.8731]; 
	 var flagCountry = flagCountryOfLogin;
	 var countryListIdsShips =[];
	 var centerFocus;
	 var zoom;
	 var shipData;
	 var filterdShipData;
	
	 var flagShipRespondNormal=[];
	 var flagShipMissPosition=[];
	 var flagShipNotRespond=[];
	 var surpicInteractionOnFlag = 2;   
	 var foreignShipRespondNormal=[];
	 var foreignShipMissPosition=[];
	 var foreignShipNotRespond=[];
	 var rightClickSurpicRadius;
	 var rightClickSurpicRectangle;
	 var rightClickSurpicCenter = "";
	 //var geoserver_url="http://10.210.9.27:8080/geoserver/wms";  //Local server
	 //var geoserver_url="http://192.168.10.50:8086/geoserver/wms";  //DMZ
	 
	 var AllShipStatusInfo = new Map();
	 var surpicShipInfoForDisplay = new Map();
	 var historyShipInfoForDisplay = new Map();
	 var StandingOrderInfo;
	 /* var shipfeaturesFromImo; */
	 var historyLayer;
	 var extentThreshold = 90;
	 var graticuleOn = false;
	 
	 var draggingPopup;
	 console.log(flagCountry);
	 
	 if(flagCountry =='1065')
	 {
		 centerFocus = india
		 zoom = 4
	 }
	 else
	 {
		 centerFocus = sriLanka
		 zoom = 4
	 }	 
	 
	 var graticuleOnStyle =  new ol.Graticule({
	      // the style to use for the lines, optional.
	      strokeStyle: new ol.style.Stroke({
	        color: 'rgba(255,120,0,0.9)',
	        width: 2,
	        lineDash: [0.5, 4]
	      }),
	      showLabels: true,	     
	      latLabelPosition: 0.05,	      
	      wrapX: false
	    });
	 
	 var graticuleOffStyle =  new ol.Graticule({
	      // the style to use for the lines, optional.
	      strokeStyle: new ol.style.Stroke({
	        color: 'rgba(255,120,0,0.0)',
	        width: 0,
	        lineDash: [0.1, 2]
	      }),
	      showLabels: true,	      
	      latLabelPosition: 0.05,	     
	      wrapX: false
	    })
   
  
	// For creating layers from database
	
	 var vectorSource_Temp = new ol.source.Vector(
	{              
		 wrapX: false,
    	 noWrap: true
    });
	 
	var vectorSelectedShipSource = new ol.source.Vector(
	{              
		 wrapX: false,
		 noWrap: true
	});
	
	var vectorLayer_SelectedShipPosition = new ol.layer.Vector({
    	source: vectorSelectedShipSource,   
    	name: "selectedShipPositionLayer"
	});
	
	
	var pathProjectionSource = new ol.source.Vector(
			{              
				 wrapX: false,
				 noWrap: true
			});
			
			var vectorLayer_pathProjection = new ol.layer.Vector({
		    	source: pathProjectionSource,   
		    	name: "pathProjectionLayer"
			});
	
			
	var vectorSourceFlagShipRespondNormal = new ol.source.Vector(
	{              
		 wrapX: false,
    	 noWrap: true
    });
	
	var vectorLayerFlagShipRespondNormal = new ol.layer.Vector({
			source: vectorSourceFlagShipRespondNormal,
			 name:"ShipLayerOnLoadNormalFlag"
	});


	var vectorSourceFlagShipMissPosition = new ol.source.Vector(
	{              
		 wrapX: false,
		 noWrap: true
    });
	
	var vectorLayerFlagShipMissPosition = new ol.layer.Vector({
			source: vectorSourceFlagShipMissPosition,
			name:"ShipLayerOnLoadMissingFlag"
	});

	var vectorSourceFlagShipNoRespons = new ol.source.Vector(
	{              
		 wrapX: false,
		 noWrap: true
	});
	
	var vectorLayerFlagShipNoRespons = new ol.layer.Vector({
			source: vectorSourceFlagShipNoRespons,
			name:"ShipLayerOnLoadNoResponsFlag"
	});

	//vectorLayerFlagShipNoRespons.setZIndex(1000);
	var vectorSourceForeignShipRespondNormal = new ol.source.Vector(
	{              
		 wrapX: false,
		 noWrap: true
	});
	
	var vectorLayerForeignShipRespondNormal = new ol.layer.Vector({
			source: vectorSourceForeignShipRespondNormal,
			name:"ShipLayerOnLoadNormalForeign"
	});


	var vectorSourceForeignShipMissPosition = new ol.source.Vector(
	{              
		 wrapX: false,
		 noWrap: true
	});
	
	var vectorLayerForeignShipMissPosition = new ol.layer.Vector({
			source: vectorSourceForeignShipMissPosition,
			name:"ShipLayerOnLoadMissingForeign"
	});

	var vectorSourceForeignShipNoRespons = new ol.source.Vector(
	{              
		 wrapX: false,
		 noWrap: true
	});
	var vectorLayerForeignShipNoRespons = new ol.layer.Vector({
			source: vectorSourceForeignShipNoRespons,
			name:"ShipLayerOnLoadNoResponsForeign"
	});

	var zIndex = 1000;
	vectorLayerFlagShipMissPosition.setZIndex(zIndex);
	vectorLayerFlagShipNoRespons.setZIndex(zIndex);
	vectorLayerFlagShipRespondNormal.setZIndex(zIndex);
	vectorLayerForeignShipRespondNormal.setZIndex(zIndex);
	vectorLayerForeignShipMissPosition.setZIndex(zIndex);
	vectorLayerForeignShipNoRespons.setZIndex(zIndex);
	
	
	
// -------------------Generate SO Layer----------------------------


	var vectorSource = new ol.source.Vector(
	{              
		 wrapX: false,
    	 noWrap: true
    });
	
	var vectorLayer = new ol.layer.Vector({
    	source: vectorSource,  
    	name:"StandingOrder"
           
	});
	//vectorLayer.setZIndex(900);
	function generateStandingOrder(flagCountry)
	{
		try{
			$.ajax({
		    type: "POST",
		    traditional: true,
		    url: "/lrit/map/SO",
		    data: {'cg_lritid': flagCountry},
		    success:function(response) {
		    	try{
			    	jsondata = response;
			    	StandingOrderInfo = response;
			    	var SoPolygonStyle = new ol.style.Style({
			    		stroke: new ol.style.Stroke({
			            color: 'blue',
			            width: 1
			            })
				     });
				  
					  for(var i =0; i<jsondata[0].length; i++)
					  {
						  var features = new ol.Feature({
							  name: "POLYGON" + i,
							  geometry: new ol.geom.Polygon(jsondata[0][i].poslist.coordinates),	   
							  ddpversion_no: jsondata[0][i].ddpversion_no,
							  area_id: jsondata[0][i].area_id,
							  cg_lritid: jsondata[0][i].cg_lritid,
							  type: "StandingOrder" // jsondata[i].type
						  });
				
						  features.setStyle(SoPolygonStyle);   
						  vectorLayer.getSource().addFeature(features.clone());  
					 }
		    	}
				catch(err)
				{
					console.info('error in generating Standing Order: ' + err)
				}
		    }
		  });
		}
		catch(err)
		{
			console.info('error in generating Standing Order: ' + err)
		}
	}
	
	
	
	// ----------Generate Ship Position Layer----------------------------
	
    var flagShipPositionStyle = new ol.style.Style({	    
   	image: new ol.style.Circle({		             
   		 radius: 3,
   		 stroke: new ol.style.Stroke({
   	     color: 'blue',
   	     width: 1
   		 }),
   		 fill: new ol.style.Fill({
   	      color: 'blue'
   	       }),	    	     
   	 })
    });		      

	 var foreignShipPositionStyle = new ol.style.Style({
		 image: new ol.style.Circle({		             
			 radius: 3,
			 stroke: new ol.style.Stroke({
		     color: 'yellow',
		     width: 1
			 }),
			 fill: new ol.style.Fill({
		      color: 'yellow'
		          })
		 })
	 });
    

//-----Base Map and Ship Layer to be displayed on load --------------------   
	  
	try{	
		 var layers = [      
		   new ol.layer.Tile({   
			   preload: Infinity,
			   source: new ol.source.TileWMS({
		       url: geoserver_url,//'http://10.210.9.27:8080/geoserver/wms',
		       params: {'LAYERS': 'LRIT_baseMap'}, //,'FORMAT':'image/jpeg'
		       ratio: 1,
		       serverType: 'geoserver',
		    // Countries have transparency, so do not fade tiles:
	            transition: 0,
		      // crossOrigin:'anonymous',
		       wrapX: false,
		       noWrap: true,
		       opacity: 0
		     })
		   }) ,  vectorLayer		  
			  
		 ];
		}
	catch(err)
	{
		console.info('error in loading map from geoserver: ' + err)
	}

//-----End of Base Map and Ship Layer to be displayed on load --------------------     
	 
	//-----------Map Control Buttons (vertical tool bar) ---------------------
	var element = document.createElement('div');
	var searchShipVertBtn = "";
	var shipByFlagVertBtn="";
	var shipByBoundaryVertBtn="";
	var shipByStatusVertBtn="";
	var surpicRequestVertBtn="";
	var customPolygonVertBtn="";
	var shipHistoryVertBtn="";
	var pathProjVertBtn="";
	var distancRingRangeVertBtn="";
	var gridVertBtn="";	
	var GMLVertBtn="";
	var sarAreaViewVertBtn="";
	var layerListVertBtn="";
	var geographicalLayerVertBtn="";
	var sarRegionVertBtn="";
	var oceanRegionVertBtn="";
	var shippingCompanyVertBtn = "";
	
	 //console.info('above if: ' + userActivitiesAllowed)
	 
	 if(advancedMapFunctionalitiesAnalysisAllowed ==='true'){
		 customPolygonVertBtn = "<button type=\"button\" id=\"customPolygonButton\" class=\"verticalButton\" title=\"Custom Polygon\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\custom_polygon.png\"></button>";
		 pathProjVertBtn = "<button type=\"button\" id =\"pathProjectionButton\" class=\"verticalButton \" title=\"Path Predication\"> <img class=\"icon\" src = \"\\lrit\\resources\\icons\\path_Prediction.png\"></button>";
		 distancRingRangeVertBtn = "<button type=\"button\" id = \"distanceBearingButton\" class=\"verticalButton\" title=\"Distance & Bearing Tool\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\distance_Bearing.png\"></button>";
		 gridVertBtn ="<button type=\"button\" id=\"gridListButton\" class=\"verticalButton \" title=\"Grid List\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\grid.png\"></button>"; 
	 }
	 
	 if(viewAdditionalLayerAllowed ==='true'){
		 layerListVertBtn = "<button type=\"button\" id = \"waterPolyButton\" class=\"verticalButton \" title=\"Layer List \"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\layer_List.png\"></button>";
		 geographicalLayerVertBtn = "<button type=\"button\" id=\"geographicalLayerButton\" class=\"verticalButton\" title=\"Geographical Layer\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\geographical_Layer.png\"></button>";
		 sarRegionVertBtn =  "<button type=\"button\" id = \"sarRegionButton\" class=\"verticalButton\" title=\"SAR Region\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\sar_Region.png\"></button>";
		 oceanRegionVertBtn ="<button type=\"button\" id=\"oceanRegionButton\" class=\"verticalButton \" title=\"Ocean Region\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\satelite_Ocean_Region2.png\"></button>"; 
	 }
	 
	 if(filterVesselforViewingAllowed ==='true'){
		 shipByBoundaryVertBtn = "<button type=\"button\" id=\"shipInCountryBoundaryButton\" class=\"verticalButton \" title=\"Ship In Country Boundary\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\ship.png\"></button>";
		 shipByStatusVertBtn = "<button type=\"button\" id=\"vesselStatusButton\" class=\"verticalButton \" title=\"Vessel Status\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\ship_Status.png\"></button>";
		 shippingCompanyVertBtn = "<button type=\"button\" id=\"shippingCompanyButton\" class=\"verticalButton\" title=\"Shipping Company \"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\shipping_Company.png\"></button>";
		 searchShipVertBtn ="<button type=\"button\" id=\"searchShipPortButton\" class=\"verticalButton\" title=\"Search\"> <img class=\"icon\" src = \"\\lrit\\resources\\icons\\ship_Search.png\"> </button>";
		 shipHistoryVertBtn = "<button type=\"button\" id =\"shipHistoryButton\" class=\"verticalButton \" title=\"Multiple Ship History\"> <img class=\"icon\" src = \"\\lrit\\resources\\icons\\ship_History.png\"></button>";
	 }
	
	 if(viewArchievedSurpicAllowed==='true')
	 {
		 sarAreaViewVertBtn = "<button type=\"button\" id=\"sarAreaViewButton\" class=\"verticalButton \" title=\"SAR Area View \"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\sar_View.png\"></button>";
	 }
		 
	 if(shipCountryFlagAllowed==='true'){
		 shipByFlagVertBtn = "<button type=\"button\" id=\"countryListButton\" class=\"verticalButton \" title=\"Country Ships\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\flags.png\"> </button>";
	 }
	 
	if(addGeographicalAreaDDPAllowed ==='true'){		
		GMLVertBtn = "<button type=\"button\" id =\"geographicalAreaButton\" class=\"verticalButton \" title=\"Geographical Area\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\geographical_Area.png\"></button>";		
	}
	if(sarSurpicAllowed==='true')
	{
		surpicRequestVertBtn = "<button type=\"button\" id=\"sarViewButton\" class=\"verticalButton \" title=\"SAR SURPIC Request\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\sarSurpic_View.png\"></button>";
	}
	
	if(coastalSurpicAllowed=='true')
	{
		surpicRequestVertBtn = "<button type=\"button\" id=\"sarViewButton\" class=\"verticalButton \" title=\"Coastal SURPIC Request\"><img class=\"icon\" src = \"\\lrit\\resources\\icons\\sarSurpic_View.png\"></button>";
	}
		element.innerHTML = "<div class=\"ol-custom scrollbar-vertical-button\"><div class=\"ol-custom1 btn-group-vertical\">" +
				"<button type=\"button\" id=\"hideButton\" class=\"verticalButton\" title=\"Show & Hide Icons\"><img id=\"imageArrow\" class=\"icon\" src = \"\\lrit\\resources\\icons\\upArrow.png\"></button>" +
				"</div><div id=\"demo\" class=\"collapse btn-group-vertical ol-custom2 \" >" +
				searchShipVertBtn +
		        shipByFlagVertBtn + 
		        shipByBoundaryVertBtn +
		        shipByStatusVertBtn +
		        surpicRequestVertBtn +
		        customPolygonVertBtn +
		        shipHistoryVertBtn +
		        pathProjVertBtn +
		        distancRingRangeVertBtn +     
		        gridVertBtn +
		        GMLVertBtn +
		        sarAreaViewVertBtn +        
		        layerListVertBtn +
		        geographicalLayerVertBtn +
		        sarRegionVertBtn +  
		        oceanRegionVertBtn +       
		        shippingCompanyVertBtn +    
		        "</div></div>";
		
		document.body.appendChild(element);
		//document.getElementById('map').appendChild(element);		
		
	//-----------End of Map Control Buttons (vertical tool bar) --------------------- 
		
	//----------display map on load ------------------------------
	 
		
	var viewport = document.getElementById('map');

	function getMinZoom() {
	  var width = viewport.clientWidth;
	  return Math.ceil(Math.LOG2E * Math.log(width / 256));
	}

	var initialZoom = getMinZoom();
	var fspan = document.createElement('i');
	fspan.setAttribute('class', 'fa fa-expand');
	var nspan = document.createElement('i');
	nspan.setAttribute('class', 'fa fa-compress');
	
	var scaleLineControl = new ol.control.ScaleLine({className: 'ol-scale-line', target: document.getElementById('scale-line')});
	
	const view = new ol.View({
		projection: 'EPSG:4326',
		minZoom: 2,
		maxZoom: 17,
		zoom: zoom
		});
	
	var map = new ol.Map({
		 controls: ol.control.defaults({
	     zoom: false,
	     attribution: false,
	     rotate: false
	     }).extend([
		    new ol.control.Control({
	           element:element               
	        }),
	         scaleLineControl,
	         new ol.control.FullScreen(
	        		 {target: 'fullScreenButton',
	        		  source: 'fullscreen',
	        		  label: fspan,
	        		  labelActive: nspan
	        		  })
	       ]),
		layers: layers,
		target: 'map',		
		view: view
	 });  
	 
	 scaleLineControl.setUnits("nautical");
	 try {
		 countryListIdsShipsForDisplay = countryListIdsShipsForDisplay.replace("[","");
		 countryListIdsShipsForDisplay = countryListIdsShipsForDisplay.replace("]","");
		var countryListIdsShipsTemp = countryListIdsShipsForDisplay.split(',');
		 for(var p = 0; p<countryListIdsShipsTemp.length; p++)
		 {
			 countryListIdsShips.push(countryListIdsShipsTemp[p].trim());	 		 
		 }
		// console.info(countryListIdsShips);
		 getShipPositionOnPageLoad(countryListIdsShips);
		 generateStandingOrder(flagCountry);		
		 
	 }
	 catch(err) {
	   console.info(err);
	 }
	 
	 map.getView().setCenter(centerFocus);	 
	 map.addLayer(vectorLayerForeignShipRespondNormal);
	 map.addLayer(vectorLayerForeignShipMissPosition);
	 map.addLayer(vectorLayerForeignShipNoRespons);
	 map.addLayer(vectorLayerFlagShipRespondNormal);
	 map.addLayer(vectorLayerFlagShipMissPosition);
	 map.addLayer(vectorLayerFlagShipNoRespons);
	 
	 // for grid Line lables on load
	 graticuleOnStyle.setMap(null);
	 graticuleOffStyle.setMap(map);	
	 
	 var allFeaturesOnMap = new ol.Collection(
				vectorLayerFlagShipMissPosition.getSource().getFeatures().concat(vectorLayerFlagShipNoRespons.getSource().getFeatures())
				.concat(vectorLayerFlagShipRespondNormal.getSource().getFeatures()).concat(vectorLayerForeignShipRespondNormal.getSource().getFeatures())
				.concat(vectorLayerForeignShipMissPosition.getSource().getFeatures()).concat(vectorLayerForeignShipNoRespons.getSource().getFeatures())
				.concat(vectorLayer_SelectedShipPosition.getSource().getFeatures()),
			    { unique: true });
		
		var snapCollection = new ol.Collection([], {
		    unique: true
		});

		[vectorLayerFlagShipMissPosition, vectorLayerFlagShipNoRespons, vectorLayerFlagShipRespondNormal,
			vectorLayerForeignShipRespondNormal,vectorLayerForeignShipMissPosition,vectorLayerForeignShipNoRespons,
			vectorLayer_SelectedShipPosition].forEach(function(layer) {
		    layer.getSource().on('addfeature', function(evt) {
		        snapCollection.push(evt.feature);
		    });
		    layer.getSource().on('removefeature', function(evt) {
		        snapCollection.remove(evt.feature);
		    });
		});
		
		
		var snapPrueba = new ol.interaction.Snap({
		    features: snapCollection
		});

		snapPrueba.on('change', function(event) {
		    console.log(this.target);
		});

		  
	//----------End of display map on load ------------------------------  
	 
//-----------Map Control Buttons (horizontal tool bar) ---------------------

//-----------End of Map Control Buttons (horizontal tool bar) --------------------- 
//----------- To Show Key Map--------------------------------------
	function keyMap()
	{
		try{
			$('#keymap').toggleClass("down");
			var add = false;
		  	var flagFound = false;
		  	map.getControls().forEach(function (control) {
		  	  if(control instanceof ol.control.OverviewMap) { 
		  		map.removeControl(control);
		  		flagFound = true;
		  		
		  	  }  	  
		  	});
	
		  	var overviewMapControl1 = new ol.control.OverviewMap({
		  	  // see in overviewmap-custom.html to see the custom CSS used
		  	  className: 'ol-overviewmap ol-custom-overviewmap',
		  	  layers: [	  		
		  	  new ol.layer.Tile({    
		            source: new ol.source.TileWMS({
		              url:geoserver_url, //'http://10.210.9.27:8080/geoserver/wms',
		              params: {'LAYERS': 'LRIT_baseMap'},
		              //ratio: 1,
		              serverType: 'geoserver',
		              wrapX:false,
		              noWrap:true
		            }) 
		  		 }) 
		  	  ],		  	 
		  	  view: new ol.View({
		  		  projection:"EPSG:4326",
		  		 // zoom:2,
		  		  center:[0,0],
		  		  extent: [-180, -90, 180, 90],
		  		  numZoomLevels : 1,	
		  		 // resolutions: [ view.getMaxResolution() ]
		  			  }),
		  	  collapseLabel: '\u00BB',
		  	  label: '\u00AB',
		  	  collapsed: false
		  	});	
			if( flagFound ==false && add == false)
			{
				//overviewMapControl1.getView().fit([-180,-90,180,90], overviewMapControl1.getSize());
				map.addControl(overviewMapControl1);
				add = true;
			}
		}
		catch(err)
		{
			console.info('error in key Map: ' + err)
		}
	}
//----------- End of Show Key Map--------------------------------------		
	
	function horzGridOnMap(){
		try{
			$('#horzGrid').toggleClass("down");
			if(graticuleOn===false)
			{ //graticule.setMap(map);
				graticuleOffStyle.setMap(null);
				graticuleOnStyle.setMap(map);
			  graticuleOn = true;
			}
			else
			{
				 //graticule.setMap(null);
				graticuleOnStyle.setMap(null);
				graticuleOffStyle.setMap(map);				
				graticuleOn = false;
			}
			
		}
		catch(err)
		{
			console.info('error in horizontal toolbar Grid On Map: ' + err)
		}
	}
	
//----------- for ZoomExtent --------------------------------------------
		
		function zoomToExtent(){	
			try{
				var extent = vectorSource.getExtent();
				map.getView().fit(extent, map.getSize());
			}
			catch(err)
			{
				console.info('error in zoom to extent: ' + err)
			}
		}

//-----------end of ZoomExtent --------------------------------------------

		
//----------- for Zoom Percentage --------------------------------
		
		function zoomByPercentage(){
			try{
			 var e = document.getElementById("percentage");
			 var percentage = e.options[e.selectedIndex].text;			 
				  if (percentage=="25%")
					{
					  map.getView().setZoom(3);
					}
				 else if (percentage=="50%")
					map.getView().setZoom(4);
				 else if (percentage=="75%")
					map.getView().setZoom(5);
				 else if (percentage=="100%")
					map.getView().setZoom(6);	
			}
			catch(err)
			{
				console.info('error in zoom by percentage: ' + err)
			}
		}
				
//----------- End of Zoom Percentage --------------------------------	
		
//----------- -Rectangular Zoom --------------------------------------------

		var dragZoom = new ol.interaction.DragZoom({
		  condition: ol.events.condition.always
		});

		var toggleState = 0;
		$("#rectZoom").click(function () {
			try{
				$('#rectZoom').toggleClass("down");
			    if( toggleState == 0) {
			        map.addInteraction(dragZoom);
			        toggleState = 1;
			    }
			    else {
			        map.removeInteraction(dragZoom);
			        toggleState = 0;
			    }			
			}
			catch(err)
			{
				console.info('error in rectangle zoom: ' + err)
			}
		});
		
//------------ End of Rectangular Zoom -----------------------------------------


// ----------- To Enable Pan --------------------------------------
  	
  	function enablePan(){
  		try{
	  		$('#enablePanButton').toggleClass("down");
			map.getInteractions().forEach(function(interaction) {
				if (interaction instanceof ol.interaction.DragPan) {
					interaction.setActive(!interaction.getActive());
				}
			}, this);
  		}
		catch(err)
		{
			console.info('error in enable pan: ' + err)
		}
  	}

// ----------- End of Enable Pan --------------------------------------	
  	
  	
  	
  	var point = null;
  	var line = null;
  	var displaySnap = function(coordinate) {
  	  var closestFeature = vectorLayerFlagShipNoRespons.getClosestFeatureToCoordinate(coordinate);
  //	console.info( 'closestFeature: ' + closestFeature);
  	 // var info = document.getElementById('info');
  	  if (closestFeature === null) {
  	    point = null;
  	    line = null;
  	   // info.innerHTML = '&nbsp;';
  	  } else {
  	    var geometry = closestFeature.getGeometry();
  	    var closestPoint = geometry.getClosestPoint(coordinate);
  	    if (point === null) {
  	      point = new ol.geom.Point(closestPoint);
  	    } else {
  	      point.setCoordinates(closestPoint);
  	    }
  	   // var date = new Date(closestPoint[2] * 1000);
  	   // info.innerHTML =
  	   // console.info( closestFeature.get('PLT') + ' (' + date.toUTCString() + ')');
  	    var coordinates = [coordinate, [closestPoint[0], closestPoint[1]]];
  	    if (line === null) {
  	      line = new ol.geom.LineString(coordinates);
  	    } else {
  	      line.setCoordinates(coordinates);
  	    }
  	  }
  	  map.render();
  	};
  	
  	
  	
  	
  	var selected = null;
 
 map.on("pointermove", function (event) {	 
	 
	  try{	
		  		 
		  if (selected !== null) {
			  if(selected.get('type') == 'CustomCoastal')				 
				  selected.setStyle(CC_Style);	
			  else if(selected.get('type') == 'InternalWaters')				  
				  selected.setStyle(IW_Style);	
			  else if(selected.get('type') == 'CoastalSea')				  
				  selected.setStyle(CS_Style);	
			  else if(selected.get('type') == 'TerritorialSea')
				  selected.setStyle(TS_Style);	
			  else if(selected.get('type') == 'OpenSo')				 
				  selected.setStyle(OT_Style);			
			  else if(selected.get('type') == 'SarAreaForeign')				 
				  selected.setStyle(SarPolygonStyleForeign);
			  else if(selected.get('type') == 'SarAreaFlag')				 
				  selected.setStyle(SarPolygonStyleFlag);
			  
			    selected = null;
			  }

		  map.forEachFeatureAtPixel(event.pixel, function(f) {
			  
					if (f.getGeometry().getType() === 'Polygon')
					{	
					    selected = f;
					    if(f.get('type') == 'CustomCoastal')
							 f.setStyle(styleFunctionWaterPolygonCC(f));
						    //  f.setStyle(CC_Style);	
						  else if(f.get('type') == 'InternalWaters')
							  f.setStyle(styleFunctionWaterPolygonIW(f));
						     // f.setStyle(IW_Style);	
						  else if(f.get('type') == 'CoastalSea')
							  f.setStyle(styleFunctionWaterPolygonCS(f));
					    	//  f.setStyle(CS_Style);	
						  else if(f.get('type') == 'TerritorialSea')
							 f.setStyle(styleFunctionWaterPolygonTS(f));
					    	//  f.setStyle(TS_Style);	
						  else if(f.get('type') == 'OpenSo')
							  f.setStyle(styleFunctionWaterPolygonOT(f));
						  else if(f.get('type') == 'SarAreaFlag' || f.get('type') == 'SarAreaForeign')
							  f.setStyle(styleFunctionSelectSarPolygon(f));
					    
					    	 // f.setStyle(OT_Style);
					}
			  //  f.setStyle(highlightStyle);
			    return true;
			  });

		  var pointCordinates = ol.coordinate.toStringHDMS(event.coordinate); //ol.proj.toLonLat( ,'EPSG:4326')
			 
		  document.getElementById("lonLatInfo").innerHTML =pointCordinates;// latValue + " " + lonValue;
		  
		  }
		catch(err)
		{
			console.info('error in pointermove: ' + err)
		}
	  
 });
 
 function DegMinSecToLatLon(deg , min ,sec, sign){    	
		 
	  return (parseInt(deg) + (min/60)  + (sec/3600))*sign;
 }
//---------- Left click on map event -------------------------------
 
 var imoNoforPopup;
 var previousDrawDistance = 0;
 var currentDrawDistance = 0;
 var cnt= 0; 
 var dragPan;
 
 var popupLineStyle = new ol.style.Style({
	  stroke: new ol.style.Stroke({
     color: 'darkred',
     width: 1.5
    })
 });
 
 var clickevtMap = (function(evt) {	
	  try
	  {
	 
		 // displaySnap(evt.coordinate);
		  
		  $("#rightClickDiv").hide();
		  console.log(evt.coordinate);    	 
		  var geometryFeature = map.forEachFeatureAtPixel(
				  evt.pixel, function(feature) { return feature;
	 		});
		
		if(geometryFeature)
		{
			if (geometryFeature.getGeometry().getType() === 'Point')
			{	
				// if distance and bearing tool is on then on click display distance
				if(distanceFlag==true)
				{
					//sleep(300);
					countDrawDistance = countDrawDistance +1;
					currentDrawDistance = countDrawDistance;
					if(countDrawDistance>=2 && currentDrawDistance== previousDrawDistance+1){
						calculateDistanceBearing();	
						previousDrawDistance = currentDrawDistance;
					}
					else
					{
						
						if(countDrawDistance==1)
						{
						// to remove other(previous) drawn line
						 // removePreviousDistanceLine();
						  
						  // map.un('singleclick', clickevt);
						  document.getElementById("distanceTableId").value = '';
						  elementTable.innerHTML = "<div class=\"ol-customtest\"> <table id = \"distanceTable\" class=\"table table-bordered table-hover\" > <tr class = \"trMap\"> "+					
							"<th > Source Lat </th> <th> Source Long </th> <th> Dest Lat </th> <th> Dest Long </th>"+
							"<th> Distance </th> <th> Angle </th> <th> Cum. Distance </th> </tr></table>" +
							"</div";
						}
						previousDrawDistance = currentDrawDistance;
					}
					
				}
				else if(pathProjectionStartFlag == true)
				{	
					currentShip = geometryFeature.get('imo_no');					
					var pointCordinates = ol.proj.toLonLat(geometryFeature.getGeometry().getCoordinates(),'EPSG:4326');
					currentShipLat = pointCordinates[1];
					currentShipLong = pointCordinates[0];
					currentShipCourse = geometryFeature.get('course');	
					currentShipSpeed = geometryFeature.get('speed');
					document.getElementById("PPimoNoVal").value = currentShip;
					//showPathProjection(currentShipLong, currentShipLat, currentShipCourse, pathProjectionLength*100000);	
					if (currentShip){
						//showPathProjection(currentShipLong, currentShipLat, currentShipCourse, pathProjectionLength*100000);
						pathProjectionLength = $("#idRingDistance").val();
						pathProjectionAfterThisHour = pathProjectionLength;
						if(currentShipSpeed>0 && currentShipCourse>0)
							showPathProjection(currentShipLat,currentShipLong, currentShipCourse, currentShipSpeed*1850*pathProjectionAfterThisHour);//in meters, Knots is nm/hr
						else
							alert('Not enough information for Predicting Path')
					}
				}
				else if (surpicInteractionOnFlag<2)
				{
					surpicInteractionOnFlag++;
					
				}
				// else on click display ship details
				else if (ringRangeFlag==false && pathProjectionStartFlag == false && distanceFlag==false && surpicInteractionOnFlag>1 && customPolygonInteractionFlag==false){
			
					var coordinate = evt.coordinate;
					
					var hdms = ol.coordinate.toStringHDMS(ol.proj.toLonLat(coordinate,'EPSG:4326'));
					
					imoNoforPopup = geometryFeature.get('message_id');	
					if(imoNoforPopup==null)
						imoNoforPopup = geometryFeature.get('imo_no');
					
					//var messageIdForPopup = geometryFeature.get('message_id').trim();	
					var clickedShipInfo;		
					var currentShipState = 'latest';
					if(imoNoforPopup != undefined || imoNoforPopup!=null)
						clickedShipInfo = AllShipStatusInfo.get(imoNoforPopup.trim()); //getShipInfoFromloadedShipData(imoNoforPopup);	
					//clickedShipInfo = AllShipStatusInfo.get(messageIdForPopup.trim()); //getShipInfoFromloadedShipData(imoNoforPopup);
											
					if(clickedShipInfo == undefined)
						{
						    messageIdForPopup = geometryFeature.get('message_id').trim();						   
							clickedShipInfo = surpicShipInfoForDisplay.get(messageIdForPopup);		
							currentShipState = 'surpic';	
							imoNoforPopup = messageIdForPopup;
						}
					if(clickedShipInfo == undefined)
						{
						 	messageIdForPopup = geometryFeature.get('message_id').trim();						 	
							clickedShipInfo = historyShipInfoForDisplay.get(messageIdForPopup);
							currentShipState = 'history'; 		
							imoNoforPopup = messageIdForPopup;
						}
					
					if(clickedShipInfo == undefined){
						imoNoforPopup = geometryFeature.get('imo_no');
						currentShipState = 'latest';
						clickedShipInfo = AllShipStatusInfo.get(imoNoforPopup.trim());
					}
					
					if(clickedShipInfo != undefined && clickedShipInfo.popupStatus==false) //
					{						
						clickedShipInfo.popupStatus=true;
						if(currentShipState == 'latest')
							AllShipStatusInfo.set(imoNoforPopup.trim(), clickedShipInfo); 
						else if(currentShipState == 'surpic')
							surpicShipInfoForDisplay.set(imoNoforPopup.trim(), clickedShipInfo);
						else if(currentShipState == 'history')
							historyShipInfoForDisplay.set(imoNoforPopup.trim(), clickedShipInfo);
						
						var pointCordinates = ol.coordinate.toStringHDMS(ol.proj.toLonLat(geometryFeature.getGeometry().getCoordinates(),'EPSG:4326'));
						
						var noSpaceStr = pointCordinates.replace(/\s/g, '');
						
						var index = noSpaceStr.indexOf("N");
						
						if(index==-1)
							index = noSpaceStr.indexOf("S");
						
						var indexTime = (clickedShipInfo.timestamp4).indexOf("T");
						var indexPlus = (clickedShipInfo.timestamp4).indexOf("+");
						var date = new Date(Date.parse(clickedShipInfo.timestamp4));
						
						var dd = date.getDate();
						var mm = date.getMonth()+1; 
						var yyyy = date.getFullYear();
						
						var hr = date.getHours();
						var min = date.getMinutes();
						var sec = date.getSeconds();
					
						if(dd<10) 
						{
						    dd='0'+dd;
						} 
			
						if(mm<10) 
						{
						    mm='0'+mm;
						} 
						
						date =dd+'/'+  mm+'/'+ yyyy;
						var time = hr +":"+ min +":" + sec;						
						var featureToCompare;
						var flagCountryToCompare;
						if(countryListIdsShips.length>1)
				    	{
					    	featureToCompare = clickedShipInfo.dc_id; //'dc_id';
					    	flagCountryToCompare = '3065';
				    	}
					    else if(countryListIdsShips.length==1)
				    	{
					    	featureToCompare = clickedShipInfo.data_user_provider; //'dataUserProvider';
					    	flagCountryToCompare = countryListIdsShips[0].trim();;
				    	}
												
						var datauserCountry = null;
						
						if(clickedShipInfo.data_user_provider !=null)
							datauserCountry = getCgNameByCgLritId(clickedShipInfo.data_user_provider.trim());
						
						if(featureToCompare == flagCountryToCompare)
						{
							var speedInfo = clickedShipInfo.speed;
							var courseInfo = clickedShipInfo.course;
							if(speedInfo!=null)
								speedInfo = speedInfo + " knots";
							if(courseInfo!=null)
								courseInfo = courseInfo + " deg";
							
							output =  
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (noSpaceStr.substring(0,index+1)).trim()+" </div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (noSpaceStr.substring(index+1,noSpaceStr.length)).trim()+"</div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ ((clickedShipInfo.timestamp4).substring(0,indexTime)).trim() +"</div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ ((clickedShipInfo.timestamp4).substring(indexTime+1,indexPlus)).trim() +"</div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (clickedShipInfo.imo_no) +" </div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (clickedShipInfo.mmsi_no) +" </div>" +						
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (clickedShipInfo.ship_name) +" </div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ speedInfo +"</div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ courseInfo +"</div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ datauserCountry +" </div>"; //+
						}
						else
							{
							output = 
								"<div onmousedown=\"return false\" class= \"popupFont\">"+ (noSpaceStr.substring(0,index+1)).trim()+" </div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (noSpaceStr.substring(index+1,noSpaceStr.length)).trim()+"</div>" +		
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ ((clickedShipInfo.timestamp4).substring(0,indexTime)).trim() +"</div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ ((clickedShipInfo.timestamp4).substring(indexTime+1,indexPlus)).trim() +"</div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (clickedShipInfo.imo_no) +" </div>" +
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (clickedShipInfo.mmsi_no) +" </div>" +						
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ (clickedShipInfo.ship_name) +" </div>" +							
							"<div onmousedown=\"return false\" class= \"popupFont\">"+ datauserCountry +" </div>";
							}
					var popupdiv=document.createElement("div");
					var overlayId = imoNoforPopup + '_popup'; // clickedShipInfo.message_id.trim();
				    var id = overlayId; // (cnt++)+'popup';
				    popupdiv.setAttribute("id", id);
				    popupdiv.setAttribute("class", "ol-popup");
				  				 
				    document.body.appendChild(popupdiv);
				    
				    var companyVesselDetailsDiv = "";
				    if(featureToCompare == flagCountryToCompare)
				    {
				    	companyVesselDetailsDiv = "<div  onmousedown=\"return false\" class= \"popupFont\" style=\"text-decoration: underline blue; \"><a Style=\"color: gray\" href=# id="+id+"-company \">Company Details</a></div>" +
				    	"<div  onmousedown=\"return false\" class= \"popupFont\" style=\"text-decoration: underline blue;\"><a Style=\"color: gray\" href=# id="+id+"-vessel \">Vessel Details</a> </div>";
				    }
				     popupdiv.innerHTML =  "<div onmousedown=\"return false\" id="+id+"-title class=ol-popup-title></div> <a href=# id="+id+"-closer class=ol-popup-closer></a><a href=# id="+id+"-resize class=ol-popup-resize></a>"+
				     "<div onmousedown=\"return false\" class= \"popupFont\" Style=\"padding-top:5px; color: #337ab7\">----------------------</div><div onmousedown=\"return false\" id="+id+"-content class=\"popup-content scrollbar-popup\">"+ output +  
				     companyVesselDetailsDiv + "</div>" ; 
				    
				     if(companyVesselDetailsDiv != "")				   
				     {
				    	var vesselClick  = document.getElementById(id+"-vessel");
					    vesselClick.onclick = function() {				    	
					    	window.open("/lrit/ship/viewvesselonmap/"+id.split('_')[0], '_blank');
						};
						
						 var companyClick  = document.getElementById(id+"-company");
						 		companyClick.onclick = function() {					    
						    	window.open("/lrit/users/companycsodetails/?imoNo="+id.split('_')[0], '_blank');
							};
			    	 }
					
				    // document.getElementById(id).style.resize = "both";
				     var content = document.getElementById(id+"-content");
				     var closer = document.getElementById(id+"-closer");
				     closer.onclick = function() {
				    	overlay.setPosition(undefined);
						map.removeOverlay(overlay);		
						removeLayersFromMap('popupLineLayer'+id,'name','Line'+id);
						
						 var findShipKeyInd = id.indexOf("_popup");
						 var findShipKey = (id.substring(0,findShipKeyInd)).trim();
						var clickedShipInfoClose = AllShipStatusInfo.get(findShipKey);	
			   			var currentShipStateClose =  'latest';
			   			if(clickedShipInfoClose == undefined)
						{		   						   
			   				clickedShipInfoClose = surpicShipInfoForDisplay.get(findShipKey);	
			   				currentShipStateClose = 'surpic';
						}
					    if(clickedShipInfoClose == undefined)
						{			 						 	
					    	clickedShipInfoClose = historyShipInfoForDisplay.get(findShipKey);
					    	currentShipStateClose = 'history';
						}
					    if(clickedShipInfoClose != undefined){
						
					    	clickedShipInfoClose.popupStatus=false;
					    	
						if(currentShipStateClose == 'latest')
							AllShipStatusInfo.set(findShipKey, clickedShipInfoClose); 
						else if(currentShipStateClose == 'surpic')
							surpicShipInfoForDisplay.set(findShipKey, clickedShipInfoClose);
						else if(currentShipStateClose == 'history')
							historyShipInfoForDisplay.set(findShipKey, clickedShipInfoClose);
					    }				
					//AllShipStatusInfo.set(imoNoforPopup, clickedShipInfo); 
					};
					var resize = document.getElementById(id+"-resize");
					resize.onclick = function() {				    	
				    	 if(clickedShipInfo.popupResizeStatus==false)
				    	 { 			
				    		 document.getElementById(id+"-resize").className = "ol-popup-resizeMin";				    		
					    	 clickedShipInfo.popupResizeHeight = document.getElementById(id+"-content").offsetHeight;
					    	 clickedShipInfo.popupResizeWidth = document.getElementById(id+"-content").offsetWidth;
					    	 document.getElementById(id).style.height = '100px';
					    	 document.getElementById(id).style.width = '125px';
					    	 document.getElementById(id+"-content").style.height = '55px'; 
					    	 document.getElementById(id+"-content").style.width = '110px'; 
					    	 clickedShipInfo.popupResizeStatus=true;					    	 
				    	 }
				    	 else
			    		 {
				    		 document.getElementById(id+"-resize").className = "ol-popup-resize";
				    		 document.getElementById(id).style.height = clickedShipInfo.popupResizeHeight + 45 + 'px';
				    		 var popupWidthResize = (clickedShipInfo.popupResizeWidth + 30);
				    		 if (popupWidthResize<130)
				    			 popupWidthResize = 130;
					    	 document.getElementById(id).style.width = popupWidthResize +'px';
					    	 document.getElementById(id+"-content").style.height = clickedShipInfo.popupResizeHeight + 'px';
					    	 document.getElementById(id+"-content").style.width = clickedShipInfo.popupResizeWidth+ 'px';
					    	 clickedShipInfo.popupResizeStatus=false;
			    		 }
				    	 				    	
				     }
			      
			      
				   var overlay = new ol.Overlay({
					   id : overlayId,
				       element: popupdiv,				       
				       autoPan: true,
				       autoPanAnimation: {
				    	   duration: 250
				       }				      
			       });     			       
				 
			      map.addOverlay(overlay);    
			     
			     // var pointCordinates = [clickedShipInfo.longitude, clickedShipInfo.latitude];
			      overlay.setPosition(coordinate);			     
			      countDrag = 0;
			      var vectorLayer_Line;
			      var start_point = [coordinate];
   			      var end_point = [overlay.getPosition()];
   			    
   			       var lineCoordinates = [start_point, end_point]; 
   			    
   			       removeLayersFromMap('popupLineLayer'+overlayId,'name','Line'+overlayId);
   			       vectorLayer_Line = new ol.layer.Vector({
   			    	      source: new ol.source.Vector({
   			    	          features: [new ol.Feature({
   			    	              geometry: new ol.geom.LineString(lineCoordinates),
   			    	              name: 'Line'+overlayId
   			    	          })],
   			    	          wrapX: false,
   			    	          noWrap: true
   			    	      }),
   			    	      name: "popupLineLayer" +overlayId
   			    	  });
   			    map.addLayer(vectorLayer_Line);
   			    
   			 
   			 
   			 popupdiv.addEventListener('mousedown', function(evt) {   	
   				evt.preventDefault();
   				draggingPopup =   overlayId; 
   				var mousedowncoord = map.getEventCoordinate(evt);
   				var currentpopupcoord = map.getOverlayById(draggingPopup).getPosition();
   			  function move(evt) {   				  
   				var popupdragoffest =  [mousedowncoord[0]-map.getEventCoordinate(evt)[0],mousedowncoord[1]-map.getEventCoordinate(evt)[1]];
   				var newposition = [currentpopupcoord[0]-popupdragoffest[0],currentpopupcoord[1]-popupdragoffest[1] ];
   				map.getOverlayById(draggingPopup).setPosition(newposition);     				  
   				//map.getOverlayById(draggingPopup).setPosition(map.getEventCoordinate(evt));
   				
   			  }
   			  function end(evt) {   				
   			    window.removeEventListener('mousemove', move);
   			    window.removeEventListener('mouseup', end);
   			    var findShipKeyInd = draggingPopup.indexOf("_popup");
   			    var findShipKey = (id.substring(0,findShipKeyInd)).trim();
	   			var clickedShipInfoTemp = AllShipStatusInfo.get(findShipKey);	
	   			 
	   			if(clickedShipInfoTemp == undefined)
				{		   						   
	   				clickedShipInfoTemp = surpicShipInfoForDisplay.get(findShipKey);		
				}
			    if(clickedShipInfoTemp == undefined)
				{			 						 	
				 	clickedShipInfoTemp = historyShipInfoForDisplay.get(findShipKey);
				}
			    if(clickedShipInfoTemp != undefined){
				   
					var pointCordinates = [clickedShipInfoTemp.longitude, clickedShipInfoTemp.latitude];
					countDrag = countDrag+5;				;
					start_point = pointCordinates;
		   			 end_point =  map.getOverlayById(draggingPopup).getPosition();					    				
					var lineCoordinates = [start_point, end_point]; 
				       
				       removeLayersFromMap('popupLineLayer'+draggingPopup,'name','Line'+draggingPopup);
				       
				       var features = new ol.Feature({
				    	   	  name: 'Line'+draggingPopup,
							  geometry: new ol.geom.LineString(lineCoordinates)					  
						  });
			
				       features.setStyle(popupLineStyle);
				       vectorLayer_Line = new ol.layer.Vector({
				    	      source: new ol.source.Vector({wrapX: false,
				    		    	 noWrap: true}),
				    	      name: "popupLineLayer" +draggingPopup
				    	  });
				        vectorLayer_Line.getSource().addFeature(features);		       
				       	map.addLayer(vectorLayer_Line);
			    }
   			  }
   			  
   			  window.addEventListener('mousemove', move);
   			  window.addEventListener('mouseup', end);
   			 
   			});
   			
			  			      
				}
			} // end of else on click display ship details
	   	} // end of if (geometryFeature.getGeometry().getType() === 'Point')
			else if (geometryFeature.getGeometry().getType() === 'LineString')
			{
								
				var currentSelectedLine = geometryFeature.get('name');
					
					var overlay = map.getOverlays();
					var overlayToRemoveArr = [];					
					for (var m=0; m<overlay.getLength(); m++)
					{						
						var currentOverlayId = overlay.item(m).getId();
						
						if(currentOverlayId!= undefined )
						{							
							if( currentOverlayId.match(currentSelectedLine))
							{
								overlayToRemoveArr.push(currentOverlayId);							
							}
						}
					}
					
					for (var n=0; n<overlayToRemoveArr.length; n++)
					{
						var overlayToRemove = map.getOverlayById(overlayToRemoveArr[n]);
						map.removeOverlay(overlayToRemove);
					}					
				
					var features = vectorLayer_SelectedShipPosition.getSource().getFeatures();
					features.forEach(function(feature) {
						  
				    	if (feature.get('name').match(currentSelectedLine))	      
						    {
				    		vectorLayer_SelectedShipPosition.getSource().removeFeature(feature);
				    	};
				    });
			}
			
		} //end of if(geometryFeature)	
		else{
			if(pathProjectionStartFlag == true)
			{	
				addInteractionCircle(evt.coordinate);		
				
			}
		}
			
  }
	catch(err)
	{
		console.info('error in Left Click on map event: ' + err)
	}
}); // end of function
 
 
//add single click event to map
 map.on('click', clickevtMap ); 
//----------End of Left click on map event -------------------------------		

//---------- Right Click on Ship -----------------------------------------
	
	var elementList = document.createElement('div');
	elementList.id = "rightClickDiv";	
	elementList.className = "rightClickDivStyle";
	var historyShownFlag = false;
	var pathProjectionFlag = false;
	var surpicRequestFlag = false;
	var currentShip;
	var currentShipLat;
	var currentShipLong;
	var currentShipCourse;
	var rightClickCoodinate;
	var showSarSurpicRequestFlag = false;
	map.getViewport().addEventListener('contextmenu', function (evt) {
		try{
			
			evt.preventDefault();		
			
			if(distanceFlag==true)
			{					
				countDrawDistance = countDrawDistance +1;
				currentDrawDistance = countDrawDistance;
				if(countDrawDistance>=2 && currentDrawDistance== previousDrawDistance+1){
					calculateDistanceBearing();	
					previousDrawDistance = currentDrawDistance;
				}
				else
				{					
					if(countDrawDistance==1)
					{
					// to remove other(previous) drawn line
					 // removePreviousDistanceLine();					 
					  document.getElementById("distanceTableId").value = '';
					  elementTable.innerHTML = "<div class=\"ol-customtest\"> <table id = \"distanceTable\" class=\"table table-bordered table-hover\" > <tr class = \"trMap\"> "+					
						"<th > Source Lat </th> <th> Source Long </th> <th> Dest Lat </th> <th> Dest Long </th>"+
						"<th> Distance </th> <th> Angle </th> <th> Cum. Distance </th> </tr></table>" +
						"</div";
					}
					previousDrawDistance = currentDrawDistance;
				}				
			}
			
			else
			{
				
			$("#rightClickDiv").show();
				
			var coordinate = map.getEventCoordinate(evt);
			rightClickCoodinate = coordinate;
			var pixel = map.getPixelFromCoordinate(coordinate);
			
			var geometryFeature = map.forEachFeatureAtPixel(pixel,
				  function(feature) { return feature;
			 });
			
			var points = turf.point(coordinate);
		    var searchWithin = turf.polygon(StandingOrderInfo[0][0].poslist.coordinates); 			
			var ptsWithin = turf.pointsWithinPolygon(points, searchWithin); 			
		    var turfFeature = ptsWithin.features;		    
		    var commonOptionForeginShip;
		    var commonOptionFlagShip = "";
		    var sarSurpicRequestToAdd = "";
		    
		    var featuresSar = vectorLayerSURPIC24Hours.getSource().getFeatures();
		    var ptsWithinSarFlag = false;
		   featuresSar.forEach(function(feature) {	
	    		var searchWithinSar = turf.polygon(feature.getGeometry().getCoordinates());
	    		var ptsWithinSar = turf.pointsWithinPolygon(points, searchWithinSar); 			
			    var turfFeatureSar = ptsWithinSar.features;
			    if(turfFeatureSar.length>0)
			    	ptsWithinSarFlag = true;
		    });	
		    
		    var featuresSarCurrent = layerCustomPolygon.getSource().getFeatures();
		    
		    featuresSarCurrent.forEach(function(feature) {		  
		    	if (feature.get('areatype') === 'sararea')	      
			    {
		    		var searchWithinSarCurrent = turf.polygon(feature.getGeometry().getCoordinates());
		    		var ptsWithinSarCurrent = turf.pointsWithinPolygon(points, searchWithinSarCurrent); 			
				    var turfFeatureSarCurrent = ptsWithinSarCurrent.features;
				    if(turfFeatureSarCurrent.length>0)
				    	ptsWithinSarFlag = true;
			    }
		    });	
		   var rightClickSARPollShipAllowed = 'false';
		    if (rightClickSARPollAllowed==='true' && ptsWithinSarFlag == true)
		    	rightClickSARPollShipAllowed = 'true';
		    
		    //console.info('ptsWithinSarFlag: ' + ptsWithinSarFlag);
		    
		    if(turfFeature.length>0 && coastalSurpicAllowed === 'true')    	{
		    	showSarSurpicRequestFlag = true;	    	
				sarSurpicRequestToAdd = "<li class=\"sarsurpicClass liRightClick\"> <a href=\"#\">Coastal SURPIC Request </a> </li>";	    	
	    	}
		    else if(sarSurpicAllowed==='true')
	    	{
		    	sarSurpicRequestToAdd = "<li class=\"sarsurpicClass liRightClick\"> <a href=\"#\">SAR SURPIC Request </a> </li>";
	    	}
		    if(rightClickAllowed === 'true' &&  rightClickSARPollShipAllowed==='true'  && rightClickPortAllowed ==='true'){
		    	
			 commonOptionForeginShip = "<li class=\"portFClass liRightClick\"> <a href=\"#\">Port </a> </li>" +
			"<li class=\"costalFClass liRightClick\"> <a href=\"#\">Coastal </a> </li>" +
			//"<li class=\"archiveFClass liRightClick\"> <a href=\"#\">Archive </a> </li>" +
			//"<li class=\"stopClass liRightClick\"> <a href=\"#\">Stop </a> </li>" +
			//"<li class=\"pollClass liRightClick\"> <a href=\"#\">Poll </a> </li>" +
			"<li class=\"sarPollFClass liRightClick\"> <a href=\"#\">SAR Poll </a> </li>" +
			"<li class=\"showHistClass liRightClick\"> <a href=\"#\">Show History</a> </li>"; /*+
			"<li class=\"showPathClass liRightClick\"> <a href=\"#\">Show Path Projection</a> </li>";*/
			 
			 commonOptionFlagShip = "<li class=\"flagClass liRightClick\"> <a href=\"#\">Flag </a> </li>" +
				"<li class=\"stopFlagClass liRightClick\"> <a href=\"#\">Stop </a> </li>" +
				"<li class=\"pollFlagClass liRightClick\"> <a href=\"#\">Poll </a> </li>" +
				"<li class=\"showHistClass liRightClick\"> <a href=\"#\">Show History </a> </li>"; /* +	
				"<li class=\"showPathClass liRightClick\"> <a href=\"#\">Show Path Projection</a> </li>";*/
		    }
		    else if(rightClickAllowed === 'true' &&  rightClickSARPollShipAllowed==='false' && rightClickPortAllowed ==='true')
	    	{
	    	
		    	 commonOptionForeginShip = "<li class=\"portFClass liRightClick\"> <a href=\"#\">Port </a> </li>" +
					"<li class=\"costalFClass liRightClick\"> <a href=\"#\">Coastal </a> </li>" +
					//"<li class=\"archiveFClass liRightClick\"> <a href=\"#\">Archive </a> </li>" +
					//"<li class=\"stopClass liRightClick\"> <a href=\"#\">Stop </a> </li>" +
					//"<li class=\"pollClass liRightClick\"> <a href=\"#\">Poll </a> </li>" +					
					"<li class=\"showHistClass liRightClick\"> <a href=\"#\">Show History</a> </li>"; /*+
					"<li class=\"showPathClass liRightClick\"> <a href=\"#\">Show Path Projection</a> </li>";*/
					 
					 commonOptionFlagShip = "<li class=\"flagClass liRightClick\"> <a href=\"#\">Flag </a> </li>" +
						"<li class=\"stopFlagClass liRightClick\"> <a href=\"#\">Stop </a> </li>" +
						"<li class=\"pollFlagClass liRightClick\"> <a href=\"#\">Poll </a> </li>" +
						"<li class=\"showHistClass liRightClick\"> <a href=\"#\">Show History </a> </li>"; /* +	
						"<li class=\"showPathClass liRightClick\"> <a href=\"#\">Show Path Projection</a> </li>";*/
	    	}
		    else if(rightClickAllowed === 'true' &&  rightClickSARPollShipAllowed ==='false' && rightClickPortAllowed ==='false')
	    	{
	    	
		    	 commonOptionForeginShip = "<li class=\"costalFClass liRightClick\"> <a href=\"#\">Coastal </a> </li>" +
					//"<li class=\"archiveFClass liRightClick\"> <a href=\"#\">Archive </a> </li>" +
					//"<li class=\"stopClass liRightClick\"> <a href=\"#\">Stop </a> </li>" +
					//"<li class=\"pollClass liRightClick\"> <a href=\"#\">Poll </a> </li>" +					
					"<li class=\"showHistClass liRightClick\"> <a href=\"#\">Show History</a> </li>"; /*+
					"<li class=\"showPathClass liRightClick\"> <a href=\"#\">Show Path Projection</a> </li>";*/
					 
					 commonOptionFlagShip = "<li class=\"flagClass liRightClick\"> <a href=\"#\">Flag </a> </li>" +
						"<li class=\"stopFlagClass liRightClick\"> <a href=\"#\">Stop </a> </li>" +
						"<li class=\"pollFlagClass liRightClick\"> <a href=\"#\">Poll </a> </li>" +
						"<li class=\"showHistClass liRightClick\"> <a href=\"#\">Show History </a> </li>"; /* +	
						"<li class=\"showPathClass liRightClick\"> <a href=\"#\">Show Path Projection</a> </li>";*/
	    	}
		    	
		    
			if(geometryFeature)
			{			
				if (geometryFeature.getGeometry().getType() === 'Point')
				{			
					// To save current ship details					 
					currentShip = geometryFeature.get('imo_no');
					var pointCordinates = ol.proj.toLonLat(geometryFeature.getGeometry().getCoordinates(),'EPSG:4326');
					currentShipLat = pointCordinates[0];
					currentShipLong = pointCordinates[1];
					currentShipCourse = geometryFeature.get('course');
					
					
					var featureToCompare;
					var flagCountryToCompare;
					if(countryListIdsShips.length>1)
			    	{
				    	featureToCompare ='dc_id';
				    	flagCountryToCompare = '3065';
			    	}
				    else if(countryListIdsShips.length==1)
			    	{
				    	featureToCompare = 'dataUserProvider';
				    	flagCountryToCompare = countryListIdsShips[0].trim();;
			    	}
					
					
					
					// if Ship is Foreign Ship Show following Options.
				      if(geometryFeature.get(featureToCompare) != flagCountryToCompare)		    	  
			    	  {				    	 
				    	  elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
				    	  commonOptionForeginShip + sarSurpicRequestToAdd + "</ul>"; //sarSurpicRequestToAdd+
			    	  }			     
				      // if Ship is Flag Ship Show following Options.
				       if(geometryFeature.get(featureToCompare) == flagCountryToCompare)
			    	  {				    	 
				    	  elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
				    	  commonOptionFlagShip + sarSurpicRequestToAdd + "</ul>";	
			    	  }			     
				}
				else
				{
					if (geometryFeature.get("areatype") === 'userarea'){ //CustomPolygon
						customPolygonRighClickMenu(geometryFeature,elementList);
					}		
					
					if (geometryFeature.get("areatype") === 'surpicregion'){ //surpicregion
						surpicRegionRighClickMenu(geometryFeature,elementList);
					}
				}
			}
			else
			{			
				if (historyShownFlag && pathProjectionFlag == false && surpicRequestFlag == false)
					{
					// if click at any point but history is shown
						elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
						sarSurpicRequestToAdd +					
						"<li class=\"hideHistClass liRightClick\" > <a href=\"#\">Hide History </a> </li>" +			
						"</ul>";
					}
				else if (pathProjectionFlag && historyShownFlag == false && surpicRequestFlag == false)
				{
				// if click at any point but path projection is shown
					elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
					sarSurpicRequestToAdd +				
					"<li class=\"hidePathClass liRightClick\" > <a href=\"#\">Hide Path Projection </a> </li>" +			
					"</ul>";
				}
				else if (surpicRequestFlag && historyShownFlag == false && pathProjectionFlag == false)
				{
				// if click at any point but path projection is shown
					elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
					sarSurpicRequestToAdd +
					"<li class=\"closeSarClass liRightClick\" > <a href=\"#\">Close SAR </a>  </li>" +								
					"</ul>";
				}
				
				else if (pathProjectionFlag && historyShownFlag && surpicRequestFlag == false)
				{
				// if click at any point but path projection is shown
					elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
					sarSurpicRequestToAdd +				
					"<li class=\"hideHistClass liRightClick\" > <a href=\"#\">Hide History </a> </li>" +
					"<li class=\"hidePathClass liRightClick\" > <a href=\"#\">Hide Path Projection </a> </li>" +			
					"</ul>";
				}
				else if(surpicRequestFlag && pathProjectionFlag && historyShownFlag == false)
				{
					elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
					sarSurpicRequestToAdd +
					"<li class=\"closeSarClass liRightClick\" > <a href=\"#\">Close SAR </a>  </li>" +	
					"<li class=\"hidePathClass liRightClick\" > <a href=\"#\">Hide Path Projection </a> </li>" +
					"</ul>";
				}
				else if(surpicRequestFlag && historyShownFlag && pathProjectionFlag == false)
				{
					elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
					sarSurpicRequestToAdd +
					"<li class=\"closeSarClass liRightClick\" > <a href=\"#\">Close SAR </a>  </li>" +	
					"<li class=\"hideHistClass liRightClick\" > <a href=\"#\">Hide History </a> </li>" +
					"</ul>";
				}
				else if(surpicRequestFlag && pathProjectionFlag && historyShownFlag)
				{
					elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
					sarSurpicRequestToAdd +
					"<li class=\"closeSarClass liRightClick\" > <a href=\"#\">Close SAR </a>  </li>" +	
					"<li class=\"hidePathClass liRightClick\" > <a href=\"#\">Hide Path Projection </a> </li>" +
					"<li class=\"hideHistClass liRightClick\" > <a href=\"#\">Hide History </a> </li>" +
					"</ul>";
				}			
				else
					{
					// if click at any point
						elementList.innerHTML ="<ul class=\"ulRightClick\" id=\"results\">" +
						sarSurpicRequestToAdd +
						/*"<li class=\"closeSarClass liRightClick\" > <a href=\"#\">Close SAR </a>  </li>" +*/		
						"</ul>";
					}
			}
			
			// add right click overlay to map	 
			var overlay = new ol.Overlay({
		 		 element: elementList,
		         autoPan: true,
		         autoPanAnimation: {
		         duration: 250
		         }
		       });     
		       
		      map.addOverlay(overlay);    
		      overlay.setPosition(coordinate);
			}
		}
	catch(err)
	{
		console.info('error in generating Standing Order: ' + err)
	}
	});
		document.body.appendChild(elementList);

//----------- End of Right Click on Ship --------------------------------------------------------------

		
		function removeAllLayersRefresh()	
		{
			
			//try{
			 	removeGeographicalLayers();
				var layersToRemove = [];
				  map.getLayers().forEach(function (layer) {
				      if (layer.get('name') != undefined && !(layer.get('name')).match("ShipLayerOnLoad") &&
				    		  !(layer.get('name')).match("StandingOrder")&& !(layer.get('name')).match("sarRegionLayer") &&
				    		  !(layer.get('name')).match("SURPIC24HoursLayer")) {		    	 
				    	  layer.getSource().clear();
				    	  layersToRemove.push(layer);		         
				      }
				  });
		
				  var len = layersToRemove.length;
				 
				  for(var i = 0; i < len; i++) {
					  
					  var features = layersToRemove[i].getSource().getFeatures();
					  	features.forEach(function(feature) {		  
				    		layersToRemove[i].getSource().removeFeature(feature);					    	
					    });					
					  	map.removeLayer(layersToRemove[i]);
				      }
				  
				 // map.getOverlays().clear();
				  disableMeasuringTool();
				  stopPathProjection();				 
				  vectorSource_Temp.clear();
				  vectorLayerFlagShipRespondNormal.setVisible(true);
				  vectorLayerFlagShipMissPosition.setVisible(true);
				  vectorLayerFlagShipNoRespons.setVisible(true);
				  vectorLayerForeignShipRespondNormal.setVisible(true);
				  vectorLayerForeignShipMissPosition.setVisible(true);
				  vectorLayerForeignShipNoRespons.setVisible(true);		
				  map.addLayer(layerCustomPolygon);
				  map.addLayer(vectorLayer_SelectedCountryShipPosition);
				  map.addLayer(vectorLayer_SelectedCountryBoundaryShipPosition);
				  map.addLayer(vectorLayer_SelectedCompanyShipPosition);
				  map.addLayer(vectorLayer_SelectedShipPosition);
				  map.addLayer(vectorLayer_pathProjection);
				  map.addLayer(vectorLayer_selectedPort);
				  map.addLayer(layerSurpicPolygon);
				  map.getView().setZoom(zoom);
				  map.getView().setCenter(centerFocus);
				  layerSARRegion.setVisible(false);
				  $('#ringDistanceInfo').hide();
				  
				  surpicInteractionOnFlag = 2;
				  customPolygonInteractionFlag = false;
				  var overlay = map.getOverlays();
					var overlayToRemoveArr = [];					
					for (var m=0; m<overlay.getLength(); m++)
					{						
						var currentOverlayId = overlay.item(m).getId();
						
						if(currentOverlayId!= undefined )
						{							
							if( currentOverlayId.match("grid") || currentOverlayId.match("distance") )
							{
								overlayToRemoveArr.push(currentOverlayId);							
							}
							if(currentOverlayId.match("_popup"))
							{							
								var findShipKeyInd = currentOverlayId.indexOf("_popup");
					   			var findShipKey = (currentOverlayId.substring(0,findShipKeyInd)).trim();
								var clickedShipInfoClose = AllShipStatusInfo.get(findShipKey);	
					   			var currentShipStateClose =  'latest';
					   			if(clickedShipInfoClose == undefined)
								{		   						   
					   				clickedShipInfoClose = surpicShipInfoForDisplay.get(findShipKey);	
					   				currentShipStateClose = 'surpic';
								}
							    if(clickedShipInfoClose == undefined)
								{			 						 	
							    	clickedShipInfoClose = historyShipInfoForDisplay.get(findShipKey);
							    	currentShipStateClose = 'history';
								}
							    if(clickedShipInfoClose != undefined){
								
							    	clickedShipInfoClose.popupStatus=false;
							    	
								if(currentShipStateClose == 'latest')
									AllShipStatusInfo.set(findShipKey, clickedShipInfoClose); 
								else if(currentShipStateClose == 'surpic')
									surpicShipInfoForDisplay.set(findShipKey, clickedShipInfoClose);
								else if(currentShipStateClose == 'history')
									historyShipInfoForDisplay.set(findShipKey, clickedShipInfoClose);
							    }
							    
							    overlayToRemoveArr.push(currentOverlayId);	
						    }
							
							
						}
					}
					
					for (var n=0; n<overlayToRemoveArr.length; n++)
					{
						var overlayToRemove = map.getOverlayById(overlayToRemoveArr[n]);
						map.removeOverlay(overlayToRemove);
					}			
				 
					filterdShipData = shipData;
					$("#countryListForm")[0].reset();
					$("#LayerForm")[0].reset();					
					$("#CountryBoundaryListForm")[0].reset();
					$("#ShippingCompanyForm")[0].reset();
					$("#geographicalLayersForm")[0].reset();
					$("#pathProjectionForm")[0].reset();
					$("#DistanceBearingForm")[0].reset();
					$("#sarSurpicForm")[0].reset();
					//$("#searchShipPortForm")[0].reset();
					resetSearchShipPortForm();
					$("#showSatelliteOceanRegionModalForm")[0].reset();
					$("#filterVesselStatusModalForm")[0].reset();
					$("#sarAreaViewModalForm")[0].reset();
					$("#shipHistoryMainForm")[0].reset();
					//$("#customPolygonForm")[0].reset();					
					$("#gridListForm")[0].reset();
					
					
					
				 // if(document.getElementById("distanceTableId"))
				  //		document.getElementById("distanceTableId").value = '';
				//	$('#distanceTableId').hide();	
				  			 
			/*}
			catch(err)
			{
				console.info('error in removeAllLayersRefresh: ' + err)
			}*/
		}		
		
 
// ------Generate & remove Waterpolygon Layer----------------------------
	
	function removeOverlayByCondition()
	{
		  var overlay = map.getOverlays();
			var overlayToRemoveArr = [];					
			for (var m=0; m<overlay.getLength(); m++)
			{						
				var currentOverlayId = overlay.item(m).getId();
				
				if(currentOverlayId!= undefined )
				{							
					
					if(currentOverlayId.match("_popup"))
					{							
						var findShipKeyInd = currentOverlayId.indexOf("_popup");
			   			var findShipKey = (currentOverlayId.substring(0,findShipKeyInd)).trim();
			   			var flagVisibleOnMap = false;
			   			for (var n=0; n<filterdShipData.length; n++)
						{
			   				if(filterdShipData[n].message_id==findShipKey || filterdShipData[n].imo_no==findShipKey)
		   					{
			   					flagVisibleOnMap = true;
		   					}
							
						}
			   			if(flagVisibleOnMap==false)
			   			{
							var clickedShipInfoClose = AllShipStatusInfo.get(findShipKey);	
				   			var currentShipStateClose =  'latest';
				   			if(clickedShipInfoClose == undefined)
							{		   						   
				   				clickedShipInfoClose = surpicShipInfoForDisplay.get(findShipKey);	
				   				currentShipStateClose = 'surpic';
							}
						    if(clickedShipInfoClose == undefined)
							{			 						 	
						    	clickedShipInfoClose = historyShipInfoForDisplay.get(findShipKey);
						    	currentShipStateClose = 'history';
							}
						    if(clickedShipInfoClose != undefined){
							
						    	clickedShipInfoClose.popupStatus=false;
						    	
							if(currentShipStateClose == 'latest')
								AllShipStatusInfo.set(findShipKey, clickedShipInfoClose); 
							else if(currentShipStateClose == 'surpic')
								surpicShipInfoForDisplay.set(findShipKey, clickedShipInfoClose);
							else if(currentShipStateClose == 'history')
								historyShipInfoForDisplay.set(findShipKey, clickedShipInfoClose);
						    }
						    
						    overlayToRemoveArr.push(currentOverlayId);	
						    removeLayersFromMap('popupLineLayer'+currentOverlayId,'name','Line'+currentOverlayId);
			   			}
				    }
					
					
				}
			}
			
			for (var n=0; n<overlayToRemoveArr.length; n++)
			{
				var overlayToRemove = map.getOverlayById(overlayToRemoveArr[n]);
				map.removeOverlay(overlayToRemove);
			}			
		 
	}
		
		
	function removeLayersFromMap(layerName,featureProperty,featurePropertyValue)	
	{
		try{
			var layersToRemove = [];
			  map.getLayers().forEach(function (layer) {
			      if (layer.get('name') != undefined && (layer.get('name')).match(layerName)) {		    	 
			    	  layer.getSource().clear();
			    	  layersToRemove.push(layer);		         
			      }
			  });
	
			  var len = layersToRemove.length;
			  for(var i = 0; i < len; i++) {
				  
				  var features = layersToRemove[i].getSource().getFeatures();
				  	features.forEach(function(feature) {
				  
				    	if (feature.get(featureProperty) == featurePropertyValue)	      
						    {
				    		layersToRemove[i].getSource().removeFeature(feature);
				    	};
				    });
				
			      map.removeLayer(layersToRemove[i]);
			  }
		}
		catch(err)
		{
			console.info('error in removeLayersFromMap: ' + err)
		}
	}
		
	  function removeWaterPolygonLayer(typeName){	
		  try{
		  		removeLayersFromMap('vectorLayer_WaterPolygon','type',typeName);		  
		  }
		catch(err)
		{
			console.info('error in removeWaterPolygonLayer: ' + err)
		}
	  }
	  
	  
	  
		  
		  function styleFunctionWaterPolygonIW(feature){
			//console.info(map.getView().getZoom());
			 return [
				  new ol.style.Style({
				  stroke: new ol.style.Stroke({
		              color: 'red'
		            }),
		            text: new ol.style.Text({
				        font: '14px Calibri,sans-serif',
				        fill: new ol.style.Fill({ color: '#000' }),
				        stroke: new ol.style.Stroke({
				          color: '#fff', width: 4
				        }),
				        // get the text from the feature - `this` is ol.Feature
				        // and show only under certain resolution
				        //text: map.getView().getZoom() > 5 ? feature.get('areaId') : '',
				        text: feature.get('area_id'),
				       // offsetX: -20,
				       // offsetY: 20,
				        overflow: true
				      })
			     })
			 ];
		  }
	 
		  
		  function styleFunctionWaterPolygonTS(feature){
			//console.info(map.getView().getZoom());
			 return [
				  new ol.style.Style({
				  stroke: new ol.style.Stroke({
		              color: 'Orange'
		            }),
		            text: new ol.style.Text({
				        font: '14px Calibri,sans-serif',
				        fill: new ol.style.Fill({ color: '#000' }),
				        stroke: new ol.style.Stroke({
				          color: '#fff', width: 4
				        }),
				        // get the text from the feature - `this` is ol.Feature
				        // and show only under certain resolution
				        //text: map.getView().getZoom() > 5 ? feature.get('areaId') : '',
				        text: feature.get('area_id'),
				       // offsetX: -20,
				       // offsetY: 20,
				        overflow: true
				      })
			     })
			  ];
		  }

		  
		  function styleFunctionWaterPolygonCC(feature){
			//console.info(map.getView().getZoom());
			 return [
				  new ol.style.Style({
				  stroke: new ol.style.Stroke({
		              color: 'green'
		            }),
		            text: new ol.style.Text({
				        font: '14px Calibri,sans-serif',
				        fill: new ol.style.Fill({ color: '#000' }),
				        stroke: new ol.style.Stroke({
				          color: '#fff', width: 4
				        }),
				        // get the text from the feature - `this` is ol.Feature
				        // and show only under certain resolution
				        //text: map.getView().getZoom() > 5 ? feature.get('areaId') : '',
				        text: feature.get('area_id'),
				       // offsetX: -20,
				       // offsetY: 20,
				        overflow: true
				      })
				  })
			  ];
		  }
	
	  function styleFunctionWaterPolygonOT(feature){
			//console.info(map.getView().getZoom());
			 return [
			  new ol.style.Style({
			  stroke: new ol.style.Stroke({
	              color: 'magenta'
	            }),
	            text: new ol.style.Text({
			        font: '14px Calibri,sans-serif',
			        fill: new ol.style.Fill({ color: '#000' }),
			        stroke: new ol.style.Stroke({
			          color: '#fff', width: 4
			        }),
			        // get the text from the feature - `this` is ol.Feature
			        // and show only under certain resolution
			        //text: map.getView().getZoom() > 5 ? feature.get('areaId') : '',
			        text: feature.get('area_id'),
			       // offsetX: -20,
			       // offsetY: 20,
			        overflow: true
			      })
		     })
		  ];
		 }
	  
	  function styleFunctionWaterPolygonCS(feature){
			//console.info(map.getView().getZoom());
			 return [ 
				 new ol.style.Style({
				  stroke: new ol.style.Stroke({
		              color: 'Black'
		            }),
		            text: new ol.style.Text({
				        font: '14px Calibri,sans-serif',
				        fill: new ol.style.Fill({ color: '#000' }),
				        stroke: new ol.style.Stroke({
				          color: '#fff', width: 4
				        }),
				        // get the text from the feature - `this` is ol.Feature
				        // and show only under certain resolution
				        //text: map.getView().getZoom() > 5 ? feature.get('areaId') : '',
				        text: feature.get('area_id'),
				       // offsetX: -20,
				       // offsetY: 20,
				        overflow: true
				      })
			     })
			 ];

		}
	     
	  
	  var IW_Style = new ol.style.Style({
		  fill: new ol.style.Fill({
			    color: 'rgba(255,0,0,0)'
			  }),
		  stroke: new ol.style.Stroke({
              color: 'red'
            })
	     });
	  var TS_Style = new ol.style.Style({
		  fill: new ol.style.Fill({
			    color:'rgba(255, 69, 0, 0)'
			  }),
		  stroke: new ol.style.Stroke({
              color: 'Orange'
            })
	     });
	  var CC_Style = new ol.style.Style({
		  fill: new ol.style.Fill({
			    color:'rgba(0, 255, 0, 0)'
			  }),
		  stroke: new ol.style.Stroke({
              color: 'green'
            })
	     });
	  var OT_Style = new ol.style.Style({
		  fill: new ol.style.Fill({
			    color:'rgba(0, 0, 255, 0)'
			  }),
		  stroke: new ol.style.Stroke({
              color: 'magenta'
            })
	     });
	  
	  var CS_Style = new ol.style.Style({
		  fill: new ol.style.Fill({
			    color:'rgba(255, 255, 0, 0)'
			  }),
		  stroke: new ol.style.Stroke({
              color: 'Black'
            })
	     });
	  
	  
	  
	  function generateWaterPolygonLayer(response)
	  {		
		  try{
			 var vectorWaterPolygonSource = new ol.source.Vector(
			 {              
				 wrapX: false,
		    	 noWrap: true
			 });
			  
			  var vectorLayer_WaterPolygon = new ol.layer.Vector({
			        source: vectorWaterPolygonSource,   
			        name: "vectorLayer_WaterPolygon",		       
		      });
	
			  
			  var jsondata = response;
			  for(var i =0; i<jsondata.length; i++)
		    	 {			  
					  var features = new ol.Feature({
						  name: "waterPOLYGON" + i,
						  geometry: new ol.geom.Polygon(jsondata[i].poslist.coordinates),
						  ddpversion_no: jsondata[i].ddpversionNo,
						  area_id: jsondata[i].area_id,
						  cg_lritid: jsondata[i].cg_lritid,
						  type: jsondata[i].type
					  });
							
					 if(features.get('type') == 'CustomCoastal')
						// features.setStyle(styleFunctionWaterPolygonCC(features));
					      features.setStyle(CC_Style);	
					  else if(features.get('type') == 'InternalWaters')
						  ///features.setStyle(styleFunctionWaterPolygonIW(features));
					      features.setStyle(IW_Style);	
					  else if(features.get('type') == 'CoastalSea')
						//  features.setStyle(styleFunctionWaterPolygonCS(features));
				    	  features.setStyle(CS_Style);	
					  else if(features.get('type') == 'TerritorialSea')
						  //features.setStyle(styleFunctionWaterPolygonTS(features));
				    	  features.setStyle(TS_Style);	
					  else if(features.get('type') == 'OpenSo')
						  //features.setStyle(styleFunctionWaterPolygonOT(features));
				    	  features.setStyle(OT_Style);
					  
					  vectorLayer_WaterPolygon.getSource().addFeature(features.clone());
					 		      	
		    	 }	     
			
			  map.addLayer(vectorLayer_WaterPolygon);	
		   }
			catch(err)
			{
				console.info('error in generate WaterPolygon Layer: ' + err)
			}
	  }
	  
	
	  function getAvailableWaterpolygonCountryWise()
		 {
		  
			$.ajax({
			    type: "POST",
			    traditional: true,
			    url: "/lrit/map/WaterPolygons",
			    data: {},
			    success:function(response) {	
			    	try{
		    		jsondata = response;
					 for(i =0; i<jsondata.length; i++)
					 {
						  var features = new ol.Feature({						   
						    	 cg_lritid: jsondata[i].cglritID,
						    	 type: jsondata[i].type					    
						  });			
					 }
			    	}
					catch(err)
					{
						console.info('error in getting available water polygon country wise: ' + err)
					}
				 }
			});
		  
		 }
		    
//-----End of Water Polygon ---------------------------------------------
	  
// -----------Generate Selected countries Ship Position Layer------------------
	  
	  
	  var vectorSelectedCountryShipSource = new ol.source.Vector(
	  {              
		 wrapX: false,
		 noWrap: true
	  });
				
	  var vectorLayer_SelectedCountryShipPosition = new ol.layer.Vector({
		  source: vectorSelectedCountryShipSource,   
		  name: "SelectedCountryShipLayer"
	  });	
	  map.addLayer(vectorLayer_SelectedCountryShipPosition);
	     function generateSelectedShipLayer(selectedCountries)
	     {
	    	 try{
		    	// to remove previous Ship Layer
		    	 vectorLayer_SelectedCountryShipPosition.getSource().clear();
		    	 var currentShipData = [];
		    	 vectorSource_Temp.clear();	    	 
		    	hideAllShipLayer();
		    	
		    	 for(var i =0; i<shipData.length; i++)
			     {	
		    		 for (var j=0; j<selectedCountries.length; j++)
	    			 {
	    			 	if ( shipData[i].data_user_provider == selectedCountries[j])
				 		{
	    			 		var features = new ol.Feature({
	    				    	  name: "POINT" + i,
	    				    	  geometry: new ol.geom.Point([shipData[i].longitude, shipData[i].latitude] ),	
	    				    	  message_id: shipData[i].message_id,
	    				    	  ship_borne_equipment_id: shipData[i].ship_borne_equipment_id,
	    				    	  asp_id: shipData[i].asp_id,
	    				    	  imo_no: shipData[i].imo_no,
	    				    	  mmsi_no: shipData[i].mmsi_no,
	    				    	  dc_id: shipData[i].dc_id,
	    				    	  timestamp4: shipData[i].timestamp4,
	    				    	  dataUserRequestor: shipData[i].data_user_requestor,
	    				    	  ship_name: shipData[i].ship_name,
	    				    	  dataUserProvider: shipData[i].data_user_provider,
	    				    	  ddpversion_no: shipData[i].ddpversion_no,
	    				    	  type: 'SelectedCountryShip'
	    				      });
	    			 		currentShipData.push(shipData[i]);
		    			      //if(features.get('dataUserRequestor') == flagCountry)		    	  
		    			    //	  features.setStyle(foreignShipPositionStyle);	
		    			    //  else if(features.get('dataUserProvider') == flagCountry)
		    			    	  features.setStyle(flagShipPositionStyle);		  
		    			    
		    			      vectorLayer_SelectedCountryShipPosition.getSource().addFeature(features.clone());	   
		    			      vectorSource_Temp.addFeature(features.clone());
				 		}
	    			 }
			     }	
		    	 filterdShipData = [];
		    	 filterdShipData = currentShipData;
		    	 
		    	 removeOverlayByCondition();
		    	 var extent = vectorSource_Temp.getExtent();		    	
		    	 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
				 { 
		    		 var diffExtent = extent[2]- extent[0];
		    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
		    		 {
		    			 var center = ol.extent.getCenter(extent);							
		    			 map.getView().setCenter(center);
		    		 }
		    		 else
		    			 map.getView().fit(extent, map.getSize());	    		
				 }	    	 
	    	 }
			catch(err)
			{
				console.info('error in generating selcted country ship layer: ' + err)
			}
	     }
	     
	     
	     
// -----end of Generate Selected Ship Position Layer----------------------------------	     
	     
// --------Generate Selected company Ship Position Layer------------------------------
	     var vectorSelectedCompanyShipSource = new ol.source.Vector(
		  {              
			 wrapX: false,
			 noWrap: true
		  });
					
		  var vectorLayer_SelectedCompanyShipPosition = new ol.layer.Vector({
			  source: vectorSelectedCompanyShipSource,   
			  name: "SelectedCompanyShipLayer"
		  });	
		  map.addLayer(vectorLayer_SelectedCompanyShipPosition);		  
	     function generateSelectedShipCompnyLayer(selectedCompany)
	     {
	    	 try{
		    	// to remove previous Ship Layer
		    	vectorLayer_SelectedCompanyShipPosition.getSource().clear();
		    	hideAllShipLayer(); 
		    	vectorSource_Temp.clear();
		    	var currentShipData = [];
	    		 for (var k=0; k<selectedCompany.length; k++)
	    			 {
		    			 for (var j=0; j<selectedCompany[k].length; j++)
		    			 {		    				 
		    				 var shipDataTemp = AllShipStatusInfo.get(selectedCompany[k][j].trim());
		    				 if(shipDataTemp!=undefined)
		    				 { 
		    					 var features = new ol.Feature({
		    			    	  name: "POINT" + k,
		    			    	  geometry: new ol.geom.Point([shipDataTemp.longitude, shipDataTemp.latitude] ),	
		    			    	  message_id: shipDataTemp.message_id,
		    			    	  ship_borne_equipment_id: shipDataTemp.ship_borne_equipment_id,
		    			    	  asp_id: shipDataTemp.asp_id,
		    			    	  imo_no: shipDataTemp.imo_no,
		    			    	  mmsi_no: shipDataTemp.mmsi_no,
		    			    	  dc_id: shipDataTemp.dc_id,
		    			    	  timestamp4: shipDataTemp.timestamp4,
		    			    	  dataUserRequestor: shipDataTemp.data_user_requestor,
		    			    	  ship_name: shipDataTemp.ship_name,
		    			    	  dataUserProvider: shipDataTemp.data_user_provider,
		    			    	  ddpversion_no: shipDataTemp.ddpversion_no,
		    			    	  course:shipDataTemp.course,
		    			    	  speed:shipDataTemp.speed,
		    			    	  type:'SelectedCompanyShip'
		    					 });		//getShipInfoFromloadedShipData(selectedCompany[k][j]);						
						    	 features.setStyle(countryShipPositionStyle);		  
						    	 vectorLayer_SelectedCompanyShipPosition.getSource().addFeature(features.clone());
						    	 vectorSource_Temp.addFeature(features.clone());
						    	 
						    	 currentShipData.push(shipDataTemp);
		    				 }
		    				 
		    				 
		    			 }
	    			 }
	    		 

	    		 filterdShipData = [];
	    		 filterdShipData = currentShipData;
	    		 removeOverlayByCondition();
		    	 var extent = vectorSource_Temp.getExtent();		    	
		    	 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
				 { 
		    		 var diffExtent = extent[2]- extent[0];
		    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
		    		 {
		    			 var center = ol.extent.getCenter(extent);							
		    			 map.getView().setCenter(center);
		    		 }
		    		 else
		    			 map.getView().fit(extent, map.getSize());	    		
				 }
	    	 }
	 		catch(err)
	 		{
	 			console.info('error in generating selcted company ship layer: ' + err)
	 		}
	     }	     
// -----end of Generate Selected company Ship Position Layer----------------------------
	     	  
// --------Generate Ship in Selected Country boundary Layer----------------------------
	     
	     // var selectedFlag = 1065;
	     var countryShipPositionStyle = new ol.style.Style({
	    	 image: new ol.style.Circle({		             
	    		 radius: 3,
	    		 stroke: new ol.style.Stroke({
	    	     color: 'red',
	    	     width: 1
	    		 }),
	    		 fill: new ol.style.Fill({
	    	      color: 'red'
	    	          })
	    	 })
	     });		
	     
	     var vectorSelectedCountryBoundaryShipSource = new ol.source.Vector(
	   		  {              
	   			 wrapX: false,
	   			 noWrap: true
	   		  });
	   					
	   		  var vectorLayer_SelectedCountryBoundaryShipPosition = new ol.layer.Vector({
	   			  source: vectorSelectedCountryBoundaryShipSource,   
	   			  name: "countryBoundrayLayer"
	   		  });		
	   	  map.addLayer(vectorLayer_SelectedCountryBoundaryShipPosition);	
	     function generateCountryBoundaryLayer(selectedFlag)
	     { 	
	    	 try{	    		
		    	 vectorLayer_SelectedCountryBoundaryShipPosition.getSource().clear();	    	 
		    	 hideAllShipLayer();
		    	 
		    	 $.ajax({
		 		    type: "POST",
		 		    traditional: true,
		 		    url: "/lrit/map/SO",
		 		    data: { 'cg_lritid': selectedFlag },
		 		    success:function(response) {	
		 		    	try{
		 		    	vectorSource_Temp.clear();
		 		    	jsondata = response;
		 		    	
		 		    	
				 		var SoPolygonStyle = new ol.style.Style({
				 			  stroke: new ol.style.Stroke({
				              color: 'blue',
				              width: 1
				             })
				 	     });
				 		var polygonCoordinates=[[]];
				 		  for(var i =0; i<jsondata.length; i++)
			 			  {
				 			 for(var j =0; j<jsondata[i].length; j++)
					 		  {
					 			  var features = new ol.Feature({
					 				  name: "POLYGON" + i,
					 				  geometry: new ol.geom.Polygon(jsondata[i][j].poslist.coordinates),	   
					 				  ddpversion_no: jsondata[i][j].ddpversion_no,
					 				  area_id: jsondata[i][j].area_id,
					 				  cg_lritid: jsondata[i][j].cg_lritid,
					 				  type: "CountryBoundary" // jsondata[i].type
					 			  });			 	
					 			 features.setStyle(SoPolygonStyle);   
					 			 vectorLayer_SelectedCountryBoundaryShipPosition.getSource().addFeature(features.clone()); 
					 			 //vectorSource_Temp.addFeature(features.clone());
					 			 polygonCoordinates[i] = jsondata[i][j].poslist.coordinates;
					 			
					 		 }		 			
			 			  }
		 		  
				 		 var shipCoordinate=[[]];
				 		
			 			 for(var i =0; i<shipData.length; i++)
					     {
						 	shipCoordinate[i] = [shipData[i].longitude, shipData[i].latitude];
					     } 			
			 		 			
			 			var points = turf.points(shipCoordinate); 			
			 			var searchWithin = turf.multiPolygon(polygonCoordinates); 			
			 			var ptsWithin = turf.pointsWithinPolygon(points, searchWithin); 			
					    var turfFeature = ptsWithin.features;		    
					    var findvalueX = 0;
					    var findvalueY = 0;
					    
				     	function findShipData(shipDataTofind) {
				     		var coord = [shipDataTofind.longitude ,shipDataTofind.latitude ];		     		
				     		return coord[0] ==findvalueX && coord[1]==findvalueY;
				    	}	
				     	
				     	var currentShipData = [];
					     for(var j =0; j<turfFeature.length; j++)
					     {		   
					    	 var geom = turf.getGeom(turfFeature[j]);
					    	 var shipInBoundaryCoordinates = turf.getCoords(geom)
					    	 findvalueX = shipInBoundaryCoordinates[0];
					    	 findvalueY = shipInBoundaryCoordinates[1];
					    	 
					    	 var foundIndex = shipData.findIndex(findShipData);		    	 
						     var features = new ol.Feature({
						    	  name: "POINT" + j,
						    	  geometry: new ol.geom.Point(shipInBoundaryCoordinates),	
						    	  message_id: shipData[foundIndex].message_id,
						    	  ship_borne_equipment_id: shipData[foundIndex].ship_borne_equipment_id,
						    	  asp_id: shipData[foundIndex].asp_id,
						    	  imo_no: shipData[foundIndex].imo_no,
						    	  mmsi_no: shipData[foundIndex].mmsi_no,
						    	  dc_id: shipData[foundIndex].dc_id,
						    	  timestamp4: shipData[foundIndex].timestamp4,
						    	  dataUserRequestor: shipData[foundIndex].data_user_requestor,
						    	  ship_name: shipData[foundIndex].ship_name,
						    	  dataUserProvider: shipData[foundIndex].data_user_provider,
						    	  ddpversion_no: shipData[foundIndex].ddpversion_no
						      });
					     
					      features.setStyle(countryShipPositionStyle); 
					      vectorLayer_SelectedCountryBoundaryShipPosition.getSource().addFeature(features.clone());		 
					      vectorSource_Temp.addFeature(features.clone());	
					      currentShipData.push(filterdShipData[foundIndex]);
				    	 }	   
					     
					     filterdShipData = [];
				    	 filterdShipData = currentShipData;	   
				    	 removeOverlayByCondition();
				    	 var extent = vectorSource_Temp.getExtent();	
				    	 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
						 { 
				    		 var diffExtent = extent[2]- extent[0];
				    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
				    		 {
				    			 var center = ol.extent.getCenter(extent);							
				    			 map.getView().setCenter(center);
				    		 }
				    		 else
				    			 map.getView().fit(extent, map.getSize());	    		
						 }
		 		    	}
		 		 		catch(err)
		 		 		{
		 		 			console.info('error in generating selcted country boundries ship layer: ' + err)
		 		 		}
		 		    }	 		    
		 	  });
	    	 }
	 		catch(err)
	 		{
	 			console.info('error in generating selcted country boundries ship layer: ' + err)
	 		}
	    	 
	     }
	     
// ---------End of Generate Ship in Selected Country boundary Layer---------------
	

	
 // Black Point for ship history and path projection display		
 	 var styleBlack = new ol.style.Style({
 		 image: new ol.style.Circle({		             
 			 radius: 3,
 			 stroke: new ol.style.Stroke({
 		     color: 'black',
 		     width: 1
 			 }),
 			 fill: new ol.style.Fill({
 		      color: 'black'
 		          })
 		 })
 	 });
 	var styleStartPointHistory = new ol.style.Style({
		 image: new ol.style.Circle({		             
			 radius: 7,
			 stroke: new ol.style.Stroke({
		     color: 'black',
		     width: 2
			 }),
			 /*fill: new ol.style.Fill({
		      color: 'black'
		          })*/
		 })
	 });
			
		
// ------------ Display Ship History ----------------------------------------------
		
	function showShipHistory(){	  	   
		try{
		var imoNo = (document.getElementById("imoNumber").value).split(',');
		vectorSource_Temp.clear();
		historyShipInfoForDisplay.clear();
		$.each( imoNo, function( key, imoNoValue )  {
			var positionCount= 4;
	  	    var coordinates= [[]];
	  	    var features = new Array();
	  	    var transformedCoordinates = new Array();
	  	    var layerName = "showHistoryLayer" + imoNo;
		  	var featureType = "showHistoryFeature";  
		  	hideHistory(layerName);
		  	
		  	$.ajax({
			    type: "POST",
			    traditional: true,
			    url: "/lrit/map/ShipHistory",
			    data: { 'imoNo': imoNoValue, 'startDate' : startDate, 'endDate': endDate},
			    success:function(response) {	
		    	try{
			    	jsondata = response;		    		
			    	if(jsondata.length<1)
				  		alert("No Ship History Found.");
			    	else
			    	{
			    		 
			    		var shipDataTemp = AllShipStatusInfo.get(jsondata[0].imo_no.trim());	
						if(shipDataTemp==undefined && (companyUser!="none"))
						{
							jsondata = [];
						}
						if(jsondata.length<1)
					  		alert("No Ship History Found.");
						else{
							historyShownFlag = true;
						  for(var i =0; i<jsondata.length; i++)
						  {								  
							  coordinates[i] = [jsondata[i].longitude, jsondata[i].latitude];
							  transformedCoordinates[i] = ol.proj.transform(coordinates[i] , 'EPSG:4326', 'EPSG:3857');	
						  }
						
						  historyLayer = lineArrowFunction(transformedCoordinates,coordinates,layerName,featureType); 
					  		for (var i = 0; i < jsondata.length; ++i) {		
					  			jsondata[i].popupStatus = false;
					  			jsondata[i].speed = 'NA';
					  			jsondata[i].course  = 'NA';
					  			historyShipInfoForDisplay.set((jsondata[i].message_id).trim(),jsondata[i]);
							  	features[i] = new ol.Feature({
							    	  name: "POINT" + i,
							    	  geometry: new ol.geom.Point([jsondata[i].longitude, jsondata[i].latitude] ),	
							    	  message_id: jsondata[i].message_id,
							    	  ship_borne_equipment_id: jsondata[i].ship_borne_equipment_id,
							    	  asp_id: jsondata[i].asp_id,
							    	  imo_no: jsondata[i].imo_no,
							    	  mmsi_no: jsondata[i].mmsi_no,
							    	  dc_id: jsondata[i].dc_id,
							    	  timestamp4: jsondata[i].timestamp4,
							    	  dataUserRequestor: jsondata[i].data_user_requestor,
							    	  ship_name: jsondata[i].ship_name,
							    	  dataUserProvider: jsondata[i].data_user_provider,
							    	  ddpversion_no: jsondata[i].ddpversion_no,
							    	  course:jsondata[i].course,						    	 
							      });
						    
							  //	features[i].getGeometry().transform('EPSG:4326', 'EPSG:3857');
							  	features[i].setStyle(styleBlack);
							  	if(i===0)							  									  				
							  		features[i].setStyle(styleStartPointHistory);
							  			
							  		historyLayer.getSource().addFeature(features[i]);
				  				}
					  			map.addLayer(historyLayer);
					  			 var extent = vectorSource_Temp.getExtent();					  			
					  			if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
								 { 
						    		 var diffExtent = extent[2]- extent[0];
						    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
						    		 {
						    			 var center = ol.extent.getCenter(extent);							
						    			 map.getView().setCenter(center);
						    		 }
						    		 else
						    			 map.getView().fit(extent, map.getSize());	    		
								 }
							}
			    	} 
			    	
			    }
				catch(err)
				{
					console.info('error in generating ship history: ' + err)
				}
			    }
			  }); 
		  	
		});
		 
		}
		catch(err)
		{
			console.info('error in generating ship history: ' + err)
		}
	}
	
// ------------ End of Display Ship History ----------------------------------------------	
	
// ------------ Hide Ship History----------------------------------------------
	function hideHistory(layerName){	
		 try{
			  var featureTypeName;		 
	
			  if(layerName.match('showHistoryLayer'))
			  {			  
				  	featureTypeName = 'showHistoryFeature';			  	
				  	removeLayersFromMap(layerName,'type',featureTypeName)			  
			  }
			  if (layerName.match('pathProjectionLayer'))
			  {
				  featureTypeName = 'pathProjectionFeature';
				  removeLayersFromMap(layerName,'type',featureTypeName)
				//  map.addLayer(vectorLayer_pathProjection);
			  }		
		 }
		catch(err)
		{
			console.info('error in hiding ship history: ' + err)
		}
	}
	
// ------------ End of Hide Ship History----------------------------------------------	

//--------------- Path Projection Starts--------------------------------------------

	function stopPathProjection()
	{
		try{
			if(pathProjectionStartFlag==true)
			{
				pathProjectionStartFlag = false;
				map.removeInteraction(draw);
				 $('#ringDistanceInfo').hide();
				flagCircleInteraction =false;
				
				$('#pathProjectionButton').toggleClass("down");
				
			}
		}
		catch(err)
		{
			console.info('error in stop path projection: ' + err)
		}
		
	}
	
	function showPathProjection(lat1, lon1, brng, dist)
	{	try{
			if(lat1 != null && lon1  != null && brng != null)
			{	
				var point = destVincenty(lat1, lon1, brng, dist);
			 	var coordinates= [[]];
			    var features;
			    var pathProjImo = document.getElementById("PPimoNoVal").value;
			  	var layerName = "pathProjectionLayer" + pathProjImo;	  
			  	hideHistory("pathProjectionLayer" + pathProjImo)
			  	var featureType = "pathProjectionFeature";  	  	
			  	  var transformedCoordinates = new Array();
		 	    
					coordinates[0] = [lon1, lat1];
					coordinates[1] = [point.lng, point.lat];						
		
			  	for (var i = 0; i < coordinates.length; ++i) {	  		
			  	    transformedCoordinates[i] = ol.proj.transform(coordinates[i] , 'EPSG:4326', 'EPSG:3857'); 	        
			  	}	  	
			  	
			  	var shipDataTemp = AllShipStatusInfo.get(pathProjImo.trim());
			  	features =new ol.Feature({
			    	  name: "POINT" + i,
			    	  geometry: new ol.geom.Point([shipDataTemp.longitude, shipDataTemp.latitude] ),	
			    	  message_id: shipDataTemp.message_id,
			    	  ship_borne_equipment_id: shipDataTemp.ship_borne_equipment_id,
			    	  asp_id: shipDataTemp.asp_id,
			    	  imo_no: shipDataTemp.imo_no,
			    	  mmsi_no: shipDataTemp.mmsi_no,
			    	  dc_id: shipDataTemp.dc_id,
			    	  timestamp4: shipDataTemp.timestamp4,
			    	  dataUserRequestor: shipDataTemp.data_user_requestor,
			    	  ship_name: shipDataTemp.ship_name,
			    	  dataUserProvider: shipDataTemp.data_user_provider,
			    	  ddpversion_no: shipDataTemp.ddpversion_no,
			    	  course:shipDataTemp.course,
			    	  speed:shipDataTemp.speed
			      });		//getShipInfoFromloadedShipData(document.getElementById("PPimoNoVal").value);	
			  	features.setStyle(styleBlack);	
			  	
			  	var pathProjectionLayer = lineArrowFunction(transformedCoordinates,coordinates,layerName,featureType);  
			  //	console.info('pathProjectionLayer: ' + pathProjectionLayer);
			  	pathProjectionLayer.getSource().addFeature(features);
			  	map.addLayer(pathProjectionLayer); 	  	
			  	pathProjectionFlag = true;
			}
			if(lat1 == null || lon1  == null || brng == null)
			{	
				alert('Not enough information for Predicting Path')
			}
		}
		catch(err)
		{
			console.info('error in show path projection: ' + err)
		}
	}
	
	
	function destVincenty(lat1, lon1, brng, dist) {
		try{
			 var a = 6378137;
			 var b = 6356752.3142;
			 var f = 1 / 298.257223563; // WGS-84 ellipsiod
		     var s = dist;
		     var alpha1 = brng*Math.PI/180;
		     var sinAlpha1 = Math.sin(alpha1);
		     var cosAlpha1 = Math.cos(alpha1);
		     var tanU1 = (1 - f) * Math.tan(lat1*Math.PI/180);
		     var cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1));
		     var sinU1 = tanU1 * cosU1;
		     var sigma1 = Math.atan2(tanU1, cosAlpha1);
		     var sinAlpha = cosU1 * sinAlpha1;
		     var cosSqAlpha = 1 - sinAlpha * sinAlpha;
		     var uSq = cosSqAlpha * (a * a - b * b) / (b * b);
		     var A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
		     var B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
		     var sigma = s / (b * A);
		     var sigmaP = 2 * Math.PI;
		     var sinSigma;
		     var cosSigma;
		     var cos2SigmaM;
			     
			 while (Math.abs(sigma - sigmaP) > 1e-12) {
				 var cos2SigmaM = Math.cos(2*sigma1 + sigma);
				 sinSigma = Math.sin(sigma);
				 cosSigma = Math.cos(sigma);
			     var deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
			     sigmaP = sigma;
			     sigma = s / (b * A) + deltaSigma;
			 };
			 var tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1;
			    var lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1, (1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp));
			    var lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1);
			    var C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
			     var L = lambda - (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
			     var revAz = Math.atan2(sinAlpha, -tmp); // final bearing
			 return {
			    lat: lat2*180/Math.PI,
			    lng: lon1 + L*180/Math.PI,
			  };
		}
		catch(err)
		{
			console.info('error in destVincenty: ' + err)
		}
		     
		};
		
//--------------- End of Path Projection Starts--------------------------------------------
	
//--------------- Path Projection Line Styling --------------------------------------------	
				
		function lineArrowFunction(coordinates,transformedCoordinates,layerName,featureType)
		{
			try{
				var vectorLine = new ol.source.Vector({ wrapX: false,
		        	 noWrap: true});
				var startPointArrow;
				var endPointArrow;
				for (var i = 0; i < transformedCoordinates.length-1; i++) {
	
				     var startPoint = transformedCoordinates[i];
				     var endPoint = transformedCoordinates[i+1];
				    if(startPoint[0]!=endPoint[0] || startPoint[1]!=endPoint[1])
				{
				    var lineArray = [startPoint, endPoint];
				    var featureLine = new ol.Feature({
				        geometry: new ol.geom.LineString(lineArray)
				    });
	
				    var lineStyle = new ol.style.Style({
				        fill: new ol.style.Fill({
				            color: '#00FF00',
				            weight: 4
				        }),
				        stroke: new ol.style.Stroke({
				            color: '#000000',
				            width: 2
				        })
				    });
				    featureLine.setStyle(lineStyle);
				    vectorLine.addFeature(featureLine);
				  
				  	startPointArrow =startPoint;
				  	endPointArrow = endPoint;
				    
				    var start = startPoint; //[18900936.34178992, 17483855.023378];
				  	var end = endPoint;//[14782115.182438798, 2829141.871457948];	
				  	
				    var dx = end[0] - start[0];
				    var dy = end[1] - start[1];
				    var rotation = Math.atan2(dy, dx)
				    var iconStyle = new ol.style.Style({
				        image: new ol.style.Icon(({				           
				            color: 'green',
				            src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAAdpJREFUOBGFVDtOA0EMnewGCS7ACeAYUIISEtpAxRGgRaLlFijiFkCAlgqJDokT0CAqJD7ZxLznsScT2GR35IzXnzdvbG9CWPZIKOhuS3u3lLKroWZbllbvyxIB9gB5TIGZL9kaFQltxoDdDsB8dTTPfI0YKUBCy3VA3SQ4Ke/cHrKYZFuoSFihD0AdBZtmv1L2NM9iFmIkR3YyYEYKJeUYO4XrPovVpqX3WmXGbs8ACDIx8Vrua24jy6x7APDa/UDnpSnUufJaLmFp3UNCzq5KcFJWBkjQvrHUafh/23p23wbgDAnktgaWM3bdjAVr52C+T9QSr+4d/8NyvrO3Buj1ciDfCeW+nGWa3YAh9bnrNbBzUDL35SwVowBYge9ibEU9sb1Se3wRbBMT6iTAzlaqhxBziKH2Gbt+OjN2kx3lMJOVL+q00Zd3PLHM2R3biV/KAV8edha7JUGeKNTNRh/ZfkL4xFy/KU7z2uW1oc4GHSJ1DbIK/QAyguTsfBLi/yXhEXAN8fWOD22Iv61t+uoe+LYQfQF5S1lSXmksDAMaCyleIGdgsjkHwhqz2FG0k8kvYQM5p5BnAx608HKOgNdpmF6iQh8aHOeS9atgi511lDofSlKE4ggh679ecGIXq+UAsgAAAABJRU5ErkJggg==',
				        anchor: [0.75-0.25, 0.5-0.3],
				        rotateWithView: false,
				        rotation: -rotation + Math.PI/2				          
				        }))
				    });
				    var iconFeature = new ol.Feature({
				        geometry: new ol.geom.Point(endPoint)
				    });
				  
				    	iconFeature.setStyle(iconStyle);
				    	vectorLine.addFeature(iconFeature);
				    	vectorSource_Temp.addFeature(iconFeature);
				   
				}
				}
				
				/*if(startPointArrow[0]!=endPointArrow[0] || startPointArrow[1]!=endPointArrow[1])
				{
					 var start = startPointArrow; //[18900936.34178992, 17483855.023378];
					  var	end = endPointArrow;//[14782115.182438798, 2829141.871457948];				 	 
					    var dx = end[0] - start[0];
					    var dy = end[1] - start[1];
					    var rotation = Math.atan2(dy, dx)
					    var iconStyle = new ol.style.Style({
					        image: new ol.style.Icon(({				           
					            color: 'green',
					            src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAAdpJREFUOBGFVDtOA0EMnewGCS7ACeAYUIISEtpAxRGgRaLlFijiFkCAlgqJDokT0CAqJD7ZxLznsScT2GR35IzXnzdvbG9CWPZIKOhuS3u3lLKroWZbllbvyxIB9gB5TIGZL9kaFQltxoDdDsB8dTTPfI0YKUBCy3VA3SQ4Ke/cHrKYZFuoSFihD0AdBZtmv1L2NM9iFmIkR3YyYEYKJeUYO4XrPovVpqX3WmXGbs8ACDIx8Vrua24jy6x7APDa/UDnpSnUufJaLmFp3UNCzq5KcFJWBkjQvrHUafh/23p23wbgDAnktgaWM3bdjAVr52C+T9QSr+4d/8NyvrO3Buj1ciDfCeW+nGWa3YAh9bnrNbBzUDL35SwVowBYge9ibEU9sb1Se3wRbBMT6iTAzlaqhxBziKH2Gbt+OjN2kx3lMJOVL+q00Zd3PLHM2R3biV/KAV8edha7JUGeKNTNRh/ZfkL4xFy/KU7z2uW1oc4GHSJ1DbIK/QAyguTsfBLi/yXhEXAN8fWOD22Iv61t+uoe+LYQfQF5S1lSXmksDAMaCyleIGdgsjkHwhqz2FG0k8kvYQM5p5BnAx608HKOgNdpmF6iQh8aHOeS9atgi511lDofSlKE4ggh679ecGIXq+UAsgAAAABJRU5ErkJggg==',
					        anchor: [0.75-0.25, 0.5-0.3],
					        rotateWithView: false,
					        rotation: -rotation + Math.PI/2				          
					        }))
					    });
					    var iconFeature = new ol.Feature({
					        geometry: new ol.geom.Point(endPoint)
					    });
					iconFeature.setStyle(iconStyle);
					vectorLine.addFeature(iconFeature);
				}*/
				var vectorLayer = new ol.layer.Vector({
				    source: vectorLine,
				    name: layerName,
				   
				});
				vectorLayer.setZIndex(1);
				//map.addLayer(vectorLayer);
				return(vectorLayer);
			}
			catch(err)
			{
				console.info('error in creating line and arrow: ' + err)
			}
		}
			
//---------------End of  Path Projection Line Styling --------------------------------------------			
	  
    
 //------------Grid Function-----------------------------------------------------------     
	 function removeGridLayer()
	 {	 
		 try{
		 removeLayersFromMap('gridLayer','name','POLYGON')	
		 
		  var overlay = map.getOverlays();
			var overlayToRemoveArr = [];					
			for (var m=0; m<overlay.getLength(); m++)
			{						
				var currentOverlayId = overlay.item(m).getId();
				
				if(currentOverlayId!= undefined )
				{							
					if( currentOverlayId.match("grid"))
					{
						overlayToRemoveArr.push(currentOverlayId);							
					}
				}
			}
			
			for (var n=0; n<overlayToRemoveArr.length; n++)
			{
				var overlayToRemove = map.getOverlayById(overlayToRemoveArr[n]);
				map.removeOverlay(overlayToRemove);
			}			

			
		// map.getOverlays().clear();
		 }
		 catch(err)
			{
				console.info('error remove grid layer: ' + err)
			}
	 }
 	  
 	var vectorGridSource = new ol.source.Vector(
	{  
		wrapX: false,
		noWrap: true
	});
	  
	var vectorGridLayer = new ol.layer.Vector({
        	source: vectorGridSource,  
        	name: 'gridLayer'
               
      });
	
	var gridStyle = new ol.style.Style({
		  stroke: new ol.style.Stroke({
            color: 'blue'
      })
   });
 	var gridNo = 0;  
	function displayGrid()
	{	 
		try{
			optionGridValue = document.getElementsByName('gridOptionsRadios');		 
			 for(i = 0; i < optionGridValue.length; i++) { 
		         if(optionGridValue[i].checked) 
		    	 {
		       	  gridNo = optionGridValue[i].value;
		    	 }
			 }	
			
			if (gridNo ==0)
				removeGridLayer();
			
			else{
				
				removeGridLayer();			
				vectorSource_Temp.clear();		
				 var shipCoordinate=[[]];	 
				
				 for(var i =0; i<filterdShipData.length; i++)
			     {
					 	shipCoordinate[i] = [filterdShipData[i].longitude, filterdShipData[i].latitude];
			     }
					 
				 			
				var points = turf.points(shipCoordinate);
				var squareGrid = createGrid(gridNo);				
				
				
			  for (var i = 0; i < squareGrid.length; i++) {
				  var featureGrid =squareGrid[i];
				  var geom = turf.getGeom(featureGrid);
				  var gridCoordinates = turf.getCoords(geom);	      
			      			
				  var ptsWithin = turf.pointsWithinPolygon(points, featureGrid); 			
				  var turfFeature = ptsWithin.features;
				 
			      var features = new ol.Feature({
					  name: "POLYGON",
					  geometry: new ol.geom.Polygon(gridCoordinates)   
				  });
			         
			      tooltipCoord = features.getGeometry().getInteriorPoint().getCoordinates();	    
			      var gridDiv=document.createElement("div");
				  var id =  (i)+'grid';
				  gridDiv.setAttribute("id", id);
				  gridDiv.setAttribute("class", "tooltipGrid tooltipGrid-static");				    
				  gridDiv.innerHTML= 'vessel count: ' + turfFeature.length;		    
				  document.body.appendChild(gridDiv);			    			    
				  
				  var overlay = new ol.Overlay({
					  id : id,
				      element: gridDiv			      
			      });     
				
				  overlay.setPosition(tooltipCoord);
			      map.addOverlay(overlay);   
				  features.setStyle(gridStyle);   
				  vectorGridLayer.getSource().addFeature(features.clone());  	
				  vectorSource_Temp.addFeature(features.clone());  
				}	
			 map.addLayer(vectorGridLayer); 	 
			 
			 var extent = vectorSource_Temp.getExtent();		
				
				if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
				 { 
		   		 var diffExtent = extent[2]- extent[0];
		   		 if( diffExtent >= 0 && diffExtent < extentThreshold)
		   		 {
		   			 var center = ol.extent.getCenter(extent);							
		   			 map.getView().setCenter(center);
		   		 }
		   		 else
		   			 map.getView().fit(extent, map.getSize());	    		
				 }
			}
		}
		catch(err)
		{
			console.info('error in display grid layer: ' + err)
		}
  }
//------------ End of Grid Function-----------------------------------------------------------    
  


// ----------- Distance and Bearing Angle -------------------------------------------------------------
  	map.addLayer(vectorLayer_SelectedShipPosition);
    var previousFirstX = 0;
    var previousFirstY = 0;
    var previousSecondX = 0;
    var previousSecondY = 0;
    
    var styleDistancePoint = new ol.style.Style({
		 image: new ol.style.Circle({		             
			 radius: 3,
			 stroke: new ol.style.Stroke({
		     color: 'blue',
		     width: 1
			 }),
			 fill: new ol.style.Fill({
		      color: 'blue'
		          })
		 })
	 });	
    
	function calculateDistanceBearing()
	{
		try{
		var transformedCoordinates = new Array();		
		var lastPoint = distancecoordinates[distancecoordinates.length-1];
		var firstPoint = 0;
		for(var x=distancecoordinates.length-1; x >= 0; x--)
		{
			var temp = distancecoordinates[x];
			if(lastPoint[0]!=temp[0] && lastPoint[1]!=temp[1] )
			{
				firstPoint = distancecoordinates[x];
				break;
			}
		}
			
		 transformedCoordinates[0] = firstPoint; //ol.proj.transform(firstPoint , 'EPSG:3857','EPSG:4326'); //distancecoordinates.length-3		
		 transformedCoordinates[1] = lastPoint; //ol.proj.transform(lastPoint ,'EPSG:3857','EPSG:4326'); //distancecoordinates.length-1
		   
		var firstPointDistanceX = transformedCoordinates[0][0];  // longitude point1
		var firstPointDistanceY = transformedCoordinates[0][1];  // latitude point1
		var secondPointDistanceX = transformedCoordinates[1][0]; // longitude point2
		var secondPointDistanceY = transformedCoordinates[1][1]; // latitude point2
		
		 var pt1 = turf.point([firstPointDistanceX, firstPointDistanceY]);
		    
		 var pt2 = turf.point([secondPointDistanceX, secondPointDistanceY]);
		      
		 var bearing = turf.bearing(pt1, pt2); // ,{final: 'false'}
		 if(bearing<0)
			 bearing = bearing + 360;
		 var options = {units: 'miles'}; 
		 var distance = turf.distance(pt1, pt2, options);	
		 cumDistance = cumDistance + distance ;
		
		  var distanceNM = distance*0.868976;
		  var cumDistanceNM = cumDistance*0.868976;
		  
		  var elementRow = document.createElement('tr');
		  elementRow.className="trMap";
	     
		  var checkSamePoint = false;
		  
		  if(firstPointDistanceY== previousFirstY && firstPointDistanceX== previousFirstX &&
				  secondPointDistanceY== previousSecondY && secondPointDistanceX == previousSecondX)
			  checkSamePoint = true;
		  
		  if (checkSamePoint==false)
		  { 
			  elementRow.innerHTML = "<th >"+  firstPointDistanceY.toFixed(2) +"</th> <th> "+  firstPointDistanceX.toFixed(2) +
			  "</th> <th>"+ secondPointDistanceY.toFixed(2) +" </th> <th> "+ secondPointDistanceX.toFixed(2) +" </th>"+
			  "<th> "+ distanceNM.toFixed(2) + 'nm' +" </th> <th> "+ bearing.toFixed(2) +" </th> <th>"+ cumDistanceNM.toFixed(2) + 'nm' +" </th>";
			  document.getElementById("distanceTable").insertBefore(elementRow,document.getElementById("distanceTable").children[1]);	
			  previousFirstX = firstPointDistanceX;
			  previousFirstY = firstPointDistanceY;
			  previousSecondX = secondPointDistanceX;
			  previousSecondY = secondPointDistanceY;
			  
			  var  measureTooltipElement = document.createElement('div');
			    measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
			  var  measureTooltip = new ol.Overlay({
			    element: measureTooltipElement,
			    offset: [0, -15],
			    positioning: 'bottom-center',
			    id: 'distance' + currentDistancLine +currentOverlayCount
			  });
			  currentOverlayCount = currentOverlayCount+1;
			  map.addOverlay(measureTooltip);			 
			  measureTooltipElement.innerHTML = distanceNM.toFixed(2) +", " + bearing.toFixed(2) ;
		      measureTooltip.setPosition([(firstPointDistanceX+secondPointDistanceX)/2, (firstPointDistanceY+secondPointDistanceY)/2]);
		      
		      var featureStartPoint =new ol.Feature({
		    	  name: "distance" + currentDistancLine + " Point " + currentOverlayCount + "1",
		    	  geometry: new ol.geom.Point([firstPointDistanceX, firstPointDistanceY]),	
		    	 
		      });	
		      var featureEndPoint =new ol.Feature({
		    	  name: "distance" + currentDistancLine + " Point " + currentOverlayCount + "2",
		    	  geometry: new ol.geom.Point([secondPointDistanceX, secondPointDistanceY]),	
		    	 
		      });	
		      featureStartPoint.setStyle(styleDistancePoint);	
		      featureEndPoint.setStyle(styleDistancePoint);	
		  	
		  	  
		  	vectorLayer_SelectedShipPosition.getSource().addFeature(featureStartPoint);
		  	vectorLayer_SelectedShipPosition.getSource().addFeature(featureEndPoint);
		      
		      
		    //  vectorLayer_SelectedShipPosition.setZIndex(901);
		      
		  }
		}
		catch(err)
		{
			console.info('error in calculating distance and bearing: ' + err)
		}
	}
		
	function removePreviousDistanceLine()
	{		
		vectorLayer_SelectedShipPosition.getSource().clear();		
	}
		
	var distanceFlag = false;		
	var measuringTool;	
	var countDrawDistance = 0;
	var cumDistance = 0;		
	var distancecoordinates=0;  
	var elementTable = document.createElement('div');		
  	elementTable.id = "distanceTableId";
  	var currentDistancLine = 1;
  	var currentOverlayCount = 1;
  	//var select = new ol.interaction.Select();
  	
  /*	const snap = new ol.interaction.Snap({
  	  source: vectorLayer_SelectedShipPosition.getSource(),
  	  pixelTolerance: 20
  	});*/

  	
  	
  	
  	//
	var enableMeasuringTool = function() {
  		try{
	  		$('#distanceBearingButton').toggleClass("down");
			distanceFlag = true;
			map.removeInteraction(measuringTool);
			//map.removeInteraction(snap);
			var geometryType = 'LineString';
			 
			measuringTool = new ol.interaction.Draw({
			    type: geometryType,
			    source: vectorLayer_SelectedShipPosition.getSource()			   
			});
	
			$("#distanceTableId").show();
			
		  	if(document.getElementById("distanceTableId"))
		  		document.getElementById("distanceTableId").value = '';
		  	
			elementTable.innerHTML = "<div class=\"ol-customtest\"><table id = \"distanceTable\" class=\"table table-bordered table-hover\" > <tr class = \"trMap\"> "+					
										"<th > Source Lat </th> <th> Source Long </th> <th> Dest Lat </th> <th> Dest Long </th>"+
										"<th> Distance </th> <th> Angle </th> <th> Cum. Distance </th> </tr></table>" +	"</div";
			
			document.getElementById("map").appendChild(elementTable);
			$(elementTable).draggable(
			{containment: "parent"	}	
			);	   
			measuringTool.on('drawstart', function(event) {		
			   event.feature.on('change', function(event) {		    	
			    	 var geometry =  event.target.getGeometry();				  
			    	 distancecoordinates = geometry.getCoordinates();	
			    });
			});
			
				  
			measuringTool.on('drawend', function(e) {		  
			     /*calculateDistanceBearing(); */
			      previousFirstX = 0;
				  previousFirstY = 0;
				  previousSecondX = 0;
				  previousSecondY = 0;
				 cumDistance = 0;					 
				 countDrawDistance = -1;
				 
				 previousDrawDistance = 0;
				 currentDrawDistance = 0;
				 distancecoordinates=0;
				 e.feature.setProperties({
					    'id': 1234,
					    'name': 'distance' + currentDistancLine
					  })
				currentDistancLine = currentDistancLine+1;
				 currentOverlayCount = 1;
				 
				  var distancLine_Style = new ol.style.Style({
					  stroke: new ol.style.Stroke({
			              color: 'blue',
			            	  width: 1
			            })
				     });
				  e.feature.setStyle(distancLine_Style);
			});		
		  map.addInteraction(measuringTool);
		 // map.addInteraction(snap);
  		}
  		catch(err)
		{
			console.info('error in enabling drawing distance tool: ' + err)
		}
	};

	var disableMeasuringTool = function() {
		try{
			map.removeInteraction(measuringTool);
		if(distanceBearingFlag==true){
			$('#distanceBearingButton').toggleClass("down");
			$('#ringDistanceInfo').hide();			
			distanceFlag = false;
			ringRangeFlag = false;
			//removePreviousDistanceLine();
			map.removeInteraction(measuringTool);
			map.removeInteraction(snapPrueba);
			map.addInteraction(snapPrueba);
			//
			$('#distanceTableId').remove();		
			distanceBearingFlag = false;
		}
		}
  		catch(err)
		{
			console.info('error in disabling drawing distance tool: ' + err)
		}
	};
	
// ---------End of Distance and Bearing Angle -----------------------------------------

// ---------SAR Regions Layer ---------------------------------------------------------
	try{
		
		if(flagCountry=='1065')
		{
			var sourceSARRegion = new ol.source.Vector({
				wrapX: false,
		    	noWrap: true,
			    url: '../resources/js/mrcc.json',
			    format: new ol.format.GeoJSON()
			});		
		}
		else if(flagCountry=='1136')
		{
			var sourceSARRegion = new ol.source.Vector({
				wrapX: false,
		    	noWrap: true,
			    url: '../resources/js/mrccColombo.json',
			    format: new ol.format.GeoJSON()
			});	
		}
		var styleSARRegion=  new ol.style.Style({
			  stroke: new ol.style.Stroke({
	              color: 'Blue',
	              width :3  
	            })
		     });
		
		
		function setSARStyle(features,resolution){ 
			var color ="Black";
			var mrccText = "";
			var textPosition=[];
			if(features.get("name") === "MRCCMumbai") {
				color = "Red";
				mrccText = "MRCC \n Mumbai";
				textPosition = [66,12];	    		
	    	}
	    	if(features.get("name") === "MRCCChennai"){
	    		color = "Green";
				mrccText = "MRCC \n Chennai";
				textPosition = [83,12];	    		
	    	}
	    	if(features.get("name") === "MRCCPortblair"){
	    		color = "Blue";
				mrccText = "MRCC \n Portblair";
				textPosition = [92,12];	    		
	    	}
	    	if(features.get("name") === "MRCCColombo"){
	    		color = "Black";
				mrccText = "MRCC \n Colombo";
				textPosition = [79,5];	    		
	    	}			
			
	    	var styleSARRegion=  [new ol.style.Style({
	    			stroke: new ol.style.Stroke({
	    				color: color,
	    				width :3  
	            	})	          
		     	}),		
		     	new ol.style.Style({
				  stroke: new ol.style.Stroke({
		              color: 'Blue',
		              width :3  
		            }),
		            geometry:new ol.geom.Point(textPosition),
		            text: new ol.style.Text({			             
			             textAlign:"center",
			             baseline:"top",
			             text: mrccText,
			             font: 'bold 12px Calibri,sans-serif',
			             fill: new ol.style.Fill({
			               color: '#000'
			             }),
			             stroke: new ol.style.Stroke({
			               // color: '#fff',
			            	color: 'white',
			               width: 3
			             })
			           })
			     })
		
		
		
	    	];
		
		return styleSARRegion;
		}
		
		
		var layerSARRegion = new ol.layer.Vector({
		    source: sourceSARRegion,
		    name: "sarRegionLayer",
		    style: setSARStyle 	
		});
		
		
		map.addLayer(layerSARRegion);		
		layerSARRegion.setVisible(false);		
	}
	catch(err)
	{
		console.info('error in SAR Region Layer: ' + err)
	}
	function showSARRegions(){
		//map.addLayer(layerSARRegion);
		layerSARRegion.setVisible(true);
	}
	
	function hideSARRegions(){
		
		layerSARRegion.setVisible(false);
	}

// --------- SAR Region (MRCC) Related Function Ends ------
		
   function createSurpicRequest(){		
		
		openModalForm("sarsurpicrequest");			
	}
   
   function applysarsurpicRequest(){	
		try{
			var sarSurpicJsonObject;
			var sarArea;
			if($("input[name='sarareaType']:checked").val() === "rectangle"){	    		  
	    		  sarArea = applyRectangle();	    		 
	    		  sarArea.latOffset = sarArea.latOffset + sarArea.lat;
	    		  sarArea.longOffset = sarArea.longOffset + sarArea.lon;
	    		  sarAreaToSave = applySarCircle('rectangle');
	    		  
	    	  } else if($("input[name='sarareaType']:checked").val() === "circle"){
	    		  sarArea = applyCircle(); 	
	    		  sarAreaToSave = applySarCircle('circle');
	    	  }								
			displayPolygonOnMap(sarArea,"sararea",sourceCustomPolygon);	
			surpicRequestFlag = true;
		}
		catch(err)
		{
			console.info('error in apply sarsurpic Request: ' + err)
		}
	}
		
   
   
	var prevurl='';
	// Open form inside Modal Component ---------------------
	 function openModalForm(url){
    	  try{
	    	  //if(prevurl === url){ 
	    		  $("#pageModal").modal('show');
	    	 // }else{ 
	    	  $.ajax({url: url ,
	    		    success: function(result){
	    		    	 try{
			              //alert("success"+result);
			               $("#pageModaldialogContent").html(result);               
			               $("#pageModal").modal('show'); 
	    		    	 }
    		    	  catch(err)
    			  	  {
    		  			console.info('error in open Modal Form: ' + err)
    			  	  }
	           }});
	    	 // prevurl = url;
	    	 // }
    	  }
    	  catch(err)
	  	  {
  			console.info('error in open Modal Form: ' + err)
	  	  }
      }
	 
	 function applyPolygon(){
    	 try{ 
	    	 var areaname = $("#areaname").val();
	    	 
	    	  var latDegValues = $("input[id='latDeg']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var latMinValues = $("input[id='latMin']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var latDirection = $("select[id='latDirection']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonDegValues = $("input[id='lonDeg']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonMinValues = $("input[id='lonMin']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonDirection = $("select[id='lonDirection']")
	          .map(function(){return $(this).val();}).get();
	        
	    	  var latOut=[];
	    	  var lonOut=[];
	    	  
	    	  for(var i =0 ; i < latDegValues.length;i++){	    		 
	    		  latOut.push(DegMinToLatLon(latDegValues[i],latMinValues[i],latDirection[i]));
	    		  lonOut.push(DegMinToLatLon(lonDegValues[i],lonMinValues[i],lonDirection[i]));
	    	  }
	    	  var polygontype ="polygon";
	          var polygon={
	        		polygonId:null,
	        		polygonName: areaname,  
	          		polygontype: polygontype,
	          		lat : latOut,
	          		lon : lonOut
	          }
	          
	    	  return polygon;
    	 }
    	 catch(err)
 		{
 			console.info('error in apply Polygon: ' + err)
 		}
      }
     
//---------------- Custom Polygon ---- ------------------------------------
									
	var draw;
	var sourceCustomPolygon = new ol.source.Vector({
		 wrapX: false,
    	 noWrap: true
	});
		
	var layerCustomPolygon = new ol.layer.Vector({
	    source: sourceCustomPolygon,
	    name: "customPolygonLayer"
	});
	
	map.addLayer(layerCustomPolygon);
//----------------End of Custom Polygon ---- ------------------------------------

//----------------display search ship/port  ---- ------------------------------------
	
	var vectorSource_selectedPort = new ol.source.Vector(
	{              
		 wrapX: false,
    	 noWrap: true
    });
	
	
	var vectorLayer_selectedPort = new ol.layer.Vector({
		source:vectorSource_selectedPort,
		name: "selectedPortLayer"
	})		
	map.addLayer(vectorLayer_selectedPort);
	
	function generatePortLayer(response)
	{
		try{
	        /* invoke your function */
			vectorSource_Temp.clear();
		    	var portStyle = new ol.style.Style({
			    	 image: new ol.style.Circle({		             
			    		 radius: 4,
			    		 stroke: new ol.style.Stroke({
			    	     color: 'black',
			    	     width: 1
			    		 }),
			    		 fill: new ol.style.Fill({
			    	      color: 'black'
			    	    })
			    	 }),					    	
		    	 text: new ol.style.Text({
		             offsetY: 20,	
		             font: 'bold 12px Calibri,sans-serif',
		             fill: new ol.style.Fill({
		               color: '#000'
		             }),
		             stroke: new ol.style.Stroke({
		               //color: '#fff',
		            	color: 'white',
		               width: 3
		             })
		           })
			     });	
		    	var pointToFocusData = [[]];
		     for(var j =0; j<response.length; j++)
		     {							    	
		    	 if(response[j]!=null)
		    		 pointToFocusData.push([response[j].position.coordinates[0], response[j].position.coordinates[1]]);
			      var featuresSelectedPort = new ol.Feature({
			    	  name: "POINT" + j,
			    	  portName: response[j].name,
			    	  geometry: new ol.geom.Point([response[j].position.coordinates[0], response[j].position.coordinates[1]] )			
			      });					      
		    
				  //featuresSelectedPort.getGeometry().transform('EPSG:4326', 'EPSG:3857');   
				// featuresSelectedPort.setStyle(portStyle);
			      featuresSelectedPort.setStyle(function(featuresSelectedPort,resolution) {
    	  				portStyle.getText().setText(getTextPortName(featuresSelectedPort,resolution));
						return portStyle;
			        });
			 
				  vectorLayer_selectedPort.getSource().addFeature(featuresSelectedPort);
				  
				  vectorSource_Temp.addFeature(featuresSelectedPort.clone());
	    	 }   
		     
		     var extent = vectorSource_Temp.getExtent();
			 
			 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
			 { 
	    		 var diffExtent = extent[2]- extent[0];
	    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
	    		 {
	    			 var center = ol.extent.getCenter(extent);							
	    			 map.getView().setCenter(center);
	    		 }
	    		 else
	    			 map.getView().fit(extent, map.getSize());	    		
			 }
		    // map.getView().setZoom(3);
			 
		    }
			catch(err)
			{
				console.info('error in display Search Port:' + err)
			}
			
	}
	
	function displaySearchShipPortLayer()
	{
		try{
		radioBtnsSearchShipPort = document.getElementsByName("searchType");
		vectorSource_Temp.clear();
		//vectorLayer_selectedPort.getSource().clear();
		var pointToFocusData = [[]];
		
		for(var i=0;i<radioBtnsSearchShipPort.length; i++){
			
			if(searchTypeChecked == "port")
			{			
				try{
				var table = jq('#searchShipPortTable').DataTable();
				table.search('').draw();
				var portSelectedIndexForDisplay = [[]];
				var cc=0;				
				for(var i=0; i<portData.length; i++)
					{
						if($("#checkboxShipPort_"+i).prop("checked") == true)
						{
							portSelectedIndexForDisplay[cc] = portData[i].locode;
							cc++;
						}	
						
					}
				
				 $.ajax({
					    type: "POST",
					    traditional: true,
					    url: "/lrit/map/PortDetailsForDisplay?locode="+portSelectedIndexForDisplay,					    					   
					    success:function(response) {
					    	try{
					        /* invoke your function */					       
						    	var portStyle = new ol.style.Style({
							    	 image: new ol.style.Circle({		             
							    		 radius: 4,
							    		 stroke: new ol.style.Stroke({
							    	     color: 'black',
							    	     width: 1
							    		 }),
							    		 fill: new ol.style.Fill({
							    	      color: 'black'
							    	    })
							    	 }),					    	
						    	 text: new ol.style.Text({
						             offsetY: 20,	
						             font: 'bold 12px Calibri,sans-serif',
						             fill: new ol.style.Fill({
						               color: '#000'
						             }),
						             stroke: new ol.style.Stroke({
						               //color: '#fff',
						            	color: 'white',
						               width: 3
						             })
						           })
							     });	
				    		     
						     for(var j =0; j<response.length; j++)
						     {							    	
						    	 if(response[j]!=null)
						    		 pointToFocusData.push([response[j].position.coordinates[0], response[j].position.coordinates[1]]);
							      var featuresSelectedPort = new ol.Feature({
							    	  name: "POINT" + i,
							    	  portName: response[j].name,
							    	  geometry: new ol.geom.Point([response[j].position.coordinates[0], response[j].position.coordinates[1]] )			
							      });					      
						    
								  //featuresSelectedPort.getGeometry().transform('EPSG:4326', 'EPSG:3857');   
								  //featuresSelectedPort.setStyle(portStyle);
							      featuresSelectedPort.setStyle(function(featuresSelectedPort,resolution) {
				    	  				portStyle.getText().setText(getTextPortName(featuresSelectedPort,resolution));
										return portStyle;
							        });
								  
								  vectorLayer_selectedPort.getSource().addFeature(featuresSelectedPort);
								 // console.info(vectorLayer_selectedPort);
								 // map.addLayer(vectorLayer_selectedPort);
								  vectorSource_Temp.addFeature(featuresSelectedPort);
					    	 }   
						     
						     var extent = vectorSource_Temp.getExtent();
							 
							 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
							 { 
					    		 var diffExtent = extent[2]- extent[0];
					    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
					    		 {
					    			 var center = ol.extent.getCenter(extent);							
					    			 map.getView().setCenter(center);
					    		 }
					    		 else
					    			 map.getView().fit(extent, map.getSize());	    		
							 }
						    // map.getView().setZoom(3);
							 
						    }
							catch(err)
							{
								console.info('error in display Search Port:' + err)
							}
					    }   
			 		
					});	 
				}
				catch(err)
				{
					console.info('error in display Search Port:' + err)
				}
				}//port
			else
				{		
				try{
					var cc=0;
					
					for(var k=0; k<searchShipPortShipData.length; k++)
						{ 						
							if($("#checkboxShipPort_"+k).prop("checked") == true)
							{	
								var shipDataTemp = AllShipStatusInfo.get(searchShipPortShipData[k].imo_no.trim());	
								if(shipDataTemp!=undefined)
								{
									var featuresSelectedPort =  new ol.Feature({
							    	  name: "POINT" + k,
							    	  geometry: new ol.geom.Point([shipDataTemp.longitude, shipDataTemp.latitude] ),	
							    	  message_id: shipDataTemp.message_id,
							    	  ship_borne_equipment_id: shipDataTemp.ship_borne_equipment_id,
							    	  asp_id: shipDataTemp.asp_id,
							    	  imo_no: shipDataTemp.imo_no,
							    	  mmsi_no: shipDataTemp.mmsi_no,
							    	  dc_id: shipDataTemp.dc_id,
							    	  timestamp4: shipDataTemp.timestamp4,
							    	  dataUserRequestor: shipDataTemp.data_user_requestor,
							    	  ship_name: shipDataTemp.ship_name,
							    	  dataUserProvider: shipDataTemp.data_user_provider,
							    	  ddpversion_no: shipDataTemp.ddpversion_no,
							    	  course:shipDataTemp.course,
			    			    	  speed:shipDataTemp.speed
							      });		// getShipInfoFromloadedShipData(searchShipPortShipData[i].imo_no);	
									var searchShipStyle = new ol.style.Style({
							    	 image: new ol.style.Circle({		             
							    		 radius: 4,
							    		 stroke: new ol.style.Stroke({
							    	     color: 'black',
							    	     width: 1
							    		 }),
							    		 fill: new ol.style.Fill({
							    	      color: 'black'
							    	    })
							    	 })
									});	
									//featuresSelectedPort.getGeometry().transform('EPSG:4326', 'EPSG:3857');   
									featuresSelectedPort.setStyle(searchShipStyle);		  
								    vectorLayer_selectedPort.getSource().addFeature(featuresSelectedPort);
								    vectorSource_Temp.addFeature(featuresSelectedPort.clone());
								}
						    }   
						}
					
					var extent = vectorSource_Temp.getExtent();
					
					if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
					 { 
			    		 var diffExtent = extent[2]- extent[0];
			    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
			    		 {
			    			 var center = ol.extent.getCenter(extent);							
			    			 map.getView().setCenter(center);
			    		 }
			    		 else
			    			 map.getView().fit(extent, map.getSize());	    		
					 }
				    // map.getView().setZoom(3);
				}
				catch(err)
				{
					console.info('error in display Search Ship: ' + err)
				}
				} // end of else
			} // end of for(i=0;i<radioBtnsSearchShipPort.length; i++)
			 
			//map.addLayer(vectorLayer_selectedPort);			
			 //$("#searchShipPortTable").children().remove(); 
			 //document.getElementById("displaySerachButton").disabled = true;
			//Clear the SearchShipPort Form
			 $("#searchShipPortModal").modal('hide');			 
		}
		catch(err)
		{
			console.info('error in display Search Ship Port Layer' + err)
		}
	}
	
//---------------- End of display search ship/port ---- ------------------------------------	
	
//------------------------------------------------------Filter Vessel on Status -------------------------------------------------------------------------------------------- -->				
	
	var optionVesselTypeSelected;
	var optionVesselStatusSelected;
	var optionFVStatus;
	function filterVesselStatusFunc()
	{	
		try{
		
		 optionFVStatus = document.getElementsByName('FVStatusOptionVesselType');		 
		 for(var i = 0; i < optionFVStatus.length; i++) { 
             if(optionFVStatus[i].checked) 
        	 {
            	 optionVesselTypeSelected = optionFVStatus[i].value;
        	 }
         }	
			
		 optionFVStatus = document.getElementsByName('FVStatusOptionStatus');		 
		 for(var i = 0; i < optionFVStatus.length; i++) { 
             if(optionFVStatus[i].checked) 
        	 {
            	 optionVesselStatusSelected = optionFVStatus[i].value;
        	 }
         }	

		
		 if(optionVesselTypeSelected == "flagVessels")
		 {	
			  if(optionVesselStatusSelected == "respondingNormally")
				  {
					  hideAllShipLayer();
				      vectorLayerFlagShipRespondNormal.setVisible(true);
				     
				  }
			  else if(optionVesselStatusSelected== "missingPosition")
				  {
					  hideAllShipLayer();
				      vectorLayerFlagShipMissPosition.setVisible(true);
				  }
			  else if(optionVesselStatusSelected == "notResponding")
				  {
					  hideAllShipLayer();					  
				      vectorLayerFlagShipNoRespons.setVisible(true);
					  
				  }
			  else
				  {
				  	hideAllShipLayer();
					 vectorLayerFlagShipRespondNormal.setVisible(true);
					 vectorLayerFlagShipMissPosition.setVisible(true);
					 vectorLayerFlagShipNoRespons.setVisible(true);
				  }
		 }
			 	
		 
		 else if(optionVesselTypeSelected == "foreignVessels")
			 {
		
			  if(optionVesselStatusSelected == "respondingNormally")
			  {
				  hideAllShipLayer();
			      vectorLayerForeignShipRespondNormal.setVisible(true);
			  }
		  else if(optionVesselStatusSelected== "missingPosition")
			  {
				  hideAllShipLayer();
			      vectorLayerForeignShipMissPosition.setVisible(true);
			  }
		  else if(optionVesselStatusSelected == "notResponding")
			  {
				  hideAllShipLayer();				 
			      vectorLayerForeignShipNoRespons.setVisible(true);
			  }
		  else
			  {
				  hideAllShipLayer();
				  vectorLayerForeignShipRespondNormal.setVisible(true);
				  vectorLayerForeignShipMissPosition.setVisible(true);
				  vectorLayerForeignShipNoRespons.setVisible(true);			  
			  }
			 }
		 else if (optionVesselTypeSelected=="allVessels")
			{

			 if(optionVesselStatusSelected == "respondingNormally")
				  {
					 hideAllShipLayer();
					 vectorLayerForeignShipRespondNormal.setVisible(true);
					 vectorLayerFlagShipRespondNormal.setVisible(true);
				  }
			  else if(optionVesselStatusSelected== "missingPosition")
				  {
					  hideAllShipLayer();
					  vectorLayerFlagShipMissPosition.setVisible(true);
				      vectorLayerForeignShipMissPosition.setVisible(true);
				  }
			  else if(optionVesselStatusSelected == "notResponding")
				  {
					  hideAllShipLayer();
					  vectorLayerFlagShipNoRespons.setVisible(true);
				      vectorLayerForeignShipNoRespons.setVisible(true);
				  }
			  else
				  {
					 vectorLayerForeignShipRespondNormal.setVisible(true);
					 vectorLayerForeignShipMissPosition.setVisible(true);
					 vectorLayerForeignShipNoRespons.setVisible(true);
					 vectorLayerFlagShipRespondNormal.setVisible(true);
					 vectorLayerFlagShipMissPosition.setVisible(true);
					 vectorLayerFlagShipNoRespons.setVisible(true);
				  }				
			}
		}
		catch(err)
		{
			console.info('error in filter Vessel Status Function: ' + err)
		}
	}
	
	 function hrDiff(dt2) 
	 {
		 try{			 
			 var today = new Date();
			 var diff =(today.getTime() - dt2.getTime()) / 1000;			 
			
			 diff /= 3600;		 
			 return Math.abs(Math.round(diff));
		 }
		 catch(err)
		{
			console.info('error in hrDiff Function: ' + err)
		}
	 }
	 
	 function hideAllShipLayer() 
	 {
		 try{			 
			 vectorLayerForeignShipRespondNormal.setVisible(false);
			 vectorLayerForeignShipMissPosition.setVisible(false);
			 vectorLayerForeignShipNoRespons.setVisible(false);
			 vectorLayerFlagShipRespondNormal.setVisible(false);
			 vectorLayerFlagShipMissPosition.setVisible(false);
			 vectorLayerFlagShipNoRespons.setVisible(false);
		 }
		 catch(err)
		{
			console.info('error in hide All Ship Layer: ' + err)
		}
	 }
	 
	 function resetFVStatusModal()
	 {
		 try{
			 document.getElementById("FVStatusOptionVesselType").checked = true;
			 document.getElementById("FVStatusOptionStatus").checked = true;
		 }
		 catch(err)
		{
			console.info('error in reset filter Vessel Status Modal: ' + err)
		}
	 }

	
//------------------------------------------------------Filter Vessel on Status Ends-------------------------------------------------------------------------------------------- -->							
	 
    
 	//-------------------------------------------------------Display Geographical Layers--------------------------------------------------------------------------------------------
	 
     
     function removeGeographicalLayers()
     {   
    	 try{
    		// console.info('removeGeographicalLayers')GeographicalPorts
	    	 var layersToRemove = [];
			  map.getLayers().forEach(function (layer) {			  
			      if (layer.get('name') != undefined && (layer.get('name') === 'layerLakes' ||
			    		  layer.get('name') === 'layerSeas' || layer.get('name') === 'layerBays' ||
			    		  layer.get('name') === 'layerLightHouse' || layer.get('name') === 'layerShipWrek' ||
			    		  layer.get('name') === 'layerBathyLines' || layer.get('name') === 'layerOcean' || layer.get('name') === 'layerGeographicalPorts')) {		
			    	  //layer.getSource().clear();
			    	  layersToRemove.push(layer);
			    	  //layer.getSource().clear();
			    	  //map.removeLayer(layer);         
			      }
			  });		
				
			  var len = layersToRemove.length;
			  
			  for(var i = 0; i < len; i++) {			  
				 // layersToRemove[i].getSource().clear();			  	
			      map.removeLayer(layersToRemove[i]);
			  }
    	 }
    	 catch(err)
 		{
 			console.info('error in remove Geographical Layers: ' + err)
 		}
     }
     
     var sourceLayerOcean, sourceBethyLines, sourceLayerSeas, sourceLayerBays, sourceLayerLakes, sourceLightHouse, sourceShipWreck, sourceGeographicalPorts;
     function displayGeographicalLayers()
     {   
    	 try{
    		 
    	  removeGeographicalLayers();
	     
	      for(var i = 0; i< selectedGeographicalLayersList.length; i++)
	    	  {
	    	  
	    	  optionGeoLayerSelected = selectedGeographicalLayersList[i];
	    	   console.info('optionGeoLayerSelected: ' + optionGeoLayerSelected);
	    	  	if(optionGeoLayerSelected == "Lakes" )
			   	  {

  	  			  sourceLayerLakes  = new ol.source.ImageWMS({
		              url: geoserver_url, //'http://10.210.9.27:8080/geoserver/wms',
		              params: {'LAYERS': 'LakesGrp'},
		              ratio: 1,
		              serverType: 'geoserver',
			          noWrap: true,
			          wrapX:false
		                      
		          });
		    	  	  var  layerLakes =new ol.layer.Image({ 
		    	  		  extent: [-180, -90, 180, 90],
		    	  		  source: sourceLayerLakes,
		    	  		  name : "layerLakes"});
		    	  	  
		    	  	  progressID = "progressIDLayerLakes"; 
		    	  	  addprogress(sourceLayerLakes, progressID); 
		    	  	  map.addLayer(layerLakes);
		   	  
			   	  }  
	    	  	else if(optionGeoLayerSelected == "Seas" )
	    	  		{
	    	  	          sourceLayerSeas = new ol.source.ImageWMS({
			              url: geoserver_url, //'http://10.210.9.27:8080/geoserver/wms',
			              params: {'LAYERS': 'SeasGrp', 'TILED': false},
			              ratio: 1,
			              serverType: 'geoserver',
				              noWrap: true,
				              wrapX:false
			            		          
			          });
	    	  		
	    	  	  var  layerSeas =new ol.layer.Image({  
	    	  		  	extent: [-180, -90, 180, 90],
	    	  		  	source: sourceLayerSeas,
	    	  		  	name :"layerSeas" });
	    	  	  
	    	  	  	progressID = "progressIDLayerSeas"; 
	    	  	  	addprogress(sourceLayerSeas, progressID); 
	    	  		map.addLayer(layerSeas);
	    	  		}
	    	  	else if(optionGeoLayerSelected == "Bays" )
	    	  		{
	    	  		
	  		          sourceLayerBays = new ol.source.ImageWMS({
			              url: geoserver_url, //'http://10.210.9.27:8080/geoserver/wms',
			              params: {'LAYERS': 'BaysGrp'},
			              ratio: 1,
			              serverType: 'geoserver',
					      noWrap: true,
					      wrapX:false	          
			          });
    	  		 
    	  		 var  layerBays =new ol.layer.Image({  
    	  			 	extent: [-180, -90, 180, 90],
			            source: sourceLayerBays,
			            name:"layerBays" })
    	  		 
    	  		progressID = "progressIDLayerBays"; 
    	  		addprogress(sourceLayerBays, progressID); 
    	  		map.addLayer(layerBays);
	    	  		}
	    	  	else if(optionGeoLayerSelected == "LightHouse" )
	    	  		{
	    	  		
	    	  	   sourceLightHouse = new ol.source.ImageWMS({
			              url: geoserver_url, //'http://10.210.9.27:8080/geoserver/wms',
			              params: {'LAYERS': 'LightHouseGrp'},
			              ratio: 1,
			              serverType: 'geoserver',
					          noWrap: true,
					          wrapX:false
			            })	
		    	  		
		    	        var  layerLightHouse =new ol.layer.Image({    
		    	        	extent: [-180, -90, 180, 90],
				            source: sourceLightHouse,
				            name: "layerLightHouse"
		    	        });
			          
			        progressID = "progressIDLightHouse";    
			        addprogress(sourceLightHouse, progressID);	    
	    	  		map.addLayer(layerLightHouse);
	    	  		}
	    	  	else if(optionGeoLayerSelected == "ShipWreks" )
	    	  		{	    	  		
	    	 		  sourceShipWreck = new ol.source.ImageWMS({
			              url: geoserver_url, // 'http://10.210.9.27:8080/geoserver/wms',
			              params: {'LAYERS': 'ShipWrekGrp'},
			              ratio: 1,
			              serverType: 'geoserver',
				          noWrap: true,
				          wrapX:false
			            });
		    	  		
		    	  	  var  layerShipWrek =new ol.layer.Image({  
		    	  		extent: [-180, -90, 180, 90],
				            source: sourceShipWreck, 
				            name: "layerShipWrek"});
		    	  	  	
		    	  	  	progressID = "progressIDShipWreck";
				        addprogress(sourceShipWreck, progressID);	
		    	  		
				        map.addLayer(layerShipWrek);
	    	  		}
	    	  	else if(optionGeoLayerSelected == "BethyLines" )
	    	  		{
	    	  		
  	  			  sourceBethyLines = new ol.source.ImageWMS({
		              url: geoserver_url, // 'http://10.210.9.27:8080/geoserver/wms',
		              params: {'LAYERS': 'BathyLinesGrp'},
		              ratio: 1,
		              serverType: 'geoserver',
			          noWrap: true,
			          wrapX:false
		            })	
	            
		  	  	  var layerBathyLines  = new ol.layer.Image({ 
		  	  		extent: [-180, -90, 180, 90],
				            source: sourceBethyLines,
				            name : "layerBathyLines"
				            });
			            progressID = "progressIDBethyLines";
		  	  	    addprogress(sourceBethyLines, progressID);	
		  	  		map.addLayer(layerBathyLines);
	    	  		}
	    	  	else if( optionGeoLayerSelected == "Oceans" )
	    	  		{
  	  			  sourceLayerOcean = new ol.source.ImageWMS({
		              url: geoserver_url,//'http://10.210.9.27:8080/geoserver/wms',
		              params: {'LAYERS': 'Ocean1234Grp'},
		              ratio: 1,
		              serverType: 'geoserver',
			          noWrap: true,
			          wrapX:false
		            })
    	  		
	    	  		var  layerOcean = new ol.layer.Image({ 
	    	  		extent: [-180, -90, 180, 90],
			          	  source : sourceLayerOcean,  
			          	  name : "layerOcean"
			          });
	    	  		progressID = "progressIDOcean";
	    	  		addprogress(sourceLayerOcean, progressID);	
	    	  		
	    	  		map.addLayer(layerOcean);
	    	  		}
	    	  	
	    	  	else if( optionGeoLayerSelected == "GeographicalPorts" )
    	  		{
	  			  sourceLayerGeographicalPorts = new ol.source.ImageWMS({
	              url: geoserver_url,//'http://10.210.9.27:8080/geoserver/wms',
	              params: {'LAYERS': 'PortsGrp'},
	              ratio: 1,
	              serverType: 'geoserver',
		          noWrap: true,
		          wrapX:false
	            })
	  		
    	  		var  layerGeographicalPorts = new ol.layer.Image({ 
    	  		extent: [-180, -90, 180, 90],
		          	  source : sourceLayerGeographicalPorts,  
		          	  name : "layerGeographicalPorts"
		          });
    	  		progressID = "progressIDGeographicalPorts";
    	  		addprogress(sourceLayerGeographicalPorts, progressID);	
    	  		
    	  		map.addLayer(layerGeographicalPorts);
    	  		}
	    	  }
    	 }
    	 catch(err)
 		{
 			console.info('error in display Geographical Layers: ' + err)
 		}
     }
     
    
   //---------------------------------------------End of Display Geographical Layers-------------------------------------------------------------------------------------------- 
    
   //------------------------------------------Display Satellite Ocean Region-------------------------------------------------------------------------------------
 	var vectorSourceSatelliteOceanRegion = new ol.source.Vector(
			{              
			   	noWrap : true,
  	        	wrapX: false
	        }
	     );
	
	
 	var vectorLayerSatelliteOceanRegion = new ol.layer.Vector(
  			{      
  				source: vectorSourceSatelliteOceanRegion,
  				name: 'vectorSourceSatelliteOceanRegion'
  	        }
  	     );
	
 	
 	function removeSatelliteOceanRegionLayer(){
		  try{
			  removeLayersFromMap('vectorSourceSatelliteOceanRegion','name','POLYGON');
		  }
		  catch(err)
			{
				console.info('error in remove Satellite Ocean Region Layer: ' + err)
			}
	  }
 	
     function displaySateliteOceanRegion()
     {
    	try{
	     	var bboxRegionIndian = [-13.25, 76, 138.75, -76]; // Indian Ocean Region
	     	var bboxRegion2 =  [-91.5, 76, 60.5, -76]; //  Atlantic Ocean Region East
	     	var bboxRegion3 = [-127.5, 76, 24.5, -76]; // Atlantic Ocean Region West
	     	var bboxRegion4Right = [102.5, 76, 180, -76]; // Pacific Ocean Region
	     	var bboxRegion4Left = [-180, 76, -110, -76]; //Pacific Ocean Region
	
	     	var polyRegionIndian = turf.bboxPolygon(bboxRegionIndian);
	     	var polyRegion2 = turf.bboxPolygon(bboxRegion2);
	     	var polyRegion3 = turf.bboxPolygon(bboxRegion3);
	     	var polyRegion4Right = turf.bboxPolygon(bboxRegion4Right);
	     	var polyRegion4Left = turf.bboxPolygon(bboxRegion4Left);
	     	
	     	  var regionIndianOceanRegionStyle = new ol.style.Style({
	    		  stroke: new ol.style.Stroke({
	                  color: 'green',
	                  width: 2
	                }),
	                fill: new ol.style.Fill({
	                    color: 'rgba(0, 255, 0, 0.2)'
	                  }),
	                  strokeLineJoin: 'round',
	                  text: new ol.style.Text({
					        font: '24px Calibri,sans-serif',
					        fill: new ol.style.Fill({ color: '#000' }),
					        stroke: new ol.style.Stroke({
					          color: '#fff', width: 4
					        }),
					       
					        text:'Indian',					       
					        overflow: true
					      })
	                  
	//    			     	   		 fill: 'green',	
	//    			     	    	 opacity: '0.5'
	    	     });
	     	  
	     	 var region2OceanRegionStyle = new ol.style.Style({
	    		  stroke: new ol.style.Stroke({
	                  color: 'red',
	                  width: 2
	                }),
	                fill: new ol.style.Fill({
	                    color: 'rgba(255,0, 0, 0.2)'
	                  }),
	                  strokeLineJoin: 'round',
	                  text: new ol.style.Text({
	                	    offsetY: 30,
					        font: '24px Calibri,sans-serif',
					        fill: new ol.style.Fill({ color: '#000' }),
					        stroke: new ol.style.Stroke({
					          color: '#fff', width: 4
					        }),
					       
					        text:'AtlanticEast',					       
					        overflow: true
					      })
	//    			     	   		 fill: 'green',	
	//    			     	    	 opacity: '0.5'
	    	     });
	     	 
	     	 var region3OceanRegionStyle = new ol.style.Style({
	    		  stroke: new ol.style.Stroke({
	                  color: 'blue',
	                  width: 2
	                }),
	                fill: new ol.style.Fill({
	                    color: 'rgba(0,0, 255, 0.2)'
	                  }),
	                  strokeLineJoin: 'round',
	                  text: new ol.style.Text({
					        font: '24px Calibri,sans-serif',
					        fill: new ol.style.Fill({ color: '#000' }),
					        stroke: new ol.style.Stroke({
					          color: '#fff', width: 4
					        }),
					       
					        text:'AtlanticWest',					       
					        overflow: true
					      })
	//    			     	   		 fill: 'green',	
	//    			     	    	 opacity: '0.5'
	    	     });
	     	 
	     	 var region4OceanRegionStyle = new ol.style.Style({
	    		  stroke: new ol.style.Stroke({
	                  color: 'black',
	                	  width: 2
	                }),
	                fill: new ol.style.Fill({
	                    color: 'rgba(255, 255, 255, 0.2)'
	                  }),
	                  strokeLineJoin: 'round',
	                  text: new ol.style.Text({
					        font: '24px Calibri,sans-serif',
					        fill: new ol.style.Fill({ color: '#000' }),
					        stroke: new ol.style.Stroke({
					          color: '#fff', width: 4
					        }),
					       
					        text:'Pacific',					       
					        overflow: true
					      })
	//    			     	   		 fill: 'green',	
	//    			     	    	 opacity: '0.5'
	    	     });
	     	vectorSource_Temp.clear();
	     	vectorLayerSatelliteOceanRegion.getSource().clear();
	     	for(var m= 0; m< selectedOceanRegionList.length; m++)
	     	{
	     		var selectedOceanRegion = selectedOceanRegionList[m];
	     		if (selectedOceanRegion == 'Indian')
		     	 {
		     		var features = new ol.Feature({
					  name: "POLYGON",
					  geometry: new ol.geom.Polygon(polyRegionIndian.geometry.coordinates),	   
			  
		     		});
		     	 	
					 features.setStyle(regionIndianOceanRegionStyle);   
					 vectorLayerSatelliteOceanRegion.getSource().addFeature(features.clone()); 
					 vectorSource_Temp.addFeature(features.clone());
		     	 }
		     	if (selectedOceanRegion == 'AtlanticEast')
		    	 {
					  var features = new ol.Feature({
						  name: "POLYGON",
						  geometry: new ol.geom.Polygon(polyRegion2.geometry.coordinates),	   
						 
					  });
			     	 
					 features.setStyle(region2OceanRegionStyle);   
					 vectorLayerSatelliteOceanRegion.getSource().addFeature(features.clone()); 
					 vectorSource_Temp.addFeature(features.clone());
		    	 }  
		     	
		     	if (selectedOceanRegion == 'AtlanticWest')
		    	 {
					  var features = new ol.Feature({
						  name: "POLYGON",
						  geometry: new ol.geom.Polygon(polyRegion3.geometry.coordinates),	   
				  
					  });
			     	 
					 features.setStyle(region3OceanRegionStyle);   
					 vectorLayerSatelliteOceanRegion.getSource().addFeature(features.clone()); 
					 vectorSource_Temp.addFeature(features.clone());
		    	 }
		     	if (selectedOceanRegion == 'Pacific')
		    	 {
					  var features = new ol.Feature({
						  name: "POLYGON",
						  geometry: new ol.geom.Polygon(polyRegion4Right.geometry.coordinates),	   
					  
					  });
			     	 
					 features.setStyle(region4OceanRegionStyle);   
					 vectorLayerSatelliteOceanRegion.getSource().addFeature(features.clone()); 
					 vectorSource_Temp.addFeature(features.clone());
					 
					  var features = new ol.Feature({
						  name: "POLYGON",
						  geometry: new ol.geom.Polygon(polyRegion4Left.geometry.coordinates),	   
				 
					  });
			     	
					 features.setStyle(region4OceanRegionStyle);   
					 vectorLayerSatelliteOceanRegion.getSource().addFeature(features.clone()); 
					 vectorSource_Temp.addFeature(features.clone());
		    	 }
	     	}
	     	 map.addLayer(vectorLayerSatelliteOceanRegion);
	     	 
			 var extent = vectorSource_Temp.getExtent();
				//console.info(extent);
				if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
				 { 
		    		 var diffExtent = extent[2]- extent[0];
		    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
		    		 {
		    			 var center = ol.extent.getCenter(extent);							
		    			 map.getView().setCenter(center);
		    		 }
		    		 else
		    			 map.getView().fit(extent, map.getSize());	    		
				 }
    	}
    	catch(err)
		{
			console.info('error in display Satelite Ocean Region: ' + err)
		}
     }
     
     
   
   //--------------------------------------End of Display Satellite Ocean Region-------------------------------------------------------------------------------------
	 
  // Custom Polygon ------------------------------------
		
	// Add Geographical Area  -----------------------------
	var geographicAreaPolygonList =new Map();
	 
	function addGeographicalArea(){
		//openModalForm("addgeographicalarea");
		window.open("addgeographicalarea", '_blank');
	}	
	
	// Add Geographical Area  Ends ------------------------


	//-------------------Archived SURPIC---------------------------- 
	
	var SarPolygonStyle;
	 var SarPolygonStyleFlag = new ol.style.Style({
		  stroke: new ol.style.Stroke({
             color: 'red',
             width:3
           }),
           fill: new ol.style.Fill({
               color: [211, 211, 211, 0.2]
           })
	     });	
	 
		
	 
	 function styleFunctionSelectSarPolygon(feature){
			//console.info(map.getView().getZoom());
			 return [
				  new ol.style.Style({
				  stroke: feature.getStyle().getStroke() /*new ol.style.Stroke({
		              color: 'red',		
		              width:3
		            })*/,
		            text: new ol.style.Text({
				        font: '14px Calibri,sans-serif',
				        fill: new ol.style.Fill({ color: '#000' }),
				        stroke: new ol.style.Stroke({
				          color: '#fff', width: 4
				        }),
				        // get the text from the feature - `this` is ol.Feature
				        // and show only under certain resolution
				        //text: map.getView().getZoom() > 5 ? feature.get('areaId') : '',
				        text: feature.get('data_user_requestor')+" \n " +feature.get('sarsurpicrequest_timestamp').split('+')[0] ,
				       // offsetX: -20,
				       // offsetY: 20,
				        overflow: true
				      })
				  })
			  ];
		  }
	 
	  var SarPolygonStyleForeign = new ol.style.Style({
		  stroke: new ol.style.Stroke({
             color: 'blue'			             
           }),
           fill: new ol.style.Fill({
               color: [211, 211, 211, 0.2]
           })
	     });	
	
	  
	  var vectorSourceSURPIC24Hours = new ol.source.Vector(
		{              
		   	noWrap : true,
	        	wrapX: false
        }
     );
  	
  	
   	var vectorLayerSURPIC24Hours = new ol.layer.Vector(
		{      
			source: vectorSourceSURPIC24Hours,
			name: "SURPIC24HoursLayer"
	 
	    }
	 );     	
	  
	  
   	map.addLayer(vectorLayerSURPIC24Hours);
	  
	function displaySelectedSurpicArea(messageIdsSurpicAreaSelectedList,layertoAddData)
    { 
		 	

		  $.ajax({
	     		
			    type: "POST",
			    traditional: true,
			    url: "/lrit/map/findAllCgLritIdSurpicArea",
			    data: { 'cg_lritId': countryListIdsShips},
			    success:function(responseSurpicArea) {

					  $.ajax({
				     		
						    type: "POST",
						    traditional: true,
						    url: "/lrit/map/SurpicAreaByMessageID",
						    data: { 'message_idList': messageIdsSurpicAreaSelectedList },
						    success:function(response) {
							    
							try{	
					    		
					    		jsondata = response;
					    		 for(var i =0; i<jsondata.length; i++)
								  {
								  if((jsondata[i].rectangular_area == null || jsondata[i].rectangular_area == "") &&
										  (jsondata[i].circular_area != null || jsondata[i].circular_area != "") )//If SURPIC is circle
									  {
									  
									  jsondata[i].circular_area = jsondata[i].circular_area.toUpperCase();						
									  var flagN =1;
									  var flagE =1; 
									  //Extract Latitude
									  	var indexLat = jsondata[i].circular_area.indexOf("N");
									  if(indexLat < 0)
										  {
										   indexLat = jsondata[i].circular_area.indexOf("S");
										   flagN = 0;
										  }
									  var latitude
									  if(flagN == 1)
										  latitude =  jsondata[i].circular_area.substring(0,indexLat-1);
									  else
										  latitude =  '-' + jsondata[i].circular_area.substring(0,indexLat-1);
									  
									  //Extract Longitude
									  	var indexLong = jsondata[i].circular_area.indexOf("E");							  	
						
									  	if(indexLong < 0)
											  {
											  	indexLong = jsondata[i].circular_area.indexOf("W");
											  	flagE = 0;
											  }
										  														  
										  if(flagE == 1)
											  longitude =  jsondata[i].circular_area.substring(indexLat+2,indexLong-1);
										  else
											  longitude =  '-' + jsondata[i].circular_area.substring(indexLat+2,indexLong-1);
										  
										  //Extract Radius
										  var radius = jsondata[i].circular_area.substring(indexLong+2, jsondata[i].circular_area.length)*1.151;									 
										  var newCord = ol.proj.transform([longitude, latitude], 'EPSG:4326', 'EPSG:3857');
										  var center =  [Number(longitude), Number(latitude)];							  
										  var options = {steps: 360, units: 'miles', options: {}}; //degrees
										  var circle = turf.circle(center, radius, options);
										  var features = new ol.Feature({
												  name: "POLYGON",
												  type: "SarArea",
												  geometry: new ol.geom.Polygon(circle.geometry.coordinates),
												  data_user_requestor: jsondata[i].data_user_requestor,	
									     	 	  sarsurpicrequest_timestamp: jsondata[i].sarsurpicrequest_timestamp
										  
											  });
								  }
								 else if((jsondata[i].circular_area == null || jsondata[i].circular_area == "") &&
										  (jsondata[i].rectangular_area != null || jsondata[i].rectangular_area != "") )
								 {	
						
									 jsondata[i].rectangular_area = jsondata[i].rectangular_area.toUpperCase();
									
									  var flagN =1;
									  var flagE =1; 						  
									  var tempdataArray = jsondata[i].rectangular_area.split(":");						  
									  //Extract Latitude						  
									  	var indexLat = tempdataArray[0].indexOf("N");
									  if(indexLat < 0)
										  {
										   indexLat = tempdataArray[0].indexOf("S");
										   flagN = 0;
										  }
									  var latitude
									  if(flagN == 1)
										  latitude =  tempdataArray[0].substring(0,indexLat-1);
									  else
										  latitude =  '-' + tempdataArray[0].substring(0,indexLat-1);
									  
									  //Extract Longitude
									  	var indexLong = tempdataArray[1].indexOf("E");							  	
										  if(indexLong < 0)
											  {
											  	indexLong = tempdataArray[1].indexOf("W");
											  	flagE = 0;
											  }
										  														  
										  if(flagE == 1)
											  longitude =  tempdataArray[1].substring(0,indexLong-1);
										  else
											  longitude =  '-' + tempdataArray[1].substring(0,indexLong-1);
										  
										  //Extract Offset
										  indexLat = tempdataArray[2].indexOf("N");
										  var offsetNorth = tempdataArray[2].substring(0,indexLat-1);
										  
										  indexLong = tempdataArray[3].indexOf("E");
										  var offsetEast = tempdataArray[3].substring(0,indexLong-1);
										  
										  //Draw Rectangle												  
										  var rectangleCoordinates = [Number(longitude), Number(latitude), Number(longitude)+ Number(offsetEast), Number(latitude)+Number(offsetNorth)];							  				
										  rectangle = turf.bboxPolygon(rectangleCoordinates);
										  
										  //Add Rectangular Features
									     	 var features = new ol.Feature({
												  name: "POLYGON",
												  type: "SarArea",
												  geometry: new ol.geom.Polygon(rectangle.geometry.coordinates),	  
												  data_user_requestor: jsondata[i].data_user_requestor,	
									     	 	  sarsurpicrequest_timestamp: jsondata[i].sarsurpicrequest_timestamp
										          });						 	
									 
									  }	
								  
								  var setFlagStyle = false;
								  for(var k=0; k<responseSurpicArea.length;k++ )
								  {  
									  
									  if (jsondata[i].data_user_requestor===flagCountry || (jsondata[i].data_user_requestor).trim() ===(responseSurpicArea[k].sar_asp_dc_CglritID).trim())
								    	{
										 
										  features.set('type','SarAreaFlag'); 
										  features.setStyle(SarPolygonStyleFlag); 
										  setFlagStyle = true;
								    	}									 
								  }
								 
								  if(setFlagStyle===false)
							    	{
									  	features.setStyle(SarPolygonStyleForeign);
								    	features.set('type','SarAreaForeign'); 
							    	}
								  
								    layertoAddData.getSource().addFeature(features.clone());						
									vectorSource_Temp.addFeature(features.clone());
								  }
					 
					    		 // map.addLayer(layertoAddData);
					    		  layertoAddData.setZIndex(1);
								  
					
						    	}
								catch(err)
								{
									console.info('error in SurpicArea: ' + err)
								}
						    }
					  });
			    }
		  });
		
    }
	
	
        
    function displaySelectedSurpicAreaByMessageID(response,vectorLayerSURPICArchived)
    {    	
    	 try {
    		 
    		 var vectorSourceSURPICArchived = new ol.source.Vector(
    	    			{              
    	    			   	noWrap : true,
    	      	        	wrapX: false
    	    	        }
    	    	     );
    	    	
    	    	
    	     	var vectorLayerSURPICArchived = new ol.layer.Vector(
    	      			{      
    	      				source: vectorSourceSURPICArchived,
    	      				name: "SURPICArchivedLayer"
    	      	     
    	      	        }
    	      	     );     	
    	     	map.addLayer(vectorLayerSURPICArchived);
    	     	var table = jq('#sarAreaViewRightTable').DataTable();
    			table.search('').draw();
    			
    	     	var messageIdsSurpicAreaSelectedList = [];
    	     	//console.info("messageIdsSurpicArea: " + messageIdsSurpicArea);
    			 for(var i=0; i<messageIdsSurpicArea.length;i++)
    			 {
    				 if($("#" + messageIdsSurpicArea[i].trim() + "_sarAreaMsgId").prop("checked") == true) 
    		      	{	
    					 messageIdsSurpicAreaSelectedList.push(messageIdsSurpicArea[i]);	      	
    		      	}
    			 }		
    			 
    		 vectorSource_Temp.clear();		    
    		// console.info('messageIdsSurpicAreaSelectedList: ' + messageIdsSurpicAreaSelectedList);
			 if(messageIdsSurpicAreaSelectedList!=null || messageIdsSurpicAreaSelectedList!=[])
				 displaySelectedSurpicArea(messageIdsSurpicAreaSelectedList,vectorLayerSURPICArchived);
    		 
			  var extent = vectorSource_Temp.getExtent();	
			  if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
				 { 
		    		 var diffExtent = extent[2]- extent[0];
		    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
		    		 {
		    			 var center = ol.extent.getCenter(extent);							
		    			 map.getView().setCenter(center);
		    		 }
		    		 else
		    			 map.getView().fit(extent, map.getSize());	    		
				 }
    		     		 
			 			  
			 if($("#sarRegionAreaVessel").prop("checked") == true)
			 {
				 if(messageIdsSurpicArea.length>0)
				 {
					 $.ajax({
						    type: "POST",
						    traditional: true,
						    url: "/lrit/map/ShipsInSurpicArea",
						    data: { 'message_id': messageIdsSurpicArea },
						    success:function(response) {					    	
						    	displayShipsInSurpicArea(response);				        
						      }				 		
						});
				 }
			 }		 			 
			 
		  }
		 catch(err) {
			console.info(err);
		} 
    }
    
	   
    function displayShipsInSurpicArea(surpicShipInfo)
    {
    	try{
	    	
	    	var vectorSourceSURPICArchivedShip = new ol.source.Vector(
				{              
				   	noWrap : true,
		        	wrapX: false
		        }
		     );
	    	
	    	
	     	var vectorLayerSURPICArchivedShip = new ol.layer.Vector(
	  			{      
	  				source: vectorSourceSURPICArchivedShip,
	  				name: "SURPICArchivedShipLayer"
	  	     
	  	        }
	  	     );
	     	
	     	var sarShipPositionStyle = new ol.style.Style({
		    	 image: new ol.style.Circle({		             
		    		 radius: 3,
		    		 stroke: new ol.style.Stroke({
		    	     color: 'red',
		    	     width: 1
		    		 }),
		    		 fill: new ol.style.Fill({
		    	      color: 'red'
		    	          })
		    	 })
		     });		
	     	
	    	// to remove previous Ship Layer
	    	//vectorLayer_ShipPosition.getSource().clear();
	    	//hideAllShipLayer(); 
	    	vectorSource_Temp.clear();
	    	surpicShipInfoForDisplay.clear();
	    	
			 for (var i=0; i<surpicShipInfo.length; i++)
			 {
				// var shipDataTemp = AllShipStatusInfo.get(surpicShipInfo[i].imo_no);
				// if(shipDataTemp)
				 surpicShipInfo[i].popupStatus = false;
				 surpicShipInfoForDisplay.set((surpicShipInfo[i].message_id).trim(), surpicShipInfo[i]);
				 
				 var features =new ol.Feature({				 
			    	  name: "POINT" + i,
			    	  geometry: new ol.geom.Point([surpicShipInfo[i].longitude, surpicShipInfo[i].latitude] ),	
			    	  message_id: surpicShipInfo[i].message_id,
			    	  ship_borne_equipment_id: surpicShipInfo[i].ship_borne_equipment_id,
			    	  asp_id: surpicShipInfo[i].asp_id,
			    	  imo_no: surpicShipInfo[i].imo_no,
			    	  mmsi_no: surpicShipInfo[i].mmsi_no,
			    	  dc_id: surpicShipInfo[i].dc_id,
			    	  timestamp4: surpicShipInfo[i].timestamp4,
			    	  dataUserRequestor: surpicShipInfo[i].data_user_requestor,
			    	  ship_name: surpicShipInfo[i].ship_name,
			    	  dataUserProvider: surpicShipInfo[i].data_user_provider,
			    	  ddpversion_no: surpicShipInfo[i].ddpversion_no,
			    	  course:'NA',
			    	  speed:'NA'
			      });
				 	
				 					
			    	 features.setStyle(sarShipPositionStyle);		  
			    	 vectorLayerSURPICArchivedShip.getSource().addFeature(features.clone());
			    	 vectorSource_Temp.addFeature(features.clone());
				 }
			 map.addLayer(vectorLayerSURPICArchivedShip);
			 vectorLayerSURPICArchivedShip.setZIndex(3);
			 var extent = vectorSource_Temp.getExtent();	
			 
			 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
			 { 
	    		 var diffExtent = extent[2]- extent[0];
	    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
	    		 {
	    			 var center = ol.extent.getCenter(extent);							
	    			 map.getView().setCenter(center);
	    		 }
	    		 else
	    			 map.getView().fit(extent, map.getSize());	    		
			 }
    	}
    	catch(err)
		{
			console.info('error in display Ships In Surpic Area: ' + err)
		}
    }
//--------------------------------------------------------------------------------//
    // for custom polygon 
    //var currentPolygonFeature;
    function openCustomPolygonForm(){	
		try{
			currentPolygonFeature = "";
			openModalForm("custompolygonform");
			
		}
		catch(err)
		{
			console.info('error in open Custom Polygon Form: ' + err)
		}
	}			
	
	var draw;
	var sourceCustomPolygon = new ol.source.Vector({
		wrapX: false
	});
		
	var layerCustomPolygon = new ol.layer.Vector({
		source: sourceCustomPolygon,
		name: "customPolygonLayer"
	});
	
	map.addLayer(layerCustomPolygon);
	var customPolygonInteractionFlag = false;
	function addInteraction() {
		try{
		draw = new ol.interaction.Draw({
			source: sourceCustomPolygon,
			type: 'Polygon',
		  });				  
		draw.on('drawend', function(event) {    
		
			event.feature.setProperties({					    
				'areatype': 'userarea'
			  })
			map.removeInteraction(draw);
			customPolygonInteractionFlag = false;
		});
			map.addInteraction(draw);
		}
		catch(err)
		{
			console.info('error in add Interaction for polygon: ' + err)
		}
	  }
	
	var tempHandler;
	function freehandDrawOnMap(){
		
		 map.un('singleclick', clickevtMap);
		//map.removeEventListener('click');
		 customPolygonInteractionFlag = true;
		addInteraction(draw);
	}			

	var currentPolygonFeature;
	function customPolygonRighClickMenu(geometryFeature,elementList){
		try{
			currentPolygonFeature = geometryFeature;
			var innerHtmlStr = "<ul class=\"ulRightClick\" id=\"results\">" +
			"<li class=\"customPolygonHideClass liRightClick\"> <a href=\"#\">Hide </a> </li>" +
			"<li class=\"customPolygonHideAllClass liRightClick\"> <a href=\"#\">Hide All </a> </li>" +
			"<li class=\"customPolygonDiscardClass liRightClick\"> <a href=\"#\">Discard </a> </li>" ;
			
			 if(geometryFeature.get("polygonId")){
				 innerHtmlStr = innerHtmlStr +"<li class=\"customPolygonModifyClass liRightClick\"> <a href=\"#\">Modify </a> </li>";
			 } else {
				 innerHtmlStr = innerHtmlStr +"<li class=\"customPolygonSaveClass liRightClick\"> <a href=\"#\">Save </a> </li>" ;
			 }
			innerHtmlStr = innerHtmlStr +"</ul>";	
			elementList.innerHTML = innerHtmlStr;
		}
		catch(err)
		{
			console.info('error in custom Polygon Righ Click Menu: ' + err)
		}
	}


	function modifypolygon(){
		
		$.ajax({url: "/lrit/map/getselecteduserarea?id="+currentPolygonFeature.get("polygonId") ,type: 'get'
			 ,success: function(result){
				 try{	    			    	 
					 openSaveFormWithData("saveform",result[0]);
				 }
				catch(err)
				{
					console.info('error in modify Polygon: ' + err)
				}
		 }});    	 
		
	}

	function hideCustomPolygon(){
		try{
			var feature = currentPolygonFeature;
			if (feature) {
				  sourceCustomPolygon.removeFeature(feature);
			  
			}
		}
		catch(err)
		{
			console.info('error in hide custom Polygon: ' + err)
		}
	}

	function hideSurpicPolygon(){
		try{
			var features = layerCustomPolygon.getSource().getFeatures();
			
			features.forEach(function(feature) {
				  
		    	if (feature.get('areatype') == 'sararea')	      
				    {
		    		layerCustomPolygon.getSource().removeFeature(feature);
		    	};
		    });
		}
		catch(err)
		{
			console.info('error in hide surpic Polygon: ' + err)
		}
	}

	function hideAllCustomPolygons(){	
		try{
		sourceCustomPolygon.clear();	
		}
		catch(err)
		{
			console.info('error in hide all custom Polygon: ' + err)
		}
	}


	function discardPolygon(){	
		try{
			if ((confirm("Click OK to delete Selected User Defined Area from Database")) == true) {
				hideCustomPolygon();
				$.ajax({url: "/lrit/map/closeselecteduserarea?id="+currentPolygonFeature.get("polygonId") ,type: 'get'
					 ,success: function(result){		    			    	 
						//alert(result);
				 }});
			} 
		}
		catch(err)
		{
			console.info('error in discard Polygon: ' + err)
		}
	}

	//--------------------------------------------------------------------------------//	
    
    
	//-------------------Progress Bar while map is loading----------------------------					   
	function addprogress(sourceLayer, progressID)
	{
		
			if(document.getElementById("progressID")!== null)
			{	         
		       $("#progress").remove();		       
			}
		       
  		var progressDiv = document.createElement('Progress');	
    	progressDiv.id = 'progress'
        document.body.appendChild(progressDiv); 
    	
        var progress = new Progress(document.getElementById('progress'));
        	
        	sourceLayer.on('imageloadstart', function() {
  			progress.addLoading(progressID);
             	});
        		
  			sourceLayer.on('imageloadend', function() {
            progress.addLoaded(progressID);
          });
  		
  		sourceLayer.on('imageloaderror', function() {
  			  progress.addLoaded(progressID);
            
          });
	
	}
     function Progress(el) {       
       this.el = el;	
       this.loading = 0;
       this.loaded = 0;
     }

     /**
      * Increment the count of loading tiles.
      */
     Progress.prototype.addLoading = function(progressID) {    
         		 
         $("#progressID").remove();		     
         $("#progress").remove();	     		      
    	 var imgProgress = document.createElement("img");  
    	 imgProgress.id = "progressID"
    	 imgProgress.src = "\\lrit\\resources\\icons\\progress.gif";
    	 var progressDiv = document.getElementById("map");   
    	 progressDiv.appendChild(imgProgress);
    	
    	  var progressOverlay = new ol.Overlay({
   		  position: map.getView().getCenter(),
   		  positioning: 'center-center',
   		  element: document.getElementById("progressID"),		  
   		});
   		map.addOverlay(progressOverlay);
   		zoomChangedFlag = 0;
    		       
     };

     	Progress.prototype.addLoaded = function(progressID) {
         $("#progressID").remove();	    
         $("#progress").remove();

     };
 	
//--------------------------------------------------------------------------------
 
     // to display ship name on different resolution
    
     var getText = function(feature, resolution) {
         var maxResolution = 0.02;//1400;
         var text = feature.get('ship_name');        
         if (resolution > maxResolution) {			
           text = '';
         }
         return text;
     }; 
     
     // path projection batch in circle
     
     function shipsInsideCirclePathPrediction(featureCircle){
  	   //console.info(feature.getGeometry().getRadius());
  	  var circle =  ol.geom.Polygon.fromCircle(featureCircle.getGeometry(),12,90);  	 
  	  var searchWithin = turf.polygon(circle.getCoordinates());  	  
  	   var shipPoints =[[]];
  		 for(var i =0; i<filterdShipData.length; i++)
  	     {			 	
  			 shipPoints[i] = turf.point([filterdShipData[i].longitude, filterdShipData[i].latitude],{shipIndex : i });			 
  	     }		
  		 var collection = turf.featureCollection(shipPoints);
  		 ptsWithin = turf.pointsWithinPolygon(collection, searchWithin); 			
  		
  		// To Mark selected ship ----------
  		// var format = new ol.format.GeoJSON();
  		 //var marker = format.readFeatures(ptsWithin);		 
  		// sourceCustomPolygon.addFeatures(marker);
  		//---------------------------------- 
  		 
  		 turf.featureEach(ptsWithin, function (currentFeature) {
  			 var selectedship=filterdShipData[currentFeature.properties['shipIndex']];
  			 currentShip = selectedship.imo_no;
  			 document.getElementById("PPimoNoVal").value = currentShip;  			 
  			 //showPathProjection( selectedship.latitude, selectedship.longitude, selectedship.course, pathProjectionLength*100000);
  			    pathProjectionLength = $("#idRingDistance").val();
				pathProjectionAfterThisHour = pathProjectionLength;
				if(selectedship.speed>0 && selectedship.course>0)
					showPathProjection(selectedship.latitude, selectedship.longitude, selectedship.course, selectedship.speed*1850*pathProjectionAfterThisHour);//in meters, Knots is nm/hr	
				//else
					//alert('Not enough information for Predicting Path')
  			});
  		
     }
             
     var flagCircleInteraction=false;
     map.addLayer(vectorLayer_pathProjection);
     function addInteractionCircle(coords) {
      
  	   if (flagCircleInteraction==false){
  		    
  	   draw= new ol.interaction.Draw({
  	      source: vectorLayer_pathProjection.getSource(),
  	      type: 'Circle',
  	      //maxPoints:2,
  	      //freehand:true,
  	      geometryFunction : function(coordinates, geometry) { 
  	             
  	              var pt1 = coords; //map.getView().getCenter();  	             
  	              var pt2 = coordinates[1];
  	              var radius = Math.sqrt( (pt1[0]-pt2[0])*(pt1[0]-pt2[0]) + (pt1[1]-pt2[1])*(pt1[1]-pt2[1]) );
  	              if (!geometry) {
  		                geometry = new ol.geom.Circle(pt1, radius);
  		           } 
  	              geometry.setCenterAndRadius(pt1, radius); 
  	              coordinates.splice(0,2); 
  	              coordinates.push(pt1)
  	              coordinates.push(pt2)
  	              return geometry;
  	            }
  	    });  	  
  	    
  	    draw.on('drawend', function(event) { 			
  				shipsInsideCirclePathPrediction(event.feature);
  				map.removeInteraction(draw);
  				flagCircleInteraction =false; 
  				
  			});
  	    map.addInteraction(draw);  	 
  	    flagCircleInteraction = true;
  	  }
     }
     
     
     
     var getTextPortName = function(feature, resolution) {
         var maxResolution = 0.2;//1400;
         var text = feature.get('portName');         
         if (resolution > maxResolution) {			
           text = '';
         }
         return text;
     };
    
      
     //-RingRange--------------
     var rangeRingStyle = new ol.style.Style({	    
		 stroke: new ol.style.Stroke({
             color: 'gray',
             width:0.2
           })
		    });	
	 var outerRingStyle = new ol.style.Style({	    
		 stroke: new ol.style.Stroke({
             color: 'black',
            // lineDash: [4, 4]
           })
		    });	
	 var rangeLineStyle = new ol.style.Style({	    
		 stroke: new ol.style.Stroke({
             color: 'black',
             lineDash: [4, 4]
           }),
	 		text: new ol.style.Text({
	 			placement:"point",
	 			textAlign:"start"
	 		})
		    });	
     
	 var ringRangeFlag = false;
	 
     var addInteractionRangeRing = function() {
   		try{
 	  		
 			$('#distanceBearingButton').toggleClass("down");
    	 	ringRangeFlag = true;
			map.removeInteraction(measuringTool);
 
 			
 			//map.removeInteraction(snap);
 			var geometryType = 'LineString';
 			 
 			measuringTool = new ol.interaction.Draw({
 			    type: geometryType,
 			    source: vectorLayer_SelectedShipPosition.getSource(),
 			    maxPoints:2
 			});			  
 			measuringTool.on('drawend', function(e) {		  
 				var geometry =  e.feature.getGeometry();				  
			    distancecoordinates = geometry.getCoordinates();	

 				var firstPointDistanceX = distancecoordinates[0][0];  // longitude point1
 				var firstPointDistanceY = distancecoordinates[0][1];  // latitude point1
 				var secondPointDistanceX = distancecoordinates[1][0]; // longitude point2
 				var secondPointDistanceY = distancecoordinates[1][1]; // latitude point2				 
			    var pt1 = turf.point(distancecoordinates[0]);				    
				var pt2 = turf.point(distancecoordinates[1]);
    
 				 var bearing = turf.bearing(pt1, pt2); // ,{final: 'false'}
 				 if(bearing<0)
 					 bearing = bearing + 360;				
 				
 				
 			     var options = {units: 'miles'}; 
				 var finalRadius_miles = turf.distance(distancecoordinates[0], distancecoordinates[1], options);	
				 var center = distancecoordinates[0];
				 // alert("Center : "+ distancecoordinates[0] +"Distance : " + finalRadius_miles+" Bearing : "+bearing);
				 
				  var  measureTooltipElement = document.createElement('div');
				    measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
				  var  measureTooltip = new ol.Overlay({
				    element: measureTooltipElement,
				    offset: [0, -15],
				    positioning: 'bottom-center',
				    id: 'distance' + currentDistancLine +currentOverlayCount
				  });
				  currentOverlayCount = currentOverlayCount+1;
				  map.addOverlay(measureTooltip);			 
				  measureTooltipElement.innerHTML = finalRadius_miles.toFixed(2) +", " + bearing.toFixed(2) ;
			      measureTooltip.setPosition([(firstPointDistanceX+secondPointDistanceX)/2, (firstPointDistanceY+secondPointDistanceY)/2]);

			      var featureStartPoint =new ol.Feature({
			    	  name: "distance" + currentDistancLine + " Point " + currentOverlayCount + "1",
			    	  geometry: new ol.geom.Point([firstPointDistanceX, firstPointDistanceY]),	
			    	 
			      });	
			      var featureEndPoint =new ol.Feature({
			    	  name: "distance" + currentDistancLine + " Point " + currentOverlayCount + "2",
			    	  geometry: new ol.geom.Point([secondPointDistanceX, secondPointDistanceY]),	
			    	 
			      });	
			      featureStartPoint.setStyle(styleDistancePoint);	
			      featureEndPoint.setStyle(styleDistancePoint);				  	
			  	  
			  	vectorLayer_SelectedShipPosition.getSource().addFeature(featureStartPoint);
			  	vectorLayer_SelectedShipPosition.getSource().addFeature(featureEndPoint);
				
 				if(finalRadius_miles){
 					if(finalRadius_miles > 0){
 				 e.feature.setProperties({
 					    'id': 1234,
 					    'name': 'distance' + currentDistancLine
 					  })
 				currentDistancLine = currentDistancLine+1;
 				 currentOverlayCount = 1;
 				 
 				  var distancLine_Style = new ol.style.Style({
 					  stroke: new ol.style.Stroke({
 			              color: 'blue',
 			            	  width: 1
 			            }),
 			            text: new ol.style.Text({
 				 			placement:"point",
 				 			textAlign:"start",
 				 			//text:	finalRadius_miles.toFixed(2) +", " + bearing.toFixed(2) 
 				 		})
 				  
 				     });
 				  e.feature.setStyle(distancLine_Style); 				  
 				  
 				//-------------- 				 
 				// Outer Circle
 				 var outerCircle =  turf.circle(center, finalRadius_miles, options);
 				 var outerCircleFeature = new ol.Feature({geometry : new ol.geom.Polygon(outerCircle.geometry.coordinates), radius:finalRadius_miles});
 				 outerCircleFeature.setStyle(outerRingStyle);
 				 sourceCustomPolygon.addFeature(outerCircleFeature); 
 				 
 				 //Rings---
 				ringdistanceNM = $("#idRingDistance").val();    	     	    	     	
  	     	  	ringdistance = ringdistanceNM * 1.1507;
  	     	  	
 				 for (var i = ringdistance ; i < finalRadius_miles; i+=ringdistance) {    	           
   	        	  radius = i;  	        	  
   	        	  
   	           
   	              
   	            var circle = turf.circle(center, radius, options);
 	            	var featureLocal = new ol.Feature({
 						  name: "POLYGON",
 						  geometry: new ol.geom.Polygon(circle.geometry.coordinates),
 						  radius:radius  						  
 					});
 	            	featureLocal.setStyle(rangeRingStyle); 	            	
 	            	sourceCustomPolygon.addFeature(featureLocal);	            	
     	            }   	           
   	           				 
 				if (finalRadius_miles > 0 ){
    	        	
 	    	          for(var j = 0 ;j<360;j+=22.5){          		
 		            	
 		            	 var options3 = {units: 'miles'};
 		       	         var destination = turf.destination(center, finalRadius_miles, j, options3);
 		       	         var lineFeature= new ol.Feature({geometry : new ol.geom.LineString([center, destination.geometry.coordinates ]),start : center , end : destination.geometry.coordinates,angle:j});
 		       	         lineFeature.setStyle(function(lineFeature,resolution) {
 		       	        	var lineLabel="";
 		       	        	if(lineFeature.get("angle")===0) lineLabel="N";
 		       	        	else if(lineFeature.get("angle")===180) lineLabel="S";	
 		       	        	else if(lineFeature.get("angle")===90) lineLabel="E";
 		       	        	else if(lineFeature.get("angle")===270) lineLabel="W";
 		       	        	rangeLineStyle.getText().setText(lineLabel);
 		       	        	return rangeLineStyle;
 		       	         });
 			 
 		       	         sourceCustomPolygon.addFeature(lineFeature);
 		            	}	          
 	    	        }
 					}
				 }
 				  //--------------
 			});		
 		  map.addInteraction(measuringTool);
   		}
   		catch(err)
 		{
 			console.info('error in enabling Ring Range tool: ' + err)
 		}
 	};

     
     
     //-RingRangeEnd-----------
      
     
     var sourceSurpicPolygon = new ol.source.Vector({
 		wrapX: false
 	});
 		
 	var layerSurpicPolygon = new ol.layer.Vector({
 		source: sourceSurpicPolygon,
 		name: "surpicPolygonLayer"
 	});
     map.addLayer(layerSurpicPolygon);
     // SURPIC Ring Interaction   
     function addInteractionSurpicRing() {     	
    	 var outerRingStyle = new ol.style.Style({	    
    		 stroke: new ol.style.Stroke({
                 color: 'blue',
                // lineDash: [4, 4]
               })
    		    });	    	 
    	 
    	 var finalRadius = 0;
    	 var finalRadius_miles = 0;
    	 var finalCenter=[];     	
			map.removeInteraction(measuringTool);
    	 
    	  measuringTool= new ol.interaction.Draw({
    	      source: sourceSurpicPolygon,
    	      type: 'Circle',  
    	      
    	      condition: (e) => {
    	    	  if(coastalSurpicAllowed==='true')
    	    	  {  let coords = e.coordinate
    	    	    let features = vectorSource.getFeaturesAtCoordinate(coords);
    	    	    if (features.length > 0) {
    	    	      return true;
    	    	    } else {
    	    	      return false;
    	    	    }
    	    	  }
    	    	  else
    	    		  return true;
    	    	  },
    	    	  
    	      // maxPoints:2,
    	      // freehand:true,
    	      geometryFunction : function(coordinates, geometry) {
    	          var center = coordinates[0];
    	          var last = coordinates[1];
    	          var dx = center[0] - last[0];
    	          var dy = center[1] - last[1];
    	          var radius = Math.sqrt(dx * dx + dy * dy);
    	          var newCoordinates = [];
    	          var options = {steps: 360, units: 'degrees', options: {}}; // degrees
    	          if(radius >0){
    	        	  var circle = turf.circle(center, radius, options);
    	        	  newCoordinates.push(circle.geometry.coordinates);
    	        	 
    	      		}
    	          finalRadius= radius;
    	          var options2 = {units: 'degrees'};
    	          var destination = turf.destination(center, radius, 0, options2);
    	          
    	          var options = {units: 'miles'};
    	          var distance = turf.distance(center, destination, options);

    	          finalRadius_miles = distance;
    	          finalCenter = center;    	          
    	          
    	          
    	          if (!geometry) {
    	            geometry = new ol.geom.Polygon(newCoordinates);
    	          } else {
    	            geometry.setCoordinates(newCoordinates);
    	          }    	         
    	          return geometry;
    	        }
    	    });  
    	    
    	  measuringTool.on('drawend', function(evt){    		   
    		   
    		   var options = {steps: 360, units: 'miles', options: {}}; // degrees
    	        if (evt.feature.getGeometry().getType() == 'Polygon') {
    	          center = finalCenter; 
    	        
    	        
       		rightClickSurpicCenter = center;
       		   	rightClickSurpicRadius = Math.round(finalRadius_miles/1.15);
       		    rightClickSurpicRectangle = "";
    	          // Outer Circle
    	          var outerCircle =  turf.circle(center, finalRadius_miles, options);
    	         // var outerCircleFeature = new ol.Feature({geometry : new ol.geom.Polygon(outerCircle.geometry.coordinates), radius:finalRadius_miles,areatype:"surpicregion",type:"circle",center:center});
    	         // outerCircleFeature.setStyle(outerRingStyle);     	       
    	          evt.feature.setGeometry(new ol.geom.Polygon(outerCircle.geometry.coordinates));  
    	          evt.feature.setProperties({					    
    					
    						 'radius':finalRadius_miles,'radiusNM':finalRadius_miles/1.15,'areatype':'surpicregion','type':'circle','center':center
    				  })
    				 // evt.feature.setStyle(outerRingStyle);
    	          //sourceCustomPolygon.addFeature(outerCircleFeature);
    	          map.removeInteraction(measuringTool);
    	          //surpicInteractionOnFlag = false;
    	        }
    	      });
    	   
    	    map.addInteraction(measuringTool);    	   
    	  
       }
     
     
     function addInteractionSurpicRectangle() {     	
    	 var outerRingStyle = new ol.style.Style({	    
    		 stroke: new ol.style.Stroke({
                 color: 'blue',
                // lineDash: [4, 4]
               })
    		    });	    	 
    	 
    	 var finalRadius = 0;
    	 var finalRadius_miles = 0;
    	 var finalCenter=[];     	
			map.removeInteraction(measuringTool);
    	 
    	  measuringTool= new ol.interaction.Draw({
    	      source: sourceSurpicPolygon,
    	      type: 'Circle',    
    	      condition: (e) => {
    	    	  if(coastalSurpicAllowed==='true')
    	    	  {  let coords = e.coordinate
    	    	    let features = vectorSource.getFeaturesAtCoordinate(coords);
    	    	    if (features.length > 0) {
    	    	      return true;
    	    	    } else {
    	    	      return false;
    	    	    }
    	    	  }
    	    	  else
    	    		  return true;
    	    	  },
    	    	  
    	      geometryFunction : new ol.interaction.Draw.createBox()
    	    });  
    	    
    	  measuringTool.on('drawend', function(evt){
    		  
    		  var surpicRectangleInfo = evt.feature.getGeometry().getExtent();
    		  
    		  rightClickSurpicCenter = [surpicRectangleInfo[0],surpicRectangleInfo[1]];
    		  rightClickSurpicRectangle = [surpicRectangleInfo[2]-surpicRectangleInfo[0],surpicRectangleInfo[3]-surpicRectangleInfo[1]];
    		  rightClickSurpicRadius = "";
    		  map.removeInteraction(measuringTool);

    		  evt.feature.setProperties({					    
					
					 'areatype':'surpicregion','type':'rectangle'
			  });    	        
    		 // surpicInteractionOnFlag = false;
    	      });
    	   
    	    map.addInteraction(measuringTool);  
    	  
       }

     function surpicRegionRighClickMenu(geometryFeature,elementList){
   		try{
   			currentPolygonFeature = geometryFeature;
   			var innerHtmlStr = "<ul class=\"ulRightClick\" id=\"results\">" +
   			"<li class=\" sarsurpicClass\"> <a href=\"#\">Request Surpic </a> </li>";
   			 			
   			
   			innerHtmlStr = innerHtmlStr +"</ul>";	
   			elementList.innerHTML = innerHtmlStr;
   		}
   		catch(err)
   		{
   			console.info('error in Surpic Region Right Click Menu: ' + err)
   		}
   	}
       
     
     
     
     