	 var extentThreshold = 90;	
	var currentPolygonFeature; 
	/* var vectorSource_Temp = new ol.source.Vector(
	{              
		 wrapX: false,
    	 noWrap: true
    });*/

 	
 
	function createSurpicRequestRightClick(){	
	
			$.ajax({url: "sarsurpicrequest" ,
			    success: function(result){
		    	try{
		           $("#pageModaldialogContent").html(result);               
		           $("#pageModal").modal('show'); 
		           if(rightClickCoodinate){
		        	   $("#rectanglediv").hide();
		        	   $("#circlediv").show();
		        	   rightClickCoodinateLonLat = ol.proj.toLonLat(rightClickCoodinate,'EPSG:4326');			
		        	   var data ={
							lat : rightClickCoodinateLonLat[1],
							lon : rightClickCoodinateLonLat[0]
						}
					
		        	   $('input:radio[name=sarareaType]:nth(1)').attr('checked',true);
		     		   
		     		   $("#latSignCircle").val((data.lat < 0) ? -1 : 1 );
		     		   $("#lonSignCircle").val((data.lon < 0) ? -1 : 1 );
		         		
		     		   [latDeg ,latMin] = LatLonToDegMin(data.lat);  
		     		   [lonDeg ,lonMin] = LatLonToDegMin(data.lon);      		   
		     		  
		     		   $("#latDegCircle").val(latDeg);
		     		   $("#latMinCircle").val(latMin);		            		
		     		   $("#lonDegCircle").val(lonDeg);
		     		   $("#lonMinCircle").val(lonMin);  
		     		   
		     		   //Fill rectangular area form      		   
		     		  $("#latSignLB").val((data.lat < 0) ? -1 : 1 );
		    		  $("#lonSignLB").val((data.lon < 0) ? -1 : 1 );
		        		
		    		  $("#latDegLB").val(latDeg);
		    		  $("#latMinLB").val(latMin);		            		
		    		  $("#lonDegLB").val(lonDeg);
		    		  $("#lonMinLB").val(lonMin);  
		    		  
		    		  document.getElementById("apply").disabled = false;
		           }
	           
			    }
				catch(err)
				{
					console.info('error in create Surpic Request Right Click: ' + err)
				}
	       }			
		});		
		
	}



	
	
	function displayPolygonOnMap(polygonshape ,areatype,vectorsource,sarFlag){
		try
		{
			var polygon;
			vectorSource_Temp.clear();
			//vectorsource.clear();
			 var sarStyle = new ol.style.Style({	    
	    		 stroke: new ol.style.Stroke({
	                 color: 'red',
	                 width: 1
	               })
	    		    });	
			 
			if(polygonshape.polygontype ==="polygon"){ 
				var coords = [];
	    		 for(var j =0 ;j<polygonshape.lon.length;j++){
	    			 coords.push([polygonshape.lon[j],polygonshape.lat[j]])
	    		 }
	    	 	polygon = new ol.geom.Polygon([coords]);	  
	    	 	console.info('polygon coords: ' + coords);
	   	 	}
	   	 	else if(polygonshape.polygontype ==="rectangle")
	   	 	{
	   	 		var coords = [[polygonshape.lon,polygonshape.lat],[polygonshape.lon,polygonshape.latOffset],[polygonshape.longOffset,polygonshape.latOffset],[polygonshape.longOffset,polygonshape.lat],[polygonshape.lon,polygonshape.lat]]
	   	 		polygon = new ol.geom.Polygon([coords]);	
	   	 		console.info('rectangle coords: ' + coords);
	   	 	}
	   	 	else if(polygonshape.polygontype ==="circle"){ 
	   	 	 
	   	 	 // var centerLongitudeLatitude =  ol.proj.transform([polygonshape.lon,polygonshape.lat], 'EPSG:4326', 'EPSG:3857');		 
			  var options = {steps: 360, units: 'miles', options: {}}; //degrees
			  polygonshape.radius = polygonshape.radius * 1.1507;
			  
			  //console.info("center surpic");
	       		// console.info([polygonshape.lon,polygonshape.lat]);
	       		 
	       		   	
	       		
	       		// console.info("radius surpic nm ");
	       		//console.info(polygonshape.radius);
			  var circle = turf.circle([polygonshape.lon,polygonshape.lat], polygonshape.radius, options);
			   polygon = new ol.geom.Polygon(circle.geometry.coordinates);  // 	 		)
			   console.info('circle coords: ' + circle.geometry.coordinates);
	   	 	}
			
			if(polygon){
			//polygon.transform('EPSG:4326', 'EPSG:3857')
				
				/* 
				  var features = vectorsource.getFeatures();
				  console.info(('polygonId'))
				  	features.forEach(function(feature) {	
				  		console.info("for: " + feature.get('polygonId'))
				  		if(feature.get('polygonId')==polygonshape.polygonId)
				  			{				  				
				  				console.info("if: " + polygonshape.polygonId)
				  				vectorsource.removeFeature(feature);
				  			}
				    });	*/
				
				if(currentPolygonFeature!=undefined && currentPolygonFeature!="" )
				{  var featureTemp = currentPolygonFeature;
					if (featureTemp) {
						  sourceCustomPolygon.removeFeature(featureTemp);
					}
				}
				
	    		var feature = new ol.Feature(polygon);
	    		feature.setProperties({areatype: areatype, polygonId : polygonshape.polygonId ,polygonName : polygonshape.polygonName ,polygontype : polygonshape.polygontype });  
	    		feature.setId(Math.floor(Math.random() * 1000));
	    		
	    		if(sarFlag===true)
	    			feature.setStyle(sarStyle);
	    	 	vectorsource.addFeature( feature );
	    	 	vectorSource_Temp.addFeature(feature.clone());
				}
			
			var extent = vectorSource_Temp.getExtent();		
			
			if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
			 { 
	   		 var diffExtent = extent[2]- extent[0];
	   		 if( diffExtent >= 0 && diffExtent < extentThreshold)
	   		 {
	   			 var center = ol.extent.getCenter(extent);							
	   			 map.getView().setCenter(center);
	   		 }
	   		 else
	   			 map.getView().fit(extent, map.getSize());	    		
			 }
		}
		catch(err)
		{
			console.info('error in display polygon on map: ' + err)
		}
	}
	
	
	function convertSignFromNSEWString(input){
		//console.info(input);
		if(input==="1" || input === "-1") return input;
		var ret = 1;
		if (input ==="S" || input ==="W"){
			ret= - 1;
		}		
		return ret;
	}
	
	
	  function applyRectangle(){
    	  try{
	    	  var areaname = $("#areaname").val();
	    	//  console.info([$("#latDegLB").val(),$("#latMinLB").val(),convertSignFromNSEWString($("#latSignLB").val())]); 	    	  
	    	 
	    	  
	    	  var latOutLB= DegMinToLatLon($("#latDegLB").val(),$("#latMinLB").val(),convertSignFromNSEWString($("#latSignLB").val()));
	    	  var lonOutLB= DegMinToLatLon($("#lonDegLB").val(),$("#lonMinLB").val(),convertSignFromNSEWString($("#lonSignLB").val()));
	    	  
	    	  if( document.getElementById("latDegRT").value.length < 1)
	    		  document.getElementById("latDegRT").value = 0;
	    	  if( document.getElementById("latMinRT").value.length < 1)
	    		  document.getElementById("latMinRT").value = 0;
	    	  if( document.getElementById("lonDegRT").value.length < 1)
	    		  document.getElementById("lonDegRT").value = 0;
	    	  if( document.getElementById("lonMinRT").value.length < 1)
	    		  document.getElementById("lonMinRT").value = 0;
	    	  
	    	  var latOutRT= DegMinToLatLon($("#latDegRT").val(),$("#latMinRT").val(),convertSignFromNSEWString($("#latSignRT").val()));
	    	  var lonOutRT= DegMinToLatLon($("#lonDegRT").val(),$("#lonMinRT").val(),convertSignFromNSEWString($("#lonSignRT").val()));
	
	    	  var polygontype ="rectangle";
	          var polygon={
	        		polygonId:null,
	          		polygonName: areaname, 
	          		polygontype: polygontype,
	          		lat : latOutLB,
	          		lon : lonOutLB,
	          		latOffset : latOutRT,
	          		longOffset : lonOutRT,
	          }
	    	//  console.info(JSON.stringify(polygon));
	          return polygon;
    	  }
    	  catch(err)
  		 {
  			console.info('error in apply Rectangle: ' + err)
  		 }
      }
      
      
      function applyCircle(){
    	  try{
    	  var areaname = $("#areaname").val();
    	  
    	//  console.info([$("#latDegCircle").val(),$("#latMinCircle").val(),$("#latSignCircle").val()]); 
    	  
    	  var latOutCircle= DegMinToLatLon($("#latDegCircle").val(),$("#latMinCircle").val(),convertSignFromNSEWString($("#latSignCircle").val()));
    	  var lonOutCircle= DegMinToLatLon($("#lonDegCircle").val(),$("#lonMinCircle").val(),convertSignFromNSEWString($("#lonSignCircle").val()));
    	  var radius = $("#radius").val();
    	  //console.info([latOutCircle, lonOutCircle]);
    	  if( document.getElementById("radius").value.length < 1)
    		  document.getElementById("radius").value = 0;
    	 
    	  var polygontype ="circle";
          var polygon={
        		polygonId:null,
          		polygonName: areaname, 
          		polygontype: polygontype,
          		lat : latOutCircle,
          		lon : lonOutCircle,
          		radius : radius
          		
          }
         // console.info('inside apply circle');
    	 // console.info(JSON.stringify(polygon));
          return polygon;
    	  }
    	  catch(err)
    	  {
  			console.info('error in apply Circle: ' + err)
    	  }
      }
      
      function checkLatValue(LatValue)
      {
    	  try{
	    	  if(LatValue.length==1)
	    	   LatValue = '0' + LatValue;
	    	  
	    	  return LatValue;
    	  }
    	  catch(err)
    	  {
  			console.info('error in check Lat Value: ' + err)
    	  }
      }
      
      function checkLonValue(LonValue)
      {
    	  try{
	    	  if(LonValue.length==1)
	    		  LonValue = '00' + LonValue;
	    	  else if(LonValue.length==2)
	    		  LonValue = '0' + LonValue; 
	    	  
	    	  return LonValue;
    	  }
    	  catch(err)
    	  {
  			console.info('error in check Lon Value: ' + err)
    	  }
      }
      
      function checkLatSign(signValue)
      {
    	  try{
	    	  var signReturn;
	    	  if(signValue==1)
	    		  signReturn = 'N';
	    	  else 
	    		  signReturn = 'S'; 
	    	  
	    	  return signReturn;
    	  }
    	  catch(err)
    	  {
  			console.info('error in check Lat Sign: ' + err)
    	  }
      }
      function checkLonSign(signValue)
      {
    	  try{
	    	  var signReturn;
	    	  if(signValue==1)
	    		  signReturn = 'E';
	    	  else 
	    		  signReturn = 'W'; 
	    	  
	    	  return signReturn;
    	  }
    	  catch(err)
    	  {
  			console.info('error in check Lon Sign: ' + err)
    	  }
      }
      
      function applySarCircle(sarType){
    	//  try{
	    	  var areaname = $("#areaname").val();
	    	  
	    	 // console.info([$("#latDegCircle").val(),$("#latMinCircle").val(),$("#latSignCircle").val()]); 
	    	  var circularAreaValues = '';
	    	  var rectangularAreaValues ='';
	    	  if(sarType =='circle')
	    	  {  var latDegCircle = checkLatValue($("#latDegCircle").val());
		    	  var latMinCircle = checkLatValue($("#latMinCircle").val());
		    	  var latSignCircle = checkLatSign($("#latSignCircle").val());
		    	  var lonDegCircle = checkLonValue($("#lonDegCircle").val());
		    	  var lonMinCircle = checkLatValue($("#lonMinCircle").val());
		    	  var lonSignCircle = checkLonSign($("#lonSignCircle").val());
		    	  var radius = $("#radius").val();
		    	  circularAreaValues = latDegCircle + '.' + latMinCircle + '.' + latSignCircle +':'+lonDegCircle+'.' + lonMinCircle + '.'+ lonSignCircle +':'+radius;
	    	 
	    	  }
	    	  else{ 
	    	  var latDegRectangle = checkLatValue($("#latDegLB").val());
	    	  var latMinRectangle = checkLatValue($("#latMinLB").val());
	    	  var latSignRectangle = checkLatSign($("#latSignLB").val());    	  
	    	  var lonDegRectangle = checkLonValue($("#lonDegLB").val());
	    	  var lonMinRectangle = checkLatValue($("#lonMinLB").val());
	    	  var lonSignRectangle = checkLonSign($("#lonSignLB").val());
	    	  
	    	  var latDegRectangleOffset = checkLatValue($("#latDegRT").val());
	    	  var latMinRectangleOffset = checkLatValue($("#latMinRT").val());
	    	  var latSignRectangleOffset = checkLatSign($("#latSignRT").val());    	  
	    	  var lonDegRectangleOffset = checkLonValue($("#lonDegRT").val());
	    	  var lonMinRectangleOffset = checkLatValue($("#lonMinRT").val());
	    	  var lonSignRectangleOffset = checkLonSign($("#lonSignRT").val());
	    	   rectangularAreaValues = latDegRectangle + '.' + latMinRectangle + '.' + latSignRectangle +':'+lonDegRectangle+'.' + lonMinRectangle + '.'+ lonSignRectangle +':'+
	     	 	latDegRectangleOffset + '.' + latMinRectangleOffset + '.' + latSignRectangleOffset +':'+lonDegRectangleOffset+'.' + lonMinRectangleOffset + '.'+ lonSignRectangleOffset;
	     	 
	    	  }
	    	  
	          var polygon={
	        		  circular_area: circularAreaValues ,
	        		  rectangular_area: rectangularAreaValues, 
	        		  ship_type: $("#shipType").val(),
	        		  data_user_requestor :  $("#countrySurpicRequestList").val(), 
	        		  no_of_positions : $("#numberOfPositions").val(),          		
	          		
	          }
	         /// console.info('inside applySarCircle circle');
	    	 // console.info(JSON.stringify(polygon));
	          return polygon;
	          /* }
      
		  catch(err)
		  {
				console.info('error in apply Sar Circle: ' + err)
		  }*/
      }
      
      function LatLonToDegMin(val){
    	  try{
	    	  var valAbs = Math.abs(val)
	    	  var dlatsplit = valAbs.toString().split('.');
	    	  var min =Math.round( parseFloat("0."+dlatsplit[1]) * 60); 
	    	 /* console.info(val);
	    	  console.info(dlatsplit);
	    	  console.info(min);*/
	    	  return [dlatsplit[0] , min];
    	  }
    	  catch(err)
    	  {
			console.info('error in LatLonToDegMin: ' + err)
    	  }
      }
      
      function DegMinToLatLon(deg , min , sign){    	
    	  
    	  return (parseInt(deg) + (min/60))*sign;
      }
     