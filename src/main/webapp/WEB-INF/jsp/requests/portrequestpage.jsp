<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Port Request</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css" var="a" />
<link rel="stylesheet" href="${a}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css" var="b" />
<link rel="stylesheet" href="${b}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="c" />
<link rel="stylesheet" href="${c}">
<!-- daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="d" />
<link rel="stylesheet" href="${d}">

<!-- bootstrap datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="e" />
<link rel="stylesheet" href="${e}">

<!-- iCheck for checkboxes and radio inputs -->
<spring:url value="/plugins/iCheck/all.css" var="f" />
<link rel="stylesheet" href="${f}">

<!-- Bootstrap Color Picker -->
<spring:url
	value="/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css"
	var="g" />
<link rel="stylesheet" href="${g}">

<!-- Bootstrap time Picker -->
<spring:url value="/plugins/timepicker/bootstrap-timepicker.min.css"
	var="h" />
<link rel="stylesheet" href="${h}">

<!-- Select2 -->
<spring:url value="/bower_components/select2/dist/css/select2.min.css"
	var="i" />
<link rel="stylesheet" href="${i}">

<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="j" />
<link rel="stylesheet" href="${j}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css" var="k" />
<link rel="stylesheet" href="${k}">

<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<spring:url value="/requests/persistportrequest"
	var="persistportrequest" />
<spring:url value="/requests/getshipdetailsflagfromdc"
	var="getshipdetailsflagfromdc" />
<spring:url value="/requests/getshipdetailsnonflagfromdc"
	var="getshipdetailsnonflagfromdc" />
<spring:url value="/requests/getreportingrate" var="getreportingrate" />
<spring:url value="/requests/getportdetails" var="getportdetails" />
<spring:url value="/requests/getportfacility" var="getportfacility" />


<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js" var="l" />
<script src="${l}"></script>
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>

<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="m" />
<script src="${m}"></script>

<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>

<!-- Select2 -->
<spring:url
	value="/bower_components/select2/dist/js/select2.full.min.js" var="n" />
<script src="${n}"></script>

<!-- date-range-picker -->
<spring:url value="/bower_components/moment/min/moment.min.js" var="r" />
<script src="${r}"></script>

<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="s" />
<script src="${s}"></script>
<!-- bootstrap datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="t" />
<script src="${t}"></script>

<!-- bootstrap color picker -->
<spring:url
	value="/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"
	var="u" />
<script src="${u}"></script>

<!-- bootstrap time picker -->
<spring:url value="/plugins/timepicker/bootstrap-timepicker.min.js"
	var="v" />
<script src="${v}"></script>

<!-- SlimScroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="w" />
<script src="${w}"></script>

<!-- iCheck 1.0.1 -->
<spring:url value="/plugins/iCheck/icheck.min.js" var="x" />
<script src="${x}"></script>

<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js" var="y" />
<script src="${y}"></script>

<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="z" />
<script src="${z}"></script>

<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>

<!-- other js -->
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- other js end -->
<script type="text/javascript">
	$(document).ready(function() {

		$('#start').daterangepicker({
			minDate : new Date(),
			timePicker : true,
			timePicker24Hour : true,
			locale : {
				format : 'MM/DD/YYYY HH:mm:ss'
			},
			singleDatePicker : true
		}

		);

		$('#end').daterangepicker({
			minDate : new Date(),
			timePicker : true,
			timePicker24Hour : true,
			locale : {
				format : 'MM/DD/YYYY HH:mm:ss'
			},
			singleDatePicker : true
		});

		$("input[type='radio'][name='portradio']").click(function() {

			if ($(this).is(':checked')) {
				var radval = $(this).val();
				if (radval == 1) {
					document.getElementById('portfacilitycodeinput').value="";
					document.getElementById('portfacilitynameinput').value="";
					$('#ports').show();
					$('#portfacilitydiv').hide();
					document.getElementById("portcodeinput").required = true;
					document.getElementById("portnameinput").required = true;
					document.getElementById("portfacilitycodeinput").removeAttribute("required");
					document.getElementById("portfacilitynameinput").removeAttribute("required");

				}
				if (radval == 2) {
					document.getElementById('portcodeinput').value="";
					document.getElementById('portnameinput').value="";
					$('#ports').hide();
					$('#portfacilitydiv').show();
					document.getElementById("portfacilitycodeinput").required = true;
					document.getElementById("portfacilitynameinput").required = true;
					document.getElementById("portcodeinput").removeAttribute("required");
					document.getElementById("portnameinput").removeAttribute("required");
					

				}
			}
		});

		var ref = window.opener.rightClickShipData;
		var refType = window.opener.rightClickShipRequestType;
		if (ref) {
			$('#imono').val(ref.imo_no);
			$('#mmsino').val(ref.mmsi_no);
			$('#vesselName').val(ref.ship_name);
			$('#usercountry').val(ref.data_user_provider);

		}

	});

	function ShowHideDivTime() {

		if (($('#timeinput').val() == '1') || ($('#timeinput').val() == '9')
				|| ($('#timeinput').val() == '0')) {
			$('#starttime').hide();
			$('#endtime').hide();
		} else {
			$('#starttime').show();
			$('#endtime').show();

		}
	}

	function ShowHideDivDistance() {

		if (($('#distanceinput').val() == '0')) {
			$('#starttime').hide();
			$('#endtime').hide();
		} else {
			$('#starttime').show();
			$('#endtime').show();

		}

	}
	function ShowRequesttypetime() {

		if (($('#accesstypeinput').val() == '3')) {
			$('#requesttypeinputfortime').hide();
			$('#requesttypeinputfordistance').show();
			$('#distance').show();
			document.getElementById("distance").required = true;
			

		} else if ($('#accesstypeinput').val() == '5') {
			$('#requesttypeinputfordistance').hide();
			$('#requesttypeinputfortime').show();
			$('#distance').hide();
			document.getElementById('distance').value="";
			document.getElementById("distance").removeAttribute("required");
			
		}

		else if ($('#accesstypeinput').val() == '0') {
			$('#requesttypeinputfordistance').hide();
			$('#requesttypeinputfortime').hide();
			$('#distance').hide();
		}
	}
	//this function shows flag and non flag vessel after clicking on imo button
	function showTable() {

		var radVal = document.portrequest.vessel.value;
		var urlvessel;
		if (radVal == 1) {

			urlvessel = "${getshipdetailsflagfromdc}";
		}

		if (radVal == 2) {
			urlvessel = "${getshipdetailsnonflagfromdc}";
		}

		var table = $('#Shipdetail').DataTable({
			dom : 'lfrtip',
			"bDestroy" : true,
			"ajax" : {
				"url" : urlvessel,
				"type" : "GET",
				"dataSrc" : ""
			},

			"columnDefs" : [ {
				"orderable" : false,
				"className" : 'select-checkbox',
				"targets" : 0,
				"defaultContent" : ""
			} ],
			"select" : {
				"style" : 'os',
				"selector" : 'td:first-child'
			},
			"order" : [ [ 1, 'asc' ] ],

			"columns" : [ {
				"data" : ""
			}, {
				"data" : "imoNo"
			}, {
				"data" : "vesselName"
			}, {
				"data" : "mmsiNo"
			}, {
				"data" : "dataUserProvider"
			} ]

		});
		$('#select').on('click', function() {

			var imo, mmsi, vessel;
			$.each(table.rows('.selected').data(), function() {
				imo = this["imoNo"];
				mmsi = this["mmsiNo"];
				vessel = this["vesselName"];
				country = this["dataUserProvider"];
				$('#imono').val(imo);
				$('#mmsino').val(mmsi);
				$('#vesselName').val(vessel);
				$('#usercountry').val(country);
			});
		});
	}

	function showReportingTable(imo) {
		var imo = document.getElementById('imono').value;

		//alert("imo2"+imo);
		var table = $('#Reportingrate').DataTable({
			dom : 'lfrtip',
			"bDestroy" : true,
			"ajax" : {
				"url" : "${getreportingrate}",
				"type" : "GET",
				"dataSrc" : "",
				"dataType" : "json",
				"data" : {
					'imo' : imo
				},

			},

			"columnDefs" : [ {
				"orderable" : false,
				"className" : 'select-checkbox',
				"targets" : 0,
				"defaultContent" : ""
			} ],

			"columns" : [ {
				"data" : "accessType"
			}, {
				"data" : "requestType"
			}, {
				"data" : "reqDurationStart"
			}, {
				"data" : "reqDurationStop"
			} ]

		});

	}

	//port
	function showPortTable() {

		var table = $('#portdetailtable').DataTable({
			//dom: 'lBrtip',
			dom : 'lfrtip',
			"bDestroy" : true,
			"ajax" : {
				"url" : "${getportdetails}",
				"type" : "GET",
				"dataSrc" : ""
			//"dataType": "json"
			},

			"columnDefs" : [ {
				"orderable" : false,
				"className" : 'select-checkbox',
				"targets" : 0,
				"defaultContent" : ""
			} ],
			"select" : {
				"style" : 'os',
				"selector" : 'td:first-child'
			},
			"order" : [ [ 1, 'asc' ] ],
			"columns" : [ {
				"data" : ""
			}, {
				"data" : "portid.locode"
			}, {
				"data" : "name"
			} ]

		});
		$('#portselect').on('click', function() {

			var portcode, name;
			$.each(table.rows('.selected').data(), function() {
				portcode = this.portid.locode;
				name = this.name;

				// alert("imo"+imo+"mmsi"+mmsi+"ship"+ship);

				$('#portcodeinput').val(portcode);
				$('#portnameinput').val(name);

			});
		});
	}

	//portfacilty
	function showPortFacilityTable() {

		var table = $('#portfacilitydetailtable').DataTable({
			//dom: 'lBrtip',
			dom : 'lfrtip',
			"bDestroy" : true,
			"ajax" : {
				"url" : "${getportfacility}",
				"type" : "GET",
				"dataSrc" : ""
			},

			"columnDefs" : [ {
				"orderable" : false,
				"className" : 'select-checkbox',
				"targets" : 0,
				"defaultContent" : ""
			} ],
			"select" : {
				"style" : 'os',
				"selector" : 'td:first-child'
			},
			"order" : [ [ 1, 'asc' ] ],
			"columns" : [ {
				"data" : ""
			}, {
				"data" : "id.imoportfacilitynumber"
			}, {
				"data" : "name"
			} ]

		});
		$('#portfacilitiesselect').on('click', function() {

			var portcode, name, portId;
			$.each(table.rows('.selected').data(), function() {
				portcode = this.id.imoportfacilitynumber;

				name = this.name;
				$('#portfacilitycodeinput').val(portcode);
				$('#portfacilitynameinput').val(name);

			});
		});
	}
</script>

<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">

							<div class="box-header with-border">
								<h3 class="box-title">
									<b>Port Request</b>
								</h3>
							</div>

							<!-- /.box-header -->
							<!-- form start -->
							<form:form id="portrequest" action="${persistportrequest}"
								method="post" name="portrequest"
								modelAttribute="portalportrequest">
								<div class="box-body">

									<div class="row">
										<div id="vesselradio" class="form-group  col-sm-6">
											<label for="vessel" class="col-sm-4 control-label">Vessel</label>
											<div class="col-sm-8">
												<label><input type="radio" name="vessel" id="flag"
													value="1" required="true">Flag Vessels</label>
												<h>&nbsp;&nbsp;&nbsp;&nbsp;</h>
												<label><input type="radio" name="vessel"
													id="nonflag" value="2" required="true">Non Flag
													Vessels</label>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">

											<label for="imono" class="col-sm-4 control-label">IMO
												NO</label>
											<div class="col-sm-8">
												<div class="input-group">
													<form:input type="text" path="imoNo"
														class="form-control col-sm-8" id="imono" name="imono"
														maxlength="7" size="7" autocomplete="off" pattern="\d{7}"
														title="Please Enter Digit." required="true" />
													<span class="input-group-btn">
														<button class="form-control" type="button"
															data-toggle="modal" data-target="#myModal"
															onclick="showTable()">
															<i class=" fa fa-search"></i>
														</button>
													</span>
												</div>
											</div>
										</div>
									</div>

									<!-- Modal -->

									<div class="modal fade" id="myModal" role="dialog">
										<div class="modal-dialog" style="width: 95%">

											<div class="modal-content">
												<div class="modal-header">
													<h4>Ship Details</h4>
												</div>
												<div class="modal-body">
													<div class="box-body">
														<table id="Shipdetail"
															class="table table-bordered table-striped "
															style="width: 100%">
															<thead>
																<tr>
																	<th></th>
																	<th data-data="imoNo" data-default-content="NA">IMO
																		Number</th>
																	<th data-data="vesselName" data-default-content="NA">Ship
																		Name</th>
																	<th data-data="mmsiNo" data-default-content="NA">MMSI
																		Number</th>
																	<th data-data="dataUserProvider"
																		data-default-content="NA">User Country</th>
																</tr>
															</thead>
														</table>
													</div>

												</div>
												<div class="modal-footer">
													<button id="select" type="button" class="btn btn-default"
														data-dismiss="modal">Select</button>
													<button type="button" class="btn btn-default"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">


											<label for="mmsino" class="col-sm-4 control-label">MMSI
												NO</label>
											<div class="col-sm-8">
												<form:input type="text" path="mmsiNo" class="form-control"
													id="mmsino" maxlength="9" size="9" name="mmsino"
													autocomplete="off" pattern="\d{9}"
													title="Please Enter Digit." required="true" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">
											<label for="shipname" class="col-sm-4 control-label">Ship
												Name</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="vesselName"
													path="vesselName" name="vesselName" maxlength="50"
													autocomplete="off" required="true" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">
											<label for="usercountry" class="col-sm-4 control-label">User
												Country</label>
											<div class="col-sm-8">
												<form:input type="text" path="userProvider"
													class="form-control" id="usercountry" name="usercountry"
													maxlength="4" autocomplete="off" required="true" />
											</div>

										</div>
									</div>


									<div class="row">
										<div class="form-group col-sm-6">


											<label for="reportingrate" class="col-sm-4">View
												Reporting Rate(Active Request)</label>
											<div class="col-sm-8">
												<button type="button"
													class="form-control col-sm-1 btn btn-primary"
													id="reportingrate" name="reportingrate" data-toggle="modal"
													data-target="#reportingrateModal"
													onclick="showReportingTable()">View Reporting Rate</button>
											</div>
										</div>
									</div>


									<!-- Modal -->

									<div class="modal fade" id="reportingrateModal" role="dialog">
										<div class="modal-dialog" style="width: 95%">

											<div class="modal-content">
												<div class="modal-header">
													<h4>View Reporting Rate(Active Request)</h4>
												</div>
												<div class="modal-body">
													<div class="box-body">
														<table id="Reportingrate"
															class="table table-bordered table-striped "
															style="width: 100%">
															<thead>
																<tr>
																	<th data-data="accessType" data-default-content="NA">Access
																		Type</th>
																	<th data-data="requestType" data-default-content="NA">RequestType</th>
																	<th data-data="reqDurationStart"
																		data-default-content="NA">Duration Start-Time</th>
																	<th data-data="reqDurationStop"
																		data-default-content="NA">Duration Stop-Time</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>


									<div class="row">
										<div class="form-group col-sm-6">
											<label for="accesstype" class="col-sm-4 control-label">Access
												Type</label>
											<div class="form-group col-sm-8">
												<form:select path="accessType" class="form-control"
													id="accesstypeinput" onchange="ShowRequesttypetime()">
													<form:option value="0">Select</form:option>
													<form:option value="3">3 - Port with Distance trigger with Port/Port Facility</form:option>
													<form:option value="5">5 - Port With time Trigger</form:option>
												</form:select>
											</div>
										</div>
									</div>


									<div class="row" id="requesttypeinputfortime"
										style="display: none">
										<div class="form-group col-sm-6">
											<label for="requesttypefortime"
												class="col-sm-4 control-label">Request Type</label>
											<div class="form-group col-sm-8">
												<form:select path="requestType" class="form-control"
													id="timeinput" onchange="ShowHideDivTime()">
													<form:option value="0">Select</form:option>
													<form:option value="1">1 - One Time Poll of Ship</form:option>
													<form:option value="2">2 - 15 Minute periodic rate</form:option>
													<form:option value="3">3 - 30 Minute periodic rate</form:option>
													<form:option value="4">4 - 1 Hour periodic rate</form:option>
													<form:option value="5">5 - 3 Hour periodic rate</form:option>
													<form:option value="6">6 - 6 Hour periodic rate</form:option>
													<form:option value="7">7 - Archived LRIT information request </form:option>
													<form:option value="8">8 - stop/Don't Start Sending Position reports</form:option>
													<form:option value="9">9 - Most recent position report</form:option>
													<form:option value="10">10 - 12 Hour periodic rate</form:option>
													<form:option value="11">11 - 24 Hour periodic rate</form:option>
												</form:select>
											</div>
										</div>
									</div>


									<div class="row" id="requesttypeinputfordistance"
										style="display: none">
										<div class="form-group col-sm-6">

											<label for="requesttypefordistance"
												class="col-sm-4 control-label">Request Type</label>
											<div class="form-group col-sm-8">
												<form:select path="requesttypedistance" class="form-control"
													id="distanceinput" onchange="ShowHideDivDistance()">
													<form:option value="0">Select</form:option>
													<form:option value="2">2 - 15 Minute periodic rate</form:option>
													<form:option value="3">3 - 30 Minute periodic rate</form:option>
													<form:option value="4">4 - 1 Hour periodic rate</form:option>
													<form:option value="5">5 - 3 Hour periodic rate</form:option>
													<form:option value="6">6 - 6 Hour periodic rate</form:option>
													<form:option value="8">8 - stop/Don't Start Sending Position reports</form:option>
													<form:option value="10">10 - 12 Hour periodic rate</form:option>
													<form:option value="11">11 - 24 Hour periodic rate</form:option>
												</form:select>
											</div>
										</div>
									</div>



									<div class="row">
										<div id="portradio" class="form-group  col-sm-6">
											<label for="portnfacility" class="col-sm-4 control-label">Port/Port
												Facility</label>
											<div class="col-sm-8">
												<label><input type="radio" name="portradio"
													id="port" value="1" required="true">Port</label>
												<h>&nbsp;&nbsp;&nbsp;&nbsp;</h>
												<label><input type="radio" name="portradio"
													id="portfacility" value="2" required="true">Port
													Facility</label>
											</div>
										</div>
									</div>

									<div id="ports" style="display: none">
										<div class="row">
											<div class="form-group col-sm-6">

												<label for="portcode" class="col-sm-4 control-label">Port
													Code</label>
												<div class="col-sm-8">
													<div class="input-group">
														<form:input type="text" path="portCode"
															class="form-control col-sm-8" id="portcodeinput"
															name="portcode" autocomplete="off" />
														<span class="input-group-btn">
															<button class="form-control" type="button"
																data-toggle="modal" data-target="#porttable"
																onclick="showPortTable()">
																<i class=" fa fa-search"></i>
															</button>
														</span>
													</div>
												</div>
											</div>
										</div>

										<div class="modal fade" id="porttable" role="dialog">
											<div class="modal-dialog" style="width: 95%">

												<div class="modal-content">
													<div class="modal-header">
														<h4>Port Details</h4>
													</div>
													<div class="modal-body">
														<div class="box-body">
															<table id="portdetailtable"
																class="table table-bordered table-striped "
																style="width: 100%">
																<thead>
																	<tr>
																		<th></th>
																		<th data-data="portid.locode"
																			data-default-content="NA">Port Code</th>
																		<th data-data="name" data-default-content="NA">Port
																			Name</th>

																	</tr>
																</thead>
															</table>
														</div>

													</div>
													<div class="modal-footer">
														<button id="portselect" type="button"
															class="btn btn-default" data-dismiss="modal">Select</button>
														<button type="button" class="btn btn-default"
															data-dismiss="modal">Close</button>
													</div>
												</div>
											</div>
										</div>


										<div class="row">
											<div id="portname" class="form-group  col-sm-6">
												<label for="portname" class="col-sm-4 control-label">Port
													Name</label>
												<div class="col-sm-8">
													<form:input path="" type="text" id="portnameinput"
														class="form-control" autocomplete="off" />
												</div>
											</div>
										</div>

									</div>



									<!-- port facility -->

									<div id="portfacilitydiv" style="display: none">
										<div class="row">
											<div class="form-group col-sm-6">

												<label for="portfacilitycode" class="col-sm-4 control-label">Port
													Facility Code</label>
												<div class="col-sm-8">
													<div class="input-group">
														<form:input type="text" path="portfacilityCode"
															class="form-control col-sm-8" id="portfacilitycodeinput"
															name="portfacilitycode" autocomplete="off" />
														<span class="input-group-btn">
															<button class="form-control" type="button"
																data-toggle="modal" data-target="#portfacilitytable"
																onclick="showPortFacilityTable()">
																<i class=" fa fa-search"></i>
															</button>
														</span>
													</div>
												</div>
											</div>
										</div>

										<div class="modal fade" id="portfacilitytable" role="dialog">
											<div class="modal-dialog" style="width: 95%">

												<div class="modal-content">
													<div class="modal-header">
														<h4>Port Facility Details</h4>
													</div>
													<div class="modal-body">
														<div class="box-body">
															<table id="portfacilitydetailtable"
																class="table table-bordered table-striped "
																style="width: 100%">
																<thead>
																	<tr>
																		<th></th>
																		<th data-data="id.imoportfacilitynumber"
																			data-default-content="NA">Port Facility Code</th>
																		<th data-data="name" data-default-content="NA">Port
																			Facility Name</th>

																	</tr>
																</thead>
															</table>
														</div>

													</div>
													<div class="modal-footer">
														<button id="portfacilitiesselect" type="button"
															class="btn btn-default" data-dismiss="modal">Select</button>
														<button type="button" class="btn btn-default"
															data-dismiss="modal">Close</button>
													</div>
												</div>
											</div>
										</div>


										<div class="row">
											<div id="portfacilityname" class="form-group  col-sm-6">
												<label for="portfacilityname" class="col-sm-4 control-label">Port
													Facility Name</label>
												<div class="col-sm-8">
													<form:input path="" type="text" id="portfacilitynameinput"
														class="form-control" autocomplete="off" />
												</div>
											</div>
										</div>

									</div>




									<div class="row">
										<div id="starttime" style="display: none"
											class="form-group col-sm-6">
											<label for="starttimedate" class="col-sm-4 control-label">Start
												Time</label>
											<div class="col-sm-8">
												<div class="input-group date">
													<div class="input-group-addon ">
														<i class="fa fa-calendar"></i>
													</div>
													<form:input path="startTime" type="text" id="start"
														class="form-control pull-right" />
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div id="endtime" style="display: none"
											class="form-group col-sm-6">
											<label for="endtimedate" class="col-sm-4 control-label">End
												Time</label>
											<div class="col-sm-8">
												<div class="input-group date">
													<div class="input-group-addon ">
														<i class="fa fa-calendar"></i>
													</div>
													<form:input path="endTime" type="text" id="end"
														class="form-control pull-right" />
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div id="distance" style="display: none"
											class="form-group  col-sm-6">
											<label for="distance" class="col-sm-4 control-label">Distance(Nautical
												Mile)</label>
											<div class="col-sm-8">
												<form:input path="distance" type="text" id="distance"
													class="form-control" autocomplete="off" />
											</div>
										</div>
									</div>

									<div>
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />
									</div>
								</div>
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" onclick="">Submit</button>
								</div>
							</form:form>

							<!-- /.form -->
						</div>

					</div>

					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<div class="control-sidebar-bg"></div>
	<!-- </div> -->
	<!-- ./wrapper -->



</body>
</html>
