<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Flag Request</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css" var="a" />
<link rel="stylesheet" href="${a}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css" var="b" />
<link rel="stylesheet" href="${b}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="c" />
<link rel="stylesheet" href="${c}">
<!-- daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="d" />
<link rel="stylesheet" href="${d}">

<!-- bootstrap datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="e" />
<link rel="stylesheet" href="${e}">

<!-- iCheck for checkboxes and radio inputs -->
<spring:url value="/plugins/iCheck/all.css" var="f" />
<link rel="stylesheet" href="${f}">

<!-- Bootstrap Color Picker -->
<spring:url
	value="/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css"
	var="g" />
<link rel="stylesheet" href="${g}">

<!-- Bootstrap time Picker -->
<spring:url value="/plugins/timepicker/bootstrap-timepicker.min.css"
	var="h" />
<link rel="stylesheet" href="${h}">

<!-- Select2 -->
<spring:url value="/bower_components/select2/dist/css/select2.min.css"
	var="i" />
<link rel="stylesheet" href="${i}">

<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="j" />
<link rel="stylesheet" href="${j}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css" var="k" />
<link rel="stylesheet" href="${k}">

<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">


<spring:url value="/requests/persistflagrequest"
	var="persistflagrequest" />
<spring:url value="/requests/getshipdetails" var="getshipdetails" />

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">

							<div class="box-header with-border">
								<h3 class="box-title">
									<b>Flag Request</b>
								</h3>
							</div>

							<!-- /.box-header -->
							<!-- form start -->
							<form:form id="flagrequest" action="${persistflagrequest}"
								method="post" name="flagrequest"
								modelAttribute="portalflagrequest">

								<div class="box-body">

									<div class="row">
										<div class="form-group col-sm-6">

											<label for="imono" class="col-sm-4 control-label">IMO
												NO</label>
											<div class="col-sm-8">
												<div class="input-group">
													<form:input type="text" path="imono"
														class="form-control col-sm-8" id="imono" name="imono"
														maxlength="7" size="7" autocomplete="off" pattern="\d{7}"
														title="Please Enter Digit." required="true" />
													<span class="input-group-btn">
														<button class="form-control" type="button"
															data-toggle="modal" data-target="#myModal"
															onclick="showTable()">
															<i class=" fa fa-search"></i>
														</button>
													</span>
												</div>
											</div>
										</div>
									</div>

									<!-- Modal -->

									<div class="modal fade" id="myModal" role="dialog">
										<div class="modal-dialog" style="width: 95%">

											<div class="modal-content">
												<div class="modal-header">
													<h4>Ship Details</h4>
												</div>
												<div class="modal-body">
													<div class="box-body">
														<table id="Shipdetail"
															class="table table-bordered table-striped "
															style="width: 100%">
															<thead>
																<tr>
																	<th></th>
																	<th data-data="imoNo" data-default-content="NA">IMO
																		Number</th>
																	<th data-data="vesselName" data-default-content="NA">Ship
																		Name</th>
																	<th data-data="mmsiNo" data-default-content="NA">MMSI
																		Number</th>
																</tr>
															</thead>
														</table>
													</div>

												</div>
												<div class="modal-footer">
													<button id="select" type="button" class="btn btn-default"
														data-dismiss="modal">Select</button>
													<button type="button" class="btn btn-default"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">


											<label for="mmsino" class="col-sm-4 control-label">MMSI
												NO</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="mmsino"
													maxlength="9" size="9" name="mmsino" autocomplete="off"
													pattern="\d{9}" title="Please Enter Digit." required="true" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">
											<label for="shipname" class="col-sm-4 control-label">Ship
												Name</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="shipname"
													name="shipname" maxlength="50" autocomplete="off"
													required="true" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">
											<label for="requesttype" class="col-sm-4 control-label">Request
												Type</label>
											<div class="form-group col-sm-8">
												<form:select path="requestType" class="form-control"
													id="requesttypeinput" onchange="ShowHideDiv()"
													required="true">
													<form:option value="1">1 - One Time Poll of Ship</form:option>
													<form:option value="2">2 - 15 Minute periodic rate</form:option>
													<form:option value="3">3 - 30 Minute periodic rate</form:option>
													<form:option value="4">4 - 1 Hour periodic rate</form:option>
													<form:option value="5">5 - 3 Hour periodic rate</form:option>
													<form:option value="6">6 - 6 Hour periodic rate</form:option>
													<form:option value="7">7 - Archived LRIT information request (From DB)</form:option>
													<form:option value="8">8 - stop/Don't Start Sending Position reports</form:option>
													<form:option value="9">9 - Most recent position report (From DB)</form:option>
													<form:option value="10">10 - 12 Hour periodic rate</form:option>
													<form:option value="11">11 - 24 Hour periodic rate</form:option>
												</form:select>
											</div>
										</div>
									</div>
									<div class="row">
										<div id="starttime" style="display: none"
											class="form-group col-sm-6">
											<label for="starttimedate" class="col-sm-4 control-label">Start
												Time</label>
											<div class="col-sm-8">
												<div class="input-group date">
													<div class="input-group-addon ">
														<i class="fa fa-calendar"></i>
													</div>
													<form:input path="startTime" type="text" id="start"
														class="form-control pull-right" />
												</div>
											</div>
										</div>

									</div>

									<div class="row">
										<div id="endtime" style="display: none"
											class="form-group col-sm-6">
											<label for="endtimedate" class="col-sm-4 control-label">End
												Time</label>
											<div class="col-sm-8">
												<div class="input-group date">
													<div class="input-group-addon ">
														<i class="fa fa-calendar"></i>
													</div>
													<form:input path="endTime" type="text" id="end"
														class="form-control pull-right" />
												</div>
											</div>
										</div>
									</div>
									<div>
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />
									</div>
								</div>
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" onclick="">Submit</button>
								</div>
							</form:form>

							<!-- /.form -->
						</div>
					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
			</section>
		</div>

		<!-- /.content -->
	</div>
	<jsp:include page="../footer.jsp"></jsp:include>

	<div class="control-sidebar-bg"></div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<spring:url value="/bower_components/jquery/dist/jquery.min.js" var="l" />
	<script src="${l}"></script>
	<!-- jQuery 3 -->
	<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
		var="jqueryuiminjs" />
	<script src="${jqueryuiminjs}"></script>

	<!-- Bootstrap 3.3.7 -->
	<spring:url
		value="/bower_components/bootstrap/dist/js/bootstrap.min.js" var="m" />
	<script src="${m}"></script>

	<spring:url
		value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
		var="dtbootstrapminjs" />
	<script src="${dtbootstrapminjs}"></script>

	<!-- Select2 -->
	<spring:url
		value="/bower_components/select2/dist/js/select2.full.min.js" var="n" />
	<script src="${n}"></script>

	<!-- date-range-picker -->
	<spring:url value="/bower_components/moment/min/moment.min.js" var="r" />
	<script src="${r}"></script>

	<spring:url
		value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
		var="s" />
	<script src="${s}"></script>
	<!-- bootstrap datepicker -->
	<spring:url
		value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
		var="t" />
	<script src="${t}"></script>

	<!-- bootstrap color picker -->
	<spring:url
		value="/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"
		var="u" />
	<script src="${u}"></script>

	<!-- bootstrap time picker -->
	<spring:url value="/plugins/timepicker/bootstrap-timepicker.min.js"
		var="v" />
	<script src="${v}"></script>

	<!-- SlimScroll -->
	<spring:url
		value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
		var="w" />
	<script src="${w}"></script>

	<!-- iCheck 1.0.1 -->
	<spring:url value="/plugins/iCheck/icheck.min.js" var="x" />
	<script src="${x}"></script>

	<!-- FastClick -->
	<spring:url value="/bower_components/fastclick/lib/fastclick.js"
		var="y" />
	<script src="${y}"></script>

	<!-- AdminLTE App -->
	<spring:url value="/dist/js/adminlte.min.js" var="z" />
	<script src="${z}"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="/dist/js/demo.js"></script>

	<!-- other js -->
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
	<script
		src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
	<!-- other js end -->


	<!-- Page script -->

	<script type="text/javascript">
		$(document).ready(function() {

			$('#start').daterangepicker({
				minDate:new Date(),
				timePicker : true,
				timePicker24Hour : true,
				locale : {
					format : 'MM/DD/YYYY HH:mm:ss'
				},
				singleDatePicker : true
			}

			);

			$('#end').daterangepicker({
				minDate:new Date(),
				timePicker : true,
				timePicker24Hour : true,
				locale : {
					format : 'MM/DD/YYYY HH:mm:ss'
				},
				singleDatePicker : true
			});

			var ref = window.opener.rightClickShipData;
			var refType = window.opener.rightClickShipRequestType;
			if (ref) {
				$('#imono').val(ref.imo_no);
				$('#mmsino').val(ref.mmsi_no);
				$('#shipname').val(ref.ship_name);
				if (refType == "Stop")
					$("#requesttypeinput").val("8");
				else if (refType == "Poll")
					$("#requesttypeinput").val("1");
				else
					$("#requesttypeinput").val("0");
			}

		}); // end of document ready

		function ShowHideDiv() {
			if (($('#requesttypeinput').val() == '2')
					|| ($('#requesttypeinput').val() == '3')
					|| ($('#requesttypeinput').val() == '4')
					|| ($('#requesttypeinput').val() == '5')
					|| ($('#requesttypeinput').val() == '6')|| ($('#requesttypeinput').val() == '7')|| ($('#requesttypeinput').val() == '8')|| ($('#requesttypeinput').val() == '10')|| ($('#requesttypeinput').val() == '11')) {
				$('#starttime').show();
				$('#endtime').show();
				document.getElementById("start").required = true;
				document.getElementById("end").required = true;
			} else {
				$('#starttime').hide();
				$('#endtime').hide();
				document.getElementById("start").removeAttribute("required");
				document.getElementById("end").removeAttribute("required");

			}

		}

		function showTable() {
			var table = $('#Shipdetail').DataTable({
				//dom: 'lBrtip',
				dom : 'lfrtip',
				"bDestroy" : true,
				"ajax" : {
					"url" : "${getshipdetails}",
					"type" : "GET",
					"dataSrc" : ""
				},

				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'os',
					"selector" : 'td:first-child'
				},
				"order" : [ [ 1, 'asc' ] ],

				"columns" : [ {
					"data" : ""
				}, {
					"data" : "imoNo"
				}, {
					"data" : "vesselName"
				}, {
					"data" : "mmsiNo"
				} ]

			});
			$('#select').on('click', function() {

				var imo, mmsi, shipname;
				$.each(table.rows('.selected').data(), function() {
					imo = this["imoNo"];
					mmsi = this["mmsiNo"];
					ship = this["vesselName"];
					$('#imono').val(imo);
					$('#mmsino').val(mmsi);
					$('#shipname').val(ship);
				});
			});
		}

		/* function submitForm() {
			var frm = document.getElementsByName('flagrequest')[0];
			frm.submit(); // Submit the form
			frm.reset(); // Reset all form data
			return false;
		} */
	</script>
</body>
</html>
