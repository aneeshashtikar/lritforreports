<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Coastal SURPIC Request</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">
<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>

<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
 --%>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<spring:url value="/requests/persistcoastalsurpicrequest"
	var="persistcoastalsurpicrequest" />
<spring:url value="/requests/getcountrylist" var="getcountrylist" />

<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!--  Map Dependency -->
	
	<spring:url value="/resources/ol/ol.js" var="oljs" />
	<script src="${oljs}"></script>
	
	<!-- for Advanced GeoSpecial functions turf min js used-->
	<spring:url value="/resources/ol/turf.min.js" var="turfminjs" />
	<script src="${turfminjs}"></script>
	
	<spring:url value="/resources/css/ol.css" var="olcss" />
	<link rel="stylesheet" href="${olcss}">

<spring:url value="/resources/js/sarsurpicRequest.js" var="sarsurpicRequestjs" />
	<script src="${sarsurpicRequestjs}"></script>

<script type="text/javascript">
//---
var radval;

$(document).ready(function() {
	
	var rightClickCoodinate = window.opener.rightClickCoodinate;	
	var rightClickSurpicRadius = window.opener.rightClickSurpicRadius;
	var rightClickSurpicRectangle = window.opener.rightClickSurpicRectangle;
	var rightClickSurpicCenter = window.opener.rightClickSurpicCenter;
	var latDeg;
	var latMin;
	var lonDeg;
	var lonMin;
	
/* 	if (rightClickSurpicRectangle!="")
	{
		$('input:radio[name=surpicareatype]:nth(0)').attr('checked',true);
		$('input[type="radio"]').trigger("click");
	}
	else if (rightClickSurpicRadius!="")
	{
		$('input:radio[name=surpicareatype]:nth(1)').attr('checked',true);
		$('input[type="radio"]').trigger("click");
	} */
		$('input[type="radio"]').click(function() {

			if ($(this).is(':checked')) {
				radval = $(this).val();
				console.info('radval ' + radval);
				if (radval == "rectangular") {
					document.getElementById('latDegCircle').value="";
					document.getElementById('latMinCircle').value="";
					document.getElementById('lonDegCircle').value="";
					document.getElementById('lonMinCircle').value="";
					document.getElementById('radius').value=""; 
					
					document.getElementById("latDegCircle").removeAttribute("required");
					document.getElementById("latMinCircle").removeAttribute("required");
					document.getElementById("lonDegCircle").removeAttribute("required");
					document.getElementById("lonMinCircle").removeAttribute("required");
					document.getElementById("radius").removeAttribute("required");
					$('#rectanglearea').show();
					$('#circlearea').hide();
					document.getElementById("latDegLB").required = true;
					document.getElementById("latMinLB").required = true;
					document.getElementById("lonDegLB").required = true;
					document.getElementById("lonMinLB").required = true;
					document.getElementById("latDegRT").required = true;
					document.getElementById("latMinRT").required = true;
					document.getElementById("lonDegRT").required = true;
					document.getElementById("lonMinRT").required = true; 
					
					if(rightClickCoodinate!="" && rightClickSurpicCenter==""){
					 	 
					 	   rightClickCoodinateLonLat = ol.proj.toLonLat(rightClickCoodinate,'EPSG:4326');			
					 	   var  data ={
									lat : rightClickCoodinateLonLat[1],
									lon : rightClickCoodinateLonLat[0]
					 	   		}
					 	  $("#latSignLB").val((data.lat < 0) ? "S" : "N" );
				  		  $("#lonSignLB").val((data.lon < 0) ? "W" : "E" );
				      		
				  		   [latDeg ,latMin] = LatLonToDegMin(data.lat);  
				  		   [lonDeg ,lonMin] = LatLonToDegMin(data.lon); 

				  		    latDeg = latDeg.trim();
					  		latMin = (latMin.toString()).trim();
					  		lonDeg = lonDeg.trim();
					  		lonMin = (lonMin.toString()).trim();
					  		
				  		if (latDeg.length<2)
				  			 latDeg = '0' + latDeg;
				  		   
				  		if (latMin.length<2)
				  			latMin = '0' + latMin;
				  		 
				  		if (lonMin.length<2)
				  			lonMin = '0' + lonMin;
				  		 
				  		if (lonDeg.length<2)
				  			lonDeg = '00' + lonDeg;
				  		
				  		else if (lonDeg.length>1 && lonDeg.length<3)
				  			lonDeg = '0' + lonDeg;
				  		   
				  		   $("#latDegLB").val(latDeg);
				  		   $("#latMinLB").val(latMin);		            		
				  		   $("#lonDegLB").val(lonDeg);
				  		   $("#lonMinLB").val(lonMin);
						}
				  		   else{
								sourceSurpicPolygon.clear();
								 rightClickCoodinateLonLat =  rightClickSurpicCenter;			
								 var  data ={
											lat : rightClickCoodinateLonLat[1],
											lon : rightClickCoodinateLonLat[0]
							 	   		}
							 	  $("#latSignLB").val((data.lat < 0) ? "S" : "N" );
						  		  $("#lonSignLB").val((data.lon < 0) ? "W" : "E" );
						      		
						  		   [latDeg ,latMin] = LatLonToDegMin(data.lat);  
						  		   [lonDeg ,lonMin] = LatLonToDegMin(data.lon); 

						  		    latDeg = latDeg.trim();
							  		latMin = (latMin.toString()).trim();
							  		lonDeg = lonDeg.trim();
							  		lonMin = (lonMin.toString()).trim();
							  		
						  		if (latDeg.length<2)
						  			 latDeg = '0' + latDeg;
						  		   
						  		if (latMin.length<2)
						  			latMin = '0' + latMin;
						  		 
						  		if (lonMin.length<2)
						  			lonMin = '0' + lonMin;
						  		 
						  		if (lonDeg.length<2)
						  			lonDeg = '00' + lonDeg;
						  		
						  		else if (lonDeg.length>1 && lonDeg.length<3)
						  			lonDeg = '0' + lonDeg;
						  		   
						  		   $("#latDegLB").val(latDeg);
						  		   $("#latMinLB").val(latMin);		            		
						  		   $("#lonDegLB").val(lonDeg);
						  		   $("#lonMinLB").val(lonMin);
								
							}
				  		 if(rightClickSurpicRectangle!=""){						
								rightClickCoodinateLonLat =  rightClickSurpicRectangle;			
								 var  data ={
											lat : rightClickCoodinateLonLat[1],
											lon : rightClickCoodinateLonLat[0]
							 	   		}
							 	  $("#latSignRT").val((data.lat < 0) ? "S" : "N" );
						  		  $("#lonSignRT").val((data.lon < 0) ? "W" : "E" );
						      		
						  		   [latDeg ,latMin] = LatLonToDegMin(data.lat);  
						  		   [lonDeg ,lonMin] = LatLonToDegMin(data.lon); 

						  		    latDeg = latDeg.trim();
							  		latMin = (latMin.toString()).trim();
							  		lonDeg = lonDeg.trim();
							  		lonMin = (lonMin.toString()).trim();
							  		
						  		if (latDeg.length<2)
						  			 latDeg = '0' + latDeg;
						  		   
						  		if (latMin.length<2)
						  			latMin = '0' + latMin;
						  		 
						  		if (lonMin.length<2)
						  			lonMin = '0' + lonMin;
						  		 
						  		if (lonDeg.length<2)
						  			lonDeg = '00' + lonDeg;
						  		
						  		else if (lonDeg.length>1 && lonDeg.length<3)
						  			lonDeg = '0' + lonDeg;
						  		   
						  		   $("#latDegRT").val(latDeg);
						  		   $("#latMinRT").val(latMin);		            		
						  		   $("#lonDegRT").val(lonDeg);
						  		   $("#lonMinRT").val(lonMin);
							}
					
				}
				if (radval == "circular") {
					 document.getElementById('latDegLB').value="";
						document.getElementById('latMinLB').value="";
						document.getElementById('lonDegLB').value="";
						document.getElementById('lonMinLB').value="";
						document.getElementById('latDegRT').value="";
						document.getElementById('latMinRT').value="";
						document.getElementById('lonDegRT').value="";
						document.getElementById('lonMinRT').value=""; 
						
						document.getElementById("latDegLB").removeAttribute("required");
						document.getElementById("latMinLB").removeAttribute("required");
						document.getElementById("lonDegLB").removeAttribute("required");
						document.getElementById("lonMinLB").removeAttribute("required");
						document.getElementById("latDegRT").removeAttribute("required");
						document.getElementById("latMinRT").removeAttribute("required");
						document.getElementById("lonDegRT").removeAttribute("required");
						document.getElementById("lonMinRT").removeAttribute("required");
						$('#rectanglearea').hide();
						$('#circlearea').show();
						document.getElementById("latDegCircle").required = true;
						document.getElementById("latMinCircle").required = true;
						document.getElementById("lonDegCircle").required = true;
						document.getElementById("lonMinCircle").required = true;
						document.getElementById("radius").required = true;
						
						if(rightClickCoodinate!="" && rightClickSurpicCenter==""){
					 	 
					 	   rightClickCoodinateLonLat = ol.proj.toLonLat(rightClickCoodinate,'EPSG:4326');			
					 	   var  data ={
									lat : rightClickCoodinateLonLat[1],
									lon : rightClickCoodinateLonLat[0]
					 	   		}
					 	   $("#latSignCircle").val((data.lat < 0) ? "S" : "N" );
				  		   $("#lonSignCircle").val((data.lon < 0) ? "W" : "E" );
				      		
				  		   [latDeg ,latMin] = LatLonToDegMin(data.lat);  
				  		   [lonDeg ,lonMin] = LatLonToDegMin(data.lon); 
				  		
				  		 latDeg = latDeg.trim();
				  		latMin = (latMin.toString()).trim();
				  		lonDeg = lonDeg.trim();
				  		lonMin = (lonMin.toString()).trim();				  		 
							
				  		if (latDeg.length<2)
				  			 latDeg = '0' + latDeg;
				  		   
				  		if (latMin.length<2)
				  			latMin = '0' + latMin;
				  		 
				  		if (lonMin.length<2)
				  			lonMin = '0' + lonMin;
				  		 
				  		if (lonDeg.length<2)
				  			lonDeg = '00' + lonDeg;
				  		
				  		else if (lonDeg.length>1 && lonDeg.length<3)
				  			lonDeg = '0' + lonDeg;
				  		   
				  		   $("#latDegCircle").val(latDeg);
				  		   $("#latMinCircle").val(latMin);		            		
				  		   $("#lonDegCircle").val(lonDeg);
				  		   $("#lonMinCircle").val(lonMin);  
						}
						else{
							sourceSurpicPolygon.clear();
							 rightClickCoodinateLonLat =  rightClickSurpicCenter;			
						 	   var  data ={
										lat : rightClickCoodinateLonLat[1],
										lon : rightClickCoodinateLonLat[0]
						 	   		}
						 	   $("#latSignCircle").val((data.lat < 0) ? "S" : "N" );
					  		   $("#lonSignCircle").val((data.lon < 0) ? "W" : "E" );
					      		
					  		   [latDeg ,latMin] = LatLonToDegMin(data.lat);  
					  		   [lonDeg ,lonMin] = LatLonToDegMin(data.lon); 
					  		
					  		 latDeg = latDeg.trim();
					  		latMin = (latMin.toString()).trim();
					  		lonDeg = lonDeg.trim();
					  		lonMin = (lonMin.toString()).trim();				  		 
								
					  		if (latDeg.length<2)
					  			 latDeg = '0' + latDeg;
					  		   
					  		if (latMin.length<2)
					  			latMin = '0' + latMin;
					  		 
					  		if (lonMin.length<2)
					  			lonMin = '0' + lonMin;
					  		 
					  		if (lonDeg.length<2)
					  			lonDeg = '00' + lonDeg;
					  		
					  		else if (lonDeg.length>1 && lonDeg.length<3)
					  			lonDeg = '0' + lonDeg;
					  		   
					  		   $("#latDegCircle").val(latDeg);
					  		   $("#latMinCircle").val(latMin);		            		
					  		   $("#lonDegCircle").val(lonDeg);
					  		   $("#lonMinCircle").val(lonMin);  
							
						}
						if(rightClickSurpicRadius!=""){						
							$("#radius").val(rightClickSurpicRadius);
						}
					  
				  
				}
			}
		});
	});
	

var sourceCustomPolygon = window.opener.sourceCustomPolygon;
var map = window.opener.map;
//sessionStorage.setItem("mapSurpic",JSON.stringify( mapTemp));
//sessionStorage.setItem("sourceCustomPolygonSurpic", sourceCustomPolygon);
var sourceSurpicPolygon = window.opener.sourceSurpicPolygon;

var coordinates = window.opener.rightClickCoodinate;

var vectorSource_Temp = new ol.source.Vector(
		{              
			 wrapX: false,
	    	 noWrap: true
	    });
	

	

function checkwithin1000NM()
	{
		var jsondata;
		$.ajax({
		    type: "POST",
		    traditional: true,
		    url: "/lrit/map/SO",
		    data: {'cg_lritid': 1065},
		    success:function(response) {		    	
			    	jsondata = response; 			    	
			    	var searchWithin = turf.polygon(jsondata[0][0].poslist.coordinates);
					var tosearch;
					if (radval == "rectangular") 
					{
						var polygonSo = applyRectangle();	
						var polygonDrawnCoordinates = [[polygonSo.lon,polygonSo.lat],[polygonSo.lon,polygonSo.lat+polygonSo.latOffset],[polygonSo.lon+polygonSo.longOffset,polygonSo.lat+polygonSo.latOffset],
							[polygonSo.lon + polygonSo.longOffset,polygonSo.lat],[polygonSo.lon,polygonSo.lat]];
						tosearch = turf.polygon([polygonDrawnCoordinates]);
					}
					else if (radval == "circular") 
					{
						var polygonSo = applyCircle();
						var options = {steps: 360, units: 'miles', options: {}}; //degrees
						polygonSo.radius = polygonSo.radius * 1.1507;
						tosearch = turf.circle([polygonSo.lon,polygonSo.lat], polygonSo.radius, options)
						
					}
						if (radval == "rectangular" || radval == "circular")	    	 	
						{
							var insideBoundaryCheck = turf.booleanWithin(tosearch, searchWithin);						
							console.info('insideBoundaryCheck: ' + insideBoundaryCheck);
							var frm = document.getElementsByName('surpicrequest');
							if(insideBoundaryCheck===true)
							{
								$('#surpicrequest').submit();
								//return true;
							}
							else
								{
								alert("not within 1000 NM");
									return false;
								}
						}
						else
							return false;
		    }
		});	 
	}

	
	
	 function submitForm() {	
			
		    window.opener.rightClickSurpicRadius = "";
			window.opener.rightClickSurpicRectangle = "";
			window.opener.rightClickSurpicCenter = "";
			window.opener.rightClickCoodinate = "";
			var insideBoundaryCheck = checkwithin1000NM();		
		}
	
	function applysarsurpicRequestLocal(RectangularArea, CircularArea){	
		console.info('inside applysarsurpicRequest ')
		console.info('RectangularArea ' + RectangularArea);
		console.info('CircularArea ' + CircularArea);		
		/* $('#mainSection').hide();
		$('#surpicrequestBox').hide() */
		try{
			 var sarSurpicJsonObject;
			 
			var sarArea;			
			if(RectangularArea != null && CircularArea==""){	    		  
	    		 // sarArea = applyRectangle();	    		 
	    		  
	    		 var rectangleInfo = RectangularArea.split(":");
	    		  var latInfo = rectangleInfo[0].split(".");
	    		  var lonInfo = rectangleInfo[1].split(".");
	    		  var latOffsetInfo = rectangleInfo[2].split(".");
	    		  var lonOffsetInfo = rectangleInfo[3].split(".");	    		  
	    		 
	    		  var latOutLB= DegMinToLatLon(latInfo[0],latInfo[1],convertSignFromNSEWString(latInfo[2]));
		    	  var lonOutLB= DegMinToLatLon(lonInfo[0],lonInfo[1],convertSignFromNSEWString(lonInfo[2]));
		    	  
	    		  var latOutRT= DegMinToLatLon(latOffsetInfo[0],latOffsetInfo[1],convertSignFromNSEWString(latOffsetInfo[2]));
		    	  var lonOutRT= DegMinToLatLon(lonOffsetInfo[0],lonOffsetInfo[1],convertSignFromNSEWString(lonOffsetInfo[2]));
		
		    	  var polygontype ="rectangle";
		          var sarArea={
		        	    polygonId:Math.floor(Math.random() * 1000),
			          	polygonName: "rect" + Math.floor(Math.random() * 1000) , 
		          		polygontype: polygontype,
		          		lat : latOutLB,
		          		lon : lonOutLB,
		          		latOffset : latOutRT,
		          		longOffset : lonOutRT,
		          }
		          sarArea.latOffset = sarArea.latOffset + sarArea.lat;
	    		  sarArea.longOffset = sarArea.longOffset + sarArea.lon;
	    		  
	    		  //sarAreaToSave = applySarCircle('rectangle');
	    		  
	    	  } else if(RectangularArea == "" && CircularArea !=null){
	    		  //sarArea = applyCircle();
	    		  
	    		  var polygontype ="circle";
	    		  var circleInfo = CircularArea.split(":");
	    		  var latInfo = circleInfo[0].split(".");
	    		  var lonInfo = circleInfo[1].split(".");
	    		  var radius = circleInfo[2];
	    		  var latOutCircle= DegMinToLatLon(latInfo[0],latInfo[1],convertSignFromNSEWString(latInfo[2]));
	        	  var lonOutCircle= DegMinToLatLon(lonInfo[0],lonInfo[1],convertSignFromNSEWString(lonInfo[2]));
	              sarArea={
	               polygonId:Math.floor(Math.random() * 1000),
			      	polygonName: "circ" + Math.floor(Math.random() * 1000) ,  
	          		polygontype: polygontype,
	          		lat : latOutCircle,
	          		lon : lonOutCircle,
	          		radius : radius		          		
		          }
          
	    		 // sarAreaToSave = applySarCircle('circle');
	    	  }
			sarFlag = true;
			displayPolygonOnMap(sarArea,"SarAreaFlag",sourceCustomPolygon,sarFlag);
			
			//map.surpicRequestFlag = true; 
		 }
		catch(err)
		{
			console.info('error in apply coastal sarsurpic Request: ' + err)
		}
	}
		
</script>
</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
		<c:if test="${(successMsg != null) || (errorMsg != null)}" >
				<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Acknowledgement</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">

						<!-- ADD YOUR MESSAGE CONTENTS HERE  -->
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
							<script type="text/javascript">  applysarsurpicRequestLocal('${RectangularArea}','${CircularArea}');</script>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<!-- /#ADD YOUR MESSAGE CONTENTS HERE  -->
					</div>

					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
				
			</c:if>
			<c:if test="${(successMsg == null) && (errorMsg == null)}" >
			<!-- Main content -->
			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
							 <script type="text/javascript"> applysarsurpicRequestLocal('${RectangularArea}','${CircularArea}');</script>
							<!--  <script type="text/javascript">applysarsurpicRequestLocal();</script> -->
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger close' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">

							<div class="box-header with-border">
								<h3 class="box-title">
									<b>SURPIC Request</b>
								</h3>
							</div>

							<!-- /.box-header -->
							<!-- form start -->
							<form:form id="surpicrequest" action="${persistcoastalsurpicrequest}" 
								method="post" name="surpicrequest" 
								modelAttribute="portalsurpicrequest">

								<div class="box-body">



									<div class="row">
										<div id="surpicarea" class="form-group  col-sm-6">
											<label for="surpicarea" class="col-sm-4 control-label">Area Type</label>
											<div class="col-sm-8">

												<label> <input type="radio" name="surpicareatype"
													id="rectangle" value="rectangular" required="true"> SAR
													Rectangular Area
												</label> <label><input type="radio" name="surpicareatype"
													id="circle" value="circular" required="true"> SAR
													Circular Area</label>
											</div>
										</div>
									</div>




									<div id="rectanglearea" style="display: none">
										<div class="row">
											<div class="form-group col-sm-8">
												<table>
													<tbody>
														<tr align="left">
															<th class="col-sm-3 control-label">South West</th>
															<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
															<td>Latitude</td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="rectangleLatitudedegree"
																	type="text" maxlength="2" size="2" id='latDegLB'
																	pattern="[0-9][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input id='latMinLB'
																	path="rectangleLatitudemin" type="text" maxlength="2"
																	size="2" pattern="[0-5][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:select id='latSignLB'
																	path="rectangleLatitudedir">
																	<form:option value="N">North</form:option>
																	<form:option value="S">South</form:option>
																</form:select></td>
															<td>&nbsp;&nbsp;&nbsp;</td>
															<td>Longitude</td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="rectangleLongitudedegree"
																	type="text" maxlength="3" size="3" id='lonDegLB'
																	pattern="[0-1][0-9][0-9]" title="Enter Valid data.." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="rectangleLongitudemin"
																	type="text" maxlength="2" size="2" id='lonMinLB'
																	pattern="[0-5][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:select path="rectangleLongitudedir"
																	id='lonSignLB'>
																	<form:option value="E">East</form:option>
																	<form:option value="W">West</form:option>
																</form:select></td>
														</tr>
													</tbody>
												</table>
												<!-- </div> -->
											</div>
										</div>

										<div class="row">
											<div class="form-group col-sm-8">
												<table>
													<tbody>
														<tr align="left">
															<th class="col-sm-3 control-label">Offset</th>
															<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
															<td>Latitude</td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="offsetLatitudedegree"
																	type="text" maxlength="2" size="2" id='latDegRT'
																	pattern="[0-9][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="offsetLatitudemin"
																	id='latMinRT' type="text" maxlength="2" size="2"
																	pattern="[0-5][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:select path="offsetLatitudedir"
																	id='latSignRT'>
																	<form:option value="N">North</form:option>
																	<form:option value="S">South</form:option>
																</form:select></td>
															<td>&nbsp;&nbsp;&nbsp;</td>
															<td>Longitude</td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="offsetLongitudedegree"
																	type="text" maxlength="3" size="3" id='lonDegRT'
																	pattern="[0-1][0-9][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="offsetLongitudemin"
																	type="text" maxlength="2" size="2" id='lonMinRT'
																	pattern="[0-5][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:select path="offsetLongitudedir"
																	id='lonSignRT'>
																	<form:option value="E">East</form:option>
																	<form:option value="W">West</form:option>
																</form:select></td>
														</tr>
													</tbody>
												</table>

											</div>
										</div>
									</div>


									<!-- circular -->
									<div id="circlearea" style="display: none">
										<div class="row">
											<div class="form-group col-sm-8">
												<table>
													<tbody>
														<tr align="left">
															<th class="col-sm-3 control-label">Center</th>
															<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
															<td>Latitude</td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="circleLatitudedegree"
																	type="text" maxlength="2" size="2" id='latDegCircle'
																	pattern="[0-9][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="circleLatitudemin"
																	id='latMinCircle' type="text" maxlength="2" size="2"
																	pattern="[0-5][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:select id='latSignCircle'
																	path="circleLatitudedir">
																	<form:option value="N">North</form:option>
																	<form:option value="S">South</form:option>
																</form:select></td>
															<td>&nbsp;&nbsp;&nbsp;</td>
															<td>Longitude</td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="circleLongitudedegree"
																	type="text" maxlength="3" size="3" id='lonDegCircle'
																	pattern="[0-1][0-9][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:input path="circleLongitudemin"
																	type="text" maxlength="2" size="2" id='lonMinCircle'
																	pattern="[0-5][0-9]" title="Enter Valid data." /></td>
															<td>&nbsp;&nbsp;</td>
															<td><form:select path="circleLongitudedir"
																	id='lonSignCircle'>
																	<form:option value="E">East</form:option>
																	<form:option value="W">West</form:option>
																</form:select></td>
														</tr>
													</tbody>
												</table>
												<!-- </div> -->
											</div>
										</div>



										<div class="row">
											<div class="form-group col-sm-6">

												<label for="radius" class="col-sm-4 control-label">Radius</label>
												<div class="col-sm-8">
													<form:input type="text" path="radius" class="form-control"
														maxlength="3"  id="radius" name="radius"
														/>

												</div>
											</div>
										</div>

									</div>




									<div class="row"  >
										<div class="form-group col-sm-6">


											<label for="shiptype" class="col-sm-4">IMO Ship Type</label>
											<div class="form-group col-sm-8">
												<form:select path="shipType" class="form-control"
													id="shiptype" onchange="" multiple="true">
													<form:option value="0100">Passenger</form:option>
													<form:option value="0200">Cargo</form:option>
													<form:option value="0300">Tanker</form:option>
													<form:option value="0400">Mobile offshore drilling unit</form:option>
													<form:option value="9900">Other</form:option>
												</form:select>
											</div>
										</div>
									</div>


									<div class="row" >
										<div class="form-group col-sm-6">


											<label for="countries" class="col-sm-4">Country</label>
											<div class="form-group col-sm-8">
												<form:select path="Countries" class="form-control" id="country"
													name="country">
													 <form:option value="0">Select</form:option> 
													<c:forEach items="${countrylist}" var="category">													 
														<form:option value="${category.cgName}">${category.cgName}</form:option>
													</c:forEach>
													
												</form:select>
											</div>
										</div>
									</div>







									<div class="row">
										<div class="form-group col-sm-6">


											<label for="position" class="col-sm-4 control-label">No. of
												Positions</label>
											<div class="form-group col-sm-8">
												<form:select path="numberOfPosition" class="form-control"
													id="positioninput" onchange="" >
													<form:option value="0">Select</form:option>
													<form:option value="1">1</form:option>
													<form:option value="2">2</form:option>
													<form:option value="3">3</form:option>
													<form:option value="4">4</form:option>
												</form:select>
											</div>
										</div>
									</div>







								</div>
								<div class="box-footer">
								<!--  	<button type="button" class="btn btn-primary" onclick="submitForm();">Submit</button> -->
								<button type="button" class="btn btn-primary" onclick="submitForm();">Submit</button>
								</div>
							</form:form>

							<!-- /.form -->
						</div>

					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
			</section>
			</c:if>
		</div>

		<!-- /.content -->
		
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<div class="control-sidebar-bg"></div>
	<!-- </div> -->
	<!-- ./wrapper -->



</body>
</html>
