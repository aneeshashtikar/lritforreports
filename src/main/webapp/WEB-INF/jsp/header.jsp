<!-- Header.jsp Start -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:url value="/resources/img/images.jpg" var="Userlogo_profile" />
<spring:url value="/resources/img/header_12.jpg" var="bannerImage" />
<spring:url value="/logout" var="logout" />
<spring:url value="/map/mapHome" var="home" />

<spring:url value="/users/userprofile" var="userprofile" />
<spring:url value="/users/changepassword" var="changepassword" />


<spring:url value="/alerts/viewallalerts" var="viewallalerts" />
<spring:url value="/alerts/viewveryhighseverity?severity=VeryHigh" var="alertswithseverityveryhigh"/> 
<spring:url value="/alerts/viewveryhighseverity?severity=High" var="alertswithseverityhigh"/> 
<spring:url value="/alerts/viewveryhighseverity?severity=Medium" var="alertswithseveritymedium"/> 
<spring:url value="/alerts/viewveryhighseverity?severity=Low" var="alertswithseveritylow"/> 
<spring:url value="/alerts/getalertcount" var="getalertcount"/>
<spring:url value="/alerts/getClosedAlertsPage" var="getClosedAlertsPage"/>
<spring:url value="/pendingtask/getPendingTaskCount" var="getpendingtaskcount"/>
<spring:url value="/pendingtask/getRequestPendingTaskPage" var="getRequestPendingTaskPage"/>

<body>
<div id="sound"></div>
<div id="header-img">
	<header class="main-header main-header-custom ">
		<!-- Logo -->
		<%--Commented logo of lrit in header--%>
		<!-- <a href="/index2.html" class="logo"> mini logo for sidebar mini 50x50 pixels
			<span class="logo-mini">LRIT</span> logo for regular state and mobile devices
			<span class="logo-lg"><b>LRIT</b></span>
		</a> -->
		<a href="${home}"> <img class="img-fluid" src="${bannerImage}" height="100px" width= "100%">
		</a>
	<!-- toggle button for menu -->
		 <a href="#" class="sidebar-toggle" data-toggle="push-menu"
				role="button"> <span class="sr-only">Toggle navigation</span> <span
				class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
			</a> 
			<!-- toggle button for menu -end -->
		<!-- Header Navbar: style can be found in header.less -->
	</header>
	 <nav class="navbar navbar-static-top main-header-notification">
	<!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu"
				role="button"> <span class="sr-only">Toggle navigation</span> <span
				class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
			</a> -->
	<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<!-- Messages: style can be found in dropdown.less-->
					<li><a href="${home}" > <i
							class="fa fa-home" id="homeIcon" >home</i> 
					</a>
					
					<li class="dropdown messages-menu"><a href="${getRequestPendingTaskPage}"
						> <i
							class="fa fa-envelope-o"></i> <span class="label label-success" id="pendingTaskTotalCount"></span>
					</a>
					
						<%-- <ul class="dropdown-menu">
							<li class="header">You have pending task</li>
							<li>
								<!-- inner menu: contains the actual data -->
								<ul class="menu">
								<li><a href="${alertswithseverityveryhigh }"> <i class="fa fa-users text-aqua"></i>
											Very High Severity(<span id = "veryHighCount"></span>)
									</a></li>
									
								</ul>
							</li>
							<li class="footer"><a href="#">See All Messages</a></li>
						</ul> --%>
						
						</li>
					<!-- Notifications: style can be found in dropdown.less -->
					<li class="dropdown notifications-menu"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <i
							class="fa fa-bell" id="bellIcon"></i> <span class="label label-warning" id="noOfAlerts"></span>
					</a>
						<ul class="dropdown-menu" id="severity">
							<li class="header">You have <span id="totalAlertCount"></span> notifications</li>
							<li>
								<!-- inner menu: contains the actual data -->
								<ul class="menu" >
									<li><a href="${alertswithseverityveryhigh }"> <i class="fa fa-users text-aqua"></i>
											Very High Severity(<span id = "veryHighCount"></span>)
									</a></li>
									<li><a href="${alertswithseverityhigh }"> <i class="fa fa-warning text-yellow"></i>
										High Severity(<span id = "highCount"></span>)
									</a></li>
									<li><a href="${alertswithseveritymedium }"> <i class="fa fa-users text-red"></i>
											Medium Severity(<span id = "mediumCount"></span>)
									</a></li>

									<li><a href="${alertswithseveritylow }"> <i
											class="fa fa-shopping-cart text-green"></i> Low Severity(<span id = "lowCount"></span>)
									</a></li>
									<li><a href="${getClosedAlertsPage }"><i class="fa fa-bell-slash"></i> Closed Alerts
									</a></li>
								</ul>
							</li>
							<li class="footer"><a href="${viewallalerts}">View all</a></li>
						</ul></li>
					<!-- Tasks: style can be found in dropdown.less -->
					<!-- comment start : This is commented to show only two option in the header
					comment end: This is commented to show only two option in the header -->
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
							<i class="fa fa-user"></i> <span class="hidden-xs">${sessionScope.USER.name}</span>
					</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header">
								<!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
								<img src="${Userlogo_profile}" class="img-circle"
								alt="User Image">
								<p><a href="${userprofile}" >${sessionScope.USER.name}</a></p>
							</li>
							<!-- Menu Body -->

							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="${changepassword}" class="btn btn-default btn-flat">Change Password</a>
								</div>
								<div class="pull-right">
								<!-- <div style="text-align: center"> -->
								<form action="${logout}" id="logout" method="post">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<a href="#" class="btn btn-default btn-flat"
									onclick="document.getElementById('logout').submit();">Logout</a>

							</div>
							</li>
						</ul></li>
				</ul>
			</div>
	</nav> 
</div>
</body>
<script>
 /*   $(document).ready(function() {
	  alertCounter();
		pendingTaskCounter();
}); */   
 
document.addEventListener('DOMContentLoaded', function() {
	alertCounter();
	pendingTaskCounter();
}, false);

var interval = 1000 * 60 * 2; 

setInterval(function(){
	alertCounter();
	pendingTaskCounter();
}, 300000); 

setInterval(function(){	
	displayRightSideBarData();
}, 300000);  

function alertCounter() {
	$.ajax({
		url : "${getalertcount}",
		success : function(result) {
			localStorage.setItem("totalCount", result.totalCount);
			localStorage.setItem("veryHighCount", result.VeryHigh);
			localStorage.setItem("highCount", result.High);
			localStorage.setItem("mediumCount", result.Medium);
			localStorage.setItem("lowCount", result.Low);
			
	 		$("#totalAlertCount").html(localStorage.getItem("totalCount"));
			$("#noOfAlerts").html(localStorage.getItem("totalCount"));

			if(localStorage.getItem("totalCount") == 0)
			{
				console.log("inside jquery if");
				//$('#bellIcon').find('#bellIcon').stop(true, true).fadeOut();
			    $("#bellIcon").stop(true, true);
			}
			if(localStorage.getItem("veryHighCount") > 0)
			{
				$("#veryHighCount").html(localStorage.getItem("veryHighCount"));
				playSound();
				document.getElementById("bellIcon").style.color = "red";
				blink();	
			}
			else
			{
				$("#veryHighCount").html(0);
				if(document.getElementById("beepSound") != null)
					stopSound();
				document.getElementById("bellIcon").style.color = "green";
			}
				
			if(localStorage.getItem("highCount") > 0)
			{
				$("#highCount").html(localStorage.getItem("highCount"));
				blink();
			}
			else
			{
				$("#highCount").html(0);
			}

			
			if(localStorage.getItem("mediumCount") > 0)
			{
				$("#mediumCount").html(localStorage.getItem("mediumCount"));
				blink();
			}
			else
			{
				$("#mediumCount").html(0);
			}
				

			if(localStorage.getItem("lowCount") > 0)
			{
				$("#lowCount").html(localStorage.getItem("lowCount"));
				blink();
			}
			else
			{
				$("#lowCount").html(0);
			}
			
		} 
	});
}

//To reflect changes in all opened tabs
window.addEventListener('storage', alertCounter);

function pendingTaskCounter()
{
	$.ajax({
		url : "${getpendingtaskcount}",
		success : function(data) {
			$("#pendingTaskTotalCount").html(data);
		}
	});
}

/* Function for playing sound */
function playSound() {
	var filename = '../resources/sound/beep-08b111';
	var mp3Source = '<source src="' + filename + '.mp3" type="audio/mpeg">';
	var embedSource = '<embed hidden="true" autostart="true" loop="true" src="' + filename +'.mp3">';
	document.getElementById("sound").innerHTML = '<audio controls="controls" loop="loop" autoplay="autoplay" id="beepSound" style="display:none">'
			+ mp3Source + embedSource + '</audio>';
}

function stopSound()
{
	var sound = document.getElementById("beepSound");
	sound.pause();
}

/* function for blinking the bell icon */
 function blink(){
	    $('#bellIcon').delay(100).fadeTo(200,0.5).delay(100).fadeTo(200,1, blink);
	}
</script>
<!-- Header.jsp End -->