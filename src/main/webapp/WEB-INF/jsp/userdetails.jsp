<!-- 
--Created finalform.jsp as the sample form finalized by team for Shipping Company.
--included a shipbourne table in tab_2 
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Custom Form</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<%-- <spring:url value="/resources/bower_components/Ionicons/css/ionicons.min.css" var="ioniconsmincss" /> --%>
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- Theme style -->
<%-- <spring:url value="/resources/dist/css/AdminLTE.min.css" var="adminltemincss" /> --%>
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<%-- <spring:url value="/resources/dist/css/skins/_all-skins.min.css" var="allskinmincss" /> --%>
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<%-- <spring:url value="/resources/custom.css" var="customcss" /> 
<link rel="stylesheet" href="${customcss}"> --%>

<spring:url value="/login" var="Login" />

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<spring:url
	value="/resources/lrit-dashboard.css"
	var="lritcustomcss" />
	<link rel="stylesheet" href="${lritcustomcss}">
<style>
.customform {
	text-align: left;
}
</style>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Vessel Registration Form</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Forms</a></li>
					<li class="active">General Elements</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-lg-12">
						<!-- general form elements -->


						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Horizontal Form</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->
							<div class="container col-md-12">
							<!-- <div class="row"> -->
							<!-- <div class="col-md-10"> -->
							<form class="form-horizontal" id="userForm">
								<div class="box-body">

									<!-- Custom form -->
									<div class="nav-tabs-custom">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#tab_1" data-toggle="tab">User
													Details</a></li>
											<li><a href="#tab_2" data-toggle="tab">Address Details</a></li>
											<li class="pull-right"><a href="#" class="text-muted"><i
													class="fa fa-gear"></i></a></li>
										</ul>
										<div class="tab-content">
											<!-- First Tab -->
											<div class="tab-pane active" id="tab_1">
												<!-- FIRST ROW -->							
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="loginid" class="col-sm-4 ">
														Login Id</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="loginid" placeholder="" required="required">
														</div>
													</div>
												</div>
												<!-- SECOND ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="name" class="col-sm-4">
														Name</label>
														<div class="col-sm-3">
															<select class="form-control" id="title" required="required">
															<option selected="selected">Mr.</option>
															<option>Mrs.</option>
															<option>Ms.</option>
															</select>
														</div>
														<div class="col-sm-5">
																<input type="text" class="form-control"
																id="firstname" placeholder="First Name" required="required">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<div class="col-sm-5">
															<input type="text" class="form-control"
																id="middlename" placeholder="Middle Name" >
														</div>
														<div class="col-sm-5">
															<input type="text" class="form-control"
																id="lastname" placeholder="Last Name" required="required">
														</div>
													</div>
												</div>
												<!-- THIRD ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="status" class="col-sm-4">
															Status</label>
														<div class="col-sm-8">
															<select class="form-control" id="status">
															<option>Active Status</option>
															<option>Deactive Status</option>
															<option>Locked Status</option>
															<option>Unlocked Status</option>
															</select>
														</div>
													</div>
												</div>
												<!-- FOURTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="category" class="col-sm-4 ">
															Category</label>
														<div class="col-sm-8">
															<select class="form-control" id="category" required="required">
                  											<option selected="selected">Select</option>
                  											<option>Navy</option>
                  											<option>Coast Guard</option>
                  											<option>DGS</option>
                  											<option>Shipping Company</option>
                  											<option>Port Authority</option>
                											</select>
														</div>
													</div>
												</div>
												<!-- FIFTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="vesseltype" class="col-sm-4">
															Basic Role</label>
														<div class="col-sm-8">
															<button type="button" class="btn btn-primary">SelectRole</button> 
														</div>
													</div>
												</div>
											</div>
											<!-- First Tab End-->
										<div class="tab-pane" id="tab_2">
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="plotno" class="col-sm-4 ">Building/Plot No</label>
													<div class="col-sm-8">
											 			<input type="text" class="form-control"
														id="plotno" required="required">
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="address1" class="col-sm-4">Address 1</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" 
														id="address1" required="required">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="address2" class="col-sm-4">Address 2</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" 
														id="address2" required="required">
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="address3" class="col-sm-4">Address 3</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" 
													id="address3" required="required">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="city" class="col-sm-4 ">City</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" id="city"
														required="required">
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="district" class="col-sm-4">District</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" id="district"
															required="required">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="landmark" class="col-sm-4 ">Landmark</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="landmark">
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="pincode" class="col-sm-4 ">Pin Code</label>
 													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="pincode" required="required">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="state" class="col-sm-4 ">State</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="state" required="required">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="country" class="col-sm-4 ">Country</label>
													<div class="col-sm-8">
														<select class="form-control" id="country" required="required">
                  											<option>India</option>
                  											<option>Sri Lanka</option>
                  											<option>USA</option>
                										</select>
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="telephoneno" class="col-sm-4 ">Telephone No</label>
 													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="telephoneno" required="required">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="mobileno" class="col-sm-4 ">Mobile No</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="mobileno">
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="fax" class="col-sm-4 ">Fax</label>
 													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="fax">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="emailid" class="col-sm-4 ">Email Id</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="emailid" required="required">
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="website" class="col-sm-4 ">Website</label>
 													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="website">
													</div>
												</div>
											</div>
										</div>
											<!-- /.tab-pane -->
									</div>
								</div>
									<!-- Custom form ?End-->
								</div>
								<!-- /.box -->
								<div class="box-footer">
								    <button type="button" class="btn btn-info pull-left btn-prev">Previous</button>
									<button type="button" class="btn btn-info pull-right btn-next">Next</button>
								</div>
							</form>
							</div>
							<!-- /.form -->
							</div>
							<!--/.col (left) -->
							</div>
							<!-- right column -->
							<!--/.col (right) -->
						</div>
						<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.4.0
			</div>
			<strong>Copyright &copy; 2014-2016 <a
				href="https://adminlte.io">Almsaeed Studio</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<jsp:include page="rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<spring:url value="/bower_components/jquery/dist/jquery.min.js"
		var="jqueryminjs" />
	<script src="${jqueryminjs}"></script>
	<!-- Bootstrap 3.3.7 -->
	<spring:url
		value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
		var="bootstrapminjs" />
	<script src="${bootstrapminjs}"></script>
	<!-- FastClick -->
	<spring:url value="/bower_components/fastclick/lib/fastclick.js"
		var="fastclickjs" />
	<script src="${fastclickjs}"></script>
	<!-- AdminLTE App -->

	<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
	<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
	<script src="${adminlteminjs}"></script>
	<!-- AdminLTE for demo purposes -->
	<spring:url value="/dist/js/demo.js" var="demojs" />
	<%-- <spring:url value="/resources/dist/js/demo.js" var="demojs" /> --%>
	<script src="${demojs}"></script>
	<!-- Table Script -->
	<!-- <script src="js/table_script.js"></script> -->
	<!-- Tab Script -->
	<script src="js/tab_script.js"></script>
</body>
</html>