<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<spring:url var="getCGBillableItemsService"
	value="/service/getCGBillableItemsService"></spring:url>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Send To DG" /></jsp:include>

<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
		<h1>Send Invoice to DGS</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Forms</a></li>
			<li class="active">General Elements</li>
		</ol>
	</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Send Invoice to DGS</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form:form class="form-horizontal" id="invoiceForm"
						modelAttribute="invoice">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<!-- <ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Invoice
												Details</a></li>
										<li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li>
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul> -->
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<!-- Include serviceCommanFields -->
										<jsp:include page="../include/serviceViewFields.jsp"></jsp:include>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="remarks" class="col-sm-4 ">Remarks</label>
												<div class="col-sm-8">
													<textarea class="form-control" id="remarks"
														placeholder="Remarks, if Any"
														name="billingServiceStatuses[0].remarks" required="true"
														pattern="[^]+" title="Taxes Should be Numberic Digits."></textarea>
												</div>
											</div>
										</div>
									</div>
									<!-- First Tab End-->
									<!-- /.tab-pane -->
								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
						<div class="box-footer">
							<form:input type="submit" class="btn btn-primary" path=""
								value="Send Invoice To DG"></form:input>
						</div>
					</form:form>

					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

<script src="/lritbilling//resources/js/billing.js"></script>
<script>
	$(document).ready(function() {
		$('.contractingGovernment').show();
		var billingServiceId = "${invoice.billingServiceId}";
		showContractingGovernment(billingServiceId);
	});

	function showContractingGovernment(billingServiceId) {
		if (billingServiceId == 0) {
			console.log("billingServiceId == 0");
		} else {
			var url = "${getCGBillableItemsService}";
			setBillableItemsDataTable(url, billingServiceId);
			//showTable(billingServiceId);
		}
	}

	/*
	function showTable(billingServiceId) {
		$('#contractingGovernment').css("display", "");
		var dtable = $('#contractingGovernment').DataTable({
			//dom: 'lBrtip',
			"bDestroy" : true,
			dom : 'lBfrtip',
			"ajax" : {
				"url" : "${getCGBillableItemsService}",
				"type" : "POST",
				async : false,
				"data" : {
					"billingServiceId" : billingServiceId
				},
				"dataSrc" : ""
			},
			"columns" : [ {
				"data" : "provider"
			}, {
				"data" : "consumer"
			}, {
				"data" : "billingPaymentBillableItemCategory.itemName"
			}, {
				"data" : "billingPaymentBillableItemCategory.rate"
			}, {
				"data" : "systemCount"
			}, {
				"data" : "cost"
			} ],
			"rowsGroup" : [// Always the array (!) of the column-selectors in specified order to which rows groupping is applied
			// (column-selector could be any of specified in https://datatables.net/reference/type/column-selector)
			0, 1 ],
			"responsive" : {
				"details" : {
					"type" : 'column',
					"target" : 'tr'
				}
			},
			"columnDefs" : [ ]
		});
	}
	 */
</script>

<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>