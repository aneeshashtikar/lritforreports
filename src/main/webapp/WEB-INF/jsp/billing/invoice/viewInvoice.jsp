<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="View Invoice" />
</jsp:include>

<spring:url var="generatedInvoiceURL" value="/invoice/generatedInvoice/"></spring:url>
<spring:url var="approvedInvoiceURL" value="/invoice/approvedInvoice/"></spring:url>
<spring:url var="viewAdviceDocument" value="/invoice/adviceDocument"></spring:url>

<style>
.messagesScrollable {
	height: 300px;
	overflow-y: scroll
}

.scrollable {
	height: 150px;
	overflow-y: scroll
}
</style>

<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">View Invoice</h3>
					</div>
					<form:form class="form-horizontal" id="cspBillForm"
						modelAttribute="invoice" enctype="multipart/form-data">
						<div class="box-body">
							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs"></ul>
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="billNumber" class="col-sm-4">Invoice No.</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="billNo"
														path="invoiceNo" placeholder="Bill No." readonly="true"></form:input>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="fromDate" class="col-sm-4 ">From</label>
												<div class="col-sm-8">
													<form:input type="text" data-provide="datepicker"
														class="form-control datepicker" id="fromDate"
														placeholder="From" path="fromDate" readonly="true" />

												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="toDate" class="col-sm-4">Till</label>
												<div class="col-sm-8">
													<form:input type="text" data-provide="datepicker"
														class="form-control datepicker" id="toDate"
														placeholder="Till" path="toDate" readonly="true" />
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="" class="col-sm-4">Bill Payee</label>
												<div class="col-sm-8">
													<form:input class="form-control" id=""
														path="billingContract.agency2Name" readonly="true" />
												</div>
											</div>
										</div>
										<!-- SECOND ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="billingContract" class="col-sm-4">Contract</label>
												<div class="col-sm-8">
													<form:input class="form-control" id="billingContract"
														placeholder="Contract"
														path="billingContract.contractNumber" readonly="true" />
												</div>
											</div>
										</div>
										<div class="box box-info contractingGovernments">
											<div class="box-header with-border">
												<h3 class="box-title" align="center">Message Charges</h3>
												<div class="box-tools pull-right">
													<button type="button" class="btn btn-box-tool"
														data-widget="collapse">
														<i class="fa fa-minus"></i>
													</button>
												</div>
											</div>
											<!-- /.box-header -->
											<div class="box-body messagesScrollable">
												<!-- messageTable  listInvoices-->
												<table id="messageTable"
													class="table table-bordered table-striped "
													style="width: 100%">
													<thead>
														<tr>
															<th>Provider</th>
															<th>Consumer</th>
															<th>Billable Item Category</th>
															<th>Unit Price</th>
															<th>Manual Count</th>
															<th>System Count</th>
															<th>Cost</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${invoice.billingBillableItems}"
															var="billableItem" varStatus="status">
															<tr>
																<td>${billableItem.providerName}</td>
																<td>${billableItem.consumerName}</td>
																<td>${billableItem.billingPaymentBillableItemCategory.itemName}</td>
																<td>${billableItem.rate}</td>
																<td>${billableItem.manualCount}</td>
																<td>${billableItem.systemCount}</td>
																<td>${billableItem.cost}</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<!-- /.table end -->
											</div>
											<!-- /.box-body -->
											<!-- <div class="box-footer clearfix"></div> -->
											<!-- /.box-footer -->
										</div>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="subTotal" class="col-sm-4 ">Sub Total</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="subTotal"
														placeholder="SubTotal" path="subTotal" readonly="true" />
												</div>
											</div>
										</div>
										<!-- FOURTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="levies" class="col-sm-4 "> Levies</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control levies"
														id="levies" placeholder="Levies" path="levies"
														readonly="true" />
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="taxes" class="col-sm-4 ">Taxes</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control taxes"
														id="taxes" placeholder="Taxes" path="taxes"
														readonly="true" />
												</div>
											</div>
										</div>
										<!-- FIFTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="grandTotal" class="col-sm-4 ">Grand
													Total</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control"
														id="grandTotal" placeholder="Grand Total"
														path="grandTotal" readonly="true" />
												</div>
											</div>
										</div>
										
										<!-- Status Details -->
										<c:if test="${not empty  invoice.billingServiceStatuses}">
											<div id="statusDetails" class="box box-info">
												<div class="box-header with-border">
													<h3 class="box-title" align="center">Invoice Status</h3>
													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool"
															data-widget="collapse">
															<i class="fa fa-minus"></i>
														</button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body scrollable">
													<table id="statusTable"
														class="table table-bordered table-striped "
														style="width: 100%">
														<thead>
															<tr>
																<th>Date</th>
																<th>Status</th>
																<th>Remarks</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${invoice.billingServiceStatuses}"
																var="billingStatuses" varStatus="status">
																<tr>
																	<td>${billingStatuses.date}</td>
																	<td>${billingStatuses.billingStatus.statusName}</td>
																	<td>${billingStatuses.remarks}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
													<!-- /.table end -->
												</div>
											</div>
										</c:if>
										<!-- Status Details Ends -->

										<!-- Generated Invoice Starts -->
										<c:if test="${not empty generatedInvoices}">
											<div id="generatedInvoices" class="box box-info">
												<div class="box-header with-border">
													<h3 class="box-title" align="center">Generated
														Invoices</h3>
													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool"
															data-widget="collapse">
															<i class="fa fa-minus"></i>
														</button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body scrollable">
													<table id="generatedInvoiceTable"
														class="table table-bordered table-striped "
														style="width: 100%">
														<thead>
															<tr>
																<th>Date</th>
																<th>Generated Invoice</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${generatedInvoices}"
																var="generatedInvoice" varStatus="status">
																<tr>
																	<td>${generatedInvoice.date}</td>
																	<td><a
																		href="${generatedInvoiceURL }${generatedInvoice.generatedInvoiceId}">Generated
																			Invoice_${generatedInvoice.generatedInvoiceId}</a></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</c:if>
										<!-- Generated Invoices End -->

										<!-- Approved Invoices Start -->
										<c:if test="${not empty approvedInvoices}">
											<div id="approvedInvoices" class="box box-info">
												<div class="box-header with-border">
													<h3 class="box-title" align="center">Approved Invoices</h3>
													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool"
															data-widget="collapse">
															<i class="fa fa-minus"></i>
														</button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body scrollable">
													<!-- messageTable  listInvoices-->
													<table id="approvedInvoiceTable"
														class="table table-bordered table-striped "
														style="width: 100%">
														<thead>
															<tr>
																<th>Date</th>
																<th>Approved Invoice</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${approvedInvoices}"
																var="approvedInvoice" varStatus="status">
																<tr>
																	<td>${approvedInvoice.date}</td>
																	<td><a
																		href="${approvedInvoiceURL }${approvedInvoice.approvedInvoiceId}">Approved
																			Invoice_${approvedInvoice.approvedInvoiceId}</a></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
													<!-- /.table end -->
												</div>
											</div>
										</c:if>
										<!-- Approved Invoices Ends -->

										<!-- Payment Details Start-->
										<c:if
											test="${not empty invoice.billingPaymentDetailsForServices}">
											<div id="paymentDetails" class="box box-info ">
												<div class="box-header with-border">
													<h3 class="box-title" align="center">Payment Details</h3>
													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool"
															data-widget="collapse">
															<i class="fa fa-minus"></i>
														</button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body scrollable">
													<!-- messageTable  listInvoices-->
													<table id="messageTable"
														class="table table-bordered table-striped "
														style="width: 100%">
														<thead>
															<tr>
																<th>Paid By</th>
																<th>Paid For</th>
																<th>Paid Amount</th>
																<th>Payment Date</th>
																<th>Transaction Information</th>
																<th>Payment Advice Document</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach
																items="${invoice.billingPaymentDetailsForServices}"
																var="paymentService" varStatus="status">
																<tr>
																	<td>${invoice.billingContract.agency2Name}</td>
																	<td><c:forEach
																			items="${paymentService.billingPaymentDetailsPerMembers}"
																			var="paymentMember">
      																${paymentMember.consumerName}, 
    															</c:forEach></td>
																	<td>${paymentService.paidAmount}</td>
																	<td>${paymentService.paymentDate}</td>
																	<td>${paymentService.transactionInfo}</td>
																	<td><a
																	href="${viewAdviceDocument}/${paymentService.pmtDetServiceId}">Payment Advice Document</a></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
													<!-- /.table end -->
												</div>
											</div>
										</c:if>
										<!-- Payment Details End -->

										<!-- Reminder Details Starts -->
										<c:if test="${not empty invoice.billingReminderLogs}">
											<div id="reminderDetails" class="box box-info">
												<div class="box-header with-border">
													<h3 class="box-title" align="center">Reminder Details</h3>
													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool"
															data-widget="collapse">
															<i class="fa fa-minus"></i>
														</button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body scrollable">
													<table id="reminderTable"
														class="table table-bordered table-striped "
														style="width: 100%">
														<thead>
															<tr>
																<th>Send Date</th>
																<th>Remarks</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${invoice.billingReminderLogs}"
																var="billingReminder" varStatus="status">
																<tr>
																	<td>${billingReminder.sendDate}</td>
																	<td>${billingReminder.remarks}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
													<!-- /.table end -->
												</div>
											</div>
										</c:if>
										<!-- Reminder Ends -->

									</div>
									<!-- First Tab End-->
								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
					</form:form>
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>


<jsp:include page="../../finalFooter.jsp"></jsp:include>