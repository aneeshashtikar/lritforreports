<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp"><jsp:param
		name="titleName" value="Mail To Agency" /></jsp:include>

<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
		<h1>Mail Invoice To Agency</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Forms</a></li>
			<li class="active">General Elements</li>
		</ol>
	</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Mail Invoice To Agency</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form class="form-horizontal" id="invoiceForm" method="POST">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<!-- <ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Invoice
												Details</a></li>
										<li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li>
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul> -->
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<!-- FIRST ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="ccEmailId" class="col-sm-4 ">CC</label>
												<div class="col-sm-8">
													<input type="email" class="form-control"  id="ccEmailId"
														name="ccEmailId" placeholder="CC Email Id" multiple>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="mailBody" class="col-sm-4 ">Mail Body</label>
												<div class="col-sm-8">
													<textarea type="text" class="form-control" id="mailBody"
														name="mailBody"
														placeholder="Invoice number, Airmail details of invoice dispatch, Signature 
															"
														required="required"></textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="remarks" class="col-sm-4 ">Remarks</label>
												<div class="col-sm-8">
													<textarea type="text" class="form-control" id="remarks"
														name="remarks" placeholder="Remarks, if Any"
														required="required"></textarea>
												</div>
											</div>
										</div>
									</div>
									<!-- First Tab End-->
								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="Send Mail" />
						</div>
					</form>

					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>


<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>