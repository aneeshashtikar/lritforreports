<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<spring:url var="getCGBillableItemsService"
	value="/service/getCGBillableItemsService"></spring:url>


<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Update Count" /></jsp:include>

<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
		<h1>Update Billable Item Count for the Invoice</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Forms</a></li>
			<li class="active">General Elements</li>
		</ol>
	</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Update Billable Item Count for the
							Invoice</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form:form class="form-horizontal" id="invoiceForm"
						modelAttribute="invoice">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<!-- <ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Invoice
												Details</a></li>
										<li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li>
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul> -->
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<!-- Include serviceCommanFields -->
										<%-- <jsp:include page="../include/serviceViewFields.jsp"></jsp:include> --%>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="fromDate" class="col-sm-4 ">From</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="fromDate"
														placeholder="From" path="fromDate" readonly="true" />

												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="toDate" class="col-sm-4">Till</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="toDate"
														placeholder="Till" path="toDate" readonly="true" />
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="" class="col-sm-4">Agency Name</label>
												<div class="col-sm-8">
													<%-- <input class="form-control" id="" value="${agencyName} "
															readonly="true" /> --%>
													<form:input class="form-control" id=""
														path="billingContract.agency2Name" readonly="true" />
												</div>
											</div>
										</div>
										<!-- SECOND ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="billingContract" class="col-sm-4">Contract</label>
												<div class="col-sm-8">
													<form:input class="form-control" id="billingContract"
														placeholder="Contract"
														path="billingContract.contractNumber" readonly="true" />
												</div>
											</div>
										</div>
										<div class="box box-info">
											<div class="box-header with-border">
												<h3 class="box-title" align="center">Message Charges</h3>
											</div>
											<!-- /.box-header -->
											<div class="box-body">
												<table id="contractingGovernmentManual"
													class="table table-bordered table-striped "
													style="width: 100%">
													<thead>
														<tr>
															<!-- <th data-data="null" ></th> -->
															<!-- <th data-data="null" data-default-content=""></th>		 -->
															<th>Provider</th>
															<th>Consumer</th>
															<th>Billable Item Category</th>
															<th>Unit Price</th>
															<th>Count</th>
															<th>Cost</th>
															<!-- <th data-data="speed" data-default-content="NA">Total USD</th> -->
														</tr>
														<c:forEach items="${invoice.billingBillableItems}"
															varStatus="i" var="items">
															<tr>
																<td><form:hidden
																		path="billingBillableItems[${i.index}].billableItemId" />
																	<%-- <form:input path="billingBillableItems[${i.index}].provider" readonly="true"/> --%>
																	${invoice.billingBillableItems[i.index].provider} -
																	${invoice.billingBillableItems[i.index].providerName}</td>
																<td>
																	<%-- <form:input path="billingBillableItems[${i.index}].consumer" readonly="true"/> --%>
																	${invoice.billingBillableItems[i.index].consumer} -
																	${invoice.billingBillableItems[i.index].consumerName}
																</td>
																<td>
																	<%-- <form:input path="billingBillableItems[${i.index}].billingPaymentBillableItemCategory.itemName" readonly="true"/> --%>
																	${invoice.billingBillableItems[i.index].billingPaymentBillableItemCategory.itemName}
																</td>
																<td><form:input class="form-control"
																		id="rate_${i.index }"
																		path="billingBillableItems[${i.index}].rate"
																		readonly="true" /></td>
																<td><form:input class="form-control count"
																		id="manualcount_${i.index }" alt="${i.index }"
																		path="billingBillableItems[${i.index}].manualCount" /></td>
																<td><form:input class="form-control bcost"
																		id="cost_${i.index }"
																		path="billingBillableItems[${i.index}].cost"
																		readonly="true" /></td>
															</tr>
														</c:forEach>
														<%-- <c:forEach items="${hashedBillableItems}"
																varStatus="provider" var="providerBillable">
																<c:forEach items="${providerBillable.value}"
																	varStatus="consumer" var="consumerBillable">
																	<tr>
																		<!-- <td>Provider</td>
																		<td>Consumer</td>
																		<td>Billable Item Category</td>
																		<td>Unit Price</td>
																		<td>Count</td>
																		<td>Cost</td> -->
																		<td> ${consumerBillable.valuec} - ${consumerBillable.key}</td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																	</tr>
																</c:forEach>
															</c:forEach> --%>
													</thead>
												</table>
												<!-- /.table end -->
											</div>
											<!-- /.box-body -->
											<div class="box-footer clearfix"></div>
											<!-- /.box-footer -->
										</div>
										<!-- Subtotal Rows -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="subTotal" class="col-sm-4 ">Sub Total</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="subTotal"
														placeholder="SubTotal" path="subTotal" readonly="true" />
												</div>
											</div>
										</div>
										<!-- FOURTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="levies" class="col-sm-4 "> Levies</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="levies"
														placeholder="Levies" path="levies" readonly="true" />
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="taxes" class="col-sm-4 ">Taxes</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="taxes"
														placeholder="Taxes" path="taxes" readonly="true" />
												</div>
											</div>
										</div>
										<!-- FIFTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="grandTotal" class="col-sm-4 ">Grand
													Total</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control"
														id="grandTotal" placeholder="Grand Total"
														path="grandTotal" readonly="true" />
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="remarks" class="col-sm-4 ">Remarks</label>
												<div class="col-sm-8">
													<!-- <textarea type="text" class="form-control" id="remarks"
															name="remarks" placeholder="Remarks, if Any"></textarea> -->
													<textarea type="text" class="form-control" id="remarks"
														name="billingServiceStatuses[0].remarks"
														placeholder="Remarks, if Any" required="required"></textarea>
												</div>
											</div>
										</div>
									</div>
									<!-- First Tab End-->
									<!-- /.tab-pane -->
								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
						<div class="box-footer">
							<form:input type="submit" class="btn btn-primary" path=""
								value="Update Count"></form:input>
						</div>
					</form:form>

					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<spring:url var="billingjs" value="/resources/js/billing.js"></spring:url>
<script src="${billingjs}"></script>
<script>
	$(document).ready(function() {
		$('.count').keyup(calculateMessageCost);
	});
</script>


<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>