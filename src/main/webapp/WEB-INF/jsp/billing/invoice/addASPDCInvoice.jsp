<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Invoice Generation" />
</jsp:include>


<!-- Spring URL   -->
<spring:url var="getAgencys" value="/contract/getAgencys"></spring:url>
<spring:url var="getContract" value="/contract/getContractOnContractId"></spring:url>
<spring:url var="getAgencyWiseContracts"
	value="/contract/getAgencyWiseContracts"></spring:url>
<spring:url var="getBillableItems" value="/invoice/getBillableItems"></spring:url>
<spring:url var="billingjs" value="/resources/js/billing.js"></spring:url>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Invoice Form</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<!-- <div class="row"> -->
					<!-- <div class="col-md-10"> -->

					<!-- Form -->
					<form:form class="form-horizontal" id="invoiceForm"
						onsubmit="return validateInvoice()" modelAttribute="service"
						enctype="multipart/form-data">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<!-- <ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Invoice
												Details</a></li>
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul> -->
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<!--
											<div class="alert alert-success" role="alert">...</div>
											<div class="alert alert-info" role="alert">...</div>
											<div class="alert alert-warning" role="alert">...</div>
											<div class="alert alert-danger" role="alert">...</div>
											 -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="contractType" class="col-sm-4 ">Contract
													Type</label>
												<div class="col-sm-8">
													<select class="form-control" id="contractType"
														required="required">
														<c:forEach var="contract" items="${contractTypes}">
															<option value="${contract.contractTypeId}"
																<c:if test="${contract.contractTypeId == service.billingContract.billingContractType.contractTypeId}">selected</c:if>>${contract.typename}</option>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>

										<!-- Include serviceCommanFields -->
										<jsp:include page="../include/serviceCommanFields.jsp"></jsp:include>

										<input type="hidden" id="thresholdAmount" />

									</div>
									<!-- First Tab End-->
								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
						<div class="box-footer">
							<form:input type="submit" class="btn btn-primary" path=""
								value="Save Invoice"></form:input>
						</div>
						<!-- /.box -->
					</form:form>

					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script src="${billingjs}"></script>
<script type="text/javascript">
	$(function() {
		$("#fromDate").datepicker({
			numberOfMonths : 2,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() + 1);
				$("#toDate").datepicker("option", "minDate", dt);
			}
		});
		$("#toDate").datepicker({
			numberOfMonths : 2,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() - 1);
				$("#fromDate").datepicker("option", "maxDate", dt);
			}
		});
	});

	function validateDateRange() {
		$("#fromDate").datepicker({
			numberOfMonths : 2,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() + 1);
				$("#toDate").datepicker("option", "minDate", dt);
			}
		});
		$("#toDate").datepicker({
			numberOfMonths : 2,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() - 1);
				$("#fromDate").datepicker("option", "maxDate", dt);
			}
		});
	}
</script>
<script>
	$(document).ready(function() {
		$('.contractingGovernment').hide();
		$('.contractingGovernment').show();

		var contractNo = "${service.billingContract.contractId}";
		if (contractNo != "") {
			setBillableItems(contractNo);
		}

		validateDateRange();

	});

	function showContracts() {
		var contractType = $('#contractType').val();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		var agencyCode = $("#agency").val();

		console.log(agencyCode)

		var url = "${getAgencyWiseContracts}";
		var data = {
			"fromDate" : fromDate,
			"toDate" : toDate,
			"contractType" : contractType,
			"agencyCode" : agencyCode
		};

		addContracts(url, data);

	}

	function setAgencys() {

		var contractType = $('#contractType').val();

		var url = "${getAgencys}";
		var data = {
			"contractType" : contractType
		};

		addAgency(url, data);
	}

	function setBillableItems(contractNo) {

		var contractId = $('#billingContract').val();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();

		//var url = "${getCGBillableItems}";
		var url = "${getBillableItems}";
		var data = {
			"contractId" : contractId,
			"fromDate" : fromDate,
			"toDate" : toDate
		};

		//addBillableMessages(url, data);
		addInvoiceBillableItems(url, data);
		//validateInvoice(contractId);
	}

	function validateInvoice() {
		console.log(' Valid Contract');

		var contractId = $('#billingContract').val();
		var url = "${getContract}";
		var data = {
			"contractId" : contractId
		};

		var result = getJSONData(url, data);

		if (result.hasOwnProperty('thresholdAmount')) {

			var thresholdAmount = document.getElementById('thresholdAmount');
			thresholdAmount.value = result.thresholdAmount;

			var subTotal = document.getElementById('subTotal').value;
			if (subTotal < result.thresholdAmount)
				return confirm('Messaging cost is less than thresholdAmount '
						+ result.thresholdAmount
						+ '\n Still Want to Generate Invoice');

		}

		return true;
	}

	//$("#fromDate,#toDate").change(setAgencys);

	//$("#agency").change(setCGAgencyContracts);

	$('#contractType').change(setAgencys);

	$('#billingContract').change(setBillableItems);

	$('#subTotal,#levies,#taxes').change(grandTotalFn);

	$('#subTotal,#levies,#taxes').keypress(grandTotalFn);
</script>


<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>