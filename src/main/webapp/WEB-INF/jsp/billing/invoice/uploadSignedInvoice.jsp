<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Upload Signed Invoice" />
</jsp:include>


<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
<script>
	//TO OPTIMIZE THE SAME CODE
	function showErrorForElement(error, elementName) {
		if (error != null) {
			document.getElementById(elementName + "error").innerHTML = error;
			return false;
		} else {
			document.getElementById(elementName + "error").innerHTML = "";
			return true;
		}
	}

	function isEmpty(str) {
		return !str.replace(/\s+/, '').length;
	}

	function FileValidation(elementName) {
		//var file = document.getElementById("approvedCopy").value;
		var file = document.getElementById(elementName).value;
		var error = null;

		if (isEmpty(file)) {
			error = "upload file";
		} else {
			var Extension = file.substring(file.lastIndexOf('.') + 1)
					.toLowerCase();
			console.log("Extension " + Extension);
			if (Extension != "pdf") {
				error = "Upload pdf file";
			}
		}
		return showErrorForElement(error, elementName);
	}

	function validation() {
		return FileValidation('approvedCopy');
	}
</script>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
		<h1>Upload Signed Invoice</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Forms</a></li>
			<li class="active">General Elements</li>
		</ol>
	</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Upload Signed Invoice</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form:form class="form-horizontal" id="invoiceForm" method="POST"
						onsubmit="return validation();" modelAttribute="serviceStatus"
						enctype="multipart/form-data">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<!-- <ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Invoice
												Details</a></li>
										<li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li>
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul> -->
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<!-- FIRST ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="approvedCopy" class="col-sm-4">Approved
													Copy</label>
												<div class="col-sm-8">
													<input type="file" id="approvedCopy" class="form-control"
														name="approvedCopyFile" />
													<p class="help-block">Upload Signed/Approved Invoice.</p>
													<div class="error " id="approvedCopyerror"></div>
													<form:errors path="date" cssClass="error" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="remarks" class="col-sm-4 ">Remarks</label>
												<div class="col-sm-8">
													<form:textarea type="text" class="form-control"
														id="remarks" path="remarks" placeholder="Remarks, if Any"
														required="required"></form:textarea>
												</div>
											</div>
										</div>
									</div>
									<!-- First Tab End-->
								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="Upload" />
						</div>
					</form:form>

					<!-- /.form -->		
					
					<!-- </div> -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>


<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>