<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Send To DG" />
</jsp:include>

<!-- SPRING urls  -->
<spring:url var="uploadedInvoice"
	value="/invoice/uploaded/${billingServiceID }"></spring:url>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Send Invoice to DGS</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<form:form class="form-horizontal" id="invoiceForm"
						modelAttribute="invoice">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="generatedInvoice" class="col-sm-4">Generated
													Invoice</label>
												<div class="col-sm-8">
													<label><a href="${uploadedInvoice }">Click to
															View</a></label>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="remarks" class="col-sm-4 ">Remarks</label>
												<div class="col-sm-8">
													<textarea class="form-control" id="remarks"
														placeholder="Remarks, if Any"
														name="billingServiceStatuses[0].remarks" required="true"
														pattern="[^]+" title="Taxes Should be Numberic Digits."></textarea>
												</div>
											</div>
										</div>
									</div>
									<!-- First Tab End-->
									<!-- /.tab-pane -->
								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
						<div class="box-footer">
							<form:input type="submit" class="btn btn-primary" path=""
								value="Send Invoice To DG"></form:input>
						</div>
					</form:form>
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>