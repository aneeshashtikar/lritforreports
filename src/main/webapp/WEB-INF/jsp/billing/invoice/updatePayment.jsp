<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<spring:url var="getCGBillableItemsService"
	value="/service/getCGBillableItemsService"></spring:url>
<spring:url var="billingjs" value="/resources/js/billing.js"></spring:url>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Update Payment" /></jsp:include>


<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
		<h1>Update Payment Details of the Invoice</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Forms</a></li>
			<li class="active">General Elements</li>
		</ol>
	</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Update Payment Details of the Invoice</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form:form class="form-horizontal" id="invoiceForm"
						modelAttribute="invoice" enctype="multipart/form-data">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<!-- <ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Invoice
												Details</a></li>
										<li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li>
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul> -->
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<!-- Include serviceCommanFields -->
										<%-- <jsp:include page="../include/serviceViewFields.jsp"></jsp:include> --%>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="fromDate" class="col-sm-4 ">From</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="fromDate"
														placeholder="From" path="fromDate" readonly="true" />

												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="toDate" class="col-sm-4">Till</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="toDate"
														placeholder="Till" path="toDate" readonly="true" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="" class="col-sm-4">Agency Name</label>
												<div class="col-sm-8">
													<form:input class="form-control" id=""
														path="billingContract.agency2Name" readonly="true" />
												</div>
											</div>
										</div>
										<!-- SECOND ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="billingContract" class="col-sm-4">Contract</label>
												<div class="col-sm-8">
													<form:input class="form-control" id="billingContract"
														placeholder="Contract"
														path="billingContract.contractNumber" readonly="true" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="amountPaid" class="col-sm-4 ">Amount
													Paid</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control"
														id="amountPaid" placeholder="Amount Paid"
														path="billingPaymentDetailsForServices[0].paidAmount"
														autocomplete="off" pattern="\d+(\.\d{1,20})?$"
														title="Amount Paid Should be Numberic Digits."
														required="required" />
												</div>
												<div class="col-sm-6">
													<form:errors path="grandTotal" cssClass="error" />
												</div>
											</div>
										</div>
										<div class="box box-info">
											<div class="box-header with-border">
												<h3 class="box-title" align="center">Select Consumer</h3>
											</div>
											<!-- /.box-header -->
											<div class="box-body">
												<table id="contractingGovernmentManual"
													class="table table-bordered table-striped "
													style="width: 100%">
													<thead>
														<tr>
															<th></th>
															<th>Consumer</th>
															<th>Total USD</th>
															<!-- <th>Amount Paid</th> -->
														</tr>
													</thead>
													<tbody>
														<c:forEach
															items="${invoice.billingPaymentDetailsForServices}"
															varStatus="pservice" var="paymentService">
															<c:forEach
																items="${invoice.billingPaymentDetailsForServices[pservice.index].billingPaymentDetailsPerMembers}"
																varStatus="member" var="paymentMembers">
																<tr>
																	<td><input type="checkbox" checked="checked"
																		disabled="disabled"></td>
																	<td>${paymentMembers.consumer}-
																		${paymentMembers.consumerName}</td>
																	<td>${paymentMembers.cost}</td>
																	<%-- <td><form:input type="text" class="form-control"
																				path="billingPaymentDetailsForServices[${pservice.index}].billingPaymentDetailsPerMembers[${member.index}].paidAmount"
																				readonly="true" /></td> --%>
																</tr>
															</c:forEach>
														</c:forEach>
														<c:forEach items="${unCheckedMembers}" varStatus="member"
															var="paymentMembers">
															<tr>
																<%-- ${serviceCount} is size of paymentservice --%>
																<td><input type="checkbox" name="consumers"
																	value="${paymentMembers.value.consumer}"></td>
																<td>${paymentMembers.value.consumer}-
																	${paymentMembers.value.consumerName}<input
																	type="hidden"
																	name="billingPaymentDetailsForServices[0].billingPaymentDetailsPerMembers[${member.index}].consumer"
																	value="${paymentMembers.value.consumer}" />
																</td>
																<td><label
																	id="cost_${paymentMembers.value.consumer}">${paymentMembers.value.cost}</label><input
																	type="hidden"
																	name="billingPaymentDetailsForServices[0].billingPaymentDetailsPerMembers[${member.index}].cost"
																	value="${paymentMembers.value.cost}" /> <input
																	type="hidden" class="form-control"
																	name="billingPaymentDetailsForServices[0].billingPaymentDetailsPerMembers[${member.index}].paidAmount"
																	value="${paymentMembers.value.cost}" /></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<!-- /.table end -->
											</div>
											<!-- /.box-body -->
											<div class="box-footer clearfix"></div>
											<!-- /.box-footer -->
										</div>

										<!-- FOURTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="levies" class="col-sm-4 ">Levies</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="levies"
														placeholder="Levies" path="levies" readonly="true" />
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="taxes" class="col-sm-4 ">Taxes</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="taxes"
														placeholder="Taxes" path="taxes" readonly="true" />
												</div>
											</div>
										</div>
										<!-- FIFTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="grandTotal" class="col-sm-4 ">Grand
													Total</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control"
														id="grandTotal" placeholder="Grand Total"
														path="grandTotal" readonly="true" />
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="remainedAmount" class="col-sm-4 ">Remaining
													Amount</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="remainedAmount"
														value="${remainedAmount}" readonly="true" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="transactionInfo" class="col-sm-4 ">Transaction
													Information</label>
												<div class="col-sm-8">
													<textarea type="text" class="form-control"
														id="transactionInfo"
														name="billingPaymentDetailsForServices[0].transactionInfo"
														placeholder="Transaction information" required="required"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-6">
											<label for="remarks" class="col-sm-4 ">Remarks</label>
											<div class="col-sm-8">
												<textarea type="text" class="form-control" id="remarks"
													name="billingServiceStatuses[0].remarks"
													placeholder="Remarks, if Any" required="required"></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-6">
											<label for="paymentAdviceDocument" class="col-sm-4">Payment Advice Document </label>
											<div class="col-sm-8">
												<input type="file" id="paymentAdviceDocument" class="form-control"
													name="paymentAdviceDocument" />
												<p class="help-block">Upload Payment Advice Document.</p>
											</div>
										</div>
										<div class="form-group col-sm-6">
												<label for="taxes" class="col-sm-4 ">Payment Date</label>
												<div class="col-sm-8">
													<input type="text" class="form-control datepicker" id="paymentDate"
														placeholder="Payment Date" name="paymentDate" autocomplete="off"  />
												</div>
											</div>
									</div>
									<!-- First Tab End-->
									<!-- /.tab-pane -->
								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
						<div class="box-footer">
							<form:input type="submit" class="btn btn-primary" path=""
								value="Payment "></form:input>
						</div>
					</form:form>
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<spring:url var="billingjs" value="/resources/js/billing.js"></spring:url>
<script src="${billingjs}"></script>
<script>
	$(document).ready(function() {
		//$('.count').keyup(calculateMessageCost);
	});
</script>

<script>
	function validatePayment() {

		var consumers = $('input[name="consumers"]').not(":disabled");
		var remainedAmount = parseFloat($('#remainedAmount').val());
		//console.log(consumers);
		if (consumers.length > 0) {
			var checked = false;
			var paidAmount = parseFloat($('#amountPaid').val());
			var selectedCountryAmount = 0;

			var index = 0;
			for (; index < consumers.length; index++) {
				if (consumers[index].checked == true) {
					console.log(consumers[index].value);
					var countryLritId = consumers[index].value;
					console.log("PAid Amount " + countryLritId);
					selectedCountryAmount += parseFloat($(
							'#cost_' + countryLritId).text());
					checked = true;
				}
			}
			if (!checked) {
				alert("Select at least 1 Consumer");
				return false;
			}

			if (paidAmount < selectedCountryAmount) {
				alert("Entered Amount is Less than selected agency");
				return false;
			}
		} else {
			if ( paidAmount <= 0 ) {
				alert("Entered Amount Should greater than zero");	
			}
		}

		if (remainedAmount < paidAmount) {
			alert("Entered Amount Should be less than Remaining Amount");
			return false;
		}

		return true;
	}

	$(function() {
		$("#paymentDate").datepicker({
			format : 'yyyy-mm-dd',
			 autoclose: true
		});
	});
	
</script>


<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>