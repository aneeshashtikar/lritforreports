<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp"><jsp:param
		name="titleName" value="List Invoice" /></jsp:include>


<spring:url var="showInvoiceList" value="/invoice/listInvoice"></spring:url>

<!-- URLS FOR INVOICE ACTIONS -->
<spring:url var="sendToDG" value="/invoice/sendInvoice/"></spring:url>
<spring:url var="uploadSignedInvoice"
	value="/invoice/uploadSignedInvoice/"></spring:url>
<spring:url var="sendMail" value="/invoice/mailInvoice/"></spring:url>
<spring:url var="updateCount" value="/invoice/updateCount/"></spring:url>
<spring:url var="updatePayment" value="/invoice/updatePayment/"></spring:url>
<spring:url var="sendReminder" value="/invoice/sendReminder/"></spring:url>
<spring:url var="viewInvoice" value="/invoice/viewInvoice/"></spring:url>

<!-- Content Wrapper. Contains page content -->
<script type="text/javascript">
	function showInvoices() {
		//alert("Hie");
		showTable();
	}
	function showTable() {
		$('#listInvoices').css("display", "");
		var contractType = document.getElementById('contractType').value;
		var fromDate = document.getElementById('fromDate').value;
		var toDate = document.getElementById('toDate').value;
		var dtable = $('#listInvoices')
				.DataTable(
						{
							//dom: 'lBrtip',
							dom : 'lBfrtip',
							"destroy": true,
							"ajax" : {
								"url" : "${showInvoiceList}",
								"type" : "POST",
								"data" : {
									'contractType' : contractType,
									'fromDate' : fromDate,
									'toDate' : toDate
								},
								"dataSrc" : ""
							},
							"columns" : [
									{
										"data" : "invoiceBillNo"
									},
									{
										"data" : "date"
									},
									{
										"data" : "fromDate"
									},
									{
										"data" : "toDate"
									},
									{
										"data" : "status"
									},
									{
										"searchable" : false,
										"orderable" : false,
										"render" : function(data, type, row) {
											var updateCount = "<a href='${updateCount}" + row.billingServiceId + "' target='_blank'>Update Count</a>";
											var updatePayment = "<a href='${updatePayment}" + row.billingServiceId + "' target='_blank'>Update Payment</a>";
											var sendReminder = "<a href='${sendReminder}" + row.billingServiceId + "' target='_blank'>Send Reminder</a>";
											var viewInvoice = "<a href='${viewInvoice}" + row.billingServiceId + "' target='_blank'>View Invoice</a>";

											if (row.lastStatus == 1
													|| row.lastStatus == 2) {
												return "<a href='${sendToDG}" + row.billingServiceId + "' target='_blank'>Send_To_DG</a>"
														+ "<br>" + viewInvoice;
											} else if (row.lastStatus == 3) {
												return "<a href='${uploadSignedInvoice}" + row.billingServiceId + "' target='_blank'>Upload_Signed_Invoice</a>"
														+ "<br>" + viewInvoice;
											} else if (row.lastStatus == 4) {
												return "<a href='${sendMail}" + row.billingServiceId + "' target='_blank'>Send Mail</a>"
														+ "<br>" + viewInvoice;
											} else if (row.lastStatus == 5) {
												return updateCount + "<br>"
														+ updatePayment
														+ "<br>" + sendReminder
														+ "<br>" + viewInvoice;
											} else if (row.lastStatus == 7) {
												return updatePayment + "<br>"
														+ sendReminder + "<br>"
														+ viewInvoice;
											} else {
												return viewInvoice;
											}
										}
									} ],
							"responsive" : {
								"details" : {
									"type" : 'column',
									"target" : 'tr'
								}
							},
							"buttons" : [ 'csv', 'excel', 'pdf' ]
							
						});
	}
</script>
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<!-- Small boxe (Stat box) -->
		<div class="row">
			<div class="col-md-10">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Invoices</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<!-- form start -->
					<div class="box-body">

						<!-- Include alertMessages  -->
						<jsp:include page="../include/alertMessages.jsp"></jsp:include>

						<div class="row">
							<div class="form-group col-sm-6">
								<label for="contractType" class="col-sm-4 ">Contract
									Type</label>
								<div class="col-sm-8">
									<select class="form-control" id="contractType">
										<c:forEach var="contract" items="${contractTypes}">
											<option value="${contract.contractTypeId}">${contract.typename}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-sm-6">
								<label for="fromDate" class="col-sm-4 ">Invoice From</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker"
										id="fromDate" placeholder="Invoice From" />
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label for="toDate" class="col-sm-4">Invoice Till</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker" id="toDate"
										placeholder="Invoice Till" />
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<input type="button" class="btn btn-primary" id="listButton"
							onclick="showInvoices(); return false;" value="List Invoices" />
					</div>
					<!-- /.form -->
				</div>
				<div class="box">
					<!-- Report Table -->
					<div class="box-body">
						<table id="listInvoices"
							class="table table-bordered table-striped "
							style="display: none; width: 100%">
							<thead>
								<tr>
									<!-- <th data-data="null" ></th> -->
									<th data-data="invoiceNo" data-default-content="NA">Invoice
										Number</th>
									<th data-data="date" data-default-content="NA">Invoice
										Date</th>
									<th data-data="fromDate" data-default-content="NA">Generated
										From</th>
									<th data-data="toDate" data-default-content="NA">Generated
										Till</th>
									<th data-data="status" data-default-content="NA">Status</th>
									<th data-data="action" data-default-content="NA">Action</th>
								</tr>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>

			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
		</div>
	</section>
</div>
<script>
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy-mm-dd',
			 autoclose: true
		});
	});
</script>


<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>