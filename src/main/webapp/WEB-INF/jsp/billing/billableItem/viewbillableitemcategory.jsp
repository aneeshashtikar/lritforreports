<%@page import="in.gov.lrit.billing.model.contract.ContractType"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="View Billable Item" /></jsp:include>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
				<h1>View Billable Item Category</h1>
				<ol class="breadcrumb">
					<li><a href="/lritbilling/"><i class="fa fa-dashboard"></i> Home</a></li>
				</ol>
			</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">View Billable Item Category</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<!-- <div class="row"> -->
					<!-- <div class="col-md-10"> -->
					<form:form class="form-horizontal" id="billableItemCategory"
						modelAttribute="billableItemCategory">
						<div class="box-body">
							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<!-- <ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1" data-toggle="tab">Item
														Category Details</a></li>
												<li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li>
												<li class="pull-right"><a href="#" class="text-muted"><i
														class="fa fa-gear"></i></a></li>
											</ul> -->
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="fromDate" class="col-sm-4 ">Contract
													Type</label>
												<div class="col-sm-8">
													<form:select class="form-control"
														path="billingContractType.contractTypeId"
														id="contractTypeId">
														<form:option value="0" selected="selected">-- Select Contract Type --</form:option>
														<form:options items="${contractTypeList }"
															itemValue="contractTypeId" itemLabel="typename" />
													</form:select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="fromDate" class="col-sm-4 ">Select
													Category : </label>
												<div class="col-sm-8">
													<form:select class="form-control"
														path="billableItemCategoryId" id="billableItemCategoryId">

													</form:select>
												</div>
											</div>
										</div>
										<div class="row" id="billableItemCategoryTableRow">
											<div class="box">
												<!-- /.box-header -->
												<div class="box-body">
													<!-- Table added for shipbourne equipment details -->
													<%-- <table class="table no-margin"
														id="billableItemCategoryTable">
														<thead>
															<tr>

																<th data-data="item_name" data-default-content="NA">Billable
																	Item Category Name</th>

																<th data-data="rate" data-default-content="NA">Rate</th>

																<th data-data="valid_from" data-default-content="NA">Valid
																	From</th>

																<th data-data="valid_till" data-default-content="NA">Valid
																	Till</th>

															</tr>
														</thead>
														<tbody id="tBody">

															<tr id="trBillableItem">
																<td data-data="item_name" data-default-content="NA"><form:input
																		path="itemName" class="form-control billitems"
																		id="itemName" required="required" /></td>

																<td data-data="rate" data-default-content="NA"><form:input
																		path="rate" class="form-control billitems" id="rate"
																		required="required" /></td>

																<td data-data="valid_from" data-default-content="NA"><form:input
																		path="validFrom"
																		class="form-control datepicker billitems"
																		id="valid_from" required="required" /></td>

																<td data-data="valid_till" data-default-content="NA">
																	<form:input path="validTill"
																		class="form-control datepicker billitems"
																		id="valid_till" required="required" />
																</td>
															</tr>
														</tbody>
													</table> --%>
													<table id="listBillableItemCategoryLogs"
														class="table table-bordered table-striped "
														style="display: none; width: 100%">
														<thead>
															<tr>
																<th data-data="invoiceNo" data-default-content="NA">Rate</th>
																<th data-data="date" data-default-content="NA">Entry
																	Date</th>
																<th data-data="fromDate" data-default-content="NA">Valid
																	From</th>
																<th data-data="toDate" data-default-content="NA">Valid
																	Till</th>
																<th data-data="status" data-default-content="NA">Remark</th>
																<th data-data="edit" data-default-content="NA">edit</th>
															</tr>
														</thead>
													</table>
													<!-- /.table end -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.box-body -->
						<%-- <div class="box-footer clearfix">
										<form:input type="submit" class="btn btn-primary" path=""
											value="Save Billable Category"></form:input>
									</div> --%>
						<!-- /.box-footer -->
					</form:form>
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
	</section>
	<!-- content  -->
</div>
<!-- content-wrapper -->
<spring:url var="getBillableItemCategory"
	value="/billable/getBillableItemCategory"></spring:url>
<spring:url var="getBillableItemCategoryDetails"
	value="/billable/getBillableItemCategoryDetails"></spring:url>
<spring:url var="getBillableItemCategoryLogsDetails"
	value="/billable/getBillableItemCategoryLogsDetails"></spring:url>
<spring:url var="updateBillableCategoryLog"
	value="/billable/updateBillableCategoryLog/"></spring:url>
<script type="text/javascript">
	$(document).ready(function() {
		$('#billableItemCategoryTableRow').hide();
		$('.billitems').prop("readonly", true);
		$('.billitems').prop("disabled", true);
	});
</script>
<script>
	/* 
	$( ".datepicker" ).datepicker({
		  changeYear: true,
		  changeMonth: true
		}); */

	$('#contractTypeId')
			.change(
					function() {
						var contractTypeId = $('#contractTypeId').val();
						$('#billableItemCategoryId').find('option').remove();
						$
								.ajax({
									url : "${getBillableItemCategory}",
									async : false,
									data : {
										"contractTypeId" : contractTypeId
									},
									success : function(result, textStatus, xhr) {
										console.log(result);
										console.log(textStatus);
										console.log(xhr);
										$('#billableItemCategoryId')
												.append(
														$("<option></option>")
																.attr("value",
																		"0")
																.text(
																		"--- Select BillableItemCategory ---"));
										$
												.each(
														result,
														function(key, value) {
															$(
																	'#billableItemCategoryId')
																	.append(
																			$(
																					"<option></option>")
																					.attr(
																							"value",
																							value.billableItemCategoryId)
																					.text(
																							value.itemName));
														});
									}
								});
					});

	function setBillableItemCategoryLogs(tableData) {
		
		if(tableData == ""){
			tableData = [];
		}
		$('#listBillableItemCategoryLogs').css("display", "");
		$('#listBillableItemCategoryLogs').DataTable({
			dom : 'lBfrtip',
			data : tableData,
			"columns" : [ {
				"data" : "rate"
			}, {
				"data" : "entryDate"
			}, {
				"data" : "validFrom"
			}, {
				"data" : "validTill"
			}, {
				"data" : "remark"
			} ,{
				"searchable" : false,
				"orderable" : false,
				"render" : function(data, type, row) {
					return "<a href='${updateBillableCategoryLog}" + row.paymentBillableItemCategoryLogId + "' target='_blank'>Edit</a>";
				}
			}],
			"responsive" : {
				"details" : {
					"type" : 'column',
					"target" : 'tr'
				}
			},
			"buttons" : [ 'csv', 'excel', 'pdf' ],
			"bDestroy" : true
		});

	}

	$('#billableItemCategoryId').change(function() {
		var billableItemCategoryId = $('#billableItemCategoryId').val();
		$('.billitems').val('');
		$.ajax({
			url : "${getBillableItemCategoryLogsDetails}",
			async : false,
			data : {
				"billableItemCategoryId" : billableItemCategoryId
			},
			statusCode : {
				500 : function() {
					$('#billableItemCategoryTableRow').hide();
				}
			},
			success : function(result, textStatus, xhr) {
				
				console.log(result);
				setBillableItemCategoryLogs(result);
				$('#billableItemCategoryTableRow').show();

				/* $('#itemName').val(result.itemName);
				$('#rate').val(result.rate);
				$('#valid_from').val(result.validFrom);
				$('#valid_till').val(result.validTill);
				$('#remarks').val(result.remarks); */
			}
		});

	});

	$(function() {
		$("#valid_from").datepicker({
			dateFormat : 'yy-mm-dd',
			changeYear : true,
			changeMonth : true,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() + 1);
				$("#valid_till").datepicker("option", "minDate", dt);
			}
		});
		$("#valid_till").datepicker({
			dateFormat : 'yy-mm-dd',
			changeYear : true,
			changeMonth : true,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() - 1);
				$("#valid_from").datepicker("option", "maxDate", dt);
			}
		});
	});
</script>



<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>