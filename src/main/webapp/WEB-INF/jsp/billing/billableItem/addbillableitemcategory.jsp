<%@page import="in.gov.lrit.billing.model.contract.ContractType"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Add Billable Item" />
</jsp:include>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Add Billable Item Category</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form:form class="form-horizontal" id="billableItemCategory"
						onsubmit="return validateForm();"
						modelAttribute="billableItemCategory">
						<div class="box-body">
							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">

										<!-- Include alertMessages  -->
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="fromDate" class="col-sm-4 ">Contract
													Type</label>
												<div class="col-sm-8">
													<form:select class="form-control"
														path="billingContractType.contractTypeId">
														<form:options items="${contractTypeList }"
															itemValue="contractTypeId" itemLabel="typename" />
													</form:select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="box billableItemCategory">
												<!-- /.box-header -->
												<div class="box-body">
													<!-- Table added for shipbourne equipment details -->
													<table class="table no-margin" id="messageTable">
														<thead>
															<tr>
																<th>Billable Item Category Name</th>
																<th>Rate</th>
																<th>Valid From</th>
																<th>Valid Till</th>
															</tr>
														</thead>
														<tbody id="tBody">
															<tr id="trBillableItem">
																<td><form:input path="itemName"
																		class="form-control" id="itemName" required="required" /></td>
																<td><form:input
																		path="paymentBillableItemCategoryLog.rate"
																		class="form-control" id="rate" required="required" /></td>
																<td><form:input
																		path="paymentBillableItemCategoryLog.validFrom"
																		class="form-control datepicker" id="valid_from"
																		required="required" /></td>
																<td><form:input
																		path="paymentBillableItemCategoryLog.validTill"
																		class="form-control datepicker" id="valid_till"
																		required="required" /></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="remark" class="col-sm-4 ">Remark</label>
												<div class="col-sm-8">
													<%-- <form:textarea
														path="paymentBillableItemCategoryLogs[0].remark"
														class="form-control" placeholder="Remark, if any"
														id="remark" /> --%>
													<form:textarea
														path="paymentBillableItemCategoryLog.remark"
														class="form-control" placeholder="Remark, if any"
														id="remark" />
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer clearfix">
							<form:input type="submit" class="btn btn-primary" path=""
								value="Save Billable Category"></form:input>
						</div>
						<!-- /.box-footer -->
					</form:form>

					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (right) -->
		</div>
	</section>
	<!-- content  -->
</div>
<!-- content-wrapper -->

<script>
	function validateForm() {

		return true;
	}

	$(function() {
		$("#valid_from").datepicker({
			format : 'yyyy-mm-dd',
			changeYear : true,
			changeMonth : true,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() + 1);
				$("#valid_till").datepicker("option", "minDate", dt);
			}
		});
		$("#valid_till").datepicker({
			format : 'yyyy-mm-dd',
			changeYear : true,
			changeMonth : true,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() - 1);
				$("#valid_from").datepicker("option", "maxDate", dt);
			}
		});
	});
</script>

<jsp:include page="../../finalFooter.jsp"></jsp:include>