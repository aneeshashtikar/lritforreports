<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="List of Contracts " />
</jsp:include>
<spring:url var="getContract" value="/contract/getContract"></spring:url>
<spring:url var="terminate" value="/contract/terminate/"></spring:url>
<spring:url var="getContractByContractId"
	value="/contract/getContractByContractId/"></spring:url>

<spring:url var="edit" value="/contract/getContractByContractId/"></spring:url>
<spring:url var="view" value="/contract/viewContractByContractId/"></spring:url>

<script type="text/javascript">

	function listOfAllContracts() {
		showTable();
	}
	var dtable = new resultSet();

	function showTable() {
		$('#listOfContracts').css("display", "");
		var contractType = document.getElementById('contract_type_id').value;
		var periodFrom = document.getElementById('valid_from').value;
		var periodTill = document.getElementById('valid_to').value;
		var dtable = $('#listOfContracts')
				.DataTable(
						{
	
							dom : 'lBfrtip',
							"ajax" : {
								"url" : "${getContract}",
								"type" : "POST",
								"data" : {
									'contractType' : contractType,
									'fromDate' : periodFrom,
									'toDate' : periodTill
								},
								"dataSrc" : ""
							},
							"columns" : [
									{
										"data" : "id",
										 'render': function (data, type, row, meta) {
									        return meta.row + meta.settings._iDisplayStart + 1;
									        } 
									},
									{
										"data" : "contractNo"
									},
									{
										"data" : "contractType"
									},
									{
										"data" : "agency2Name"
									},
									{
										"data" : "validFrom"
									},
									{
										"data" : "validTo"
									},
									{	
										"data" : "validity",
										 'render' : function (data, type, row) {
								             return data == '' ? 'Terminated' : 'Active'
									            }
									},
									{

										"searchable":false,
										"ordable":false,
										'render' : function (data, type, row) {
											var terminate = "<a href='${terminate}"+ row.contractId +  "' target=\"_blank\">Terminate Contract</a></br>";
											var edit = "<a href='${edit}" + row.contractId + "'  target=\"_blank\">Edit Contract</a></br>";
											var view = "<a href='${view}" + row.contractId + "'  target=\"_blank\">View Contract</a></br>";

											if(row.validity == null) {
												return edit + " " + terminate + " " +view;
											}else {
												
												return "";
											}
								          
									   }
									}
								 ],
							"responsive" : {
								"details" : {
									"type" : 'column',
									"target" : 'tr'
								}
							},
							"columnDefs" : [ {
								"className" : 'control',
								"targets" : 0
							} ],
							
							"bDestroy" : true
							
						});

	}

/* 	
   function terminateClick(id) {
	var contractId = id;
	//var url = '/lrit/contract/terminate/'+id;
	var url = '${terminate}'+id;
	if(confirm(" Are You Sure want to Terminate ? ")) {
		window.location=url;
	} 
	
  } 
   function editClick(id) {
		var contractId = id;
		//var url = 'http://localhost:8080/lrit/getContractByContractId?id='+id;
			//var url = '/lrit/contract/getContractByContractId/'+id;
			var url = '${getContractByContractId}'+id;
		if(confirm("Are You Sure want to Edit ?")) {
			window.location=url;
		} 
		
	  }  */

</script>
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">View Contracts</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<!-- <div class="row"> -->
					<!-- <div class="col-md-10"> -->

					<div class="box-body">
						<jsp:include page="../include/alertMessages.jsp"></jsp:include>
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="fromDate" class="col-sm-4">Contract Type</label>
								<div class="col-sm-8">
									<select class="form-control" name='contractType'
										id="contract_type_id">
										<c:forEach var="contract" items="${contractType}">
											<option id="${contract.getContractTypeId()}"
												value="${contract.getContractTypeId()}">${contract.getTypename()}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<!-- FOURTH ROW -->
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="name" class="col-sm-4">Contract From</label>
								<div class="col-sm-8">
									<input type="text" class="form-control pull-right datepicker"
										id="valid_from" placeholder="Contract From" autocomplete="off">

								</div>
							</div>
							<div class="form-group col-sm-6">
								<label for="name" class="col-sm-4">Contract To</label>
								<div class="col-sm-8">

									<input type="text" class="form-control pull-right datepicker"
										id="valid_to" placeholder="Contract To" autocomplete="off">


								</div>
							</div>

						</div>

						<!-- EIGHTEENTH ROW -->

						<div class="box-footer">
							<input type="button" class="btn btn-primary" id="listButton"
								onclick="listOfAllContracts(); return false;"
								value="List Contracts" />
						</div>

						<!-- /.tab-pane -->

					</div>
					<div class="box">

						<!-- Report Table -->

						<div class="box-body">

							<table id="listOfContracts"
								class="table table-bordered table-striped "
								style="display: none; width: 100%">
								<thead>
									<tr>
										<!-- <th data-data="null" ></th> -->
										<th data-data="null" data-default-content="">Sr. No.</th>

										<th data-data="contractNumber" data-default-content="NA">Contract
											Number</th>
										<th data-data="typename" data-default-content="NA">Contract
											Type</th>
										<th data-data="agency2" data-default-content="NA">Agency
										</th>
										<th data-data="validFrom" data-default-content="NA">Contract
											From</th>
										<th data-data="validTo" data-default-content="NA">Contract
											Till</th>
										<th data-data="validFrom" data-default-content="NA">Status</th>
										<th data-data="null" data-orderable="false"
											data-default-content="NA">Action</th>

									</tr>
								</thead>
							</table>
						</div>
						<!-- /.box-body -->

					</div>
				</div>
				<!-- /.form -->
			</div>
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../../common.jsp"></jsp:include>
			</div>
			<!--/.col (left) -->
		</div>
		<!-- right column -->
		<!--/.col (right) -->

		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script src="../js/tab_script.js"></script>


<script>
		/* 	
		 $(document).ready(function(){
		 $("#validFrom").datepicker({
		 format : 'yyyy-mm-dd',
		 numberOfMonths: 2,
		 onSelect: function(selected) {
		 $("#validTo").datepicker("option","minDate", selected);
		 }
		 });
		 $("#validTo").datepicker({ 
		 format : 'yyyy-mm-dd',
		 numberOfMonths: 2,
		 onSelect: function(selected) {
		 $("#validFrom").datepicker("option","maxDate", selected);
		 }
		 });  
		 }); */
		$(function() {
			$("#valid_from").datepicker({
				format : 'yyyy-mm-dd',
				autoclose: true,
				onSelect : function(selected) {
					console.log("123");
					$("#valid_to").datepicker("option", "minDate", selected);
				}

			});
			$("#valid_to").datepicker({
				format : 'yyyy-mm-dd',
				autoclose: true,
				onSelect : function(selected) {
					console.log("123");
					$("#valid_from").datepicker("option", "maxDate", selected);
				}
			});
		});

		/* $(function() {
			$("#validFrom").datepicker({
				dateFormat : 'yyyy-mm-dd',
				changeYear : true,
				changeMonth : true,
				onSelect : function(selected) {
					var dt = new Date(selected
					dt.setDate(dt.getDate() + 1);
					$("#validTo").datepicker("option", "minDate", dt);
				}
			});
			$("#validTo").datepicker({
				dateFormat : 'yyyy-mm-dd',
				changeYear : true,
				changeMonth : true,
				onSelect : function(selected) {
					var dt = new Date(selected);
					dt.setDate(dt.getDate() - 1);
					$("#validFrom").datepicker("option", "maxDate", dt);
				}
			});
		}); */
	</script>
<jsp:include page="../../finalFooter.jsp"></jsp:include>