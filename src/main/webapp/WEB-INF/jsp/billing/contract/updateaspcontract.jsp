<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="ASP Contract " />
</jsp:include>


<spring:url var="getContractingGovernments"
	value="/contract/getContractingGovernments"></spring:url>
<spring:url var="getAgencyDetails" value="/contract/getAgencyDetails"></spring:url>
<!-- <script type='javascript' src='/lritbilling//resources/js/validation.js'></script> -->
<spring:url var="validationjs" value="/resources/js/validation.js"></spring:url>
<script src="${validationjs}"></script>
<spring:url var="saveASPDC" value="/aspcontract/updateaspcontract"></spring:url>
<spring:url var="viewContractDocument" value="/contract/upload"></spring:url>
<spring:url var="contractjs" value="/resources/js/contract.js"></spring:url>
<script src="${contractjs}"></script>
<script>
	function validateUpdateASPContract() {
		//	alert("IN Update ASP");
		console.log("Inside validateContract ");
		var contractValidationFlag = contractDetailsValidation();
		console.log("1");
		var agency1ValidationFlag = agency1Validation();
		console.log("2");
		var agency2ValidationFlag = agency2Validation();
		console.log("3");
		var checkBoxFlagAgency1 = checkBoxValidationAgency1();
		var checkBoxFlagAgency2 = checkBoxValidationAgency2();

		//var annexureValidationFlag = validateAnnexureInformation();

		if (contractValidationFlag == true && agency1ValidationFlag == true
				&& agency2ValidationFlag == true && checkBoxFlagAgency1 == true
				&& checkBoxFlagAgency2 == true
				&& annexureValidationFlag == true) {
			return true;
		} else {
			alert("There are Some errors in your information.");
			return false;
		}
	}

	function checkBoxValidationAgency1() {
		var checkboxs = document.getElementsByName("contractingGov1");
		var okay = false;
		for (var i = 0, l = checkboxs.length; i < l; i++) {
			if (checkboxs[i].checked) {
				okay = true;
				break;
			}
		}
		if (okay) {
			return true;
		} else {
			document.getElementById("agency1CGError").innerHTML = "Please Select at least One Contracting Government for Agency 1";
			//alert("Please Select at least One Contracting Government for Agency 1");
			return false;

		}
	}

	function checkBoxValidationAgency2() {
		var checkboxs = document.getElementsByName("contractingGov2");
		var okay = false;
		for (var i = 0, l = checkboxs.length; i < l; i++) {
			if (checkboxs[i].checked) {
				okay = true;
				break;
			}
		}
		if (okay) {
			return true;
		} else {
			document.getElementById("agency2CGError").innerHTML = "Please Select at least One Contracting Government for Agency 2";
			//alert("Please Select at least One Contracting Government for Agency 2 ");
			return false;

		}
	}

	function agency1Validation() {
		console.log("agency1Validation");

		var agency1EmailFlag = agency1EmailValidation();
		var agency1AddressFlag = agency1AddressValidation();
		var agency1PhoneFlag = agency1PhoneValidation();
		var agency1FaxFlag = agency1FaxValidation();
		var agency1FinancialEmailFlag = agency1FinancialEmailValidation();
		var agency1FinancialPhoneFlag = agency1FinancialPhoneValidation();
		var agency1FinancialAddressFlag = agency1FinancialAddressValidation();

		var agency1NameOfCompanyFlag = agency1NameOfCompanyValidation();
		var agency1TransactionInformationFlag = agency1TransactionInformationValidation();

		if (agency1EmailFlag == true && agency1AddressFlag == true
				&& agency1PhoneFlag == true && agency1FaxFlag == true
				&& agency1FinancialEmailFlag == true
				&& agency1FinancialPhoneFlag == true
				&& agency1FinancialAddressFlag == true
				&& agency1NameOfCompanyFlag == true
				&& agency1TransactionInformationFlag == true) {
			console.log("value is true");
			return true;
		}
		return false;
	}

	function agency1EmailValidation() {
		var agency1generalEmail = document
				.getElementById("agency1.generalEmail").value;
		console.log("agency1generalEmail No. " + agency1generalEmail);
		var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		var error;
		console.log('testing condition   :' + regex.test(agency1generalEmail));

		if (isEmpty(agency1generalEmail)) {
			error = "Agency General Email is Empty";
		} else if (!regex.test(agency1generalEmail)) {
			error = "Agency1 General Email not valid";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency1generalEmailError").innerHTML = error;
			//alert(error);
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency1generalEmailError").innerHTML = "";
			return true;
		}
	}

	function agency1FinancialEmailValidation() {
		var agency1financialEmail = document
				.getElementById("agency1.financialEmail").value;
		console.log("agency1financialEmail No. " + agency1financialEmail);
		var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		var error;
		console
				.log('testing condition   :'
						+ regex.test(agency1financialEmail));

		if (isEmpty(agency1financialEmail)) {
			error = "Agency General Email is Empty";
		} else if (!regex.test(agency1financialEmail)) {
			error = "Agency1 General Email not valid";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency1financialEmailError").innerHTML = error;
			//	alert(error);
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency1financialEmailError").innerHTML = "";
			return true;
		}
	}

	function agency1PhoneValidation() {
		var agency1generalPhone = document
				.getElementById("agency1.generalPhone").value;
		console.log("agency1generalPhone No. " + agency1generalPhone);
		var regex = /^[+]*[-\\s\\./0-9]*$/;
		var error;
		console.log('testing condition   :' + regex.test(agency1generalPhone));

		if (isEmpty(agency1generalPhone)) {
			error = "Agency 1 Phone Number is Empty";
		} else if (!regex.test(agency1generalPhone)) {
			error = "Agency 1 Phone number should contain only numbers";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency1generalPhoneError").innerHTML = error;
			//alert(error);

			return false;
		} else {
			document.getElementById("agency1generalPhoneError").innerHTML = "";
			return true;
		}
	}

	function agency1FaxValidation() {
		var agency1generalFax = document.getElementById("agency1.generalFax").value;
		console.log("agency1generalFax No. " + agency1generalFax);
		var regex = /^[+]*[-\\s\\./0-9]*$/;
		var error;
		console.log('testing condition   :' + regex.test(agency1generalFax));

		if (isEmpty(agency1generalFax)) {
			error = "Agency 1 Phone Number is Empty";
		} else if (!regex.test(agency1generalFax)) {
			error = "Agency 1 Phone number should contain only numbers";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency1generalFaxError").innerHTML = error;
			//	alert("Annual Management Should not Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency1generalFaxError").innerHTML = "";
			return true;
		}
	}

	function agency1FinancialPhoneValidation() {
		var agency1financialPhone = document
				.getElementById("agency1.financialPhone").value;
		console.log("agency1financialPhone No. " + agency1financialPhone);
		var regex = /^[+]*[-\\s\\./0-9]*$/;
		var error;
		console
				.log('testing condition   :'
						+ regex.test(agency1financialPhone));

		if (isEmpty(agency1financialPhone)) {
			error = "Agency 1 Phone Number is Empty";
		} else if (!regex.test(agency1financialPhone)) {
			error = "Agency 1 Phone number should contain only numbers";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency1financialPhoneError").innerHTML = error;
			//	alert(error);
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency1financialPhoneError").innerHTML = "";
			return true;
		}
	}

	function agency1AddressValidation() {
		var agency1generalAddress = document
				.getElementById("agency1.correspondenceAddress").value;
		if (isEmpty(agency1generalAddress)) {
			document.getElementById("agency1generalAddressError").innerHTML = "Agency 1 address is Empty";
			//	alert("Agency 1 General Address is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency1generalAddressError").innerHTML = "";
			return true;
		}
	}

	function agency1FinancialAddressValidation() {
		var agency1financialAddress = document
				.getElementById("agency1.financialContact").value;
		if (isEmpty(agency1financialAddress)) {
			document.getElementById("agency1financialAddressError").innerHTML = "Agency 1 address is Empty";
			//alert("Agency 1 General Address is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency1financialAddressError").innerHTML = "";
			return true;
		}
	}

	function agency1NameOfCompanyValidation() {
		var agency1nameOfCompany = document
				.getElementById("agency1.nameOfCompany").value;
		if (isEmpty(agency1nameOfCompany)) {
			document.getElementById("agency1NameOfCompanyError").innerHTML = "Agency 1 Name of Company is Empty";
			//alert("Agency 1 Name of Company is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency1NameOfCompanyError").innerHTML = "";
			return true;
		}
	}

	function agency1TransactionInformationValidation() {
		var agency1transactionInstruction = document
				.getElementById("agency1.transactionInstruction").value;
		if (isEmpty(agency1transactionInstruction)) {
			document.getElementById("agency1transactionInstructionError").innerHTML = "Agency 1 transaction information is Empty";
			//alert("Agency 1 Transaction Information is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency1financialAddressError").innerHTML = "";
			return true;
		}
	}

	/*

	 Agency 2 

	 */

	function agency2Validation() {
		console.log("agency2Validation");

		var agency2EmailFlag = agency2EmailValidation();
		var agency2AddressFlag = agency2AddressValidation();
		var agency2PhoneFlag = agency2PhoneValidation();
		var agency2FaxFlag = agency2FaxValidation();
		var agency2FinancialEmailFlag = agency2FinancialEmailValidation();
		var agency2FinancialPhoneFlag = agency2FinancialPhoneValidation();
		var agency2FinancialAddressFlag = agency2FinancialAddressValidation();

		var agency2NameOfCompanyFlag = agency2NameOfCompanyValidation();
		var agency2TransactionInformationFlag = agency2TransactionInformationValidation();

		if (agency2EmailFlag == true && agency2AddressFlag == true
				&& agency2PhoneFlag == true && agency2FaxFlag == true
				&& agency2FinancialEmailFlag == true
				&& agency2FinancialPhoneFlag == true
				&& agency2FinancialAddressFlag == true
				&& agency2NameOfCompanyFlag == true
				&& agency2TransactionInformationFlag == true) {
			console.log("value is true");
			return true;
		}
		return false;
	}

	function agency2EmailValidation() {
		var agency2generalEmail = document
				.getElementById("agency2.generalEmail").value;
		console.log("Agency 2 Email " + agency2generalEmail);
		var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		var error;
		console.log('testing condition   :' + regex.test(agency2generalEmail));

		if (isEmpty(agency2generalEmail)) {
			error = "Agency 2 General Email is Empty";
		} else if (!regex.test(agency2generalEmail)) {
			error = "agency2 General Email not valid";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency2generalEmailError").innerHTML = error;
			//alert(error);
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency2generalEmailError").innerHTML = "";
			return true;
		}
	}

	function agency2FinancialEmailValidation() {
		var agency2financialEmail = document
				.getElementById("agency2.financialEmail").value;
		console.log("Contract No. " + agency2financialEmail);
		var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		var error;
		console
				.log('testing condition   :'
						+ regex.test(agency2financialEmail));

		if (isEmpty(agency2financialEmail)) {
			error = "Agency Financial Email is Empty";
		} else if (!regex.test(agency2financialEmail)) {
			error = "agency 2 Financial Email not valid";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency2financialEmailError").innerHTML = error;
			//alert(error);
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency2financialEmailError").innerHTML = "";
			return true;
		}
	}

	function agency2PhoneValidation() {
		var agency2generalPhone = document
				.getElementById("agency2.generalPhone").value;
		console.log("agency2generalPhone No. " + agency2generalPhone);
		var regex = /^[+]*[-\\s\\./0-9]*$$/;
		var error;
		console.log('testing condition   :' + regex.test(agency2generalPhone));

		if (isEmpty(agency2generalPhone)) {
			error = "Agency 2 Phone Number is Empty";
		} else if (!regex.test(agency2generalPhone)) {
			error = "Agency 2 Phone number not valid";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency2generalPhoneError").innerHTML = error;
			//alert(error);
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency2generalPhoneError").innerHTML = "";
			return true;
		}
	}

	function agency2FaxValidation() {
		var agency2generalFax = document.getElementById("agency2.generalFax").value;
		console.log("agency2generalFax " + agency2generalFax);
		var regex = /^[+]*[-\\s\\./0-9]*$/;
		var error;
		console.log('testing condition   :' + regex.test(agency2generalFax));

		if (isEmpty(agency2generalFax)) {
			error = "Agency 2 Fax Number is Empty";
		} else if (!regex.test(agency2generalFax)) {
			error = "Agency 2 Fax number should contain only numbers";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency2generalFaxError").innerHTML = error;
			//alert("Agency 2 Fax Should not Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency2generalFaxError").innerHTML = "";
			return true;
		}
	}

	function agency2FinancialPhoneValidation() {
		var agency2financialPhone = document
				.getElementById("agency2.financialPhone").value;
		console.log("agency2financialPhone No. " + agency2financialPhone);
		var regex = /^[+]*[-\\s\\./0-9]*$/;
		var error;
		console.log('testing1 condition   :'
				+ regex.test(agency2financialPhone));

		if (isEmpty(agency2financialPhone)) {
			error = "Agency 2 Phone Number is Empty";
		} else if (!regex.test(agency2financialPhone)) {
			error = "Agency 2 Phone number Not valid";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("agency2financialPhoneError").innerHTML = error;
			//alert(error);

			return false;
		} else {
			document.getElementById("agency2financialPhoneError").innerHTML = "";
			return true;
		}
	}

	function agency2AddressValidation() {
		var agency2generalAddress = document
				.getElementById("agency2.correspondenceAddress").value;
		if (isEmpty(agency2generalAddress)) {
			document.getElementById("agency2generalAddressError").innerHTML = "Agency 2 general Address is Empty";
			//alert("Agency 2 general Address is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency2generalAddressError").innerHTML = "";
			return true;
		}
	}

	function agency2FinancialAddressValidation() {
		var agency2financialAddress = document
				.getElementById("agency2.financialContact").value;
		if (isEmpty(agency2financialAddress)) {
			document.getElementById("agency2financialContactError").innerHTML = "Agency 2 Contact is Empty";
			//alert("Agency 2 Contact is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency2financialContactError").innerHTML = "";
			return true;
		}
	}

	function agency2NameOfCompanyValidation() {
		var agency2nameOfCompany = document
				.getElementById("agency2.nameOfCompany").value;
		if (isEmpty(agency2nameOfCompany)) {
			document.getElementById("agency2NameOfCompanyError").innerHTML = "Agency 2 Name of Company is Empty";
			//alert("Agency 2 Name of Company is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency2NameOfCompanyError").innerHTML = "";
			return true;
		}
	}

	function agency2TransactionInformationValidation() {
		var agency2transactionInstruction = document
				.getElementById("agency2.transactionInstruction").value;
		if (isEmpty(agency2transactionInstruction)) {
			document.getElementById("agency2TransactionInstructionError").innerHTML = "Agency 2 transaction information is Empty";
			//alert("Transaction instruction is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("agency2TransactionInstructionError").innerHTML = "";
			return true;
		}
	}

	function contractDetailsValidation() {
		console.log("contractDetailsValidation");

		var contractNumberFlag = contractNumberValidation();
		var validFromFlag = validFromValidation();
		var validTillFlag = validTillValidation();
		var thresholdAmountFlag = thresholdAmountValidation();
		var thresholdPeriodFlag = thresholdPeriodValidation();
		var invoiceDuePeriodFlag = invoiceDuePeriodValidation();
		var roiFlag = roiValidation();
		var governingLawFlag = governingLawValidation();
		var dateFlag = dateFromToValidation();
		var currrencyFlag = currencyTypeValidation();
		/* var fileFlag = fileValidation();
		var annexureDateFlag = annexureDateValidation(); */

		if (contractNumberFlag == true && validFromFlag == true
				&& thresholdAmountFlag == true && thresholdPeriodFlag == true
				&& invoiceDuePeriodFlag == true && roiFlag == true
				&& governingLawFlag == true && dateFlag == true && currrencyFlag == true /* && fileFlag == true && annexureDateFlag == true */) {
			console.log("value is true");
			return true;
		}
		return false;
	}
	function currencyTypeValidation() {
		var currencyType = document.getElementById("billingCurrencyType.currencyType").value;
		console.log("currencyType"+currencyType);
		if (isEmpty(currencyType)) {
			document.getElementById("currencyTypeError").innerHTML = "Please Select Currency Type";
			return false;
		} else {
			document.getElementById("currencyTypeError").innerHTML = "";
			return true;
		}
	}

	function isEmpty(str) {
		return !str.toString().replace(/\s+/, '').length;
	}

	function contractNumberValidation() {
		var contractNumber = document.getElementById("contractNumber").value;
		console.log("Contract No. " + contractNumber);

		var error;

		if (isEmpty(contractNumber)) {
			error = "Contract Number is Empty";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("contractNumberError").innerHTML = error;
			//alert(error);
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("contractNumberError").innerHTML = "";
			return true;
		}
	}
	function validFromValidation() {
		var validFrom = document.getElementById("validFrom").value;
		if (isEmpty(validFrom)) {

			document.getElementById("validToError").innerHTML = "Valid From Date is Empty";
			//alert("Valid From Date is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("validToError").innerHTML = "";
			return true;
		}
	}

	function validTillValidation() {
		var validFrom = document.getElementById("validTo").value;
		if (isEmpty(validFrom)) {

			document.getElementById("validToError").innerHTML = "Valid To Date is Empty";
			//alert("Valid To Date is Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("validToError").innerHTML = "";
			return true;
		}
	}

	function thresholdAmountValidation() {
		var thresholdAmount = document.getElementById("thresholdAmount").value;
		console.log("awardAmount No. " + thresholdAmount);
		var regex = /^[-+]?[0-9]+\.*[0-9]*$/;
		var error;
		console.log('testing condition   :' + regex.test(thresholdAmount));

		if (isEmpty(thresholdAmount)) {
			error = "Threshold Amount is Empty";
		} else if (!regex.test(thresholdAmount)) {
			error = "Threshold amount should contain only numbers";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("thresholdAmountError").innerHTML = error;
			//alert("Threshold Amount Should not Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("thresholdAmountError").innerHTML = "";
			return true;
		}
	}

	function thresholdPeriodValidation() {
		var thresholdPeriod = document.getElementById("thresholdPeriod").value;
		console.log("thresholdPeriod No. " + thresholdPeriod);
		var regex = /^\d{0,5}$/;
		var error;
		console.log('testing condition   :' + regex.test(thresholdPeriod));

		if (isEmpty(thresholdPeriod)) {
			error = "Threshold Period is Empty";
		} else if (!regex.test(thresholdPeriod)) {
			error = "Threshold period should contain only numbers";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("thresholdPeriodError").innerHTML = error;
			//alert("Threshold Period Should not Empty");
			return false;
		} else {
			document.getElementById("thresholdPeriodError").innerHTML = "";
			return true;
		}
	}

	function invoiceDuePeriodValidation() {
		var invoiceDuePeriod = document.getElementById("invoiceDuePeriod").value;
		console.log("thresholdPeriod No. " + invoiceDuePeriod);
		var regex = /^\d{0,5}$/;
		var error;
		console.log('testing condition   :' + regex.test(invoiceDuePeriod));

		if (isEmpty(invoiceDuePeriod)) {
			error = "Payment Due Period is Empty";
		} else if (!regex.test(invoiceDuePeriod)) {
			error = "Payment due period should contain only numbers";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("invoiceDuePeriodError").innerHTML = error;
			//alert("Invoice due period Should not Empty");
			return false;
		} else {
			document.getElementById("invoiceDuePeriodError").innerHTML = "";
			return true;
		}
	}

	function roiValidation() {
		var roiLatePayment = document.getElementById("roiLatePayment").value;
		console.log("roiLatePayment No. " + roiLatePayment);
		var regex = /^[-+]?[0-9]+\.*[0-9]*$/;
		var error;
		console.log('testing condition   :' + regex.test(roiLatePayment));

		if (isEmpty(roiLatePayment)) {
			error = "Rate of interest is Empty";
		} else if (!regex.test(roiLatePayment)) {
			error = "Rate of interest should contain only numbers";
		}
		console.log("error " + error);
		if (error != null) {
			document.getElementById("roiLatePaymentError").innerHTML = error;
			//alert("Rate of interest Should not Empty");
			$('[href="#tabs_1"]').click();
			return false;
		} else {
			document.getElementById("roiLatePaymentError").innerHTML = "";
			return true;
		}
	}

	function governingLawValidation() {
		var governingLaw = document.getElementById("governingLaw").value;
		if (isEmpty(governingLaw)) {
			document.getElementById("governingLawError").innerHTML = "Governing law is Empty";
			//alert("Govrning law is Empty");

			return false;
		} else {
			document.getElementById("governingLawError").innerHTML = "";
			return true;
		}
	}

	
	function dateFromToValidation() {

		var fromDate = document.getElementById("validFrom").value;
		var toDate = document.getElementById("validTo").value;

		if ((Date.parse(fromDate) <= Date.parse(toDate))) {
			document.getElementById("dateError").innerHTML = "";
			return true;
		} else {

			document.getElementById("dateError").innerHTML = "Valid to Date should be less than Valid From Date.";
			return false;
		}

	}

	$(document).ready(
			function() {
				$(".preferenceSelect").change(
						function() {
							var selected = $("option:selected", $(this)).val();
							var thisID = $(this).attr("id");
							$(".preferenceSelect option").each(function() {
								$(this).show();
							});
							$(".preferenceSelect").each(
									function() {
										if ($(this).attr("id") != thisID) {
											$(
													"option[value='" + selected
															+ "']", $(this))
													.attr("disabled", true);
										}
									});

						});
			});

	$(document).ready(function() {
		$("#cg1").hide();
		$("#cg2").hide();
	});

	//Get Agency Details 
	function getAgencyDetails(agencyCode) {
		$
				.ajax({
					type : "POST",
					data : "agencyCode=" + agencyCode,
					url : "${getAgencyDetails}",
					success : function(response) {
						//alert(response != null);
						//alert(response["generalPhone"]);
						if (response["generalPhone"] == undefined)
							document.getElementById("agency1.generalPhone").value = "";
						else
							document.getElementById("agency1.generalPhone").value = response["generalPhone"];

						if (response["generalEmail"] == undefined)
							document.getElementById("agency1.generalEmail").value = "";
						else
							document.getElementById("agency1.generalEmail").value = response["generalEmail"];

						if (response["correspondenceAddress"] == undefined)
							document
									.getElementById("agency1.correspondenceAddress").value = "";
						else
							document
									.getElementById("agency1.correspondenceAddress").value = response["correspondenceAddress"];

						if (response["generalFax"] == undefined)
							document.getElementById("agency1.generalFax").value = "";
						else
							document.getElementById("agency1.generalFax").value = response["generalFax"];

						if (response["financialEmail"] == undefined)
							document.getElementById("agency1.financialEmail").value = "";
						else
							document.getElementById("agency1.financialEmail").value = response["financialEmail"];

						if (response["financialPhone"] == undefined)
							document.getElementById("agency1.financialPhone").value = "";
						else
							document.getElementById("agency1.financialPhone").value = response["financialPhone"];

						if (response["financialContact"] == undefined)
							document.getElementById("agency1.financialContact").value = "";
						else
							document.getElementById("agency1.financialContact").value = response["financialContact"];

						if (response["nameOfCompany"] == undefined)
							document.getElementById("agency1.nameOfCompany").value = "";
						else
							document.getElementById("agency1.nameOfCompany").value = response["nameOfCompany"];

						if (response["transactionInstruction"] == undefined)
							document
									.getElementById("agency1.transactionInstruction").value = "";
						else
							document
									.getElementById("agency1.transactionInstruction").value = response["transactionInstruction"];
					}

				});
	}

	function getAgencyDetailsAgency2(agencyCode) {

		$
				.ajax({
					type : "POST",
					data : "agencyCode=" + agencyCode,
					url : "${getAgencyDetails}",
					success : function(response) {
						if (response["generalPhone"] == undefined)
							document.getElementById("agency2.generalPhone").value = "";
						else
							document.getElementById("agency2.generalPhone").value = response["generalPhone"];

						if (response["generalEmail"] == undefined)
							document.getElementById("agency2.generalEmail").value = "";
						else
							document.getElementById("agency2.generalEmail").value = response["generalEmail"];

						if (response["correspondenceAddress"] == undefined)
							document
									.getElementById("agency2.correspondenceAddress").value = "";
						else
							document
									.getElementById("agency2.correspondenceAddress").value = response["correspondenceAddress"];

						if (response["generalFax"] == undefined)
							document.getElementById("agency2.generalFax").value = "";
						else
							document.getElementById("agency2.generalFax").value = response["generalFax"];

						if (response["financialEmail"] == undefined)
							document.getElementById("agency2.financialEmail").value = "";
						else
							document.getElementById("agency2.financialEmail").value = response["financialEmail"];

						if (response["financialPhone"] == undefined)
							document.getElementById("agency2.financialPhone").value = "";
						else
							document.getElementById("agency2.financialPhone").value = response["financialPhone"];

						if (response["financialContact"] == undefined)
							document.getElementById("agency2.financialContact").value = "";
						else
							document.getElementById("agency2.financialContact").value = response["financialContact"];

						if (response["nameOfCompany"] == undefined)
							document.getElementById("agency2.nameOfCompany").value = "";
						else
							document.getElementById("agency2.nameOfCompany").value = response["nameOfCompany"];

						if (response["transactionInstruction"] == undefined)
							document
									.getElementById("agency2.transactionInstruction").value = "";
						else
							document
									.getElementById("agency2.transactionInstruction").value = response["transactionInstruction"];

					}
				});
	}

	function getContractingGovernments1() {
		$("#cg1").show();
		var agencyCode = document.getElementById("agency1.agencyCode").value;
		getAgencyDetails(agencyCode);
		if (agencyCode == 0) {
			$("#errorselectMainCategory")
					.append(
							'<div id="errorMsg"><p class="text-red" style="padding-top:5px;">Please select Agency </p><div>');
			$("#agency1.agencyCode").empty();
		} else {

			$.ajax({
				type : "POST",
				data : "agencyCode=" + agencyCode,
				url : "${getContractingGovernments}",
				success : function(response) {
					$("#contractingGovernments1").empty();
					for (var i = 0; i < response.length; i++) {
						var obj = response[i];
						$("#contractingGovernments1").append(
								'<input type=checkbox name="contractingGov1" value="'+obj.cgLritid+'"/>'
										+ obj.cgLritid + ' - ' + obj.cgName
										+ '<br>');
					}
					$("#errorselectMainCategory").empty();
				},
				error : function(e) {
					alert('Getting Error1: ' + e);
				}
			});
		}
	}

	function getContractingGovernments2() {
		$("#cg2").show();
		var agencyCode = document.getElementById("agency2.agencyCode").value;
		//alert(agencyCode);
		getAgencyDetailsAgency2(agencyCode);
		if (agencyCode == 0) {
			$("#errorselectMainCategory")
					.append(
							'<div id="errorMsg"><p class="text-red" style="padding-top:5px;">Please select Agency </p><div>');
			$("#agency1.agencyCode").empty();
		} else {
			$.ajax({
				type : "POST",
				data : "agencyCode=" + agencyCode,
				url : "${getContractingGovernments}",
				success : function(response) {
					$("#contractingGovernments2").empty();
					for (var i = 0; i < response.length; i++) {
						var obj = response[i];
						$("#contractingGovernments2").append(
								'<input type=checkbox name="contractingGov2" value="'+obj.cgLritid+'"/>'
										+ obj.cgLritid + ' - ' + obj.cgName
										+ '<br>');
					}
					$("#errorselectMainCategory").empty();
				},
				error : function(e) {
					alert('Getting Error1: ' + e);
				}
			});
		}
	}
</script>

<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<section class="content">
		<div class="row">
			<!-- left column -->
			 <div class="col-md-10">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Edit ASP/DC Contract Details</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<!-- <div class="row"> -->
					<!-- <div class="col-md-10"> -->
					<form:form class="form-horizontal" id="contractDetails"
						action="${saveASPDC}"
						onsubmit="return validateUpdateASPContract();"
						modelAttribute="contract" enctype="multipart/form-data">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active" id="contractdetfirst"
										onclick="tabChange(id, _prev, _next)"><a href="#tab_1"
										data-toggle="tab">Contract Details</a></li>

									<li id="agency1detsecond" onclick="tabChange(id, _prev, _next)"><a
										href="#tab_2" data-toggle="tab">Agency 1 Details</a></li>

									<li id="agency2detlast" onclick="tabChange(id, _prev, _next)">
										<a href="#tab_3" data-toggle="tab">Agency 2 Details</a>
									</li>

									<li class="pull-right"><a href="#" class="text-muted"><i
											class="fa fa-gear"></i></a></li>
								</ul>
								<div class="tab-content">
									<!-- First Tab Dummy -->
									<div class="tab-pane active" id="tab_1">
										<br>
										<%-- 	<c:choose>
											<c:when test="${alt == '1'}">
												<div class="alert alert-danger" role="alert">${msg1}</div>
												<div class="alert alert-danger" role="alert">${msg2}</div>

											</c:when>
											<c:when test="${alt == '2'}">
												<div class="alert alert-success" role="alert">${msg1}</div>

											</c:when>
										</c:choose> --%>
										<jsp:include page="../include/alertMessages.jsp"></jsp:include>
										<form:hidden class="form-control"
											id="billingContractType.contractTypeId"
											path="billingContractType.contractTypeId" required="required"
											value="1" />
										<form:hidden class="form-control" id="contractId"
											path="contractId" value="${contract.contractId}"
											name="contractId" />

										<!-- SECOND ROW -->

										<div class="row">
											<div class="form-group col-sm-12">
												<label for="contractNumber" class="col-sm-3">Contract
													Number</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="contractNumber" path="contractNumber" readonly="true"></form:input>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="contractNumber"
												cssClass="error" />
										</div>
										<div class="error" id="contractNumberError"></div>

										<!-- THIRD ROW -->

										<!-- FOURTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="validFrom" class="col-sm-3"> Valid From</label>
												<div class="col-sm-3">
													<div class="input-group date">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>

														<form:input type="text"
															class="form-control pull-right datepicker" id="validFrom"
															path="validFrom" autocomplete="off"></form:input>

													</div>
												</div>
												<label for="validTo" class="col-sm-3"> Valid To</label>
												<div class="col-sm-3">
													<div class="input-group date">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
														<form:input type="text"
															class="form-control pull-right datepicker" id="validTo"
															path="validTo" autocomplete="off"></form:input>
													</div>
												</div>

											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="validFrom"
												cssClass="error" />
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="validTo" cssClass="error" />
										</div>
										<div class="error" id="validToError"></div>
										<div class="error" id="dateError"></div>
										<!-- FIFTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="thresholdAmount" class="col-sm-3">
													Threshold Amount</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="thresholdAmount" path="thresholdAmount"></form:input>
												</div>
												<label for="thresholdPeriod" class="col-sm-3">Threshold
													Period (In Days)</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="thresholdPeriod" path="thresholdPeriod"></form:input>
												</div>

											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="thresholdAmount"
												cssClass="error" />
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="thresholdPeriod"
												cssClass="error" />
										</div>
										<div class="col-sm-12">
											<div class="error" class="col-sm-3" id="thresholdAmountError"></div>
											<div class="error" class="col-sm-3" id="thresholdPeriodError"></div>
										</div>

										<!-- THIRTEENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="invoice_due_period" class="col-sm-3">
													Payment Due Period (In Working Days) </label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="invoiceDuePeriod" path="invoiceDuePeriod"></form:input>
												</div>
												<label for="roi_late_payment" class="col-sm-3"> Rate
													of Interest(if late payment) </label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="roiLatePayment" path="roiLatePayment"></form:input>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="invoiceDuePeriod"
												cssClass="error" />
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="roiLatePayment"
												cssClass="error" />
										</div>
										<div class="error" id="invoiceDuePeriodError"></div>
										<div class="error" id="roiLatePaymentError"></div>
										<!-- FOURTEENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="renewal_mode" class="col-sm-3"> Renewal
													Mode </label>
												<div class="col-sm-3">
													<div class="radio">
														<label> <form:radiobutton name="renewal_mode"
																id="renewal_mode" value="auto" path="renewalMode" />
															Auto
														</label> <label> <form:radiobutton name="renewal_mode"
																id="renewal_mode" checked="checked" value="manual"
																path="renewalMode" />Manual

														</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-12">
													<label for="fromDate" class="col-sm-3">Select
													Currency</label>
												<div class="col-sm-3">
													<form:select class="form-control preferenceSelect"
														path="billingCurrencyType.currencyType"
														name='billingCurrencyType.currencyType'
														id="currencyType">
														
														<c:forEach var="currency" items="${currencyTypes}"
															varStatus="theCount">
															<form:option value="${currency.currencyType}"
																label="${currency.currencyType} - ${currency.conversionRate}" />

														</c:forEach>
													</form:select>
												</div>
											</div>
										</div>
											<div class="error" id="currencyTypeError"></div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3"
												path="billingCurrencyType.currencyType" cssClass="error" />
										</div>
										<!-- FIFTEENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="governing_law" class="col-sm-3">
													Governing Law </label>
												<div class="col-sm-3">
													<form:textarea rows="" cols="" class="form-control"
														path="governingLaw" id="governingLaw"></form:textarea>
												</div>
											</div>
										</div>
										<div class="error" id="governingLawError"></div>
										<!-- SEVENTEENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="contract_document" class="col-sm-3">Contract
													Annexure</label> <input type="file" id="contract_annexure"
													name="contract_annexure" class="col-sm-3"></input>
												<p class="help-block">Upload Signed Contract Here.</p>
											</div>

										</div>
										<div class="error" id="contractDocumentError"></div>
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="contractAnnexureDate" class="col-sm-3">
													Contract Annexure Date</label>
												<div class="col-sm-3">
													<div class="input-group date">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
														<input type="text"
															class="form-control pull-right datepicker"
															id="contractAnnexureDate" name="contractAnnexureDate"></input>

													</div>
												</div>
											</div>
										</div>
										<div class="error" id="contractAnnexureDateError"></div>
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="remarks" class="col-sm-3"> Remarks </label>
												<div class="col-sm-3">
													<textarea rows="" cols="" class="form-control" id="remarks"
														name="remarks"></textarea>
												</div>
											</div>
										</div>
										<c:if test="${contract.contractId > 0}">
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="contract_document" class="col-sm-3">Uploaded
														Contract Document</label> <a
														href="${viewContractDocument}/${contract.contractId}">Signed
														Contract</a>
												</div>
											</div>
										</c:if>
									</div>
									<div class="tab-pane" id="tab_2">
										<!-- SIXTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="status" class="col-sm-6">Agency 1
													Details:</label>

											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="fromDate" class="col-sm-3">Select Agency
													1</label>
												<div class="col-sm-3">
													<form:select class="form-control preferenceSelect"
														path="agency1.agencyCode" name='agency1.agencyCode'
														id="agency1.agencyCode">
														<form:option
															label="${contract.agency1.agencyCode} ${contract.agency1.name}"
															selected="selected"
															value="${contract.agency1.agencyCode}" />
													</form:select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="name" class="col-sm-3"> Name of Agency</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency1.name" path="agency1.name"
														placeholder="Name of Agency"></form:input>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4" id="errorselectMainCategory"></div>
										</div>
										<!-- Dynamic check Box start  -->
										<div class="row" id="cg1">
											<div class="form-group col-sm-12">

												<label for="fromDate" class="col-sm-3">Select
													Contracting Governments</label>
												<div class="col-sm-3" id="contractingGovernments1">
													<select name="contractingGovernments1" class="form-control"
														id="contractingGovernments1">
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="fromDate" class="col-sm-3"></label>
												<div class="col-sm-6">
													<div class="box box-info contractingGovernment">
														<div class="box-header with-border">
															<h3 class="box-title" align="center">Edit
																Contracting Governments Information</h3>
														</div>
														<div class="box-body">
															<table >
																<tr>
																	<th style="text-align: center">Contracting
																		Governments</th>
																	<th style="text-align: center">Valid From</th>
																	<th style="text-align: center">Valid To</th>
																</tr>
																<c:forEach var="agency1ContractStakeholders"
																	items="${agency1ContractStakeholders}">
																	<tr>
																		<td><input type="checkbox"
																			value="${agency1ContractStakeholders.memberId}"
																			name="contractingGov1" checked="checked" />${agency1ContractStakeholders.memberId}
																			${agency1ContractStakeholders.name}</td>
																		<td><input type="text"
																			class="form-control pull-right datepick"
																			name="agency1ContractGovValidFrom"
																			value="${agency1ContractStakeholders.validFrom}"
																			placeholder="Valid From" autocomplete="off"></input></td>
																		<td><input type="text"
																			class="form-control pull-right datepick"
																			name="agency1ContractGovValidTo"
																			value="${agency1ContractStakeholders.validTo}"
																			placeholder="Valid To" autocomplete="off"></input></td>
																	</tr>
																</c:forEach>
																<c:forEach var="agency1CG" items="${agency1CG}">
																	<tr>
																		<td><input type="checkbox"
																			value="${agency1CG.memberId}" name="contractingGov1" />${agency1CG.memberId}
																			${agency1CG.name}</td>
																		<td><input type="text"
																			class="form-control pull-right datepick"
																			name="agency1ContractGovValidFrom"
																			value="${agency1CG.validFrom}"
																			placeholder="Valid From" autocomplete="off"></input></td>
																		<td><input type="text"
																			class="form-control pull-right datepick"
																			name="agency1ContractGovValidTo"
																			value="${agency1CG.validTo}" placeholder="Valid To"
																			autocomplete="off"></input></td>
																	</tr>
																</c:forEach>

															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency1" cssClass="error" />
										</div>
										<div class="error" id="agency1CGError"></div>
										<!-- SEVENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="status" class="col-sm-3">1)General
													Details:</label>

											</div>
										</div>
										<!-- EIGHTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="generalEmail" class="col-sm-3"> Email </label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency1.generalEmail" path="agency1.generalEmail"></form:input>
												</div>
												<label for="generalPhone" class="col-sm-3">Phone</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency1.generalPhone" path="agency1.generalPhone"></form:input>
												</div>
											</div>
										</div>

										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency1.generalEmail"
												cssClass="error" />
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency1.generalPhone"
												cssClass="error" />
										</div>

										<div class="error" id="agency1generalEmailError"></div>
										<div class="error" id="agency1generalPhoneError"></div>
										<!-- NINTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="correspondenceAddress" class="col-sm-3">
													Address </label>
												<div class="col-sm-3">
													<form:textarea rows="" cols="" class="form-control"
														id="agency1.correspondenceAddress"
														path="agency1.correspondenceAddress"></form:textarea>
												</div>

												<label for="generalFax" class="col-sm-3">Fax</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency1.generalFax" path="agency1.generalFax"></form:input>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3"
												path="agency1.correspondenceAddress" cssClass="error" />
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency1.generalFax"
												cssClass="error" />
										</div>
										<div class="error" id="agency1generalAddressError"></div>
										<div class="error" id="agency1generalFaxError"></div>
										<!-- TENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="financialDetails" class="col-sm-3">2)Financial
													Details:</label>

											</div>
										</div>
										<!-- ELEVENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="financialEmail" class="col-sm-3"> Email
												</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency1.financialEmail" path="agency1.financialEmail"></form:input>
												</div>


												<label for="financialPhone" class="col-sm-3">Phone</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency1.financialPhone" path="agency1.financialPhone"></form:input>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency1.financialEmail"
												cssClass="error" />
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency1.financialPhone"
												cssClass="error" />
										</div>
										<div class="error" id="agency1financialEmailError"></div>
										<div class="error" id="agency1financialPhoneError"></div>

										<!-- TWELTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="financialContact" class="col-sm-3">
													Address </label>
												<div class="col-sm-3">
													<form:textarea rows="" cols="" class="form-control"
														id="agency1.financialContact"
														path="agency1.financialContact"></form:textarea>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency1.financialContact"
												cssClass="error" />

										</div>
										<div class="error" id="agency1financialAddressError"></div>

										<!-- SIXTEENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="nameOfCompany" class="col-sm-3"> Name of
													Company </label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency1.nameOfCompany" path="agency1.nameOfCompany"></form:input>
												</div>
											</div>
										</div>

										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency1.nameOfCompany"
												cssClass="error" />

										</div>
										<div class="error" id="agency1NameOfCompanyError"></div>


										<!-- NINTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="transactionInstruction" class="col-sm-3">
													Transaction Information </label>
												<div class="col-sm-3">
													<form:textarea rows="" cols="" class="form-control"
														id="agency1.transactionInstruction"
														path="agency1.transactionInstruction"></form:textarea>
												</div>
											</div>
										</div>
										<div class="error" id="agency1transactionInstructionError"></div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3"
												path="agency1.transactionInstruction" cssClass="error" />

										</div>
							
									</div>
									<!-- Agency 2 Start -->
									<div class="tab-pane" id="tab_3">
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="status" class="col-sm-6">Agency 2
													Details:</label>

											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-12">

												<label for="fromDate" class="col-sm-3">Select Agency
													2</label>
												<div class="col-sm-3">
													<form:select class="form-control preferenceSelect"
														path="agency2.agencyCode" name='agency2.agencyCode'
														id="agency2.agencyCode"
														onchange="javascript:return getContractingGovernments2();">
														<form:option value="${contract.agency2.agencyCode}"
															label="${contract.agency2.agencyCode} ${contract.agency2.name}"
															selected="selected" />
														<c:forEach var="lritAspInfo" items="${lritNameDtos}"
															varStatus="theCount">
															<form:option value="${lritAspInfo.lritid}"
																label="${lritAspInfo}" />
														</c:forEach>
													</form:select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4" id="errorselectMainCategory"></div>
										</div>

										<div class="row">
											<div class="form-group col-sm-12">
												<label for="name" class="col-sm-3"> Name of Agency</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency2.name" path="agency2.name"
														placeholder="Name of Agency"></form:input>
												</div>
											</div>
										</div>

										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency2" cssClass="error" />
										</div>
										<div class="error" id="agency2CGError"></div>
										<!-- Dynamic check Box start  -->
										<div class="row" id="cg2">
											<div class="form-group col-sm-12">

												<label for="fromDate" class="col-sm-3">Select
													Contracting Governments</label>
												<div class="col-sm-3" id="contractingGovernments2">
													<select name="contractingGovernments2" class="form-control"
														id="contractingGovernments2">

													</select>

												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="fromDate" class="col-sm-3"></label>
												<div class="col-sm-6">
													<div class="box box-info contractingGovernment">
														<div class="box-header with-border">
															<h3 class="box-title" align="center">Edit
																Contracting Governments Information</h3>
														</div>
														<div class="box-body">
															<table>
																<tr>
																	<th style="text-align: center">Contracting
																		Governments</th>
																	<th style="text-align: center">Valid From</th>
																	<th style="text-align: center">Valid To</th>
																</tr>


																<c:forEach var="agency2ContractStakeholders"
																	items="${agency2ContractStakeholders}">
																	<tr>
																		<td><input type="checkbox"
																			value="${agency2ContractStakeholders.memberId}"
																			name="contractingGov2" checked="checked" />${agency2ContractStakeholders.memberId}
																			${agency2ContractStakeholders.name}</td>
																		<td><input type="text"
																			class="form-control pull-right datepick"
																			name="agency2ContractGovValidFrom"
																			value="${agency2ContractStakeholders.validFrom}"
																			placeholder="Valid From" autocomplete="off"></input></td>
																		<td><input type="text"
																			class="form-control pull-right datepick"
																			name="agency2ContractGovValidTo"
																			value="${agency2ContractStakeholders.validTo}"
																			placeholder="Valid To" autocomplete="off"></input></td>
																	</tr>
																</c:forEach>
																<c:forEach var="agency2CG" items="${agency2CG}">
																	<tr>
																		<td><input type="checkbox"
																			value="${agency2CG.memberId}" name="contractingGov2" />${agency2CG.memberId}
																			${agency2CG.name}</td>
																		<td><input type="text"
																			class="form-control pull-right datepick"
																			 name="agency2ContractGovValidFrom"
																			value="${agency2CG.validFrom}"
																			placeholder="Valid From" autocomplete="off"></input></td>
																		<td><input type="text"
																			class="form-control pull-right datepick"
																			 name="agency2ContractGovValidTo"
																			value="${agency2CG.validTo}" placeholder="Valid To"
																			autocomplete="off"></input></td>
																	</tr>
																</c:forEach>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="name" class="col-sm-3"> Additional Contracting Govts.</label>
										<div >
											<div class="tab-pane" id="agency2AddtionalCGDetails_Tab" class="col-sm-4">
											
												<form:input path="contractLength" hidden="true" />
												<div class="error " id="agency2AddtionalCGDetailstableerror"></div>
												  
												<table id="agency2AddtionalCGDetailsTable"
													class="table table-bordered table-hover dataTable" >
													<thead>
														<tr>
															
															<th>Contracting Govt.</th>
															<th>Valid From</th>
															<th>Valid To</th>

															<th><input type="button" value="Add Row"
																class="btn btn-primary  btn-flat"
																onclick="addCGsRow()" /></th>
														</tr>
													</thead>
															<tbody>
																	<c:forEach items="${contractStakeholdersForAdditionalCGs}" var="cgs"
																		varStatus="status">
																		<tr id="cgsDetails">
														
																			<td>
																				<select class="form-control" id="contractingGovs" name="CG">
																				<option value="">--Select--</option>
																					<c:forEach items="${contractingGovts}" var="cgsdetails" varStatus="status">
																						<option value="${cgsdetails.cgLritid}">${cgsdetails.cgLritid}-${cgsdetails.cgName}</option>
																					</c:forEach>
																				</select>
																			</td>
																			<td><input type="text"
																					name="fromDate" class="datepicker_recurring_start"/></td>
																			<td><input type="text"
																					name="toDate" class="datepicker_recurring_start"/></td>
																		
																			<td style="width: 100px"><input id="removeCG"
																				type="button" value="Remove" onclick="Remove(this)"
																				class="btn btn-primary  btn-flat" /></td>
																		</tr>
																		
																	</c:forEach>
																</tbody>
													
												</table>
												<!-- </div> -->
											
										</div>
												</div>
											</div>
										</div>
									
										
										<!-- SEVENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="status" class="col-sm-3">1)General
													Details:</label>

											</div>
										</div>
										<!-- EIGHTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="generalEmail" class="col-sm-3"> Email </label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency2.generalEmail" path="agency2.generalEmail"
														pattern="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"></form:input>
												</div>


												<label for="generalPhone" class="col-sm-3">Phone</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency2.generalPhone" path="agency2.generalPhone"></form:input>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency2.generalEmail"
												cssClass="error" />
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency2.generalPhone"
												cssClass="error" />
										</div>
										<div class="error" id="agency2generalEmailError"></div>
										<div class="error" id="agency2generalPhoneError"></div>
										<!-- NINTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="correspondenceAddress" class="col-sm-3">
													Address </label>
												<div class="col-sm-3">
													<form:textarea rows="" cols="" class="form-control"
														id="agency2.correspondenceAddress"
														path="agency2.correspondenceAddress"></form:textarea>
												</div>

												<label for="generalFax" class="col-sm-3">Fax</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency2.generalFax" path="agency2.generalFax"></form:input>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3"
												path="agency2.correspondenceAddress" cssClass="error" />
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency2.generalFax"
												cssClass="error" />
										</div>
										<div class="error" id="agency2generalAddressError"></div>
										<div class="error" id="agency2generalFaxError"></div>
										<!-- TENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="financialDetails" class="col-sm-3">2)Financial
													Details:</label>

											</div>
										</div>
										<!-- ELEVENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="financialEmail" class="col-sm-3"> Email
												</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency2.financialEmail"
														pattern="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
														title="Email Should Not Contain Special Characters e.g, !, @, #, $ etc."
														path="agency2.financialEmail"></form:input>
												</div>
												<label for="financialPhone" class="col-sm-3">Phone</label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency2.financialPhone" path="agency2.financialPhone"></form:input>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency2.financialEmail"
												cssClass="error" />
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency2.financialPhone"
												cssClass="error" />
										</div>
										<div class="error" id="agency2financialEmailError"></div>
										<div class="error" id="agency2financialPhoneError"></div>
										<!-- TWELTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="financialContact" class="col-sm-3">
													Address </label>
												<div class="col-sm-3">
													<form:textarea rows="" cols="" class="form-control"
														id="agency2.financialContact"
														path="agency2.financialContact"></form:textarea>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency2.financialContact"
												cssClass="error" />
										</div>
										<div class="error" id="agency2financialContactError"></div>
										<!-- SIXTEENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="nameOfCompany" class="col-sm-3"> Name of
													Company </label>
												<div class="col-sm-3">
													<form:input type="text" class="form-control"
														id="agency2.nameOfCompany" path="agency2.nameOfCompany"></form:input>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3" path="agency2.nameOfCompany"
												cssClass="error" />
										</div>
										<div class="error" id="agency2NameOfCompanyError"></div>
										<!-- NINTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<label for="transactionInstruction" class="col-sm-3">
													Transaction Information </label>
												<div class="col-sm-3">
													<form:textarea rows="" cols="" class="form-control"
														id="agency2.transactionInstruction"
														path="agency2.transactionInstruction"></form:textarea>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<label class="col-sm-3"></label>
											<form:errors class="col-sm-3"
												path="agency2.transactionInstruction" cssClass="error" />
										</div>
										<div class="error" id="agency2TransactionInstructionError"></div>
										<!-- EIGHTEENTH ROW -->
										<div class="row">
											<div class="form-group col-sm-12">
												<div class="form-group col-sm-6"></div>
												<div class="form-group col-sm-6">
													<!-- <a class="btn btn-primary btnPrevious">Previous</a> -->
													<button type="submit" value="submit" id="submit"
														class="btn btn-primary">Update</button>
												</div>
											</div>
										</div>
									</div>

									<!-- /.tab-pane -->
								</div>
							</div>
							<div class="box-footer" id="addbtn">
								<button type="button" class="btn btn-info pull-right btn-next">Next</button>
							</div>
							<!-- Custom form ?End-->
						</div>
						<!-- /.box -->
					</form:form>


					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<!--/.col (right) -->
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script src="../js/tab_script.js"></script>


<script>
/* $(document).ready(function(){
	addCGsRow();
}); */

	function addCGsRow(){
	
	var presentRows = $("#agency2AddtionalCGDetailsTable > tbody > tr");
	
	var row = `<tr id="cgsDetails"><td><select class="form-control" name="CG"><option value="">--Select--</option><c:forEach items="${contractingGovts}" var="cgs" varStatus="status"><option value="${cgs.cgLritid}">${cgs.cgLritid}-${cgs.cgName}</option></c:forEach></select></td>
		<td><input type="text" class= "datepicker_recurring_start" name="fromDate"/></td>
		<td><input type="text" class= "datepicker_recurring_start" name="toDate"/></td>
		<td><input id="removeCG"
			type="button" value="Remove" onclick="Remove(this)"
			class="btn btn-primary  btn-flat" /></td>
		</tr>`;
	
	$('#agency2AddtionalCGDetailsTable > tbody').append(row);
	} 
	/* 
	$( ".datepicker" ).datepicker({
		  changeYear: true,
		  changeMonth: true
		}); */

	/* 	$(function() {
			$("#validFrom").datepicker({
				dateFormat : 'yy-mm-dd',
				changeYear : true,
				changeMonth : true,
				onSelect : function(selected) {
					var dt = new Date(selected);
					dt.setDate(dt.getDate() + 1);
					$("#validTo").datepicker("option", "minDate", dt);
				}
			});
			$("#validTo").datepicker({
				dateFormat : 'yy-mm-dd',
				changeYear : true,
				changeMonth : true,
				onSelect : function(selected) {
					var dt = new Date(selected);
					dt.setDate(dt.getDate() - 1);
					$("#validFrom").datepicker("option", "maxDate", dt);
				}
			});
			$("#contractAnnexureDate").datepicker({
				dateFormat : 'yy-mm-dd',
				changeYear : true,
				changeMonth : true
				
			});
		}); */
	$(function() {
		$("#validFrom").datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true,
			onSelect : function(selected) {
				$("#validTo").datepicker("option", "minDate", selected);
			}

		});
		$("#validTo").datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true,
			onSelect : function(selected) {
				$("#validFrom").datepicker("option", "maxDate", selected);
			}
		});
		$("#contractAnnexureDate").datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true

		});
		$("#cgStakeholderValidFrom").datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true

		});
		$("#cgStakeholderValidTo").datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true

		});
		$("#cgNonStakeholderValidFrom").datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true

		});
		$("#cgNonStakeholderValidTo").datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true

		});

	 $('.datepick').each(function(){
		    $(this).datepicker({
		    	format : 'yyyy-mm-dd',
				autoclose: true
				
		});
		}); 
	});
   
	/* 
	 $('.btnNext').click(function() {
	 $('.nav-tabs > .active').next('li').find('a').trigger('click');
	 });

	 $('.btnPrevious').click(function() {
	 $('.nav-tabs > .active').prev('li').find('a').trigger('click');
	 }); */
	 function getAllCGs(){
			var allCg = new Array();
			var allCg1 = document.getElementById("Agency2AddContractGovts");
			alert(allCg1);
			var i;
		
			for( i = 0; i < allCg1.size; i++){
				
				
			}
	 }
	 
	 $('body').on('focus',".datepicker_recurring_start", function(){
		    $(this).datepicker({
		    	format : 'yyyy-mm-dd',
				autoclose: true
			 });
		});	
</script>

<jsp:include page="../../finalFooter.jsp"></jsp:include>
