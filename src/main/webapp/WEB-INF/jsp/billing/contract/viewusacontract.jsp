<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="USA Contract " />
</jsp:include>
<spring:url var="getAgencyDetails" value="/contract/getAgencyDetails"></spring:url>
<spring:url var="getContractingGovernments"
	value="/contract/getContractingGovernments"></spring:url>
<spring:url var="saveusa" value="/usacontract/usacontract"></spring:url>
<spring:url var="viewContractDocument" value="/contract/upload"></spring:url>
<spring:url var="contractjs" value="/resources/js/contract.js"></spring:url>
<script src="${contractjs}"></script>
<spring:url var="viewContractAnnexure" value="/contract/uploadAnnexure"></spring:url>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			 <div class="col-md-10">
				<!-- general form elements -->


				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">USA Contract Details</h3>
					</div>


					<!-- /.box-header -->
					<!-- form start -->
					
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->
						<form:form class="form-horizontal" method="POST"
							modelAttribute="contract" enctype="multipart/form-data">
							<div class="box-body">

								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active" id="contractdetfirst"
										onclick="tabChange(id, _prev, _next)"><a 
										href="#tab_1" data-toggle="tab">Contract Details</a></li>
										
										<li id="agency1detsecond" 
										onclick="tabChange(id, _prev, _next)"><a 
										href="#tab_2" data-toggle="tab">Agency 1 Details</a></li>
										
										<li id="agency2detlast" onclick="tabChange(id, _prev, _next)">
										<a href="#tab_3" data-toggle="tab">Agency 2 Details</a></li>

										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab Dummy -->
										<div class="tab-pane active" id="tab_1">
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="contractNumber" class="col-sm-3">Contract
														Number/Purchase Order No.</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="contractNumber" path="contractNumber"
															placeholder="Contract Number" readonly="true"></form:input>
													</div>

													<label for="contractSpecialist" class="col-sm-3">Contract
														Specialist</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="contractSpecialist" path="contractSpecialist"
															placeholder="Contract Specialist" readonly="true"></form:input>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="validFrom" class="col-sm-3"> Valid From</label>
													<div class="col-sm-3">
														<div class="input-group date">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>

															<form:input type="text"
																class="form-control pull-right datepicker"
																id="validFrom" path="validFrom" placeholder="Valid From"
																autocomplete="off" readonly="true"></form:input>

														</div>
													</div>
													<label for="validTo" class="col-sm-3"> Valid To</label>
													<div class="col-sm-3">
														<div class="input-group date">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>

															<form:input type="text"
																class="form-control pull-right datepicker" id="validTo"
																path="validTo" placeholder="Valid To" autocomplete="off" readonly="true"></form:input>

														</div>
													</div>
												</div>
											</div>

											<!-- THIRD ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="awardAmount" class="col-sm-3">Award
														Amount(&#36;)</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="awardAmount" path="awardAmount"
															placeholder="Award Amount" readonly="true"></form:input>
													</div>

													<label for="amendmentNumber" class="col-sm-3">Amendment
														Number</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="amendmentNumber" path="amendmentNumber"
															placeholder="Amendment Number" readonly="true"></form:input>
													</div>
												</div>
											</div>
											<!-- THIRD ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="samRegistrationNumber" class="col-sm-3">SAM
														Registration Number</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="samRegistrationNumber" path="samRegistrationNumber"
															placeholder="SAM Registration No." readonly="true"></form:input>
													</div>
													<label for="samRegistrationDate" class="col-sm-3">
														SAM Registration Date</label>
													<div class="col-sm-3">
														<div class="input-group date">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>

															<form:input type="text"
																class="form-control pull-right datepicker"
																id="samRegistrationDate" path="samRegistrationDate"
																autocomplete="off" readonly="true"></form:input>

														</div>
													</div>
												</div>
											</div>

											<!-- THIRD ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="routingCode" class="col-sm-3">Routing
														Code</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="routingCode" path="routingCode"
															placeholder="Routing Code" readonly="true"></form:input>
													</div>
												</div>
											</div>

											<jsp:include page="documentFields.jsp">
												<jsp:param name="titleName" value="USA Contract " />
											</jsp:include>

										</div>

										<div class="tab-pane" id="tab_2">
											<jsp:include page="agency1fields.jsp">
												<jsp:param name="titleName" value="USA Contract" />
											</jsp:include>

										</div>

										<!-- Agency 2 Start -->
										<div class="tab-pane" id="tab_3">
											<jsp:include page="agency2fields.jsp">
												<jsp:param name="titleName" value="USA Contract " />
											</jsp:include>

											<!-- <a class="btn btn-primary btnPrevious">Previous</a> -->
											<%-- </form:form> --%>

											<!-- /.tab-pane -->
										</div>
										<!-- <div class="box-footer">
											<button type="button" class="btn btn-info pull-left btn-prev btnPrevious">Previous</button>
									<button type="button" class="btn btn-info pull-right btn-next btnNext">Next</button>
										</div> -->
										<div class="box-footer" id="addbtn">
									<button type="button" class="btn btn-info pull-right btn-next">Next</button>
								</div>
									</div>
								</div>
								<!-- Custom form ?End-->
							</div>

							<!-- /.box -->

						</form:form>
					
				</div>
				<!-- /.form -->
			</div>
			<!--/.col (left) -->
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
		</div>
		<!-- right column -->
		<!--/.col (right) -->

		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="../js/tab_script.js"></script>

<jsp:include page="../../finalFooter.jsp"></jsp:include>