<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="CSP Contract" />
</jsp:include>


<spring:url var="getAgencyDetails" value="/contract/getAgencyDetails"></spring:url>
<spring:url var="getContractingGovernments"
	value="/contract/getContractingGovernments"></spring:url>
<script type='javascript' src='/lritbilling//resources/js/validation.js'></script>
<spring:url var="saveCSP" value="/cspcontract/cspcontract"></spring:url>
<spring:url var="viewContractDocument" value="/contract/upload"></spring:url>
<spring:url var="viewContractAnnexure" value="/contract/uploadAnnexure"></spring:url>
<spring:url var="contractjs" value="/resources/js/contract.js"></spring:url>
<script src="${contractjs}"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">



	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			 <div class="col-md-10">
				<!-- general form elements -->
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Add CSP Contract Details</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->
						<form:form method="POST" class="form-horizontal"
							id="contractDetails" modelAttribute="contract"
							enctype="multipart/form-data">
							<div class="box-body">


								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active" id="contractdetfirst"
											onclick="tabChange(id, _prev, _next)"><a href="#tab_1"
											data-toggle="tab">Contract Details</a></li>

										<li id="agency1detsecond"
											onclick="tabChange(id, _prev, _next)"><a href="#tab_2"
											data-toggle="tab">Agency 1 Details</a></li>

										<li id="agency2detlast" onclick="tabChange(id, _prev, _next)">
											<a href="#tab_3" data-toggle="tab">Agency 2 Details</a>
										</li>

										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab Dummy -->
										<div class="tab-pane active" id="tab_1">


											<div class="row">
												<div class="form-group col-sm-12">
													<label for="contractNumber" class="col-sm-3">Contract
														Number</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="contractNumber" path="contractNumber"
															placeholder="Contract Number" readonly="true"></form:input>
													</div>
												</div>
											</div>

											<!-- FOURTH ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="validFrom" class="col-sm-3"> Valid From</label>
													<div class="col-sm-3">
														<div class="input-group date">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>

															<form:input type="text"
																class="form-control pull-right datepicker"
																id="validFrom" path="validFrom" placeholder="Valid From"
																autocomplete="off" readonly="true"></form:input>

														</div>
													</div>
													<label for="validTo" class="col-sm-3"> Valid To</label>
													<div class="col-sm-3">
														<div class="input-group date">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>
															<form:input type="text"
																class="form-control pull-right datepicker"
																path="validTo" id="validTo" placeholder="Valid To"
																autocomplete="off" readonly="true"></form:input>
														</div>
													</div>
												</div>
											</div>

											<!-- THIRD ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="managementCharges" class="col-sm-3">Annual
														Management Charges(&#8377;)</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="managementCharges" path="managementCharges"
															placeholder="Annual Management Charges" readonly="true"></form:input>
													</div>

												<%-- 	<label for="currencyType" class="col-sm-3 ">Currency
														Type</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="currencyType" path="currencyType"
															placeholder="Currency Type" readonly="true" />
													</div> --%>

												</div>
											</div>

											<jsp:include page="documentFields.jsp">
												<jsp:param name="titleName" value="CSP Contract " />
											</jsp:include>
										</div>
										<div class="tab-pane" id="tab_2">
											<jsp:include page="agency1fields.jsp">
												<jsp:param name="titleName" value="CSP Contract " />
											</jsp:include>

										</div>
										<!-- Agency 2 Start -->
										<div class="tab-pane" id="tab_3">
											<jsp:include page="agency2fields.jsp">
												<jsp:param name="titleName" value="CSP Contract " />
											</jsp:include>
											<%-- 	</form:form>
 --%>
										</div>
										<!-- EIGHTEENTH ROW -->

										<!-- <div class="box-footer">
											<button type="button" class="btn btn-info pull-left btn-prev btnPrevious">Previous</button>
									<button type="button" class="btn btn-info pull-right btn-next btnNext">Next</button>
										</div> -->
										<div class="box-footer" id="addbtn">
											<button type="button"
												class="btn btn-info pull-right btn-next">Next</button>
										</div>
										<!-- /.tab-pane -->
									</div>

									<!-- Custom form ?End-->
								</div>
								<!-- /.box -->
							</div>


						</form:form>
				
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<!--/.col (right) -->
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

<!-- Table Script -->
<script src="../js/tab_script.js"></script>


<jsp:include page="../../finalFooter.jsp"></jsp:include>