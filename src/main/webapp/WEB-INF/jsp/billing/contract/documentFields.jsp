<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<spring:url var="viewContractDocument" value="/contract/upload"></spring:url>
<spring:url var="viewContractAnnexure" value="/contract/uploadAnnexure"></spring:url>

<div class="row">
	<div class="form-group col-sm-12">
		<label for="billingCurrencyType.currencyType" class="col-sm-3">Currency Type</label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control" id="billingCurrencyType.currencyType"
				path="billingCurrencyType.currencyType" readonly="true"></form:input>
		</div>
	</div>
</div>





<c:if test="${contract.contractId > 0}">
	<div class="row">
		<div class="form-group col-sm-12">
			<label for="contract_document" class="col-sm-3">Uploaded
				Contract Document</label> <a
				href="${viewContractDocument}/${contract.contractId}"
				target="_blank">Signed Contract</a>
		</div>
	</div>
</c:if>
<c:if test="${annexureEmpty}">
	<div class="row">
		<div class="form-group col-sm-12">
			<label for="contract_document" class="col-sm-3">Uploaded
				Contract Annexures</label>
			<div class="col-sm-6">
				<c:forEach var="annexure" items="${contractAnnexures}"
					varStatus="loopCounter">
					<c:out value="${loopCounter.count}." />
					<a href="${viewContractAnnexure}/${annexure.contractAnnexureId}">Contract
						Annexure</a>&nbsp;&nbsp;&nbsp;
					Annexure Date:-&nbsp;&nbsp;<c:out value="${annexure.date}" />
					<br>
				</c:forEach>
			</div>
		</div>
	</div>
</c:if>



















