<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
	<div class="form-group col-sm-6">
		<label for="status" class="col-sm-6">Agency 2 Details:</label>
	</div>

</div>
<div class="row">
	<div class="form-group col-sm-12">

		<label for="fromDate" class="col-sm-3">Select Agency 2</label>
		<div class="col-sm-3">
			<form:select class="form-control preferenceSelect"
				path="agency2.agencyCode" name='agency2.agencyCode'
				id="agency2.agencyCode">
				<form:option value="${contract.agency2.agencyCode}"
					label="${contract.agency2.agencyCode} ${contract.agency2.name}"
					selected="selected" />

				<c:forEach var="lritAspInfo" items="${lritNameDtos}"
					varStatus="theCount">
					<form:option value="${lritAspInfo}" label="${lritAspInfo}" />

				</c:forEach>
			</form:select>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4" id="errorselectMainCategory"></div>
</div>
<div class="row">
	<div class="form-group col-sm-12">
		<label for="name" class="col-sm-3"> Name of Agency</label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control" id="agency2.name"
				path="agency2.name" placeholder="Name of Agency" readonly="true"></form:input>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group col-sm-12">
		<label for="fromDate" class="col-sm-3"></label>
		<div class="col-sm-3">
			<form:hidden path="agency2.name" />
			<c:forEach var="agency2ContractStakeholders"
				items="${agency2ContractStakeholders}" varStatus="loopCounter">
				<c:out value="${loopCounter.count}." />
				<c:out
					value="${agency2ContractStakeholders.cgLritid} ${agency2ContractStakeholders.cgName}" />
				<br>
			</c:forEach>
		</div>
	</div>
</div>
<!-- SEVENTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		 <label
			for="status" class="col-sm-3">1)General Details:</label>

	</div>
</div>
<!-- EIGHTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="generalEmail" class="col-sm-3"> Email </label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency2.generalEmail" path="agency2.generalEmail"
				pattern="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
				placeholder="General Email" readonly="true"></form:input>
		</div>


		<label for="generalPhone" class="col-sm-3">Phone</label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency2.generalPhone" path="agency2.generalPhone"
				placeholder="General Phone" readonly="true"></form:input>
		</div>
	</div>
</div>

<!-- NINTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="correspondenceAddress" class="col-sm-3"> Address </label>
		<div class="col-sm-3">
			<form:textarea rows="" cols="" class="form-control"
				id="agency2.correspondenceAddress"
				path="agency2.correspondenceAddress"
				placeholder="Correspondence Address" readonly="true"></form:textarea>
		</div>

		<label for="generalFax" class="col-sm-3">Fax</label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control" id="agency2.generalFax"
				path="agency2.generalFax" placeholder="General Fax" readonly="true"></form:input>
		</div>
	</div>
</div>

<!-- TENTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		 <label
			for="financialDetails" class="col-sm-3">2)Financial Details:</label>

	</div>
</div>
<!-- ELEVENTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="financialEmail" class="col-sm-3"> Email </label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency2.financialEmail"
				pattern="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
				title="Email Should Not Contain Special Characters e.g, !, @, #, $ etc."
				path="agency2.financialEmail" placeholder="Financial Email"
				readonly="true"></form:input>
		</div>
		<label for="financialPhone" class="col-sm-3">Phone</label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency2.financialPhone" path="agency2.financialPhone"
				placeholder="Financial Phone" readonly="true"></form:input>
		</div>
	</div>
</div>


<!-- TWELTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="financialContact" class="col-sm-3"> Address </label>
		<div class="col-sm-3">
			<form:textarea rows="" cols="" class="form-control"
				id="agency2.financialContact" path="agency2.financialContact"
				placeholder="Financial Address" readonly="true"></form:textarea>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<label class="col-sm-3"></label>
	<form:errors class="col-sm-3" path="agency2.financialContact"
		cssClass="error" />
</div>
<div class="error" id="agency2financialContactError"></div>
<!-- SIXTEENTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="nameOfCompany" class="col-sm-3"> Name of Company </label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency2.nameOfCompany" path="agency2.nameOfCompany"
				placeholder="Name of Company" readonly="true"></form:input>
		</div>
	</div>
</div>

<!-- NINTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="transactionInstruction" class="col-sm-3">
			Transaction Information </label>
		<div class="col-sm-3">
			<form:textarea rows="" cols="" class="form-control"
				id="agency2.transactionInstruction"
				path="agency2.transactionInstruction"
				placeholder="Transaction Instruction" readonly="true"></form:textarea>
		</div>
	</div>
</div>
