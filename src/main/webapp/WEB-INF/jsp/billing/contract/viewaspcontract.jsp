<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="ASP Contract " />
</jsp:include>


<spring:url var="getDDPBasedCGs" value="/contract/getDDPBasedCGs"></spring:url>
<spring:url var="getContractingGovernments"
	value="/contract/getContractingGovernments"></spring:url>
<spring:url var="getAgencyDetails" value="/contract/getAgencyDetails"></spring:url>
<script type='javascript' src='/lritbilling//resources/js/validation.js'></script>
<spring:url var="saveASPDC" value="/aspcontract/aspcontract"></spring:url>
<spring:url var="viewContractDocument" value="/contract/upload"></spring:url>
<spring:url var="viewContractAnnexure" value="/contract/uploadAnnexure"></spring:url>
<spring:url var="contractjs" value="/resources/js/contract.js"></spring:url>
<script src="${contractjs}"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			 <div class="col-md-10">
				<!-- general form elements -->


				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Add ASP/DC Contract Details</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
				
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->
						<form:form class="form-horizontal" id="contractDetails"
							enctype="multipart/form-data" modelAttribute="contract">
							<div class="box-body">
								<!-- /lrit/ -->
								<!-- Custom form -->
								<div class="nav-tabs-custom">
										<ul class="nav nav-tabs">
										<li class="active" id="contractdetfirst"
										onclick="tabChange(id, _prev, _next)"><a 
										href="#tab_1" data-toggle="tab">Contract Details</a></li>
										
										<li id="agency1detsecond" 
										onclick="tabChange(id, _prev, _next)"><a 
										href="#tab_2" data-toggle="tab">Agency 1 Details</a></li>
										
										<li id="agency2detlast" onclick="tabChange(id, _prev, _next)">
										<a href="#tab_3" data-toggle="tab">Agency 2 Details</a></li>

										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab Dummy -->
										<div class="tab-pane active" id="tab_1">


											<div class="row">
												<div class="form-group col-sm-12">
													<label for="contractNumber" class="col-sm-3">Contract
														Number</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="contractNumber" path="contractNumber"
															placeholder="Contract Number" readonly="true"></form:input>
													</div>
												</div>
											</div>
											<!-- FOURTH ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="validFrom" class="col-sm-3"> Valid From</label>
													<div class="col-sm-3">
														<div class="input-group date">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>

															<form:input type="text"
																class="form-control pull-right datepicker"
																id="validFrom" path="validFrom" placeholder="Valid From"
																readonly="true"></form:input>


														</div>
													</div>
													<label for="validTo" class="col-sm-3"> Valid To</label>
													<div class="col-sm-3">
														<div class="input-group date">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>
															<form:input type="text"
																class="form-control pull-right datepicker" id="validTo"
																path="validTo" placeholder="Valid To" readonly="true"></form:input>
														</div>
													</div>


												</div>
											</div>
											<!-- FIFTH ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="thresholdAmount" class="col-sm-3">
														Threshold Amount</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="thresholdAmount" path="thresholdAmount"
															placeholder="Threshold Amount" readonly="true"></form:input>
													</div>

													<label for="thresholdPeriod" class="col-sm-3">Threshold
														Period (In Days)</label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="thresholdPeriod" path="thresholdPeriod"
															placeholder="Threshold Period" readonly="true"></form:input>
													</div>

												</div>
											</div>
											<div class="col-sm-12">
												<label class="col-sm-3"></label>
												<form:errors class="col-sm-3" path="thresholdAmount"
													cssClass="error" />
												<label class="col-sm-3"></label>
												<form:errors class="col-sm-3" path="thresholdPeriod"
													cssClass="error" />
											</div>
											<!-- THIRTEENTH ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="invoice_due_period" class="col-sm-3">
														Payment Due Period (In Working Days) </label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="invoiceDuePeriod" path="invoiceDuePeriod"
															placeholder="Invoice Due Period" readonly="true"></form:input>
													</div>
													<label for="roi_late_payment" class="col-sm-3">
														Rate of Interest(if late payment) </label>
													<div class="col-sm-3">
														<form:input type="text" class="form-control"
															id="roiLatePayment" path="roiLatePayment"
															Placeholder="Rate of Interest " readonly="true"></form:input>
													</div>
												</div>
											</div>

											<!-- FIFTEENTH ROW -->
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="governing_law" class="col-sm-3">
														Governing Law </label>
													<div class="col-sm-3">
														<form:textarea rows="" cols="" class="form-control"
															path="governingLaw" id="governingLaw"
															placeholder="Governing Law" readonly="true"></form:textarea>
													</div>
												</div>
											</div>

											<jsp:include page="documentFields.jsp">
												<jsp:param name="titleName" value="ASP Contract " />
											</jsp:include>




										</div>
										<div class="tab-pane" id="tab_2">

											<jsp:include page="agency1fields.jsp">
												<jsp:param name="titleName" value="ASP Contract " />
											</jsp:include>
										</div>
										<!-- Agency 2 Start -->
										<div class="tab-pane" id="tab_3">

											<jsp:include page="agency2fields.jsp">
												<jsp:param name="titleName" value="ASP Contract " />
											</jsp:include>
											<div class="row">
												<div class="form-group col-sm-12">
													<div class="form-group col-sm-6"></div>
													<div class="form-group col-sm-6"></div>
												</div>
											</div>
										</div>
										<!-- /.tab-pane -->
									</div>
								</div>
								<div class="box-footer" id="addbtn">
									<button type="button" class="btn btn-info pull-right btn-next">Next</button>
								</div>


								<!-- Custom form ?End-->
							</div>
							<!-- /.box -->

						</form:form>
						<!-- /.form -->
						<!--/.col (left) -->
					
					<!-- right column -->
					<!--/.col (right) -->
				</div>
			</div>
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- Table Script -->
<script src="../js/tab_script.js"></script>


<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>