<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
	<div class="form-group col-sm-6">
		<label for="status" class="col-sm-6">Agency 1 Details:</label>

	</div>
</div>
<div class="row">
	<div class="form-group col-sm-12">
		<label for="fromDate" class="col-sm-3">Select Agency 1</label>
		<div class="col-sm-3">
			<form:select class="form-control preferenceSelect"
				path="agency1.agencyCode" name='agency1.agencyCode'
				id="agency1.agencyCode">
				<form:option
					label="${contract.agency1.agencyCode} ${contract.agency1.name}"
					selected="selected" value="${contract.agency1.agencyCode}" readonly="true" />
			</form:select>
		</div>
	</div>
</div>

<div class="row">
	<div class="form-group col-sm-12">
		<label for="fromDate" class="col-sm-3"></label>
		<div class="col-sm-3">
			<c:forEach var="agency1ContractStakeholders"
				items="${agency1ContractStakeholders}" varStatus="loopCounter">
				<c:out value="${loopCounter.count}." />
				<c:out value="${agency1ContractStakeholders.cgLritid} ${agency1ContractStakeholders.cgName}" />
				<br>
					
			</c:forEach>
		</div>
	</div>
</div>
<!-- SEVENTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		 <label
			for="status" class="col-sm-3">1)General Details:</label>

	</div>
</div>
<!-- EIGHTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="generalEmail" class="col-sm-3"> Email </label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency1.generalEmail" path="agency1.generalEmail"
				placeholder="General Mail"  readonly="true"></form:input>
		</div>
		<label for="generalPhone" class="col-sm-3">Phone</label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency1.generalPhone" path="agency1.generalPhone"
				placeholder="General Phone" readonly="true"></form:input>
		</div>
	</div>
</div>



<!-- NINTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="correspondenceAddress" class="col-sm-3"> Address </label>
		<div class="col-sm-3">
			<form:textarea rows="" cols="" class="form-control"
				id="agency1.correspondenceAddress"
				path="agency1.correspondenceAddress"
				Placeholder="Correspondence Address" readonly="true"></form:textarea>
		</div>

		<label for="generalFax" class="col-sm-3">Fax</label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control" id="agency1.generalFax"
				path="agency1.generalFax" Placeholder="General Fax" readonly="true"></form:input>
		</div>
	</div>
</div>

<!-- TENTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		 <label
			for="financialDetails" class="col-sm-3">2)Financial Details:</label>

	</div>
</div>
<!-- ELEVENTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="financialEmail" class="col-sm-3"> Email </label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency1.financialEmail" path="agency1.financialEmail"
				placeholder="Financial Email" readonly="true"></form:input>
		</div>


		<label for="financialPhone" class="col-sm-3">Phone</label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency1.financialPhone" path="agency1.financialPhone"
				placeholder="Financial Phone" readonly="true"></form:input>
		</div>
	</div>
</div>

<!-- TWELTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="financialContact" class="col-sm-3"> Address </label>
		<div class="col-sm-3">
			<form:textarea rows="" cols="" class="form-control"
				id="agency1.financialContact" path="agency1.financialContact"
				placeholder="Financial Address" readonly="true"></form:textarea>
		</div>
	</div>
</div>

<!-- SIXTEENTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="nameOfCompany" class="col-sm-3"> Name of Company </label>
		<div class="col-sm-3">
			<form:input type="text" class="form-control"
				id="agency1.nameOfCompany" path="agency1.nameOfCompany"
				placeholder="Name of Comapny" readonly="true"></form:input>
		</div>
	</div>
</div>
<!-- NINTH ROW -->
<div class="row">
	<div class="form-group col-sm-12">
		<label for="transactionInstruction" class="col-sm-3">
			Transaction Information </label>
		<div class="col-sm-3">
			<form:textarea rows="" cols="" class="form-control"
				id="agency1.transactionInstruction"
				path="agency1.transactionInstruction"
				placeholder="Transaction Instruction" readonly="true"></form:textarea>
		</div>
	</div>
</div>

