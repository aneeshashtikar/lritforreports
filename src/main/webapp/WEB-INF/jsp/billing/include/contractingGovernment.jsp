<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title" align="center">Message Charges</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<!-- Table added for shipbourne equipment details -->
		<table class="table no-margin" id="messageTable">
			<thead>
				<tr>
					<th>Contracting Government</th>
					<th>Billable Item Category</th>
					<th>Unit Price</th>
					<th>Count</th>
					<th class="verify" style="display: none;">Verified Count</th>
					<th>Cost</th>
					<th>Total USD</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><form:input type="text" class="form-control" id="" placeholder="Consumer" path="billingBillableItems[0].consumer" value="Kenya"/></td>
					<td>Billable Item Category</td>
					<td>Unit Price</td>
					<td><form:input type="text" class="form-control" id="" placeholder="Consumer" path="billingBillableItems[0].systemCount" value="120"/></td>
					<td class="verify" style="display: none;">Verified Count</td>
					<td>Cost</td>
					<td>Total USD</td>
				</tr>
				<tr>
					<td><form:input type="text" class="form-control" id="" placeholder="Consumer" path="billingBillableItems[1].consumer" value="Kenya"/></td>
					<td>Billable Item Category</td>
					<td>Unit Price</td>
					<td><form:input type="text" class="form-control" id="" placeholder="Consumer" path="billingBillableItems[1].systemCount" value="120"/></td>
					<td class="verify" style="display: none;">Verified Count</td>
					<td>Cost</td>
					<td>Total USD</td>
				</tr>
			</tbody>
		</table>
		<!-- /.table end -->
	</div>
	<!-- /.box-body -->
	<div class="box-footer clearfix"></div>
	<!-- /.box-footer -->
</div>