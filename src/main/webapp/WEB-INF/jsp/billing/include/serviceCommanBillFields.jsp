<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:include page="../include/serviceCommanFieldsForBill.jsp"></jsp:include>
<div class="row">
	<div class="form-group col-sm-6">
		<label for="billdoc" class="col-sm-4">Bill Document</label>
		<div class="col-sm-8">
			<input type="file" id="billdoc" class="form-control"
				name="billdocument" />
			<p class="help-block">Upload Signed/Approved Bill.</p>
		</div>
	</div>
</div>