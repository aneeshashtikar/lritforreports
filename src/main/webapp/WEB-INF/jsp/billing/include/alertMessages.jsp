<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${not empty message}">
	<div class="alert alert-success" role="alert">
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		${message}
	</div>
</c:if>
<c:if test="${not empty error}">
	<div class="alert alert-danger" role="alert">
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		${error}
	</div>
</c:if>