<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!-- FIRST ROW -->
<style>
.messagesScrollable {
	height: 300px;
	overflow-y: scroll
}

.scrollable {
	height: 150px;
	overflow-y: scroll
}
</style>
<div class="row">
	<div class="form-group col-sm-6">
		<label for="fromDate" class="col-sm-4">From</label>
		<div class="col-sm-8">
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<form:input type="text" data-provide="datepicker"
					class="form-control  pull-right datepicker" id="fromDate"
					placeholder="From" path="fromDate"
					pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"
					title="Enter Proper From Date." required="true" />

			</div>
		</div>
		<div class="col-sm-6">
			<form:errors path="fromDate" cssClass="error" />
		</div>
	</div>
	<div class="form-group col-sm-6">
		<label for="toDate" class="col-sm-4">Till</label>
		<div class="col-sm-8">
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				<form:input type="text" data-provide="datepicker"
					class="form-control datepicker" id="toDate" placeholder="Till"
					path="toDate"
					pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"
					title="Enter Proper Till Date." required="true" />
			</div>
		</div>
		<div class="col-sm-6">
			<form:errors path="toDate" cssClass="error" />
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group col-sm-6">
		<label for="agency" class="col-sm-4">Agency Name</label>
		<div class="col-sm-8">
			<select class="form-control" id="agency" required="required">
				<c:forEach var="agency" items="${agencys}">
					<option value="${agency.lritid}"
						<c:if test="${agency.lritid == service.billingContract.agency2.agencyCode}">selected</c:if>>${agency.name}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="form-group col-sm-6">
		<input type="button" class="btn btn-primary" id="showContract"
			onclick="showContracts(); return false;" value="Search Contract" />
	</div>

</div>

<div class="row">
	<div class="form-group col-sm-6">
		<label for="billingContract" class="col-sm-4">Contract</label>
		<div class="col-sm-8">

			<form:select class="form-control" id="billingContract"
				placeholder="Contract" path="billingContract.contractId">
				<%-- 		<form:option value="1">xyz1</form:option>
				<form:option value="2">pqr2</form:option> --%>
				<c:forEach var="contract" items="${contracts}">
					<option value="${contract.contractId}"
						<c:if test="${contract.contractId == service.billingContract.contractId}">selected</c:if>>${contract.contractNumber}</option>
				</c:forEach>
			</form:select>
		</div>
		<div class="col-sm-6">
			<form:errors path="billingContract.contractId" cssClass="error" />
		</div>
	</div>
</div>
<div class="box box-info contractingGovernment ">
	<div class="box-header with-border">
		<h3 class="box-title" align="center">Message Charges</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<!-- messageTable  listInvoices-->
		<table id="messageTable" class="table table-bordered table-striped "
			style="width: 100%">
			<thead>
				<tr>
					<th>Provider</th>
					<th>Consumer</th>
					<th>Billable Item Category</th>
					<th>Unit Price</th>
					<th>System Count</th>
					<th>Manual Count</th>
					<th>Cost</th>
					<!-- <th>Total USD</th> -->
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<!-- /.table end -->
	</div>
	<!-- /.box-body -->
	<div class="box-footer clearfix"></div>
	<!-- /.box-footer -->
</div>
<!-- Deviation for Verifying bill -->
<%-- <div class="row verify" style="display: none;">
	<div class="form-group col-sm-6">
		<label for="deviation" class="col-sm-4 ">Deviation</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control verify" id="deviation"
				placeholder="deviation" path="" />
		</div>
	</div>
</div> --%>
<!-- Subtotal Rows -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="subTotal" class="col-sm-4 ">Sub Total</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="subTotal"
				placeholder="SubTotal" path="subTotal" required="true"
				autocomplete="off" pattern="\d+(\.\d{1,2})?$"
				title="Input Should be Numberic Digits." />
		</div>
		<div class="col-sm-6">
			<form:errors path="subTotal" cssClass="error" />
		</div>
	</div>
</div>
<!-- FOURTH ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="levies" class="col-sm-4 ">Levies</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="levies"
				placeholder="Levies" path="levies" required="true"
				autocomplete="off" pattern="\d+(\.\d{1,2})?$"
				title="Levies Should be Numberic Digits." />
		</div>
		<div class="col-sm-6">
			<form:errors path="levies" cssClass="error" />
		</div>
	</div>
	<div class="form-group col-sm-6">
		<label for="taxes" class="col-sm-4 ">Taxes</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="taxes"
				placeholder="Taxes" path="taxes" required="true" autocomplete="off"
				pattern="\d+(\.\d{1,2})?$" title="Taxes Should be Numberic Digits." />
		</div>
		<div class="col-sm-6">
			<form:errors path="taxes" cssClass="error" />
		</div>
	</div>
</div>
<!-- FIFTH ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="grandTotal" class="col-sm-4 ">Grand Total</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="grandTotal"
				placeholder="Grand Total" path="grandTotal" required="true"
				autocomplete="off" pattern="\d+(\.\d{1,2})?$"
				title="Grand Total Should be Numberic Digits." />
		</div>
		<div class="col-sm-6">
			<form:errors path="grandTotal" cssClass="error" />
		</div>
	</div>
</div>
<!-- EIGHT ROW -->
<!-- This Following row is for invoice -->
<%-- <div class="row">
	<div class="form-group col-sm-6">
		<label for="invoiceNo" class="col-sm-4 "> Invoice
			Number</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control"
				id="invoiceNo" placeholder="Invoice Number"
				path="invoiceNo" />
		</div>
	</div>
</div> --%>
<!-- Bill Form Attributes -->
<%--
<div class="row">
	<div class="form-group col-sm-6">
		<label for="billNo" class="col-sm-4 ">Bill Number</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="billNo"
				placeholder="Bill Number" path="billNo" />
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group col-sm-12">
		<label for="billdoc" class="col-sm-3">Bill Document</label>
		<form:input type="file" id="billdoc" class="col-sm-3"
			path="billdoc" />
		<p class="help-block">Upload Signed/Approved Invoice.</p>
	</div>
</div> 
--%>
<!-- NINETH ROW -->
<!-- This Following row is for invoice -->
<%-- <div class="row">
	<div class="form-group col-sm-6">
		<label for="generatedCopy" class="col-sm-4 ">
			Generated Copy</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control"
				id="generatedCopy" placeholder="Generated Copy"
				path="generatedCopy" />
		</div>
	</div>
	<div class="form-group col-sm-6">
		<label for="approvedCopy" class="col-sm-4 ">Approved
			Copy</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control"
				id="approvedCopy" placeholder="Approved Copy"
				path="approvedCopy" />
		</div>
	</div>
</div>
--%>
<%--
<div class="row">
	<div class="form-group col-sm-12">
		<label for="contract_document" class="col-sm-3">Contract
			Document</label> 
			<form:input type="file" id="contract_document"
			class="col-sm-3" path=""/>
		<p class="help-block">Upload Signed Contract Here.</p>
	</div>
</div> --%>
<script>
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true
		});
	});
</script>