<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!-- FIRST ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="fromDate" class="col-sm-4 ">From</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="fromDate"
				placeholder="From" path="fromDate" readonly="true" />

		</div>
	</div>
	<div class="form-group col-sm-6">
		<label for="toDate" class="col-sm-4">Till</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="toDate"
				placeholder="Till" path="toDate" readonly="true" />
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group col-sm-6">
		<label for="" class="col-sm-4">Agency Name</label>
		<div class="col-sm-8">
			<form:input class="form-control" id="" path="billingContract.agency2Name"
															readonly="true" />
		</div>
	</div>
</div>
<!-- SECOND ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="billingContract" class="col-sm-4">Contract</label>
		<div class="col-sm-8">
			<form:input class="form-control" id="billingContract"
				placeholder="Contract" path="billingContract.contractNumber"
				readonly="true" />
		</div>
	</div>
</div>
<div class="box box-info contractingGovernment">
	<div class="box-header with-border">
		<h3 class="box-title" align="center">Message Charges</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
	<!-- Table information in billing.js  -->
		<table id="contractingGovernment"
			class="table table-bordered table-striped "
			style="display: none; width: 100%">
			<thead>
				<tr>
					<!-- <th data-data="null" ></th> -->
					<!-- <th data-data="null" data-default-content=""></th>		 -->
					<th data-data="invoiceNo" data-default-content="NA">Provider</th>
					<th data-data="invoiceNo" data-default-content="NA">Consumer</th>
					<th data-data="date" data-default-content="NA">Billable Item
						Category</th>
					<th data-data="fromDate" data-default-content="NA">Unit Price</th>
					<th data-data="toDate" data-default-content="NA">Count</th>
					<th data-data="status" data-default-content="NA">Cost</th>
					<!-- <th data-data="speed" data-default-content="NA">Total USD</th> -->
				</tr>
			</thead>
		</table>
		<!-- /.table end -->
	</div>
	<!-- /.box-body -->
	<div class="box-footer clearfix"></div>
	<!-- /.box-footer -->
</div>
<!-- Deviation for Verifying bill -->
<div class="row verify" style="display: none;">
	<div class="form-group col-sm-6">
		<label for="deviation" class="col-sm-4 ">Deviation</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control verify" id="deviation"
				placeholder="deviation" path="" />
		</div>
	</div>
</div>
<!-- Subtotal Rows -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="subTotal" class="col-sm-4 ">Sub Total</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="subTotal"
				placeholder="SubTotal" path="subTotal" readonly="true" />
		</div>
	</div>
</div>
<!-- FOURTH ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="levies" class="col-sm-4 "> Levies</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="levies"
				placeholder="Levies" path="levies" readonly="true" />
		</div>
	</div>
	<div class="form-group col-sm-6">
		<label for="taxes" class="col-sm-4 ">Taxes</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="taxes"
				placeholder="Taxes" path="taxes" readonly="true" />
		</div>
	</div>
</div>
<!-- FIFTH ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="grandTotal" class="col-sm-4 ">Grand Total</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="grandTotal"
				placeholder="Grand Total" path="grandTotal" readonly="true" />
		</div>
	</div>
</div>
<script>
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy-mm-dd'
		});
	});
</script>