<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!-- FIRST ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="fromDate" class="col-sm-4 ">From</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="fromDate"
				placeholder="Invoice From" path="fromDate" />
		</div>
	</div>
	<div class="form-group col-sm-6">
		<label for="toDate" class="col-sm-4">Till</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="toDate"
				placeholder="Invoice Till" path="toDate" />
		</div>
	</div>
</div>
<!-- SECOND ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="billingContract" class="col-sm-4">Contract</label>
		<div class="col-sm-8">
			<form:select class="form-control" id="billingContract"
				placeholder="Contract" path="billingContract.contractId">
				<form:option value="1">option 1</form:option>
				<form:option value="2">option 2</form:option>
				<form:option value="3">option 3</form:option>
				<form:option value="4">option 4</form:option>
			</form:select>
		</div>
	</div>
</div>
<!-- EIGHT ROW -->
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title" align="center">Message Charges</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<!-- Table added for shipbourne equipment details -->
		<table class="table no-margin" id="messageTable">
			<thead>
				<tr>
					<th>Contracting Government</th>
					<th>Message Type</th>
					<th>Unit Price</th>
					<th>Message Count</th>
					<th>Messaging Cost</th>
					<th>Total USD</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Contracting Government</td>
					<td>Message Type</td>
					<td>Unit Price</td>
					<td>Message Count</td>
					<td>Messaging Cost</td>
					<td>Total USD</td>
				</tr>
			</tbody>
		</table>
		<!-- /.table end -->
	</div>
	<!-- /.box-body -->
	<div class="box-footer clearfix"></div>
	<!-- /.box-footer -->
</div>
<!-- FOURTH ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="levies" class="col-sm-4 "> Levies</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="levies"
				placeholder="Levies" path="levies" />
		</div>
	</div>
	<div class="form-group col-sm-6">
		<label for="taxes" class="col-sm-4 ">Taxes</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="taxes"
				placeholder="Taxes" path="taxes" />
		</div>
	</div>
</div>
<!-- FIFTH ROW -->
<div class="row">
	<div class="form-group col-sm-6">
		<label for="subTotal" class="col-sm-4 ">Sub Total</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="subTotal"
				placeholder="SubTotal" path="subTotal" />
		</div>
	</div>
	<div class="form-group col-sm-6">
		<label for="grandTotal" class="col-sm-4 ">Grand Total</label>
		<div class="col-sm-8">
			<form:input type="text" class="form-control" id="grandTotal"
				placeholder="Grand Total" path="grandTotal" />
		</div>
	</div>
</div>