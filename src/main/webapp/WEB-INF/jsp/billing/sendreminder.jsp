<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<jsp:include page="../finalHeader.jsp">
<jsp:param name="titleName" value="Send Reminder" /></jsp:include>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- <section class="content-header">
				<h1>Contract</h1>
				<ol class="breadcrumb">
					<li><a href="/lritbilling/"><i class="fa fa-dashboard"></i>Home</a></li>
				</ol>
			</section> -->

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->


				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Send Reminder</h3>
					</div>
					<form:form class="form-horizontal" id="invoiceForm"
						modelAttribute="reminderLog">
						<div class="box-body">
							<div class="nav-tabs-custom">
								<div class="tab-content">
									<div class="tab-pane active">

										<!-- Include alertMessages  -->
										<jsp:include page="include/alertMessages.jsp"></jsp:include>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="invoice_no" class="col-sm-4">Invoice No.</label>
												<div class="col-sm-8">
													<form:input class="form-control"
														path="billingService.invoiceNo" readonly="true" />
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="invoice_date" class="col-sm-4">Invoice
													Date</label>
												<div class="col-sm-8">
													<form:label type="text" class="form-control" id="date"
														path="billingService.date">${reminderLog.billingService.date}</form:label>
												</div>
											</div>
										</div>

										<!-- FOURTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="name" class="col-sm-4"> Invoice From</label>
												<div class="col-sm-8">
													<div class="input-group date">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
														<form:label class="form-control pull-right "
															id="from_date" path="billingService.fromDate">${reminderLog.billingService.fromDate}</form:label>
													</div>
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="name" class="col-sm-4"> Invoice To</label>
												<div class="col-sm-8">
													<div class="input-group date">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>

														<form:label class="form-control pull-right" id="to_date"
															path="billingService.toDate">${reminderLog.billingService.toDate}</form:label>

													</div>
												</div>
											</div>
										</div>
										<!-- FIFTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="pending_amount" class="col-sm-4">Pending
													Amount</label>
												<div class="col-sm-8">
													<form:label type="text" class="form-control"
														id="pending_amount" path="">${remainedAmount}</form:label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="remarks" class="col-sm-4">Remarks</label>
												<div class="col-sm-8">
													<form:textarea path="remarks" class="form-control"
														placeholder="Remarks" id="remarks" required="required" />
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<form:input type="submit" class="btn btn-primary" path=""
								value="Send Reminder"></form:input>
						</div>
					</form:form>
					<!-- /.box-header -->
				</div>
				<!-- /.form -->
			</div>
			<!--/.col (left) -->
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../common.jsp"></jsp:include>
			</div>
		</div>
		<!-- right column -->
		<!--/.col (right) -->
</div>

<script>
	/* 
	$( ".datepicker" ).datepicker({
		  changeYear: true,
		  changeMonth: true
		}); */

	$(function() {
		$("#valid_from").datepicker({
			changeYear : true,
			changeMonth : true,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() + 1);
				$("#valid_to").datepicker("option", "minDate", dt);
			}
		});
		$("#valid_to").datepicker({
			changeYear : true,
			changeMonth : true,
			onSelect : function(selected) {
				var dt = new Date(selected);
				dt.setDate(dt.getDate() - 1);
				$("#valid_from").datepicker("option", "maxDate", dt);
			}
		});
	});
</script>


<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../finalFooter.jsp"></jsp:include>