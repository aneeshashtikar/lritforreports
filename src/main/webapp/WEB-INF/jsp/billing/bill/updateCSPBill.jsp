<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Bill" />
</jsp:include>
<!-- Spring URL   -->
<spring:url var="getContract" value="/contract/getContract"></spring:url>
<spring:url var="getCGBillableItemsForCSPBill"
	value="/service/getCGBillableItemsForCSPBill"></spring:url>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Update CSP Bill</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->

						<form:form class="form-horizontal" id="cspBillForm"
							modelAttribute="bill" enctype="multipart/form-data">
							<div class="box-body">

								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">CSP
												Bill Details</a></li>
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab -->
										<div class="tab-pane active" id="tab_1">
											<c:choose>
												<c:when test="${alt == '1'}">
													<div class="alert alert-danger" role="alert">${message}</div>

												</c:when>
												<c:when test="${alt == '2'}">
													<div class="alert alert-success" role="alert">${message}</div>

												</c:when>
											</c:choose>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="billNumber" class="col-sm-4">CSP Bill
														No.</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="billNo"
															path="billNo" placeholder="CSP Bill No." readonly="true"></form:input>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="fromDate" class="col-sm-4 ">From</label>
													<div class="col-sm-8">
														<form:input type="text" data-provide="datepicker"
															class="form-control datepicker" id="fromDate"
															placeholder="From" path="fromDate" readonly="true" />

													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="toDate" class="col-sm-4">Till</label>
													<div class="col-sm-8">
														<form:input type="text" data-provide="datepicker"
															class="form-control datepicker" id="toDate"
															placeholder="Till" path="toDate" readonly="true" />
													</div>
												</div>
											</div>

											<div class="row">
												<div class="form-group col-sm-6">
													<label for="" class="col-sm-4">Agency Name</label>
													<div class="col-sm-8">
														<input class="form-control" id="" value="${agencyName} "
															readonly="readonly" />
														<%-- 	<form:input class="form-control" id=""
															path="agencyName" readonly="true" /> --%>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="" class="col-sm-4">Annual Management Charges</label>
													<div class="col-sm-8">
														<input class="form-control" id="annualManagementCharges" value="${annualManagementCharges} "
															readonly="readonly" />
														<%-- 	<form:input class="form-control" id=""
															path="agencyName" readonly="true" /> --%>
													</div>
												</div>
											</div>
											<!-- SECOND ROW -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="billingContract" class="col-sm-4">Contract</label>
													<div class="col-sm-8">
														<form:input class="form-control" id="billingContract"
															placeholder="Contract"
															path="billingContract.contractNumber" readonly="true" />
													</div>
												</div>
											</div>
											<div class="box box-info contractingGovernment">
												<div class="box-header with-border">
													<h3 class="box-title" align="center">Message Charges</h3>
												</div>
												<!-- /.box-header -->
												<div class="box-body">
													<!-- messageTable  listInvoices-->
													<table id="messageTable"
														class="table table-bordered table-striped "
														style="width: 100%">
														<thead>
															<tr>
																<th>Provider</th>
																<th>Consumer</th>
																<th>Billable Item Category</th>
																<th>Unit Price</th>
																<th>Count</th>
																<th>Cost</th>
																<!-- <th>Total USD</th> -->
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${bill.billingBillableItems}"
																var="bill" varStatus="status">
																<tr>
																	<td><form:hidden
																			path="billingBillableItems[${status.index}].billableItemId" />
																		${bill.providerName}</td>
																	<td>${bill.consumerName}</td>
																	<td>${bill.billingPaymentBillableItemCategory.itemName}</td>

																	<td><form:input class="form-control"
																			id="rate_${status.index }"
																			path="billingBillableItems[${status.index}].billingPaymentBillableItemCategory.rate"
																			readonly="true" /></td>
																	<td><input class="count" alt="${status.index}"
																		id="manualcount_${status.index}"
																		name="billingBillableItems[${status.index}].manualCount"
																		value="${bill.manualCount}" /></td>
																	<td><input class="bcost"
																		name="billingBillableItems[${status.index}].cost"
																		value="${bill.cost}" id="cost_${status.index}" readonly="true"/></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
													<!-- /.table end -->
												</div>
												<!-- /.box-body -->
												<div class="box-footer clearfix"></div>
												<!-- /.box-footer -->
											</div>
											<!-- Deviation for Verifying bill -->
											<div class="row verify" style="display: none;">
												<div class="form-group col-sm-6">
													<label for="deviation" class="col-sm-4 ">Deviation</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control verify"
															id="deviation" placeholder="deviation" path="" />
													</div>
												</div>
											</div>
											<!-- Subtotal Rows -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="subTotal" class="col-sm-4 ">Sub Total</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="subTotal"
															placeholder="SubTotal" path="subTotal" readonly="true"/>
													</div>
												</div>
											</div>
											<!-- FOURTH ROW -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="levies" class="col-sm-4 "> Levies</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control levies" id="levies" 
															placeholder="Levies" path="levies" />
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="taxes" class="col-sm-4 ">Taxes</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control taxes" id="taxes" 
															placeholder="Taxes" path="taxes" />
													</div>
												</div>
											</div>
											<!-- FIFTH ROW -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="grandTotal" class="col-sm-4 ">Grand
														Total</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control"
															id="grandTotal" placeholder="Grand Total"
															path="grandTotal" />
													</div>
												</div>
											</div>
											<div class="row">

												<div class="form-group col-sm-6">
													<label for="dollarRate" class="col-sm-4">Dollar
														Rate</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control dollarRate"
															id="dollarRate" placeholder="Dollar Rate"
															path="dollarRate"/>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="remarks" class="col-sm-4 ">Remarks</label>
													<div class="col-sm-8">

														<textarea type="text" class="form-control" id="remarks"
															name="billingServiceStatuses[0].remarks"
															placeholder="Remarks, if Any" required="required"></textarea>
													</div>
												</div>
											</div>

										</div>
										<!-- First Tab End-->

									</div>
								</div>
								<!-- Custom form ?End-->
							</div>
							<div class="box-footer">
								<form:input type="submit" class="btn btn-primary" path=""
									value="Update Bill"></form:input>
							</div>
							<!-- /.box -->
						</form:form>
					
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
			<!-- right column -->
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

<spring:url var="billingjs" value="/resources/js/billing.js"></spring:url>
<script src="${billingjs}"></script>

<script>
	$(document).ready(function() {
		$('.count').keyup(calculateMessageCostForCSP);
		
	});
	$(document).ready(function() {
		$('.dollarRate').keyup(calculateMessageCostForCSPByDollarRate);
	});
	$(document).ready(function() {
		$('.levies').keyup(calculateMessageCostForCSP);
	});
	$(document).ready(function() {
		$('.taxes').keyup(calculateMessageCostForCSP);
	});
</script>
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>