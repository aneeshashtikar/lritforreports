<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="List Bill" />
</jsp:include>

<spring:url var="showBillList" value="/bill/listBill"></spring:url>

<spring:url var="updatecspbill" value="/billCSP/updateCSPBill/"></spring:url>
<spring:url var="updatebill" value="/bill/updateBill/"></spring:url>
<spring:url var="viewcspbill" value="/billCSP/viewCSPBill/"></spring:url>
<spring:url var="viewbill" value="/bill/viewBill/"></spring:url>
<spring:url var="sendverifieddg" value="/bill/sendVerifiedBillCG/"></spring:url>
<spring:url var="verifybill" value="/bill/verifyBill/"></spring:url>
<spring:url var="updatepayment" value="/bill/updatePayment/"></spring:url>

<!-- Content Wrapper. Contains page content -->
<script type="text/javascript">
	function shipSpeedFormSubmit() {
		
		showTable();
	}

	function showTable() {
		$('#listBills').css("display", "");
		var contractType = document.getElementById('contractType').value;
		var fromDate = document.getElementById('fromDate').value;
		var toDate = document.getElementById('toDate').value;
		
		var dtable = $('#listBills').DataTable({
			dom : 'lBfrtip',
			"ajax" : {
				"url" : "${showBillList}",
				"type" : "POST",
				"data" : {
					'contractType' : contractType,
					'fromDate' : fromDate,
					'toDate' : toDate
				},
				"dataSrc" : ""
			},
			"columns" : [ {
				"data" : "id",
				'render': function (data, type, row, meta) {
			        return meta.row + meta.settings._iDisplayStart + 1;
			        }
			}, {
				"data" : "billNo"
			},{
				"data" : "date"
			}, {
				"data" : "fromDate"
			}, {
				"data" : "toDate"
			}, {
				"data" : "status"
			}, {
				"searchable":false,
				"ordable":false,
				'render' : function (data, type, row) {

					var updateBillByContractType = row.contractType == 3 ? "<a href='${updatecspbill}" : "<a href='${updatebill}" ;
					var viewBillByContractType = row.contractType == 3 ? "<a href='${viewcspbill}" : "<a href='${viewbill}" ;

					var sentToDG = "<a href='${sendverifieddg}" + row.billingServiceId + "'  target=\"_blank\">Send To DG</a></br>";
					
					//var verifyBill =  "<a href='${verifybill}" + row.billingServiceId + "' target=\"_blank\">Verify Bill</a></br>";
					
					var updateBill = updateBillByContractType + row.billingServiceId + "'  target=\"_blank\">Update Bill</a></br>";
					
					var viewBill = viewBillByContractType + row.billingServiceId + "'  target=\"_blank\">View Bill</a></br>";
					
					var updatePayment = "<a href='${updatepayment}" + row.billingServiceId + "'  target=\"_blank\">Update Payment</a></br>";


					/* if(row.statusId == 8) {
						return  verifyBill + "<br>" + viewBill;
					} */
					if(row.statusId == 9 ) {
						return sentToDG + "<br>" + updateBill+ "<br>" + viewBill;
					}
					else if(row.statusId == 10 || row.statusId == 7) {
						return updatePayment+ "<br>" + viewBill;
						
					}else if(row.statusId == 2 ) {
						return sentToDG+ "<br>" + viewBill;
					}
					else if(row.statusId == 6) {
						return  viewBill;
					}
					else
						return "";

				}
			} ],
			"responsive" : {
				"details" : {
					"type" : 'column',
					"target" : 'tr'
				}
			},
			"columnDefs" : [ {
				"className" : 'control',
				"targets" : 0
			} ],
			"bDestroy" : true,
			"order": [[ 1, 'asc' ]],
			"buttons" : [ 'csv', 'excel', 'pdf' ]
		});
	}
</script>
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<!-- Small boxe (Stat box) -->
		<div class="row">
			<div class="col-md-10">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Bills</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<div class="container col-md-12"></div>
					<!-- form start -->
					<div class="box-body">
						<jsp:include page="../include/alertMessages.jsp"></jsp:include>
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="fromDate" class="col-sm-4">Contract Type</label>
								<div class="col-sm-8">
									<select class="form-control" name='contractType'
										id="contractType">
										<c:forEach var="contract" items="${contractType}">
											<option id="${contract.getContractTypeId()}"
												value="${contract.getContractTypeId()}">${contract.getTypename()}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="fromDate" class="col-sm-4 ">Bill From</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker"
										id="fromDate" placeholder="Bill From" />
								</div>
							</div>
							<div class="form-group col-sm-6">
								<label for="toDate" class="col-sm-4">Bill Till</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker" id="toDate"
										placeholder="Bill Till" />
								</div>
							</div>
						</div>

					</div>
					<div class="box-footer">
						<input type="button" class="btn btn-primary" id="listButton"
							onclick="shipSpeedFormSubmit(); return false;" value="List Bills" />
					</div>
					<!-- /.form -->
				</div>
				<div class="box">
					<!-- Report Table -->
					<div class="box-body">
						<table id="listBills" class="table table-bordered table-striped "
							style="display: none; width: 100%">
							<thead>
								<tr>

									<th data-data="null" data-default-content="">Sr. No.</th>
									<th data-data="billNo" data-default-content="">Bill No.</th>
									<th data-data="date" data-default-content="NA">Bill Date</th>
									<th data-data="fromDate" data-default-content="NA">Generated
										From</th>
									<th data-data="toDate" data-default-content="NA">Generated
										Till</th>
									<th data-data="status" data-default-content="NA">Status</th>
									<th data-data="action" data-default-content="NA">Action</th>
								</tr>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>

		</div>
	</section>
</div>
<script>
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy-mm-dd',
			autoclose: true
		});
	});
</script>
<!-- /.content -->
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>