<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Bill Verification" />
</jsp:include>
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Verify Bill</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
				
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->

					
						<form:form class="form-horizontal" id="billForm"
							action="${actionURL}" modelAttribute="bill">
							<div class="box-body">

								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Bill
												Details</a></li>
										<!-- <li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li> -->
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab -->
										<div class="tab-pane active" id="tab_1">
											
											<%-- <jsp:include page="../include/serviceViewFields.jsp"></jsp:include> --%>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="fromDate" class="col-sm-4 ">From</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="fromDate"
															placeholder="From" path="fromDate" readonly="true" />

													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="toDate" class="col-sm-4">Till</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="toDate"
															placeholder="Till" path="toDate" readonly="true" />
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="" class="col-sm-4">Agency Name</label>
													<div class="col-sm-8">
														<form:input class="form-control" id=""
															path="billingContract.agency2.name"
															readonly="true" />
													</div>
												</div>
											</div>
											<!-- SECOND ROW -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="billingContract" class="col-sm-4">Contract</label>
													<div class="col-sm-8">
														<form:input class="form-control" id="billingContract"
															placeholder="Contract"
															path="billingContract.contractNumber" readonly="true" />
															<form:hidden class="form-control" id="billingContractId"
															
															path="billingContract.contractId" value="${billingContract.contractId}" readonly="true" />
													</div>
												</div>
											</div>
											<div class="row">
										<div class="box-body">
												<table id="contractingGovernmentManual"
													class="table table-bordered table-striped "
													style="width: 100%">
													<thead>
														<tr>
															<!-- <th data-data="null" ></th> -->
															<!-- <th data-data="null" data-default-content=""></th>		 -->
															<th>Provider</th>
															<th>Consumer</th>
															<th>Billable Item Category</th>
															<th>Unit Price</th>
															<th>Manual Count</th>
															<th>System Count</th>
															<th>Cost</th>
															<!-- <th data-data="speed" data-default-content="NA">Total USD</th> -->
														</tr>
														<c:forEach items="${bill.billingBillableItems}"
															varStatus="i" var="items">
															<tr>
																<td><form:hidden
																		path="billingBillableItems[${i.index}].billableItemId" />
																	<form:input path="billingBillableItems[${i.index}].providerName" readonly="true" hidden="true"/>
																	${bill.billingBillableItems[i.index].providerName}</td>
																<td>
																	<form:input path="billingBillableItems[${i.index}].consumerName" readonly="true" hidden="true" />
																	${bill.billingBillableItems[i.index].consumerName}
																</td>
																<td>
																	<form:input path="billingBillableItems[${i.index}].billingPaymentBillableItemCategory.itemName"  readonly="true" hidden="true"/>
																	<form:hidden path="billingBillableItems[${i.index}].billingPaymentBillableItemCategory.billableItemCategoryId" value="${bill.billingBillableItems[i.index].billingPaymentBillableItemCategory.billableItemCategoryId}" readonly="true"/>
																	${bill.billingBillableItems[i.index].billingPaymentBillableItemCategory.itemName}
																</td>
																<td><form:input class="form-control"
																		id="rate_${i.index }"
																		path="billingBillableItems[${i.index}].billingPaymentBillableItemCategory.rate"
																		readonly="true" /></td>
																<td><form:input class="form-control count"
																		id="manualcount_${i.index }" alt="${i.index }"
																		path="billingBillableItems[${i.index}].manualCount" readonly="true" /></td>
																<td><form:input class="form-control count"
																		id="systemcount_${i.index }" alt="${i.index }"
																		path="billingBillableItems[${i.index}].systemCount" readonly="true"/></td>
																<td><form:input class="form-control bcost"
																		id="cost_${i.index }"
																		path="billingBillableItems[${i.index}].cost" readonly="true"/></td>
															</tr>
														</c:forEach>
													</thead>
												</table>
												<!-- /.table end -->
											</div> 
											</div>
											<div class="row verify" style="display: none;">
												<div class="form-group col-sm-6">
													<label for="deviation" class="col-sm-4 ">Deviation</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control verify"
															id="deviation" placeholder="deviation" path="" />
													</div>
												</div>
											</div>
											<!-- Subtotal Rows -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="subTotal" class="col-sm-4 ">Sub Total</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="subTotal"
															placeholder="SubTotal" path="subTotal" readonly="true" />
													</div>
												</div>
											</div>
											<!-- FOURTH ROW -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="levies" class="col-sm-4 "> Levies</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="levies"
															placeholder="Levies" path="levies" readonly="true" />
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="taxes" class="col-sm-4 ">Taxes</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="taxes"
															placeholder="Taxes" path="taxes" readonly="true" />
													</div>
												</div>
											</div>
											<!-- FIFTH ROW -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="grandTotal" class="col-sm-4 ">Grand
														Total</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control"
															id="grandTotal" placeholder="Grand Total"
															path="grandTotal" readonly="true" />
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="remarks" class="col-sm-4 ">Remarks</label>
													<div class="col-sm-8">
														<textarea  class="form-control" id="remarks"
														name="billingServiceStatuses[0].remarks"
															placeholder="Remarks, if Any"></textarea>
													</div>
												</div>
											</div>
										</div>
										<!-- First Tab End-->

									</div>
								</div>
								<!-- Custom form ?End-->
							</div>
							<div class="box-footer">
								<form:input type="submit" class="btn btn-primary" path=""
									value="Save Report"></form:input>
							</div>
							<!-- /.box -->
							<!-- <div class="box-footer">
								<button type="button" class="btn btn-info pull-left btn-prev">Previous</button>
								<button type="button" class="btn btn-info pull-right btn-next">Next</button>
							</div> -->
						</form:form>
					
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
			<!-- right column -->
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	 $(document).ready(function(){
	 /* $('.form-control').prop('readonly',true);
	 $('select[class="form-control"]').attr("disabled", true);  */
	 $('.verify').show();
	 $('.verify').prop('readonly',false);
	
	 }); 
	<!--
//-->
</script>
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>