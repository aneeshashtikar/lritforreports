<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Bill" />
</jsp:include>

<!-- Spring URL   -->
<spring:url var="getContract" value="/contract/getCGContractList"></spring:url>
<spring:url var="getAgencyWiseContracts" value="/contract/getAgencyWiseContracts"></spring:url>
<spring:url var="getAgencys" value="/contract/getAgencys"></spring:url>
<spring:url var="getCGBillableItemsForBill" value="/bill/getCGBillableItemsForBill"></spring:url>
<spring:url var="getAnnualManagementCharges" value="/bill/getAnnualManagementCharges"></spring:url>
<spring:url var="billingjs" value="/resources/js/billing.js"></spring:url>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Add CSP Bill</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->
						<form:form class="form-horizontal" id="cspBillForm" modelAttribute="bill"  enctype="multipart/form-data">
							<div class="box-body">

								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">

										<li class="active"><a href="#tab_1" data-toggle="tab">CSP Bill
												Details</a></li>
										<!-- <li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li> -->
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab -->
										<div class="tab-pane active" id="tab_1">
											<c:choose>
												<c:when test="${alt == '1'}">
													<div class="alert alert-danger" role="alert">${message}</div>

												</c:when>
												<c:when test="${alt == '2'}">
													<div class="alert alert-success" role="alert">${message}</div>

												</c:when>
											</c:choose>
												<div class="row">
												<div class="form-group col-sm-6">
													<label for="billNumber" class="col-sm-4">CSP Bill No.</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="billNo"
															path="billNo" placeholder="CSP Bill No."></form:input>
													</div>
												</div>
											</div>
											

											<div class="row">
												<div class="form-group col-sm-6">
													<label for="dollarRate" class="col-sm-4">Dollar
														Rate</label>
													<div class="col-sm-8">

														<form:input type="text" class="form-control dollarRate" id="dollarRate"
															placeholder="Dollar Rate" path="dollarRate" ></form:input>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="dollarRate" class="col-sm-4">Annual Management Charges</label>
													<div class="col-sm-8">

														<input type="text" class="form-control" id="annualManagementCharges"
															placeholder="AnnualManagementCharges" readonly="readonly"></input>
													</div>
												</div>
											</div>
											<jsp:include page="../include/serviceCommanBillFields.jsp"></jsp:include>
												<c:if test="${bill.billingServiceId > 0}">
														<div class="row">
															<div class="form-group col-sm-12">
																<label for="bill_document" class="col-sm-3">Uploaded
																	Bill</label> <a
																	href="/lritbilling//bill/uploaded/${bill.billingServiceId}">Bill</a>
															</div>
														</div>
													</c:if>
										</div>
										<!-- First Tab End-->

									</div>
								</div>
								<!-- Custom form ?End-->
							</div>
							<div class="box-footer">
								<form:input type="submit" class="btn btn-primary" path=""
									value="Save CSP Bill"></form:input>
							</div>
							<!-- /.box -->
						</form:form>
					
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
			<!-- right column -->
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script src="${billingjs}"></script>
<script>
	$(document).ready(function() {
		$('.contractingGovernment').hide();
		$('.contractingGovernment').show();
	});

	function totalCost() {
		var subtotal = 0;
		$(".bcost").each(function() {
			subtotal = subtotal + parseFloat($(this).val());
		});
		$('#subTotal').val(subtotal.toFixed(2));
		return subtotal;
	}

	function showContracts() {
		var contractType = $('#contractType').val();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		var agencyCode = $("#agency").val();

		console.log(agencyCode)
		
		var url = "${getAgencyWiseContracts}";
		var data = {
			"fromDate" : fromDate,
			"toDate" : toDate,
			"contractType" : 3,
			"agencyCode" : agencyCode
		};
		addContracts(url, data);
		$('#grandTotal').val(0.00);
	}
	
	function changeCost() {
		var alt = $(this).attr("alt");
		var cost = 0.0;
		var dollarRate = $("#dollarRate").val();
		cost = (parseFloat($("#rate_" + alt).val()) * parseFloat($(this).val())) * parseFloat(dollarRate);
		$('#cost_' + alt).val(cost.toFixed(2));
		console.log(cost);
		totalCost();
		grandTotalFn();
	}

	function getAnnualManagementCharges(){

		var contractId = $('#billingContract').val();
		var url = "${getAnnualManagementCharges}";
		var data = {
				"contractId" : contractId
			};
	
		var result = getJSONData(url, data);
		$("#annualManagementCharges").val(result.toFixed(2));
		
	}

	// COMMON AJAX DATA
	function getJSONData(url, data) {
		var jsonData = {};
		$.ajax({
			url : url,
			async : false,
			data : data
		}).done(function(result) {
			jsonData = result;
		});
		return jsonData;
	}

	function listContract() {

		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
	
		var url = "${getContract}";
		var data = {
			"fromDate" : fromDate,
			"toDate" : toDate,
			"contractType" : 3
		};
		$('#grandTotal').val(0.00);
		var result = getJSONData(url, data);

		$('#billingContract').find('option').remove();

		$('#billingContract').append(
				$("<option></option>").attr("value", "").text(
						"-- Select Contract --"));
		$.each(result, function(key, value) {
			$('#billingContract').append(
					$("<option></option>").attr("value", value.contractId)
							.text(value.contractNumber));
		});
	}

	function getBillableItems() {

		var contractId = $('#billingContract').val();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		console.log(contractId);
		console.log(fromDate);
		console.log(toDate);
		var url = "${getCGBillableItemsForBill}";
		var data = {
			"contractId" : contractId,
			"fromDate" : fromDate,
			"toDate" : toDate
			
		};

		var result = getJSONData(url, data);
		var subTotal = 0;
		//var dollarRate = $('#dollarRate').val();
		//alert("Dollar rate");
		//alert(parseFloat(dollarRate));
	$('#grandTotal').val(0.00);		
	$('#messageTable tbody').empty();

		$.each(result,function(key, value) {
							subTotal = (subTotal + parseFloat(value.cost));
							var provider = '<input type="hidden" class="form-control" name="billingBillableItems['+key+'].provider" value="' + value.provider +'" readonly="true"/>';
							var providerName = value.providerName;
							var consumer = '<input type="hidden" class="form-control" name="billingBillableItems['+key+'].consumer" value="' + value.consumer +'" readonly="true"/>';
							var consumerName =  value.consumerName;
							var itemId = '<input type="hidden" name="billingBillableItems['+key+'].billingPaymentBillableItemCategory.billableItemCategoryId" value="' + value.billingPaymentBillableItemCategory.billableItemCategoryId +'" />';
							var itemName = value.billingPaymentBillableItemCategory.itemName;
							var itemRate = '<input type="text" class="form-control" id="rate_'+key+'" value="' + value.rate +'" readonly="true"/>';
							var systemCount = '<input type="text" class="form-control" alt="'+key+'"  id="systemcount_'+key+'" name="billingBillableItems['+key+'].systemCount" value="' + value.systemCount +'" readonly="true"/>';
							var manualCount = '<input type="text" class="form-control count" id="manualcount_'+key+'" alt="'+key+'" name="billingBillableItems['+key+'].manualCount" value="' + value.manualCount +'" />';
							var cost = '<input type="text" class="form-control bcost" id="cost_'+key+'" name="billingBillableItems['+key+'].cost" value="' + value.cost +'" readonly="true"/>';  
							$("#messageTable tbody")
									.append(
											'<tr><td>'
											+  providerName + provider
											+ '</td><td>'
											+  consumerName + consumer
											+ '</td><td>'
											+ itemName + itemId
											+ '</td><td>'
											+ itemRate
											+ '</td><td>'
											+ systemCount
											+ '</td><td>'
											+ manualCount
											+ '</td><td>' + cost
											+ '</td></tr>');
							//subTotal = subTotal * parseFloat(dollarRate);
							$('#subTotal').val(subTotal.toFixed(2));
							console.log(subTotal);
							$('#grandTotal').val(0.00);
							grandTotalFn();
						});
	}

	//$("#fromDate,#toDate").change(listContract);

	$('#billingContract').change(getBillableItems);

	$('#billingContract').change(getAnnualManagementCharges);
	
	$('#subTotal,#levies,#taxes,#annualManagementCharges').change(grandTotalFn);

	$('#subTotal,#levies,#taxes,#annualManagementCharges').keypress(grandTotalFn);

	$(document.body).on('keyup', '.count', changeCost);

	$(document).ready(function() {
		$('#grandTotal').val(0.00);
		$('.dollarRate').keyup(calculateMessageCostForCSPByDollarRate);
	});
	
	function grandTotalFn() {

		var grandTotal = parseFloat($('#subTotal').val())
				+ parseFloat($('#levies').val())
				+ parseFloat($('#taxes').val())
				+ parseFloat($('#annualManagementCharges').val());
		console.log("grandTotalinFn"+grandTotal);
		if(isNaN(grandTotal)){
			$('#grandTotal').val(0.00);
		}
		else{
			$('#grandTotal').val(grandTotal.toFixed(2));
		}
		
	}
</script>
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>