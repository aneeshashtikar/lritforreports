<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../../finalHeader.jsp"></jsp:include>
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-lg-12">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Horizontal Form</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<div class="container col-md-12">
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->

						<form:form class="form-horizontal" id="invoiceForm" modelAttribute="bill" enctype="multipart/form-data">
							<div class="box-body">

								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Invoice
												Details</a></li>
										<!-- <li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li> -->
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab -->
										<div class="tab-pane active" id="tab_1">
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="billdoc" class="col-sm-4">Bill Document</label>
													<div class="col-sm-8">
														<input type="file" id="billdoc" class="form-control"
															name="billdocument" />
														<p class="help-block">Upload Signed/Approved Invoice.</p>
													</div>
												</div>
											</div>
										</div>
										<!-- First Tab End-->

									</div>
								</div>
								<!-- Custom form ?End-->
							</div>
							<div class="box-footer">
								<form:input type="submit" class="btn btn-primary" path=""
									value="Save Bill"></form:input>
							</div>
							<!-- /.box -->
							<!-- <div class="box-footer">
								<button type="button" class="btn btn-info pull-left btn-prev">Previous</button>
								<button type="button" class="btn btn-info pull-right btn-next">Next</button>
							</div> -->
						</form:form>
					</div>
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<!-- right column -->
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<jsp:include page="../../finalFooter.jsp"></jsp:include>