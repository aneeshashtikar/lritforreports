<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Bill " />
</jsp:include>
<!-- Spring URL   -->
<spring:url var="getContractList" value="/contract/getCGContractList"></spring:url>
<spring:url var="getAgencyWiseContracts" value="/contract/getAgencyWiseContracts"></spring:url>
<spring:url var="getAgencys" value="/contract/getAgencys"></spring:url>
<spring:url var="getCGBillableItemsForBill" value="/bill/getCGBillableItemsForBill"></spring:url>
<spring:url var="getCGName" value="/service/getCGName"></spring:url>
<spring:url var="billingjs" value="/resources/js/billing.js"></spring:url>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">			
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Add ASP/DC Bill</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
				
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->

						<form:form class="form-horizontal" id="billForm"
							modelAttribute="bill" enctype="multipart/form-data">
							<div class="box-body">

								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Bill
												Details</a></li>
										<!-- <li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li> -->
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab -->
										<div class="tab-pane active" id="tab_1">
											<c:choose>
												<c:when test="${alt == '1'}">
													<div class="alert alert-danger" role="alert">${message}</div>

												</c:when>
												<c:when test="${alt == '2'}">
													<div class="alert alert-success" role="alert">${message}</div>

												</c:when>
											</c:choose>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="billNumber" class="col-sm-4">Bill No.</label>
													<div class="col-sm-8">
														<form:input type="text" class="form-control" id="billNo"
															path="billNo" placeholder="Bill No."></form:input>
													</div>
												</div>
											</div>
									
											<jsp:include page="../include/serviceCommanBillFields.jsp"></jsp:include>
										</div>
										<!-- First Tab End-->

									</div>
								</div>
								<!-- Custom form ?End-->
							</div>
							<div class="box-footer">
								<form:input type="submit" class="btn btn-primary" path=""
									value="Save Bill"></form:input>
							</div>
							<!-- /.box -->
						</form:form>
				
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
			<!-- right column -->
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script src="${billingjs}"></script>
<script>
	$(document).ready(function() {
		$('.contractingGovernment').hide();
		$('.contractingGovernment').show();
	});

	function totalCost() {
		var subtotal = 0;
		$(".bcost").each(function() {
			subtotal = subtotal + parseFloat($(this).val());
		});
		$('#subTotal').val(subtotal);
		return subtotal;
	}

	function changeCost() {
		var alt = $(this).attr("alt");
		var cost = 0.0;
		cost = parseFloat($("#rate_" + alt).val()) * parseFloat($(this).val());
	
		$('#cost_' + alt).val(cost.toFixed(2));
		console.log(cost);
		totalCost();
		grandTotalFn();
	}

	// COMMON AJAX DATA
	function getJSONData(url, data) {
		var jsonData = {};
		$.ajax({
			url : url,
			async : false,
			data : data
		}).done(function(result) {
			jsonData = result;
		});
		return jsonData;
	}

	function listContract() {

		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();

		var url = "${getContractList}";
		var data = {
			"fromDate" : fromDate,
			"toDate" : toDate,
			"contractType" : 1
		};

		var result = getJSONData(url, data);

		console.log(result);
		$('#billingContract').find('option').remove();

		$('#billingContract').append(
				$("<option></option>").attr("value", "").text(
						"-- Select Contract --"));
		$.each(result, function(key, value) {
			$('#billingContract').append(
					$("<option></option>").attr("value", value.contractId)
							.text(value.contractNumber));
		});
	}

	function showContracts() {
		
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		var agencyCode = $("#agency").val();
		console.log(agencyCode)

		var url = "${getAgencyWiseContracts}";
		var data = {
			"fromDate" : fromDate,
			"toDate" : toDate,
			"contractType" : 1,
			"agencyCode" : agencyCode
		};

		addContracts(url, data);

	}
	
	function getBillableItems() {
		var contractId = $('#billingContract').val();

		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
	
		var url = "${getCGBillableItemsForBill}";
		var data = {
			"contractId" : contractId,
			"fromDate" : fromDate,
			"toDate" : toDate
		};

		console.log("fromDate " + fromDate);
		console.log("toDate " + toDate);
		var result = getJSONData(url, data);
		var subTotal = 0;

		$('#messageTable tbody').empty();

		$.each(result,function(key, value) {
							subTotal = subTotal + parseFloat(value.cost);
							var provider = '<input type="hidden" class="form-control" name="billingBillableItems['+key+'].provider" value="' + value.provider +'" readonly="true"/>';
							var providerName = value.providerName;
							var consumer = '<input type="hidden" class="form-control" name="billingBillableItems['+key+'].consumer" value="' + value.consumer +'" readonly="true"/>';
							var consumerName = value.consumerName;
							var itemId = '<input type="hidden" name="billingBillableItems['+key+'].billingPaymentBillableItemCategory.billableItemCategoryId" value="' + value.billingPaymentBillableItemCategory.billableItemCategoryId +'" />';
							var itemName = value.billingPaymentBillableItemCategory.itemName;
							var itemRate = '<input type="text" class="form-control" id="rate_'+key+'" value="' + value.rate +'" readonly="true"/>';
							var systemCount = '<input type="text" class="form-control" alt="'+key+'"  id="systemcount_'+key+'" name="billingBillableItems['+key+'].systemCount" value="' + value.systemCount +'" readonly="true"/>';
							var manualCount = '<input type="text" class="form-control count" id="manualcount_'+key+'" alt="'+key+'" name="billingBillableItems['+key+'].manualCount" value="' + value.manualCount +'" />';

							var cost = '<input type="text" class="form-control bcost" id="cost_'+key+'" name="billingBillableItems['+key+'].cost" value="' + value.cost +'" readonly="true"/>'; 
							$("#messageTable tbody")
									.append(
											'<tr><td>'
													+  providerName + provider
													+ '</td><td>'
													+  consumerName + consumer
													+ '</td><td>'
													+ itemName + itemId
													+ '</td><td>'
													+ itemRate
													+ '</td><td>'
													+ systemCount
													+ '</td><td>'
													+ manualCount
													+ '</td><td>' + cost
													+ '</td></tr>');

/* 							var cost = '<input type="text" class="form-control bcost" id="cost_'+key+'" name="billingBillableItems['+key+'].cost" value="' + value.cost +'" />';
							$("#messageTable tbody").append(
									'<tr><td>' + providerName + provider
											+ '</td><td>' + consumerName
											+ consumer + '</td><td>' + itemName
											+ itemId + '</td><td>' + itemRate
											+ '</td><td>' + systemCount
											+ manualCount + '</td><td>' + cost
											+ '</td></tr>');
 */
							$('#subTotal').val(subTotal.toFixed(2));
							console.log(subTotal);
							grandTotalFn();
						});
	}

	/* $("#fromDate,#toDate").change(listContract); */

	$('#billingContract').change(getBillableItems);

	$('#subTotal,#levies,#taxes').change(grandTotalFn);

	$('#subTotal,#levies,#taxes').keypress(grandTotalFn);

	$(document.body).on('keyup', '.count', changeCost);

	function grandTotalFn() {
		var grandTotal = parseFloat($('#subTotal').val())
				+ parseFloat($('#levies').val())
				+ parseFloat($('#taxes').val());
		$('#grandTotal').val(grandTotal);
	}
</script>
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>