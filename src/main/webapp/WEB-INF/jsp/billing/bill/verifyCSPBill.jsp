<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Bill" />
</jsp:include>
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Verify Bill</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->

						<spring:url value="/invoice/saveInvoice" var="actionURL" />
						<form:form class="form-horizontal" id="invoiceForm"
							action="${actionURL}" modelAttribute="bill">
							<div class="box-body">

								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Bill
												Details</a></li>
										<!-- <li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li> -->
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab -->
										<div class="tab-pane active" id="tab_1">
											<jsp:include page="../include/serviceViewFields.jsp"></jsp:include>
											<%-- <div class="row">
												<div class="form-group col-sm-6">
													<label for="billdoc" class="col-sm-4">Bill Document</label>
													<div class="col-sm-8">
														<form:label type="file" id="billdoc" class=""
															path="billdoc" />
														<p class="help-block">Upload Signed/Approved Invoice.</p>
													</div>
												</div>
											</div> --%>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="currencyType" class="col-sm-4 ">Currency
														Type</label>
													<div class="col-sm-8">
														<form:label type="text" class="form-control"
															id="currencyType" placeholder="Currency Type"
															path="currencyType" />
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="dollarRate" class="col-sm-4">Dollar
														Rate</label>
													<div class="col-sm-8">
														<form:label type="text" class="form-control" id="toDate"
															placeholder="Dollar Rate" path="dollarRate" />
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="remarks" class="col-sm-4 ">Remarks</label>
													<div class="col-sm-8">
														<textarea type="text" class="form-control" id="remarks"
															placeholder="Remarks, if Any"></textarea>
													</div>
												</div>
											</div>
										</div>
										<!-- First Tab End-->

									</div>
								</div>
								<!-- Custom form ?End-->
							</div>
							<div class="box-footer">
								<form:input type="submit" class="btn btn-primary" path=""
									value="Save Report"></form:input>
							</div>
							<!-- /.box -->
							<!-- <div class="box-footer">
								<button type="button" class="btn btn-info pull-left btn-prev">Previous</button>
								<button type="button" class="btn btn-info pull-right btn-next">Next</button>
							</div> -->
						</form:form>
				
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
			<!-- right column -->
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script type="text/javascript">
	 $(document).ready(function(){
	 /* $('.form-control').prop('readonly',true);
	 $('select[class="form-control"]').attr("disabled", true);  */
	 $('.verify').show();
	 $('.verify').prop('readonly',false);
	
	 }); 
	<!--
//-->
</script>
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>