<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Bill" />
</jsp:include>
<%-- <jsp:include page="../../headerHead.jsp"></jsp:include> --%>
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Update Count</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
				
						<!-- <div class="row"> -->
						<!-- <div class="col-md-10"> -->

						<form:form class="form-horizontal" id="invoiceForm" modelAttribute="bill">
							<div class="box-body">

								<!-- Custom form -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab">Bill
												Details</a></li>
										<!-- <li><a href="#tab_2" data-toggle="tab">Contracting
												Government</a></li> -->
										<li class="pull-right"><a href="#" class="text-muted"><i
												class="fa fa-gear"></i></a></li>
									</ul>
									<div class="tab-content">
										<!-- First Tab -->
										<div class="tab-pane active" id="tab_1">
											<!-- Include serviceCommanFields -->
											<jsp:include page="../include/serviceViewFields.jsp"></jsp:include>
											
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="remarks" class="col-sm-4 ">Remarks</label>
													<div class="col-sm-8">
														<textarea type="text" class="form-control" id="remarks"
															placeholder="Remarks, if Any"></textarea>
													</div>
												</div>
											</div>
										</div>
										<!-- First Tab End-->
										<!-- /.tab-pane -->
									</div>
								</div>
								<!-- Custom form ?End-->
							</div>
							<div class="box-footer">
								<form:input type="submit" class="btn btn-primary" path=""
									value="Send Invoice To DG"></form:input>
							</div>
						</form:form>
					
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			</div>
			<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
			<!-- right column -->
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<script>
	$(document).ready(function(){
		/* $('.form-control').prop("readonly",true);
		$('select[class="form-control"]').attr("disabled", true);  */
	});
</script>
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>