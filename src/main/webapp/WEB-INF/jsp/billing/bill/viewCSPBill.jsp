<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:include page="../../finalHeader.jsp">
	<jsp:param name="titleName" value="Bill" />
</jsp:include>
<!-- Spring URL   -->
<spring:url var="getContract" value="/contract/getContract"></spring:url>
<spring:url var="getCGBillableItemsForCSPBill"
	value="/service/getCGBillableItemsForCSPBill"></spring:url>
<spring:url var="viewBillDocument" value="/bill/uploaded"></spring:url>
<spring:url var="viewAdviceDocument" value="/bill/adviceDocument"></spring:url>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Update CSP Bill</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<!-- <div class="row"> -->
					<!-- <div class="col-md-10"> -->

					<form:form class="form-horizontal" id="cspBillForm"
						modelAttribute="bill" enctype="multipart/form-data">
						<div class="box-body">

							<!-- Custom form -->
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab_1" data-toggle="tab">CSP
											Bill Details</a></li>
									<li class="pull-right"><a href="#" class="text-muted"><i
											class="fa fa-gear"></i></a></li>
								</ul>
								<div class="tab-content">
									<!-- First Tab -->
									<div class="tab-pane active" id="tab_1">
										<c:choose>
											<c:when test="${alt == '1'}">
												<div class="alert alert-danger" role="alert">${message}</div>

											</c:when>
											<c:when test="${alt == '2'}">
												<div class="alert alert-success" role="alert">${message}</div>

											</c:when>
										</c:choose>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="billNumber" class="col-sm-4">CSP Bill
													No.</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="billNo"
														path="billNo" placeholder="CSP Bill No." readonly="true"></form:input>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="fromDate" class="col-sm-4 ">From</label>
												<div class="col-sm-8">
													<form:input type="text" data-provide="datepicker"
														class="form-control datepicker" id="fromDate"
														placeholder="From" path="fromDate" readonly="true" />

												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="toDate" class="col-sm-4">Till</label>
												<div class="col-sm-8">
													<form:input type="text" data-provide="datepicker"
														class="form-control datepicker" id="toDate"
														placeholder="Till" path="toDate" readonly="true" />
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="" class="col-sm-4">Bill Payee</label>
												<div class="col-sm-8">
													<input class="form-control" id="" value="${agencyName}"
														readonly="readonly" />
													<%-- 	<form:input class="form-control" id=""
															path="agencyName" readonly="true" /> --%>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-sm-6">
												<label for="" class="col-sm-4">Annual Management
													Charges</label>
												<div class="col-sm-8">
													<input class="form-control" id="annualManagementCharges"
														value="${annualManagementCharges} " readonly="readonly" />
													<%-- 	<form:input class="form-control" id=""
															path="agencyName" readonly="true" /> --%>
												</div>
											</div>
										</div>
										<!-- SECOND ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="billingContract" class="col-sm-4">Contract</label>
												<div class="col-sm-8">
													<form:input class="form-control" id="billingContract"
														placeholder="Contract"
														path="billingContract.contractNumber" readonly="true" />
												</div>
											</div>
										</div>
										<div class="box box-info contractingGovernment">
											<div class="box-header with-border">
												<h3 class="box-title" align="center">Message Charges</h3>

												<div class="box-tools pull-right">
													<button type="button" class="btn btn-box-tool"
														data-widget="collapse">
														<i class="fa fa-minus"></i>
													</button>
												</div>
											</div>
											<!-- /.box-header -->
											<div class="box-body messagesScrollable">
												<!-- messageTable  listInvoices-->
												<table id="messageTable"
													class="table table-bordered table-striped "
													style="width: 100%">
													<thead>
														<tr>
															<th>Provider</th>
															<th>Consumer</th>
															<th>Billable Item Category</th>
															<th>Unit Price</th>
															<th>Manual Count</th>
															<th>System Count</th>
															<th>Cost</th>
															<!-- <th>Total USD</th> -->
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${bill.billingBillableItems}" var="bill"
															varStatus="status">
															<tr>
																<td><form:hidden
																		path="billingBillableItems[${status.index}].billableItemId" />
																	${bill.providerName}</td>
																<td>${bill.consumerName}</td>
																<td>${bill.billingPaymentBillableItemCategory.itemName}</td>

																<td>${bill.rate}</td>
																<td>${bill.manualCount}</td>
																<td>${bill.systemCount}</td>
																<td>${bill.cost}</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<!-- /.table end -->
											</div>
											<!-- /.box-body -->
											<div class="box-footer clearfix"></div>
											<!-- /.box-footer -->
										
									</div>
									
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="subTotal" class="col-sm-4 ">Sub Total</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control" id="subTotal"
														placeholder="SubTotal" path="subTotal" readonly="true" />
												</div>
											</div>
										</div>
										<!-- FOURTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="levies" class="col-sm-4 "> Levies</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control levies"
														id="levies" placeholder="Levies" path="levies"
														readonly="true" />
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label for="taxes" class="col-sm-4 ">Taxes</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control taxes"
														id="taxes" placeholder="Taxes" path="taxes"
														readonly="true" />
												</div>
											</div>
										</div>
										<!-- FIFTH ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="grandTotal" class="col-sm-4 ">Grand
													Total</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control"
														id="grandTotal" placeholder="Grand Total"
														path="grandTotal" readonly="true" />
												</div>
											</div>
										</div>
										<div class="row">

											<div class="form-group col-sm-6">
												<label for="dollarRate" class="col-sm-4">Dollar Rate</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control dollarRate"
														id="dollarRate" placeholder="Dollar Rate"
														path="dollarRate" readonly="true" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="remarks" class="col-sm-4 ">Remarks</label>
												<div class="col-sm-8">

													<textarea type="text" class="form-control" id="remarks"
														name="billingServiceStatuses[0].remarks"
														placeholder="Remarks, if Any" required="required"
														readonly="readonly"></textarea>
												</div>
											</div>
										</div>
										
										
										<c:if test="${bill.billingServiceId > 0}">
											<div class="row">
												<div class="form-group col-sm-12">
													<label for="bill_document" class="col-sm-3">Uploaded
														Bill Document</label> <a
														href="${viewBillDocument}/${bill.billingServiceId}"
														target="_blank">Received Bill</a>
												</div>
											</div>
										</c:if>
										
										<div class="box box-info paymentDetails">
											<div class="box-header with-border">
												<h3 class="box-title" align="center">Payment Details</h3>
												<div class="box-tools pull-right">
													<button type="button" class="btn btn-box-tool"
														data-widget="collapse">
														<i class="fa fa-minus"></i>
													</button>
												</div>
											</div>
											<!-- /.box-header -->
											<div class="box-body scrollable">
												<!-- messageTable  listInvoices-->
												<table id="messageTable"
													class="table table-bordered table-striped "
													style="width: 100%">
													<thead>
														<tr>
															<th>Paid By</th>
															<th>Paid For</th>
															<th>Paid Amount</th>
															<th>Payment Date</th>
															<th>Transaction Information</th>
															<th>Payment Advice Document</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach
															items="${bill.billingPaymentDetailsForServices}"
															var="bill" varStatus="status">
															<tr>
																<td>${agencyName}</td>
																<td><c:forEach
																	items="${bill.billingPaymentDetailsPerMembers}"
																	var="billMembers">
																	${billMembers.consumerName}
																</c:forEach></td>
																<td>${bill.paidAmount}</td>
																<td>${bill.paymentDate}</td>
																<td>${bill.transactionInfo}</td>
																<td><a
																	href="${viewAdviceDocument}/${bill.pmtDetServiceId}">Payment Advice Document</a></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<!-- /.table end -->
											</div>
											
											
										<div class="box box-info statusDetails ">
											<div class="box-header with-border">
												<h3 class="box-title" align="center">Status Details</h3>
												<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool"
												data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
											</div>
											</div>
											<!-- /.box-header -->
											<div class="box-body scrollable">
												<!-- messageTable  listInvoices-->
												<table id="messageTable"
													class="table table-bordered table-striped "
													style="width: 100%">
													<thead>
														<tr>
															<th>Date</th>
															<th>Remarks</th>
															<th>Status</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach
															items="${bill.billingServiceStatuses}"
															var="status" >
															<tr>
																<td>${status.date}</td>
																<td>${status.remarks}</td>
																<td>${status.statusName}</td>
																
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<!-- /.table end -->
											</div>
											<!-- /.box-body -->
											<div class="box-footer clearfix"></div>
											<!-- /.box-footer -->
										</div>
											<!-- /.box-body -->
											<div class="box-footer clearfix"></div>
											<!-- /.box-footer -->
										</div>

									</div>
									<!-- First Tab End-->

								</div>
							</div>
							<!-- Custom form ?End-->
						</div>
						
						<!-- /.box -->
					</form:form>
				</div>
					<!-- /.form -->
				</div>
				<!--/.col (left) -->
			
				<div id="rightSideBar" class="col-md-2">
					<jsp:include page="../../common.jsp"></jsp:include>
				</div>
			<!-- right column -->
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

<script src="/lritbilling//resources/js/billing.js"></script>
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../../finalFooter.jsp"></jsp:include>