<!-- Common.jsp Start -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <!-- DataTables -->
  <spring:url value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" var="dtbootstrapmincss"/>
  <link rel="stylesheet" href="${dtbootstrapmincss}">
  <spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">

<script type="text/javascript">


var len = $('script').filter(function () {
    return ($(this).attr('src') == '/lrit/bower_components/datatables.net/js/jquery.dataTables.min.js');
}).length;
var len1 = $('script').filter(function () {
    return ($(this).attr('src') == '/lrit/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
}).length;

//if there are no scripts that match, the load it
if (len === 0) {
    $.getScript('/lrit/bower_components/datatables.net/js/jquery.dataTables.min.js');
}
if (len1 === 0) {
    $.getScript('/lrit/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
}
</script>
<spring:url value="/route/getddpversion" var="getddpversion"/>
<spring:url value="/sidebar/getreportingFrequency" var="getreportingFrequency"/>
<spring:url value="/sidebar/getflagShipCountByReportingStatus" var="getflagShipCountByReportingStatus"/>
<spring:url value="/sidebar/vesselDetailListByReportingStatus" var="vesselDetailListByReportingStatus"/>
<spring:url value="/sidebar/vesselDetailListByFrequencyRate" var="vesselDetailListByFrequencyRate"/>


	<body>
						<div class="box">
						<div class="box-header">
								<h4 class="box-title">DDP version:<br /><span id="ddpVersion"></span></h4>
						</div>
						</div>
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Flag vessels reporting status</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<div class="box-body no-padding scrollable">
								<table class="table table-condensed">

									<tr>
										<td>1.</td>
										<td>Responding Normally</td>
										<td><span class="badge bg-green"><a href="#" class="abadge" onclick="vesselReportingStatusData('Responding_Normally');"  
										id="flagShipRespondNormalIndex"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>2.</td>
										<td>Missing Position</td>
										<td><span class="badge bg-yellow"><a href="#" class="abadge" onclick="vesselReportingStatusData('Missing_Position');"  
										id="flagShipMissPositionIndex"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									 <tr>
										<td>3.</td>
										<td>Not Responding Normally</td>
										<td><span class="badge bg-red "><a href="#"  class="abadge" onclick="vesselReportingStatusData('Not_Responding');"  
										id="flagShipNotRespondIndex"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>4.</td>
										<td>Transmitting Erroneously</td>
										<td><span class="badge bg-red"> <a href="#" class="abadge" onclick="vesselReportingStatusData('Transmitting_Erroneously');"  
										id="transmittingErroneouslyCount"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>5.</td>
										<td>Inactive Ships</td>
										<td><span class="badge bg-red"><a href="#" class="abadge" onclick="vesselReportingStatusData('InactiveShip');" 
										id="InactiveShipCount"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									
								</table>
							</div>
						</div>

						<div class="box">
							<div class="box-header">
								<h3 class="box-title"> Within 1000 NM</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<div class="box-body no-padding scrollable">
								<table id = "within1000NM" class="table table-condensed">
									<!-- <tr>
										<th style="width: 10px">S. No.</th>
										<th>Country</th>
										<th>No. of ships</th>
									</tr> -->
									<!-- <tr>
										<td>1.</td>
										<td>india</td>
										<td><span class="badge bg-green">4</span></td>
									</tr> -->
								</table>
							</div>
						</div>


						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Flag vessels world wide</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<div class="box-body no-padding scrollable">
								<table id = "flagWorldWide" class="table table-condensed">
									<!-- <tr>
										<th style="width: 10px">S. No.</th>
										<th>Country</th>
										<th>No. of ships</th>
									</tr> -->
									<!-- <tr>
										<td>1.</td>
										<td>China</td>
										<td><span class="badge">1</span></td>
									</tr>
									<tr>
										<td>2.</td>
										<td>Oman</td>
										<td><span class="badge">1</span></td>
									</tr> -->
								</table>
							</div>
						</div>


						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Flag vessels by Reporting frequency</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<div class="box-body no-padding  scrollable">
								<table class="table table-condensed">
									<tr>
										<td>1.</td>
										<td>24 hrs</td>
										<td><span class="badge">
										<a href="#" class="abadge" onclick="vesselReportingStatusData('1440');" 
										id="fourteenfourty"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>2.</td>
										<td>12 hrs</td>
										<td><span class="badge">
										<a href="#" class="abadge" onclick="vesselReportingStatusData('720');" 
										id="seventwenty"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>3.</td>
										<td>6 hrs</td>
										<td><span class="badge">
										<a href="#" class="abadge" onclick="vesselReportingStatusData('360');" 
										id="threesixty"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>4.</td>
										<td>3 hrs</td>
										<td><span class="badge">
										<a href="#" class="abadge" onclick="vesselReportingStatusData('180');" 
										id="oneeighty"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>5.</td>
										<td>1 hrs</td>
										<td>
										<span class="badge">
										<a href="#" class="abadge" onclick="vesselReportingStatusData('60');" 
										id="sixty"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>6.</td>
										<td>30 mins</td>
										<td>
										<span class="badge">
										<a href="#" class="abadge" onclick="vesselReportingStatusData(30);" 
										id="thirty"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>
									<tr>
										<td>7.</td>
										<td>15 mins</td>
										<td><span class="badge">
										<a href="#" class="abadge" onclick="vesselReportingStatusData('15');" 
										id="fifteen"
	 		 				 			data-target="#vesseldetailmodel" data-toggle="modal">0</a></span></td>
									</tr>

								</table>
								</div>
							</div>
							
							<!-- Modal Data Read from GIS-->

									<div class="modal fade" id="detailmodel" role="dialog">
										<div class="modal-dialog" style="width: 95%">

											<div class="modal-content">
												<div class="modal-header">
													<h4>Ship Details</h4>
												</div>
												<div class="modal-body">
													<div class="box-body">
														<table id="SidebarDetailData"
															class="table table-bordered table-striped "
															style="width: 100%">
															<thead>
																<tr>
																	<th data-data="imo_no" data-default-content="NA">IMO Number</th>
																	<th data-data="mmsi_no" data-default-content="NA">MMSI No</th>
																	<th data-data="ship_name" data-default-content="NA">Ship Name</th>
																	<th data-data="timestamp4" data-default-content="NA">Latest Reporting Time</th>
																</tr>
															</thead>
														</table>
													</div>

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									
									<!-- End Modal Data Read from GIS-->
							
									<!-- Modal Data Read from View-->
									<div class="modal fade" id="vesseldetailmodel" role="dialog">
										<div class="modal-dialog" style="width: 95%">

											<div class="modal-content">
												<div class="modal-header">
													<h4>Ship Details</h4>
												</div>
												<div class="modal-body">
													<div class="box-body">
														<table id="SidebarVesselDetailData"
															class="table table-bordered table-striped "
															style="width: 100%">
															<thead>
																<tr>
																	<th class="target_Id" data-data="vesselId" data-default-content="">Vessel Id</th>
																	<th data-data="imoNo" data-default-content="NA">IMO Number</th>
																	<th data-data="mmsiNo" data-default-content="NA">MMSI Number</th>
																	<th data-data="vesselName" data-default-content="NA">Vessel Name</th>
																	<th data-data="callSign" data-default-content="NA">Call Sign</th>
																</tr>
															</thead>
														</table>
													</div>

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Modal Data Read from View-->
							
							</body>
							<!-- <script>

			</body>				
							var flagShipNotRespondIndex = localStorage.getItem("flagShipNotRespondIndex"); 
							var flagShipRespondNormalIndex = localStorage.getItem("flagShipRespondNormalIndex"); 
							var flagShipMissPositionIndex = localStorage.getItem("flagShipMissPositionIndex");  
							
							$("#flagShipNotRespondIndex").html(flagShipNotRespondIndex);
							$('#flagShipRespondNormalIndex').html(flagShipRespondNormalIndex);
							$('#flagShipMissPositionIndex').html(flagShipMissPositionIndex);	 
							 
							</script> -->
<script>

 var dd;
 var mapCountryWiseShipDataRightSideBar = sessionStorage.getItem("mapCountryWiseShipDataRightSideBar");
	var mapCountryWiseShipDataRightSideBarObject = JSON.parse(mapCountryWiseShipDataRightSideBar);
 function displayRightSideBarData()
 {
		var shipDataRightSideBar = sessionStorage.getItem("shipDataRightSideBar"); 
		var countLoop = 0;

		var mapFlagShipDataWorldWideRightSideBar = sessionStorage.getItem("mapFlagShipDataWorldWideRightSideBar");
		var mapFlagShipDataWorldWideRightSideBarObject = JSON.parse(mapFlagShipDataWorldWideRightSideBar);
		
		var mapCountryWiseShipDataRightSideBar = sessionStorage.getItem("mapCountryWiseShipDataRightSideBar");
		var mapCountryWiseShipDataRightSideBarObject = JSON.parse(mapCountryWiseShipDataRightSideBar);
					
			
		  /*Within 1000NM Box
		  */
		 if(mapCountryWiseShipDataRightSideBarObject!=null)
		 { 
			 $('#within1000NM').html("");	
			 $('#within1000NM').addClass("table table-condensed");
			 $('#within1000NM').append('<tbody><tr><th style="width: 10px">S. No.</th> <th>Country</th><th>No. of ships</th></tr>');
		  	 Object.entries(mapCountryWiseShipDataRightSideBarObject).forEach(function([key,value]){
			  	var tempValue = JSON.parse(value);
		 	 	countLoop = countLoop + 1;		
	 				 $('#within1000NM').append('<tr> <td> '+ countLoop +'. </td>'+							
	 		 				'<td>'+ key +' </td>' +
	 		 				'<td>' + '<a href="#" id="' +key+'-vesselnm"'+
	 		 				' data-target="#detailmodel" data-toggle="modal">' +
	 		 				 '<span class="badge">' + tempValue.length  + '</a> ' +' </span></td>' +								
	 		 				'</tr>'); 

	 				var vesselClick  = document.getElementById(key+"-vesselnm");
	 				//console.log("vessel clisck value"+vesselClick);
	 				if(vesselClick !=null)
	 				{
		 				vesselClick.onclick = function() {    
	 				    showData(tempValue);
	 				}
	 				}
		 });
		  $('#within1000NM').append('/<tbody>');
		 } 

		 /* Flag Vessel World wide Box*/
		 if(mapFlagShipDataWorldWideRightSideBarObject!=null)
		 { 
		 $('#flagWorldWide').html("");						
		 countLoop = 0;
		 $('#flagWorldWide').addClass("table table-condensed");
		 $('#flagWorldWide').append('<tbody><tr><th style="width: 10px">S. No.</th> <th>Country</th><th>No. of ships</th></tr>');
		Object.entries(mapFlagShipDataWorldWideRightSideBarObject).forEach(function([key,value]){
		 	 countLoop = countLoop + 1;		
		 	var tempValue = JSON.parse(value);
		 	$('#flagWorldWide').append('<tr> <td> '+ countLoop +'. </td>'+							
		 				'<td>'+ key +' </td>' +
		 				'<td>' + '<a href="#" id="' +key+'-vesselww"'+
		 				' data-target="#detailmodel" data-toggle="modal">' +
		 				 '<span class="badge">' + tempValue.length  + '</a> ' +' </span></td>' +								
		 				'</tr>'); 
		 	var vesselClick  = document.getElementById(key+"-vesselww");
				//console.log("vessel clisck value"+vesselClick);
				if(vesselClick !=null)
				{
 				vesselClick.onclick = function() {    
				    showData(tempValue);
				}
				}
		 });  
		$('#flagWorldWide').append('/<tbody>');
		 }		  
 }

/*Display DDP version*/ 
function getDDPVersion()
{
	console.log("calling version function")
	$.ajax({
		url : "${getddpversion}",
		success : function(result) {
			$("#ddpVersion").html(result);
		}
		});
}

/*Display frequency count in flag vessel by reporting frequecy */
 function frequencyCountFunction()
{
	console.log("Getting the frequency count")
	$.ajax({
		url : "${getreportingFrequency}",
		success : function(result) {
			if(result.FifteenMin > 0)
			{	$("#fifteen").html(result.FifteenMin);
			} if(result.ThirtyMin > 0)
			{	$("#thirty").html(result.ThirtyMin);
			}if(result.OneHr > 0)
			{	$("#sixty").html(result.OneHr);
			}if(result.ThreeHr > 0)
			{	$("#oneeighty").html(result.ThreeHr);
			}if(result.SixHr > 0)
			{	$("#threesixty").html(result.SixHr);
			}if(result.TwelveHr > 0)
			{	$("#seventwenty").html(result.TwelveHr);
			}if(result.TwentyFourHr > 0)
			{	$("#fourteenfourty").html(result.TwentyFourHr);
			}  
			}
	});
} 
	
/* Display vessel count in Flag vessels reporting status */
function flagShipByReportingStatusFunction()
{
	console.log("Getting the flag ship count by reporting status")
	$.ajax({
		url : "${getflagShipCountByReportingStatus}",
		success : function(result) {
			if(result.Responding_Normally > 0)
			{	$("#flagShipRespondNormalIndex").html(result.Responding_Normally);
			} 
			if(result.Missing_Position > 0)
			{	$("#flagShipMissPositionIndex").html(result.Missing_Position);
			}
			if(result.Not_Responding > 0)
			{	$("#flagShipNotRespondIndex").html(result.Not_Responding);
			}
			if(result.Transmitting_Erroneously > 0)
			{	$("#transmittingErroneouslyCount").html(result.Transmitting_Erroneously);
			}
			if(result.InactiveShip > 0)
			{	$("#InactiveShipCount").html(result.InactiveShip);
			}
			}
	});
} 

/*This function loads all the function on window load*/
function loadData() {
	 getDDPVersion();
	  frequencyCountFunction();
	  displayRightSideBarData();
	  flagShipByReportingStatusFunction();
}
	
/*This function shows the vessel data in pop-up window on right side bar from calculated map data*/
 function showData(value)
{
	var table = $('#SidebarDetailData').DataTable({
		dom : 'lfrtip',
		"bDestroy" : true ,
		"data" : value 
	}); 
} 


/* This function returnsv the vessel details from view on clicking on the number*/
function vesselReportingStatusData(status)
{
	var data,url;
	var varType= typeof status;
	console.log("Type of value passed on click" +varType);
	if(varType == 'string')
	{
		data = {
				'vesselStatus'	: status
			},
		url= "${vesselDetailListByReportingStatus}"
	}	
	else
	{
		data = {
				'freqRate'	: status
			};
		url= "${vesselDetailListByFrequencyRate}";
	}
	var table = $('#SidebarVesselDetailData').DataTable({
		 	"dom" : 'lfrtip',
			"bDestroy" : true ,
		 	"ajax" : {
					"url" : url,
					"data" : data ,
					"dataSrc" : ""
			},
			"columns" : [ 
				 {
				"data" : "vesselId"
			}, {
				"data" : "imoNo"
			}, {
				"data" : "mmsiNo"
			}, {
				"data" : "vesselName"
			}, {
				"data" : "callSign"
			}
			 ],
		});
}
window.onload = loadData;
</script>							
<!-- Common.jsp End -->