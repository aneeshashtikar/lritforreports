<!-- 
--Created finalform.jsp as the sample form finalized by team for Shipping Company.
--included a shipbourne table in tab_2 
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Custom Form</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<%-- <spring:url value="/resources/bower_components/Ionicons/css/ionicons.min.css" var="ioniconsmincss" /> --%>
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- Theme style -->
<%-- <spring:url value="/resources/dist/css/AdminLTE.min.css" var="adminltemincss" /> --%>
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<%-- <spring:url value="/resources/dist/css/skins/_all-skins.min.css" var="allskinmincss" /> --%>
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<%-- <spring:url value="/resources/custom.css" var="customcss" /> 
<link rel="stylesheet" href="${customcss}"> --%>

<spring:url value="/login" var="Login" />

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<spring:url
	value="/resources/lrit-dashboard.css"
	var="lritcustomcss" />
	<link rel="stylesheet" href="${lritcustomcss}">
<style>
.customform {
	text-align: left;
}
</style>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Vessel Registration Form</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Forms</a></li>
					<li class="active">General Elements</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-lg-12">
						<!-- general form elements -->


						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Horizontal Form</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->
							<div class="container col-md-12">
							<!-- <div class="row"> -->
							<!-- <div class="col-md-10"> -->
							<form class="form-horizontal" id="shipForm">
								<div class="box-body">

									<!-- Custom form -->
									<div class="nav-tabs-custom">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#tab_1" data-toggle="tab">Ship
													Details</a></li>
											<li><a href="#tab_2" data-toggle="tab">Shipbourne Equipment Id Detail</a></li>
											<li><a href="#tab_3" data-toggle="tab">Company Details</a></li>
											<li class="pull-right"><a href="#" class="text-muted"><i
													class="fa fa-gear"></i></a></li>
										</ul>
										<div class="tab-content">
											<!-- First Tab -->
											<div class="tab-pane active" id="tab_1">
												<!-- FIRST ROW -->							
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="imonumber" class="col-sm-4 ">IMO
															Number</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="imonumber" placeholder="IMO Number">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="mmsinumber" class="col-sm-4">MMSI Number</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="mmsinumber"
																placeholder="MMSI Number">
														</div>
													</div>
												</div>
												<!-- SECOND ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="shipname" class="col-sm-4 ">
															Ship Name</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="shipname" placeholder="Ship Name">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="callsign" class="col-sm-4 ">Call Sign</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="callsign"
																placeholder="call sign">
														</div>
													</div>
												</div>
												<!-- THIRD ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="yearofbuilt" class="col-sm-4">
															Year of Built</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="yearofbuilt" placeholder="Year of Built">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="grosstonnage" class="col-sm-4">Gross Tonnage</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="grosstonnage"
																placeholder="Gross Tonnage">
														</div>
													</div>
												</div>
												<!-- FOURTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="deadweight" class="col-sm-4 ">
															Dead Weight(DWT)</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="deadweight" placeholder="Dead Weight">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="registrationno" class="col-sm-4 ">Registration No</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="registrationno"
																placeholder="">
														</div>
													</div>
												</div>
												<!-- FIFTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="vesseltype" class="col-sm-4">
															Vessel Type</label>
														<div class="col-sm-8">
															<select class="form-control" id="deadweight" placeholder="Dead Weight">
																<option>option 1</option>
																<option>option 2</option>
																<option>option 3</option>
																<option>option 4</option>
																<option>option 5</option>
															</select>  
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="vesselcategory" class="col-sm-4">Vessel Category</label>
														<div class="col-sm-8">
																<select class="form-control" id="grosstonnage" placeholder="Gross Tonnage">
																<option>option 1</option>
																<option>option 2</option>
																<option>option 3</option>
																<option>option 4</option>
																<option>option 5</option>
															</select>  
														</div>
													</div>
												</div>
												<!-- SIXTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="deadweight" class="col-sm-4 ">
															P & I Club</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="piclub" placeholder="P & i cLUB">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="group" class="col-sm-4 ">Group</label>
														<div class="col-sm-8">
																<select class="form-control" id="group" placeholder="Group Name">
																<option>option 1</option>
																<option>option 2</option>
																<option>option 3</option>
																<option>option 4</option>
																<option>option 5</option>
															</select>  
														</div>
													</div>
												</div>
												<!-- SEVENTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="timedelay" class="col-sm-4 ">
															Time Delay</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="timedelay" placeholder="in minutes">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="grosstonnage" class="col-sm-4 ">Registration Status</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="grosstonnage"
																placeholder="Gross Tonnage">
														</div>
													</div>
												</div>
												
											</div>
											<!-- First Tab End-->
											<!-- /.tab-pane -->
											<div class="tab-pane" id="tab_2">
													<div class="box box-info">
														<div class="box-header with-border">
															<h3 class="box-title" align="center">Shipborne Equipment Id Detail</h3>
														</div>
														<!-- /.box-header -->
														<div class="box-body">
														       <!-- Table added for shipbourne equipment details -->
																<table class="table no-margin" id="shipborneTable">
																	<thead>
																		<tr>
																			<th>Shipborne Equipment Id</th>
																			<th>Manufact Type</th>
																			<th>Manufact Name</th>
																			<th>Manufact Model</th>
																			<th>DNID No</th>
																			<th>Member No</th>
																			<th>Ocean Region</th>
																			<th>IsActive</th>
																			<th>Reg Status</th>
																			<th>
																				<button type="button" class="btn btn-info" id="addBtn">Add Row</button>
																			</th>
																		</tr>
																	</thead>
																	<tbody>
																		
																	</tbody>
																</table>
																<!-- /.table end -->
														</div>
														<!-- /.box-body -->
														<div class="box-footer clearfix">
														  
														</div>
														<!-- /.box-footer -->
													</div>
												</div>
											<!-- /.tab-pane -->
											<div class="tab-pane" id="tab_3">
											<div class="row">
													<div class="form-group col-sm-6">
														<label for="imonumber" class="col-sm-4 ">Shipping Company Name</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="imonumber" placeholder="IMO Number">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="mmsinumber" class="col-sm-4">Shipping Company Code</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="mmsinumber"
																placeholder="MMSI Number">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="imonumber" class="col-sm-4 ">Owner Company Name</label>
														<div class="col-sm-8">
															<select class="form-control" id="group" placeholder="Group Name">
																<option>option 1</option>
																<option>option 2</option>
																<option>option 3</option>
																<option>option 4</option>
																<option>option 5</option>
															</select> 
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="mmsinumber" class="col-sm-4">Owner Company Code</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="mmsinumber"
																placeholder="MMSI Number">
														</div>
													</div>
												</div>
													<div class="row">
													<div class="form-group col-sm-6">
														<label for="imonumber" class="col-sm-4 ">Manager Company Name</label>
														<div class="col-sm-8">
															<select class="form-control" id="group" placeholder="Group Name">
																<option>option 1</option>
																<option>option 2</option>
																<option>option 3</option>
																<option>option 4</option>
																<option>option 5</option>
															</select> 
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="mmsinumber" class="col-sm-4">Manager Company Code</label>
														<div class="col-sm-8">
															<input type="text" class="form-control" id="mmsinumber"
																placeholder="MMSI Number">
														</div>
													</div>
												</div>
													<div class="row">
													<div class="form-group col-sm-6">
														<label for="imonumber" class="col-sm-4 ">Company Security Officer</label>
														<div class="col-sm-8">
															<input type="text" class="form-control"
																id="imonumber" placeholder="IMO Number">
														</div>
													</div>
												</div>
											</div>
											<!-- /.tab-pane -->
										</div>
									</div>
									<!-- Custom form ?End-->
								</div>
								<!-- /.box -->
								<div class="box-footer">
								    <button type="button" class="btn btn-info pull-left btn-prev">Previous</button>
									<button type="button" class="btn btn-info pull-right btn-next">Next</button>
								</div>
							</form>
							</div>
							<!-- /.form -->
							</div>
							<!--/.col (left) -->
							</div>
							<!-- right column -->
							<!--/.col (right) -->
						</div>
						<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.4.0
			</div>
			<strong>Copyright &copy; 2014-2016 <a
				href="https://adminlte.io">Almsaeed Studio</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<spring:url value="/bower_components/jquery/dist/jquery.min.js"
		var="jqueryminjs" />
	<script src="${jqueryminjs}"></script>
	<!-- Bootstrap 3.3.7 -->
	<spring:url
		value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
		var="bootstrapminjs" />
	<script src="${bootstrapminjs}"></script>
	<!-- FastClick -->
	<spring:url value="/bower_components/fastclick/lib/fastclick.js"
		var="fastclickjs" />
	<script src="${fastclickjs}"></script>
	<!-- AdminLTE App -->

	<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
	<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
	<script src="${adminlteminjs}"></script>
	<!-- AdminLTE for demo purposes -->
	<spring:url value="/dist/js/demo.js" var="demojs" />
	<%-- <spring:url value="/resources/dist/js/demo.js" var="demojs" /> --%>
	<script src="${demojs}"></script>
	<!-- Table Script -->
	<script src="js/table_script.js"></script>
</body>
</html>