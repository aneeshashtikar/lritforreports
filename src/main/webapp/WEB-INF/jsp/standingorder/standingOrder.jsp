<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Standing Order</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->

<spring:url value="/getSODetail" var="getSODetail" />
<spring:url value="/getGeoDetail" var="getGeoDetail" />
<spring:url value="/getExCountry" var="getExCountry" />
<spring:url value="/getExShip" var="getExShip" />
<spring:url value="/updateStatus" var="updateStatus" />
<spring:url value="/editStandingOrder" var="editStandingOrder" />
<spring:url value="/callDC" var="callDC" />

<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<%-- <spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url> --%>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>


<style>
.table_wrapper {
	display: block;
	overflow-x: auto;
	white-space: nowrap;
}
</style>
</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>

		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">
						<div id="status"></div>
						<!-- Horizontal Form -->
						<!-- <div class="box box-info"> -->
						<div class="box-header with-border">
							<h3 class="box-title">Standing Order</h3>
						</div>
						<!-- form start -->
						<!-- /.box-header -->
						<div class="box-body">
							<!-- Custom form -->
							<div class="form-group">
								<div class="col-sm-3">
									<select class="form-control" id="SOSelect"
										onchange="operateSOFunc()">
										<option value="allSO" selected>All SO</option>
										<option value="createdSO">Created SO</option>
										<option value="approvedSO">Approved SO</option>
										<option value="openedSO">Opened SO</option>
										<option value="rejectedSO">Rejected SO</option>
										<option value="failedSO">Failed SO</option>
										<option value="closedSO">Closed SO</option>
										<option value="deletedSO">Deleted SO</option>
									</select>
								</div>
								<a href="createStandingOrder" target="_blank"><button
										type="button" class="btn btn-primary col-sm-2">Create SO</button></a>
							</div>

							<div class="form-group" style="padding-top:40px;">
								<div class="col-sm-12">
									<div id="divallSO">
										<table id="allSO" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>SO Id</th>
													<!-- <th>Message Id</th> -->
													<th>DDP Version</th>
													<th>Created Date</th>
													<th>Parent SO Id</th>
													<th>Lrit Id</th>
													<th>Status</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divcreatedSO" style="display: none">
										<table id="createdSO" style="width:100%"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>SO Id</th>
													<!-- <th>Message Id</th> -->
													<th>DDP Version</th>
													<th>Created Date</th>
													<th>Parent SO Id</th>
													<th>Lrit Id</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divapprovedSO" style="display: none">
										<table id="approvedSO" style="width:100%"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>SO Id</th>
													<!-- <th>Message Id</th> -->
													<th>DDP Version</th>
													<th>Created Date</th>
													<th>Parent SO Id</th>
													<th>Lrit Id</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divopenedSO" style="display: none">
										<table id="openedSO" style="width:100%"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>SO Id</th>
													<th>Message Id</th>
													<th>DDP Version</th>
													<th>Created Date</th>
													<th>Parent SO Id</th>
													<th>Lrit Id</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divrejectedSO" style="display: none">
										<table id="rejectedSO" style="width:100%"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>SO Id</th>
													<!-- <th>Message Id</th> -->
													<th>DDP Version</th>
													<th>Created Date</th>
													<th>Parent SO Id</th>
													<th>Lrit Id</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divfailedSO" style="display: none">
										<table id="failedSO" style="width:100%"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>SO Id</th>
													<!-- <th>Message Id</th> -->
													<th>DDP Version</th>
													<th>Created Date</th>
													<th>Parent SO Id</th>
													<th>Lrit Id</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divclosedSO" style="display: none">
										<table id="closedSO" style="width:100%"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>SO Id</th>
													<!-- <th>Message Id</th> -->
													<th>DDP Version</th>
													<th>Created Date</th>
													<th>Parent SO Id</th>
													<th>Lrit Id</th>
												</tr>
											</thead>
										</table>
									</div>
									<div id="divdeletedSO" style="display: none">
										<table id="deletedSO" style="width:100%"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>SO Id</th>
													<!-- <th>Message Id</th> -->
													<th>DDP Version</th>
													<th>Created Date</th>
													<th>Parent SO Id</th>
													<th>Lrit Id</th>
													<th>Action</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>

		<div class="modal fade" id="SOModal" role="dialog">
			<div class="modal-dialog" style="width: 50%">

				<div class="modal-content">
					<div class="modal-header">
						<h4>SO Details</h4>
					</div>
					<div class="modal-body">
						<div class="box-body">
							<div class="col-sm-4">
								<table id="geoArea" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Selected Area</th>
										</tr>
									</thead>
								</table>
							</div>
							<div id="divexCoun" class="col-sm-4">
								<table id="exCoun" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Excluded Countries</th>
										</tr>
									</thead>
								</table>
							</div>
							<div id="divexShip" class="col-sm-4">
								<table id="exShip" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Excluded Vessel Type</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<script>
		var allSOTable, createdSOTable, approvedSOTable, openedSOTable, rejectedSOTable, failedSOTable, closedSOTable, deletedSOTable, geoAreatable, soId, areacode;
		
		$(document).ready(function() {
			allSOTable = $('#allSO').DataTable({
				"ajax" : {
					"url" : "${getSODetail}",
					"data" : {
						'status' : "All"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "standingorderId",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				}, /* {
					"data" : "portalRequest.messageId"
				}, */ {
					"data" : "portalRequest.currentddpversion"
				}, {
					"data" : "portalRequest.requestGenerationTime"
				}, {
					"data" : "parentsoid"/* ,
					"render" : function(data, type, row, meta) {
						return '<a>' + data + '</a>';
					} */
				}, {
					"data" : "lritId"
				}, {
					"data" : "status"
				} ],
				"order" : [ [ 1, 'asc' ] ],
				"paging" : true//,
				//"pageLength": 15,
				//"lengthChange": false,
				//"scrollY" : "500px",
				//"scrollCollapse" : true,
				//"info" : false
			});//end datatable function
			$('#allSO tbody').on('click', 'a', function() {
				var data = allSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				showDetails();
			});
		}); //end document.ready
		
		$(document).ready(function() {
			createdSOTable = $('#createdSO').DataTable({
				"ajax" : {
					"url" : "${getSODetail}",
					"data" : {
						'status' : "Created"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "standingorderId",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				}, /* {
					"data" : "portalRequest.messageId"
				}, */ {
					"data" : "portalRequest.currentddpversion"
				}, {
					"data" : "portalRequest.requestGenerationTime"
				}, {
					"data" : "parentsoid"
				}, {
					"data" : "lritId"
				}, {
					"data" : ""
				} ],
				"columnDefs" : [ {
					"targets" : -1,
					"defaultContent" : "&nbsp;&nbsp;&nbsp;<button class=\"edit btn btn-primary\">Edit</button>&nbsp;&nbsp;&nbsp;<button class=\"approve btn btn-primary\">Approve</button>&nbsp;&nbsp;&nbsp;<button class=\"delete btn btn-primary\">Delete</button>"
				} ],
				"order" : [ [ 1, 'asc' ] ],
				"paging" : true//,
				//"scrollY" : "500px",
				//"scrollCollapse" : true,
				//"info" : false
			});//end datatable function
			
			$('#createdSO tbody').on('click', 'a', function() {
				var data = createdSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				showDetails();
			});

			$('#createdSO tbody').on('click', '.edit', function() { 
				var data = createdSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				editSO();
			});
			$('#createdSO tbody').on('click', '.approve', function() { 
				var data = createdSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				var result = updateStatus("Approved");
				if(!(result.localeCompare("Successfully Updated"))){
					$("#status")
					.html(
							"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Approved</div>");
				}else{
					$("#status")
					.html(
							"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed</div>");
				}
			});
			$('#createdSO tbody').on('click', '.delete', function() { 
				var data = createdSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				var result = updateStatus("Deleted");
				if(!(result.localeCompare("Successfully Updated"))){
					$("#status")
					.html(
							"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Deleted</div>");
				}else{
					$("#status")
					.html(
							"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed</div>");
				}
			});
		}); //end document.ready

		$(document).ready(function() {
			approvedSOTable = $('#approvedSO').DataTable({
				"ajax" : {
					"url" : "${getSODetail}",
					"data" : {
						'status' : "Approved"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "standingorderId",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				}, /* {
					"data" : "portalRequest.messageId"
				}, */ {
					"data" : "portalRequest.currentddpversion"
				}, {
					"data" : "portalRequest.requestGenerationTime"
				}, {
					"data" : "parentsoid"
				}, {
					"data" : "lritId"
				}, {
					"data" : ""
				} ],
				"columnDefs" : [ {
					"orderable" : false,
					"targets" : -1,
					"data" : null,
					"defaultContent" : "&nbsp;&nbsp;&nbsp;<button class=\"open btn btn-primary\">Open</button>&nbsp;&nbsp;&nbsp;<button class=\"reject btn btn-primary\">Reject</button>"
				} ],
				"order" : [ [ 1, 'asc' ] ],
				"paging" : true//,
				//"scrollY" : "500px",
				//"scrollCollapse" : true,
				//"info" : false
			});//end datatable function
			
			$('#approvedSO tbody').on('click', 'a', function() {
				var data = approvedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				showDetails();
			});

			$('#approvedSO tbody').on('click', '.open', function() {
				var data = approvedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				//code to call DC
				$.ajax({url : "${callDC}",type : "POST",data : {SOId : soId},
										success : function(data) {
											if(data == "true"){
												$("#status")
												.html(
														"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Opened</div>");
												updateStatus("Opened");
											}else{
												$("#status")
												.html(
														"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed to Open</div>");
												updateStatus("Failed");
											}
										},
										error : function(error) {
											$("#status")
											.html(
													"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed to Open</div>");
											updateStatus("Failed");
										}
									});
			});

			$('#approvedSO tbody').on('click', '.reject', function() {
				var data = approvedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				var result = updateStatus("Rejected");
				if(!(result.localeCompare("Successfully Updated"))){
					$("#status")
					.html(
							"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Rejected</div>");
				}else{
					$("#status")
					.html(
							"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed</div>");
				}
			});
		}); //end document.ready

		$(document).ready(function() {
			openedSOTable = $('#openedSO').DataTable({
				"ajax" : {
					"url" : "${getSODetail}",
					"data" : {
						'status' : "Opened"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "standingorderId",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				}, {
					"data" : "portalRequest.messageId"
				}, {
					"data" : "portalRequest.currentddpversion"
				}, {
					"data" : "portalRequest.requestGenerationTime"
				}, {
					"data" : "parentsoid"
				}, {
					"data" : "lritId"
				}, {
					"data" : ""
				} ],
				"columnDefs" : [ {
					"orderable" : false,
					"targets" : -1,
					"data" : null,
					"defaultContent" : "&nbsp;&nbsp;&nbsp;<button class=\"closed btn btn-primary\">Close</button>&nbsp;&nbsp;&nbsp;<button class=\"edit btn btn-primary\">Edit</button>"
				} ],
				"order" : [ [ 1, 'asc' ] ],
				"paging" : true//,
				//"scrollY" : "500px",
				//"scrollCollapse" : true,
				//"info" : false
			});//end datatable function
			
			$('#openedSO tbody').on('click', 'a', function() {
				var data = openedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				showDetails();
			});

			$('#openedSO tbody').on('click', '.closed', function() {
				var data = openedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				$.ajax({url : "${callDC}",type : "POST",data : {SOId : soId},
					success : function(data) {
						if(data == "true"){
							$("#status")
							.html(
									"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Closed</div>");
							updateStatus("Closed");
						}else{
							$("#status")
							.html(
									"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed to Close</div>");
							updateStatus("Opened");
						}
					},
					error : function(error) {
						$("#status")
						.html(
								"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed to Close</div>");
						updateStatus("Opened");
					}
				});
			});
			
			$('#openedSO tbody').on('click', '.edit', function() { 
				var data = openedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				editSO();
			});
		}); //end document.ready

		$(document).ready(function() {
			failedSOTable = $('#failedSO').DataTable({
				"ajax" : {
					"url" : "${getSODetail}",
					"data" : {
						'status' : "Failed"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "standingorderId",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				}, /* {
					"data" : "portalRequest.messageId"
				}, */ {
					"data" : "portalRequest.currentddpversion"
				}, {
					"data" : "portalRequest.requestGenerationTime"
				}, {
					"data" : "parentsoid"
				}, {
					"data" : "lritId"
				}, {
					"data" : ""
				} ],
				"columnDefs" : [ {
					"orderable" : false,
					"targets" : -1,
					"data" : null,
					"defaultContent" : "<button class=\"reopen btn btn-primary\">Reopen</button>"
				} ],
				"order" : [ [ 1, 'asc' ] ],
				"paging" : true//,
				//"scrollY" : "500px",
				//"scrollCollapse" : true,
				//"info" : false
			});//end datatable function
			
			$('#failedSO tbody').on('click', 'a', function() {
				var data = failedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				showDetails();
			});

			$('#failedSO tbody').on('click', '.reopen', function() {
				var data = failedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				$.ajax({url : "${callDC}",type : "POST",data : {SOId : soId},
					success : function(data) {
						//alert(data);
						if(data == "true"){
							$("#status")
							.html(
									"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Opened</div>");
							updateStatus("Opened");
						}else{
							$("#status")
							.html(
									"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed to Open</div>");
							updateStatus("Failed");
						}
					},
					error : function(error) {
						$("#status")
						.html(
								"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed to Open</div>");
						updateStatus("Failed");
					}
				});
			});
		}); //end document.ready

		$(document).ready(function() {
			rejectedSOTable = $('#rejectedSO').DataTable({
				"ajax" : {
					"url" : "${getSODetail}",
					"data" : {
						'status' : "Rejected"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "standingorderId",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				}, /* {
					"data" : "portalRequest.messageId"
				}, */ {
					"data" : "portalRequest.currentddpversion"
				}, {
					"data" : "portalRequest.requestGenerationTime"
				}, {
					"data" : "parentsoid"
				}, {
					"data" : "lritId"
				}, {
					"data" : ""
				} ],
				"columnDefs" : [ {
					"orderable" : false,
					"targets" : -1,
					"data" : null,
					"defaultContent" : "<button class=\"edit btn btn-primary\">Edit</button>"
				} ],
				"order" : [ [ 1, 'asc' ] ],
				"paging" : true//,
				//"scrollY" : "500px",
				//"scrollCollapse" : true,
				//"info" : false
			});//end datatable function
			
			$('#rejectedSO tbody').on('click', 'a', function() {
				var data = rejectedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				showDetails();
			});

			$('#rejectedSO tbody').on('click', '.edit', function() {
				var data = rejectedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				editSO();
			});
		}); //end document.ready

		$(document).ready(function() {
			closedSOTable = $('#closedSO').DataTable({
				"ajax" : {
					"url" : "${getSODetail}",
					"data" : {
						'status' : "Closed"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "standingorderId",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				}, /* {
					"data" : "portalRequest.messageId"
				}, */ {
					"data" : "portalRequest.currentddpversion"
				}, {
					"data" : "portalRequest.requestGenerationTime"
				}, {
					"data" : "parentsoid"
				}, {
					"data" : "lritId"
				} ],
				"order" : [ [ 1, 'asc' ] ],
				"paging" : true//,
				//"scrollY" : "500px",
				//"scrollCollapse" : true,
				//"info" : false
			});//end datatable function
			
			$('#closedSO tbody').on('click', 'a', function() {
				var data = closedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				showDetails();
			});
		}); //end document.ready

		$(document).ready(function() {
			deletedSOTable = $('#deletedSO').DataTable({
				"ajax" : {
					"url" : "${getSODetail}",
					"data" : {
						'status' : "Deleted"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "standingorderId",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				}, /* {
					"data" : "portalRequest.messageId"
				}, */ {
					"data" : "portalRequest.currentddpversion"
				}, {
					"data" : "portalRequest.requestGenerationTime"
				}, {
					"data" : "parentsoid"
				}, {
					"data" : "lritId"
				}, {
					"data" : ""
				} ],
				"columnDefs" : [ {
					"orderable" : false,
					"targets" : -1,
					"data" : null,
					"defaultContent" : "<button class=\"edit btn btn-primary\">Edit</button>"
				} ],
				"order" : [ [ 1, 'asc' ] ],
				"paging" : true//,
				//"scrollY" : "500px",
				//"scrollCollapse" : true,
				//"info" : false
			});//end datatable function
			
			$('#deletedSO tbody').on('click', 'a', function() {
				var data = deletedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				showDetails();
			});
			
			$('#deletedSO tbody').on('click', '.edit', function() {
				var data = deletedSOTable.row($(this).parents('tr')).data();
				soId = data["standingorderId"] + "";
				editSO();	
			});
		}); //end document.ready
		
		function operateSOFunc() {
			var operation = document.getElementById("SOSelect").value;
			
			if (operation == "allSO") {
				document.getElementById("divallSO").style.display = "block";
				allSOTable.ajax.reload();
			} else {
				document.getElementById("divallSO").style.display = "none";
			}
			
			if (operation == "createdSO") {
				document.getElementById("divcreatedSO").style.display = "block";
				createdSOTable.ajax.reload();
			} else {
				document.getElementById("divcreatedSO").style.display = "none";
			}

			if (operation == "approvedSO") {
				approvedSOTable.ajax.reload();
				document.getElementById("divapprovedSO").style.display = "block";
			} else {
				document.getElementById("divapprovedSO").style.display = "none";
			}

			if (operation == "openedSO") {
				openedSOTable.ajax.reload();
				document.getElementById("divopenedSO").style.display = "block";
			} else {
				document.getElementById("divopenedSO").style.display = "none";
			}

			if (operation == "rejectedSO") {
				rejectedSOTable.ajax.reload();
				document.getElementById("divrejectedSO").style.display = "block";
			} else {
				document.getElementById("divrejectedSO").style.display = "none";
			}

			if (operation == "failedSO") {
				failedSOTable.ajax.reload();
				document.getElementById("divfailedSO").style.display = "block";
			} else {
				document.getElementById("divfailedSO").style.display = "none";
			}

			if (operation == "closedSO") {
				closedSOTable.ajax.reload();
				document.getElementById("divclosedSO").style.display = "block";
			} else {
				document.getElementById("divclosedSO").style.display = "none";
			}
			
			if (operation == "deletedSO") {
				deletedSOTable.ajax.reload();
				document.getElementById("divdeletedSO").style.display = "block";
			} else {
				document.getElementById("divdeletedSO").style.display = "none";
			}
		}
		
		function editSO(){
			window.open("editStandingOrder?soId="+soId);
		}

		function showDetails() {	
			$('#geoArea').DataTable().clear();
			geoAreatable = $('#geoArea').DataTable({
				"ajax" : {
					"url" : "${getGeoDetail}",
					"data" : {
						'SOId' : soId
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "areacode",
					"render" : function(data, type, row, meta) {
						return '<a>' + data.toString() + '</a>';
					}
				} ],
				"destroy" : true,
				"searching" : false,
				"paging" : false,
				"scrollY" : "500px",
				"scrollCollapse" : true,
				"info" : false
			});//end datatable function
			
			document.getElementById("divexCoun").style.display = "none";
			document.getElementById("divexShip").style.display = "none";
			
			$('#geoArea tbody').on('click', 'a', function() {
				var data = geoAreatable.row($(this).parents('tr')).data();
				//alert(JSON.stringify(data));
				areacode = data["areacode"];
				showExcluded();
			});
			
			$("#SOModal").modal('toggle');
		}
		
		function showExcluded(){					
			$('#exCoun').DataTable({
				"ajax" : {
					"url" : "${getExCountry}",
					"data" : {
						'SOId' : soId,
						'areacode' : areacode
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "excludedCountries"
				} ],
				"destroy" : true,
				"searching" : false,
				"paging" : false,
				"scrollY" : "500px",
				"scrollCollapse" : true,
				"info" : false
			});//end datatable function
			
			$('#exShip').DataTable({
				"ajax" : {
					"url" : "${getExShip}",
					"data" : {
						'SOId' : soId,
						'areacode' : areacode
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : "vesselType"
				} ],
				"destroy" : true,
				"searching" : false,
				"paging" : false,
				"scrollY" : "500px",
				"scrollCollapse" : true,
				"info" : false
			});//end datatable function		
			
			document.getElementById("divexCoun").style.display = "block";
			document.getElementById("divexShip").style.display = "block";
		}
		
		function updateStatus(Status) {
			$.ajax({
				url : "${updateStatus}",
				type : "POST",
				data : {
					SOId : soId,
					Status : Status
				},
				success : function(data) {
					createdSOTable.ajax.reload();
					approvedSOTable.ajax.reload();
					openedSOTable.ajax.reload();
					rejectedSOTable.ajax.reload();
					failedSOTable.ajax.reload();
					closedSOTable.ajax.reload();
				},
				error : function(error) {
					$("#status")
					.html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Standing Order " +soId+ " Failed to Update</div>");
				}
			});
		}
	</script>

</body>
</html>