<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>New Standing Order</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->

<spring:url value="/getSOCountries" var="getSOCountries" />
<spring:url value="/getSOPolygon" var="getSOPolygon" />
<spring:url value="/persistSORequest" var="persistSORequest" />


<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<%-- <spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url> --%>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<!-- <script src="/lrit/js/button1.js"></script> -->
<!-- <script src="/lrit/js/validateform.js"></script> -->
<script src="/lrit/js/viewvessel.js"></script>

<style>
.table_wrapper {
	display: block;
	overflow-x: auto;
	white-space: nowrap;
}
</style>
</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">
						<div id="status"></div>
						<!-- Horizontal Form -->
						<!-- <div class="box box-info"> -->
						<div class="box-header with-border">
							<h3 class="box-title">New Standing Order</h3>
						</div>
						<!-- form start -->
						<!-- /.box-header -->

						<div class="box-body">
							<!-- Custom form -->
							<div class="form-group col-sm-12">
								<div class="col-sm-4">
									<label class="form-control">Select Area Type</label> <select
										class="form-control" id="areaSelect"
										onchange="operateAreaFunc()">
										<option value="IW">Internal Waters</option>
										<option value="TS">Territorial Sea</option>
										<option value="CC">Custom Coastal</option>
										<option value="CS">Coastal Sea</option>
									</select>
									<div id="divIW">
										<table id="InternalWaters"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th colspan="2">InternalWaters</th>
												</tr>
												<tr>
													<th style="width: 20%">Select</th>
													<th>Area</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divTS" style="display: none">
										<table id="TerritorialSea"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th colspan="2">TerritorialSea</th>
												</tr>
												<tr>
													<th style="width: 20%">Select</th>
													<th>Area</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divCC" style="display: none">
										<table id="CustomCoastal"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th colspan="2">CustomCoastal</th>
												</tr>
												<tr>
													<th style="width: 20%">Select</th>
													<th>Area</th>
												</tr>
											</thead>
										</table>
									</div>

									<div id="divCS" style="display: none">
										<table id="CoastalSea"
											class="table table-bordered table-striped">
											<thead>
												<tr>
													<th colspan="2">CoastalSea</th>
												</tr>
												<tr>
													<th style="width: 20%">Select</th>
													<th>Area</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
								<div class="col-sm-4">
									<label class="form-control">Exclude Countries</label>
									<table id="excludeCountries"
										class="table table-bordered table-striped">
										<thead>
											<tr>
												<th style="width: 20%">Select</th>
												<th>Country Name</th>
												<th>Lrit ID</th>
											</tr>
										</thead>
									</table>
								</div>
								<div class="col-sm-4">
									<label class="form-control">Exclude Vessel Type</label>
									<table id="excludeShipType"
										class="table table-bordered table-striped">
										<thead>
											<tr>
												<th style="width: 20%">Select</th>
												<th>Vessel Type</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td style="width: 20%"></td>
												<td>Passenger 0100</td>
											</tr>
											<tr>
												<td style="width: 20%"></td>
												<td>Cargo 0200</td>
											</tr>
											<tr>
												<td style="width: 20%"></td>
												<td>Tanker 0300</td>
											</tr>
											<tr>
												<td style="width: 20%"></td>
												<td>Mobile off Shore 0400</td>
											</tr>
											<tr>
												<td style="width: 20%"></td>
												<td>Others 9900</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.box -->

						<div class="box-footer">
							<button type="button" class="btn btn-primary col-sm-2" id="addSO"
								onclick="saveData()">Add SO</button>
							<button type="button" class="btn btn-primary col-sm-2" id="submitSO"
								style="margin-left:16px" onclick="submitData()">Submit</button>
						</div>

						<!-- Custom form ?End-->
					</div>
					<!--/.col (left) -->

					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<script type="text/javascript">
		var excludeShipTypeTable, InternalWatersTable, TerritorialSeaTable, CustomCoastalTable, CoastalSeaTable, excludeCountriesTable, combine = [];

		$(document).ready(function() {
			populateTables();
		}); //end document.ready

		function populateTables() {

			excludeShipTypeTable = $('#excludeShipType').DataTable({
				"columns" : [ {
					"data" : ""
				}, {
					"data" : "shipType"
				} ],
				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'so',
					"selector" : 'td:first-child'
				},
				"searching" : false,
				"order" : [ [ 1, 'asc' ] ],
				"paging" : false,
				"info" : false
			});

			InternalWatersTable = $('#InternalWaters').DataTable({
				"ajax" : {
					"url" : "${getSOPolygon}",
					"data" : {
						'areaType' : "InternalWaters"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : ""
				}, {
					"data" : "areaid"
				} ],

				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'so',
					"selector" : 'td:first-child'
				},
				"searching" : false,
				"order" : [ [ 1, 'asc' ] ],
				"paging" : false,
				"scrollY" : "525px",
				"scrollCollapse" : true,
				"info" : false
			});//end datatable function

			TerritorialSeaTable = $('#TerritorialSea').DataTable({
				"ajax" : {
					"url" : "${getSOPolygon}",
					"data" : {
						'areaType' : "TerritorialSea"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : ""
				}, {
					"data" : "areaid"
				} ],

				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'so',
					"selector" : 'td:first-child'
				},
				"searching" : false,
				"order" : [ [ 1, 'asc' ] ],
				"paging" : false,
				"scrollY" : "525px",
				"scrollCollapse" : true,
				"info" : false
			});//end datatable function

			CustomCoastalTable = $('#CustomCoastal').DataTable({
				"ajax" : {
					"url" : "${getSOPolygon}",
					"data" : {
						'areaType' : "CustomCoastal"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : ""
				}, {
					"data" : "areaid"
				} ],

				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'so',
					"selector" : 'td:first-child'
				},
				"searching" : false,
				"order" : [ [ 1, 'asc' ] ],
				"paging" : false,
				"scrollY" : "525px",
				"scrollCollapse" : true,
				"info" : false
			});//end datatable function

			CoastalSeaTable = $('#CoastalSea').DataTable({
				"ajax" : {
					"url" : "${getSOPolygon}",
					"data" : {
						'areaType' : "CoastalSea"
					},
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : ""
				}, {
					"data" : "areaid"
				} ],

				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'so',
					"selector" : 'td:first-child'
				},
				"searching" : false,
				"order" : [ [ 1, 'asc' ] ],
				"paging" : false,
				"scrollY" : "525px",
				"scrollCollapse" : true,
				"info" : false
			});//end datatable function

			excludeCountriesTable = $('#excludeCountries').DataTable({
				"ajax" : {
					"url" : "${getSOCountries}",
					"dataSrc" : ""
				},
				"columns" : [ {
					"data" : ""
				}, {
					"data" : "cgName"
				}, {
					"data" : "id.cgLritid"
				} ],
				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'multi',
					"selector" : 'td:first-child'
				},
				"order" : [ [ 1, 'asc' ] ],
				"paging" : false,
				"scrollY" : "550px",
				"scrollCollapse" : true,
				"info" : false
			});//end datatable function
		}

		function saveData() {

			if (InternalWatersTable.rows('.selected').count()
					|| TerritorialSeaTable.rows('.selected').count()
					|| CustomCoastalTable.rows('.selected').count()
					|| CoastalSeaTable.rows('.selected').count()) {
				
				var str = "[area: ", displayStr = "Area: ";
				$.each(InternalWatersTable.rows('.selected').data(),
						function() {
							str = str + this["areaid"] + ",";
							displayStr = displayStr + this["areaid"] + ", ";
						});
				$.each(TerritorialSeaTable.rows('.selected').data(),
						function() {
							str = str + this["areaid"] + ",";
							displayStr = displayStr + this["areaid"] + ", ";
						});
				$.each(CustomCoastalTable.rows('.selected').data(), function() {
					str = str + this["areaid"] + ",";
					displayStr = displayStr + this["areaid"] + ", ";
				});
				$.each(CoastalSeaTable.rows('.selected').data(), function() {
					str = str + this["areaid"] + ",";
					displayStr = displayStr + this["areaid"] + ", ";
				});

				str = str.substring(0, str.length - 1) + "ship: ";
				displayStr = displayStr.substring(0, displayStr.length - 2);
				$.each(excludeShipTypeTable.rows('.selected').data(),
						function() {
							str = str + this["shipType"] + ",";
						});

				if (excludeShipTypeTable.rows('.selected').count()) {
					str = str.substring(0, str.length - 1) + "country: ";
				} else {
					str = str + "country: ";
				}
				$.each(excludeCountriesTable.rows('.selected').data(),
						function() {
							str = str + this["id"]["cgLritid"] + ",";
						});

				if (excludeCountriesTable.rows('.selected').count()) {
					str = str.substring(0, str.length - 1) + "]";
				} else {
					str = str + "]";
				}
				InternalWatersTable.rows().deselect();
				TerritorialSeaTable.rows().deselect();
				CustomCoastalTable.rows().deselect();
				CoastalSeaTable.rows().deselect();
				excludeCountriesTable.rows().deselect();
				excludeShipTypeTable.rows().deselect();
				combine.push(str);
				$("#status")
						.html(
								"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><b>" + displayStr + "</b> Added </div>");
			} else {
				$("#status")
						.html(
								"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Select min 1 Area to create Standing Order</div>");
			}
		}

		function submitData() {
			if (combine.length == 0) {
				$("#status")
						.html(
								"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Add Min 1 Area to create Standing Order</div>");
			} else {
				$.ajax({
					url : "${persistSORequest}",
					type : "POST",
					data : {
						combine : combine,
						pSOId : ""
					},

					success : function(data) {
						$("#status").html(data);
					},
					error : function(error) {
						$("#status").html(data);
					}
				});
			}
		}

		function operateAreaFunc() {
			var operation = document.getElementById("areaSelect").value;

			if (operation == "IW") {
				document.getElementById("divIW").style.display = "block";
			} else {
				document.getElementById("divIW").style.display = "none";
			}

			if (operation == "TS") {
				document.getElementById("divTS").style.display = "block";
			} else {
				document.getElementById("divTS").style.display = "none";
			}

			if (operation == "CC") {
				document.getElementById("divCC").style.display = "block";
			} else {
				document.getElementById("divCC").style.display = "none";
			}

			if (operation == "CS") {
				document.getElementById("divCS").style.display = "block";
			} else {
				document.getElementById("divCS").style.display = "none";
			}
		}
	</script>
</body>
</html>