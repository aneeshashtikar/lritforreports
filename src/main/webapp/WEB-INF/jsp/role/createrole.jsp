<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Register Role Form</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->

<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- Theme style -->


<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->

<%-- <spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}"> --%>

<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">



<spring:url value="/roles/persistrole" var="persistrole" />
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<%-- <spring:url value="../css/multiselect.css" var="multiselectcss" />

<link rel="stylesheet" href="${multiselectcss}">

<script src="../js/multiselect.min.js"></script> --%>
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<!-- Select2 -->
<spring:url value="/bower_components/select2/dist/css/select2.min.css"
	var="dropdowncss" />
<link rel="stylesheet" href="${dropdowncss}">
<!-- Select2 -->
<spring:url
	value="/bower_components/select2/dist/js/select2.full.min.js"
	var="dropdownjss" />
<script src="${dropdownjss}"></script>


<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
<script src="${adminlteminjs}"></script>

<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">



<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-lg-10">
						<div class="message"></div>
						<!-- general form elements -->
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>

						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Register Role</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->

							<div class="box-body">
								<div class="form-group">
									<form:form id="roleregistration" action="${persistrole}"
										method="post" name="roleregistration"
										modelAttribute="portalrole" onsubmit="return Validation()">
										<div class="box-body">
											<div class="col-sm-6">
												<div class="row">
													<div class="form-group col-sm-10">
														<label class="col-sm-4 ">Role Name</label>
														<div class="col-sm-8">
															<form:input path="rolename" type="text"
																class="form-control" id="rolename"
																placeholder="Role Name" autocomplete="off"
																pattern="[A-Za-z ]{1,50}" title="Enter alphabets "
																required="true" />
														</div>
													</div>
												</div>
												<div class="row">
													<div class="form-group col-sm-10">
														<label class="col-sm-4 ">Short Name</label>
														<div class="col-sm-8">
															<form:input path="shortname" type="text"
																class="form-control" id="shortname"
																placeholder="Short Name" autocomplete="off"
																pattern="[A-Za-z ]{1,50}" title="Enter alphabets "
																required="true" />
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="row">
													<div class="form-group col-sm-10">
														<label for="activities" class="col-sm-4">Assign
															Activities</label>
														<div>
															<form:select class="select2 dropdown-text col-sm-8"
																id="activities" path="activities" multiple="true">
										 <c:forEach items="${activities}" var="activity">
        									<form:option value="${activity.activityId}">${activity.activityname}</form:option>
    										</c:forEach>

															</form:select>



														</div>
													</div>
												</div>

											</div>

										</div>

										<!-- /.box-body -->

										<div class="box-footer">
											<button type="submit" class="btn btn-primary pull-left">Submit</button>
										</div>
									</form:form>
									<!-- /.form -->
								</div>
							</div>
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->

					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>

		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<script>
		$('.select2').select2();
		function Validation() {

			var activities = document.getElementById("activities").value;

			if (activities == '') {

				$(".message")
						.append(
								"<div class='alert alert-danger colose'><a href='#' class='close' data-dismiss='alert'"
		+"	aria-label='close'>&times;</a>"
										+ "Select activities" + " </div>")
				return false;
			}
			return true;
		}
	</script>
</body>
</html>