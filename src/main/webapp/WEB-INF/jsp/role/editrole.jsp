<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="">
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Edit Role</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Title Page</title>
<!-- Bootstrap CSS -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">

<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">


<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="icon" href="../resources/img/dgslogo.ico">
<!-- jQuery -->
<!-- <script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->
<!-- Bootstrap JavaScript -->
<!-- <script
		src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script> -->
<!-- <script src="../js/jquery.multi-select.js"></script> -->

<%-- <spring:url value="../css/multiselect.css" var="multiselectcss" />

<link rel="stylesheet" href="${multiselectcss}">

<script src="../js/multiselect.min.js"></script> --%>
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- Select2 -->
<spring:url value="/bower_components/select2/dist/css/select2.min.css"
	var="dropdowncss" />
<link rel="stylesheet" href="${dropdowncss}">
<!-- Select2 -->
<spring:url
	value="/bower_components/select2/dist/js/select2.full.min.js"
	var="dropdownjss" />
<script src="${dropdownjss}"></script>
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">

<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
<script src="${adminlteminjs}"></script>

<spring:url value="/roles/saveeditrole" var="editrole" />
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>


		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">
						<div class="message"></div>
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Role Form</h3>
							</div>

							<div>
								<form:form id="roleregistration" action="${editrole}"
									method="post" name="roleregistration"
									modelAttribute="portalrole" onsubmit="return Validation()">
									<div class="box-body">
										<form:input path="roleId" type="hidden" class="form-control"
											id="roleId" />
										<div class="col-sm-6">
											<div class="row">
												<div class="form-group col-sm-10">
													<label class="col-sm-4 ">Role Name</label>
													<div class="col-sm-8">
														<form:input path="rolename" type="text"
															class="form-control" id="rolename"
															placeholder="Role Name" readonly="true" />
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-10">
													<label class="col-sm-4 ">Short Name</label>
													<div class="col-sm-8">
														<form:input path="shortname" type="text"
															class="form-control" id="shortname"
															placeholder="Short Name" readonly="true" />
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="row">
												<div class="form-group col-sm-10">
													<label class="col-sm-4 ">Add Activities</label>
													<div class="">
														<%-- 	<form:select class="select2 col-sm-8" id="activities"
															path="activities" multiple="true">
										 <c:forEach items="${activities}" var="activity">
        									<form:option value="${activity.activityId}">${activity.activityname}</form:option>
    										</c:forEach>

														</form:select> --%>


														<%-- 	<form:select class="form-control select2" id="roles"
																				path="roles" multiple="true"
																				data-placeholder="Select a State">
																				<c:forEach items="${portalRoles}" var="role">

																					<div>
																						<c:choose>
																							<c:when test="${role.flag eq true}">
																								<form:option value="${role.roleId}"
																									selected="true">${role.rolename}</form:option>
																							</c:when>
																							<c:otherwise>
																								<form:option value="${role.roleId}">${role.rolename}</form:option>
																							</c:otherwise>
																						</c:choose>
																					</div>	            
																			
																	</c:forEach>
															
															         
															</form:select> --%>

														<form:select class="select2 col-sm-8" id="activities"
															path="activities" multiple="true">
										 <c:forEach items="${activities}" var="activity">
																<div>
																	<c:choose>
																		<c:when test="${activity.flag eq true}">
        									<form:option value="${activity.activityId}"
																				selected="true">${activity.activityname}</form:option>
																		</c:when>
																		<c:otherwise>
																			<form:option value="${activity.activityId}">${activity.activityname}</form:option>
																		</c:otherwise>
																	</c:choose>
																</div>	            										
</c:forEach>

														</form:select>
													</div>
												</div>

											</div>
										</div>
									</div>
									<!-- /.box-body -->

									<div class="box-footer">
										<button type="submit" class="btn btn-primary pull-left">Submit</button>
									</div>
								</form:form>
							</div>
							<!-- right column -->
							<!--/.col (right) -->
						</div>
						<!-- /.col-lg-10 -->
					</div>
					<!-- /.col-lg-10 -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
					
				</div>
				<!-- row -->
			</section>
			<!-- /.content -->
		</div>
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<!-- Control Sidebar -->
	<jsp:include page="../rightbar.jsp"></jsp:include>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>

	<!-- ./wrapper -->




	<!-- ends -->

	<script>
		/* document.multiselect('#activities'); */
		$('.select2').select2();
		function Validation() {

			var activities = document.getElementById("activities").value;

			if (activities == '') {

				$(".message")
						.append(
								"<div class='alert alert-danger colose'><a href='#' class='close' data-dismiss='alert'"
		+"	aria-label='close'>&times;</a>"
										+ "Select activities" + " </div>")
				return false;
			}
			return true;
		}
	</script>
</body>

</html>
