<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html lang="">
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>View Role Form</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Title Page</title>
<!-- Bootstrap CSS -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">

<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="icon" href="../resources/img/dgslogo.ico">
<!-- jQuery -->

<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>


<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
<script src="${adminlteminjs}"></script>

<spring:url value="/roles/editrole" var="editrole" />
<spring:url value="/roles/viewrole" var="viewrole" />
<script>
$(document)
.ready(
		function() {
			
	$('#back').click(function() {
		//do something

		var url = "${viewrole}";
		//	alert("url "+url);
		window.location = url;
	});
		});
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>


		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">View Role Details</h3>
							</div>

							<div>
								<form:form id="viewroledetails" modelAttribute="portalrole"
									method="GET" action="${editrole}">
									<!-- <div class="panel with-nav-tabs panel-info">

										 -->
									<div class="box-body">
										<div class="panel-body">
											<table class="table table-striped table-bordered table-hover"
												id="viewroledetail-table">
												<form:input path="roleId" type="hidden" />
												<tr>
													<th>Role Name</th>
													<td>${portalrole.rolename}</td>
												</tr>
												<tr>
													<th>Short Name</th>
													<td>${portalrole.shortname}</td>
												</tr>
												<tr>
													<th>Activities</th>
													<td><select id="activities">  
															<c:forEach items="${activities}" var="activity">
        									<option value="${activity.activityId}">${activity.activityname}</option>
    										</c:forEach>

													</select></td>
												</tr>

											</table>
										</div>
										<!-- </div> -->
									</div>

									<div class="box-footer">
									<sec:authorize access="hasAuthority('editrole')">
										<c:if test="${portalrole.status == 'active'}">
											<button type="submit" class="btn btn-primary pull-right">Edit</button>
										</c:if>
										</sec:authorize>
										<button id="back" type="button"
											class="btn btn-primary pull-left">Back</button>
									</div>

								</form:form>
							</div>
							<!-- right column -->
							<!--/.col (right) -->
						</div>
						<!-- /.col-lg-10 -->
					</div>
					<!-- /.col-lg-10 -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
					
				</div>
				<!-- row -->
			</section>
			<!-- /.content -->
		</div>
	</div>
	<!-- /.content-wrapper -->





	<jsp:include page="../footer.jsp"></jsp:include>

	<!-- Control Sidebar -->
	<jsp:include page="../rightbar.jsp"></jsp:include>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>

	<!-- ./wrapper -->




	<!-- ends -->
	<!-- 
	<script>
		document.multiselect('#activities');
	</script> -->
</body>

</html>
