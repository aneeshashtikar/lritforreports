<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>View Role</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>

<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>


<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>

<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">


<spring:url value="/roles/getrolelist" var="getrolelist" />
<spring:url value="/roles/viewroledetails" var="viewrole"/>

<spring:url value="/roles/deregisterrole" var="deregisterrole" />
<spring:url value="/roles/editrole" var="editrole" />
<style>
label.col-sm-4{
padding-left :0px;
}


div.col-sm-5{
padding-left :0px;
}</style>
<script type="text/javascript">
	var resultSet = function() {
		this.item = {};
	};

	var listtable = new resultSet();

	var targetTableId = 'viewrole-table';

	var dTable = function(targetTableId, resultSet,status) {
		//alert("status "+status);
		/* if ($.fn.dataTable.isDataTable(targetTableId)) {
			resultSet.item.ajax.reload();
		} else { */
			
			/* resultSet.item = */ 
			 resultSet.item =$('#viewrole-table').DataTable(
					{

						"ajax" : {
							/* "dataType" : 'json', */
							"url" : "${getrolelist}",
							"type" : "GET",
							"data" : {
								'Status' : status
							},
							"dataSrc" : ""

						},
						"responsive" : {
							"details" : {
								"type" : 'column',
								"target" : 'tr'
							}
						},
						"columnDefs" : [ {
							"targets" : 0,
							"title" : "Role Id",
							"targets" : "Role_Id",
							"render" : function(data, type, row, meta) {

								return '<a href="${viewrole}?roleId=' + data
										+ '">' + data + '</a>';
							}
						}/* ,
						{
							"targets" : 4,
							 "visible": false
							
						} */]

					});
			
			if(status=="active"){
				//alert("active ");
				resultSet.item.columns( '#edit' ).visible( true );
				resultSet.item.columns( '#delete' ).visible( true );
			}else {
			//	alert("activedddddddddddddd ");
				resultSet.item.columns( '#edit' ).visible( false );
				resultSet.item.columns( '#delete' ).visible( false );
				
			}
			

			
			
	
	}

	var urlmethod = function(roleId, url, listtable, targetTableId) {

		$
				.ajax({
					url : url,
					type : 'POST',
					data : {
						roleId : roleId
					},
					dataType : "json",
					success : function(data) {

						if (data == "Role has been Inactivated") {
							$(".message")
									.append(
											"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'"
							+"	aria-label='close'>&times;</a>"
													+ data + " </div>")
						} else {
							$(".message")
									.append(
											"<div class='alert alert-danger colose'><a href='#' class='close' data-dismiss='alert'"
							+"	aria-label='close'>&times;</a>"
													+ data + " </div>")
						}
						/* dTable('#' + targetTableId, listtable); */
						$('#viewrole-table').DataTable().clear().destroy();
						dTable('#' + targetTableId, listtable,"active");
					},
					error : function(error) {
						alert("Error AJAX not working: " + error);
					}
				});

	}

	$(document)
			.ready(
					function() {
						
					

						dTable('#' + targetTableId, listtable,"active");
<%--Zoom out handle for delete button--%>
	$('body')
								.on(
										'click',
										'#viewrole-table tbody td>button[name=btndelete]',
										function() {

											var roleId = listtable.item.row(
													$(this).parents('tr'))
													.data().roleId;
											var url = "${deregisterrole}";
											if (confirm(" Are You Sure ? ")) {
												urlmethod(roleId, url,
														listtable,
														targetTableId);
											}
											/* var url = "${deregisterrole}?roleId="+roleId;
											if(confirm(" Are You Sure ? ")) {
												window.location=url;
											} */
										});
<%--Zoom in handle for delete button--%>
	$('body')
								.on(
										'click',
										'#viewrole-table.collapsed tbody li span>button[name=btndelete]',
										function() {
											var cellIndex = listtable.item.responsive
													.index($(this).parent()
															.parent());
											var roleId = listtable.item.data()[cellIndex.row].roleId;
											var url = "${deregisterrole}";
											if (confirm(" Are You Sure ? ")) {
												urlmethod(roleId, url,
														listtable,
														targetTableId);
											}
											/* var url = "${deregisterrole}?roleId="+roleId;
											if(confirm(" Are You Sure ? ")) {
												window.location=url;
											} */
										});
<%--Zoom out handle for delete button--%>
	$('body')
								.on(
										'click',
										'#viewrole-table tbody td>button[name=btnedit]',
										function() {

											var roleId = listtable.item.row(
													$(this).parents('tr'))
													.data().roleId;

											var url = "${editrole}?roleId="
													+ roleId;
											if (confirm(" Are You Sure ? ")) {
												window.location = url;
											}
										});
<%--Zoom in handle for delete button--%>
	$('body')
								.on(
										'click',
										'#viewrole-table.collapsed tbody li span>button[name=btnedit]',
										function() {

											var cellIndex = listtable.item.responsive
													.index($(this).parent()
															.parent());
											var roleId = listtable.item.data()[cellIndex.row].roleId;

											var url = "${editrole}?roleId="
													+ roleId;
											if (confirm(" Are You Sure ? ")) {
												window.location = url;
											}
										});

					});
	function onchangestatus(){
	
		$('#viewrole-table').DataTable().clear().destroy();
	//	alert("Inside onchange");
		var status=document.getElementById("status").value;
		//alert("status "+status);
		dTable('#' + targetTableId, listtable,status);
		
	}
</script>

</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">

				<div class="row">
					<div class="col-md-10">
						<div class="message"></div>
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">List of Roles</h3>
							</div>

						</div>
						<div class="box">
							<!-- Report Table -->
							<div class="box-body">
							
											<div class="form-group col-sm-5">
									<label for="status" class="col-sm-4 "> Status of Role</label>
									<div class="col-sm-8">
										<select  class="form-control"
											name="status" id="status" onchange="onchangestatus();">
											<option value="all" >All</option>
											<option value="active" selected="selected">Active</option>
											<option value="inactive">Inactive</option>
										
										</select>

</div>
</div>
								<table id="viewrole-table"
									class="table table-bordered table-striped " style="width: 100%">
									<thead>
										<tr>

											<th class="Role_Id" data-data="roleId"
												data-default-content="NA">Role Id</th>
											<th data-data="rolename" data-default-content="NA">Role
												Name</th>
											<th data-data="shortname" data-default-content="NA">Short
												Code</th>
											<th data-data="status" data-default-content="NA">Status
											</th>
											<sec:authorize access="hasAuthority('editrole')">
											<th data-data="null" id="edit"
												data-default-content="<button name='btnedit'  class='btn btn-primary'> Edit</button>"></th>
												</sec:authorize>
												<sec:authorize access="hasAuthority('deleterole')">
											<th data-data="null" id="delete"
												data-default-content="<button name='btndelete'  class='btn btn-primary'> Inactive</button>"></th>
										</sec:authorize>
										</tr>
									</thead>
								</table>
								
							</div>
							<!-- /.box-body -->
						</div>
					</div>

<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>

	<!-- Control Sidebar -->
	<jsp:include page="../rightbar.jsp"></jsp:include>
	<!-- /.control-sidebar -->

	<div class="control-sidebar-bg"></div>
	<!-- </div> -->
	<!-- ./wrapper -->



</body>
</html>