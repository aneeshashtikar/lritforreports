<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Create Ship Route</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">

<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">


<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">

<%-- <!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}"> --%>

<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">

<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js" var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<!-- jQuery UI 1.11.4 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js" var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>

<!-- added -->
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<%-- <spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script> --%>

<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>

<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
 --%>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<spring:url value="/route/saveroute" var="saveroute" />
<spring:url value="/requests/vessellist" var="getshipdetails" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<!-- Select2 -->
<spring:url
	value="https://adminlte.io/themes/AdminLTE/bower_components/select2/dist/js/select2.full.min.js"
	var="dropdownjss" />
<script src="${dropdownjss}"></script>
<spring:url
	value="https://adminlte.io/themes/AdminLTE/bower_components/select2/dist/css/select2.min.css"
	var="dropdowncss" />
<link rel="stylesheet" href="${dropdowncss}">


<spring:url value="/route/saveroute" var="saveroute" />
<spring:url value="/requests/getshipdetails" var="getshipdetails" />

<style>
.select2-container--default .select2-selection--multiple .select2-selection__choice
	{
	background-color: #337ab7;
}

body .datepicker {
	z-index: 10000 !important;
}
</style>

<script type="text/javascript">
	$(function() {
		//Initialize Select2 Elements
		$('.select2').select2();

		
		$('#startdate').datepicker({
			autoclose : true, 
			format : 'yyyy-mm-dd'
		});

		$('#enddate').datepicker({
			format : 'yyyy-mm-dd',
 			autoclose : true
	});
	})

	function showTable() {
		var table = $('#Shipdetail').DataTable({
			//dom: 'lBrtip',
			dom : 'lfrtip',
			"bDestroy" : true,
			"ajax" : {
				"url" : "${getshipdetails}",
				"type" : "GET",
				"dataSrc" : ""

			},

			"columnDefs" : [ {
				"orderable" : false,
				"className" : 'select-checkbox',
				"targets" : 0,
				"defaultContent" : ""
			} ],
			"select" : {
				"style" : 'os',
				"selector" : 'td:first-child'
			},
			"order" : [ [ 1, 'asc' ] ],

			"columns" : [ {
				"data" : ""
			}, {
				"data" : "imoNo"
			}, {
				"data" : "vesselName"
			}, {
				"data" : "mmsiNo"
			} ]

		});
		$('#select').on('click', function() {

			var imo, mmsi, shipname;
			$.each(table.rows('.selected').data(), function() {
				imo = this["imoNo"];
				mmsi = this["mmsiNo"];
				ship = this["vesselName"];
				$('#imono').val(imo);
				$('#mmsino').val(mmsi);
				$('#shipname').val(ship);
			});
		});
	}
</script>
</head>

<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">

							<div class="box-header with-border">
								<h3 class="box-title">Ship Route Detail</h3>
							</div>

							<!-- /.box-header -->
							<!-- form start -->
							<form:form id="createroute" action="${saveroute}" method="post"
								name="createroute" modelAttribute="route">
								<div class="box-body">

									<div class="row">
										<div class="form-group col-sm-6">
											<label for="imono" class="col-sm-4">IMO NO</label>
											<div class="col-sm-8">
												<form:input type="text" class="form-control" id="imono"
													name="imono" placeholder="" required="required"
													path="imoNo" />
												<button type="submit" data-toggle="modal"
													data-target="#myModal" onclick="showTable();">
													<i class=" fa fa-search"></i>
												</button>
											</div>

											<!-- Modal -->
											<div class="modal fade" id="myModal" role="dialog">
												<div class="modal-dialog" style="width: 95%">

													<div class="modal-content">
														<div class="modal-header">
															<h4>Ship Details</h4>
														</div>
														<div class="modal-body">
															<div class="box-body">
																<table id="Shipdetail"
																	class="table table-bordered table-striped "
																	style="width: 100%">
																	<thead>
																		<tr>
																			<th></th>
																			<th data-data="imoNo" data-default-content="NA">IMO
																				Number</th>
																			<th data-data="vesselName" data-default-content="NA">Ship
																				Name</th>
																			<th data-data="mmsiNo" data-default-content="NA">MMSI
																				Number</th>
																		</tr>
																	</thead>
																</table>
															</div>

														</div>
														<div class="modal-footer">
															<button id="select" type="button" class="btn btn-default"
																data-dismiss="modal">Select</button>
															<button type="button" class="btn btn-default"
																data-dismiss="modal">Close</button>
														</div>
													</div>
												</div>
											</div>


										</div>
										<div class="form-group col-sm-6">
											<label for="mmsino" class="col-sm-4"> MMSI NO</label>
											<div class="col-sm-8">
												<form:input type="text" class="form-control" id="mmsino"
													name="mmsino" required="required" path="" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">
											<label for="shipname" class="col-sm-4">Ship Name</label>
											<div class="col-sm-8">
												<form:input type="text" class="form-control" id="shipname"
													name="shipname" placeholder="" required="required" path="" />
											</div>
										</div>
										<div class="form-group col-sm-6">
											<label for="routedescription" class="col-sm-4">Description</label>
											<div class="col-sm-8">
												<form:input type="text" class="form-control"
													id="routedescription" name="routedescription"
													required="true" path="routeDescription" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-sm-6">
											<label for="startdate" class="col-sm-4">Start Date</label>
											<div class="col-sm-8">
												<div class="input-group date">
													<div class="input-group-addon ">
														<i class="fa fa-calendar datepicker"></i>
													</div>
													<form:input type="text" class="form-control pull-right"
														id="startdate" name="startdate" autocomplete="off" path="startDateString" />
												</div>
											</div>
										</div>
										<div class="form-group col-sm-6">
											<label for="end" class="col-sm-4">End Date</label>
											<div class="col-sm-8">
												<div class="input-group date">
													<div class="input-group-addon ">
														<i class="fa fa-calendar datepicker"></i>
													</div>
													<form:input type="text" class="form-control pull-right"
														id="enddate" name="enddate" autocomplete="off" path="endDateString" />
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<form:input type="hidden" path="routeStatus" id="routestatus"
											value="Open" />
										<div class="form-group col-sm-6">
											<label for="countrylist" class="col-sm-4">Country</label>
											<div class="col-sm-8">
												<form:select class="form-control select2"
													multiple="multiple" data-placeholder="Select a country"
													id="countrylist" path="countryList">
													<c:forEach items="${countrylist}" var="country">
														<form:option value="${country.cgName}">${country.cgName}</form:option>
													</c:forEach>
												</form:select>
											</div>
										</div>
									</div>
								</div>
								<div class="box-footer">
									<input type="submit" class="btn btn-info pull-right" value="Submit">
								</div>


							</form:form>

							<!-- /.form -->
						</div>

					</div>
	`					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>

	<div class="control-sidebar-bg"></div>
	<!-- </div> -->
	<!-- ./wrapper -->

</body>


</html>
