<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Edit User Form</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->

<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
<script src="${adminlteminjs}"></script>

<!-- Select2 -->
<spring:url value="/bower_components/select2/dist/css/select2.min.css"
	var="dropdowncss" />
<link rel="stylesheet" href="${dropdowncss}">
<!-- Select2 -->
<spring:url
	value="/bower_components/select2/dist/js/select2.full.min.js"
	var="dropdownjss" />
<script src="${dropdownjss}"></script>

<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">

<spring:url value="/users/persistuser" var="persistuser" />
<spring:url value="/users/persistedituser" var="edituser" />
<spring:url value="/users/checkcsoidmapping" var="checkcsoidmapping" />
<spring:url value="/users/checkdpamapping" var="checkdpamapping" />

<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<div class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-lg-10">
						<!-- general form elements -->


						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Edit User</h3>
							</div>
							<!-- form start-->
							<div class="box-body">
								<div class="form-group">
									<div class="error " id="formerror"></div>
									<!--  onsubmit="return validatePortalUser()" -->
									<form:form class="form-horizontal" action="${edituser}"
										onsubmit="return validatePortalUser(this)" method="post"
										id="editshipingcompanyuserform" name="edituser"
										enctype="multipart/form-data" modelAttribute="portaluserdto">
										<div class="box-body">

											<!-- Custom form -->
											<div class="nav-tabs-custom" id="tabs">

												<ul class="nav nav-tabs">
													<li class="active" id="userdetailstab"><a
														href="#userdetails_Tab" data-toggle="tab">User Details</a></li>
													<li id="addressdetailstab"><a
														href="#addressdetails_Tab" data-toggle="tab">Address
															Details</a></li>
													<li id="csodetailstab"><a href="#csodetails_Tab"
														data-toggle="tab">CSO Details</a></li>
													<li id="dpadetailstab"><a href="#dpadetails_Tab"
														data-toggle="tab">DPA Details</a></li>


												</ul>
												<div class="tab-content">
													<!-- First Tab -->
													<div class="tab-pane active" id="userdetails_Tab">
														<!-- FIRST ROW -->
														<div class="row">
															<div class="col-sm-6">
																<div class="row">
																	<div class="form-group required col-sm-10 ">
																		<label for="portaluser.loginid" class="col-sm-4 ">
																			Login Id</label>
																		<div class="col-sm-8">
																			<form:input path="portaluser.loginId" type="text"
																				class="form-control" id="loginid" name="loginId"
																				readonly="true" />
																			<div class="error " id="loginIderror"></div>
																		</div>
																	</div>
																</div>
																<!-- SECOND ROW -->
																<div class="row">

																	<div class="form-group col-sm-10">
																		<label for="name" class="col-sm-4 required-label">
																			Name</label>
																		<div class="col-sm-8">
																			<form:input path="portaluser.name" type="text"
																				class="form-control" id="name" name="name"
																				autocomplete="off" />
																			<div class="error" id="nameerror"></div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="form-group col-sm-10" id="countryLritIdDiv">
																		<label for="countrylritid" class="col-sm-4">Country
																			Name</label>
																		<div class="col-sm-8">
																			<form:input
																				path="portaluser.requestorsLritId.countryName"
																				type="text" class="form-control" id="countryName"
																				name="countryName" readonly="true" />

																		</div>
																	</div>
																</div>
																<!-- THIRD ROW -->
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="category" class="col-sm-4 ">
																			Category</label>
																		<div class="col-sm-8">


																			<input type="text" disabled id="category"
																				value="${category}" class="form-control" />

																		</div>

																	</div>
																	<form:input path="portaluser.category" type="text"
																		hidden="true" />
																</div>
																<div class="row">
																	<div class="form-group col-sm-10" id="companyCodeDiv">
																		<label for="companyCode" class="col-sm-4 ">Company
																			Code</label>
																		<div class="col-sm-8">
																			<form:input
																				path="portaluser.portalShippingCompany.companyCode"
																				type="text" class="form-control" id="companycode"
																				readonly="true" />
																		</div>


																	</div>
																</div>
																<div class="row" id="companyNameDiv">
																	<div class="form-group col-sm-10">
																		<label for="companyName" class="col-sm-4 ">Company
																			Name</label>
																		<div class="col-sm-8">
																			<form:input
																				path="portaluser.portalShippingCompany.companyName"
																				type="text" class="form-control" id="companyname"
																				readonly="true" />
																		</div>
																	</div>
																</div>
																<div class="row" id="sarAuthorityDiv">
																	<div class="form-group col-sm-10">
																		<label for="sarauthority" class="col-sm-4">SarAuthority
																		</label>
																		<div class="col-sm-8">
																			<form:input
																				path="portalUserSarAuthority.sarAuthority.coastalArea"
																				type="text" class="form-control" id="sarauthority"
																				name="sarauthority" readonly="true" />



																		</div>
																	</div>


																</div>

																<!-- FIFTH ROW -->
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="sarareaview" class="col-sm-4"> SAR
																			Area View</label>
																		<div class="col-sm-8">
																			<div>
																				<form:checkbox path="portaluser.sarAreaView"
																					id="sarareaview" value="true" />
																				Allow
																			</div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="scannedform" class="col-sm-4">
																			Upload Forms</label>
																		<div class="col-sm-8">
																			<input name="uploadedFile" type="file"
																				class="form-control" id="scannedforms"
																				name="scannedform[]" multiple="multiple" />
																			<div class="error " id="formuploaderror"></div>
																		</div>

																	</div>

																</div>

															</div>
															<div class="col-sm-6">
																<div class="row">

																	<div class="form-group col-sm-10">
																		<label for="roles" class="col-sm-4  required-label">Assign
																			Roles</label>
																		<div class="col-sm-8 ">
																			<form:select class="form-control select2" id="roles"
																				path="roles" multiple="true"
																				data-placeholder="Select a Role">
																				<c:forEach items="${portalRoles}" var="role">

																					<div>
																						<c:choose>
																							<c:when test="${role.flag eq true}">
																								<form:option value="${role.roleId}"
																									selected="true">${role.rolename}</form:option>
																							</c:when>
																							<c:otherwise>
																								<form:option value="${role.roleId}">${role.rolename}</form:option>
																							</c:otherwise>
																						</c:choose>
																					</div>	            
																			
																	</c:forEach>
															
															         
															</form:select>
																			<div class="error " id="roleerror"></div>



																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- First Tab End-->
													<!-- Second Tab Start -->
													<div class="tab-pane" id="addressdetails_Tab">
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="address1" class="col-sm-4  required-label">Plot
																	No</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.address1" type="text"
																		class="form-control" id="address1" name="address1"
																		autocomplete="off" />
																	<div class="error " id="address1error"></div>
																</div>

															</div>
															<div class="form-group col-sm-6">
																<label for="address2" class="col-sm-4  required-label">Area/Locality</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.address2" type="text"
																		class="form-control" id="address2" name="address2"
																		autocomplete="off" />
																	<div class="error " id="address2error"></div>
																</div>

															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="address3" class="col-sm-4  required-label">Street
																	Name</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.address3" type="text"
																		class="form-control" id="address3" name="address3"
																		autocomplete="off" />
																	<div class="error " id="address3error"></div>
																</div>

															</div>
															<div class="form-group col-sm-6">
																<label for="landmark" class="col-sm-4  required-label">Landmark</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.landmark" type="text"
																		class="form-control" id="landmark" name="landmark"
																		autocomplete="off" />
																	<div class="error " id="landmarkerror"></div>
																</div>

															</div>
														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="city" class="col-sm-4  required-label">City</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.city" type="text"
																		class="form-control" id="city" name="city"
																		autocomplete="off" />
																	<div class="error " id="cityerror"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="district" class="col-sm-4  required-label">District</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.district" type="text"
																		class="form-control" id="district" name="district"
																		autocomplete="off" />
																	<div class="error " id="districterror"></div>
																</div>

															</div>
														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="state" class="col-sm-4  required-label">State</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.state" type="text"
																		class="form-control" id="state" name="state"
																		autocomplete="off" />
																	<div class="error " id="stateerror"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="country" class="col-sm-4  required-label">Country</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.country" type="text"
																		class="form-control" id="country" name="country"
																		autocomplete="off" />
																	<div class="error " id="countryerror"></div>
																</div>
															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="pincode" class="col-sm-4  required-label">Pin
																	Code</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.pincode" type="text"
																		class="form-control" id="pincode" name="pincode"
																		autocomplete="off" />
																	<div class="error " id="pincodeerror"></div>
																</div>
															</div>
														</div>

														<div class="row">

															<div class="form-group col-sm-6">
																<label for="telephoneno"
																	class="col-sm-4 required-label ">Telephone No</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.telephone" type="text"
																		class="form-control" id="telephone" name="telephone"
																		autocomplete="off" />
																	<div class="error " id="telephoneerror"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="mobileno" class="col-sm-4  ">Mobile
																	No</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.mobileno" type="text"
																		class="form-control" name="mobileno" id="mobileno"
																		autocomplete="off" />
																	<div class="error " id="mobilenoerror"></div>
																</div>
															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="fax" class="col-sm-4 ">Fax</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.fax" type="text"
																		class="form-control" name="fax" id="fax"
																		autocomplete="off" />
																	<div class="error " id="faxerror"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="website" class="col-sm-4 ">Website</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.website" type="text"
																		class="form-control" name="website" id="website"
																		autocomplete="off" />
																	<div class="error " id="websiteerror"></div>
																</div>
															</div>

														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="emailid" class="col-sm-4  required-label">Email
																	Id</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.emailid" type="text"
																		class="form-control" name="emailid" id="emailid"
																		autocomplete="off" />
																	<div class="error " id="emailiderror"></div>
																</div>
															</div>

															<div class="form-group col-sm-6">
																<label for="emailid" class="col-sm-4 required-label">Alternate
																	Email Id</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.alternateemailid"
																		type="text" autocomplete="off" class="form-control"
																		name="alternateemailid" id="alternateemailid" />
																	<div class="error " id="alternateemailiderror"></div>
																</div>
															</div>
														</div>
													</div>
													<!-- /.tab-pane  -->
													<!-- /.Second tab end -->
													<!-- Third tab start -->
													<div class="tab-pane" id="csodetails_Tab">
														<div class="box box-info table-responsive">
															<form:input path="csolength" hidden="true" />
															<div class="error " id="csotableerror"></div>
															<div class="error" id="csoerror"></div>
															<table id="csoTable"
																class="table table-bordered table-hover dataTable">
																<thead>
																	<tr>
																		<!-- <th>Type</th> -->
																		<th class="required-label">Name</th>
																		<th class="required-label">Telephone Office</th>
																		<th>Telephone Residence</th>
																		<th>Mobile No</th>
																		<th class="required-label">Email Id</th>

																		<th><input type="button" value="Add Row"
																			class="btn btn-primary  btn-flat"
																			onclick="addRow('csoTable')" /></th>
																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${currentcsoList}" var="cso"
																		varStatus="status">
																		<tr id="csoDetails">

																			<form:input
																				path="currentcsoList[${status.index}].type"
																				value="cso" hidden="true" />
																			<form:input
																				path="currentcsoList[${status.index}].csoidseq"
																				hidden="true" />
																			<form:input
																				path="currentcsoList[${status.index}].flag"
																				value="old" hidden="true" />
																			<!-- <td>Current CSO</td> -->
																			<td><form:input
																					path="currentcsoList[${status.index}].name"
																					onchange="update(this);" /></td>
																			<td><form:input
																					path="currentcsoList[${status.index}].telephoneOffice"
																					onchange="update(this);" /></td>
																			<td><form:input
																					path="currentcsoList[${status.index}].telephoneResidence"
																					onchange="update(this);" /></td>
																			<td><form:input
																					path="currentcsoList[${status.index}].mobileNo"
																					onchange="update(this);" /></td>
																			<td><form:input
																					path="currentcsoList[${status.index}].emailid"
																					onchange="update(this);" /></td>

																			<td><input id="csoremove_edituser" type="button"
																				value="Remove" onclick="csoRemove(this)"
																				class="btn btn-primary  btn-flat" /> <input
																				id="csosize" type="text" value="${status.index}"
																				hidden="true" /></td>

																		</tr>

																	</c:forEach>
																</tbody>
															</table>
														</div>
													</div>
													<!-- /.tab-pane-->
													<!-- /.Third tab end -->
													<!-- Forth tab start -->
													<div class="tab-pane" id="dpadetails_Tab">
														<div class="box box-info table-responsive">
															<form:input path="dpalength" hidden="true" />
															<div class="error" id="dpaerror"></div>
															<table id="dpaTable"
																class="table table-bordered table-hover dataTable">
																<thead>
																	<tr>
																		<!-- <th>Type</th> -->
																		<th class="required-label">Name</th>
																		<th class="required-label">Telephone Office</th>
																		<th>Telephone Residence</th>
																		<th>Mobile No</th>
																		<th class="required-label">Email Id</th>

																		<th><input type="button" value="Add Row"
																			class="btn btn-primary  btn-flat"
																			onclick="addRowDpaTable('dpaTable')" /></th>
																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${dpaList}" var="dpa"
																		varStatus="status">
																		<tr id="dpaDetails">

																			<form:input path="dpaList[${status.index}].type"
																				value="dpa" hidden="true" />
																			<form:input path="dpaList[${status.index}].csoidseq"
																				hidden="true" />
																			<form:input path="dpaList[${status.index}].flag"
																				value="old" hidden="true" />

																			<td><form:input
																					path="dpaList[${status.index}].name"
																					onchange="updatedpa(this);" /></td>
																			<td><form:input
																					path="dpaList[${status.index}].telephoneOffice"
																					onchange="updatedpa(this);" /></td>
																			<td><form:input
																					path="dpaList[${status.index}].telephoneResidence"
																					onchange="updatedpa(this);" /></td>
																			<td><form:input
																					path="dpaList[${status.index}].mobileNo"
																					onchange="updatedpa(this);" /></td>
																			<td><form:input
																					path="dpaList[${status.index}].emailid"
																					onchange="updatedpa(this);" /></td>

																			<td id="button1"><input id="dparemove_edituser"
																				type="button" value="Remove"
																				onclick="dpaRemove(this)"
																				class="btn btn-primary  btn-flat" /> <input
																				id="dpasize" type="text" value="${status.index}"
																				hidden="true" /></td>
																		</tr>

																	</c:forEach>
																</tbody>
															</table>
														</div>
													</div>
													<!-- /.tab-pane-->
													<!-- /.Forth tab end -->
												</div>

											</div>

										</div>
										<!-- Custom form ?End-->
										<!-- /.box -->
										<div class="box-footer" id="addbtn">
											<button id="next" type="button"
												class="nexttab btn btn-primary pull-right btn-next">Next</button>
											<button id="previous" type="button"
												class="btn btn-primary pull-left  btn-prev">Previous</button>

											<button id="submit" type="submit"
												class="btn btn-primary pull-right  ">Submit</button>
										</div>
									</form:form>
								</div>
							</div>
							<!-- /.form -->
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>


		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->



	<!-- Tab Script -->
	<spring:url value="/resources/js/user_tabs.js" var="tabscriptjs" />
	<script src="${tabscriptjs}"></script>
	<spring:url value="/resources/js/addRow.js" var="addrowjs" />
	<script src="${addrowjs}"></script>
	<script>
		$(document).ready(function() {

			//Initialize Select2 Elements
			$('.select2').select2();

			$('#submit').hide();
			$('#previous').hide();
			$('#next').show();
			$('#sarAuthorityDiv').hide();
			$('#addressdetailstab').click(function() {
				$('#submit').hide();
				$('#previous').show();
				$('#next').show();
			});

			$('#userdetailstab').click(function() {

				$('#submit').hide();
				$('#previous').hide();
				$('#next').show();
			});
			$('#csodetailstab').click(function() {

				$('#submit').hide();
				$('#previous').show();
				$('#next').show();
			});
			$('#dpadetailstab').click(function() {

				$('#submit').show();
				$('#previous').show();
				$('#next').hide();
			});
			var category = "${category}";
			if (category == "CoastGuard") {
				$('#sarAuthorityDiv').show();
			}

		});

		function csoRemove(CsoDpaObject) {
			//alert("Inside csoRemove "+ CsoDpaObject.id);
			var getParentNode = CsoDpaObject.parentNode;
			//alert("getParentNode " +getParentNode);
			var id = getParentNode.getElementsByTagName("input")[1].value;
			var csoid = "currentcsoList" + id + ".csoidseq";
			//alert("csoidseq "+csoid);
			csoid = document.getElementById(csoid).value;
			//alert("csoid "+csoid);
			$.ajax({
				url : '${checkcsoidmapping}',
				type : 'GET',
				data : {
					csoId : csoid
				},
				contentType : 'application/json; charset=utf-8',
				success : function(data) {
					//alert("data "+data);
					if (data == false) {
						Remove(CsoDpaObject);
					} else {
						alert("Cso cannot be deleted it is mapped with table");
					}
				},
				error : function(error) {
					alert("Error AJAX not working: " + error);
				}
			});

		}
		function dpaRemove(CsoDpaObject) {
			//alert("Inside dpaRemove "+ CsoDpaObject.id);
			var getParentNode = CsoDpaObject.parentNode;
			//alert("getParentNode " +getParentNode);
			var id = getParentNode.getElementsByTagName("input")[1].value;
			var csoid = "dpaList" + id + ".csoidseq";
			//alert("csoidseq "+csoid);
			csoid = document.getElementById(csoid).value;
			//alert("csoid "+csoid);

			$.ajax({
				url : '${checkdpamapping}',
				type : 'GET',
				data : {
					csoId : csoid
				},
				contentType : 'application/json; charset=utf-8',
				success : function(data) {
					//alert("data "+data);
					if (data == false) {
						//alert("Inside dpa remoce");
						Remove(CsoDpaObject);
					} else {
						alert("Dpa cannot be deleted it is mapped with table");
					}
				},
				error : function(error) {
					alert("Error AJAX not working: " + error);
				}
			});

		}
	</script>

</body>
</html>