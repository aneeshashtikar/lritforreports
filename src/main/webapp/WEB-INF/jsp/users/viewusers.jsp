<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>View Users</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>

<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>

<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>


<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>

<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">


<spring:url value="/users/getuserlist" var="getuserlist" />
<spring:url value="/users/getalluserlist" var="getalluserlist" />
<spring:url value="/users/edituser" var="edituser" />

<spring:url value="/users/deregisteruser" var="deregisteruser" />
<spring:url value="/users/viewuserdetails" var="viewuserdetails" />


<script type="text/javascript">
	var resultSet = function() {
		this.item = {};
	};

	var listtable = new resultSet();

	var targetTableId = 'viewuser-table';

	var dTable = function(targetTableId, resultSet, status) {

		var url;
		if (status == "all") {
			url = "${getalluserlist}";
		} else {
			url = "${getuserlist}";
		}
		resultSet.item = $('#viewuser-table').DataTable(
				{

					"ajax" : {

						"url" : url,
						"type" : "GET",
						"data" : {
							'Status' : status
						},
						"dataSrc" : "",
						"bDestroy" : true

					},
					"responsive" : {
						"details" : {
							"type" : 'column',
							"target" : 'tr'
						}
					},
					"columnDefs" : [ {
						"targets" : 0,
						"title" : "Login Id",
						"targets" : "Login_Id",
						"render" : function(data, type, row, meta) {

							return '<a href="${viewuserdetails}?loginId='
									+ data + '">' + data + '</a>';

						}
					} ]

				});
		if (status == "registered" || status == "incomplete") {

			resultSet.item.columns('#edit').visible(true);
			resultSet.item.columns('#delete').visible(true);
		} else {

			resultSet.item.columns('#edit').visible(false);
			resultSet.item.columns('#delete').visible(false);

		}

	}

	var urlmethod = function(loginId, url, listtable, targetTableId) {

		$
				.ajax({
					url : url,
					type : 'POST',
					data : {
						loginId : loginId
					},
					dataType : "json",
					success : function(data) {

						if (data == "User has deregistered successfully") {
							$(".message")
									.append(
											"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'"
									+"	aria-label='close'>&times;</a>"
													+ data + " </div>")
						} else {
							$(".message")
									.append(
											"<div class='alert alert-danger colose'><a href='#' class='close' data-dismiss='alert'"
									+"	aria-label='close'>&times;</a>"
													+ data + " </div>")
						}
						$('#viewuser-table').DataTable().clear().destroy();
						dTable('#' + targetTableId, listtable, "registered");

					},
					error : function(error) {
						alert("Error AJAX not working: " + error);
					}
				});

	}

	$(document)
			.ready(
					function() {
						<%--Zoom out handle for delete button--%>
						dTable('#' + targetTableId, listtable, "registered");
						$('body')
								.on(
										'click',
										'#viewuser-table tbody td>button[name=btndelete]',
										function() {
											var loginId = listtable.item.row(
													$(this).parents('tr'))
													.data().loginId;

											var url = "${deregisteruser}";
											urlmethod(loginId, url, listtable,
													targetTableId);

										});
<%--Zoom in handle for delete button--%>
	$('body')
								.on(
										'click',
										'#viewuser-table.collapsed tbody li span>button[name=btndelete]',
										function() {
											var cellIndex = listtable.item.responsive
													.index($(this).parent()
															.parent());
											var loginId = listtable.item.data()[cellIndex.row].loginId;
											var url = "${deregisteruser}";
											if (confirm(" Are You Sure ? ")) {
												urlmethod(loginId, url,
														listtable,
														targetTableId);
											}

										});
	<%--Zoom out handle for edit button--%>
						$('body')
								.on(
										'click',
										'#viewuser-table tbody td>button[name=btnedit]',
										function() {
											var loginId = listtable.item.row(
													$(this).parents('tr'))
													.data().loginId;

											var url = "${edituser}?loginId="
													+ loginId;

											window.location = url;
										});
<%--Zoom in handle for edit button--%>
	$('body')
								.on(
										'click',
										'#viewuser-table.collapsed tbody li span>button[name=btnedit]',
										function() {
											var cellIndex = listtable.item.responsive
													.index($(this).parent()
															.parent());
											var loginId = listtable.item.data()[cellIndex.row].loginId;
											var url = "${edituser}?loginId="
													+ loginId;

											window.location = url;

										});

					});
	function onchangestatus() {

		$('#viewuser-table').DataTable().clear().destroy();

		var status = document.getElementById("status").value;
		dTable('#' + targetTableId, listtable, status);

	}
</script>

</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">

				<div class="row">
					<div class="col-md-10">
						<div class="message"></div>
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">List of Users</h3>
							</div>

						</div>
						<div class="box">
							<!-- Report Table -->
							<div class="box-body">

								<div class="form-group col-sm-5">
									<label for="status" class="col-sm-4 ">Status of User</label>
									<div class="col-sm-8">
										<select class="form-control" name="status" id="status"
											onchange="onchangestatus();">
											<option value="all">All</option>
											<!-- 	<option value="active" selected="selected">Active</option>
											<option value="inactive">Inactive</option>
											<option value="incomplete">Incomplete</option> -->
											<option value="registered" selected="selected">Registered</option>
											<option value="deregistered">Deregistered</option>
											<option value="incomplete">Incomplete</option>

										</select>

									</div>
								</div>
								<table id="viewuser-table"
									class="table table-bordered table-striped " style="width: 100%">
									<thead>
										<tr>

											<th class="Login_Id" data-data="loginId"
												data-default-content="NA">Login Id</th>
											<th data-data="name" data-default-content="NA">Name</th>
											<th data-data="category" data-default-content="NA">Category</th>
											<th data-data="status" data-default-content="NA">Status</th>
											<th data-data="country" data-default-content="NA">CountryName</th>
											<sec:authorize access="hasAuthority('edituser')">
												<th data-data="null" id="edit"
													data-default-content="<button name='btnedit' class='btn btn-primary'> Edit</button>"></th>
											</sec:authorize>
											<sec:authorize access="hasAuthority('deleteuser')">
												<th data-data="null" id="delete"
													data-default-content="<button name='btndelete'  class='btn btn-primary'> Deregister</button>"></th>
											</sec:authorize>
										</tr>
									</thead>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
					</div>



					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<!-- Control Sidebar -->
	<%-- <jsp:include page="../rightbar.jsp"></jsp:include> --%>
	<!-- /.control-sidebar -->

	<!-- <div class="control-sidebar-bg"></div> -->
	<!-- </div> -->
	<!-- ./wrapper -->



</body>
</html>
