<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>View User Detail</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">


<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">



<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->

<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
<script src="${adminlteminjs}"></script>

<!-- Select2 -->
<spring:url value="/bower_components/select2/dist/css/select2.min.css"
	var="dropdowncss" />
<link rel="stylesheet" href="${dropdowncss}">
<!-- Select2 -->
<spring:url
	value="/bower_components/select2/dist/js/select2.full.min.js"
	var="dropdownjss" />
<script src="${dropdownjss}"></script>

<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">

<spring:url value="/users/persistuser" var="persistuser" />
<spring:url value="/users/edituser" var="edituser" />
<spring:url value="/users/download" var="download" />



</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-lg-10">
						<!-- general form elements -->


						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">View User Detail</h3>
							</div>
							<!-- form start-->
							<div class="box-body">
								<div class="form-group">

									<form:form class="form-horizontal" method="GET"
										id="viewuserdetailsform" name="viewuserdetailsform"
										modelAttribute="portaluserdto">
										<div class="box-body">

											<!-- Custom form -->
											<div class="nav-tabs-custom" id="tabs">

												<ul class="nav nav-tabs">
													<li class="active" id="userdetailstab"><a
														href="#userdetails_Tab" data-toggle="tab">User Details</a></li>
													<li id="addressdetailstab"><a
														href="#addressdetails_Tab" data-toggle="tab">Address
															Details</a></li>
													<c:if test="${category == 'Shipping Company'}">
														<li id="csodetailstab"><a href="#csodetails_Tab"
															data-toggle="tab">CSO Details</a></li>
														<li id="dpadetailstab"><a href="#dpadetails_Tab"
															data-toggle="tab">DPA Details</a></li>
														<li id="csodpamappingtab"><a
															href="#csodpamapping_Tab" data-toggle="tab">CSO-Alternate
																CSO Mapping</a></li>
													</c:if>

												</ul>
												<div class="tab-content">
													<!-- First Tab -->
													<div class="tab-pane active" id="userdetails_Tab">
														<!-- FIRST ROW -->
														<div class="row">
															<div class="col-sm-6">

																<div class="row">
																	<div class="form-group required col-sm-10 ">
																		<label for="portaluser.loginid" class="col-sm-4 ">
																			Login Id</label>
																		<div class="col-sm-8">
																			<input value="${portaluserdto.portaluser.loginId }"
																				type="text" class="form-control" id="loginid"
																				readonly />
																		</div>
																	</div>
																</div>
																<!-- SECOND ROW -->
																<div class="row">

																	<div class="form-group col-sm-10">
																		<label for="name" class="col-sm-4 "> Name</label>
																		<div class="col-sm-8">
																			<input value="${portaluserdto.portaluser.name}"
																				type="text" class="form-control" id="name" readonly />
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="form-group col-sm-10" id="countryLritIdDiv">
																		<label for="countrylritid" class="col-sm-4">Country
																			Name</label>
																		<div class="col-sm-8">
																			<input type="text" class="form-control"
																				id="countryName" readonly
																				value="${portaluserdto.portaluser.requestorsLritId.countryName}" />

																		</div>
																	</div>
																</div>
																<!-- THIRD ROW -->
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="category" class="col-sm-4 ">
																			Category</label>
																		<div class="col-sm-8">


																			<input type="text" readonly id="category"
																				value="${category}" class="form-control" />

																		</div>

																	</div>

																</div>

																<c:if test="${category == 'Shipping Company'}">
																	<div class="row">
																		<div class="form-group col-sm-10" id="companyCodeDiv">
																			<label for="companyCode" class="col-sm-4 ">Company
																				Code</label>
																			<div class="col-sm-8">
																				<input
																					value="${portaluserdto.portaluser.portalShippingCompany.companyCode }"
																					type="text" class="form-control" id="companycode"
																					readonly />
																			</div>


																		</div>
																	</div>
																	<div class="row" id="companyNameDiv">
																		<div class="form-group col-sm-10">
																			<label for="companyName" class="col-sm-4 ">Company
																				Name</label>
																			<div class="col-sm-8">
																				<input
																					value="${portaluserdto.portaluser.portalShippingCompany.companyName}"
																					type="text" class="form-control" id="companyname"
																					readonly />
																			</div>
																		</div>
																	</div>
																</c:if>
																<c:if test="${category == 'CoastGuard'}">
																	<div class="row" id="sarAuthorityDiv">
																		<div class="form-group col-sm-10">
																			<label for="sarauthority" class="col-sm-4">SarAuthority
																			</label>
																			<div class="col-sm-8">
																				<input
																					value="${portaluserdto.portalUserSarAuthority.sarAuthority.coastalArea}"
																					type="text" class="form-control" id="sarauthority"
																					name="sarauthority" readonly />



																			</div>
																		</div>


																	</div>
																</c:if>

																<!-- FIFTH ROW -->
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="sarareaview" class="col-sm-4"> SAR
																			Area View</label>
																		<div class="col-sm-8">
																			<div>
																				<input type="checkbox" name="portaluser.sarAreaView"
																					id="sarareaview" disabled /> Allow
																			</div>
																		</div>
																	</div>
																</div>

																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="scannedform" class="col-sm-4">
																			Forms</label>
																		<div class="col-sm-8">

																			<%-- <a
																				href="/lrit/users/download/${portaluserdto.portaluser.loginId } ">Download
																				a File</a> --%>
																				<div class="col-sm-8">
																			<a href="${download}?loginId=${portaluserdto.portaluser.loginId } " target="_blank">View Registration Form </a>  
																		</div>
																		</div>
																	</div>

																</div>

															</div>
															<div class="col-sm-6">
																<div class="row">

																	<div class="form-group col-sm-10">
																		<label for="roles" class="col-sm-4">Assign
																			Roles</label>
																		<div class="col-sm-8 ">
																			<select class="form-control select2" id="roles"
																				multiple data-placeholder="Select a Role">
																				<c:forEach items="${portalRoles}" var="role">


																					<c:choose>
																						<c:when test="${role.flag eq true}">

																							<option value="${role.roleId}" selected>${role.rolename}</option>
																						</c:when>
																						<c:otherwise>

																						</c:otherwise>
																					</c:choose>
																					            
																			
																	</c:forEach>          
																			</select>



																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- First Tab End-->
													<!-- Second Tab Start -->
													<div class="tab-pane" id="addressdetails_Tab">
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="address1" class="col-sm-4">Plot No</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.address1 }"
																		type="text" class="form-control" id="address1"
																		readonly />
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="address2" class="col-sm-4">Area/Locality</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.address2}"
																		type="text" class="form-control" id="address2"
																		readonly />
																</div>
															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="address3" class="col-sm-4">Street
																	Name</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.address3}"
																		type="text" class="form-control" id="address3"
																		readonly />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="city" class="col-sm-4 ">City</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.city}"
																		type="text" class="form-control" id="city" readonly />
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="district" class="col-sm-4">District</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.district}"
																		type="text" class="form-control" id="district"
																		readonly />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="landmark" class="col-sm-4 ">Landmark</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.landmark}"
																		type="text" class="form-control" id="landmark"
																		readonly />
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="pincode" class="col-sm-4 ">Pin Code</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.pincode}"
																		type="text" class="form-control" id="pincode" readonly />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="state" class="col-sm-4 ">State</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.state}"
																		type="text" class="form-control" id="state" readonly />
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="country" class="col-sm-4 ">Country</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.country}"
																		type="text" class="form-control" id="country" readonly />

																</div>
															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="telephoneno" class="col-sm-4 ">Telephone
																	No</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.telephone}"
																		type="text" class="form-control" id="telephone"
																		readonly />
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="mobileno" class="col-sm-4 ">Mobile
																	No</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.mobileno }"
																		type="text" class="form-control" readonly />
																</div>
															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="fax" class="col-sm-4 ">Fax</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.fax}"
																		type="text" class="form-control" id="fax" readonly />
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="website" class="col-sm-4 ">Website</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.website}"
																		type="text" class="form-control" id="website" readonly />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="emailid" class="col-sm-4 ">Email Id</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.emailid}"
																		type="text" class="form-control" id="emailid" readonly />
																</div>
															</div>
															<div class="row">
															<div class="form-group col-sm-6">
																<label for="emailid" class="col-sm-4 ">Alternate Email Id</label>
																<div class="col-sm-8">
																	<input value="${portaluserdto.portaluser.alternateemailid}"
																		type="text" class="form-control" id="alternateemailid" readonly />
																</div>
															</div>

														</div>
													</div>
													</div>
													<!-- /.tab-pane  -->
													<!-- /.Second tab end -->
													<!-- Third tab start -->
													<div class="tab-pane" id="csodetails_Tab">
														<div class="box box-info">

															<table id="csoTable"
																class="table table-bordered table-hover dataTable">
																<thead>
																	<tr>

																		<th>Name</th>
																		<th>Telephone Office</th>
																		<th>Telephone Residence</th>
																		<th>Mobile No</th>
																		<th>Email Id</th>

																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${currentcsoList}" var="cso"
																		varStatus="status">
																		<tr id="currentCsoDetails">

																			<td><input value="${ cso.name}" readonly /></td>
																			<td><input value="${ cso.telephoneOffice}"
																				readonly /></td>
																			<td><input value="${cso.telephoneResidence }"
																				readonly /></td>
																			<td><input value="${cso.mobileNo }" readonly /></td>
																			<td><input value="${ cso.emailid}" readonly /></td>



																		</tr>

																	</c:forEach>
																</tbody>
															</table>

														</div>
													</div>
													<!-- /.tab-pane-->
													<!-- /.Third tab end -->
													<!-- Forth tab start -->
													<div class="tab-pane" id="dpadetails_Tab">
														<div class="box box-info">

															<table id="dpaTable"
																class="table table-bordered table-hover dataTable">
																<thead>
																	<tr>

																		<th>Name</th>
																		<th>Telephone Office</th>
																		<th>Telephone Residence</th>
																		<th>Mobile No</th>
																		<th>Email Id</th>

																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${dpaList}" var="dpa"
																		varStatus="status">
																		<tr id="dpaDetails">

																			<td><input value="${ dpa.name}" readonly /></td>
																			<td><input value="${dpa.telephoneOffice }"
																				readonly /></td>
																			<td><input value="${ dpa.telephoneResidence}"
																				readonly /></td>
																			<td><input value="${ dpa.mobileNo}" readonly /></td>
																			<td><input value="${ dpa.emailid}" readonly /></td>


																		</tr>

																	</c:forEach>
																</tbody>
															</table>

														</div>

													</div>
													<!-- /.Forth tab end -->

													<!--#csodpamapping_Tab  -->
													<!-- Fifth tab start -->
													<div class="tab-pane" id="csodpamapping_Tab">
														<div class="box box-info">

															<table id="csodpamapping"
																class="table table-bordered table-hover dataTable">
																<thead>
																	<tr>

																		<th>Cso</th>
																		<th>AlternateCSO</th>


																	</tr>
																</thead>
																<tbody>
																	<c:forEach items="${currentcsoList}" var="cso"
																		varStatus="status">
																		<tr id="currentCsoDetails">

																			<td><input value="${cso.name }" readonly /></td>
																			<td><c:forEach items="${currentcsoList}"
																					var="alternatecso" varStatus="status">
																					<c:choose>
																						<%-- <c:when test="${role.flag eq true}"> --%>
																						<c:when
																							test="${cso.parentCsoid == alternatecso.csoidseq}">
																							<input value="${alternatecso.name}" readonly>
																						</c:when>
																						<c:otherwise>


																						</c:otherwise>
																					</c:choose>
																				</c:forEach></td>
																		</tr>

																	</c:forEach>
																</tbody>
															</table>

														</div>

													</div>
													<!-- /.Fifth tab end -->
												</div>

											</div>





										</div>
										<!-- Custom form ?End-->
										<!-- /.box -->
										<div class="box-footer" id="addbtn">
											<button id="next" type="button"
												class="nexttab btn btn-primary pull-right btn-next">Next</button>
											<button id="previous" type="button"
												class="btn btn-primary pull-left  btn-prev">Previous</button>
											<!-- </div>
									<div class="box-footer" id="submit"> -->
											<sec:authorize access="hasAuthority('edituser')">
												<c:if
													test="${portaluserdto.portaluser.status == 'registered' || portaluserdto.portaluser.status == 'incomplete' }">
													<button id="submit" type="button"
														class="btn btn-primary pull-right  ">Edit</button>
												</c:if>
											</sec:authorize>
										</div>
									</form:form>
								</div>
							</div>
							<!-- /.form -->
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>

		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->



	<!-- Tab Script -->
	<!-- <script src="../js/tab_script.js"></script> -->
	<spring:url value="/resources/js/user_tabs.js" var="tabscriptjs" />
	<script src="${tabscriptjs}"></script>

	<script>
		/* 	function OnSubmitForm()
		 {
		 var loginId=  document.getElementById("loginid").value;
		 var url="${edituser}?loginId="+loginId;
		 alert("url "+url);
		 document.viewuserdetailsform.action =url;
		 //return url;
		 return true;

		 } */
		$('#submit').click(function() {
			//do something
			var loginId = document.getElementById("loginid").value;
			var url = "${edituser}?loginId=" + loginId;
			//alert("url "+url);
			window.location = url;
		});
		/* $('submit')
		.on(
				'click',
				'#button[name=btnedit]',
				function() {

					var roleId = listtable.item.row(
							$(this).parents('tr'))
							.data().roleId;

					var url = "${editrole}?roleId="
							+ roleId;
					if (confirm(" Are You Sure ? ")) {
						window.location = url;
					}
				}); */
		$(document).ready(function() {
			//Initialize Select2 Elements

			$('.select2').select2();

			$('#submit').hide();
			$('#previous').hide();
			$('#next').show();
			$('#sarAuthorityDiv').hide();
			$('#addressdetailstab').click(function() {
				var category = document.getElementById("category").value;
				//alert("category "+category);
				if (category == 'Shipping Company') {
					$('#submit').hide();
					$('#previous').show();
					$('#next').show();
				} else {
					$('#submit').show();
					$('#previous').show();
					$('#next').hide();
				}
			});

			$('#userdetailstab').click(function() {

				$('#submit').hide();
				$('#previous').hide();
				$('#next').show();

			});
			$('#csodetailstab').click(function() {

				$('#submit').hide();
				$('#previous').show();
				$('#next').show();
			});
			$('#dpadetailstab').click(function() {

				$('#submit').hide();
				$('#previous').show();
				$('#next').show();
			});
			$('#csodpamappingtab').click(function() {

				$('#submit').show();
				$('#previous').show();
				$('#next').hide();
			});

			var category = "${category}";
			if (category == "CoastGuard") {
				$('#sarAuthorityDiv').show();
			}

		});
		/* function addRow(tableID) {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var counts = rowCount - 1;

			var cell1 = row.insertCell(0);
			var name = document.createElement("input");
			name.type = "text";
			name.name = "csoList[" + counts + "].name";
			cell1.appendChild(name);

			var cell2 = row.insertCell(1);
			var telephoneOffice = document.createElement("input");
			telephoneOffice.type = "text";
			telephoneOffice.name = "csoList[" + counts + "].telephoneOffice";
			cell2.appendChild(telephoneOffice);

			var cell3 = row.insertCell(2);
			var telephoneResidence = document.createElement("input");
			telephoneResidence.type = "text";
			telephoneResidence.name = "csoList[" + counts
					+ "].telephoneResidence";
			cell3.appendChild(telephoneResidence);

			var cell4 = row.insertCell(3);
			var mobileNo = document.createElement("input");
			mobileNo.type = "text";
			mobileNo.name = "csoList[" + counts + "].mobileNo";
			cell4.appendChild(mobileNo);

			var cell5 = row.insertCell(4);
			var emailid = document.createElement("input");
			emailid.type = "text";
			emailid.name = "csoList[" + counts + "].emailid";
			cell5.appendChild(emailid);

			var cell6 = row.insertCell(5);
			var type = document.createElement("select");
			type.name = "csoList[" + counts + "].type";
			var list = [ "cso", "dpa" ];
			var options_str = "";
			list.forEach(function(option) {
				options_str += '<option value="' + option + '">' + option
						+ '</option>';
			});
			type.innerHTML = options_str;
			cell6.appendChild(type);

			var cell7 = row.insertCell(6);
			var remove = document.createElement("input");
			remove.type = "button";
			remove.value = "Remove";
			remove.setAttribute("onclick", "Remove(this)");
			remove.setAttribute('class', 'btn btn-primary  btn-flat');
			cell7.appendChild(remove);

		}
		function Remove(button) {
			//Determine the reference of the Row using the Button.
			var IDnum = 0;
			var row = button.parentNode.parentNode;
			//alert(row);
			var name = row.getElementsByTagName("td")[0].innerHTML;
			var Id = row.rowIndex;
			Id--;
			var element = "csoList" + Id + ".flag";
			document.getElementById(element).value = 'remove';

			//Get the reference of the Table.
			var table = document.getElementById("csoTable");

			//Delete the Table row using it's Index.
			table.deleteRow(row.rowIndex);

		}
		function update(obj) {

			var row = obj.parentNode.parentNode;

			var Id = row.rowIndex;
			Id--;

			var element = "csoList" + Id + ".flag";
			document.getElementById(element).value = 'update';
			var chk = document.getElementById(element).value;

		} */
	</script>

</body>
</html>