<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Company Details</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<%-- <spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url> --%>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<!-- <script src="/lrit/js/button1.js"></script> -->
<!-- <script src="/lrit/js/validateform.js"></script> -->
<script src="/lrit/js/viewvessel.js"></script>

<!-- <style>
/* .side-box {
	margin-bottom: 0px;
	/* height: 200px;
} */
</style> -->

<style>
.table_wrapper {
	display: block;
	overflow-x: auto;
	white-space: nowrap;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var table = $('#shipborneTable').DataTable({
		});
	});
</script>


</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<!-- <section class="content-header">
				<h1>Vessel Registration Form</h1>
			</section> -->

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">
						<!-- Horizontal Form -->
						<!-- <div class="box box-info"> -->
							<div class="box-header with-border">
								<h3 class="box-title">Company Details</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->
							<form class="form-horizontal" id="companyDetailForm"
								name="companyDetailForm">
								<div class="box-body">

									<!-- Custom form -->
									<div class="nav-tabs-custom">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#tab_1" data-toggle="tab">Company
													Details</a></li>
											<li><a href="#tab_2" data-toggle="tab">CSO Detail</a></li>
											<!-- <li class="pull-right"><a href="#" class="text-muted"><i
													class="fa fa-gear"></i></a></li> -->
										</ul>
										<div class="tab-content">
											<!-- First Tab -->
											<div class="tab-pane active" id="tab_1">
												<!-- FIRST ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="compName" class="col-sm-4 ">Name of
															Company</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="compName"
																placeholder="Name of Company" readonly value="${userDetails.portalShippingCompany.companyName }">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="compCode" class="col-sm-4">Company
															Code</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="compCode"
																placeholder="Company Code" readonly value="${userDetails.portalShippingCompany.companyCode }">
															<!-- value="${userPojo.portalShippingCompany.companyCode } -->
														</div>
													</div>
												</div>
												<!-- SECOND ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="plotNo" class="col-sm-4 "> Building /
															Plot No</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="plotNo"
																placeholder="Building / Plot No" readonly>
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="add1" class="col-sm-4 ">Address 1</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="add1"
																placeholder="Address 1" readonly value="${userDetails.address1 }">
														</div>
													</div>
												</div>
												<!-- THIRD ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="add2" class="col-sm-4"> Address 2</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="add2"
																placeholder="Address 2" readonly value="${userDetails.address2 }">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="add3" class="col-sm-4">Address 3</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="add3"
																placeholder="Address 3" readonly value="${userDetails.address3 }">
														</div>
													</div>
												</div>
												<!-- FOURTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="city" class="col-sm-4 "> City</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="city"
																placeholder="City" readonly value="${userDetails.city }">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="district" class="col-sm-4 ">District</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="district"
																placeholder="district" readonly value="${userDetails.district }">
														</div>
													</div>
												</div>
												<!-- FIFTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="landmark" class="col-sm-4"> Landmark</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="landmark"
																placeholder="Landmark" readonly value="${userDetails.landmark }">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="pincode" class="col-sm-4"> Pin Code</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="pincode"
																placeholder="Pin Code" readonly value="${userDetails.pincode }">
														</div>
													</div>
												</div>
												<!-- SIXTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="state" class="col-sm-4 "> State</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="state"
																placeholder="State" readonly value="${userDetails.state }">
														</div>
													</div>
												</div>
												<!-- SEVENTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="country" class="col-sm-4 "> Country</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="country"
																placeholder="Country" readonly value="${userDetails.country }">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="telno" class="col-sm-4 ">Telephone No</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="telno"
																placeholder="Telephone No" readonly value="${userDetails.telephone }">
														</div>
													</div>
												</div>
												<!-- EIGHT ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="mobile" class="col-sm-4 "> Mobile</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="mobile"
																placeholder="Mobile" readonly value="${userDetails.mobileno }">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="fax" class="col-sm-4 ">Fax</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="fax"
																placeholder="Fax" readonly value="${userDetails.fax }">
														</div>
													</div>
												</div>
												<!-- NINTH ROW -->
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="email" class="col-sm-4 "> Email ID</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="email"
																placeholder="Email ID" readonly value="${userDetails.emailid }">
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="website" class="col-sm-4 ">Website</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="website"
																placeholder="Website" readonly value="${userDetails.website }">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="form-group col-sm-6">
														<label for="email" class="col-sm-4 "> </label>
														<div class="col-sm-7">
															<label></label>
														</div>
													</div>
													<div class="form-group col-sm-6">
														<label for="website" class="col-sm-4 "></label>
														<div class="col-sm-7">
															<label>eg. http://www.yahoo.com</label>
														</div>
													</div>
												</div>
											</div>
											<!-- First Tab End-->


											<!-- /.tab-pane -->
											<div class="tab-pane" id="tab_2">
												<!-- <div class="box box-info"> -->
												<div class="box">
													<div class="box-header with-border">
														<h3 class="box-title" align="center">CSO's Details</h3>
													</div>
													<!-- /.box-header -->
													<div class="box-body">
														<!-- Table added for shipbourne equipment details -->
														<table class="table table-bordered table-striped" id="shipborneTable">
															<thead>
																<tr>
																	<th></th>
																	<th>Name of the Person</th>
																	<th>Telephone Office</th>
																	<th>Telephone Residence</th>
																	<th>Mobile No</th>
																	<th>Email Id</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<th>CSO Details</th>
																	<th>${csoDetails.name}</th>
																	<th>${csoDetails.telephoneOffice}</th>
																	<th>${csoDetails.telephoneResidence }</th>
																	<th>${csoDetails.mobileNo }</th>
																	<th>${csoDetails.emailid}</th>
																</tr>
																<tr>
																	<th>Alternate CSO Details</th>
																	<th>${alternateCsoDetails.name}</th>
																	<th>${alternateCsoDetails.telephoneOffice}</th>
																	<th>${alternateCsoDetails.telephoneResidence }</th>
																	<th>${alternateCsoDetails.mobileNo }</th>
																	<th>${alternateCsoDetails.emailid}</th>
																</tr>
															</tbody>
														</table>
														<!-- /.table end -->
													</div>
													<!-- /.box-body -->
													</div>
													<div class="box-footer clearfix"></div>
													<!-- /.box-footer -->
												<!-- </div> -->
											</div>
											<!-- Second Tab End-->
										</div>
									</div>
									<!-- Custom form ?End-->
								</div>
								<!-- /.box -->
								<!-- <div class="box-footer">
									<a href="#tab_1" data-toggle="tab"><button type="button" class="btn btn-info pull-left btn-prev">Previous</button></a>
									<a href="#tab_2" data-toggle="tab"><button type="button" class="btn btn-info pull-right btn-next">Next</button></a>
								</div> -->
							</form>

							<!-- /.form -->
						<!--</div>-->
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>

		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
</body>
</html>