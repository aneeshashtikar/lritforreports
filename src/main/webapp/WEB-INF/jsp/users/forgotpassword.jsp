<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Set Password</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- iCheck -->
<spring:url value="/plugins/iCheck/square/blue.css" var="bluecss" />
<link rel="stylesheet" href="${bluecss}">



<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<spring:url value="/users/forgotpassword" var="forgotpassword" />
<spring:url value="/resources/img/header_12.jpg" var="bannerImage" />
<spring:url value="/users/setpassword" var="setpassword" />
<style>
#wrapper-img {
	background-image: url("../resources/img/img2.jpg");
}
.login-box, .register-box {
    width: 500px; 
    margin: 7% auto;
}
</style>
</head>
<body class="hold-transition login-page">
	<div id="wrapper-img" class="wrapper">
		<div id="header-img">
			<header class="main-header main-header-custom ">
				<img class="img-fluid" src="${bannerImage}" height="100px"
					width="100%">


			</header>
		</div>

		<div class="login-box">
	
			<!-- /.login-logo -->
			<div class="login-box-body">
				<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
				<p class="login-box-msg">forgot Password</p>

				<form:form id="setpassword" action="${forgotpassword}"
										method="post" name="setpassword" modelAttribute="portaluser">
										<div class="box-body">
											<div class="row">
												<div class="form-group col-sm-12">
													<label class="col-sm-4 ">LoginId</label>
													<div class="col-sm-8">
															<form:input path="loginId" type="text"
															class="form-control" id="loginId" autocomplete="off"
															placeholder="loginId" pattern="[a-zA-Z0-9]+"
															title="LoginId should be alpha numeric" require="true" />
													</div>
												</div>
											</div>
											
											


										</div>
										<!-- /.box-body -->

										<div class="box-footer">
											<button type="submit" class="btn btn-primary pull-left">Submit</button>
										</div>
									</form:form>
									<!-- /.form -->
			</div>
			<!-- /.login-box-body -->
		</div>
		<!-- /.login-box -->
	</div>
	<!-- /.Wrapper end -->

	<!-- jQuery 3 -->
	<spring:url value="/bower_components/jquery/dist/jquery.min.js"
		var="jqueryminjs" />
	<script src="${jqueryminjs}"></script>
	<!-- Bootstrap 3.3.7 -->
	<spring:url
		value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
		var="bootstrapminjs" />
	<script src="${bootstrapminjs}"></script>
	<!-- iCheck -->
	<spring:url value="/plugins/iCheck/icheck.min.js" var="icheck" />
	<script src="${icheck}"></script>
	<script src=""></script>
	<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
