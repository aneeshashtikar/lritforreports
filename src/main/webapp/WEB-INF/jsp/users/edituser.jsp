<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Edit User Form</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<%-- <spring:url value="/resources/bower_components/Ionicons/css/ionicons.min.css" var="ioniconsmincss" /> --%>
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- Theme style -->

<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<%-- <spring:url value="/resources/dist/css/skins/_all-skins.min.css" var="allskinmincss" /> --%>
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<spring:url value="/login" var="Login" />

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->

<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
<script src="${adminlteminjs}"></script>

<!-- Select2 -->
<spring:url value="/bower_components/select2/dist/css/select2.min.css"
	var="dropdowncss" />
<link rel="stylesheet" href="${dropdowncss}">
<!-- Select2 -->
<spring:url
	value="/bower_components/select2/dist/js/select2.full.min.js"
	var="dropdownjss" />
<script src="${dropdownjss}"></script>


<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<script src="../js/upload.js"></script>
<spring:url value="/users/persistuser" var="persistuser" />
<spring:url value="/users/persistedituser" var="edituser" />
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-lg-10">
						<!-- general form elements -->


						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Edit User</h3>
							</div>
							<!-- form start-->
							<div class="box-body">
								<div class="form-group">
									<div class="error " id="formerror"></div>
									<form:form class="form-horizontal" action="${edituser}"
										method="post" id="edituserform" name="edituser"
										onsubmit="return validatePortalUser(this)"
										enctype="multipart/form-data" modelAttribute="portaluserdto">
										<div class="box-body">

											<!-- Custom form -->
											<div class="nav-tabs-custom" id="tabs">

												<ul class="nav nav-tabs">
													<li class="active" id="userdetailstab"><a
														href="#userdetails_Tab" data-toggle="tab">User Details</a></li>
													<li id="addressdetailstab"><a
														href="#addressdetails_Tab" data-toggle="tab">Address
															Details</a></li>


												</ul>
												<div class="tab-content">
													<!-- First Tab -->
													<div class="tab-pane active" id="userdetails_Tab">
														<!-- FIRST ROW -->
														<div class="row">
															<div class="col-sm-6">
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="portaluser.loginid" class="col-sm-4 ">
																			Login Id</label>
																		<div class="col-sm-8">
																			<form:input path="portaluser.loginId" type="text"
																				class="form-control" id="loginid" name="loginId"
																				readonly="true" />
																			<div class="error " id="loginIderror"></div>
																		</div>
																	</div>
																</div>
																<!-- SECOND ROW -->
																<div class="row">

																	<div class="form-group col-sm-10">
																		<label for="name" class="col-sm-4 required-label">
																			Name</label>
																		<div class="col-sm-8">
																			<form:input path="portaluser.name" type="text"
																				class="form-control" id="name" name="name"
																				autocomplete="off" />
																			<div class="error" id="nameerror"></div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="form-group col-sm-10" id="countryLritIdDiv">
																		<label for="countrylritid" class="col-sm-4">Country
																			Name</label>
																		<div class="col-sm-8">
																			<form:input
																				path="portaluser.requestorsLritId.countryName"
																				type="text" class="form-control" id="countryName"
																				name="countryName" readonly="true" />

																		</div>
																	</div>
																</div>
																<!-- THIRD ROW -->
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="category" class="col-sm-4 ">
																			Category</label>
																		<div class="col-sm-8">


																			<input type="text" disabled id="category"
																				value="${category}" class="form-control" />

																		</div>

																	</div>
																	<form:input path="portaluser.category" type="text"
																		hidden="true" />
																</div>
																<div class="row" id="sarAuthorityDiv">
																	<div class="form-group col-sm-10">
																		<label for="sarauthority" class="col-sm-4">SarAuthority
																		</label>
																		<div class="col-sm-8">
																			<form:input
																				path="portalUserSarAuthority.sarAuthority.coastalArea"
																				type="text" class="form-control" id="sarauthority"
																				name="sarauthority" readonly="true" />



																		</div>
																	</div>


																</div>

																<!-- FIFTH ROW -->
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="sarareaview" class="col-sm-4"> SAR
																			Area View</label>
																		<div class="col-sm-8">
																			<div>
																				<form:checkbox path="portaluser.sarAreaView"
																					id="sarareaview" value="true" />
																				Allow
																			</div>
																		</div>
																	</div>
																</div>

																<!-- SIXTH ROW -->
																<div class="row">
																	<div class="form-group col-sm-10">
																		<label for="scannedform" class="col-sm-4">
																			Upload Forms</label>
																		<div class="col-sm-8">
																			<input name="uploadedFile" type="file"
																				class="form-control" id="scannedforms"
																				name="scannedform[]" multiple="multiple" />
																			<div class="error " id="formuploaderror"></div>
																		</div>
																	</div>
																	<span id="heading"></span>
																	<div class="form-group col-sm-6" id="forms"></div>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="row">

																	<div class="form-group col-sm-10">
																		<label for="roles" class="col-sm-4 required-label">Assign
																			Roles</label>
																		<div class="col-sm-8 ">
																			<form:select class="form-control select2" id="roles"
																				path="roles" multiple="true"
																				data-placeholder="Select a State">
																				<c:forEach items="${portalRoles}" var="role">

																					<div>
																						<c:choose>
																							<c:when test="${role.flag eq true}">
																								<form:option value="${role.roleId}"
																									selected="true">${role.rolename}</form:option>
																							</c:when>
																							<c:otherwise>
																								<form:option value="${role.roleId}">${role.rolename}</form:option>
																							</c:otherwise>
																						</c:choose>
																					</div>	            
																			
																	</c:forEach>
															
															         
															</form:select>

																			<div class="error " id="roleerror"></div>

																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- First Tab End-->
													<!-- Second Tab Start -->
													<div class="tab-pane" id="addressdetails_Tab">
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="address1" class="col-sm-4 required-label">Plot
																	No</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.address1" type="text"
																		class="form-control" id="address1" name="address1"
																		autocomplete="off" />
																	<div class="error " id="address1error"></div>
																</div>

															</div>
															<div class="form-group col-sm-6">
																<label for="address2" class="col-sm-4 required-label">Area/Locality</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.address2" type="text"
																		class="form-control" id="address2" name="address2"
																		autocomplete="off" />
																	<div class="error " id="address2error"></div>
																</div>
															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="address3" class="col-sm-4 required-label">Street
																	Name</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.address3" type="text"
																		class="form-control" id="address3" name="address3"
																		autocomplete="off" />
																	<div class="error " id="address3error"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="landmark" class="col-sm-4 required-label">Landmark</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.landmark" type="text"
																		class="form-control" id="landmark" name="landmark"
																		autocomplete="off" />
																	<div class="error " id="landmarkerror"></div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="city" class="col-sm-4 required-label">City</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.city" type="text"
																		class="form-control" id="city" name="city"
																		autocomplete="off" />
																	<div class="error " id="cityerror"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="district" class="col-sm-4 required-label">District</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.district" type="text"
																		class="form-control" id="district" name="district"
																		autocomplete="off" />
																	<div class="error " id="districterror"></div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="state" class="col-sm-4 required-label">State</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.state" type="text"
																		class="form-control" id="state" name="state"
																		autocomplete="off" />
																	<div class="error " id="stateerror"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="country" class="col-sm-4 required-label">Country</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.country" type="text"
																		class="form-control" id="country" name="country"
																		autocomplete="off" />
																	<div class="error " id="countryerror"></div>
																</div>
															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="pincode" class="col-sm-4 required-label">Pin
																	Code</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.pincode" type="text"
																		class="form-control" id="pincode" name="pincode"
																		autocomplete="off" />
																	<div class="error " id="pincodeerror"></div>
																</div>
															</div>
														</div>

														<div class="row">

															<div class="form-group col-sm-6">
																<label for="telephoneno" class="col-sm-4 required-label">Telephone
																	No</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.telephone" type="text"
																		class="form-control" id="telephone" name="telephone"
																		autocomplete="off" />
																	<div class="error " id="telephoneerror"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="mobileno" class="col-sm-4 ">Mobile
																	No</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.mobileno" type="text"
																		class="form-control" name="mobileno"
																		autocomplete="off" id="mobileno" />
																	<div class="error " id="mobilenoerror"></div>
																</div>
															</div>
														</div>
														<div class="row">

															<div class="form-group col-sm-6">
																<label for="fax" class="col-sm-4 ">Fax</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.fax" type="text"
																		class="form-control" name="fax" id="fax"
																		autocomplete="off" />
																	<div class="error " id="faxerror"></div>
																</div>
															</div>
															<div class="form-group col-sm-6">
																<label for="website" class="col-sm-4 ">Website</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.website" type="text"
																		class="form-control" name="website" id="website"
																		autocomplete="off" />
																	<div class="error " id="websiteerror"></div>
																</div>
															</div>

														</div>
														<div class="row">
															<div class="form-group col-sm-6">
																<label for="emailid" class="col-sm-4 required-label">Email
																	Id</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.emailid" type="text"
																		class="form-control" name="emailid" id="emailid"
																		autocomplete="off" />
																	<div class="error " id="emailiderror"></div>
																</div>
															</div>

															<div class="form-group col-sm-6">
																<label for="emailid" class="col-sm-4 required-label">Alternate
																	Email Id</label>
																<div class="col-sm-8">
																	<form:input path="portaluser.alternateemailid"
																		type="text" autocomplete="off" class="form-control"
																		name="alternateemailid" id="alternateemailid" />
																	<div class="error " id="alternateemailiderror"></div>
																</div>
															</div>
														</div>
													</div>
													<!-- /.tab-pane  -->
													<!-- /.Second tab end -->

												</div>
											</div>
											<!-- Custom form ?End-->
										</div>
										<!-- /.box -->
										<div class="box-footer" id="addbtn">
											<button id="next" type="button"
												class="btn btn-primary pull-left  btn-next">Next</button>
											<button id="previous" type="button"
												class="btn btn-primary pull-left  btn-prev">Previous</button>
											<!-- </div>
									<div class="box-footer" id="submit"> -->
											<button id="submit" type="submit"
												class="btn btn-primary pull-right  ">Submit</button>
										</div>
									</form:form>
								</div>
							</div>
							<!-- /.form -->
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>

		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->



	<!-- Tab Script -->

	<spring:url value="/resources/js/user_tabs.js" var="tabscriptjs" />
	<script src="${tabscriptjs}"></script>


	<script>
		$(document).ready(function() {

			//Initialize Select2 Elements
			$('.select2').select2();

			$('#submit').hide();
			$('#previous').hide();
			$('#next').show();
			$('#sarAuthorityDiv').hide();
			$('#addressdetailstab').click(function() {
				$('#submit').show();
				$('#previous').show();
				$('#next').hide();
			});

			$('#userdetailstab').click(function() {

				$('#submit').hide();
				$('#previous').hide();
				$('#next').show();
			});
			var category = "${category}";
			if (category == "CoastGuard") {
				$('#sarAuthorityDiv').show();
			}

		});
	</script>

</body>
</html>