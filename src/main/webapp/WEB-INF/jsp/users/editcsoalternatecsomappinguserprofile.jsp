<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="">
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>CSO AlternateCSO Mapping</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Title Page</title>
<!-- Bootstrap CSS -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">

<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">


<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="icon" href="../resources/img/dgslogo.ico">
<!-- jQuery -->

<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- Select2 -->
<%-- <spring:url value="/bower_components/select2/dist/css/select2.min.css"
	var="dropdowncss" />
<link rel="stylesheet" href="${dropdowncss}"> --%>
<!-- Select2 -->
<%-- <spring:url
	value="/bower_components/select2/dist/js/select2.full.min.js"
	var="dropdownjss" />
<script src="${dropdownjss}"></script> --%>
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">

<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
<script src="${adminlteminjs}"></script>

<spring:url value="/users/persisteditcsomappinguserprofile" var="persistcsomapping" />
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>


		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">
						<div class="message"></div>
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">CSO-Alternate CSO Mapping</h3>
							</div>

							<div>
								<form:form id="csoAcsoMapping" action="${persistcsomapping}"
									method="post" name="csoAcsoMapping" modelAttribute="portaldto">
									<div class="box-body">
										<%-- <form:input path="loginId" type="hidden" class="form-control"
											id="loginId" /> --%>

										<div class="col-sm-6">
											<div class="row">
												<div class="form-group col-sm-10">
													<form:input path="portaluser.loginId" type="hidden"
														class="form-control" value="${portaluser.loginId }"
														id="loginId" />
													<table id="csoTable"
														class="table table-bordered table-hover dataTable">
														<thead>
															<tr>

																<th>CSO</th>
																<th>Alternate CSO</th>

															</tr>
														</thead>
														<tbody>
															<c:forEach items="${currentcsoList}" var="cso"
																varStatus="status">
																<tr id="portalcsoList">

																	<form:input
																		path="currentcsoList[${status.index}].csoidseq"
																		value="${cso.csoidseq }" hidden="true" />
																	<form:input
																		path="currentcsoList[${status.index}].name"
																		value="${cso.name }" hidden="true" />	

																	<td>${cso.name }</td>



																	<td><form:select id="parentCsoid"
																			path="currentcsoList[${status.index}].parentCsoid"
																			class="form-control ">

																			<form:option value="">select
																							</form:option>
																			<c:forEach items="${currentcsoList}" var="cso1"
																				varStatus="status1">
																				<c:choose>
																					<%-- <c:when test="${role.flag eq true}"> --%>
																					<c:when test="${cso.csoidseq == cso1.csoidseq}">

																					</c:when> 
																					<c:when test="${cso.parentCsoid == cso1.csoidseq}">
																						<form:option selected="true"
																							value="${cso1.csoidseq}">${cso1.name}
																							</form:option>
																					</c:when>
																					<c:otherwise>
																						<form:option value="${cso1.csoidseq}">${cso1.name}
																							</form:option>
																					</c:otherwise>
																				</c:choose>









																			</c:forEach>
																		</form:select></td>
																</tr>

															</c:forEach>
														</tbody>
													</table>
												</div>

											</div>
										</div>
									</div>
									<!-- /.box-body -->

									<div class="box-footer">
										<button type="submit" class="btn btn-primary pull-left">Submit</button>
									</div>
								</form:form>
							</div>
							<!-- right column -->
							<!--/.col (right) -->
						</div>
						<!-- /.col-lg-10 -->
					</div>
					<!-- /.col-lg-10 -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
				<!-- row -->
			</section>
			<!-- /.content -->
		</div>
	</div>
	<!-- /.content-wrapper -->







	<jsp:include page="../footer.jsp"></jsp:include>

	<!-- Control Sidebar -->
	<jsp:include page="../rightbar.jsp"></jsp:include>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>

	<!-- ./wrapper -->




	<!-- ends -->

	
</body>

</html>
