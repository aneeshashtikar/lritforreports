<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SAR SURPIC Report</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
	<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>


<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>





<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
 --%>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<spring:url value="/requests/persistflagrequest"
	var="persistflagrequest" />
<spring:url value="/requests/getshipdetails" var="getshipdetails" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"

	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>



<spring:url value="/report/getSarSurpicRequestReport" var="getSarSurpicRequestReport" />
<spring:url value="/report/getSarSurpicRequestResponse" var="getSarSurpicRequestResponse" />
<style>
body .datepicker{
z-index: 10000 !important;
}
.dataTables_wrapper .dt-buttons { float:none; text-align:center; }
</style>
	

<script>
$(document).ready(function () {

	 $('#startdate').datepicker({
			format: 'yyyy-mm-dd',
			autoclose : true
		});
		$('#enddate').datepicker({
			format: 'yyyy-mm-dd',
			autoclose : true
	}); 
	 

	//function standingOrderFormSubmit() 
	 $('#sarSurpicbutton').click(function() {
		$('#standingOrderTable').css("display", "");
		var sarauthority = document.getElementById('sarauthority').value;
		var startdate = document.getElementById('startdate').value;
		var startD = new Date(startdate);
		
		var enddate = document.getElementById('enddate').value;
		var endD = new Date(enddate);
		
		var requesttype = document.getElementById('sarrequesttype').value;
		
		var dtable = $('#standingOrderTable').DataTable({
			dom : 'Bfrtip',
			"destroy": true,
			"ajax" : {
				"url" : "${getSarSurpicRequestReport}",
				"type" : "POST",
				"dataSrc" : "",
				 "dataType" : "json",
				 "data" : {
					'sarAuthority' : sarauthority,
					'startDate'		: startD,
					'endDate'		:	endD,
					'sarRequestType':	requesttype
				}
				 
			},
			"responsive" : {
				"details" : {
					"type" : 'column',
					"target" : 'tr'
				}
			},
			"columnDefs": [ {
			      "targets": 1,
			      "title" : "Message ID",
			      "render"	: function (data, type, row, meta) {
				   var message = "'"+data+"'";
					return '<a href="#" onclick="sarRequestResponses('+message+');" data-toggle="modal" data-target="#myModal">'+data+'</a>'; 
						/* console.log('column data:='+data); */
		      }
	  } ],
			"createdRow": function (row, data, index) {
			    var info = dtable.page.info();
			    $('td', row).eq(0).html(index + 1 + info.page * info.length);
			},
			"buttons" : [ 'csv', 'excel', 'pdf' ]
			});

		dtable.on( 'order.dt search.dt', function () {
			dtable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			      cell.innerHTML = i+1;
			      dtable.cell(cell).invalidate('dom'); 
			  } );
			} ).draw(); 
		
	});	
}); 

function sarRequestResponses(data)
{
	//console.log("data="+data);
	 var dTable1 = $('#sarSurpicDetails2').DataTable( {
			
			dom : 'Bfrtip',
			"destroy": true,
			"ajax" : {
				"url" : "${getSarSurpicRequestResponse}",
				"type" : "POST",
				"dataSrc" : "",
				 "dataType" : "json",
				 "data" : {
					 'messageId' : data
					 }
			},
			
			"columns" : [ {
				"data" : ""
			}, {
				"data" : "receiptMessageId"
			}, {
				"data" : "message"
			}, {
				"data" : "resTimestamp"
			},  {
				"data" : "originator"
			}, {
				"data" : "destination"
			} ],
		
			
			"responsive" : {
				"details" : {
					"type" : 'column',
					"target" : 'tr'
				}
			},
			"buttons" : [ 'csv', 'excel', 'pdf' ]
			});
		
	 dTable1.on( 'order.dt search.dt', function () {
			dTable1.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			      cell.innerHTML = i+1;
			      dTable1.cell(cell).invalidate('dom'); 
			  } );
			} ).draw(); 
}
</script>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">SAR SURPIC Position Report</h3>
							</div>
							<!-- /.box-header -->
						</div>
						<div class="box-body">
						<div class="row">
							<div class="form-group col-sm-6">
							<label for="startdate" class="col-sm-4">Start Date</label>
							<div class="col-sm-8">
							<div class="input-group date">
							<div class="input-group-addon ">
							<i class="fa fa-calendar datepicker"></i>
							</div>
							<input type="text" class="form-control pull-right" id="startdate" name="startdate" autocomplete="off">
							</div>
							</div>
							</div>
							<div class="form-group col-sm-6">
							<label for="end" class="col-sm-4">End Date</label>
							<div class="col-sm-8">
							<div class="input-group date">
							<div class="input-group-addon ">
							<i class="fa fa-calendar datepicker"></i>
							</div>
							<input type="text" class="form-control pull-right" id="enddate" name="enddate" autocomplete="off">
							</div>
							</div>
							</div>
						</div>
							<div class="row">
							<div class="form-group col-sm-6">
									<label class="col-sm-4">Request Type</label> 
									<div class="col-sm-8">
									<select class="form-control" id="sarrequesttype" name="sarrequesttype">
									<option value="Rectangular">Rectangular</option>
									<option value="Circular">Circular</option>
									<option value="Both">Both</option>
									</select>
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label class="col-sm-4">SAR Authority</label> 
									<div class="col-sm-8">
									<select class="form-control" id="sarauthority" name="sarauthority">
									<option value="Our">Our</option>
									<option value="Foreign">Foreign</option>
									<option value="Both">Both</option>
									</select>
									</div>
								</div>
							
							</div>		
							
						</div>
						<div class="box-footer">
								<!-- <input type="button" class="btn btn-primary" id="button"
									onclick="standingOrderFormSubmit(); " value="Send" /> -->
									<input type="button" class="btn btn-primary" id="sarSurpicbutton"
									 value="Send" />
							</div>

						<div class="box">
							<!-- Report Table -->
							
							<div class="box-body">
								<table id="standingOrderTable"
									class="table table-bordered table-striped "
									style="display: none; width: 100%">
									<thead>
										<tr>
											<!-- <th data-data="null" ></th> -->
											<th data-data="" data-default-content="">Sr.No</th>
											<th data-data="messageId" data-default-content="NA" >Message ID</th>
											<th data-data="requestor" data-default-content="NA">Requestor</th>
											<th data-data="sarCirArea" data-default-content="-">SAR Circular Area</th>
											<th data-data="sarRecArea" data-default-content="-">SAR Rectangular Area</th>
										</tr>
									</thead>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
						</div>
					</div>
						<div class="modal fade" id="myModal" role="dialog">
											<div class="modal-dialog" style="width: 95%">

												<div class="modal-content">
													<div class="modal-header">
														<h4>Responses</h4>
													</div>
													<div class="modal-body">
														<div class="box-body">
															<table id="sarSurpicDetails2"
																class="table table-bordered table-striped "
																style="width: 100%">
																<thead>
																	<tr>
																		<th data-data="" data-default-content="">Sr. No.</th>
																		<th data-data="receiptMessageId" data-default-content="NA">Receipt Message ID
																			Number</th>
																		<th data-data="message" data-default-content="NA">Message</th>
																		<th data-data="resTimestamp" data-default-content="NA">TimeStamp</th>
																		<th data-data="originator" data-default-content="NA">Originator</th>
																		<th data-data="destination" data-default-content="NA">Destination</th>
																	</tr>
																</thead>
															</table>
														</div>

													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default"
															data-dismiss="modal">Close</button>
													</div>
												</div>
											</div>
										</div>
					
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

	<jsp:include page="../footer.jsp"></jsp:include>


</body>
</html>