<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>CSP Log Report</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
	<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->

<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>


<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>





<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
 --%>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"

	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<spring:url value="/report/getCspLogReport" var="getCspLogReport" />
	



<style>
body .datepicker{
z-index: 10000 !important;
}
.dataTables_wrapper .dt-buttons { float:none; text-align:right; }

</style>
	

<script>
$(document).ready(function () {

	$('#startdate').daterangepicker({
		startDate: new Date(),
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		singleDatePicker : true,
		autoclose : true
	});
	
	$('#enddate').daterangepicker({
		startDate: new Date(),
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		singleDatePicker : true,
		autoclose : true
}); 
 
	 

	//function standingOrderFormSubmit() 
	 $('#csplogreportbtn').click(function() {
		$('#cspLogReportTable').css("display", "");
		var startdate = document.getElementById('startdate').value;
		var startD = new Date(startdate);
		var enddate = document.getElementById('enddate').value;
		var endD = new Date(enddate);
		
		var dtable = $('#cspLogReportTable').DataTable({
			dom : 'lfrtip',
			"destroy": true,
			"ajax" : {
				"url" : "${getCspLogReport}",
				"type" : "POST",
				 "dataType" : "json",
				 "data" : {
					'startDate'		: startD,
					'endDate'		:	endD,
				},
				"dataSrc" : function(jsonData){
					
					var returned_data = new Array();
					
					for (var i = 0; i < jsonData.length; i++) {
						console.log("lat= "+jsonData[i].latitude+"=log="+jsonData[i].longitude);
						var latLogData = new Array();
						latLogData.push(jsonData[i].latitude);
						latLogData.push(jsonData[i].longitude);
						
						var convertedData = ol.coordinate.toStringHDMS(latLogData);
						var noSpaceStr = convertedData.replace(/\s/g, '');
						var index = noSpaceStr.indexOf("N");
						if (index == -1)
							index = noSpaceStr.indexOf("S");
						console.info('converted data= '+ (noSpaceStr.substring(0, index + 1)).trim()
								+ '='+ (noSpaceStr.substring(index + 1,noSpaceStr.length)).trim());
						var convertedLat = (noSpaceStr.substring(0, index + 1)).trim();
						var convertedLog = (noSpaceStr.substring(index + 1,noSpaceStr.length)).trim();
						
						returned_data.push({
								'imoNo'		:	jsonData[i].imoNo,
								'mmsiNo'	:	jsonData[i].mmsiNo,
								'shipName'	:	jsonData[i].shipName,
								'shipBorneEquipmentId'	:	jsonData[i].shipBorneEquipmentId,
								'timestamp1'	:	jsonData[i].timestamp1,
								'timestamp4'	:	jsonData[i].timestamp4,
					          	'latitude' : convertedLat,
					          	'longitude' : convertedLog,
					        })
					}
					return returned_data;

				},
			},
			
			"responsive" : {
				"details" : {
					"type" : 'column',
					"target" : 'tr'
				}
			},
			"columns" : [ {
				"data" : ""
			}, {
				"data" : "imoNo"
			}, {
				"data" : "mmsiNo"
			}, {
				"data" : "shipName"
			}, {
				"data" : "shipBorneEquipmentId"
			}, {
				"data" : "timestamp1"
			}, {
				"data" : "timestamp4"
			},{
				"data" : "latitude"
			},{
				"data" : "longitude"
			},
			 ],
			
			"createdRow": function (row, data, index) {
			    var info = dtable.page.info();
			    $('td', row).eq(0).html(index + 1 + info.page * info.length);
			},
			
			// "buttons" : [ 'csv', 'excel', 'pdf' ] 
			});

		var buttons = new $.fn.dataTable.Buttons(dtable, {
		     buttons: [
		       'excel',
		       'csv',
		       'pdf'
		    ]
		}).container().appendTo($('#buttons1'));

		    dtable.on( 'order.dt search.dt', function () {
			 dtable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			      cell.innerHTML = i+1;
			      dtable.cell(cell).invalidate('dom'); 
			  } );
			} ).draw(); 
		
	});	
}); 
</script>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">CSP Log Report</h3>
							</div>
							<!-- /.box-header -->
						</div>
						<div class="box-body">
					<div class="row">
								<div id="starttime" class="form-group col-sm-6">
									<label for="starttimedate" class="col-sm-4 control-label">From
										Date : </label>
									<div class="col-sm-8">
										<div class="input-group date">
											<div class="input-group-addon ">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="startdate"
												class="form-control pull-right" autocomplete="off" required />
										</div>
									</div>
								</div>
								<div id="endtime" class="form-group col-sm-6">
									<label for="endtimedate" class="col-sm-4 control-label">To
										Date: </label>
									<div class="col-sm-8">
										<div class="input-group date">
											<div class="input-group-addon ">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="enddate"
												class="form-control pull-right" autocomplete="off" required />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
									<input type="button" class="btn btn-primary" id="csplogreportbtn"
									 value="Get Report" />
							</div>

						<div class="box">
							<!-- Report Table -->
							
							<div class="box-body">
								<table id="cspLogReportTable"
									class="table table-bordered table-striped "
									style="display: none; width: 100%">
									<thead>
										<tr>
											<!-- <th data-data="null" ></th> -->
											<th data-data="" data-default-content="">Sr. No.</th>
											<th data-data="imoNo" data-default-content="-" >IMO No.</th>
											<th data-data="mmsiNo" data-default-content="-">MMSI No.</th>
											<th data-data="shipName" data-default-content="-">Ship Name</th>
											<th data-data="shipBorneEquipmentId" data-default-content="-">ShipBorne Equipment ID</th>
											<th data-data="timestamp1" data-default-content="-">Timestamp1</th>
											<th data-data="timestamp4" data-default-content="-">Timestamp4</th>
											<th data-data="latitude" data-default-content="-">Latitude</th>
											<th data-data="longitude" data-default-content="-">Longitude</th>
										</tr>
									</thead>
								</table>
								
								<div class="col-sm-5"></div>
								<div id="buttons1" class="col-sm-4" align="center"></div>
								<div class="col-sm-4"></div>
							</div>
							<!-- /.box-body -->
						</div>
					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
						</div>
					</div>
					
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

	<jsp:include page="../footer.jsp"></jsp:include>


</body>
</html>