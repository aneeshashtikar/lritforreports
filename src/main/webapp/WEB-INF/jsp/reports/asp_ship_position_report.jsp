<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ASP Ship Position Report</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>

<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>


<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>





<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
 --%>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<spring:url value="/requests/persistflagrequest"
	var="persistflagrequest" />
<spring:url value="/requests/getshipdetails" var="getshipdetails" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>



<spring:url value="/report/getAspShipPositionReport"
	var="getAspShipPositionReport" />
<spring:url value="/report/getAspShipPositions"
	var="getAspShipPositions" />

<style>
body .datepicker {
	z-index: 10000 !important;
}

.dataTables_wrapper .dt-buttons { float:none; text-align:center; }
</style>


<script>
$(document).ready(function () {

	 $('#startdate').daterangepicker({
		 	startDate: new Date(),
			timePicker : true,
			timePicker24Hour : true,
			locale : {
				format : 'MM/DD/YYYY HH:mm:ss'
			},
			singleDatePicker : true,
			autoclose : true
		});
		
		$('#enddate').daterangepicker({
			startDate: new Date(),
			timePicker : true,
			timePicker24Hour : true,
			locale : {
				format : 'MM/DD/YYYY HH:mm:ss'
			},
			singleDatePicker : true,
			autoclose : true
	}); 
	 

	//function standingOrderFormSubmit() 
	 $('#aspPositionBtn').click(function() {
		 
		$('#aspPositionReportTable').css("display", "");
		
		var startdate = document.getElementById('startdate').value;
		var startD = new Date(startdate)
		var enddate = document.getElementById('enddate').value;
		var endD = new Date(enddate)
		console.log(startD+" and "+endD);
		
		var dtable = $('#aspPositionReportTable').DataTable({
			dom :'lfrtip',
			"destroy": true,
			"ajax" : {
				"url" : "${getAspShipPositionReport}",
				"type" : "POST",
				"dataSrc" : "",
				  "dataType" : "json",
				 "data" : {
					'startDate'		: startD,
					'endDate'		:	endD,
				}
				 
			},
			"responsive" : {
				"details" : {
					"type" : 'column',
					"target" : 'tr'
				}
			},
			"columnDefs": [ {
			      "targets": 1,
			      "title" : "IMO NO",
			      "render"	: function (data, type, row, meta) {
				   var imoNo = "'"+data+"'";
				   var mmsiNo = "'"+row.mmsiNo+"'";
				   var shipName =  "'"+row.shipName+"'";
				   var seId =  "'"+row.shipborneEquId+"'";
				   //console.log("imoNo="+imoNo+"-"+"mmsiNo="+mmsiNo+"-"+"shipName="+shipName+"-"+"seId="+seId);
				   return '<a href="#" onclick="getShipPosition('+imoNo+','+mmsiNo+','+shipName+','+seId+')" data-toggle="modal" data-target="#myModal">'+data+'</a>'; 
		      }
	  },
	  {
          "searchable": false,
          "orderable": false,
          "targets": 0
        }
	   ],
			"createdRow": function (row, data, index) {
			    var info = dtable.page.info();
			    $('td', row).eq(0).html(index + 1 + info.page * info.length);
			},
			});

		var buttons = new $.fn.dataTable.Buttons(dtable, {
		     buttons: [
		       'excel',
		       'csv',
		       'pdf'
		    ]
		}).container().appendTo($('#buttons1'));
		
		 dtable.on( 'order.dt search.dt', function () {
		 dtable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		      cell.innerHTML = i+1;
		      dtable.cell(cell).invalidate('dom'); 
		  } );
		} ).draw(); 
		
	});	
}); 

 function getShipPosition(imono, mmsino, shipname, seid)
{
	 
		var startdate = document.getElementById('startdate').value;
		var startD = new Date(startdate);
		var enddate = document.getElementById('enddate').value;
		var endD = new Date(enddate);

		//console.log("rowData:=" + imono + "-" + mmsino + "-" + shipname + "-"
				//+ seid);
		$('#imono').text(imono);
		$('#shipname').text(shipname);
		$('#mmsino').text(mmsino);
		$('#seid').text(seid);
		$('#startd').text(startdate);
		$('#endd').text(enddate);
		var message = "ASP Ship Position Details[Start Date: "+startdate+" - End Date: "+enddate+"] IMO No: "+imono+" Ship Name: "
		+shipname+" MMSI No: "+mmsino+" Ship Borne Equipment ID: "+seid;
			console.log('final message= '+message);
		var dTable1 = $('#shipPositionDetails2').DataTable({
			dom : 'lfrtip',
			"destroy" : true,
			"ajax" : {
				"url" : "${getAspShipPositions}",
				"type" : "POST",
				"dataType" : "json",
				"dataSrc" : function(jsonData){
					
							var returned_data = new Array();
							
							for (var i = 0; i < jsonData.length; i++) {
								console.log("lat= "+jsonData[i].lattitude+"=log="+jsonData[i].longitude); 
								var latLogData = new Array();
								latLogData.push(jsonData[i].lattitude);
								latLogData.push(jsonData[i].longitude);
								
								var convertedData = ol.coordinate.toStringHDMS(latLogData);
								var noSpaceStr = convertedData.replace(/\s/g, '');
								var index = noSpaceStr.indexOf("N");
								if (index == -1)
									index = noSpaceStr.indexOf("S");
								 console.info('converted data= '+ (noSpaceStr.substring(0, index + 1)).trim()
										+ '='+ (noSpaceStr.substring(index + 1,noSpaceStr.length)).trim()); 
								var convertedLat = (noSpaceStr.substring(0, index + 1)).trim();
								var convertedLog = (noSpaceStr.substring(index + 1,noSpaceStr.length)).trim();
								
								returned_data.push({
							          'lattitude' : convertedLat,
							          'longitude' : convertedLog,
							          'timestamp1'  : jsonData[i].timestamp1,
									  'timestamp2'  : jsonData[i].timestamp2,
									  'timestamp3'  : jsonData[i].timestamp3,
									  'timestamp4'  : jsonData[i].timestamp4,
							        })
							}
							return returned_data;

						},
						"data" : {
							'imoNo' : imono,
							'startDate' : startD,
							'endDate' : endD
						},
					},

					"columns" : [ {
						"data" : ""
					}, {
						"data" : "lattitude"
					}, {
						"data" : "longitude"
					}, {
						"data" : "timestamp1"
					}, {
						"data" : "timestamp2"
					}, {
						"data" : "timestamp3"
					}, {
						"data" : "timestamp4"
					} ],

					"responsive" : {
						"details" : {
							"type" : 'column',
							"target" : 'tr'
						}
					},
				});

		var buttons = new $.fn.dataTable.Buttons(dTable1, {
			
			buttons : [ 
				{
	                extend: 'excel',
	                messageTop: message
	            },
	            {
	                extend: 'csv',
	                messageTop: message
	            },
	            {
	                extend: 'pdf',
	                messageTop: message
	            },
				 ]
		}).container().appendTo($('#buttons2'));

		dTable1.on('order.dt search.dt', function() {
			dTable1.column(0, {
				search : 'applied',
				order : 'applied'
			}).nodes().each(function(cell, i) {
				cell.innerHTML = i + 1;
				dTable1.cell(cell).invalidate('dom');
			});
		}).draw();
	}
</script>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">ASP Ship Position Report</h3>
							</div>
							<!-- /.box-header -->
						</div>
						<div class="box-body">
							<div class="row">
								<div id="starttime" class="form-group col-sm-6">
									<label for="starttimedate" class="col-sm-4 control-label">From
										Date : </label>
									<div class="col-sm-8">
										<div class="input-group date">
											<div class="input-group-addon ">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="startdate"
												class="form-control pull-right" autocomplete="off" required />
										</div>
									</div>
								</div>
								<div id="endtime" class="form-group col-sm-6">
									<label for="endtimedate" class="col-sm-4 control-label">To
										Date: </label>
									<div class="col-sm-8">
										<div class="input-group date">
											<div class="input-group-addon ">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" id="enddate"
												class="form-control pull-right" autocomplete="off" required />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<!-- <input type="button" class="btn btn-primary" id="button"
									onclick="standingOrderFormSubmit(); " value="Send" /> -->
							<input type="button" class="btn btn-primary" id="aspPositionBtn"
								value="Get Report" />
						</div>

						<div class="box">
							<!-- Report Table -->

							<div class="box-body">
								<table id="aspPositionReportTable"
									class="table table-bordered table-striped "
									style="display: none; width: 100%">
									<thead>
										<tr>
											<!-- <th data-data="null" ></th> -->
											<th data-data="" data-default-content="">Sr.No</th>
											<th data-data="imoNo" data-default-content="NA">IMO No.</th>
											<th data-data="mmsiNo" data-default-content="NA">MMSI
												No.</th>
											<th data-data="shipName" data-default-content="NA">Ship
												Name</th>
											<th data-data="shipborneEquId" data-default-content="NA">Shipborne
												Equipment ID</th>
										</tr>
									</thead>
								</table>
								<div class="col-sm-5"></div>
								<div id="buttons1" class="col-sm-4" align="center"></div>
								<div class="col-sm-4"></div>
							</div>
							<!-- /.box-body -->
						</div>
					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog" style="width: 95%">

						<div class="modal-content">
							<div class="modal-header">
								<h4>Detailed Report</h4>
							</div>
							<div class="modal-body">
							<div class="col-md-3"></div>
							<div id="reportinfo" class="col-md-8" align="center">
								<div class="row">
									<div class="form-group col-sm-4" align="left">
										<label for="imono">IMO No:</label>
											<span id="imono"> </span>
									</div>
									<div class="form-group col-sm-4" align="left">
										<label for="shipname">Ship Name: </label>
											<span id="shipname"> </span>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-4" align="left">
										<label for="mmsino">MMSI No:</label>
											<span id="mmsino"> </span>
									</div>
									<div class="form-group col-sm-4" align="left">
										<label for="seid">ShipBorne Equipment ID: </label>
											<span id="seid"> </span>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-4" align="left">
										<label for="startd">Start Date: </label>
											<span id="startd"> </span>
									</div>
									<div class="form-group col-sm-4" align="left">
										<label for="endd">End Date: </label>
											<span id="endd"> </span>
									</div>
								</div>
								</div>
								<div class="col-md-2"></div>
								<div class="box-body">
									<table id="shipPositionDetails2"
										class="table table-bordered table-striped "
										style="width: 100%">
										<thead>
											<tr>
												<th data-data="" data-default-content="">Sr. No.</th>
												<th data-data="lattitude" data-default-content="NA">Latitude</th>
												<th data-data="longitude" data-default-content="NA">Longitude</th>
												<th data-data="timestamp1" data-default-content="NA">Timestamp1</th>
												<th data-data="timestamp2" data-default-content="NA">Timestamp2</th>
												<th data-data="timestamp3" data-default-content="NA">Timestamp3</th>
												<th data-data="timestamp4" data-default-content="NA">Timestamp4</th>
											</tr>
										</thead>
									</table>
									
									<div class="col-sm-5"></div>
								<div id="buttons2" class="col-sm-4" align="center"></div>
								<div class="col-sm-4"></div>
								</div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

	<jsp:include page="../footer.jsp"></jsp:include>


</body>
</html>