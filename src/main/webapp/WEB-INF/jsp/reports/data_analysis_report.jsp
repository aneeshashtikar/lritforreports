<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Data Analysis Report</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
	<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>





<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
 --%>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<spring:url value="/requests/persistflagrequest"
	var="persistflagrequest" />
<spring:url value="/requests/getshipdetails" var="getshipdetails" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"

	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<spring:url value="/resources/ol/turf.min.js" var="turfminjs" />
	<script src="${turfminjs}"></script>


<spring:url value="/report/getAspShipPositionReport" var="getAspShipPositionReport" />
<spring:url value="/report/getAspShipPositions" var="getAspShipPositions" />
<spring:url value="/report/getvesseldetails" var="getvesseldetails" />
<spring:url value="/map/getShipIMOByDDPPolygon" var="getShipIMOByDDPPolygon" />
<spring:url value="/map/getShipIMOByCustomPolygon" var="getShipIMOByCustomPolygon" />
<spring:url value="/map/getselecteduserarea" var="getselecteduserarea" />

<spring:url value="/resources/js/shipPositions.js" var="shipPositionsjs" />
<script src="${shipPositionsjs}"></script>

<style>
body .datepicker{
z-index: 10000 !important;
}

/* .dataTables_wrapper .dt-buttons { float:none; text-align:center; } */

</style>
	

<script>
$(document).ready(function () { 
	
	$('#userdefined').hide();
	$('#geographical').hide();
	
	$('#type').change(function() {
		var _type = $('#type').val();
		
		if(_type == 'User_Defined_Area')
		{
			$('#userdefined').show();
			$('#geographical').hide();	
			
			alert('1 inside area name list');
			var _type = $('#type').val();
			$.get("getpolygonOrboundarieslist?type="+_type, function( areaNameList ) {
				//console.log(areaNameList);
				var _areaName="";
				for(var key in areaNameList) {
					_areaName += '<option value= "' + key + '">' + areaNameList[key] + '</option>';
				}
				console.log(_areaName);
				$('#areaName').html('<option value="">Select</option>'+_areaName);
			});
		}
		else if(_type == 'Standing_Order')
		{
			$('#userdefined').hide();
			$('#geographical').hide();
		}
		else if(_type == 'Geographical_Boundaries')
		{
			$('#userdefined').hide();
			$('#geographical').show();	
			
			alert('inside country name list');
			var _type = $('#type').val();	
			$.get("getpolygonOrboundarieslist?type="+_type, function( countryList ) {
				console.log(countryList);
				var _countryName="";
				for(var key in countryList) {
					_countryName += '<option value= "' + key + '">' + countryList[key] + '</option>';
				}
				console.log(_countryName);
				$('#country').html('<option value="">Select</option>'+_countryName);
			});
		}
		else
		{
			$('#userdefined').hide();
			$('#geographical').hide();
		}	
	});
	
	$('#fromdate').daterangepicker({
	 	startDate: new Date(),
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		singleDatePicker : true,
		autoclose : true
	});
	
	$('#todate').daterangepicker({
		startDate: new Date(),
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		singleDatePicker : true,
		autoclose : true
	}); 
	
			
	var resultSet = function() {
		this.item = {};
	};

	var listtable = new resultSet();

	var targetTableId = 'dataAnalysisReportTable';
		
	var dTable1 = function(targetTableId, resultSet, _coordsFormatted, _fromdate, _todate) { 		
		if ($.fn.dataTable.isDataTable(targetTableId)) {
			//$('#dataAnalysisReportTable').DataTable().ajax.reload();
			resultSet.item.ajax.reload();
		} else {
			console.log("10 targetTableId : "+targetTableId);
			console.log("11 resultSet : "+resultSet);
			console.log("12 _coordsFormatted : "+_coordsFormatted);
			console.log("13 _fromdate : "+_fromdate);
			console.log("14 _todate : "+_todate);
		resultSet.item = $('#dataAnalysisReportTable').DataTable({
				dom : 'lfrtip',
				"ajax" : {
					"url" : "${getShipIMOByCustomPolygon}",
					"type" : "GET",
					"dataSrc" : "",
					"data" : function(d) {
						d.PolyCoords = _coordsFormatted;
						d.startDate = _fromdate;
						d.endDate = _todate;
					},
					"error" : function(xhr, error, thrown) {
						window.location = "${errorPage}";
					}
				},
				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'os',
					"selector" : 'td:first-child'
				},
				"order" : [ [ 1, 'asc' ] ],
					"columns" : [ {
						"data" : ""
					}, {
						"data" : "imoNo"
					}, {
						"data" : "mmsiNo"
					}, {
						"data" : "vesselName"
					} ]
				});
	
				$('#selectUser').on('click', function() {
					$.each(table.rows('.selected').data(), function() {
						loginId = this["loginId"];
						name = this["name"];
						//alert(buttonId);
						$(buttonId1).val(loginId);
						$(buttonId2).val(name);
					});
				});
			}
		}
	
	var dTable2 = function(targetTableId, resultSet, _cgLritid, _polygonType, _fromdate, _todate) { 		
		if ($.fn.dataTable.isDataTable(targetTableId)) {
			//$('#dataAnalysisReportTable').DataTable().ajax.reload();
			resultSet.item.ajax.reload();
		} else {
		resultSet.item = $('#dataAnalysisReportTable').DataTable({
				dom : 'lfrtip',
				"ajax" : {
					"url" : "${getShipIMOByDDPPolygon}",
					"type" : "GET",
					"dataSrc" : "",
					"data" : function(d) {
						d.cgLritid = _cgLritid;
						d.polygonType = _polygonType;
						d.startDate = _fromdate;
						d.endDate = _todate;
					},
					"error" : function(xhr, error, thrown) {
						window.location = "${errorPage}";
					}
				},
				"columnDefs" : [ {
					"orderable" : false,
					"className" : 'select-checkbox',
					"targets" : 0,
					"defaultContent" : ""
				} ],
				"select" : {
					"style" : 'os',
					"selector" : 'td:first-child'
				},
				"order" : [ [ 1, 'asc' ] ],
					"columns" : [ {
						"data" : ""
					}, {
						"data" : "imoNo"
					}, {
						"data" : "mmsiNo"
					}, {
						"data" : "vesselName"
					} ]
				});
	
				$('#selectUser').on('click', function() {
					$.each(table.rows('.selected').data(), function() {
						loginId = this["loginId"];
						name = this["name"];
						//alert(buttonId);
						$(buttonId1).val(loginId);
						$(buttonId2).val(name);
					});
				});
			}
		}
	
	
	function getShipDataByCustomPolygonName(startDate,endDate)
	{
		var _areaId = $('#areaName').val();
		 alert("3 "+_areaId);
		console.log('4 inside getShipDataByCustomPolygonName');
		console.log("5 id :"+_areaId+" startdate: "+startDate+" enddate : "+endDate);
		var coordsFormatted = "LINESTRING(";
		$.ajax({
		 	type: "GET",
		    traditional: true,
		    url: "${getselecteduserarea}?id="+_areaId,	
		    async : false,
		    success: function(result){ 
		    console.log("6 "+result);
		    userareas = result;	 
		    for(var i =0 ;i<userareas.length;i++){
	    		 var coords =[];
	    	 	 var polygonshape = userareas[i];
				//var coordsFormatted = "LINESTRING(";
	    	 	if(polygonshape.polygontype ==="polygon"){ 
					var coords = [];
		    		 for(var j =0 ;j<polygonshape.lon.length;j++){
		    			 coords.push([polygonshape.lon[j],polygonshape.lat[j]])
		    			 if(j===polygonshape.lon.length-1)
		    				 coordsFormatted = coordsFormatted + " " + polygonshape.lon[j] + " " + polygonshape.lat[j] + ")";
		    			 else
		    				 coordsFormatted = coordsFormatted + " " + polygonshape.lon[j] + " " + polygonshape.lat[j] + ",";
		    		 }		 
		    		 
		    	 	console.info('polygon coords: ' + coords);
		    	 	console.info('coordsFormatted: ' + coordsFormatted);
		    	 	console.log("7 coords : "+coords);
		    	 	console.log('8 coordsFormatted: ' + coordsFormatted);
		   	 	}
		   	 	else if(polygonshape.polygontype ==="rectangle")
		   	 	{
		   	 		var coords = [[polygonshape.lon,polygonshape.lat],[polygonshape.lon,polygonshape.latOffset],[polygonshape.longOffset,polygonshape.latOffset],[polygonshape.longOffset,polygonshape.lat],[polygonshape.lon,polygonshape.lat]]
		   	 		
		   	 	    coordsFormatted = "ST_GeomFromText('LINESTRING(" +  polygonshape.lon + " " + polygonshape.lat + "," +  polygonshape.lon + " " + polygonshape.latOffset + "," +
		   	 	                       polygonshape.longOffset + " " + polygonshape.latOffset + "," + polygonshape.longOffset + " " + polygonshape.lat + ")')";
		   	 	   
		   	 		console.info('rectangle coords: ' + coords);
		   	 		console.info('coordsFormatted: ' + coordsFormatted);
		   	 	console.log("7 coords : "+coords);
		   	 console.log('8 coordsFormatted: ' + coordsFormatted);
		   	 	}
		   	 	else if(polygonshape.polygontype ==="circle"){ 
		   	 	 		 
				  var options = {steps: 360, units: 'miles', options: {}}; //degrees
				  polygonshape.radius = polygonshape.radius * 1.1507;
				  _areaId
				  var circle = turf.circle([polygonshape.lon,polygonshape.lat], polygonshape.radius, options);
				  var coordsTemp = (circle.geometry.coordinates).split(",");
				  for(var j =0 ;j<coordsTemp.length;j=j+2){
					  if(j===coordsTemp.length-2)
		    				 coordsFormatted = coordsFormatted + " " + coordsTemp[j] + " " + coordsTemp[j+1] + ")'";
		    			 else
		    				 coordsFormatted = coordsFormatted + " " + coordsTemp[j] + " " + coordsTemp[j+1] + ",";
				  }
				   console.info('circle coords: ' + circle.geometry.coordinates);
				   console.info('coordsFormatted: ' + coordsFormatted);
				   console.log("7 coords : "+coords);
				   console.log('8 coordsFormatted: ' + coordsFormatted);
		   	 	}
	    	 	console.log("9 return "+coordsFormatted);
				dTable1('#'+targetTableId, listtable, coordsFormatted, startDate, endDate);
	    	 }		 
		    
		    }
		});
		return coordsFormatted;
	}
	
	
	
	
	//function standingOrderFormSubmit() 
	 $('#dataAnalysisReport').click(function() {
		 
		 alert('2 inside dataAnalysisReport click');
		 var _fromD = $('#fromdate').val();
		 var _fromdate = new Date(_fromD);
		 var _toD = $('#todate').val();
		 var _todate = new Date(_toD);
		 var _type = $('#type').val();
		 
		 if(_type == 'User_Defined_Area')
		 {
			 //var _areaId = $('#areaName').val();
			 //alert("3 "+_areaId);	
			 //url = '';
			 //var _coordsFormatted = new Array();
			 //_coordsFormatted = getShipDataByCustomPolygonName(_areaId,_fromdate,_todate);
			 //console.log("9 return "+_coordsFormatted);
			 //dTable1('#'+targetTableId, listtable, _coordsFormatted, _fromdate, _todate);
			 getShipDataByCustomPolygonName(_fromdate,_todate);
		 }
		 else if(_type == 'Geographical_Boundaries')
		 {
			 var _cgLritid = $('#country').val();
			 var _polygonType = $('#geographicalboundaries').val();
			 dTable2('#'+targetTableId, listtable, _cgLritid, _polygonType, _fromdate, _todate);
		 }
		 else
		 {
			 alert('do nothing');
		 }		
	});		
}); 


</script>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Data Analysis Report</h3>
							</div>
							<!-- /.box-header -->
						</div>
						<div class="box-body">
							<div class="row">
								<div class="form-group col-sm-6">
									<label for="startdate" class="col-sm-4">From Date</label>
									<div class="col-sm-8">
										<div class="input-group date">
											<div class="input-group-addon ">
												<i class="fa fa-calendar datepicker"></i>
											</div>
											<input type="text" class="form-control pull-right" id="fromdate" name="fromdate" autocomplete="off">
										</div>
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label for="end" class="col-sm-4">To Date</label>
									<div class="col-sm-8">
										<div class="input-group date">
											<div class="input-group-addon ">
												<i class="fa fa-calendar datepicker"></i>
											</div>
											<input type="text" class="form-control pull-right" id="todate" name="todate" autocomplete="off">
									</div>
									</div>
								</div>
							</div>	
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="type" class="col-sm-4">Type</label>
								<div class="col-sm-8">
									<select class="form-control" id="type" name="type">
										<option value="" selected="selected">Select</option>
										<option value="User_Defined_Area">User Defined Area</option>
										<option value="Standing_Order">Standing Order</option>
										<option value="Geographical_Boundaries">Geographical Boundaries</option>
									</select>
								</div>
							</div>
						</div>
						
						<div id="userdefined" onload="findAreaNames()">
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="areaName" class="col-sm-4">Area Name</label>
								<div class="col-sm-8">
									<select class="form-control" id="areaName" name="areaName">
										<option value="" selected="selected"></option>
										<%-- <c:forEach var="areaName" items="${areaNameList}">
											<option value="${areaName}"></option>
										</c:forEach> --%>
									</select>
								</div>
							</div>
						</div>
						</div>
						<div id="geographical" onload="findCountryNames()">
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="country" class="col-sm-4">Country</label>
								<div class="col-sm-8">
									<select class="form-control" id="country" name="country">
										<option value="" selected="selected"></option>
										<%-- <c:forEach var="country" items="${countryList}">
											<option value="${country}"></option>
										</c:forEach> --%>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="geographicalboundaries" class="col-sm-4">Geographical Boundaries</label>
								<div class="col-sm-8">
									<select class="form-control" id="geographicalboundaries" name="geographicalboundaries">
										<option value="" selected="selected">Select</option>
										<option value="CoastalSea" >Coastal Boundaries (1000 nm)</option>
										<option value="CustomCoastal" >Custom Coastal Boundaries</option>
										<option value="TerritorialSea" >Territorial Boundaries</option>
										<option value="InternalWaters" >Internal Water Boundaries</option>
									</select>
								</div>
							</div>
						</div>
						</div>
						
						<div class="row">
							<div class="form-group col-sm-6">
								<label for="imoNo" class="col-sm-4">IMO No</label>
								<div class="col-sm-8">
									<input type="text" class="form-control pull-right" id="imoNo" name="imoNo" autocomplete="off">
								</div>
							</div>
						</div>

						</div>
						<div class="box-footer">
								<!-- <input type="button" class="btn btn-primary" id="button"
									onclick="standingOrderFormSubmit(); " value="Send" /> -->
									<input type="button" class="btn btn-primary" id="dataAnalysisReport"
									 value="Get Report" />
							</div>

						<div class="box">
							<!-- Report Table -->
							
							<div class="box-body">
								<table id="dataAnalysisReportTable"
									class="table table-bordered table-striped "
									style="display: none; width: 100%">
									<thead>
										<tr>
											<!-- <th data-data="null" ></th> -->
											<th data-data="" data-default-content="">Sr.No</th>
											<th data-data="imoNo" data-default-content="NA" >IMO No.</th>
											<th data-data="mmsiNo" data-default-content="NA">MMSI No.</th>
											<th data-data="vesselName" data-default-content="NA">Ship Name</th>
											<!-- <th data-data="shipborneEquId" data-default-content="NA">Shipborne Equipment ID</th> -->
										</tr>
									</thead>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
					</div>
						<div class="modal fade" id="myModal" role="dialog">
											<div class="modal-dialog" style="width: 95%">

												<div class="modal-content">
													<div class="modal-header">
														<h4>Responses</h4>
													</div>
													<div class="modal-body">
														<div class="box-body">
															<table id="shipPositionDetails2"
																class="table table-bordered table-striped "
																style="width: 100%">
																<thead>
																	<tr>
																		<th data-data="" data-default-content="">Sr. No.</th>
																		<th data-data="lattitude" data-default-content="NA">Latitude</th>
																		<th data-data="longitude" data-default-content="NA">Longitude</th>
																		<th data-data="timestamp1" data-default-content="NA">Timestamp1</th>
																		<th data-data="timestamp2" data-default-content="NA">Timestamp2</th>
																		<th data-data="timestamp3" data-default-content="NA">Timestamp3</th>
																		<th data-data="timestamp4" data-default-content="NA">Timestamp4</th>
																	</tr>
																</thead>
															</table>
														</div>

													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default"
															data-dismiss="modal">Close</button>
													</div>
												</div>
											</div>
										</div>
					
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

	<jsp:include page="../footer.jsp"></jsp:include>


</body>
</html>