<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include page="../finalHeader.jsp">
	<jsp:param name="titleName" value="View Journal Extract " />
</jsp:include>

<spring:url var="getJournalExtractLogs"
	value="/ide/getJournalExtractLogs"></spring:url>

<spring:url var="viewjournalextract" value="/ide/viewJournalExtractPost"></spring:url>

<script type="text/javascript">
	function listOfJournalExtractLogs() {
		showTable();
	}
	var dtable = new resultSet();

	function showTable() {
		$('#listOfJournalExtractLogs').css("display", "");

		var periodFrom = document.getElementById('from').value;
		//alert(periodFrom);
		var periodTill = document.getElementById('to').value;
		//alert(periodTill); 
		var dtable = $('#listOfJournalExtractLogs').DataTable({

			dom : 'lBfrtip',
			"ajax" : {
				"url" : "${getJournalExtractLogs}",
				"type" : "POST",
				"data" : {
					'from' : periodFrom,
					'to' : periodTill
				},
				"dataSrc" : ""
			},
			"columns" : [ {
				"data" : "id",
				'render' : function(data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			}, {
				"data" : "journalExtractZip"
			}, {
				"data" : "source"
			}, {
				"data" : "destination"
			}, {
				"data" : "fromDate"

			}, {
				"data" : "toDate"
			}, {
				"data" : "loadedOn"
			} ],
			"responsive" : {
				"details" : {
					"type" : 'column',
					"target" : 'tr'
				}
			},
			"columnDefs" : [ {
				"className" : 'control',
				"targets" : 0
			} ],

			"bDestroy" : true

		});

	}
</script>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->


				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Journal Extract</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<!-- <div class="row"> -->
					<!-- <div class="col-md-10"> -->
					<div class="container col-md-12"></div>
					<form:form class="form-horizontal" id="viewJournalExtract"
						action="${viewjournalextract}" method="POST">
						<div class="box-body">


							<!-- FOURTH ROW -->
							<div class="row">
								<div class="form-group col-sm-6">
									<label for="from" class="col-sm-4">From</label>
									<div class="col-sm-8">
										<input type="text" class="form-control pull-right datepicker" placeholder ="From Date"
											id="from" name="from">
									</div>
								</div>
								<div class="form-group col-sm-6">
									<label for="to" class="col-sm-4">To</label>
									<div class="col-sm-8">
										<input type="text" class="form-control pull-right datepicker" placeholder ="To Date"
											id="to" name="to">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<label class="col-sm-4"></label>
								<form:errors path="journalextractlog.fromdate" class="col-sm-8"
									cssClass="error" />
							</div>
							<div class="col-sm-8">
								<label class="col-sm-5"></label>
								<form:errors path="journalextractlog.todate" class="col-sm-4"
									cssClass="error" />
							</div>
						</div>

						<!-- EIGHTEENTH ROW -->


						<!-- /.tab-pane -->
			

				<div class="box-footer">
					<input type="button" class="btn btn-primary" id="listButton"
						onclick="listOfJournalExtractLogs(); return false;" value="Search" />

				</div>
				</form:form>
				<div class="box">

					<!-- Report Table -->

					<div class="box-body">

						<table id="listOfJournalExtractLogs"
							class="table table-bordered table-striped "
							style="display: none; width: 100%">
							<thead>
								<tr>
									<!-- <th data-data="null" ></th> -->
									<th data-data="null" data-default-content="">Sr. No.</th>

									<th data-data="journalExtractZip" data-default-content="NA">Journal
										Extract Zip</th>
									<!-- <th data-data="journalExtractCsv" data-default-content="NA">Journal Extract CSV</th> -->
									<th data-data="source" data-default-content="NA">Source
										Data Centre</th>

									<th data-data="destination" data-default-content="NA">Destination
										Data Centre</th>

									<th data-data="fromDate" data-default-content="NA">From
										Date</th>
									<th data-data="toDate" data-default-content="NA">To Date</th>
									<th data-data="loadedOn" data-default-content="NA">Loaded
										On</th>
								</tr>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->

				</div>

				<!-- /.form -->
			</div>
			<!--/.col (left) -->
		</div>
		<div id="rightSideBar" class="col-md-2">
			<jsp:include page="../common.jsp"></jsp:include>
		</div>
		<!-- right column -->
		<!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>

<script>
		$('body').on(
				'click',
				'#listOfJournalExtractLogs tbody td>button[name=approve]',
				function() {
					var id = servicestable.item.row($(this).parents('tr'))
							.data().id;
					alert(id);
					var url = "${getParticularContract}?id=" + id + "";
					window.location = url;
				});
		$(function() {
			$("#from").datepicker({
				format : 'yyyy-mm-dd',
				onSelect : function(selected) {
					console.log("123");
					$("#to").datepicker("option", "minDate", selected);
				}

			});
			$("#to").datepicker({
				format : 'yyyy-mm-dd',
				onSelect : function(selected) {
					console.log("123");
					$("#from").datepicker("option", "maxDate", selected);
				}
			});
		});
	</script>

<!-- Tab Script -->
<script src="js/tab_script.js"></script>

<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../finalFooter.jsp"></jsp:include>

