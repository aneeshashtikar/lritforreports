<%@page import="in.gov.lrit.billing.model.contract.ContractType"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:include page="../finalHeader.jsp">
	<jsp:param name="titleName" value="Upload Journal Extract " />
</jsp:include>

<spring:url value="/ide/uploadJournalExtractPost"
	var="uploadidejournalextract" />
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

	<!-- 	<section class="content-header">
		<h1>Upload Journal Extract</h1>
		<ol class="breadcrumb">
			<li><a href="/lritbilling/"><i class="fa fa-dashboard"></i>
					Home</a></li>
		</ol>
	</section>
	<br> <br> -->
	<section class="content">
		<div class="row">


			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->


				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Journal Extract</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<div class="container col-md-12"></div>
					<form:form class="form-horizontal" id="uploadJournalExtract"
						action="${uploadidejournalextract}" enctype="multipart/form-data"
						method="POST">
						<div class="box-body">

							<c:if test="${successMsg != null}">
								<div class='alert alert-success'>
									<a href='#' class='close' data-dismiss='alert'
										aria-label='close'>&times;</a> ${successMsg}
								</div>
							</c:if>
							<c:if test="${errorMsg != null}">
								<div class='alert alert-danger colose' data-dismiss='alert'>
									<a href='#' class='close' data-dismiss='alert'
										aria-label='close'>&times;</a> ${errorMsg}
								</div>
							</c:if>
							<!-- <input type="submit" value="Upload" name="journalExtract">-->
							<div class="row">
								<div class="form-group col-sm-6">
									<label for="contractNumber" class="col-sm-4">Select
										Journal Extract</label>
									<div class="col-sm-8">
										<input type="file" name="journalExtractZipFile"
											id="journalExtractZipFile" accept=".zip">* Upload
										only Zip Files...
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="Upload"
								name="journalExtract">
						</div>

					</form:form>

				</div>
			</div>
			<div id="rightSideBar" class="col-md-2">
				<jsp:include page="../common.jsp"></jsp:include>
			</div>
		</div>
	</section>
</div>
<!-- /.content-wrapper -->
<script src="js/tab_script.js"></script>
<%-- <jsp:include page="../../footerEnd.jsp"></jsp:include> --%>
<jsp:include page="../finalFooter.jsp"></jsp:include>