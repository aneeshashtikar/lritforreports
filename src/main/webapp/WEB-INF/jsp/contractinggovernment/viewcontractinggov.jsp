


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>View Contracting Government</title>
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<spring:url value="/contractinggovernment/getcontractinggovlist" var="getcontractinggovlist" />
	
<spring:url value="/contractinggovernment/deregistercontractinggov" var="deregistercontractinggov" />	
	
<spring:url value="/contractinggovernment/editcontractinggov" var="editcontractinggov"/>
	
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script> --%>
<!-- AdminLTE for demo purposes -->
<%-- <spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script> --%>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script type="text/javascript">
var resultSet = function () {
	this.item = {};
};

var listtable = new resultSet();

var targetTableId = 'viewcontractinggov-table';


var dTable = function(targetTableId,resultSet) {
 	if($.fn.dataTable.isDataTable(targetTableId)) {
 		resultSet.item.ajax.reload();
 	}
 	else {

	
	resultSet.item = 	$('#viewcontractinggov-table').DataTable({

		"ajax" : {
			"dataType" : 'json',
			"url" : "${getcontractinggovlist}",
			"type" : "GET",
			"dataSrc" : "",
			"error" : function(xhr, error, thrown) {
				window.location = "/errorpage";
			}
		},
		"responsive" : {
			"details" : {
				"type" : 'column',
				"target" : 'tr'
			}
		}

	});
 	}
}


var urlmethod = function(lritId, url,listtable,countryName,targetTableId) {
	
		$.ajax({
			url : url ,
			type : 'POST',
			data : {
				lritId : lritId
			},
			dataType: "json",
			success : function(
					data) {
				
				if(data == "Contracting Government has been deregistered") {
					$(".message").append("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'"
								+"	aria-label='close'>&times;</a>"+data+" </div>")
					} else {
						$(".message").append("<div class='alert alert-danger colose'><a href='#' class='close' data-dismiss='alert'"
								+"	aria-label='close'>&times;</a>"+data+" </div>")
					}
				dTable('#' + targetTableId , listtable, countryName);
			},
			error : function(
					error) {
				alert("Error AJAX not working: "
						+ error);
			}
		});
		//alert("after ajax");
		//dTable('#' + targetTableId , listtable, countryName);
		//alert("after datatable ");
}



	
		$(document).ready(
				function() {
				
					 dTable('#'+targetTableId, listtable);	
					 $('body').on( 'click', '#viewcontractinggov-table tbody td>button[name=btndelete]', function () {
			  		    	
			  				var lritId = listtable.item.row( $(this).parents('tr') ).data().lritId;
			  				var countryName = listtable.item.row( $(this).parents('tr') ).data().countryName;
			  			//	alert("Zoom out handle for delete button lritId "+lritId);
			  				var url = "${deregistercontractinggov}";
			  				urlmethod(lritId, url,listtable,countryName,targetTableId);
			  				/* var url = "${deregistercontractinggov}?lritId="+lritId;
			  				window.location=url; */
			  			});
			  		 	<%--Zoom in handle for delete button--%>
			  		    $('body').on( 'click','#viewcontractinggov-table.collapsed tbody li span>button[name=btndelete]',function () {
			  		    	var cellIndex =listtable.item.responsive.index($(this).parent().parent());
			  		    	var lritId = listtable.item.data()[cellIndex.row].lritId;
			  		    	var countryName = listtable.item.data()[cellIndex.row].countryName;
			  		    	var url = "${deregistercontractinggov}";
			  		    //	alert("Zoom in handle for delete button lritId "+lritId);
			  		    	urlmethod(lritId, url,listtable,countryName,targetTableId);
			  		    	/* var url = "${deregistercontractinggov}?lritId="+lritId;
			  		    	window.location=url; */
			  		    }); 
			  		
			  		    
			  		  <%--Zoom out handle for edit button--%>
					    $('body').on( 'click', '#viewcontractinggov-table tbody td>button[name=btnedit]', function () {
					    	
						 	var lritId = listtable.item.row( $(this).parents('tr') ).data().lritId;
						 	var countryName = listtable.item.row( $(this).parents('tr') ).data().countryName;
							//alert("Zoom out handle for edit button countryName "+lritId);
							//var url = "${editcontractinggov}";
						//	urlmethod(lritId, url,listtable,countryName,targetTableId);
							var url = "${editcontractinggov}?lritId="+lritId;
							window.location=url; 
						});
					 	<%--Zoom in handle for edit button--%>
					    $('body').on( 'click','#viewcontractinggov-table.collapsed tbody li span>button[name=btnedit]',function () {
					    	
					    	 var cellIndex =listtable.item.responsive.index($(this).parent().parent());
					    	var lritId = listtable.item.data()[cellIndex.row].lritId;
					    	var countryName = listtable.item.data()[cellIndex.row].countryName;
					    	//alert("Zoom in handle for edit button lritId "+lritId);
					    	//var url = "${editcontractinggov}";
					    	//urlmethod(lritId, url,listtable,countryName,targetTableId);
					    	 var url = "${editcontractinggov}?lritId="+lritId;
					    	window.location=url; 
					    }); 
					
				});
	</script>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<!-- <section class="content-header">
				<h1>List Contracting Government</h1>
			</section> -->
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-10">
					<div class="message">
					</div>
					<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if> 
						<div class="box box-info">
							<!-- <div class="box"> -->
							<div class="box-header with-border">
								<h3 class="box-title">List of Contracting Government</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<table id="viewcontractinggov-table"
									class="table table-bordered table-striped"
									style="width: 100%">
									<thead>
										<tr>
											<!-- <th data-data="id">Sr.No.</th> -->
											<th data-data="lritId" data-default-content="NA">Lrit Id</th>
											<th data-data="countryName" data-default-content="NA">Country
												Name</th>
											<th data-data="countryCode" data-default-content="NA">Country
												Code</th>
												<sec:authorize access="hasAuthority('editcontractinggovernment')">
											<th data-data="null"
												data-default-content="<button name='btnedit'  class='btn btn-primary'> Edit</button>"></th>
												</sec:authorize>
												<sec:authorize access="hasAuthority('deregistercontractinggovernment')">
											<th data-data="null"
												data-default-content="<button name='btndelete'  class='btn btn-primary'> Delete</button>"></th>
										</sec:authorize>
										</tr>
									</thead>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->


					</div>
					<!-- /.col -->
						<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>
		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->




</body>
</html>