<!-- 
--Created finalform.jsp as the sample form finalized by team for Shipping Company.
--included a shipbourne table in tab_2 
-->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Register Contracting Government Form</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<%-- <spring:url value="/resources/bower_components/Ionicons/css/ionicons.min.css" var="ioniconsmincss" /> --%>
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- Theme style -->
<%-- <spring:url value="/resources/dist/css/AdminLTE.min.css" var="adminltemincss" /> --%>
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<%-- <spring:url value="/resources/dist/css/skins/_all-skins.min.css" var="allskinmincss" /> --%>
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
<script src="${adminlteminjs}"></script>
<spring:url value="/contractinggovernment/persistcontractinggovernment"
	var="contractinggovernment" />
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style>
/* .customform {
	text-align: left;
} */
</style>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<!-- <section class="content-header">
				<h1>Register Contracting Government Form</h1>
			</section>
 -->


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-lg-10">
						<!-- general form elements -->

						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if> 
						<!-- Horizontal Form -->
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Register Contracting Government</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->
							<div class="box-body">
								<div class="form-group">

									<form:form id="contractinggovernmentregistration"
										action="${contractinggovernment}" method="post"
										name="contractinggovernmentregistration"
										modelAttribute="portalLritIdMaster">
										<div class="box-body">
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="type" class="col-sm-4 ">Type</label>
													<div class="col-sm-8">
														<form:select path="type" class="form-control" name="type"
															id="type" onchange="onchangetype();">
															<form:option value="cg" selected="selected">Contracting Government</form:option>
															<form:option value="sar">SAR Authority</form:option>
														</form:select>
													</div>

												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label class="col-sm-4 required-label">LRIT ID</label>
													<div class="col-sm-8">
														<form:input path="lritId" type="text" class="form-control"
															id="lritId" placeholder="LRIT ID" required="true"
															autocomplete="off" minlength="4" maxlength="4"
															pattern="\d{4}" title="LRIT ID should be have 4 digit." />

													</div>
												</div>
											</div>
											<div class="row" id="CountryNameDiv">
												<div class="form-group col-sm-6">
													<label class="col-sm-4 required-label">Country Name</label>
													<div class="col-sm-8">
														<form:input path="countryName" type="text"
															class="form-control" id="countryName"
															placeholder="Country Name" autocomplete="off"
															maxlength="100" pattern="[A-Za-z]{1,100}" title="Country Name should be in alphabets "/>

													</div>
												</div>
											</div>
											<div class="row" id="CountryCodeDiv">
												<div class="form-group col-sm-6">
													<label class="col-sm-4 required-label">Country Code</label>
													<div class="col-sm-8">
														<form:input path="countryCode" type="text"
															class="form-control" id="countryCode"
															placeholder="Country Code" autocomplete="off"
															maxlength="10" pattern="[A-Za-z]{1,10}" title="Country Code should be in alphabets "/>

													</div>
												</div>
											</div>
											<div class="row" id="CountryNameDropDown">
												<div class="form-group col-sm-6">
													<label class="col-sm-4 required-label">Country Name</label>
													<div class="col-sm-8">
														<select id="sarcountryname" class="form-control" 
															onchange="onchangesarcountryname()">
															<c:forEach items="${cglist}" var="list">
																<option value="${list.countryCode}">${list.countryName}</option>
															</c:forEach>
														</select>

													</div>
												</div>
											</div>
											<div class="row" id="SarCoastalAreaDiv">
												<div class="form-group col-sm-6">
													<label class="col-sm-4 required-label">SAR Authority Name</label>
													<div class="col-sm-8">
														<form:input path="coastalArea" type="text"
															class="form-control" id="coastalArea"
															placeholder="SAR Authority Name" autocomplete="off"
															maxlength="10" pattern="[A-Za-z]{1,10}" title="Coastal Area Name should be in alphabets "/>

													</div>
												</div>
											</div>
										</div>
										<!-- /.box-body -->

										<div class="box-footer">
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form:form>

								</div>

							</div>
							<!-- /.form -->
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>
		

		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->



	<script type="text/javascript">
		/* Initially hide  sar   field */
		$(document).ready(function() {
			var type = document.getElementById("type").value;
			if (type == 'sar') {
				onchangetype();
			}else {
			$('#CountryNameDropDown').hide();

			$('#SarCoastalAreaDiv').hide();
			}
		});
		/* On type hide and show the fields */
		function onchangetype() {
			var type = document.getElementById("type").value;
			if (type == 'sar') {
				$('#CountryNameDiv').hide();
				$('#CountryCodeDiv').hide();
				$('#CountryNameDropDown').show();
				$('#SarCoastalAreaDiv').show();

				onchangesarcountryname();
			} else {
				$('#CountryNameDiv').show();
				$('#CountryCodeDiv').show();
				$('#CountryNameDropDown').hide();
				$('#SarCoastalAreaDiv').hide();
				/* document.getElementById("countryName").value="";
				document.getElementById("countryCode").value=""; */
				
				
			}
		}
		/* On selected CountryName set CountryCode and CountryName */
		function onchangesarcountryname() {

			var select = document.getElementById("sarcountryname");
			var countrycode = document.getElementById("sarcountryname").value;
			var countryname = select.options[select.selectedIndex].text;

			$('#countryName').val(countryname);
			$('#countryCode').val(countrycode);

		}
	</script>

</body>
</html>