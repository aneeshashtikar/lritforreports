<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>LRIT | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->

<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Morris chart -->
<spring:url value="/bower_components/morris.js/morris.css"
	var="morriscss" />
<link rel="stylesheet" href="${morriscss}">
<!-- jvectormap -->
<spring:url value="/bower_components/jvectormap/jquery-jvectormap.css"
	var="jqueryjvectormapcss" />
<link rel="stylesheet" href="${jqueryjvectormapcss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">

  <!-- DataTables -->
  <spring:url value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" var="dtbootstrapmincss"/>
  <link rel="stylesheet" href="${dtbootstrapmincss}">
  <spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">

<link rel="stylesheet">
	<!-- href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css"> -->
	<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">

<spring:url value="/resources/img/dgslogo.ico"
	var="dgslogoICO" />
	<link rel="icon" href="${dgslogoICO}">
	<spring:url var="setgeolocation" value="/logging/setgeolocation"></spring:url>
<style>
.side-box {
	margin-bottom: 0px;
	/* height: 200px; */
}
.direct-chat-messages {
    padding: 0px;
    height: 150px;
    overflow: auto;
}
 .scrollable {
	height: 100px;
	
	overflow-y: scroll;
} 

.box{
	margin-bottom: 10px; 
}
.skin-blue .main-header .navbar .sidebar-toggle:hover .navbar-static-top .sidebar-menu>li> .treeview-menu 
{
    background-color: none;
	background: none;
}
/*#header-img{
  background: url("resources/banner.jpg") ;
  background-size: cover;
  background-color: none;
  
}
#sidebar-img {
padding-top: 100px;
	background: url ("resources/sidebarmenu.jpg");
	  background-size: cover;
  background-color: none;
}
 bg test 
#wrapper-img{ 
  background: url("resources/ship.jpg");
height: 100%; 
background-position: center;
background-repeat: no-repeat;
background-size: cover;
}*/
/* #img {
  background: url("resources/bot-back.jpg") ;
  background-size: cover;
} */
/* .content-wrapper
{
  background: url("resources/bot-back.jpg") ;
  background-size: cover;
}
/* Main Header image 
.main-header .navbar  
{
background: url("resources/banner.jpg") ;
  background-size: cover;
}
 Sidebar image 
.main-sidebar{
background: url("resources/sidebarmenu.jpg") ;
  background-size: cover;
}
 Sidebar Header image 
.main-header .logo{
height: 100px;
background: url("resources/sidebarmenu.jpg") ;
 background-size: cover;
} */





</style>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body id="img" class="hold-transition skin-blue sidebar-mini" >
	<div id="wrapper-img" class="wrapper" >

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" >
			<!-- Content Header (Page header) -->
			<!-- <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section> -->

			<!-- Main content -->
			<section id ="MainContent" class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<div class="box fullscreen" id="fullscreen">
								 <div class="box-header with-border " id = "mapTitleBox">
								<h3 class="box-title">Map</h3>
								<div class="box-tools pull-right">
								<button id = "fullScreenButton" type="button" class="btn btn-box-tool">
								</button>
								
								<!--	<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								 <button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button> -->
								</div>
							</div> 
							<div class="box-body">
						              
								<!-- <div class="row">  -->
									<!-- <div id="map"> -->
									
									
									 <jsp:include page="mapView.jsp"></jsp:include> 
									<!-- </div>  -->
								<!--  </div>  -->
							</div>
							<div id="refreshDataTime" class="refreshDataTimeStyle " Style="text-color:gray;"> </div>
							
						</div>
					</div>
					<div id= "rightSideBar" class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

						

				</div>
				
				<!-- /.row -->
				<!-- Main row -->


				<!-- <div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Reports</h3>

								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										onclick="window.open('https://10.210.8.94:8443/jasperserver');">
										<i class="fa fa-clone"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							/.box-header
							./box-body
							<div class="box-body ">
								<div class="row">
									<div>
										<iframe height="300px" width="100%"  name="iframe_a">
											src="https://10.210.8.94:8443/jasperserver"</iframe>

									</div>
								</div>
							</div>
							/.box-footer
						</div>
						/.box
					</div> -->
					
					</section>
				</div>



				<!-- /.row (main row) -->

			
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>
		<!-- Control Sidebar -->
		<%-- <jsp:include page="rightbar.jsp"></jsp:include> --%>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
<!-- 	</div> -->
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<spring:url
		value="/bower_components/jquery/dist/jquery.min.js"
		var="jqueryminjs" />
	 <script src="${jqueryminjs}"></script> 
	 <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> 

	<!-- jQuery UI 1.11.4 -->
	<spring:url
		value="/bower_components/jquery-ui/jquery-ui.min.js"
		var="jqueryuiminjs" />
	<script src="${jqueryuiminjs}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.7 -->
	<spring:url
		value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
		var="bootstrapminjs" />
	<script src="${bootstrapminjs}"></script>
	<!-- Morris.js charts -->
	<spring:url value="/bower_components/raphael/raphael.min.js"
		var="raphaelminjs" />
	<script src="${raphaelminjs}"></script>
	<spring:url value="/bower_components/morris.js/morris.min.js"
		var="morrisminjs" />
	<script src="${morrisminjs}"></script>
	<!-- Sparkline -->
	<spring:url
		value="/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"
		var="jquerysparklineminjs" />
	<script src="${jquerysparklineminjs}"></script>
	<!-- jvectormap -->
	<spring:url value="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"
		var="jqueryjvectormapjs" />
	<script src="${jqueryjvectormapjs}"></script>
	<spring:url
		value="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"
		var="jqueryjvectorworldjs" />
	<script src="${jqueryjvectorworldjs}"></script>
	<!-- jQuery Knob Chart -->
	<spring:url
		value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
		var="jqueryknobminjs" />
	<script src="${jqueryknobminjs}"></script>
	<!-- daterangepicker -->
	<spring:url value="/bower_components/moment/min/moment.min.js"
		var="momentminjs" />
	<script src="${momentminjs}"></script>
	<spring:url
		value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
		var="daterangepickerjs" />
	<script src="${daterangepickerjs}"></script>
	<!-- datepicker -->
	<spring:url
		value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
		var="bootstrapdaterangepickerminjs" />
	<script src="${bootstrapdaterangepickerminjs}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<spring:url
		value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
		var="wysihtml5allminjs" />
	<script src="${wysihtml5allminjs}"></script>
	<!-- Slimscroll -->
	<spring:url
		value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
		var="jqueryslimscrollminjs" />
	<script src="${jqueryslimscrollminjs}"></script>
	<!-- FastClick -->
	<spring:url value="/bower_components/fastclick/lib/fastclick.js"
		var="fastclickjs" />
	<script src="${fastclickjs}"></script>
	<!-- AdminLTE App -->
	<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
	<script src="${adminlteminsjs}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
	<script src="${dashboardjs}"></script>
	<!-- AdminLTE for demo purposes -->
	<spring:url value="/dist/js/demo.js" var="demojs" />
	<script src="${demojs}"></script>
	
	<!-- DataTables -->
<spring:url value="/bower_components/datatables.net/js/jquery.dataTables.min.js" var="jqdtminjs"/>
<script src="${jqdtminjs}"></script>
<spring:url value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js" var="dtbootstrapminjs"/>
<script src="${dtbootstrapminjs}"></script>
<!-- 	<script
		src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>


	<script type="text/javascript">
	 /* $('#custom-scroll').slimscroll({
		  height: '100px'
		});  */
	
		var vectorSourceWFS = new ol.source.Vector({
			wrapX : false,
			noWrap : true
		});

		var vectorWFS = new ol.layer.Vector({
			source : vectorSourceWFS,
		});

		var raster = new ol.layer.Tile({
		// source: new ol.source.OSM()
		});

		var wmsTile = new ol.layer.Tile({
			source : new ol.source.TileWMS({
				//url : 'http://10.210.9.253:8080/geoserver/wms',
				params : {
					'LAYERS' : 'indiaosm3',
					'TILED' : false
				},
				serverType : 'geoserver',
				// Countries have transparency, so do not fade tiles:
				//  transition: 0,
				wrapX : false,
				noWrap : true
			})
		})

		//----
		var map = new ol.Map({
			layers : [ wmsTile, vectorWFS ],
			target : 'map',
			view : new ol.View({
				//For India
				center : ol.proj.transform([ 72.00, 15.00 ], 'EPSG:4326',
						'EPSG:3857'),
				maxZoom : 19,
				zoom : 2
			})
		});

		// generate a GetFeature request
		var featureRequest = new ol.format.WFS().writeGetFeature({
			srsName : 'EPSG:3857',
			//featureNS: 'http://openstreemap.org',
			featurePrefix : 'osm',
			featureTypes : [ 'VesselPosition' ],
			outputFormat : 'application/json',

		});

		// then post the request and add the received features to a layer
	/* 	fetch('http://10.210.9.253:8080/GeoService/VesselPosition?method=2', {
			method : 'POST',
		//body: new XMLSerializer().serializeToString(featureRequest)
		}).then(function(response) {
			//console.info("fdasdasd");
			return response.json();
		}).then(function(json) {
			//console.info(json);
			var features = (new ol.format.GeoJSON()).readFeatures(json, {
				dataProjection : 'EPSG:4326',
				featureProjection : 'EPSG:3857'
			});

			vectorSourceWFS.addFeatures(features);
			//console.info(vectorSourceWFS);
			map.getView().fit(vectorSourceWFS.getExtent());
		}); */
	</script>

 -->
</body>
<script>
$(document).ready(function() {
	 if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function(position)
		{
			
			$.ajax({
				url:  "${setgeolocation}",
				data:  	{'latitude' : position.coords.latitude,
						'longitude' : position.coords.longitude
						}
			});

		});
	}
	else{
		} 


});
</script>
</html>
