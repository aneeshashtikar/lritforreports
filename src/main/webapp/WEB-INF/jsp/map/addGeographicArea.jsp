<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
 	<h4 class="modal-title" >Add Geographical Area</h4>
 </div>    
<!-- modal-form content -->              
<div class="modal-body" id= "modelbody">
<table class="table table-bordered">
<tr>
<td> <input type="radio" name="option" id= "add" value="add" onclick="optionclickevent();">Add Geographic Area</td>
<td> <input type="radio" name="option" id= "edit" value="edit" onclick="optionclickevent();">Edit Geographic Area</td>
</tr>
</table>
<div id="addgml">
<table class="table table-bordered">
<tr><td>
 <input type="radio" name="drawOption" id= "upload" value="fileupload" onclick="radiobuttonclickevent();">Upload GML
 </td>
<td>
 <input type="radio" name="drawOption" id= "insert" value="insertbytextfields" onclick="radiobuttonclickevent();"> Input Coordinates<br>
</td></tr>
<tr>
<td colspan="2">
 <!--    For Upload  -->  
  <div id = "uploaddiv">
  <table style="width:100%" >
  <tr><td >Select File <input type="file" id="file"></td></tr>  
  </table>
</div>
</td>

</tr>
 <!-- <tr><td>  
 Area Name: <input type="text" id ="areaname"  maxlength="50" size="10"> 
 </td></tr>  -->
 <tr><td>  
 Area Type: </td><td><select id = 'geographicalareatype' class ='form-control'>
   <option value = "InternalWaters">InternalWaters</option>
   <option value = "TerritorialSea">TerritorialSea</option>
   <option value = "SeawardAreaOf1000NM">SeawardAreaOf1000NM</option>
   <option value = "CustomCoastalAreas">CustomCoastalAreas</option>
   </select> 
 </td></tr>
</table> 
 </div> 
 
 
 <!--    For show Saved Polygons  -->  
    <div id = "showgmldiv" class="box-body" >
    
 <!--   <table style="width:100%" class="table">
  <thead>  
    <tr>
   <th colspan="2">Geographical areas</th>   
   </tr>  
     </thead> 
  </table>   -->  
<table  class="table table-hover">
  <thead>   
   <tr><th style="width : 150px;">Area Code</th><th style="width : 200px;">Area Type</th><th>Status</th></tr>     
     </thead> 
  </table>
 <div style="height: 100px; overflow-y: auto;">   
  <table class="table table-hover" >
  <tbody id ="showgmltable"></tbody>   
  </table>
   </div> 
 </div> 
 
 
<!--    For show Saved Polygons  --> 
<div id = "showpolygondiv"> 
	<table style="width:100%" class="table table-bordered">
		<thead><tr><th>Polygon Areas</th></tr></thead>
		<tbody>
 			<tr><td>Polygon Name </td></tr>  
  			 <tr><td>
  				<div style="height: 100px; overflow-y: auto;">
  					<table style="width:100%" class="table table-bordered">
   						<tbody id ="showpolygontable"></tbody>
  					</table>
  				</div>
  			</td></tr>
		</tbody>
	</table>
</div>

<!--    For Polygon  -->  
  <div id = "polygondiv">
  
  <table style="width:100%" >
  <thead><tr><th>Area Code:</th><th> <input type="text" id ="areaname"  maxlength="50" size="10"> </th></tr>
  <tr><th>Caption:</th><th><input type="text" id ="description"  maxlength="255" size="20"> </th></tr>
  <tr><th>Polygon Points <button class ="add-row" >Add row</button></th></tr></thead>
  <tr><td>Latitude</td><td>Longitude</td><td></td></tr> 
  </table>
  <div style="height: 150px; overflow-y: auto;">
  <table style="width:100%" id='polytable' class="table table-bordered coordinate-edit">
   
   <tbody id ="polygontable">
    
  </tbody> 
  </table>
  </div>
</div>
</div>            
<div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="button" id ="addpolygonbtn" onclick="addPolygon();" class="btn btn-primary" >Add Polygon</button>
      <button type="button" id ="savebtn" onclick="applyGeographicArea();" class="btn btn-primary" data-dismiss="modal">Save in DB</button>
      <button type="button" id ="showallbtn" onclick="showAllPolygonFromDraft();" class="btn btn-primary" data-dismiss="modal">Show All</button>
      <button type="button" id ="showonmapbtn" onclick="showPolygonFromDraft();" class="btn btn-primary" data-dismiss="modal">Show on Map</button>   
 </div>
              

 <script>
 $("#areaname").attr('disabled', true); 
 $("#uploaddiv").hide();
 $("#polygondiv").hide(); 
 $("#showpolygondiv").hide(); 
 
 $("#addgml").hide();
 $("#showgmldiv").hide();
 
 $("#editoptiondiv").hide();
 $("#showddppolygondiv").hide();
 
 var mode=''; 

 function radiobuttonclickevent(){
	 geographicAreaPolygonList.clear();	
	 var radioValueInput = $("input[name='drawOption']:checked").val();
	 if (radioValueInput === "fileupload"){
		// $("#apply").hide();
		    $("#uploaddiv").show();
		    $("#polygondiv").hide();
		    $("#addpolygonbtn").hide();
		    mode = 'fileupload';
	 }
	 else if (radioValueInput === "insertbytextfields"){
	 
	    //$("#apply").show();	    
	    	$("#uploaddiv").hide();	    	
	    	$("#polygontable").html("");
	    	$(".add-row").trigger("click");
	        $(".add-row").trigger("click");
	        $(".add-row").trigger("click");
	    	$("#polygondiv").show();
	    	$("#areaname").val("");
	    	$("#description").val("");	    	
	    	$("#savebtn").show();
	    	$("#showonmapbtn").show();
	    	 mode = 'input';
	    	 $("#polygondiv :input").attr("disabled", false);	
	    	 $("#areaname").attr('disabled', true); 
	    	 generateAreaCodeByPolygontype();      
	    }
	 $("#showpolygondiv").hide();	
	 $("#file").val("");     	
	 $("#geographicalareatype").attr('disabled', false);
 }


 function optionclickevent(){
	 $("#showpolygondiv").hide();
	 $("#polygondiv").hide(); 
	 resetFooterButtons(); 
	 var radioValueInput = $("input[name='option']:checked").val();
	 if (radioValueInput === "add"){
		 $("#apply").hide();
		    $("#addgml").show();
		    $("#showgmldiv").hide();
		    //$("#showddppolygondiv").hide();
		    $("#editoptiondiv").hide();
		    radiobuttonclickevent();
		  
			  
		   
	 }
	 else if (radioValueInput === "edit"){
		 $("#addgml").hide();
		 $("#editoptiondiv").show();
	 		
	    		    	
	    	showGMLList();	
	    	 //mode = 'gmledit'; 
	    	 mode = 'edit';
	    	 $("#areaname").attr('disabled', true);  
	    	 
    	 
	 }
	
	
	 
	
	 
}
 
function resetFooterButtons(){
	 $("#addpolygonbtn").hide();
	 $("#savebtn").hide();
	 $("#showallbtn").hide();
	 $("#showonmapbtn").hide();
	 $("#deletebtn").hide();
} 

 var fileContent;
 var gmlContent="";
 
 function applyGeographicArea(){
	 saveGeographicArea(); //To save latest coordinates. 	
	 	var gmlId=0;
	 	 if (mode === "gmledit"){
	 		var areaCodeGmlId = $("input[name='areaCode_gmlid']:checked").val();
			 var areaCodeGmlIdSplt = areaCodeGmlId.split(",");
			 gmlId = areaCodeGmlIdSplt[1];
			 }
	 	var data = '';
	 	var areaCodes = [];		
		//var features = sourceCustomPolygon.getFeatures();	
			for (v of geographicAreaPolygonList.values()) {
				 // console.log(v)
				  data= data + writeFeatureAsGML(v);
				  areaCodes.push(v.get("areaId"));				 
				}			
		//alert(data);
	 if(data){
		 
	 //var areaId =  $("#areaname").val();
	 var areaType =$("#geographicalareatype").val();
	 var gmldataHeader ='<?xml version="1.0" encoding="utf-8"?><gml:MultiSurface xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/gml/3.2 http://schemas.opengis.net/gml/3.2.1/gml.xsd" gml:id="'+ areaType +'"><gml:surfaceMembers>';
	 var gmldataFooter = '</gml:surfaceMembers></gml:MultiSurface>';
	 data = gmldataHeader + data + gmldataFooter;

	 gmlContent = data;
	 
		var gmldata = { 
				gmlId : gmlId,
			  	fileType :"gml",
			  	data : data,
			  	//areaId : areaId,
			  	areaIds : areaCodes,
			  	areaName : areaId,
			  	areaType : areaType	
				};
	 }
		if(gmldata){
	    	 // console.info(gmldata);
	    	  $.ajax({url: 'uploadFile' ,type: 'post',
	    		    contentType: 'application/json',
	    		    data: JSON.stringify(gmldata) ,success: function(result){
	                //alert(result);	
	                $("#edit").trigger("click");                
	            }}); 
  	  }	
		generateGMLforDownload();
		showPolygonFromDraft();
	 }


 	function writeFeatureAsGML(feature){
 			areaId = feature.get("areaId");//'GAIW1234_1'; //test
 			areaDescription = feature.get("areaDescription");//'Area 1'; //test
			var posList = getPositionListString(feature);
 			//posList='7.25 53.32 7.19 53.31 7.01 53.33 6.97 53.37 6.91 53.43 6.88 53.45 6.81 53.49 6.77 53.51 6.74 53.53 6.68 53.55 6.61 53.58 6.55 53.61 6.64 53.62 6.72 53.65 6.8 53.67 6.83 53.67 7.05 53.69 7.16 53.72 7.32 53.73 7.35 53.73 7.4 53.74 7.61 53.76 7.69 53.78 7.76 53.78 7.81 53.79 8.34 53.96 8.57 54.3 7.25 53.32'; //test
			
			var gmlText='<gml:Polygon gml:id="'+areaId+'"><gml:description>'+areaDescription+'</gml:description><gml:exterior><gml:LinearRing><gml:posList>'+posList+'</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon>';			
			return gmlText;
  	}

 	function getPositionListString(feature){
			var posList='';
			var featureLocal = feature.clone();	
	        var latlon = featureLocal.getGeometry().getCoordinates();	        
	        for(var i = 0; i < latlon.length; i++) {
	            var row = latlon[i];
	            for(var j = 0; j < row.length; j++) {
	            	posList = posList +' '+ (row[j][0]).toFixed(2) +' ' +(row[j][1]).toFixed(2);
	            }
	        }			
			return posList;
 	}
 	 	

 		
		
			
		 function generatePolygonFeatureFromInputForm(){
	    	  
	    	 var areaname = $("#areaname").val();
	    	 var isValidFlag = true;
	    	 if(areaname.trim().length >0){

		      var areatype = $("#geographicalareatype").val();
	    	 
	    	  var latDegValues = $("input[id='latDeg']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var latMinValues = $("input[id='latMin']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var latDirection = $("select[id='latDirection']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonDegValues = $("input[id='lonDeg']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonMinValues = $("input[id='lonMin']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonDirection = $("select[id='lonDirection']")
	          .map(function(){return $(this).val();}).get();
	        
	    	  var latOut=[];
	    	  var lonOut=[];
	    	  var coordinates=[];
	    	  for(var i =0 ; i < latDegValues.length;i++){
				var longitudeValue= DegMinToLatLon(lonDegValues[i],lonMinValues[i],lonDirection[i]);
				var latitudeValue = DegMinToLatLon(latDegValues[i],latMinValues[i],latDirection[i]);
				//Validation Check---
				if ((isNaN(longitudeValue)) || (isNaN(latitudeValue))) 	isValidFlag = false;
				if ((lonDegValues[i] < 0) || (lonDegValues[i] >180)) 	isValidFlag = false;
				if ((latDegValues[i] < 0) || (latDegValues[i] >90)) 	isValidFlag = false;
		    	  
	    		  coordinates.push([longitudeValue,latitudeValue]);	 
	    	  }	 
	    	  if (isValidFlag){   	 
	          var areaId=areaname;
	          var areaDescription=$("#description").val();;
	          var polygon = new ol.geom.Polygon([coordinates]);
	          //polygon.transform('EPSG:4326', 'EPSG:3857')
	    		var feature = new ol.Feature(polygon);
	    		feature.setProperties({areatype: areatype,areaId:areaId,areaDescription:areaDescription});    				    		
	    		return feature;	
	    	  }
	    	  else{
	    		  alert("Please Enter Valid inputs for Latitude and Longitude ");
		      }
	    	 } 
	    	 else{
	    		 alert("Please Enter Area Code ");
	  		 }   	  
	      }

		 
		 function showPolygonsList(){
				$("#showpolygontable").html("");
				for (v of geographicAreaPolygonList.values()) {
					 // console.log(v)
					$("#showpolygontable").append("<tr><td><input name = 'geographicarea' type='radio' onclick='editPolygonFromDraft()'  value = "+ v.get("areaId") +">"+v.get("areaId") +"</td></tr> "); 
					if((mode === "input")||(mode === "fileupload")) $("#showpolygondiv").show();
					}
				//console.info(mode);
				
						
			}

		 function editPolygonFromDraft(){				
				var areaName = $("input[name='geographicarea']:checked").val();
				editPolygonCoordinates(areaName);
				if((mode === "input")||(mode === "fileupload")) $("#showallbtn").show();
		 }
		 
		 function saveGeographicArea(){			
				var featureLocal = generatePolygonFeatureFromInputForm();
				if(featureLocal){				
					geographicAreaPolygonList.set(featureLocal.get("areaId"),featureLocal);	
				}else{
					//this.preventDefault();
				}		
		}

		function addPolygon(){
			if($("#areaname").val() === "" ){
			//	alert("Please Enter Area Code ");
			} else{
				saveGeographicArea();
				if (geographicAreaPolygonList.size > 0) showPolygonsList();
				$("#polygontable").html("");
		    	$(".add-row").trigger("click");
		        $(".add-row").trigger("click");
		        $(".add-row").trigger("click");
		    	$("#polygondiv").show();
		    	$("#areaname").val("");
		    	$("#description").val("");
		    	generateAreaCodeByPolygontype();
			}
		}

		var watertype ="";
		var areaidStart ="";
		 function generateAreaCodeByPolygontype(){
			 var idOffset = 0;
			 if (geographicAreaPolygonList.size < 1){				 
			 	watertype = $("#geographicalareatype").val();
			 	$.ajax({url: 'getnextpolygonid?type='+watertype,type: 'get',
		 		    success: function(result){
		 		    	areaidStart = result;
		 		    	$("#areaname").val(result);
		 		  }});
			 }else{
				 $("#geographicalareatype").val(watertype);
				 var areaIdPrefix = areaidStart.substring(0,9);
				 var idNum = parseInt(areaidStart.substring(9));
				 idOffset = geographicAreaPolygonList.size;	
				 $("#areaname").val(areaIdPrefix+""+(idNum+idOffset));			 
			 } 			 
		 }

		 
		 function showPolygonFromDraft(){
			 vectorSource_Temp.clear();
			 sourceCustomPolygon.clear();
				if($("#areaname").val().trim().length > 0){ 
				//	console.info("###");
					saveGeographicArea();
					 showPolygonsList();
					$('input[name="geographicarea"][value="' + $("#areaname").val() + '"]').prop('checked', true);
					$('input[name="geographicarea"]').attr('checked', true).trigger('click');
				var areaName = $("input[name='geographicarea']:checked").val();
				 $("#areaname").attr('disabled', true); 
				// console.info(sourceCustomPolygon);
				sourceCustomPolygon.clear();
				if(geographicAreaPolygonList.size >0){
					if(geographicAreaPolygonList.get(areaName)){
						var featureTemp = geographicAreaPolygonList.get(areaName);
						featureTemp.setStyle(styleFunctionGA(featureTemp));
						sourceCustomPolygon.addFeature(featureTemp);
						 vectorSource_Temp.addFeature(featureTemp);
						//sourceCustomPolygon.addFeature(geographicAreaPolygonList.get(areaName));
					}else{
						 alert("Select Polygon from List");
					}
				}else{
					// alert("Add Polygon First");
				}
				}	
				
				 var extent = vectorSource_Temp.getExtent();	
				 console.info(extent);
				 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
				 { 
		    		 var diffExtent = extent[2]- extent[0];
		    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
		    		 {
		    			 var center = ol.extent.getCenter(extent);							
		    			 map.getView().setCenter(center);
		    		 }
		    		 else
		    			 map.getView().fit(extent, map.getSize());	    		
				 }
			}	

		 function showAllPolygonFromDraft(){
			 //console.info($("#areaname").val());
			 vectorSource_Temp.clear();
			 if($("#areaname").val().trim().length > 0){ 
				 
					saveGeographicArea();
					showPolygonsList();
					$('input[name="geographicarea"][value="' + $("#areaname").val() + '"]').prop('checked', true);
					
			 sourceCustomPolygon.clear();
			 if (geographicAreaPolygonList.size >0){
				 for (v of geographicAreaPolygonList.values()) {
						v.setStyle(styleFunctionGA(v));
						sourceCustomPolygon.addFeature(v);
						vectorSource_Temp.addFeature(v);
					}	
			 }else{
				 alert("Add Polygon First");
			 }
			 }		
			 
			 
			 var extent = vectorSource_Temp.getExtent();	
			 console.info(extent);
			 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
			 { 
	    		 var diffExtent = extent[2]- extent[0];
	    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
	    		 {
	    			 var center = ol.extent.getCenter(extent);							
	    			 map.getView().setCenter(center);
	    		 }
	    		 else
	    			 map.getView().fit(extent, map.getSize());	    		
			 }
			}			 
				
		 
		function formFillFromGML(gml){
			geographicAreaPolygonList.clear();
			var gmlFeatures = getFeaturesfromGML(gml);
			if(gmlFeatures){
				//console.info(gmlFeatures);
				$("#geographicalareatype").val(gmlFeatures.areaType);
				var featureList = gmlFeatures.featureList;
				featureList.forEach(function(feature){
					geographicAreaPolygonList.set(feature.get("areaId"),feature);
				});				
				showPolygonsList();	
			} 	
		}


	function getFeaturesfromGML(gml){
		try{
		featureList = [];
		gmlwithoutNS = gml.replace(/gml:/g, "");
		 $xml = $( $.parseXML( gmlwithoutNS ) );
		 var areaType=$xml.find("MultiSurface").attr("id");
		 
		 //console.info(areaType); 
	 	 $xml.find("Polygon").each(function(){				
				var areaId =$(this).attr("id");
				var areaDescription =$(this).find("description").text();
				//console.info($(this).attr("id"));
	 			//console.info(areaDescription);
				var poslist = $(this).find("posList").text().replace( /\n/g, " " ).trim().split( " " );
				var poslistfiltered = poslist.filter(function (el) { 
					return el != '';				
				}); 				
				var lonlat=[];					
				for(var i= 0;i<poslistfiltered.length;i++){
					lonlat.push([ parseFloat(poslistfiltered[i]), parseFloat(poslistfiltered[++i])]);					
				}				
				var polygon = new ol.geom.Polygon([lonlat]);
	          //polygon.transform('EPSG:4326', 'EPSG:3857');
	    		var feature = new ol.Feature(polygon);
	    		feature.setProperties({areatype: areaType,areaId:areaId,areaDescription:areaDescription});  	    		  				    		
				featureList.push(feature);
				//console.info(feature);
	 		 });	

			if(areaType){
				var gmlFeatures = {
							areaType : areaType,
							featureList : featureList
						};

				return gmlFeatures;
			}else{
				alert("Not a Valid GML");
				}
		} catch(err){ console.info("XML Parsing Error");
			alert("Not a Valid GML");
		}
		}

		
		function editPolygonCoordinates(areaName){				
				//var areaName = $("input[name='geographicarea']:checked").val();
				 $("#polygondiv").hide();
				$("#areaname").val(areaName);				
						feature = geographicAreaPolygonList.get(areaName);
						//console.info("CALL : "+ feature +" "+areaName);
						if (feature) {
							
			    	        var featureL = feature.clone();	
			    	       // var latlon = featureL.getGeometry().transform('EPSG:3857', 'EPSG:4326').getCoordinates();
			    	        var latlon = featureL.getGeometry().getCoordinates();
			    	       var areaDescription =featureL.get("areaDescription");
			    	       $("#geographicalareatype").val(featureL.get("areatype"));
			    	       if(areaDescription) {
				    	       $("#description").val(areaDescription);
			    	       }else $("#description").val("");
			    	        var i;
			    	        var lon=[];
			    	        var lat=[];
			    	       // console.info(latlon);
			    	        featureTemp =latlon;
			    	        $("#polygontable").html("");
			    	        for(var i = 0; i < latlon.length; i++) {
			    	            var row = latlon[i];
			    	            for(var j = 0; j < row.length; j++) {
			    	               // console.info("row[" + i + "][" + j + "] = " + row[j][0]);
									[latDeg ,latMin] = LatLonToDegMin(row[j][1]);  
						            [lonDeg ,lonMin] = LatLonToDegMin(row[j][0]);      
						            var latoptionString = (row[j][1] > 0) ? "<option selected value = 1 >North</option><option value = -1 >South</option>" : "<option value = 1 >North</option><option value = -1 selected>South</option>";			            	   
						            var lonoptionString = (row[j][0] > 0) ? "<option selected value = 1 >East</option><option value = -1 >West</option>" : "<option value = 1 >East</option><option value = -1 selected >West</option>"  ;          	   	
						            			
						            var markup = "<tr><td><input type='text'  id = 'latDeg'  class = 'integercoordinate latitudedegree' maxlength='2' size='2' value = "+ latDeg +">&nbsp;<input type='text' id = 'latMin' class = 'integercoordinate latitudeminute' maxlength='2' size='2' value = "+ latMin +
						            	   "><select id = 'latDirection' > "+ latoptionString +
						                   "</select></td><td><input type='text' id='lonDeg' class ='integercoordinate longitudedegree'  maxlength='3' size='3' value = "+ lonDeg +" ><input type='text' id='lonMin' class = 'integercoordinate longitudeminute' maxlength='2' size='2' value = "+ lonMin +"><select  id = 'lonDirection'>"+
						            	  lonoptionString +"</select></td><td><button class= 'delete-row' >Remove</button></td></tr>"
						              $("#polygontable").append(markup);						
			    	            }
			    	        }

			    	        $("#polygondiv").show();
			    	       // $("#polygontable :input").attr("disabled", true);			    	        
			    	       // $("#showallbtn").show();
			    	   	 	$("#showonmapbtn").show();
			    	   	 	$("#savebtn").show(); 
			    	   	 	if(mode ==='fileupload'){
			    	   	 		$("#polygondiv :input").attr("disabled", true);	
					    	}
			    	   	 	else{
					    		$("#polygondiv :input").attr("disabled", false);	
						    }
						    if ((mode ==='gmledit') ||(mode ==='ddpedit'))
						    {	
							    $("#areaname").attr('disabled', true); 
							   // console.info("EDIT"); 
						    }
						    if (mode ==='input')
						    {	
						    	$("#addpolygonbtn").show();
						    }
			    	   	 	           
			    	    }							 				
			}
		

	//----------
		$(".coordinate-edit").on('keydown','.integercoordinate',function(e){
		    var keyPressed;
		    if (!e) var e = window.event;
		    if (e.keyCode) keyPressed = e.keyCode;
		    else if (e.which) keyPressed = e.which;
		    var hasDecimalPoint = (($(this).val().split('.').length-1)>-1); //restrict decimal point
		    if ( keyPressed == 46 || keyPressed == 8 ||((keyPressed == 190||keyPressed == 110)&&(!hasDecimalPoint && !e.shiftKey )) || keyPressed == 9 || keyPressed == 27 || keyPressed == 13 ||
		             // Allow: Ctrl+A
		            (keyPressed == 65 && e.ctrlKey === true) ||
		             // Allow: home, end, left, right
		            (keyPressed >= 35 && keyPressed <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        else {
		            // Ensure that it is a number and stop the keypress
		            if (e.shiftKey || ((keyPressed < 48) || (keyPressed > 57)) && ((keyPressed < 96) || (keyPressed > 105) )) {
		                e.preventDefault();
		            } 
		            
		        }

		  });		

			$(".coordinate-edit").on('keyup','.longitudedegree',function(e){
				   if($(this).val()>180) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
				   lonDegVal = $(this).val();
				   if(lonDegVal == 180) $(this).closest('tr').find('.longitudeminute').val(0);
			 });

		  	$(".coordinate-edit").on('keyup','.latitudedegree',function(e){
			     if($(this).val()>90) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
			     var latDegVal = $(this).val();
			      if(latDegVal == 90) $(this).closest('tr').find('.latitudeminute').val(0);
			  }); 
		  
			$(".coordinate-edit").on('keyup','.latitudeminute',function(e){
		     if($(this).val()>59) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
		     var latDegVal = $(this).closest('tr').find('.latitudedegree').val();
		     if	(latDegVal == 90) $(this).val(0);		     
		 	 }); 
		  
			$(".coordinate-edit").on('keyup','.longitudeminute',function(e){
			     if($(this).val()>59) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
			     var lonDegVal = $(this).closest('tr').find('.longitudedegree').val();
			     if	(lonDegVal == 180) $(this).val(0);
			  }); 
	
			 
	//---	
	
	var gmlDBMap = new Map(); 
				
		 function showGMLList(){
			 $.ajax({url: 'getGeographicarealist',type: 'get',
	    		    success: function(result){
	               // alert(result);
	                $("#showgmltable").html("");
	                console.info(result);
	    		    	result.forEach(function(geographicarea){
							var lenAreaCodes = geographicarea.areaCodes.length;
							//console.info(lenAreaCodes);
							for(var i =0 ; i<lenAreaCodes;i++){
								$("#showgmltable").append("<tr><td><input name = 'areaCode_gmlid' type='radio'  onclick='loadAreaFromDB()' value = '"+  geographicarea.areaCodes[i].areaCode +","+ geographicarea.gmlId +"'>"+ geographicarea.areaCodes[i].areaCode  +"</td><td>"+ geographicarea.area_type +"</td><td> Pending for IDE Approval </td></tr> "); 
								
							}
							if(lenAreaCodes > 0){
							var gmlFeatures = getFeaturesfromGML(window.atob(geographicarea.uploaded_file));
							gmlDBMap.set(geographicarea.gmlId,gmlFeatures);
							}
	    		    		  
	    		    		});


	    		    	showDDPPolygonList();
	    		    	 $("#showgmldiv").show();
	                
	            }});
		 } 
	
		 function loadAreaFromDB(){
			 mode = 'gmledit';
			 var areaCodeGmlId = $("input[name='areaCode_gmlid']:checked").val();
			 var areaCodeGmlIdSplt = areaCodeGmlId.split(",");
			// console.info(areaCodeGmlIdSplt[1]);
			 geographicAreaPolygonList.clear();

			 var featureList = gmlDBMap.get(parseInt(areaCodeGmlIdSplt[1])).featureList; //gmlFeatures.featureList;
			// console.info(featureList);
				featureList.forEach(function(feature){
					geographicAreaPolygonList.set(feature.get("areaId"),feature);
					
				});
				editPolygonCoordinates(areaCodeGmlIdSplt[0]);
				$("#showallbtn").hide();
				//console.info(areaCodeGmlId);

				
			 }


			
	var DDPDBMap = new Map(); 
	function showDDPPolygonList(){
		 $.ajax({url: 'getDDPPolygons',type: 'get',
 		    success: function(result){
             //alert(result);
            // $("#showddppolygontable").html("");
            // console.info(result);
 		    	result.forEach(function(ddppolygon){
						
						//	$("#showddppolygontable").append("<tr><td><input name = 'areaCodeddp' type='radio'  onclick='loadAreaFromDDPDB()' value = '"+  ddppolygon.area_id +"'>"+ ddppolygon.area_id  +"</td><td> &nbsp; &nbsp; &nbsp;"+ ddppolygon.caption +"</td><td>"+ getWaterType(ddppolygon.type) +"</td></tr> "); 
							
						//$("#showgmltable").append("<tr><td><input name = 'areaCodeddp' type='radio'  onclick='loadAreaFromDDPDB()' value = '"+  ddppolygon.area_id +"'>"+ ddppolygon.area_id  +"</td><td> &nbsp; &nbsp; &nbsp;"+ getWaterType(ddppolygon.type) +"</td><td> Updated in DDP</td></tr> "); 
						$("#showgmltable").append("<tr><td><input name = 'areaCode_gmlid' type='radio'  onclick='loadAreaFromDDPDB()' value = '"+  ddppolygon.area_id +"'>"+ ddppolygon.area_id  +"</td><td>"+ getWaterType(ddppolygon.type) +"</td><td> Updated in DDP</td></tr> "); 
						
						
						 var ddpFeature = new ol.Feature({
					 
					  geometry: new ol.geom.Polygon(ddppolygon.poslist.coordinates),					  
					  areaId : ddppolygon.area_id,
					  areatype : getWaterType(ddppolygon.type),
					  areaDescription:ddppolygon.caption
				  });
	
						// ddpFeature.getGeometry().transform('EPSG:4326', 'EPSG:3857');
						 
						
						DDPDBMap.set(ddppolygon.area_id,ddpFeature);
						//}
 		    		  
 		    		});
 		    	//$("#showddppolygondiv").show();
             
         }});

	}

	function loadAreaFromDDPDB(){
		 mode = 'ddpedit';
		 //var areaCodeDDP = $("input[name='areaCodeddp']:checked").val();
		 var areaCodeDDP = $("input[name='areaCode_gmlid']:checked").val();		 
		 geographicAreaPolygonList.clear();
		 geographicAreaPolygonList.set(areaCodeDDP,DDPDBMap.get(areaCodeDDP));
		 editPolygonCoordinates(areaCodeDDP);
		 $("#showallbtn").hide();
		 $("#showonmapbtn").show();
		}

	function styleFunctionGA(feature){
		//console.info(map.getView().getZoom());
		 return [
			    new ol.style.Style({			        
			    	stroke: new ol.style.Stroke({
			              color: 'red'
			            }),
			      text: new ol.style.Text({
			        font: '14px Calibri,sans-serif',
			        fill: new ol.style.Fill({ color: '#000' }),
			        stroke: new ol.style.Stroke({
			          color: '#fff', width: 4
			        }),
			        // get the text from the feature - `this` is ol.Feature
			        // and show only under certain resolution
			        //text: map.getView().getZoom() > 5 ? feature.get('areaId') : '',
			        text: feature.get('areaId'),
			       // offsetX: -20,
			       // offsetY: 20,
			        overflow: true
			      })
			    })
			  ];

		}


	var waterPolygonType = new Map();
	waterPolygonType.set("internalwaters","InternalWaters");
	waterPolygonType.set("territorialsea","TerritorialSea");
	waterPolygonType.set("seawardareaof1000nm","SeawardAreaOf1000NM");
	waterPolygonType.set("customcoastalareas","CustomCoastalAreas");
	waterPolygonType.set("customcoastal","CustomCoastalAreas");
	waterPolygonType.set("coastalsea","SeawardAreaOf1000NM");
	
	waterPolygonType.set("internalwaters","InternalWaters");
	
	function getWaterType(typeIn){
		typeIn = typeIn.replace(/\s/g,'').toLowerCase();
		return waterPolygonType.get(typeIn);

	}
	
	function downloadGML(filename, text) {
	    var element = document.createElement('a');
	    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	    element.setAttribute('download', filename);

	    element.style.display = 'none';
	    document.body.appendChild(element);

	    element.click();

	    document.body.removeChild(element);
	}

	function generateGMLforDownload(){
		if (gmlContent.length > 0) downloadGML("gml.xml", gmlContent);
		
	}

	 
 $(document).ready(function(){	 
	 
	 
     $(".add-row").click(function(){
         var markup = "<tr><td><input type='text' maxlength='2' size='2' id = 'latDeg' class = 'integercoordinate latitudedegree' >&nbsp;<input type='text' maxlength='2' size='2' id = 'latMin' class = 'integercoordinate latitudeminute' value = 0 ><select id = 'latDirection'> "+
         "<option value = 1 >North</option><option value = -1>South</option></select></td><td>  <input type='text' maxlength='3' size='3' id = 'lonDeg' class ='integercoordinate longitudedegree'><input type='text' maxlength='2' size='2' id = 'lonMin' class = 'integercoordinate longitudeminute' value = 0 ><select id = 'lonDirection'>"+
         "<option value = 1>East</option><option value = 1>West</option></select></td><td><button class= 'delete-row' >Remove</button></td></tr>"
         $("#polygontable").append(markup);
     });  
   
  
     $("#polytable").on('click','.delete-row',function(){
    	 var lonDegValues = $("input[id='lonDeg']")
         .map(function(){return $(this).val();}).get();
         if(lonDegValues.length > 3){
          $(this).parent().parent().remove();             
         }
     });

	
     
     $('input[type="file"]').change(function(e){
    	 $("#polygondiv").hide();
    	 $("#showpolygondiv").hide();
		  $("#addpolygonbtn").hide();
		  	
         
         var fileName = e.target.files[0].name;
         if ( ! window.FileReader ) {
 			return alert( 'FileReader API is not supported by your browser.' );
 		}
 		var $i = $( '#file' ), // Put file input ID here
 			input = $i[0]; // Getting the element from jQuery
 		if ( input.files && input.files[0] ) {
 			file = input.files[0]; // The file
 			fr = new FileReader(); // FileReader instance
 			fr.onload = function () {
 				// Do stuff on onload, use fr.result for contents of file
 				//$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
 				//alert(fr.result);
 				fileContent = fr.result;
 				mode = 'fileupload';
 				formFillFromGML(fileContent);
 				$("#geographicalareatype").attr('disabled', true);
 				
 			};
 			fr.readAsText( file );
 			//fr.readAsDataURL( file );
 		} else {
 			// Handle errors here
 			alert( "File not selected or browser incompatible." )
 		}         
     });

     
     $(".add-row").trigger("click");
     $(".add-row").trigger("click");
     $(".add-row").trigger("click");
     resetFooterButtons();   

     $("#geographicalareatype").change(function(){
    	 generateAreaCodeByPolygontype();
    	}); 
  
 });    
 
  </script> 
 
