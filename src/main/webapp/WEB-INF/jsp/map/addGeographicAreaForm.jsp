<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Add Geographic Area</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">
<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>

<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
 --%>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!--  Map Dependency -->
	
	<spring:url value="/resources/ol/ol.js" var="oljs" />
	<script src="${oljs}"></script>
	
	<!-- for Advanced GeoSpecial functions turf min js used-->
	<spring:url value="/resources/ol/turf.min.js" var="turfminjs" />
	<script src="${turfminjs}"></script>
	
	<spring:url value="/resources/css/ol.css" var="olcss" />
	<link rel="stylesheet" href="${olcss}">

<spring:url value="/resources/js/sarsurpicRequest.js" var="sarsurpicRequestjs" />
	<script src="${sarsurpicRequestjs}"></script>

<style>
/* .side-box {
	margin-bottom: 0px;
	/* height: 200px;
} */
</style>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<script type="text/javascript">

</script>
</head>
<body id="img" class="hold-transition skin-blue sidebar-mini" onload="onloadFunction()">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
<div class="row">
					<div class="col-md-10">
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
						
						<div id="returnmessage"></div>
						
						<div class="box box-info">

							<div class="box-header with-border">
								<h3 class="box-title">Add Geographical Area</h3>
							</div>







<!-- content -->              
<div class="box-body">
<div class="form-group" align="center">

<table class="table table-bordered">
<tr>
<td> <input type="radio" name="option" id= "add" value="add" onclick="optionclickevent();">Add Geographic Area</td>
<td> <input type="radio" name="option" id= "edit" value="edit" onclick="optionclickevent();">Edit Geographic Area</td>
</tr>
</table>
<div id="addgml" >
<table class="table table-bordered">
<tr><td width="50%">
 <input type="radio" name="drawOption" id= "upload" value="fileupload" onclick="radiobuttonclickevent();">Upload GML
 </td>
<td>
 <input type="radio" name="drawOption" id= "insert" value="insertbytextfields" onclick="radiobuttonclickevent();"> Input Coordinates<br>
</td></tr>
<tr>
<td colspan="2">
 <!--    For Upload  -->  
  <div id = "uploaddiv">
  <table style="width:100%" >
  <tr><td >Select File <input type="file" id="file"></td></tr>  
  </table>
</div>
</td>

</tr>



 <!-- <tr><td>  
 Area Name: <input type="text" id ="areaname"  maxlength="50" size="10"> 
 </td></tr>  -->
 <tr><td > 
  <div id = "areatypediv"><table style="width:100%" ><tr><td >
 Area Type: </td><td><select id = 'geographicalareatype' class ='form-control'>
   <option value = "InternalWaters">InternalWaters</option>
   <option value = "TerritorialSea">TerritorialSea</option>
   <option value = "SeawardAreaOf1000NM">SeawardAreaOf1000NM</option>
   <option value = "CustomCoastalAreas">CustomCoastalAreas</option>
   </select>   
 </td></tr></table>  
 </div>
 </td></tr>
 </table>
 </div> 
 
 
 <!--    For show Saved Polygons  -->  
 <div id = "showgmldiv" class="box box-primary"  >    
 
  <table id = "table_id" class="table table-bordered table-striped"	style="width: 100%">
  <thead>   
   <tr><th></th><th>Area Code</th><th>Area Type</th><th>GMLFile ID</th><th>Left Bottom</th><th>Right Top</th><th>Created Date</th><th>Status</th></tr>     
     </thead>
  <tbody id ="showgmltable"></tbody>   
  </table>
  
</div> 
 
 
<!--    For show Saved Polygons  --> 
<div id = "showpolygondiv"> 
	<table style="width:100%" class="table table-bordered">
		<thead><tr><th>Polygon Areas</th></tr></thead>
		<tbody>
 			<tr><td>Polygon Name </td></tr>  
  			 <tr><td>
  				<div style="height: 100px; overflow-y: auto;">
  					<table style="width:100%" class="table table-bordered">
   						<tbody id ="showpolygontable"></tbody>
  					</table>
  				</div>
  			</td></tr>
		</tbody>
	</table>
</div>

<!--    For Polygon  -->  
  <div id = "polygondiv" style="max-width:1400px" class="box box-body">
  
  <table style="width:100%" >
  <thead><tr><th>Area Code:</th><th> <input type="text" id ="areaname"  maxlength="50" size="10"> </th><th></th></tr>
  <tr><th>Caption:</th><th><input type="text" id ="description"  maxlength="255" size="20"> </th></tr>
  <tr><th>Polygon Points <button class ="add-row" >Add row</button></th></tr></thead>
  </table>
  <div class="box">
  <table style="width:100%" class="table-striped">
  <thead><tr><th style="text-align:center">Latitude</th><th style="text-align:center">Longitude</th><th></th></tr>
  <tr><th style="width:45%"><div class="col-sm-3">Degree</div><div class="col-sm-3">Minute</div><div class="col-sm-3"></div><div class="col-sm-3">DecimalDegree</div></th><th style="width:43%"><div class="col-sm-3">Degree</div><div class="col-sm-3">Minute</div><div class="col-sm-3"></div><div class="col-sm-3">DecimalDegree</div></th><th>&nbsp;&nbsp;&nbsp;&nbsp;</th></tr>
  </thead></table>
  <div style="height: 150px; overflow-y: auto;" class="box">
  <table style="width:100%" id='polytable' class="table table-bordered table-striped coordinate-edit">
   
   <tbody id ="polygontable">
    
  </tbody> 
  </table>
  </div>
  </div>
</div>
</div>    
</div>          
<div class="box-footer">
      <button type="button" class="btn btn-default pull-left" id ="resetbtn" onclick="reset();">Reset</button>
      <button type="button" id ="addpolygonbtn" onclick="addPolygon();" class="btn btn-primary pull-right" >Add Polygon</button>&nbsp;&nbsp;
      <button type="button" id ="savebtn" onclick="applyGeographicArea();" class="btn btn-primary pull-right" >Save in DB</button>&nbsp;&nbsp;
      <button type="button" id ="showallbtn" onclick="showAllPolygonFromDraft();" class="btn btn-primary pull-right" data-dismiss="modal">Show All</button>&nbsp;&nbsp;
      <button type="button" id ="showonmapbtn" onclick="showPolygonFromDraft();" class="btn btn-primary pull-right" >Show on Map</button>&nbsp;&nbsp;   
 </div>
              

 <script>
var sourceCustomPolygon = window.opener.sourceCustomPolygon;
var map = window.opener.map;
var geographicAreaPolygonList =new Map();
var extentThreshold = window.opener.extentThreshold;

var vectorSource_Temp = new ol.source.Vector(
			{              
				 wrapX: false,
		    	 noWrap: true
		    });
 
 $("#areaname").attr('disabled', true); 
 $("#uploaddiv").hide();
 $("#polygondiv").hide(); 
 $("#showpolygondiv").hide(); 
 
 $("#addgml").hide();
 $("#showgmldiv").hide();
 
 $("#editoptiondiv").hide();
 $("#showddppolygondiv").hide();

 
 
 var mode=''; 

 function radiobuttonclickevent(){
	 resetFooterButtons();
	 geographicAreaPolygonList.clear();	
	 var radioValueInput = $("input[name='drawOption']:checked").val();
	 if (radioValueInput === "fileupload"){
		 $("#resetbtn").hide();		 
		    $("#uploaddiv").show();
		    $("#polygondiv").hide();
		    $("#addpolygonbtn").hide();
		    $("#areatypediv").hide();	
		    $(".add-row").hide();	    
		    mode = 'fileupload';
	 }
	 else if (radioValueInput === "insertbytextfields"){
		 $("#resetbtn").show();
		 $(".add-row").show();
	    	$("#uploaddiv").hide();	    	
	    	$("#polygontable").html("");
	    	$(".add-row").trigger("click");
	        $(".add-row").trigger("click");
	        $(".add-row").trigger("click");
	        $(".add-row").trigger("click");
	    	$("#polygondiv").show();
	    	$("#areaname").val("");
	    	$("#description").val("");	    	
	    	$("#savebtn").show();
	    	$("#showonmapbtn").show();
	    	$("#addpolygonbtn").show();
	    	$("#areatypediv").show();
	    	 mode = 'input';
	    	 $("#polygondiv :input").attr("disabled", false);	
	    	 $("#areaname").attr('disabled', true); 
	    	 generateAreaCodeByPolygontype();      
	    }
	 $("#showpolygondiv").hide();	
	 $("#file").val("");     	
	 $("#geographicalareatype").attr('disabled', false);
 }


 function optionclickevent(){
	 $("#showpolygondiv").hide();
	 $("#polygondiv").hide(); 
	 resetFooterButtons(); 
	 var radioValueInput = $("input[name='option']:checked").val();
	 if (radioValueInput === "add"){
		 $("#apply").hide();
		    $("#addgml").show();
		    $("#showgmldiv").hide();
		    //$("#showddppolygondiv").hide();
		    $("#editoptiondiv").hide();
		    radiobuttonclickevent();
		  
			  
		   
	 }
	 else if (radioValueInput === "edit"){
		 $("#addgml").hide();
		 $("#editoptiondiv").show();
		 $(".add-row").show();	
	    		    	
	    	showGMLList();	
	    	 //mode = 'gmledit'; 
	    	 mode = 'edit';
	    	 $("#areaname").attr('disabled', true);  
	    	 
    	 
	 }
	
 
}
 
function resetFooterButtons(){
	 $("#addpolygonbtn").hide();
	 $("#savebtn").hide();
	 $("#showallbtn").hide();
	 $("#showonmapbtn").hide();
	 $("#resetbtn").hide();
	 
} 

function reset(){
	if(mode ==="input"){
		$("#polygontable").html("");
    	$(".add-row").trigger("click");
        $(".add-row").trigger("click");
        $(".add-row").trigger("click");
        $(".add-row").trigger("click");
    	$("#polygondiv").show();
    	$("#description").val("");
		}
}

 var fileContent;
 var gmlContent="";
 
 function applyGeographicArea(){
	 if(saveGeographicArea()){ //To save latest coordinates. 	
	 	var gmlId=0;
	 	var polygonToSave = geographicAreaPolygonList.values();
	 	 if (mode === "gmledit"){
	 		var areaCodeGmlId = $("input[name='areaCode_gmlid']:checked").val();
			 var areaCodeGmlIdSplt = areaCodeGmlId.split(",");
			 gmlId = areaCodeGmlIdSplt[1];
			 }else{
				 polygonToSave=[];
				 $('input[name="geographicarea"][value="' + $("#areaname").val() + '"]').prop('checked', true);
				 var selecteAreaIDs = $("#showpolygontable input:checkbox:checked").map(function(){
				      return $(this).val();
				    }).get();
				    for(var j =0 ; j<selecteAreaIDs.length;j++){
				    	polygonToSave.push(geographicAreaPolygonList.get(selecteAreaIDs[j]));
					}	
				
			}
	 	var data = '';
	 	var areaCodes = [];		
		//var features = sourceCustomPolygon.getFeatures();	
			for (v of polygonToSave) {
				 // console.log(v)
				  data= data + writeFeatureAsGML(v);
				  var polygon = turf.polygon(v.getGeometry().getCoordinates());
				  var bbox = turf.bbox(polygon);
				  console.info(bbox);	
					var areas = {
							gmlId :0,
							areaCode : v.get("areaId"),
							left_bottom_longitude : bbox[0],
							left_bottom_latitude : bbox[1],
							right_top_longitude : bbox[2],
							right_top_latitude : bbox[3]
							}	
				  			  
				  areaCodes.push(areas);				 
				}			
		//alert(data);
	 if(data){
		 
	 //var areaId =  $("#areaname").val();
	 var areaType =$("#geographicalareatype").val();
	 var gmldataHeader ='<?xml version="1.0" encoding="utf-8"?><gml:MultiSurface xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/gml/3.2 http://schemas.opengis.net/gml/3.2.1/gml.xsd" gml:id="'+ areaType +'"><gml:surfaceMembers>';
	 var gmldataFooter = '</gml:surfaceMembers></gml:MultiSurface>';
	 data = gmldataHeader + data + gmldataFooter;

	 gmlContent = data;
	 
		var gmldata = { 
				gmlId : gmlId,
			  	fileType :"gml",
			  	data : data,
			  	//areaId : areaId,
			  	polygonAreas : areaCodes,
			  	areaName : areaId,
			  	areaType : areaType	
				};
	 }
		if(gmldata){
	    	  console.info(gmldata);
	    	  $.ajax({url: 'uploadFile' ,type: 'post',
	    		    contentType: 'application/json',
	    		    data: JSON.stringify(gmldata) ,success: function(result){
	                //alert(result);	
	              //  $("#edit").trigger("click"); 

	    		    $("#returnmessage").html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Geographic Area Added and Pending for IDE Approval (GML File ID : "+ result +")</div>");
	  	                             
	            }}); 
  	  }	
		generateGMLforDownload();
		showPolygonFromDraft();
		//showSelectedPolygonFromDraft();
		
	 }
	 }


 	function writeFeatureAsGML(feature){
 			areaId = feature.get("areaId");//'GAIW1234_1'; //test
 			areaDescription = feature.get("areaDescription");//'Area 1'; //test
			var posList = getPositionListString(feature);
 			//posList='7.25 53.32 7.19 53.31 7.01 53.33 6.97 53.37 6.91 53.43 6.88 53.45 6.81 53.49 6.77 53.51 6.74 53.53 6.68 53.55 6.61 53.58 6.55 53.61 6.64 53.62 6.72 53.65 6.8 53.67 6.83 53.67 7.05 53.69 7.16 53.72 7.32 53.73 7.35 53.73 7.4 53.74 7.61 53.76 7.69 53.78 7.76 53.78 7.81 53.79 8.34 53.96 8.57 54.3 7.25 53.32'; //test
			
			var gmlText='<gml:Polygon gml:id="'+areaId+'"><gml:description>'+areaDescription+'</gml:description><gml:exterior><gml:LinearRing><gml:posList>'+posList+'</gml:posList></gml:LinearRing></gml:exterior></gml:Polygon>';			
			return gmlText;
  	}

 	function getPositionListString(feature){
			var posList='';
			var featureLocal = feature.clone();	
	        var latlon = featureLocal.getGeometry().getCoordinates();	        
	        for(var i = 0; i < latlon.length; i++) {
	            var row = latlon[i];
	            for(var j = 0; j < row.length; j++) {
	            	posList = posList +' '+ (row[j][0]).toFixed(2) +' ' +(row[j][1]).toFixed(2);
	            }
	        }			
			return posList;
 	}
 	 	

 		
		
			
		 function generatePolygonFeatureFromInputForm(){
	    	  
	    	 var areaname = $("#areaname").val();
	    	 var isValidFlag = true;
	    	 if(areaname.trim().length >0){

		      var areatype = $("#geographicalareatype").val();
	    	 
	    	  var latDegValues = $("input[id='latDeg']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var latMinValues = $("input[id='latMin']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var latDirection = $("select[id='latDirection']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonDegValues = $("input[id='lonDeg']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonMinValues = $("input[id='lonMin']")
	          .map(function(){return $(this).val();}).get();
	    	  
	    	  var lonDirection = $("select[id='lonDirection']")
	          .map(function(){return $(this).val();}).get();
	        
	    	  var latOut=[];
	    	  var lonOut=[];
	    	  var coordinates=[];
	    	  for(var i =0 ; i < latDegValues.length;i++){
				var longitudeValue= DegMinToLatLon(lonDegValues[i],lonMinValues[i],lonDirection[i]);
				var latitudeValue = DegMinToLatLon(latDegValues[i],latMinValues[i],latDirection[i]);
				//Validation Check---
				if ((isNaN(longitudeValue)) || (isNaN(latitudeValue))) 	isValidFlag = false;
				if ((lonDegValues[i] < 0) || (lonDegValues[i] >180)) 	isValidFlag = false;
				if ((latDegValues[i] < 0) || (latDegValues[i] >90)) 	isValidFlag = false;
		    	  
	    		  coordinates.push([longitudeValue,latitudeValue]);	 
	    	  }	 
			
	    	  if (isValidFlag){  
	    		//-- Restrictions after valid entries of coordinates.
	    			var errormsg ="NOERROR";
					var coordLen = coordinates.length;				
					if((coordinates[0][0] != coordinates[coordLen-1][0]) || (coordinates[0][1] != coordinates[coordLen-1][1])){
						//alert("First co-ordinate and Last co-ordinate values should be same ");
						errormsg = "First co-ordinate and Last co-ordinate values should be same ";						
					}
					if(coordLen < 4){
						errormsg = "No. of co-ordinates should be 4 OR more";
					}	
					if (errormsg === "NOERROR")	{
						} else{
							alert(errormsg);
							return;
					}				
		    	//--
		    	   	 
	          var areaId=areaname;
	          var areaDescription=$("#description").val();;
	          var polygon = new ol.geom.Polygon([coordinates]);
	          //polygon.transform('EPSG:4326', 'EPSG:3857')
	    		var feature = new ol.Feature(polygon);
	    		feature.setProperties({areatype: areatype,areaId:areaId,areaDescription:areaDescription});    				    		
	    		return feature;	
	    	  }
	    	  else{
	    		  alert("Please Enter Valid inputs for Latitude and Longitude ");
		      }
	    	 } 
	    	 else{
	    		 alert("Please Enter Area Code ");
	  		 }   	  
	      }

		 
		/* function showPolygonsList(){
				$("#showpolygontable").html("");
				for (v of geographicAreaPolygonList.values()) {
					 // console.log(v)
					$("#showpolygontable").append("<tr><td><input name = 'geographicarea' type='radio' onclick='editPolygonFromDraft()'  value = "+ v.get("areaId") +">"+v.get("areaId") +"</td></tr> "); 
					if((mode === "input")||(mode === "fileupload")) $("#showpolygondiv").show();
					}
				//console.info(mode);
				
						
			} */


		 function showPolygonsList(){
				$("#showpolygontable").html("");
				for (v of geographicAreaPolygonList.values()) {
					 // console.log(v)
					$("#showpolygontable").append("<tr><td><input name = 'geographicarea' type='checkbox' onclick='editPolygonFromDraft()'  value = "+ v.get("areaId") +">&nbsp;&nbsp;"+v.get("areaId") +"</td></tr> "); 
					if((mode === "input")||(mode === "fileupload")) $("#showpolygondiv").show();
					}
				//console.info(mode);
				
						
			}

		 function editPolygonFromDraft(){				
				var areaName = $("input[name='geographicarea']:checked").val();
				if(!areaName){
					$("#showonmapbtn").hide();
					$("#addpolygonbtn").hide();
				}
				editPolygonCoordinates(areaName);
				if((mode === "input")||(mode === "fileupload")) $("#showallbtn").show();
		 }
		 
		 function saveGeographicArea(){			
				var featureLocal = generatePolygonFeatureFromInputForm();
				if(featureLocal){				
					geographicAreaPolygonList.set(featureLocal.get("areaId"),featureLocal);	
					return true;
				}else{
					//this.preventDefault();
					return false;
				}		
		}

		function addPolygon(){
			if($("#areaname").val() === "" ){
			//	alert("Please Enter Area Code ");
			} else{
				saveGeographicArea();
				if (geographicAreaPolygonList.size > 0) showPolygonsList();
				$("#polygontable").html("");
		    	$(".add-row").trigger("click");
		        $(".add-row").trigger("click");
		        $(".add-row").trigger("click");
		        $(".add-row").trigger("click");
		    	$("#polygondiv").show();
		    	$("#areaname").val("");
		    	$("#description").val("");
		    	generateAreaCodeByPolygontype();
			}
		}

		var watertype ="";
		var areaidStart ="";
		 function generateAreaCodeByPolygontype(){
			 var idOffset = 0;
			 if (geographicAreaPolygonList.size < 1){				 
			 	watertype = $("#geographicalareatype").val();
			 	$.ajax({url: 'getnextpolygonid?type='+watertype,type: 'get',
		 		    success: function(result){
		 		    	areaidStart = result;
		 		    	$("#areaname").val(result);
		 		  }});
			 }else{
				 $("#geographicalareatype").val(watertype);
				 var areaIdPrefix = areaidStart.substring(0,9);
				 var idNum = parseInt(areaidStart.substring(9));
				 idOffset = geographicAreaPolygonList.size;	
				 $("#areaname").val(areaIdPrefix+""+(idNum+idOffset));			 
			 } 			 
		 }

		 
		 function showPolygonFromDraft(){
			 console.info("showPolygonFromDraft");
			 if (mode === "gmledit" || mode === "ddpedit"){  //edit mode
				 console.info("showPolygonFromDraft");
			 vectorSource_Temp.clear();
			 sourceCustomPolygon.clear();
			 
				if($("#areaname").val().trim().length > 0){ 
					console.info("showPolygonFromDraft");
					if(saveGeographicArea()){
					 showPolygonsList();
					 console.info($("#areaname").val());
						//$('input[name="geographicarea"][value="' + $("#areaname").val() + '"]').prop('checked', true);
						//$('input[name="geographicarea"]').attr('checked', true).trigger('click');
						//var areaName = $("input[name='geographicarea']:checked").val();
						var areaName = $("#areaname").val();
						console.info(areaName);
				 		$("#areaname").attr('disabled', true); 				
						if(geographicAreaPolygonList.size >0){
							if(geographicAreaPolygonList.get(areaName)){
								var featureTemp = geographicAreaPolygonList.get(areaName);
								featureTemp.setStyle(styleFunctionGA(featureTemp));
								sourceCustomPolygon.addFeature(featureTemp);
								 vectorSource_Temp.addFeature(featureTemp);
								 zoomtoFeatureExtent(vectorSource_Temp);
							}else{
						 		alert("Select Polygon from List");
							}
						}else{
							// alert("Add Polygon First");
						}
					}
				}	

			 } //editmode end
			 else{
				 showSelectedPolygonFromDraft();
				 }		
				
			}	

		 function showSelectedPolygonFromDraft(){
			 vectorSource_Temp.clear();
			 sourceCustomPolygon.clear();			
				//console.info(areaName);
				// $("#areaname").attr('disabled', true); 
			 var selecteAreaIDs = $("#showpolygontable input:checkbox:checked").map(function(){
					      return $(this).val();
					    }).get();
			 if($("#areaname").val().trim().length > 0){ 
				 
					saveGeographicArea();
					showPolygonsList();
					for(var j =0 ; j<selecteAreaIDs.length;j++){
						$('input[name="geographicarea"][value="' + selecteAreaIDs[j] + '"]').prop('checked', true);
					}
					$('input[name="geographicarea"][value="' + $("#areaname").val() + '"]').prop('checked', true);
			 }	
				
								
				if(geographicAreaPolygonList.size >0){
					
					polygonToSave=[];
					
					    for(var j =0 ; j<selecteAreaIDs.length;j++){
					    	//polygonToSave.push(geographicAreaPolygonList.get(selecteAreaIDs[j]));
					    	var featureTemp = geographicAreaPolygonList.get(selecteAreaIDs[j]);
							featureTemp.setStyle(styleFunctionGA(featureTemp));
							sourceCustomPolygon.addFeature(featureTemp);
							 vectorSource_Temp.addFeature(featureTemp);
							 zoomtoFeatureExtent(vectorSource_Temp);					    	
						}				
					
					
				}else{
					// alert("Add Polygon First");
				}
					
				
			}	
		

			

		 function showAllPolygonFromDraft(){
			 //console.info($("#areaname").val());
			 vectorSource_Temp.clear();
			 if($("#areaname").val().trim().length > 0){ 
				 
					saveGeographicArea();
					showPolygonsList();
					$('input[name="geographicarea"][value="' + $("#areaname").val() + '"]').prop('checked', true);
			 }	
					sourceCustomPolygon.clear();
			 if (geographicAreaPolygonList.size >0){
				 for (v of geographicAreaPolygonList.values()) {
					 $('input[name="geographicarea"][value="' + v.get("areaId") + '"]').prop('checked', true);	
					 
						v.setStyle(styleFunctionGA(v));
						sourceCustomPolygon.addFeature(v);
						vectorSource_Temp.addFeature(v);
					}
				 zoomtoFeatureExtent(vectorSource_Temp);
			 }else{
				 alert("Add Polygon First");
			 }
				 
		
			}			 
				

		function zoomtoFeatureExtent(vectorSource_Temp){
			 var extent = vectorSource_Temp.getExtent();	
			 console.info(extent);
			 if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
			 { 
	    		 var diffExtent = extent[2]- extent[0];
	    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
	    		 {
	    			 var center = ol.extent.getCenter(extent);							
	    			 map.getView().setCenter(center);
	    		 }
	    		 else
	    			 map.getView().fit(extent, map.getSize());	    		
			 }	
			// window.opener.to_blur();	
			//here();
		}
		 	 
		function formFillFromGML(gml){
			geographicAreaPolygonList.clear();
			var gmlFeatures = getFeaturesfromGML(gml);
			if(gmlFeatures){
				//console.info(gmlFeatures);
				$("#geographicalareatype").val(gmlFeatures.areaType);
				var featureList = gmlFeatures.featureList;
				featureList.forEach(function(feature){
					geographicAreaPolygonList.set(feature.get("areaId"),feature);
				});				
				showPolygonsList();	
			} 	
		}


	function getFeaturesfromGML(gml){
		try{
		featureList = [];
		gmlwithoutNS = gml.replace(/gml:/g, "");
		 $xml = $( $.parseXML( gmlwithoutNS ) );
		 var areaType=$xml.find("MultiSurface").attr("id");
		 
		 //console.info(areaType); 
	 	 $xml.find("Polygon").each(function(){				
				var areaId =$(this).attr("id");
				var areaDescription =$(this).find("description").text();
				//console.info($(this).attr("id"));
	 			//console.info(areaDescription);
				var poslist = $(this).find("posList").text().replace( /\n/g, " " ).trim().split( " " );
				var poslistfiltered = poslist.filter(function (el) { 
					return el != '';				
				}); 				
				var lonlat=[];					
				for(var i= 0;i<poslistfiltered.length;i++){
					lonlat.push([ parseFloat(poslistfiltered[i]), parseFloat(poslistfiltered[++i])]);					
				}				
				var polygon = new ol.geom.Polygon([lonlat]);
	          //polygon.transform('EPSG:4326', 'EPSG:3857');
	    		var feature = new ol.Feature(polygon);
	    		feature.setProperties({areatype: areaType,areaId:areaId,areaDescription:areaDescription});  	    		  				    		
				featureList.push(feature);
				//console.info(feature);
	 		 });	

			if(areaType){
				var gmlFeatures = {
							areaType : areaType,
							featureList : featureList
						};

				return gmlFeatures;
			}else{
				alert("Not a Valid GML");
				}
		} catch(err){ console.info("XML Parsing Error");
			alert("Not a Valid GML");
		}
		}

		
		function editPolygonCoordinates(areaName){				
				//var areaName = $("input[name='geographicarea']:checked").val();
				 $("#polygondiv").hide();
				$("#areaname").val(areaName);				
						feature = geographicAreaPolygonList.get(areaName);
						//console.info("CALL : "+ feature +" "+areaName);
						if (feature) {
							
			    	        var featureL = feature.clone();	
			    	       // var latlon = featureL.getGeometry().transform('EPSG:3857', 'EPSG:4326').getCoordinates();
			    	        var latlon = featureL.getGeometry().getCoordinates();
			    	       var areaDescription =featureL.get("areaDescription");
			    	       $("#geographicalareatype").val(featureL.get("areatype"));
			    	       if(areaDescription) {
				    	       $("#description").val(areaDescription);
			    	       }else $("#description").val("");
			    	        var i;
			    	        var lon=[];
			    	        var lat=[];
			    	       // console.info(latlon);
			    	        featureTemp =latlon;
			    	        $("#polygontable").html("");
			    	        for(var i = 0; i < latlon.length; i++) {
			    	            var row = latlon[i];
			    	            for(var j = 0; j < row.length; j++) {
			    	               // console.info("row[" + i + "][" + j + "] = " + row[j][0]);
									[latDeg ,latMin] = LatLonToDegMin(row[j][1]);  
						            [lonDeg ,lonMin] = LatLonToDegMin(row[j][0]);      
						            var latoptionString = (row[j][1] > 0) ? "<option selected value = 1 >North</option><option value = -1 >South</option>" : "<option value = 1 >North</option><option value = -1 selected>South</option>";			            	   
						            var lonoptionString = (row[j][0] > 0) ? "<option selected value = 1 >East</option><option value = -1 >West</option>" : "<option value = 1 >East</option><option value = -1 selected >West</option>"  ;          	   	
						            			
						           /* var markup = "<tr><td><input type='text'  id = 'latDeg'  class = 'integercoordinate latitudedegree' maxlength='2' size='2' value = "+ latDeg +">&nbsp;<input type='text' id = 'latMin' class = 'integercoordinate latitudeminute' maxlength='2' size='2' value = "+ latMin +
						            	   "><select id = 'latDirection' > "+ latoptionString +
						                   "</select></td><td><input type='text' id='lonDeg' class ='integercoordinate longitudedegree'  maxlength='3' size='3' value = "+ lonDeg +" ><input type='text' id='lonMin' class = 'integercoordinate longitudeminute' maxlength='2' size='2' value = "+ lonMin +"><select  id = 'lonDirection'>"+
						            	  lonoptionString +"</select></td><td><button class= 'delete-row' >Remove</button></td></tr>" */
						            	  var markup = "<tr><td class ='row-no-gutter'><div class='col-sm-3'><input type='text'  id = 'latDeg'  class = 'integercoordinate latitudedegree form-control input-sm' maxlength='2' size='2' value = "+ latDeg +"></div><div class='col-sm-3'><input type='text' id = 'latMin' class = 'integercoordinate latitudeminute form-control input-sm' maxlength='2' size='2' value = "+ latMin +
						            	   "></div><div class='col-sm-3'><select id = 'latDirection' class= 'form-control input-sm' > "+ latoptionString +
						                   "</select></div><div class='col-sm-3'><input type='text' class ='form-control input-sm latdecimal decimalcoordinate' value='"+ row[j][1] +"'></div></div></td><td class ='row-no-gutter'><div class='col-md-3'><input type='text' id='lonDeg' class ='integercoordinate longitudedegree form-control input-sm'  maxlength='3' size='3' value = "+ lonDeg +" ></div><div class='col-sm-3'><input type='text' id='lonMin' class = 'integercoordinate longitudeminute form-control input-sm' maxlength='2' size='2' value = "+ lonMin +"></div><div class='col-sm-3'><select  id = 'lonDirection' class='form-control input-sm'>"+
						            	  lonoptionString +"</select></div><div class='col-sm-3'><input type='text' class ='form-control input-sm londecimal decimalcoordinate' value='"+ row[j][0] +"'></div></td><td><button class= 'delete-row' >Remove</button></td></tr>" 	  
						              $("#polygontable").append(markup);						
			    	            }
			    	        }

			    	        $("#polygondiv").show();
			    	       // $("#polygontable :input").attr("disabled", true);			    	        
			    	       // $("#showallbtn").show();
			    	   	 	$("#showonmapbtn").show();
			    	   	 	$("#savebtn").show(); 
			    	   	 	if(mode ==='fileupload'){
			    	   	 		$("#polygondiv :input").attr("disabled", true);	
					    	}
			    	   	 	else{
					    		$("#polygondiv :input").attr("disabled", false);	
						    }
						    if ((mode ==='gmledit') ||(mode ==='ddpedit'))
						    {	
							    $("#areaname").attr('disabled', true); 
							   // console.info("EDIT"); 
						    }
						    if (mode ==='input')
						    {	
						    	$("#addpolygonbtn").show();
						    }
			    	   	 	           
			    	    }							 				
			}
		

	//----------
		$(".coordinate-edit").on('keydown','.integercoordinate',function(e){
		    var keyPressed;
		    if (!e) var e = window.event;
		    if (e.keyCode) keyPressed = e.keyCode;
		    else if (e.which) keyPressed = e.which;
		    var hasDecimalPoint = (($(this).val().split('.').length-1)>-1); //restrict decimal point
		    if ( keyPressed == 46 || keyPressed == 8 ||((keyPressed == 190||keyPressed == 110)&&(!hasDecimalPoint && !e.shiftKey )) || keyPressed == 9 || keyPressed == 27 || keyPressed == 13 ||
		             // Allow: Ctrl+A
		            (keyPressed == 65 && e.ctrlKey === true) ||
		             // Allow: home, end, left, right
		            (keyPressed >= 35 && keyPressed <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        else {
		            // Ensure that it is a number and stop the keypress
		            if (e.shiftKey || ((keyPressed < 48) || (keyPressed > 57)) && ((keyPressed < 96) || (keyPressed > 105) )) {
		                e.preventDefault();
		            } 
		            
		        }

		  });	

		$(".coordinate-edit").on('keydown','.decimalcoordinate',function(e){
		    var keyPressed;
		    if (!e) var e = window.event;
		    if (e.keyCode) keyPressed = e.keyCode;
		    else if (e.which) keyPressed = e.which;
		    var hasDecimalPoint = (($(this).val().split('.').length-1)>0); //allow decimal point
		    var hasNegativeSign =(($(this).val().split('-').length-1)>0);
		    console.info(e.target.selectionStart);
		    if (((keyPressed == 173 || keyPressed == 109) && (e.target.selectionStart == 0) && !hasNegativeSign) || keyPressed == 46 || keyPressed == 8 ||((keyPressed == 190||keyPressed == 110)&&(!hasDecimalPoint && !e.shiftKey )) || keyPressed == 9 || keyPressed == 27 || keyPressed == 13 ||
		             // Allow: Ctrl+A
		            (keyPressed == 65 && e.ctrlKey === true) ||
		             // Allow: home, end, left, right
		            (keyPressed >= 35 && keyPressed <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        else {
		            // Ensure that it is a number and stop the keypress
		            if (e.shiftKey || ((keyPressed < 48) || (keyPressed > 57)) && ((keyPressed < 96) || (keyPressed > 105) )) {
		                e.preventDefault();
		            } 
		            
		        }

		  });		

			$(".coordinate-edit").on('keyup','.longitudedegree',function(e){
				   if($(this).val()>180) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
				   lonDegVal = $(this).val();
				   if(lonDegVal == 180) $(this).closest('tr').find('.longitudeminute').val(0);
					 $(this).closest('tr').find('.londecimal').val(DegMinToLatLon($(this).closest('tr').find('.longitudedegree').val() , $(this).closest('tr').find('.longitudeminute').val() , $(this).closest('tr').find('#lonDirection').val()));
					
			 });

		  	$(".coordinate-edit").on('keyup','.latitudedegree',function(e){
			     if($(this).val()>90) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
			     var latDegVal = $(this).val();
			      if(latDegVal == 90) $(this).closest('tr').find('.latitudeminute').val(0);
			      //updateDecimalLonLat(e);
				 $(this).closest('tr').find('.latdecimal').val(DegMinToLatLon($(this).closest('tr').find('.latitudedegree').val() , $(this).closest('tr').find('.latitudeminute').val() , $(this).closest('tr').find('#latDirection').val()));
			      
			      
			  }); 
		  
			$(".coordinate-edit").on('keyup','.latitudeminute',function(e){
		     if($(this).val()>59) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
		     var latDegVal = $(this).closest('tr').find('.latitudedegree').val();
		     if	(latDegVal == 90) $(this).val(0);
			 $(this).closest('tr').find('.latdecimal').val(DegMinToLatLon($(this).closest('tr').find('.latitudedegree').val() , $(this).closest('tr').find('.latitudeminute').val() , $(this).closest('tr').find('#latDirection').val()));
		     
		     	
		     	     
		 	 }); 
		  
			$(".coordinate-edit").on('keyup','.longitudeminute',function(e){
			     if($(this).val()>59) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
			     var lonDegVal = $(this).closest('tr').find('.longitudedegree').val();
			     if	(lonDegVal == 180) $(this).val(0);
			     //updateDecimalLonLat(e);
				 $(this).closest('tr').find('.londecimal').val(DegMinToLatLon($(this).closest('tr').find('.longitudedegree').val() , $(this).closest('tr').find('.longitudeminute').val() , $(this).closest('tr').find('#lonDirection').val()));
			     
			  }); 


			$(".coordinate-edit").on('change','#latDirection', function() {
				 $(this).closest('tr').find('.latdecimal').val(DegMinToLatLon($(this).closest('tr').find('.latitudedegree').val() , $(this).closest('tr').find('.latitudeminute').val() , $(this).closest('tr').find('#latDirection').val()));
				//console.info("test");	
				});

				$(".coordinate-edit").on('change','#lonDirection', function() {	
				 $(this).closest('tr').find('.londecimal').val(DegMinToLatLon($(this).closest('tr').find('.longitudedegree').val() , $(this).closest('tr').find('.longitudeminute').val() , $(this).closest('tr').find('#lonDirection').val()));

				});	
			

				$(".coordinate-edit").on('keyup','.latdecimal',function(e){
					if(isNaN($(this).val())){						
					}else{
						   while(($(this).val()>90) || ($(this).val()<-90)) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
						   var degMin = LatLonToDegMin($(this).val());
						   $(this).closest('tr').find('.latitudedegree').val(degMin[0]);
						   $(this).closest('tr').find('.latitudeminute').val(degMin[1]);
						   var dirVal= $(this).val()/ Math.abs($(this).val());
						   $(this).closest('tr').find('#latDirection').val(dirVal)
					}
					   
				 });
				 
				$(".coordinate-edit").on('keyup','.londecimal',function(e){
					if(isNaN($(this).val())){						
					}else{
					   while(($(this).val()>180) || ($(this).val()<-180)) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
					   var degMin = LatLonToDegMin($(this).val());
					   $(this).closest('tr').find('.longitudedegree').val(degMin[0]);
					   $(this).closest('tr').find('.longitudeminute').val(degMin[1]);
					   var dirVal= $(this).val()/ Math.abs($(this).val());
					   $(this).closest('tr').find('#lonDirection').val(dirVal);
					}
				 });	
			
			 
	//---	
	
	var gmlDBMap = new Map(); 
				
		 function showGMLList(){
			 $.ajax({url: 'getGeographicarealist',type: 'get',
	    		    success: function(result){
	               // alert(result);
	               //$("#table_id").DataTable().destroy();
	               if(dBdatatable)  dBdatatable.destroy();
	                $("#showgmltable").html("");
	                console.info(result);
	                var checkedVal = "checked";
	    		    	result.forEach(function(geographicarea){
							var lenAreaCodes = geographicarea.areaCodes.length;
							//console.info(lenAreaCodes);							
							for(var i =0 ; i<lenAreaCodes;i++){								
								$("#showgmltable").append("<tr><td><input name = 'areaCode_gmlid' type='radio'  onclick='loadAreaFromDB()' value = '"+  geographicarea.areaCodes[i].areaCode +","+ geographicarea.gmlId +"'"+ checkedVal +" ></td><td>"+ geographicarea.areaCodes[i].areaCode  +"</td><td>"+ geographicarea.area_type +"</td><td>"+geographicarea.gmlId+"</td><td>["+  geographicarea.areaCodes[i].left_bottom_latitude +"," +  geographicarea.areaCodes[i].left_bottom_longitude +"]</td><td>["+  geographicarea.areaCodes[i].right_top_latitude +","+  geographicarea.areaCodes[i].right_top_longitude+"]</td><td>"+geographicarea.creation_date+"</td><td>Pending for IDE Approval </td></tr> "); 
								checkedVal = "";
							}
							if(lenAreaCodes > 0){
							var gmlFeatures = getFeaturesfromGML(window.atob(geographicarea.uploaded_file));
							gmlDBMap.set(geographicarea.gmlId,gmlFeatures);
							}
	    		    		  
	    		    		});


	    		    	showDDPPolygonList();
	    		    	 $("#showgmldiv").show();
	    		    	
	    		    	 loadAreaFromDB();
	                
	            }});
		 } 
	
		 function loadAreaFromDB(){
			 mode = 'gmledit';
			 var areaCodeGmlId = $("input[name='areaCode_gmlid']:checked").val();
			 var areaCodeGmlIdSplt = areaCodeGmlId.split(",");
			// console.info(areaCodeGmlIdSplt[1]);
			 geographicAreaPolygonList.clear();

			 var featureList = gmlDBMap.get(parseInt(areaCodeGmlIdSplt[1])).featureList; //gmlFeatures.featureList;
			// console.info(featureList);
				featureList.forEach(function(feature){
					geographicAreaPolygonList.set(feature.get("areaId"),feature);
					
				});
				editPolygonCoordinates(areaCodeGmlIdSplt[0]);
				$("#showallbtn").hide();
				//console.info(areaCodeGmlId);

				
			 }

		 function LatLonToDegMin(val){
	    	  var valAbs = Math.abs(val)
	    	  var dlatsplit = valAbs.toString().split('.');
	    	  var min =Math.round( parseFloat("0."+dlatsplit[1]) * 60);   	  
	    	  return [dlatsplit[0] , min];
	      }
	      
	      function DegMinToLatLon(deg , min , sign){    	
	    	  
	    	  return (parseInt(deg) + (min/60))*sign;
	      }

		function getdateTimeString(timestamp){
			var date = new Date(Date.parse(timestamp));			
			return date.toLocaleString("en-GB");
			
		} 
	      
			
	var DDPDBMap = new Map(); 
	function showDDPPolygonList(){
		 $.ajax({url: 'getDDPPolygons',type: 'get',
 		    success: function(result){
 		    	result.forEach(function(ddppolygon){							
					$("#showgmltable").append("<tr><td><input name = 'areaCode_gmlid' type='radio'  onclick='loadAreaFromDDPDB()' value = '"+  ddppolygon.area_id +"'></td><td>"+ ddppolygon.area_id  +"</td><td>"+ getWaterType(ddppolygon.type) +"</td><td></td><td></td><td></td><td></td><td> Updated in DDP</td></tr> "); 
					var ddpFeature = new ol.Feature({					 
					  geometry: new ol.geom.Polygon(ddppolygon.poslist.coordinates),					  
					  areaId : ddppolygon.area_id,
					  areatype : getWaterType(ddppolygon.type),
					  areaDescription:ddppolygon.caption
				  	});					 
						
					DDPDBMap.set(ddppolygon.area_id,ddpFeature);
 		    		  
 		    	});
 		    	dBdatatable =  $('#table_id').DataTable( { 		    		
 		    	  "order": [], 		    		
 		    	  "columnDefs": [
 		    		    { "orderable": false, "targets": 0 }
 		    	   ]	,	    		 		    	
 		           "scrollY":"250px",
 		           "scrollCollapse": true,
 		           "paging":false
 		       });
         }});

	}

	var dBdatatable;
	
	function loadAreaFromDDPDB(){
		 mode = 'ddpedit';
		 //var areaCodeDDP = $("input[name='areaCodeddp']:checked").val();
		 var areaCodeDDP = $("input[name='areaCode_gmlid']:checked").val();		 
		 geographicAreaPolygonList.clear();
		 geographicAreaPolygonList.set(areaCodeDDP,DDPDBMap.get(areaCodeDDP));
		 editPolygonCoordinates(areaCodeDDP);
		 $("#showallbtn").hide();
		 $("#showonmapbtn").show();
		}

	function styleFunctionGA(feature){
		//console.info(map.getView().getZoom());
		 return [
			    new ol.style.Style({			        
			    	stroke: new ol.style.Stroke({
			              color: 'red'
			            }),
			      text: new ol.style.Text({
			        font: '14px Calibri,sans-serif',
			        fill: new ol.style.Fill({ color: '#000' }),
			        stroke: new ol.style.Stroke({
			          color: '#fff', width: 4
			        }),
			        // get the text from the feature - `this` is ol.Feature
			        // and show only under certain resolution
			        //text: map.getView().getZoom() > 5 ? feature.get('areaId') : '',
			        text: feature.get('areaId'),
			       // offsetX: -20,
			       // offsetY: 20,
			        overflow: true
			      })
			    })
			  ];

		}


	var waterPolygonType = new Map();
	waterPolygonType.set("internalwaters","InternalWaters");
	waterPolygonType.set("territorialsea","TerritorialSea");
	waterPolygonType.set("seawardareaof1000nm","SeawardAreaOf1000NM");
	waterPolygonType.set("customcoastalareas","CustomCoastalAreas");
	waterPolygonType.set("customcoastal","CustomCoastalAreas");
	waterPolygonType.set("coastalsea","SeawardAreaOf1000NM");
	
	waterPolygonType.set("internalwaters","InternalWaters");
	
	function getWaterType(typeIn){
		typeIn = typeIn.replace(/\s/g,'').toLowerCase();
		return waterPolygonType.get(typeIn);

	}
	
	function downloadGML(filename, text) {
	    var element = document.createElement('a');
	    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	    element.setAttribute('download', filename);

	    element.style.display = 'none';
	    document.body.appendChild(element);

	    element.click();

	    document.body.removeChild(element);
	}

	function generateGMLforDownload(){
		if (gmlContent.length > 0) downloadGML("gml.xml", gmlContent);
		
	}

	 
 $(document).ready(function(){	 
	 
	 
     $(".add-row").click(function(){        
       
        var markup = "<tr><td class ='row-no-gutter'><div class='col-sm-3'><input type='text'  id = 'latDeg'  class = 'integercoordinate latitudedegree form-control input-sm' maxlength='2' size='2'></div><div class='col-sm-3'><input type='text' id = 'latMin' class = 'integercoordinate latitudeminute form-control input-sm' maxlength='2' size='2' ></div><div class='col-sm-3'><select id = 'latDirection' class= 'form-control input-sm' ><option selected value = 1 >North</option><option value = -1 >South</option></select></div><div class='col-sm-3'><input type='text' class ='form-control input-sm latdecimal decimalcoordinate' ></div></div></td><td class ='row-no-gutter'><div class='col-md-3'><input type='text' id='lonDeg' class ='integercoordinate longitudedegree form-control input-sm'  maxlength='3' size='3'></div><div class='col-sm-3'><input type='text' id='lonMin' class = 'integercoordinate longitudeminute form-control input-sm' maxlength='2' size='2' ></div><div class='col-sm-3'><select  id = 'lonDirection' class='form-control input-sm'><option selected value = 1 >East</option><option value = -1 >West</option></select></div><div class='col-sm-3'><input type='text' class ='form-control input-sm londecimal decimalcoordinate' ></div></td><td><button class= 'delete-row' >Remove</button></td></tr>" 
        
         $("#polygontable").append(markup);
     });  
   
  
     $("#polytable").on('click','.delete-row',function(){
    	 var lonDegValues = $("input[id='lonDeg']")
         .map(function(){return $(this).val();}).get();
         if(lonDegValues.length > 4){
          $(this).parent().parent().remove();             
         }
     });

     
     
     $('input[type="file"]').change(function(e){
    	 $("#polygondiv").hide();
    	 $("#showpolygondiv").hide();
		  $("#addpolygonbtn").hide();
		  	
         
         var fileName = e.target.files[0].name;
         if ( ! window.FileReader ) {
 			return alert( 'FileReader API is not supported by your browser.' );
 		}
 		var $i = $( '#file' ), // Put file input ID here
 			input = $i[0]; // Getting the element from jQuery
 		if ( input.files && input.files[0] ) {
 			file = input.files[0]; // The file
 			fr = new FileReader(); // FileReader instance
 			fr.onload = function () {
 				// Do stuff on onload, use fr.result for contents of file
 				//$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
 				//alert(fr.result);
 				fileContent = fr.result;
 				mode = 'fileupload';
 				formFillFromGML(fileContent);
 				$("#areatypediv").show();
 				$("#geographicalareatype").attr('disabled', true);
 				
 			};
 			fr.readAsText( file );
 			//fr.readAsDataURL( file );
 		} else {
 			// Handle errors here
 			alert( "File not selected or browser incompatible." )
 		}         
     });

     
     $(".add-row").trigger("click");
     $(".add-row").trigger("click");
     $(".add-row").trigger("click");
     resetFooterButtons();   

     $("#geographicalareatype").change(function(){
    	 generateAreaCodeByPolygontype();
    	});  

    /* $('#table_id tbody').on('click', 'tr', function () {
    	 $(this).find('input:radio').prop('checked', true);    	
     } );  */
  
 });    

 function onloadFunction(){
	 $("#add").trigger("click");
     $("#upload").trigger("click");
  

	 }
 
  </script> 
 
 </div>

					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>


				</div>
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<div class="control-sidebar-bg"></div>
	<!-- </div> -->
	<!-- ./wrapper -->



</body>
</html>
