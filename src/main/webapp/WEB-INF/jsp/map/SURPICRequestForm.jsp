<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
	
	.trRow{
	    border-collapse: collapse;
	    border: 1px solid LightGray;
	 	background-color: white;
	}
/* style="background-color: rgb(91, 188, 236)"
style="background-color: rgb(229, 244, 251);" */
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
 	<h3 class="modal-title" >SAR SURPIC Request</h3>
 </div>    
<!-- modal-form content -->              
<div class="modal-body Gismodal-body" id= "modelbody" >

<table>
<tr class="trRow">

<td>&nbsp;&nbsp;</td>

<td>
<div class = "row" style="padding-top: 20px;"> <!-- box-body no-padding style="background-color: transparent;" -->
		<div class="form-group col-sm-6">			
            	<label>
              	<input type="radio" name="sarareaType" value="rectangle" onclick="radiobuttonclickevent();" checked="checked">
              	SAR Rectangular Area
            	</label>      		
		</div>
		<div class="form-group col-sm-6">			
            	<label>
              	<input type="radio" name="sarareaType" value="circle" onclick="radiobuttonclickevent();">
             	 SAR Circular Area 
            	</label>        
		</div>	
</div>
</td>
<td>&nbsp;&nbsp;</td>
</tr>
<!--  For Rectangle  -->
<tr>
<td>&nbsp;&nbsp;</td>
</tr>
<tr  class="trRow">
<td>&nbsp;&nbsp;</td>
<td>
<div id = "SelectedDiv" style="padding-top: 20px;">
 <div id = "rectanglediv"> <!-- class = "box-body no-padding" style="background-color: transparent;" -->
<table  class="table table-condensed">					
	<tr align="center"> 
			<th style="Width:100px">  Left Bottom:</th>
			<td> Latitude</td>
			<td> <input type="text" maxlength="2" size="2" id = 'latDegLB' > </td>
			<td> <input id = 'latMinLB' type="text" maxlength="2" size="2"> </td>
			<td><select id = 'latSignLB'> <option value = 1 >North</option><option value = -1>South</option></select> </td>
			<td>Longitude</td>
			<td><input type="text" maxlength="3" size="3" id = 'lonDegLB' > </td>
			<td><input type="text" maxlength="2" size="2" id = 'lonMinLB'> </td>
			<td><select id = 'lonSignLB'><option value = 1>East</option><option value = -1>West</option></select></td>
		</tr>
		
		<tr> 
			<th style="Width:100px">  Offset:</th>
			<td> Latitude  </td>
			<td><input type="text" maxlength="2" size="2" id = 'latDegRT' ></td>
			<td><input id = 'latMinRT' type="text" maxlength="2" size="2"></td>
			<td><select id = 'latSignRT' disabled=""> <option value = 1 >North</option><option value = -1>South</option></select> </td>
			<td>Longitude</td>
			<td><input type="text" maxlength="3" size="3" id = 'lonDegRT' ></td>
			<td><input type="text" maxlength="2" size="2" id = 'lonMinRT'></td>
			<td><select id = 'lonSignRT' disabled=""><option value = 1>East</option><option value = -1>West</option></select></td>
		</tr>	
		</table> 
  </div> 
  </div>  
 <!-- For Circle  -->
<div id = "circlediv" > <!-- class = "box-body no-padding" style="background-color: transparent;"  -->
<table  class="table table-condensed">					
	<tr> 
			<td> Latitude</td>
			<td><input type="text" maxlength="2" size="2" id = 'latDegCircle' ></td>
			<td><input id = 'latMinCircle' type="text" maxlength="2" size="2"></td>
			<td><select id = 'latSignCircle'> <option value = 1 >North</option><option value = -1>South</option></select> </td>
			<td>Longitude</td>
			<td><input type="text" maxlength="3" size="3" id = 'lonDegCircle' ></td>
			<td><input type="text" maxlength="2" size="2" id = 'lonMinCircle'></td>
			<td><select id = 'lonSignCircle'><option value = 1>East</option><option value = -1>West</option></select></td>
		</tr>
		</table>
		
		 <table  class="table table-condensed">	
			<tr>
				<td >Radius  &nbsp;&nbsp;&nbsp;&nbsp; <input type="text" size="15" id = 'radius'></td>				
			</tr>	
		</table> 
 </div> 
 
 </td>
 <td>&nbsp;&nbsp;</td>
 </tr>  
<tr>
<td>&nbsp;&nbsp;</td>
</tr> 
 <tr class="trRow">
 <td>&nbsp;&nbsp;</td>
 <td> 
   <div class="row box-body" id = "numberOfPositionsDiv" style="background-color: transparent; padding-top: 20px; ">
		<div class="form-group col-sm-4">
			<label for="NumberofPositions">
				Number of Positions</label>
			<div>
				<select class="form-control" id="numberOfPositions">
					<option> </option>
					<option> 1</option>
					<option> 2</option>
					<option> 3</option>
					<option> 4</option>					
				</select>  
			</div>
		</div>
		<div class="form-group col-sm-4" id = "shipTypeSurpicRequest">
			<label for="shipType">Ship Type</label>
			<div>
					<select multiple="" class="form-control" id="shipType">					
					<option value="0100">Passenger</option>
					<option value="0200">Cargo</option>
					<option value="0300">Tankers</option>
					<option value="0400">Mobile Off Shore & Drilling Unit</option>					
				</select>  
			</div>
		</div>
		<div class="form-group col-sm-4" id = "countrySurpicRequest">
			<label for="country">Country</label>
			 <div >
					<select class="form-control" id="countrySurpicRequestList">					
				</select> 
			</div> 
		</div>
	</div>
	</td>
	<td>&nbsp;&nbsp;</td>
	</tr>
	</table>    
  </div>     
     
<div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="button" id ="apply" onclick="applysarsurpicRequest();" class="btn btn-primary" data-dismiss="modal">Apply</button>
 </div>
              

 <script>
 $("#rectanglediv").show();
 $("#circlediv").hide();
 $("#numberOfPositionsDiv").show();
 $("#shipTypeSurpicRequest").hide();
 $("#countrySurpicRequest").hide();
 
 document.getElementById("apply").disabled = true;
 
 checkValueRectangle();
 $('#countrySurpicRequestList').append('<option></option> ');
	for(var j=0; j<lritIdDisplay.length;j++)
	{		
		$('#countrySurpicRequestList').append('<option value='+lritIdDisplay[j] +'>' + lritNameDisplay[j] +	'</option> ');			
	}		
 
	
	function checkValueRectangle()
	{
		var inputLat = document.getElementById("latDegLB");
		var inputLatMin = document.getElementById("latMinLB");
		var inputLon = document.getElementById("lonDegLB");
		var inputLonMin = document.getElementById("lonMinLB");
		
		var inputLatOff = document.getElementById("latDegRT");
		var inputLatMinOff = document.getElementById("latMinRT");
		var inputLonOff = document.getElementById("lonDegRT");
		var inputLonMinOff = document.getElementById("lonMinRT");
	
		inputLat.addEventListener("keyup", function(event) {
			if (inputLat.value.length>1 && inputLon.value.length>1 && inputLonMin.value.length>1 && inputLatMin.value.length>1 &&
					inputLatOff.value.length>1 && inputLonOff.value.length>1 && inputLonMinOff.value.length>1 && inputLatMinOff.value.length>1	)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLon.addEventListener("keyup", function(event) {
			if (inputLat.value.length>1 && inputLon.value.length>1 && inputLonMin.value.length>1 && inputLatMin.value.length>1 &&
					inputLatOff.value.length>1 && inputLonOff.value.length>1 && inputLonMinOff.value.length>1 && inputLatMinOff.value.length>1	)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLatMin.addEventListener("keyup", function(event) {
			if (inputLat.value.length>1 && inputLon.value.length>1 && inputLonMin.value.length>1 && inputLatMin.value.length>1 &&
					inputLatOff.value.length>1 && inputLonOff.value.length>1 && inputLonMinOff.value.length>1 && inputLatMinOff.value.length>1	)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLonMin.addEventListener("keyup", function(event) {
			if (inputLat.value.length>1 && inputLon.value.length>1 && inputLonMin.value.length>1 && inputLatMin.value.length>1 &&
					inputLatOff.value.length>1 && inputLonOff.value.length>1 && inputLonMinOff.value.length>1 && inputLatMinOff.value.length>1	)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		
		inputLatOff.addEventListener("keyup", function(event) {
			if (inputLat.value.length>1 && inputLon.value.length>1 && inputLonMin.value.length>1 && inputLatMin.value.length>1 &&
					inputLatOff.value.length>1 && inputLonOff.value.length>1 && inputLonMinOff.value.length>1 && inputLatMinOff.value.length>1	)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLonOff.addEventListener("keyup", function(event) {
			if (inputLat.value.length>1 && inputLon.value.length>1 && inputLonMin.value.length>1 && inputLatMin.value.length>1 &&
					inputLatOff.value.length>1 && inputLonOff.value.length>1 && inputLonMinOff.value.length>1 && inputLatMinOff.value.length>1	)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLatMinOff.addEventListener("keyup", function(event) {
			if (inputLat.value.length>1 && inputLon.value.length>1 && inputLonMin.value.length>1 && inputLatMin.value.length>1 &&
					inputLatOff.value.length>1 && inputLonOff.value.length>1 && inputLonMinOff.value.length>1 && inputLatMinOff.value.length>1	)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLonMinOff.addEventListener("keyup", function(event) {
			if (inputLat.value.length>1 && inputLon.value.length>1 && inputLonMin.value.length>1 && inputLatMin.value.length>1 &&
					inputLatOff.value.length>1 && inputLonOff.value.length>1 && inputLonMinOff.value.length>1 && inputLatMinOff.value.length>1	)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		
	}
	
	function checkValueCircle()
	{
		var inputLatCircle = document.getElementById("latDegCircle");
		var inputLonCircle = document.getElementById("lonDegCircle");
		var inputLatMinCircle = document.getElementById("latMinCircle");
		var inputLonMinCircle = document.getElementById("lonMinCircle");
		var inputRadius = document.getElementById("radius");
	 	
		inputLatCircle.addEventListener("keyup", function(event) {
			if(inputLatCircle.value.length>1 && inputLonCircle.value.length>1 && inputLonMinCircle.value.length>1 && inputLatMinCircle.value.length>1 && inputRadius.value.length>1)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLonCircle.addEventListener("keyup", function(event) {
			if(inputLatCircle.value.length>1 && inputLonCircle.value.length>1 && inputLonMinCircle.value.length>1 && inputLatMinCircle.value.length>1 && inputRadius.value.length>1)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLatMinCircle.addEventListener("keyup", function(event) {
			if(inputLatCircle.value.length>1 && inputLonCircle.value.length>1 && inputLonMinCircle.value.length>1 && inputLatMinCircle.value.length>1 && inputRadius.value.length>1)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputLonMinCircle.addEventListener("keyup", function(event) {
			  if(inputLatCircle.value.length>1 && inputLonCircle.value.length>1 && inputLonMinCircle.value.length>1 && inputLatMinCircle.value.length>1 && inputRadius.value.length>1)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
		inputRadius.addEventListener("keyup", function(event) {
			  if(inputLatCircle.value.length>1 && inputLonCircle.value.length>1 && inputLonMinCircle.value.length>1 && inputLatMinCircle.value.length>1 && inputRadius.value.length>1)
			  { 
				  document.getElementById("apply").disabled = false;			 
			  }
			  else
			  {
				 document.getElementById("apply").disabled = true;
			  }
		 });
	}
	
	
	
 function radiobuttonclickevent(){
	 
	 $("#drawonmap").hide();
	    $("#apply").show();
	    var radioValue = $("input[name='sarareaType']:checked").val();
	    if (radioValue === "rectangle"){
	    	console.info('inside rectangle');
	    	var element = document.createElement("div");
	    	//element.id = 'rectanglediv';
	    	element.innerHtml = "<table  class=\"table table-condensed\">" +					
			" <tr align=\"center\">" + 
			"<th style=\"Width:100px\">  Left Bottom:</th>" + 
			"<td> Latitude</td> " +
			"<td> <input type=\"text\" maxlength=\"2\" size=\"2\" id = \"latDegLB\" > </td>" +
			"<td> <input id = \"latMinLB\" type=\"text\" maxlength=\"2\" size=\"2\"> </td>" +
			"<td><select id = \"latSignLB\"> <option value = 1 >North</option><option value = -1>South</option></select> </td>" +
			"<td>Longitude</td>" +
			"<td><input type=\"text\" maxlength=\"3\" size=\"3\" id = \"lonDegLB\"> </td>" +
			"<td><input type=\"text\" maxlength=\"2\" size=\"2\" id = \"lonMinLB\"> </td>" +
			"<td><select id = \"lonSignLB\"><option value = 1>East</option><option value = -1>West</option></select></td> </tr> <tr>" +		 
			"<th style=\"Width:100px\">  Right Top:</th>" +
			"<td> Latitude  </td>" +
			"<td> <input type=\"text\" maxlength=\"2\" size=\"2\" id = \"latDegRT\" > </td>" +
			"<td> <input id = \"latMinRT\" type=\"text\" maxlength=\"2\" size=\"2\"> </td>" +
			"<td><select id = \"latSignRT\" disabled=\"\"> <option value = 1 >North</option><option value = -1>South</option></select> </td>" +
			"<td>Longitude</td>" +
			"<td><input type=\"text\" maxlength=\"3\" size=\"3\" id = \"lonDegRT\" > </td>" +
			"<td><input type=\"text\" maxlength=\"2\" size=\"2\" id = \"lonMinRT\"> </td>" +
			"<td><select id = \"lonSignRT\" disabled=\"\"><option value = 1>East</option><option value = -1>West</option></select></td></tr></table>";		
			//$('#SelectedDiv').append(element);
			//console.info($('#SelectedDiv'));
	    	$("#rectanglediv").show();
	    	$("#circlediv").hide();
	    	$("#numberOfPositionsDiv").show();
	    	$("#shipTypeSurpicRequest").show();
	    	$("#countrySurpicRequest").show();
	    	//checkValueRectangle();
	    }else if(radioValue === "circle"){
	    	$("#rectanglediv").hide();
	    	$("#circlediv").show();
	    	
	    	var element = document.createElement("div");
	    	//element.id = 'circlediv';
	    	element.innerHtml = "<table  class=\"table table-condensed\"><tr> " +					
			"<td> Latitude</td>" +
			"<td> <input type=\"text\" maxlength=\"2\" size=\"2\" id = \"latDegCircle\" > </td>" +
			"<td> <input id = \"latMinCircle\" type=\"text\" maxlength=\"2\" size=\"2\"> </td>" +
			"<td><select id = \"latSignCircle\"> <option value = 1 >North</option><option value = -1>South</option></select> </td>" +
			"<td>Longitude</td>" +
			"<td><input type=\"text\" maxlength=\"3\" size=\"3\" id = \"lonDegCircle\" > </td>" +
			"<td><input type=\"text\" maxlength=\"2\" size=\"2\" id = \"lonMinCircle\"> </td>" +
			"<td><select id = \"lonSignCircle\"><option value = 1>East</option><option value = -1>West</option></select></td></tr></table>"+
		 	"<table  class=\"table table-condensed\"><tr><td >" +	
			"Radius  &nbsp;&nbsp;&nbsp;&nbsp; <input type=\"text\" size=\"15\" id = \"radius\"></td></tr></table>";
			//$('#SelectedDiv').append(element);
	    	$("#numberOfPositionsDiv").show();
	    	$("#shipTypeSurpicRequest").show();
	    	$("#countrySurpicRequest").show();
	    	checkValueCircle();
	    }
 }


 
  </script> 
 
