	<%@ page language="java" contentType="text/html; charset=utf-8"
		pageEncoding="utf-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
	<!--  jQuery 3  -->
	<spring:url
		value="/bower_components/jquery/dist/jquery.min.js"
		var="jqueryminjs" />
	<script src="${jqueryminjs}"></script>
	<!-- <script type="text/javascript"
		src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
	
	<!-- Daterange picker -->
	<spring:url
		value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
		var="daterangepickercss" />
	<link rel="stylesheet" href="${daterangepickercss}">
	
	<!-- daterangepicker -->
	<spring:url value="/bower_components/moment/min/moment.min.js"
		var="momentminjs" />
	<script src="${momentminjs}"></script>
	<spring:url
		value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
		var="daterangepickerjs" />
	<script src="${daterangepickerjs}"></script>
	<!-- datepicker -->
	<spring:url
		value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
		var="bootstrapdaterangepickerminjs" />
	<script src="${bootstrapdaterangepickerminjs}"></script>
	
	<!-- Data Table  -->
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
 	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

	<script type="text/javascript"
		src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script> 
					
		
		<%-- <spring:url
		value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
		var="datatblebootstrapmincss" />
	<link rel="stylesheet" href="${datatblebootstrapmincss}"> --%>
		
     <spring:url value="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" var="dataTablecss" />
	<link rel="stylesheet" href="${dataTablecss}">  
 	<script>
		 var jq = $.noConflict(true);
 	</script> 
 
	
	<!--  Map Dependency -->
	
	<spring:url value="/resources/ol/ol.js" var="oljs" />
	<script src="${oljs}"></script>
	
	<!-- for Advanced GeoSpecial functions turf min js used-->
	<spring:url value="/resources/ol/turf.min.js" var="turfminjs" />
	<script src="${turfminjs}"></script>
	
	<spring:url value="/resources/css/ol.css" var="olcss" />
	<link rel="stylesheet" href="${olcss}">
	
	<spring:url value="/resources/css/mapStyling.css" var="mapStylingcss" />
	<link rel="stylesheet" href="${mapStylingcss}">
	
	<script>
	
	//modelAttribute
	var flagCountryOfLogin = '${flagCountryOfLogin}'; // for standing order display
	var geoserver_url = '${geoserver_url}'; // geoserver_url
	var countryListIds = '${lritIds}'; // for waterpolygon chceckboxes
	var countryListName = "${lritNames}"; // for waterpolygon chceckboxes
	var companyUser = '${companyCode}';
	var countryListIdsShipsForDisplay = '${assignLritIdsShipView}'; // for waterpolygon chceckboxes
	//var companyDetails = '${companyNameList}';
	
	
	/* var startDate;
	var endDate;
	var countryFlag = true;
	// for History date and time
	$('#starttime').daterangepicker({
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		singleDatePicker : true
	}

	);
	
	$('#endtime').daterangepicker({
		timePicker : true,
		timePicker24Hour : true,
		locale : {
			format : 'MM/DD/YYYY HH:mm:ss'
		},
		singleDatePicker : true
	});


	$('#starttime').on('apply.daterangepicker', function(ev, picker) {
		
		  startDate = picker.startDate.format('YYYY-MM-DD H:mm:ss');				  
		 
		 
		});
	$('#endtime').on('apply.daterangepicker', function(ev, picker) {				
		 
		  endDate = picker.endDate.format('YYYY-MM-DD H:mm:ss');
		  
		  if(archivedSurpicButtonFlag === true)
		  {  displayArchievedSURPICAreaView();
		  	 
		  }
		 
		}); */
	
	//box box-solid bg-light-blue-gradient
	</script>
	
	
	
	
	<div class="modal fade" id="modal-default">
		<div class="modal-dialog"
			style="position: absolute; left: 255px; top: 180px; width: 150px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h5 class="modal-title ">Zoom by Percentage</h5>
				</div>
	
				<div class="modal-body">
					<div>
						<select id="percentage">
							<option value=" 25% ">25%</option>
							<option value=" 50% ">50%</option>
							<option value=" 75% ">75%</option>
							<option value=" 100% ">100%</option>
						</select>
					</div>
				</div>
	
				<div class="modal-footer">
					<button type="button" class="btn btn-sm"
						onclick="zoomByPercentage()" data-dismiss="modal">Apply</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	
	<!-- form to display layer list  -->
	<form id="LayerForm" method="post" action="SelectedWaterPolygons">
		<input type="hidden" id="selectedWaterPolygonsName"
			name="selectedWaterPolygonsName">
		<input type="hidden" id="selectedPortsName"
			name="selectedPortsName">
		
		<div class="modal fade" id="waterPolygonList">
			<div class="modal-dialog" style="Width: 450px">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title ">Layer List</h5>
					</div>
	
					<%--  <jsp:include page="WaterPolygon.jsp"></jsp:include>   ; WIDTH:500px; HEIGHT:500px   --%>
					<div class="modal-body">
						<div>						
							<table id="waterPolygonsNameTable" class="table table-hover gisDatatable" style="width:100%">
								<thead>
									<tr>
										<th style="padding: 10px"></th>
										<th style="padding: 10px">IW </th>
										<th style="padding: 10px">TS </th>
										<th style="padding: 10px">CC </th>
										<!-- <th> OT </th> -->
										<th style="padding: 10px">CS</th>
										<th style="padding: 10px"> SO </th>
										<th style="padding: 10px"> Port </th>										
									</tr>
									<tr>
									<th><input type="hidden"
										value="Select All " /> Select All</th>
									<th><input type="checkbox" id="checkbox1" value="" /></th>
									<th><input type="checkbox" id="checkbox2" value="" /></th>
									<th><input type="checkbox" id="checkbox3" value="" /></th>
									<!-- <th> <input type="checkbox" id="checkbox4" value=""/> </th> -->
									<th><input type="checkbox" id="checkbox5" value="" /></th>
									<th> <input type="checkbox" id="checkbox4" value=""/>
									<th> <input type="checkbox" id="checkbox6" value=""/>
								</tr>
								</thead>
								<tbody id="waterPolygonsNameTableBody">
								</tbody>
							</table>	
						</div>
					</div>
	
					<div class="modal-footer">
						 <button type="button" class="btn btn-primary pull-left"
						onclick="clearWaterPolygonFrom()"	>Clear</button> 
						<button type="button" class="btn btn-primary"
							onclick="displayWaterPolygon()" data-dismiss="modal">Apply</button>
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	
	</form>
	<!-- End of form to display layer list  -->
	
	
	
	<!-- form to display Country list  -->
	<form id="countryListForm" method="post" action="CountryBoundary" >
		<div class="modal fade" id="countryList">
			<div class="modal-dialog" style="Width: 300px">
				<div class="modal-content">
					<div id="modelHeaderCountry" class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title ">Country Ships</h5>
					</div>
	
					<div class="modal-body">
					
						<div >
						
							<table id="searchShipByCountryTable" class="table table-hover gisDatatable" style="width:100%">
								<thead>
									<tr>
										<th id ="searchShipByCountryTableHeader" >Select All</th>
										<th><input type="checkbox" id="checkboxCountryList"
											value="" /></th>
										
									</tr>
								</thead>
								<tbody id="searchShipByCountryTableBody">
								</tbody>
							</table>
						
						</div>
					</div>
	
					<div id="modelFooterCountry" class="modal-footer">
						<button type="button" class="btn btn-primary pull-left"
						onclick="clearCountrySelction()"	>Clear</button> 
						<button type="button" class="btn btn-primary"
							onclick="displayCountriesShips()" data-dismiss="modal">Apply</button>
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</form>
	<!-- End of form to display country list  -->
	
	<!-- form to display Country boundary list  -->
	<form id="CountryBoundaryListForm" method="post">
		<div class="modal fade" id="countryBoundaryList">
			<div class="modal-dialog" style="Width: 300px">
				<div class="modal-content">
					<div id="modelHeaderCountry" class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title ">Ship in Country Boundary</h5>
					</div>
	
					<div class="modal-body">
						
						<div>						
						<table id="searchShipByCountryBoundaryTable" class="table table-hover gisDatatable" style="width:100%">
								<thead>
									<tr>
										<th>Select All</th>
										<th><input type="checkbox" id="checkboxCountryBoundaryList"
											value="" /></th>
										
									</tr>
								</thead>
								<tbody id="searchShipByCountryBoundaryTableBody">
								</tbody>
							</table>
						
								
						</div>
					</div>
	
					<div id="modelFooterCountry" class="modal-footer">
						<button type="button" class="btn btn-primary pull-left"
						onclick="clearCountryBoundarySelction()"	>Clear</button> 
						<button type="button" class="btn btn-primary"
							onclick="displayCountriesShipsInBoundary()" data-dismiss="modal">Apply</button>
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</form>
	<!-- End of form to display country list  -->
	
	
	<!-- form to display Shipping Company   list  -->
	<form id="ShippingCompanyForm" method="post"
		action="SearchShipByCompany">
		<input type="hidden" id="companyName" name="companyName">
		<div class="modal fade" id="shipCompanyList">
			<div class="modal-dialog" style="Width: 300px">
				<div class="modal-content">
					<div id="modelHeaderCountry" class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
	
						<h5 id="titleCountry" class="modal-title ">Shipping Company</h5>
					</div>
	
					<div class="modal-body">
						
						<div >						
						<table id="searchShipByCompanyTable" class="table table-hover gisDatatable" style="width:100%">
								<thead>
									<tr>
										<th >Select All</th>
										<th><input type="checkbox" id="checkboxCompanyName"
											value="" /></th>										
									</tr>
								</thead>
								<tbody id="searchShipByCompanyTableBody">
								</tbody>
							</table>						
						</div>
					</div>
	
					<div id="modelFooterCountry" class="modal-footer">
						 <button type="button" class="btn btn-default pull-left"
							onclick="clearCompanyShipsSelction()">Clear</button> 
						<button type="button" class="btn btn-primary"
							onclick="displayCompanyShips()" data-dismiss="modal">Apply</button>
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</form>
	<!-- End of form to display Shipping Company  list  -->
	
	
	<!-- ---------------------------------------------------Display Geographical Layers-------------------------------------------------------------------------------------------- -->
	
	<form id="geographicalLayersForm" method="post"
		action="displayGeographicalLayers">
		<input type="hidden" id="username" name="username">
		<div class="modal fade" id="displayGeographicalLayerssModalId">
			<div class="modal-dialog" style="Width: 300px">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title ">Geographical Layers</h5>
					</div>
	
	
					<div class="modal-body">
	
						<div class="form-group">
							<div>
								<label> <input type="checkbox"
									name="displayGeographicalLayersOption" id="displayLakes"
									value="Lakes"> Lakes
								</label>
							</div>
							<div>
								<label> <input type="checkbox"
									name="displayGeographicalLayersOption" id="displaySeas"
									value="Seas"> Seas
								</label>
							</div>
							<div>
								<label> <input type="checkbox"
									name="displayGeographicalLayersOption" id="displayBays"
									value="Bays"> Bays
								</label>
							</div>
							<div>
								<label> <input type="checkbox"
									name="displayGeographicalLayersOption" id="displayLightHouse"
									value="LightHouse"> Light House
								</label>
							</div>
							<div>
								<label> <input type="checkbox"
									name="displayGeographicalLayersOption" id="displayShipWreks"
									value="ShipWreks"> Ship Wrecks
								</label>
							</div>
							<div>
								<label> <input type="checkbox"
									name="displayGeographicalLayersOption" id="displayBethyLines"
									value="BethyLines"> Bathy Lines
								</label>
							</div>
							<div>
								<label> <input type="checkbox"
									name="displayGeographicalLayersOption" id="displayOceans"
									value="Oceans"> Oceans
								</label>
							</div>
							<div>
								<label> <input type="checkbox"
									name="displayGeographicalLayersOption" id="displayGeographicalPorts"
									value="GeographicalPorts"> Ports
								</label>
							</div>
	
						</div>
					</div>
	
					<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default pull-left"
							data-dismiss="modal">Close</button> -->
						<button type="button" class="btn btn-primary"
							onclick="displayGeographicalLayers()" data-dismiss="modal">Apply</button>
	
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</form>
	<!-- ---------------------------------------------------Display Geographical Layers Ends-------------------------------------------------------------------------------------------- -->
	
	<!-- form to display grid list  -->
	<form id="gridListForm" method="post" action="SelectedWaterPolygons">
	<div class="modal fade" id="gridList">
		<div class="modal-dialog" style="Width: 300px">
			<div class="modal-content">
				<div id="modelHeaderCountry" class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
	
					<h5 id="titleGridList" class="modal-title ">Grid List</h5>
				</div>
	
				<div class="modal-body">
					<div class="form-group">
						<div class="radio">
							<label> <input type="radio" name="gridOptionsRadios"
								id="optionsRadios1" value="0" checked="checked"> None
							</label>
						</div>
						<div class="radio">
							<label> <input type="radio" name="gridOptionsRadios"
								id="optionsRadios2" value="1"> 1 Grid
							</label>
						</div>
						<div class="radio">
							<label> <input type="radio" name="gridOptionsRadios"
								id="optionsRadios3" value="2"> 2 Grid
							</label>
						</div>
						<div class="radio">
							<label> <input type="radio" name="gridOptionsRadios"
								id="optionsRadios4" value="4"> 4 Grid
							</label>
						</div>
						<div class="radio">
							<label> <input type="radio" name="gridOptionsRadios"
								id="optionsRadios5" value="8"> 8 Grid
							</label>
						</div>
						<div class="radio">
							<label> <input type="radio" name="gridOptionsRadios"
								id="optionsRadios6" value="16"> 16 Grid
							</label>
						</div>
						<div class="radio">
							<label> <input type="radio" name="gridOptionsRadios"
								id="optionsRadios7" value="32"> 32 Grid
							</label>
						</div>
					</div>
	
				</div>
	
				<div id="modelFooterCountry" class="modal-footer">
					<!-- <button type="button" class="btn btn-default pull-left"
						data-dismiss="modal">Close</button> -->
					<button type="button" class="btn btn-primary"
						onclick="displayGrid()" data-dismiss="modal">Apply</button>
				</div>
	
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	</form>
	<!-- End of form to displaygrid list  -->
	
	<!-- ---------------------------------------------------Path Projection Form-------------------------------------------------------------------------------------------- -->
	
	<form id="pathProjectionForm" method="post" action="pathProjectionForm">
		<input type="hidden" id="username" name="username">
		<div class="modal fade" id="pathProjectionModal">
			<div class="modal-dialog" style="Width: 300px">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title ">Path Projection</h5>
					</div>
	
					<%--  <jsp:include page="WaterPolygon.jsp"></jsp:include>   	; WIDTH:500px; HEIGHT:500px   --%>
					<div class="modal-body">
						<label>IMO No.</label> <input type="text" id="PPimoNoVal"
							name="imoNoValue"><br>
						<br>
						<br> <label>Time (in hour)</label>
						 <input type="text" id="pathLengthComboID"
							name="pathLengthComboValue">
						 <!-- <select id="pathLengthComboID">
							<option value=5>5</option>
							<option value=10>10</option>
							<option value=15>15</option>
							<option value=20>20</option>
							<option value=25>25</option>
						</select> -->
	
					</div>
					<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default pull-left"
							data-dismiss="modal">Close</button> -->
						<button type="button" class="btn btn-primary"
							onclick="pathProjectionfunc()" data-dismiss="modal">Apply</button>
	
	
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	
	</form>
	
	
	
	<!-- ---------------------------------------------------Path Projection Form-------------------------------------------------------------------------------------------- -->
	
	<!-- ---------------------------------------------------Distance & Bearing Form-------------------------------------------------------------------------------------------- -->
	<form id="DistanceBearingForm" method="post" action="DistanceBearingForm">
		
		<div class="modal fade" id="distanceBearingModal">
			<div class="modal-dialog" style="Width: 300px">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title ">Distance & Direction Tool </h5>
					</div>
	
					<%--  <jsp:include page="WaterPolygon.jsp"></jsp:include>   	; WIDTH:500px; HEIGHT:500px   --%>
					<div class="modal-body">
						<div class="form-group">
						<div class="radio">
							<label> <input type="radio" name="distanceBearingOptionsRadios"
								id="optionsRadios1" value="distance" checked="checked"> Distance & Bearing Tool
							</label>
						</div>
						<div class="radio">
							<label> <input type="radio" name="distanceBearingOptionsRadios"
								id="optionsRadios2" value="bearing"> Ring Range Tool
							</label>
						</div>
						</div>
					</div>
					<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default pull-left"
							data-dismiss="modal">Close</button> -->
						<button type="button" class="btn btn-primary"
							onclick="distanceBearingfunc()" data-dismiss="modal">Apply</button>
	
	
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	
	</form>
	
	<!-- ---------------------------------------------------Distance & Bearing Form-------------------------------------------------------------------------------------------- -->
	
	<!-- ---------------------------------------------------SAR Surpic Form-------------------------------------------------------------------------------------------- -->
	<form id="sarSurpicForm" method="post">
		
		<div class="modal fade" id="sarSurpicModal">
			<div class="modal-dialog" style="Width: 300px">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title "> Surpic Request </h5>
					</div>
	
					<%--  <jsp:include page="WaterPolygon.jsp"></jsp:include>   	; WIDTH:500px; HEIGHT:500px   --%>
					<div class="modal-body">
						<div class="form-group">
						<div class="radio">
							<label> <input type="radio" name="sarSurpicFormRadios"
								id="optionsRadios1" value="Rectangle" checked="checked"> Rectangle
							</label>
						</div>
						<div class="radio">
							<label> <input type="radio" name="sarSurpicFormRadios"
								id="optionsRadios2" value="Circle"> Circle
							</label>
						</div>
						</div>
					</div>
					<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default pull-left"
							data-dismiss="modal">Close</button> -->
						<button type="button" class="btn btn-primary"
							onclick="sarSurpicfunc()" data-dismiss="modal">Apply</button>
	
	
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	
	</form>
	
	<!-- ---------------------------------------------------End of SAR Surpic Form-------------------------------------------------------------------------------------------- -->
	
	
	<!-- form to display Show History Selection  -->
	<form id="shipHistoryMainForm" method="post">
	<div class="modal fade" id="shipHistoryForm">
		<div class="modal-dialog" style="Width: 400px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h5 class="modal-title ">Ship History</h5>
				</div>
	
				<div class="modal-body">
				<div class="form-group">
					<!-- <div>
						IMO No.: <input type="text" id="imoNumber" name="imoNumber">
					</div>
					<br></br>	 -->
					
					<div class="row">
						<div class="form-group col-sm-12">

							<label for="imono" class="col-sm-4 control-label">IMO
								NO</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" 
										class="form-control col-sm-8" id="imoNumber" name="imoNumber"										
										title="Please Enter Digit."  />
									
								</div>
							</div>
						</div>
					</div>				
					
					<div class="row">
						<div id="starttime" style="display: none"
							class="form-group col-sm-12">
							<label for="starttimedate" class=" col-sm-4 control-label">Start
								Time</label>
							<div class="col-sm-8">
								<div class="input-group date">
									<div class="input-group-addon ">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" id="start"
										class="form-control pull-right" />
								</div>
							</div>
						</div>
						
					</div>
					
					<div class="row">
					<div id="endtime" style="display: none"
							class="form-group col-sm-12">
							<label for="endtimedate" class="col-sm-4 control-label">End
								Time</label>
							<div class="col-sm-8">
								<div class="input-group date">
									<div class="input-group-addon ">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" id="end"
										class="form-control pull-right" />
								</div>
							</div>
						</div>
						</div>
					</div>
					
					
					<!-- <div id="historyTimeForm">
						Date and time range
						<div class="form-group" id="historyDateRange">
							<label>Date and time range:</label>
	
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</div>
								<input type="text" class="form-control pull-right"
									name="datetimes" id="historytime">
							</div>
							/.input group
						</div>
						/.form group
					</div> -->
	
				</div>
	
				<div class="modal-footer">
					<!-- <button type="button" class="btn btn-default pull-left"
						data-dismiss="modal">Close</button> -->
					<button type="button" class="btn btn-primary"
						onclick="showShipHistory()" data-dismiss="modal">Apply</button>
				</div>
	
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	</form>
	<!-- End of form to display Show History Selection  -->
	
	<!-- To display Polygon related Forms  -->
	<form id="customPolygonForm" method="post">
	<div class="modal fade" id="pageModal">
		<div class="modal-dialog">
			<div class="modal-content" id="pageModaldialogContent"></div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	</form>
	<!-- End of display Polygon related Forms  -->
	
	<!-- ---------------------------------------------------Search Ship and Port Form Start---------------------------------------------------------------------------------- -->
	<form class="form-Style" id="searchShipPortForm" method="post"
		action="searchShipPort">
		<input type="hidden" id="username" name="username">
		<div class="modal fade " id="searchShipPortModal">
			<!-- <div class="modal-dialog" style="Width:600px"> -->
			<div class="modal-dialog" style="Width: 600px">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title ">Search Ship/Port</h5>
					</div>
					<%--  <jsp:include page="WaterPolygon.jsp"></jsp:include>   ; WIDTH:500px; HEIGHT:500px   --%>
					<div class="modal-body">
						 <div>
							
		 					<div class = "row"> 
									<div class="form-group col-sm-6">			
							            	<label>
							              	<input type="radio" name="searchType" id="searchType" value="ship" onChange="populateCombo()" checked="checked">
							              	SHIP
							            	</label>      		
									</div>
									<div class="form-group col-sm-6">			
							            	<label>
							              	<input type="radio" name="searchType" id="searchType" value="port" onChange="populateCombo()">
							             	 PORT
							            	</label>        
									</div>	
							</div>
							<div class = "row"> 
									<div class="form-group col-sm-6">			
							            	<label id="lblSearchCriteria">Search Criteria
							            	</label>  
							            	  	<select Style="width: 150px" name="searchCriteriaCombo" id="searchCriteriaCombo">
													<option value="Ship Name">Ship Name</option>
													<option value="IMO No.">IMO No.</option>
													<option value="MMSI No.">MMSI No.</option>
													<option value="SEID No.">SEID No.</option>
													<option value="DNID No.">DNID No.</option>
												</select>
							              		    		
									</div>
									<div class="form-group col-sm-6">			
							            	<label id="lblSearchValue"> Value
							            	</label> 
							            	
							              	<input type="text" Style="width: 150px" name="txtSearchValue" id="idSearchValue">
							             	      
									</div>	
							</div>
							<div  style="padding: 5px;" >
								<!-- <table id="searchShipTable"	class="display compact" style="width:100%">
								</table>	
								 -->
								<table id="searchShipTable" class="display compact" style="width:100%">
								<!-- <thead>
									<tr>										
										<th><input type="checkbox" id="checkBoxSelectAll" name="checkBoxSelectAllName" /></th>		
										<th> IMO No </th> <th> MMSI No </th> <th>SEID No </th> <th> DNID No </th><th> Member No </th>
										<th> Ship Name </th><th> Country </th><th> Latitude </th><th> Longitude </th><th> Status </th>								
									</tr>
								</thead>
								<tbody id="searchShipTableBody">
								</tbody> -->
							</table>		
															  
							</div>
							<div  style="padding: 5px;" >
								<!-- <table id="searchPortTable"	class="display compact" style="width:100%">
								</table>	 -->	
								<table id="searchPortTable" class="display compact" style="width:100%">
								<!-- <thead>
									<tr>	
										<th><input type="checkbox" id="checkBoxSelectAll" name="checkBoxSelectAllName" /></th>		
										<th> CG Name </th> <th> CG LRITID </th> <th>regularversionno </th> <th> lcode </th><th> Name </th>
										<th> Latitude </th><th> Longitude </th>								
									</tr>
								</thead>
								<tbody id="searchPortTableBody">
								</tbody> -->
								</table>						  
							</div>
						</div>
					</div>
	
					<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default pull-left"
							data-dismiss="modal">Close</button> -->
						<button type="button" id="searchButton" class="btn btn-primary"
							onclick="searchShipDetails()" disabled>Search</button>
						<button type="button" id="displaySerachButton"
							class="btn btn-primary" onclick="displaySearchShipPortLayer()"
							data-dismiss="modal" disabled>Display on Map</button>
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	
	</form>
	
	<!-- ---------------------------------------------------Search Ship and Port Form End---------------------------------------------------------------------------------- -->
	
	<!-- ---------------------------------------------------Show Sar Area View/Archived sar --------------------------------------------------------------------------------- -->
	<form class="form-Style" id="sarAreaViewModalForm" method="post">
	<div class="modal fade " id="sarAreaViewModal">
		<!-- <div class="modal-dialog" style="Width:600px"> -->
		<div class="modal-dialog" style="Width: 300px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h5 class="modal-title ">Show Sar Area View</h5>
				</div>
	
				<div class="modal-body">
				<div class="form-group">
					<div class="row">
	
						<div class="col-lg-12">
	
							<div>
								<input type="checkbox" id="sarRegionArea"> <label
									for="sarRegionArea">Sar Region Area</label>
							</div>
	
							<div>
								<input type="checkbox" id="sarRegionAreaVessel"> <label
									for="sarRegionAreaVessel">Vessel in Sar Region Area</label>
							</div>
						</div>
						
								<div class="row">
						<div id="starttimeSar" style="display: none"
							class="form-group col-sm-12">
							<label for="starttimedateSar" class=" col-sm-4 control-label">Start
								Time</label>
							<div class="col-sm-8">
								<div class="input-group date">
									<div class="input-group-addon ">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" id="startSar"
										class="form-control pull-right" />
								</div>
							</div>
						</div>
						
					</div>
					
					<div class="row">
					<div id="endtimeSar" style="display: none"
							class="form-group col-sm-12">
							<label for="endtimedateSar" class="col-sm-4 control-label">End
								Time</label>
							<div class="col-sm-8">
								<div class="input-group date">
									<div class="input-group-addon ">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" id="endSar"
										class="form-control pull-right" />
								</div>
							</div>
						</div>
						</div>
					
						
						
						<div class="col-lg-12" id="sarAreaViewRight"></div>
						<div  style="padding: 5px;" >
							<table id="sarAreaViewRightTable"	class="table table-hover gisDatatable" style="width:100%">
							<thead><tr><th><input type="checkbox" id="checkBoxSelectAllMessageIds"  name="checkBoxSelectAllMessageName"></th>
							<th>Message Id</th>
							</tr></thead><tbody id="sarAreaViewRightTableBody"></tbody>	  
							</table>							
						</div>
	
					</div>
				</div>
				</div>
	
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id = "displaySarAreaViewApplyButton"
						onclick="displaySelectedSurpicAreaByMessageID()" data-dismiss="modal">Apply</button>
				</div>
	
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	</form>
	<!-- ---------------------------------------------------Show Sar Area View ---------------------------------------------------------------------------------- -->
	
	
	
	
	<!-- ---------------------------------------------------Filter Vessel on Status Form-------------------------------------------------------------------------------------------- -->
	
	<form class="form-Style" id="filterVesselStatusModalForm" method="post">
		<div class="modal fade" id="filterVesselStatusModalId">
			<div class="modal-dialog" style="Width: 250px">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title ">Filter Vessel on Status</h5>
					</div>
	
					<div class="modal-body" style="padding-bottom:0px;">
						<div class="form-group" style= "border: 1px solid #5ea3b3;">
						<div style="padding:10px;">
							<div class="radio">
								<label> <input type="radio"
									name="FVStatusOptionVesselType" id="FVStatusOptionVesselType"
									value="allVessels" checked="checked" onclick="enableFVStatusOptionStatus();"> All Vessels
								</label>
							</div>
							<div class="radio">
								<label> <input type="radio"
									name="FVStatusOptionVesselType" id="FVStatusOptionVesselType"
									value="flagVessels"  onclick="enableFVStatusOptionStatus();"> Flag Vessels
								</label>
							</div>
							<div class="radio">
								<label> <input type="radio"
									name="FVStatusOptionVesselType" id="FVStatusOptionVesselType"
									value="foreignVessels" onclick="enableFVStatusOptionStatus();"> Foreign Vessels
								</label>
							</div>
	
						</div>
						</div>
						
						<div class="form-group" style= "border: 1px solid #5ea3b3;">
						<div style="padding:10px;"  >
							<div class="radio">
								<label> <input type="radio" name="FVStatusOptionStatus"
									id="FVStatusOptionStatus" value="all" checked="checked">
									All
								</label>
							</div>
							<div class="radio">
								<label> <input type="radio" name="FVStatusOptionStatus"
									id="FVStatusOptionStatus" value="respondingNormally">
									Responding Normally
								</label>
							</div>
							<div class="radio">
								<label> <input type="radio" name="FVStatusOptionStatus"
									id="FVStatusOptionStatus" value="missingPosition">
									Missing Position
								</label>
							</div>
							<div class="radio">
								<label> <input type="radio" name="FVStatusOptionStatus"
									id="FVStatusOptionStatus" value="notResponding"> Not
									Responding
								</label>
							</div>
	
						</div>
						</div>
					</div>
					<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default pull-left"
							data-dismiss="modal">Close</button> -->
						<button type="button" class="btn btn-primary"
							onclick="filterVesselStatusFunc()" data-dismiss="modal">Apply</button>
					</div>
	
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</form>
	
	
	
	<!-- ---------------------------------------------------Filter Vessel on Status Form Ends-------------------------------------------------------------------------------------------- -->
	
	
	
	<!-- ---------------------------------------------------Show Satellite OCean Region --------------------------------------------------------------------------------- -->
	<form class="form-Style" id="showSatelliteOceanRegionModalForm" method="post">
	<div class="modal fade " id="showSatelliteOceanRegionModal">
		<!-- <div class="modal-dialog" style="Width:600px"> -->
		<div class="modal-dialog" style="Width: 300px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h5 class="modal-title ">Satellite Ocean Region</h5>
				</div>
	
				<div class="modal-body">
	
	
					<div>
						<input type="checkbox" id="indianOceanRegionId"> <label
							for="indianOceanRegionId">Indian Ocean Region</label>
					</div>
					<div>
						<input type="checkbox" id="pacificOceanRegionId"> <label
							for="pacificOceanRegionId">Pacific Ocean Region</label>
					</div>
					<div>
						<input type="checkbox" id="atlanticOceanRegionEastId"> <label
							for="atlanticOceanRegionEastId">Atlantic Ocean Region East</label>
					</div>
					<div>
						<input type="checkbox" id="atlanticOceanRegionWestId"> <label
							for="atlanticOceanRegionWestId">Atlantic Ocean Region
							West</label>
					</div>
	
				</div>
	
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary"
						onclick="displaySateliteOceanRegion()" data-dismiss="modal">Apply</button>
				</div>
	
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	</form>
	<!-- ---------------------------------------------------End of Show Satellite OCean Region---------------------------------------------------------------------------------- -->
	
	
	
	
	<!-- Map box  style="height: 100%; width: 100%;" class="scrollableMap"   style="background-color: transparent; " -->
	
	
	<div id="map" style="background-color: #e5f4fb;">
	
	
	<sec:authorize access="hasAuthority('addGeographicalAreaDDP')" var="addGeographicalAreaDDP"></sec:authorize>
	
	<sec:authorize access="hasAuthority('viewArchievedSurpic')" var="viewArchievedSurpic"></sec:authorize>
	
	<%-- <sec:authorize access="hasAnyAuthority('portRequestForeignVessel','archivedDataRequestMsgForeignVessel','sarPollRequest','pollRequestMsgFlagandForeignVessel','flagRequestMsgFlagVessel','coastalRequestMsgForeignVessel',
	'closeSar','stopReportingMsgFlagandFoeignVess' )" var="rightClick"></sec:authorize> --%>
	
	<sec:authorize access="hasAnyAuthority('flagrequest','coastalrequest')" var="rightClick"></sec:authorize>
	
	<sec:authorize access="hasAuthority('portrequest')" var="rightClickPortRequest"></sec:authorize>
	
	<sec:authorize access="hasAnyAuthority('sarpollrequest')" var="rightClickSARPoll"></sec:authorize>
	
	<sec:authorize access="hasAuthority('sarsurpicrequest')" var="sarSurpic"></sec:authorize>
	<sec:authorize access="hasAuthority('coastalsurpicrequest')" var="coastalSurpic"></sec:authorize>
	
	<sec:authorize access="hasAuthority('shipCountryFlag')" var="shipCountryFlag"></sec:authorize>	

	<sec:authorize access="hasAuthority('mapUser')" var="mapUser"></sec:authorize>
	
	<sec:authorize access="hasAuthority('advancedMapFunctionalitiesAnalysis')" var="advancedMapFunctionalitiesAnalysis"></sec:authorize>
	<sec:authorize access="hasAuthority('viewAdditionalLayer')" var="viewAdditionalLayer"></sec:authorize>
	<sec:authorize access="hasAuthority('filterVesselforViewing')" var="filterVesselforViewing"></sec:authorize>
	
	    <script type="text/javascript">
	     
	      var addGeographicalAreaDDPAllowed = '${addGeographicalAreaDDP}'; // '${haRoleUser}';
	      var viewArchievedSurpicAllowed = '${sarAreaViewStatus}';
	      var rightClickAllowed = '${rightClick}';
	      var sarSurpicAllowed = '${sarSurpic}';
	      var coastalSurpicAllowed = '${coastalSurpic}';
	      var mapUserAllowed = '${mapUser}';
	      var advancedMapFunctionalitiesAnalysisAllowed = '${advancedMapFunctionalitiesAnalysis}';
	      var viewAdditionalLayerAllowed = '${viewAdditionalLayer}';
	      var filterVesselforViewingAllowed = '${filterVesselforViewing}';
	      var shipCountryFlagAllowed = '${shipCountryFlag}';
	      var rightClickSARPollAllowed = '${rightClickSARPoll}';
	      var rightClickPortAllowed = '${rightClickPortRequest}';
	    </script>   
	 	<sec:authorize access="hasAuthority('mapUser')">
		    <div class="btn-group" id="HorzButtonMap">
				<button type="button" id="zoompercent" title="Percentage Zoom"
					class="btn btn-sm gisHorzButton" data-toggle="modal"
					data-target="#modal-default">
					<i class="fa fa-search" style="color: white;"><sup>%</sup></i>
				</button>
				<button type="button" id="zoomin" title="Zoom In"
					class="btn btn-sm gisHorzButton"
					onclick="map.getView().setZoom((map.getView().getZoom())+1)"
					style="color: white;">
					<i class="fa fa-search-plus"></i>
				</button>
				<button type="button" id="zoomout" title="Zoom Out"
					class="btn btn-sm gisHorzButton"
					onclick="map.getView().setZoom((map.getView().getZoom()) - 1)"
					style="color: white;">
					<i class="fa fa-search-minus"></i>
				</button>
				<button type="button" title="Lock Pan" id="enablePanButton"
					class="btn btn-sm gisHorzButton " onclick="enablePan()">
					<i class="fa fa-hand-rock-o" style="color: white;"></i>
				</button>
				<button type="button" title="Zoom To Extent" id="extentButton"
					class="btn btn-sm gisHorzButton" onclick="zoomToExtent()">
					<i class="fa fa-search" style="color: white;"></i>
				</button>
				<button type="button" title="Refresh" id="refresh"
					class="btn btn-sm gisHorzButton" onclick="removeAllLayersRefresh()">
					<i class="fa fa-refresh" style="color: white;"></i>
				</button>
				<button type="button" title="Rectangle Zoom" id="rectZoom"
					class="btn btn-sm gisHorzButton">
					<i class="fa fa-square-o" style="color: white;"></i>
				</button>
				<button type="button" title="Key Map" id="keymap"
					class="btn btn-sm gisHorzButton" onclick="keyMap()">
					<i class="fa fa-key" style="color: white;"></i>
				</button>
				<button type="button" title="Grid" id="horzGrid"
					class="btn btn-sm gisHorzButton" onclick="horzGridOnMap()">
					<i class="fa fa-th" style="color: white;"></i>
				</button>
				<div class="pull-right" Style="padding-left:180px;" id="ringDistanceInfo">					
				</div>
			</div>					
			</sec:authorize> 
			
			<div class="pull-right" Style="padding:5px; text-color:gray; background-color: #c6cfce4d; border:border:3px solid #9b9a9a1a;" id="latlonSearch">
				 Lat &nbsp; <input type="text" placeholder= "00" pattern="[0-9][0-9]{1,}"  maxlength="2" class = "textLatLonRight" name="txtLatDegSearch" id="idLatDegSearch">&deg;
				 <input type="text" placeholder= "00" pattern="[0-9][0-9]{1,}"   maxlength="2" class = "textLatLonRight" name="txtLatMinSearch" id="idLatMinSearch">&rsquo;
				 <input type="text"  placeholder= "00"  pattern="[0-9][0-9]{1,}"  maxlength="2" class = "textLatLonRight" name="txtLatSecSearch" id="idLatSecSearch">&rdquo; &nbsp; 
				 <select id = 'latSign'> <option value = 1 >N</option><option value = -1>S</option></select> &nbsp; &nbsp;
				 Lon &nbsp; <input type="text" placeholder= "000" pattern="[0-9][0-9]{1,}"  maxlength="3" class = "textLatLonRight" Style=" width: 25px;" name="txtLonDegSearch" id="idLonDegSearch">&deg;
				 <input type="text" placeholder= "00" pattern="[0-9][0-9]{1,}"  maxlength="2" class = "textLatLonRight" name="txtLonMinSearch" id="idLonMinSearch">&rsquo;
				 <input type="text" placeholder= "00" pattern="[0-9][0-9]{1,}"  maxlength="2" class = "textLatLonRight" name="txtLonSecSearch" id="idLonSecSearch">&rdquo;  &nbsp; 
				 <select id = 'lonSign'> <option value = 1 >E</option><option value = -1>W</option></select>
				 <button type="button" title="Search" id="latlonSearchButton" Style="background-color: #c6cfce4d; padding:0px; padding-left:3px;padding-right:3px;"
						class="btn btn-default pull-right" onclick="searchLatLonPoint()">
						<i class="fa fa-map-marker" ></i>
			</button> </div>				
			<div class="pull-right" Style="padding:5px;" id="lonLatInfo">0.0 0.0</div>
	 	
	
	
		<div id="scale-line" class="scale-line"></div>
		
		<!-- <div id="distanceTableId" class="ol-customtest"></div> -->
		
		<div class="ol-attribution ol-unselectable ol-control ol-uncollapsible" id="creditOpenStreetMap" Style="z-index:1000">
			<ul>
				<li>© <a>OpenStreetMap</a> contributors.
				</li>
			</ul>			
		</div>
		
	</div>
	
	<spring:url value="/resources/js/gridValue.js" var="gridValuejs" />
	<script src="${gridValuejs}"></script>
	
		<spring:url value="/resources/js/shipPositions.js" var="shipPositionsjs" />
	<script src="${shipPositionsjs}"></script>
	
	<spring:url value="/resources/js/Display_Map.js" var="DisplayMapjs" />
	<script src="${DisplayMapjs}"></script>
	
	<spring:url value="/resources/js/sarsurpicRequest.js" var="sarsurpicRequestjs" />
	<script src="${sarsurpicRequestjs}"></script>
		
		
	<%-- <spring:url value="/resources/js/html-to-image.js" var="htmlToImagejs" />
	<script src="${htmlToImagejs}"></script> --%>	
		
	<spring:url value="/resources/js/mapControls.js" var="mapControlsjs" />
	<script src="${mapControlsjs}"></script>
	
	
	