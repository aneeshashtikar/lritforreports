<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
	
	.trRow{
	    border-collapse: collapse;
	    border: 1px solid LightGray;
	 	background-color: white;
	}
/* style="background-color: rgb(91, 188, 236)"
style="background-color: rgb(229, 244, 251);" */
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
 	<h3 class="modal-title" >Create User Defined Polygon</h3>
 </div>    
<!-- modal-form content -->              
<div class="modal-body Gismodal-body" id= "modelbody1" >

<table style="width:100%;">
<tr class="trRow">

<td>&nbsp;&nbsp;</td>

<td>
<div class = "row" style="padding-top: 5px;"> <!-- box-body no-padding style="background-color: transparent;" -->
		<div class="form-group col-sm-6">			
            	<label>
              	<input type="radio" id ="createPolygonRadio" name="userareaOption" value="create" onclick="proceedNext()" checked="checked">
              	 Create Polygon
            	</label>      		
		</div>
		<div class="form-group col-sm-6">			
            	<label>
              	<input type="radio" id ="showPolygonRadio" name="userareaOption" value="show" onclick="proceedNext();">
             	 Show Polygon
            	</label>        
		</div>	
</div>
</td>
<td>&nbsp;&nbsp;</td>
</tr>
</table> 
</div>
<div class="modal-body Gismodal-body" id= "modelbody2" style="padding-top: 2px;" >
<table style="width:100%;">
<tr class="trRow">

<td>&nbsp;&nbsp;</td>

<td>
<div id ="createPolygonOptions" class = "row" style="padding-top: 5px;"> <!-- box-body no-padding style="background-color: transparent;" -->
		<div class="form-group col-sm-6">			
            	<label>
              	<input type="radio" name="drawOption"   id= "insert" value="insertbytextfields" onclick="radiobuttonclickevent()" checked="checked">
              	 Insert by Text fields
            	</label>      		
		</div>
		<div class="form-group col-sm-6">			
            	<label>
              	<input type="radio" name="drawOption" id= "draw"  value="drawonmap" onclick="radiobuttonclickevent();">
             	 Draw on Map
            	</label>        
		</div>	
</div>
<div id ="showSavedPolygon" style="padding:10px;">
		
	<table id="showPolygonTable" class="display compact" style="width:100%;">
		<thead>
			<tr>				
				<th><input type="checkbox" id="checkboxPolygonId"
					value="" /></th>		
				<!-- <th >Polygon Id </th> -->
				<th >Polygon Name</th>	
				<th >Polygon Type</th>	
				<th >Created By</th>		
				<th >Creation Date</th>	
				<th >Creation Time</th>					
			</tr>
		</thead>
		<tbody id="showPolygonTableBody">
		</tbody>
	</table>	
		
</div>
</td>
<td>&nbsp;&nbsp;</td>
</tr>
</table> 
</div>

<div class="modal-body Gismodal-body" id= "modelbody3" style="padding-top: 2px;"  >
<table style="width:100%;">
<tr class="trRow">

<td>&nbsp;&nbsp;</td>

<td>
<div id = "insertByTextOptions">
<div class = "row" style="padding-top: 5px;"> <!-- box-body no-padding style="background-color: transparent;" -->
		<div class="form-group col-sm-4">			
            	<label>
              	<input type="radio" name="userareaType"  value="rectangle" onclick="radiobuttonclickevent()" checked="checked">
              	 Rectangular
            	</label>      		
		</div>
		<div class="form-group col-sm-4">			
            	<label>
              	<input type="radio" name="userareaType" value="polygon" onclick="radiobuttonclickevent();">
             	 Polygon
            	</label>        
		</div>	
		<div class="form-group col-sm-4">			
            	<label>
              	<input type="radio" name="userareaType" value="circle" onclick="radiobuttonclickevent();">
             	 Circle
            	</label>        
		</div>	
</div>
 <div style="padding-bottom: 5px;">
 <div class = "row"> 
 <div class="form-group col-sm-4">
	Area Name: &nbsp;&nbsp; <input type="text" id ="areaname"  maxlength="50" size="10">
</div>
 <div id="insertPolygonPointsOption" class="form-group col-sm-4" style="padding-top: 5px;">

 	<input type="radio" name="polygonOption" id ="polygonInsertId" value="polygonInsert" onclick="radiobuttonclickevent();" checked="checked">
	 insert Polygon Point

</div>
<div id="uploadFileOption" class="form-group col-sm-4" style="padding-top: 10px;">

 	<input type="radio" name="polygonOption" value="polygonFile" onclick="radiobuttonclickevent();">
	 Upload File

</div>
</div>
</div>
</div>
</td>
<td>&nbsp;&nbsp;</td>
</tr>

<!--  For Rectangle  -->
<tr>
<td>&nbsp;&nbsp;</td>
</tr>
<tr  class="trRow">
<td>&nbsp;&nbsp;</td>
<td>

 <div id = "rectanglediv"  style="height: 140px;"> <!-- class = "box-body no-padding" style="background-color: transparent;" -->
<table  class="table table-condensed coordinate-edit">					
	<tr align="center"> 
			<th style="Width:100px">  Left Bottom:</th>
			<td> Latitude</td>
			<td> <input type="text" maxlength="2" size="2" id = 'latDegLB'  class = 'integercoordinate latitudedegree'> </td>
			<td> <input id = 'latMinLB' type="text" maxlength="2" size="2" class = 'integercoordinate latitudeminute'> </td>
			<td><select id = 'latSignLB'> <option value = 1 >North</option><option value = -1>South</option></select> </td>
			<td>Longitude</td>
			<td><input type="text" maxlength="3" size="3" id = 'lonDegLB'  class = 'integercoordinate longitudedegree' > </td>
			<td><input type="text" maxlength="2" size="2" id = 'lonMinLB' class ='integercoordinate longitudeminute'> </td>
			<td><select id = 'lonSignLB'><option value = 1>East</option><option value = -1>West</option></select></td>
		</tr>
		
		<tr> 
			<th style="Width:100px">  Offset:</th>
			<td> Latitude  </td>
			<td><input type="text" maxlength="2" size="2" id = 'latDegRT' class = 'integercoordinate latitudedegree'></td>
			<td><input id = 'latMinRT' type="text" maxlength="2" size="2" class = 'integercoordinate latitudeminute'></td>
			<td><select id = 'latSignRT' disabled=""> <option value = 1 >North</option><option value = -1>South</option></select> </td>
			<td>Longitude</td>
			<td><input type="text" maxlength="3" size="3" id = 'lonDegRT' class = 'integercoordinate longitudedegree'></td>
			<td><input type="text" maxlength="2" size="2" id = 'lonMinRT' class ='integercoordinate longitudeminute'></td>
			<td><select id = 'lonSignRT' disabled=""><option value = 1>East</option><option value = -1>West</option></select></td>
		</tr>	
		</table> 
  </div>   
 <!-- For Circle  -->
<div id = "circlediv" style="height: 140px;"> <!-- class = "box-body no-padding" style="background-color: transparent;"  -->
<table  class="table table-condensed coordinate-edit">					
	<tr> 
			<th style="Width:100px"> Center:</th>
			<td> Latitude</td>
			<td><input type="text" maxlength="2" size="2" id = 'latDegCircle' class = 'integercoordinate latitudedegree' ></td>
			<td><input id = 'latMinCircle' type="text" maxlength="2" size="2" class = 'integercoordinate latitudeminute'></td>
			<td><select id = 'latSignCircle'> <option value = 1 >North</option><option value = -1>South</option></select> </td>
			<td>Longitude</td>
			<td><input type="text" maxlength="3" size="3" id = 'lonDegCircle'  class = 'integercoordinate longitudedegree'></td>
			<td><input type="text" maxlength="2" size="2" id = 'lonMinCircle' class ='integercoordinate longitudeminute'></td>
			<td><select id = 'lonSignCircle'><option value = 1>East</option><option value = -1>West</option></select></td>
		</tr>
		</table>
		
		 <table  class="table table-condensed">	
			<tr>
				<td >Radius  &nbsp;&nbsp;&nbsp;&nbsp; <input type="text" size="15" id = 'radius'></td>				
			</tr>	
		</table> 
 </div> 
 
 <div id = "polygondiv" > <!-- class = "box-body no-padding" style="background-color: transparent;"  -->
<!-- <div class = "row" style="padding-top: 10px;"> box-body no-padding style="background-color: transparent;"
		<div class="form-group col-sm-6">			
            	<label>
            	Polygon Points
              	<button type="button" class="add-row">Add row</button>              	
            	</label>      		
		</div>
		<div class="form-group col-sm-6">			
            	<button type="button" id = "fileuploadbtn" onclick = showUploadFile()>File Upload</button>       
		</div>	
</div> -->

<div id = "internalpolygondiv" class = "row" style="padding-left: 5px;"> <!-- box-body no-padding style="background-color: transparent;" -->

 <!--  <table id ="polygontableHead" style="width:100%; padding-left: 25px;" >  
  <tr><td style="padding-left: 15px;">Latitude</td><td>Longitude</td></tr> 
  </table> -->
  <div id = "polytableDiv" style="height: 110px; overflow-y: auto; padding-left: 2px; padding-right: 2px;">
  <table style="width:100%" id='polytable' class="table table-condensed coordinate-edit">
   
   <tbody id ="polygontable">
    
  </tbody> 
  </table>
  </div>
<div id="polygonAddRowButton" style="padding-top: 5px;">     	
<button type="button" class="add-row">Add row</button>              	
</div>        
</div>
<div id = "fileuploadtablediv" class = "row" style="padding-left: 20px;"> <!-- box-body no-padding style="background-color: transparent;" -->
		
<table  id = "fileuploadtable" class="table table-condensed">	
	<tr>
		<td >Select File <input type="file" ></td>	
			
	</tr>	
</table> 
</div>
 </div> 
 
 </td>
 <td>&nbsp;&nbsp;</td>
 </tr> 

</table>    
</div>     
     
<div class="modal-footer">
     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
     <button type="button" id ="apply" onclick="applyUserDefinedArea();" class="btn btn-primary" data-dismiss="modal" >Apply</button>
     <button class="btn btn-primary" id="drawonmap" onclick="freehandDrawOnMap();" data-dismiss="modal">Draw on Map</button>
 </div>
              

 <script>
 var userareas;
 var countRowInsertPolygon = 0; 
 $("#polygondiv").hide();
 $("#circlediv").hide();
 $("#rectanglediv").show(); 
 $("#showSavedPolygon").hide();
 $("#fileuploadtablediv").hide();
 $("#insertPolygonPointsOption").hide();
 $("#uploadFileOption").hide();
 $("#polygonAddRowButton").hide();
 $("#drawonmap").hide();
 
 function showUploadFile()
 {
	 $("#polygondiv").show();
	 $('#fileuploadtablediv').show();	 	
 	 $("#fileuploadtable").show();
	 $('#internalpolygondiv').hide();
}
  
 function proceedNext(){
		var option = $("input[name='userareaOption']:checked").val();
		//console.info(option);
		if(option === 'show')		
		{
			//console.info("inside show");
			//openModalForm("custompolygonform?userareaOption="+option);		
			createPolygonFlag = false;
			 $.ajax({
				 	type: "POST",
				    traditional: true,
				    url: "searchCustomPolygon",
				    data: {},
	    		    success: function(result){               
	    		    userareas = result;	    		    
	   			 	//console.info(result);
	   			// $('#showPolygonTableId').html("");
	   			 //if ($('#showPolygonTableId').children().length == 0){
   				 if (jq.fn.DataTable.isDataTable( '#showPolygonTable' ) ) {			 
   				 		jq('#showPolygonTable').DataTable().destroy();
   				 		$('#showPolygonTableBody').html("");   
   				 	 
   				 }	
   				 	for(var i=0; i<result.length;i++)
	   				{	   	
   				 		var customPolygonDate = null;
   				 	    var customPolygonTime = null;
   				 		if(result[i].creation_date != null)
 				 		{
   				 			customPolygonDate = (result[i].creation_date).split('T')[0];
   				 		    customPolygonTime = ((result[i].creation_date).split('T')[1]).split('+')[0];
   				 		}
	   					$('#showPolygonTableBody').append('<tr> <td>  <input type="checkbox" name ="selectedArea" value = '+ result[i].polygonId +' ></td>'+	
	   							/* '<td> '+ result[i].polygonId +'</td>' +  d.split(' ')[0];  */
	   							'<td> '+ result[i].polygon_name +'</td>' +
	   							'<td> '+ result[i].polygon_type +'</td>' +
	   							'<td> '+ result[i].created_by +'</td>' +
	   							'<td> '+ customPolygonDate +'</td>' +
	   							'<td> '+ customPolygonTime +'</td>' +
	   							'</tr>');
	   				}	
   				 	
   				 	 	jq('#showPolygonTable').DataTable( {   	
   				 	 		"scrollX": true,
	 	   				 	"scrollY":        "150px",
	 	   			        "scrollCollapse": true,
	 	   			        "paging":         false,
	 	   			        "ordering": false,
	 	   			        "info":     false,					    
	 	   			       
	 	   			    } );	
   				
	   			// }
	   			    $("#createPolygonOptions").hide();
				 	$("#insertByTextOptions").hide();				 
				 	$("#showSavedPolygon").show();
				 	$("#polygondiv").hide();
				 	$("#circlediv").hide();
				 	$("#fileuploadtablediv").hide();
				 	$("#rectanglediv").hide(); 
				 	$("#modelbody3").hide();
				 	$("#drawonmap").hide();
				 	$("#apply").show();
	           }});		
			
			    $("#createPolygonOptions").hide();
			 	$("#insertByTextOptions").hide();				 
			 	$("#showSavedPolygon").show();
			 	$("#polygondiv").hide();
			 	$("#circlediv").hide();
			 	$("#fileuploadtablediv").hide();
			 	$("#rectanglediv").hide(); 
			 	$("#modelbody3").hide();
			 	$("#drawonmap").hide();
			 	$("#apply").show();
		}
		else
		{		 	
			createPolygonFlag = true;
		 	$("#showSavedPolygon").hide();
		 	$("#modelbody3").show();
		 	$("#modelbody2").show();		 	
		 	$("#createPolygonOptions").show();
		 	$("#insertByTextOptions").show();
		 	$("#rectanglediv").show(); 
		 	$("#fileuploadtablediv").hide();		 	
		 	$("#insert").prop("checked", true);
		 	$("#apply").show();
		 	$("#drawonmap").hide();
		}
}
 
 
 function radiobuttonclickevent(){
	 var radioValueInput = $("input[name='drawOption']:checked").val();
	 if (radioValueInput === "drawonmap"){
			$("#apply").hide();
		    $("#drawonmap").show();
		    $("#modelbody3").hide();
	 }
	 else if (radioValueInput === "insertbytextfields"){
		 $("#drawonmap").hide();
		 $("#modelbody3").show();
		 $("#insertByTextOptions").show();
		 $("#rectanglediv").show(); 
	 	 $("#apply").show();	    
	   
	 }
	    var radioValue = $("input[name='userareaType']:checked").val();
	    if (radioValue === "rectangle"){
	    	$("#rectanglediv").show();
	    	$("#polygondiv").hide();
	    	$("#circlediv").hide();
	    	$("#insertPolygonPointsOption").hide();
	    	$("#uploadFileOption").hide();
	    	$('#internalpolygondiv').hide();
	    	$('#polytableDiv').hide();
	    	$('#polytable').hide();
	    	$("#polygonAddRowButton").hide();
	    	$('#fileuploadtablediv').hide();
	    	$("#fileuploadtable").hide();
	    }else if(radioValue === "polygon"){
	    	$("#rectanglediv").hide();	
	    	$("#insertPolygonPointsOption").show();
	    	$("#uploadFileOption").show();
	    	//$("#fileuploadtablediv").hide();
	    	$("#circlediv").hide();
	    	$('#polytableDiv').show();
	    	$("#polygondiv").show();
	    	$('#internalpolygondiv').show();
	    	$('#polytable').show();
	    	$("#polygonAddRowButton").show();
	    //	console.info("polygon selected");	    	
	        
	    }else if(radioValue === "circle"){
	    	$("#rectanglediv").hide();
	    	$("#polygondiv").hide();
	    	$("#circlediv").show();	    
	    	$("#insertPolygonPointsOption").hide();
	    	$("#uploadFileOption").hide();	    	
	    	$('#internalpolygondiv').hide();
	    	$('#internalpolygondiv').hide();
	    	$('#polytableDiv').hide();
	    	$('#polytable').hide();
	    	$("#polygonAddRowButton").hide();
	    	$('#fileuploadtablediv').hide();
	    	$("#fileuploadtable").hide();
	    	
	    	
	    }else{
	    	$("#rectanglediv").hide();
	    	$("#polygondiv").hide();
	    	$("#circlediv").hide();
	    	$("#insertPolygonPointsOption").hide();
	    	$("#uploadFileOption").hide();
	    	$('#internalpolygondiv').hide();
	    	$('#polytableDiv').hide();
	    	$('#polytable').hide();
	    	$("#polygonAddRowButton").hide();
	    	$('#fileuploadtablediv').hide();
	    	$("#fileuploadtable").hide();
	    }	
	 
	    var radioValuePolygon = $("input[name='polygonOption']:checked").val();
	    if (radioValuePolygon === "polygonInsert"){
	    	$("#polygondiv").show();
	    	$('#internalpolygondiv').show();	
	    	//console.info("polygonInsert selected");	    
	    	
	    	while (countRowInsertPolygon < 3) {
	    		$(".add-row").trigger("click");
	    		  countRowInsertPolygon++;
	    		}
	    	
	    	/* 
	    	$(".add-row").trigger("click");
	        $(".add-row").trigger("click");
	        $(".add-row").trigger("click"); */
	    	$("#fileuploadtablediv").hide();
	    }else if(radioValuePolygon === "polygonFile"){	    	
	    	showUploadFile();
	    	
	    }
 }
 
 $("#latDeg").keyup(function(){
     query = $("#latDeg").val();
     //alert(query);
});

 
 $(document).ready(function(){	 
	 
	 
     $(".add-row").click(function(){
    	// console.info("add row");
    	 var markup = "<tr align=\"center\"> "+			
			"<td> Lat</td>" + 
			"<td> <input type=\"text\" maxlength=\"2\" size=\"2\" id = \"latDeg\" class = 'integercoordinate latitudedegree'> </td>" + 
			"<td> <input id = \"latMin\" type=\"text\" maxlength=\"2\" size=\"2\" class = 'integercoordinate latitudeminute'> </td>" +
			"<td><select id = \"latDirection\"> <option value = 1 >North</option><option value = -1>South</option></select> </td>" +
			"<td>Long</td>" +
			"<td><input type=\"text\" maxlength=\"3\" size=\"3\" id = \"lonDeg\" class ='integercoordinate longitudedegree' > </td>" +
			"<td><input type=\"text\" maxlength=\"2\" size=\"2\" id = \"lonMin\"  class = 'integercoordinate longitudeminute' > </td>" +
			"<td><select id = \"lonDirection\"><option value = 1>East</option><option value = -1>West</option></select></td><td><button class= 'delete-row' >Remove</button></td></tr>" ;
        /*  var markup = "<tr><td><input type='text' maxlength='2' size='2' id = 'latDeg'>&nbsp;<input type='text' maxlength='2' size='2' id = 'latMin'><select id = 'latDirection'> "+
         "<option value = 1 >North</option><option value = -1>South</option></select></td><td>  <input type='text' maxlength='3' size='3' id = 'lonDeg'><input type='text' maxlength='2' size='2' id = 'lonMin'><select id = 'lonDirection'>"+
         "<option value = 1>East</option><option value = 1>West</option></select></td><td><button class= 'delete-row' >Remove</button></td></tr>" */
         $("#polygontable").append(markup);
     });  
   
     
     $("#polygontable").on('click','.delete-row',function(){
    	 
    //	 $(".delete-row").click(function(){
    	 var lonDegValues = $("input[id='lonDeg']")
         .map(function(){return $(this).val();}).get();
         if(lonDegValues.length > 3){
          $(this).parent().parent().remove();             
         }
     });
     
   
    	 $("#checkboxPolygonId").click(function(){
    		    $('input:checkbox').not(this).prop('checked', this.checked);
    		});
    	 

    
  
 });    
    
//polygon validation

 $(".coordinate-edit").on('keydown','.integercoordinate',function(e){
     var keyPressed;
     if (!e) var e = window.event;
     if (e.keyCode) keyPressed = e.keyCode;
     else if (e.which) keyPressed = e.which;
     var hasDecimalPoint = (($(this).val().split('.').length-1)>-1); //restrict decimal point
     if ( keyPressed == 46 || keyPressed == 8 ||((keyPressed == 190||keyPressed == 110)&&(!hasDecimalPoint && !e.shiftKey )) || keyPressed == 9 || keyPressed == 27 || keyPressed == 13 ||
              // Allow: Ctrl+A
             (keyPressed == 65 && e.ctrlKey === true) ||
              // Allow: home, end, left, right
             (keyPressed >= 35 && keyPressed <= 39)) {
                  // let it happen, don't do anything
                  return;
         }
         else {
             // Ensure that it is a number and stop the keypress
             if (e.shiftKey || ((keyPressed < 48) || (keyPressed > 57)) && ((keyPressed < 96) || (keyPressed > 105) )) {
                 e.preventDefault();
             } 
             
         }

   });		

 	$(".coordinate-edit").on('keyup','.longitudedegree',function(e){
 		   if($(this).val()>180) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
 		   lonDegVal = $(this).val();
 		   if(lonDegVal == 180) $(this).closest('tr').find('.longitudeminute').val(0);
 	 });

   	$(".coordinate-edit").on('keyup','.latitudedegree',function(e){
 	     if($(this).val()>90) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
 	     var latDegVal = $(this).val();
 	      if(latDegVal == 90) $(this).closest('tr').find('.latitudeminute').val(0);
 	  }); 
   
 	$(".coordinate-edit").on('keyup','.latitudeminute',function(e){
      if($(this).val()>59) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
      var latDegVal = $(this).closest('tr').find('.latitudedegree').val();
      if	(latDegVal == 90) $(this).val(0);		     
  	 }); 
   
 	$(".coordinate-edit").on('keyup','.longitudeminute',function(e){
 	     if($(this).val()>59) $(this).val($(this).val().substring(0, ($(this).val().length-1)));
 	     var lonDegVal = $(this).closest('tr').find('.longitudedegree').val();
 	     if	(lonDegVal == 180) $(this).val(0);
 	  }); 


 	// for custom polygon
 	
 	
	
	//-------------------------------------------------------		 
	 var polygonIdModified;
	  function openSaveFormWithData(url,data){	
		 
		  $.ajax({url: url ,type: 'get',		  		    
			success: function(result){
				 try{
				   $("#pageModaldialogContent").html(result);               
				   $("#pageModal").modal('show');		              
				   $( "#insert" ).trigger( "click" );
				   if(data){
					   $("#areaname").val(data.polygonName);
					   if(data.polygontype === "polygon"){		//Prefill Fredraw/Polygon Fields
						   $('input:radio[name=userareaType]:nth(1)').attr('checked',true);		            		   
						   for(var i = 0; i < data.lon.length; i++){	            		
							[latDeg ,latMin] = LatLonToDegMin(data.lat[i]);  
							[lonDeg ,lonMin] = LatLonToDegMin(data.lon[i]);      
							var latoptionString = (data.lat[i] > 0) ? "<option selected value = 1 >North</option><option value = -1 >South</option>" : "<option value = 1 >North</option><option value = -1 selected>South</option>";			            	   
							var lonoptionString = (data.lon[i] > 0) ? "<option selected value = 1 >East</option><option value = -1 >West</option>" : "<option value = 1 >East</option><option value = -1 selected >West</option>"  ;          	   	
						
							 var markup = "<tr align=\"center\"> "+			
								"<td> Lat</td>" + 
								"<td> <input type=\"text\" maxlength=\"2\" size=\"2\" id = \"latDeg\" value = "+ latDeg +" > </td>" + 
								"<td> <input id = \"latMin\" type=\"text\" maxlength=\"2\" size=\"2\" value = "+ latMin +"> </td>" +
								"<td><select id = \"latDirection\">" + latoptionString + "</select> </td>" +
								"<td>Long</td>" +
								"<td><input type=\"text\" maxlength=\"3\" size=\"3\" id = \"lonDeg\" value = "+ lonDeg +"> </td>" +
								"<td><input type=\"text\" maxlength=\"2\" size=\"2\" id = \"lonMin\" value = "+ lonMin +"> </td>" +
								"<td><select id = \"lonDirection\">"+ lonoptionString +"</select></td><td><button class= 'delete-row' >Remove</button></td></tr>" ;
							   $("#polygontable").append(markup);	
							   $("input[name='userareaType']:checked").val("polygon");
							   $("input[name='polygonOption']:checked").val("polygonInsert");
							   radiobuttonclickevent();
						   }
						   $("#polygondiv").show();
						   $('#internalpolygondiv').show();	
					       $("#fileuploadtablediv").hide();
					   }
					   else if(data.polygontype === "rectangle"){   //Prefill Rectangle Fields	            		 
						   $('input:radio[name=userareaType]:nth(0)').attr('checked',true);
						   $("#latSignLB").val((data.lat < 0) ? -1 : 1 );
						   $("#lonSignLB").val((data.lon < 0) ? -1 : 1 );			            		
						   $("#latSignRT").val((data.latOffset < 0) ? -1 : 1 );
						   $("#lonSignRT").val((data.longOffest < 0) ? -1 : 1 );				            		
						   [latDegLB ,latMinLB] = LatLonToDegMin(data.lat);  
						   [lonDegLB ,lonMinLB] = LatLonToDegMin(data.lon);
						   $("#latDegLB").val(latDegLB);
						   $("#latMinLB").val(latMinLB);
						   $("#lonDegLB").val(lonDegLB);
						   $("#lonMinLB").val(lonMinLB);				            		
						   [latDegRT ,latMinRT] = LatLonToDegMin(data.latOffset);  
						   [lonDegRT ,lonMinRT] = LatLonToDegMin(data.longOffset);            	
						   $("#latDegRT").val(latDegRT);
						   $("#latMinRT").val(latMinRT);		            		
						   $("#lonDegRT").val(lonDegRT);
						   $("#lonMinRT").val(lonMinRT);				            		
						   $("#rectanglediv").show();			            		 
					   }
					   else if(data.polygontype === "circle"){    //Prefill Circle Fields
						   $('input:radio[name=userareaType]:nth(2)').attr('checked',true);		            		   
						   $("#latSignCircle").val((data.lat < 0) ? -1 : 1 );
						   $("#lonSignCircle").val((data.lon < 0) ? -1 : 1 );			            		
						   [latDegCircle ,latMinCircle] = LatLonToDegMin(data.lat);  
						   [lonDegCircle ,lonMinCircle] = LatLonToDegMin(data.lon); 
						   $("#latDegCircle").val(latDegCircle);
						   $("#latMinCircle").val(latMinCircle);		            		
						   $("#lonDegCircle").val(lonDegCircle);
						   $("#lonMinCircle").val(lonMinCircle);		            		   
						   $("#radius").val(data.radius);	            		   
						   $("#circlediv").show();		            		   
					   }
					   polygonIdModified = data.polygonId;						   
				   }
				   //test ---------------
				   var values = $("input[id='latDeg']")
				   .map(function(){return $(this).val();}).get();					 
				 }
			  catch(err)
				{
					console.info('error in open Save Form With Data: ' + err)
				}
			   //-------------------------
		   }});	    	
		 
	  }
	  
	  var createPolygonFlag = true;
	  function applyUserDefinedArea(){
		  try{
			 if (createPolygonFlag == true)
			 { 				 					 
				  var userArea;
				  if($("input[name='userareaType']:checked").val() === "polygon"){
					  userArea = applyPolygon();
				  } else if($("input[name='userareaType']:checked").val() === "rectangle"){
					  userArea = applyRectangle();     	  
				  } else if($("input[name='userareaType']:checked").val() === "circle"){
					  userArea = applyCircle(); 		  
				  }
				  if(userArea){
					  userArea.polygonId = polygonIdModified;	
					  /* if ($("#areaname").val().trim()="")
						  alert("Please Enter Valid Polygon Name."); */
					 
					  $.ajax({url: '/lrit/map/saveuserarea' ,type: 'post',
							contentType: 'application/json',
							data: JSON.stringify(userArea) ,success: function(result){
							//alert(result);
							 try{
								// console.log(result.polygonName);
							 
								 if((result.polygonName).trim()==""){
									// console.log("Please Enter Valid Polygon Name.");								
									 alert("Please Enter Valid Polygon Name.");
								 }
								 else if((result.polygonName).trim()=="reject"){
									 alert("Polygon Name Already Exit In DB.");
									// console.log("Polygon Name Already Exit In DB.");
								}	
								else
								{
									//console.log("Polygon Name Added In DB.");								
									//sourceCustomPolygon.clear();
									vectorSource_Temp.clear();
									sarFlag = false;
									displayPolygonOnMap(result,"userarea",sourceCustomPolygon,sarFlag);
									alert("Polygon Saved Successfully.");
								}
							 }
							 catch(err)
							{
								console.info('error in saveuserarea: ' + err)
							}
						},
						failure: function(errMsg) { 
			                console.log(errMsg);
			            }});  
					 
				  }
			 }
			  else{
			  
				  var table = jq('#showPolygonTable').DataTable();
					table.search('').draw();
			    var option = [];
		        $.each($("input[name='selectedArea']:checked"), function(){
		        	option.push($(this).val());

		        });			    	
		        vectorSource_Temp.clear();
		    	$.ajax({url: "/lrit/map/getselecteduserarea?id="+option ,type: 'get'
				     ,success: function(result){
				    	 try
				    	 {
				    	 userareas =  result;					    	 
				    	 sourceCustomPolygon.clear();
				    	 vectorSource_Temp.clear();
				    	 for(var i =0 ;i<userareas.length;i++){
				    		 var coords =[];
				    	 	 var userarea = userareas[i];					    	 	
				    	 	//console.info('userarea: ' + userarea);
				    	 	sarFlag = false;
							displayPolygonOnMap(userarea,"userarea",sourceCustomPolygon,sarFlag);
				    	 }		 
				    	 
				    	 var extent = vectorSource_Temp.getExtent();
							
							if(extent[0] != 'Infinity' && extent[0] != '-Infinity')
							 { 
					    		 var diffExtent = extent[2]- extent[0];
					    		 if( diffExtent >= 0 && diffExtent < extentThreshold)
					    		 {
					    			 var center = ol.extent.getCenter(extent);							
					    			 map.getView().setCenter(center);
					    		 }
					    		 else
					    			 map.getView().fit(extent, map.getSize());	    		
							 } 
				    	 }
						 catch(err)
						{
							console.info('error in getselecteduserarea: ' + err)
						}
		   		 }
		    	
		    	}); 
			  }
		  }
		  catch(err)
			{
				console.info('error in apply User Defined Area: ' + err)
			}
	  }
	  
	  
	  
function savePolygon(){
try{
	var feature = currentPolygonFeature;
	if (feature) {
		var featureL = feature.clone();	
		var latlon = featureL.getGeometry().getCoordinates(); // featureL.getGeometry().transform('EPSG:3857', 'EPSG:4326').getCoordinates();
		var i;
		var lon=[];
		var lat=[];
		
		featureTemp =latlon;
		for(var i = 0; i < latlon.length; i++) {
			var row = latlon[i];
			for(var j = 0; j < row.length; j++) {					
				lat.push(row[j][1]);
				lon.push(row[j][0]);
			}
		}       
		var polygontype ="polygon";
		var polygon={
				polygontype:"polygon",
				lat : lat,
				lon : lon
		}	
		if(latlon[0].length > 3){
			openSaveFormWithData("saveform",polygon);    
		}else{
			alert("Minimum 3 Points to be selected ");
			sourceCustomPolygon.removeFeature(feature);
			freehandDrawOnMap();
			
		}	
	}
}
catch(err)
{
	console.info('error in save Polygon: ' + err)
}
}

		
 	
 
  </script>
  
  
  
  
  
  