<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
 	<h4 class="modal-title" >
 			Show User Define Area	    
	</h4>
 </div>    
<div class="modal-body" id= "modelbody">
 <table >
  <tr> <td>Area Name: <input type="text" maxlength="50" size="10" id = "searchstring"></td><td> <button id = "search">
  Search
  </button>  </td>
   </tr>
     <!-- style="width:100%" -->
  </table>
  <table >
  <c:forEach items="${userareas}" var="element"> 
  <tr>
    <td><input type="checkbox" name ="selectedArea" value = "${element.polygonId}" ></td>
    <td>${element.polygon_name}</td>
    <td>${element.polygon_type}</td>
    
  </tr>
</c:forEach>  
</table>  
</div> 
 
<div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="button" id ="apply" data-dismiss="modal" class="btn btn-primary">
      Show          
      </button>
</div> 
 
<script>
//var userareas = [];

$(document).ready(function(){ 
	 
    $("#apply").click(function(){   	

        var option = [];
        $.each($("input[name='selectedArea']:checked"), function(){
        	option.push($(this).val());

        });
    	
    	console.info(option);
    	$.ajax({url: "getselecteduserarea?id="+option ,type: 'get'
		     ,success: function(result){
		    	 userareas =  result; 
		    	 console.info(userareas);	
		    	 sourceCustomPolygon.clear();
		    	 for(var i =0 ;i<userareas.length;i++){
		    		 var coords =[];
		    	 	 var userarea = userareas[i];
		    	 	if(userarea.polygontype ==="polygon"){ 
			    		 for(var j =0 ;j<userarea.lon.length;j++){
			    			 coords.push([userarea.lon[j],userarea.lat[j]])
			    		 }
			    	 	var polygon = new ol.geom.Polygon([coords]);
			    	 	//polygon.transform('EPSG:4326', 'EPSG:3857')
			    		var feature = new ol.Feature(polygon);
			    		feature.setProperties({areatype: "userarea", polygonId : userarea.polygonId ,polygonName : userarea.polygonName ,polygontype : userarea.polygontype });		    		
			    				    		
			    	 	sourceCustomPolygon.addFeature( feature );
		    	 	}
		    	 	else if(userarea.polygontype ==="rectangle")
		    	 	{
		    	 		var coords = [[userarea.lon,userarea.lat],[userarea.lon,userarea.latOffset],[userarea.longOffset,userarea.latOffset],[userarea.longOffset,userarea.lat],[userarea.lon,userarea.lat]]
		    	 		var polygon = new ol.geom.Polygon([coords]);
			    	 	//polygon.transform('EPSG:4326', 'EPSG:3857')
			    		var feature = new ol.Feature(polygon);
			    		feature.setProperties({areatype: "userarea" , polygonId : userarea.polygonId ,polygonName : userarea.polygonName ,polygontype : userarea.polygontype });		    		
			    	 	sourceCustomPolygon.addFeature( feature );
		    	 	}
		    	 	else if(userarea.polygontype ==="circle"){ 
		    	 		var centerLongitudeLatitude =[userarea.lon,userarea.lat];
		    	 		var radiusinmeters = userarea.radius;
		    	 		console.info(map.getView().getProjection().getMetersPerUnit());
		    	 		var radiusinunits = userarea.radius / (map.getView().getProjection().getMetersPerUnit());
		    	 		
		    	 		var circle= new ol.geom.Circle(centerLongitudeLatitude, radiusinunits);
		    	 		//circle.transform('EPSG:4326', 'EPSG:3857');
		    	 		console.info(circle);
		    	 		var feature = new ol.Feature(circle);
			    		feature.setProperties({areatype: "userarea",polygonId : userarea.polygonId ,polygonName : userarea.polygonName ,polygontype : userarea.polygontype });		    		
		    	 		
		    	 		sourceCustomPolygon.addFeature( feature ); 
		    	 	}
		    	 	
		    	 }		    	 
   		 }});    	
    	
    });     
 
});




$("#search").click(function(){	
	
  openModalForm("custompolygonform?userareaOption=${userareaOption}&search="+$("#searchstring").val());
	
})


 </script>
 