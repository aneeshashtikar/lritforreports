<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Manufacturer List</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
	<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<spring:url value="/manufacturer/getmanufacturer" var="getmanufacturer" />
<spring:url value="/model/viewmodel" var="viewmodel" />
<spring:url value="/manufacturer/deletemanufacturer" var="deletemanufacturer" />
<spring:url value="/manufacturer/persistmanufacturer" var="persistmanufacturer" />

<style>
/* .side-box {
	margin-bottom: 0px;
	/* height: 200px;
} */
</style>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<script type="text/javascript">
	var resultSet = function() {
		this.item = {};
	};

	var listtable = new resultSet();

	var targetTableId = 'viewmanufacturer-table';

	var dTable = function(targetTableId, resultSet) {
		if ($.fn.dataTable.isDataTable(targetTableId)) {
			resultSet.item.ajax.reload();
		} else {
			
			resultSet.item = $('#viewmanufacturer-table').DataTable({

				"ajax" : {
					"dataType" : 'json',
					"url" : "${getmanufacturer}",
					"type" : "GET",
					"dataSrc" : ""
				},
				"columnDefs": [
		            {
		                "targets": [ 0 ],
		                "visible": false,
		                "searchable": false
		            }],
				"responsive" : {
					"details" : {
						"type" : 'column',
						"target" : 'tr'
					}
				}
			});
		}
	}

	$(document).ready(function() {

		dTable('#' + targetTableId, listtable);

		 /* Zoom out handle for view model button */
		   $('body').on('click','#viewmanufacturer-table tbody td>button[name=btnViewModel]',function() {
				var manufacturerId = listtable.item.row($(this).parents('tr')).data().manufacturerId;
				var url = "${viewmodel}/"+manufacturerId;
				window.location = url;
												});  

		 /* Zoom in handle for view model button */
			 $('body').on('click','#viewmanufacturer-table.collapsed tbody li span>button[name=btnViewModel]',function() {
				var cellIndex = listtable.item.responsive.index($(this).parent().parent());
				var manufacturerId = listtable.item.data()[cellIndex.row].manufacturerId;
				var url = "${viewmodel}/"+manufacturerId;
				window.location = url;
												});  
			
			<%--Zoom out handle for delete button--%>
		 	 $('body').on('click','#viewmanufacturer-table tbody td>button[name=btnDelete]',function() {
				var manufacturerId = listtable.item.row($(this).parents('tr')).data().manufacturerId;
				var url = "${deletemanufacturer}/"+manufacturerId;
				if (confirm(" Are You Sure To Delete This MANUFACTURER And All Its MODELS ? ")) 
					{window.location = url;}
												}); 

		   	/* Zoom in handle for delete button */  
			 $('body').on('click','#viewmanufacturer-table.collapsed tbody li span>button[name=btnDelete]',function() {
				var cellIndex = listtable.item.responsive.index($(this).parent().parent());
				var manufacturerId = listtable.item.data()[cellIndex.row].manufacturerId;
				var url = "${deletemanufacturer}/"+manufacturerId;
				if (confirm(" Are You Sure To Delete This MANUFACTURER And All Its MODELS ? ")) 
					{window.location = url;}
			 									}); 
	});
</script>

</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">
		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">

						<!-- Add New Manufacturer Code -->
						
						<div class="tab-pane">
							<!--.box -->
							<div id="showError">
							<c:if test="${error != null }">
									<div class='alert alert-danger colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
										${error }
									</div>
								</c:if>
								<c:if test="${success != null }">
									<div class='alert alert-success colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
										${success }
									</div>
								</c:if>
							</div>
							<sec:authorize access="hasAuthority('editmanufacturerandmodel')">
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title" align="center">Add Manufacturer</h3>
								</div>
								<!-- /.form -->
								<form:form class="form-horizontal" id="userManufacturer"
									action="${persistmanufacturer}" method="post" modelAttribute="portalManu">
									<div class="box-body">
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="manufacturerName" class="col-sm-4">Manufacturer
													Name</label>
												<div class="col-sm-8">
													<form:input type="text" class="form-control"
														id="manufacturerName" name="manufacturerName"
														path="manufacturerName" required="required" />
												</div>
											</div>
										</div>
									</div>
									<!-- /.box -->
									<div class="box-footer">
										<input type="submit" class="btn btn-primary pull-right"
											value="Submit" />
									</div>
								</form:form>
								<!-- /.form -->
							</div>
							</sec:authorize>
						</div>
						<!--/.col (left) -->
						<!-- </div> -->
						<!-- Add New Manufacturer Code End-->
						<!-- left column -->
						<div class="tab-pane">
							<div class="box box-info">
								<!-- <div class="container col-md-12"> -->
								<div class="box-header with-border">
									<h3 class="box-title" align="center">Manufacturer List</h3>
								</div>
								<div class="nav-tabs-custom" id="tabs">
									<div class="tab-content">
										<div class="box-body">
											<div>
												<table id="viewmanufacturer-table"
													class="table table-bordered table-striped "
													style="width: 100%">
													<thead>
														<tr>
															<th data-data="manufacturerId" data-default-content="NA">Manufacturer Id</th>
															<th data-data="manufacturerName" data-default-content="NA">Manufacturer Name</th>
															<th data-data="null"
																data-default-content="<button name='btnViewModel'  class='btn btn-info'> View Model</button>"></th>
															<sec:authorize access="hasAuthority('editmanufacturerandmodel')">
															<th data-data="null"
																data-default-content="<button name='btnDelete'  class='btn btn-danger'> Delete</button>"></th>
														</sec:authorize>
														</tr>
													</thead>

												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.form -->
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<div class="control-sidebar-bg"></div>
	<!-- </div> -->
	<!-- ./wrapper -->



</body>
</html>