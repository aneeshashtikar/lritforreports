<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Vessel List</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<%-- <spring:url value="/resources/bower_components/Ionicons/css/ionicons.min.css" var="ioniconsmincss" /> --%>
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- Theme style -->
<%-- <spring:url value="/resources/dist/css/AdminLTE.min.css" var="adminltemincss" /> --%>
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<%-- <spring:url value="/resources/dist/css/skins/_all-skins.min.css" var="allskinmincss" /> --%>
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<%-- <spring:url value="/resources/custom.css" var="customcss" /> 
<link rel="stylesheet" href="${customcss}"> --%>

<%-- <spring:url value="/login" var="Login" /> --%>
<%-- <spring:url value="/ship/approvedvessel" var="approvedvessel" /> --%>
<spring:url value="/vessels/getallvessel" var="getallvessel" />
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	
	
<!-- Custom -->

<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Custom theme css -->
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">

<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>

<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<spring:url value="/vessels/getallvessel" var="getallvessel" />
<spring:url value="/vessels/allvesselforcompany" var="companyvessel" />
<spring:url value="/vessels/activevesselforcompany" var="activecompanyvessel" />
<spring:url value="/vessels/inactivevesselforcompany" var="inactivecompanyvessel" />
<spring:url value="/vessels/submittedvesselforcompany" var="submittedcompanyvessel" />
<spring:url value="/vessels/getsubmittedvessel" var="submittedvessel" /> 
<spring:url value="/vessels/inactivevessel" var="inactivevessel" />
<spring:url value="/vessels/getactivevessel" var="activevessel" />

<spring:url value="/vessels/registeredcompanyvessel" var="registeredcompanyvessel" />
<spring:url value="/vessels/registeredvessel" var="registeredvessel" /> 
<spring:url value="/ship/viewapprove" var="viewapprove" />
<spring:url value="/ship/editvessel" var="editvessel" />
<spring:url value="/ship/viewvessel" var="viewvessel" />
<spring:url value="/vessels/soldvessel" var="soldvessel" /> 
<spring:url value="/vessels/scrapvessel" var="scrapvessel" /> 
<spring:url value="/vessels/soldcompanyvessel" var="soldcompanyvessel" />
<spring:url value="/vessels/scrapcompanyvessel" var="scrapcompanyvessel" />
<spring:url value="/vessels/allactivevessel" var="allactivevessel" />
<spring:url value="/vessels/allactivevesselforcompany" var="allactivevesselforcompany" />

<style> 
 .table_wrapper{
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

</style>

<script type="text/javascript">

var url = '${getallvessel}';

var _companyCode = null;

var resultSet = function() {
	this.item = {};
};

var listtable = new resultSet();

var targetTableId = 'viewvessel-table';
		
var dTable1 = function(targetTableId, resultSet, url) {
	if ($.fn.dataTable.isDataTable(targetTableId)) {
		resultSet.item.ajax.reload();
	} else {
		if(url === '${submittedvessel}')
		{
			resultSet.item = $('#viewvessel-table').DataTable({
				"ajax" : {
					"dataType" : 'json',
					"url" : url,
					"type" : "GET",
					"dataSrc" : ""
				},
				"columnDefs": [
		            {
		                "targets": [ 5 ],
		                "visible": false,
		                "searchable": false
		            },
		            {
		                "targets": [ 6 ],
		                "visible": false,
		                "searchable": false
		            },
		            {
		            	"render": function ( data, type, row ) {
		                	return '<form action="${viewapprove}" method="post" >' +
			                	'<input type="hidden" value="'+ data + '" name="vesselId" />' +
		                	'<input type = "submit" value = "'+ data + '" class="btn btn-link" />' + 
		                	'</form>';
		                },
		                "targets": 0
			           }
			],
				"responsive" : {
					"details" : {
						"type" : 'column',
						"target" : 'tr'
					}
				}

			});
		}
		else
		{
			resultSet.item = $('#viewvessel-table').DataTable({

				"ajax" : {
					"dataType" : 'json',
					"url" : url,
					"type" : "GET",
					"dataSrc" : "",
				},
				"responsive" : {
					"details" : {
						"type" : 'column',
						"target" : 'tr'
					}
				}

			});
		}
			
	}
}

var dTable2 = function(targetTableId, resultSet, url, _companyCode) {
	if ($.fn.dataTable.isDataTable(targetTableId)) {
		resultSet.item.ajax.reload();
	} else {
			resultSet.item = $('#viewvessel-table').DataTable({

				"ajax" : {
					"dataType" : 'json',
					"url" : url,
					"type" : "GET",
					"dataSrc" : "",
					"data" : function(d){
						d.companyCode = _companyCode;
					}
				},
				"responsive" : {
					"details" : {
						"type" : 'column',
						"target" : 'tr'
					}
				}

			});
	}
}
	
$(document).ready(function() {
	
	$('#showvessel').each(function () {
        $(this).val($(this).find('option[selected]').val());
    });
	
	var _userCategory = $('#userCategory').val();
	//alert(_userCategory);
	
	if(_userCategory == 'USER_CATEGORY_SC')	
	{
		_companyCode = $('#companyCode').val();
		//alert(_companyCode);
		url = '${companyvessel}';	
		//alert(url);
		dTable2('#'+targetTableId, listtable, url, _companyCode);
	}
	
    dTable1('#'+targetTableId, listtable, url);
    
    $('#showvessel').change(function() {
    	
    	var _showvessel = $('#showvessel').val();
    	//alert(_showvessel);
    	
    	$('#viewvessel-table').DataTable().clear().destroy();
    	
    	if(_userCategory == 'USER_CATEGORY_SC')	
    	{
    		if(_showvessel == 'Inactive')
    			dTable2('#'+targetTableId, listtable, '${inactivecompanyvessel}', _companyCode);
    		else if(_showvessel == 'Active')
    			dTable2('#'+targetTableId, listtable, '${allactivevesselforcompany}', _companyCode);
    		else if(_showvessel == 'Sold')
    			dTable2('#'+targetTableId, listtable, '${soldcompanyvessel}', _companyCode);
    		else if(_showvessel == 'Scrap')
    			dTable2('#'+targetTableId, listtable, '${scrapcompanyvessel}', _companyCode);
			else 
				dTable2('#'+targetTableId, listtable, '${companyvessel}', _companyCode);
    		/* else if(_showvessel == 'Active')
    			dTable2('#'+targetTableId, listtable, '${activecompanyvessel}', _companyCode); */
    		/* else if(_showvessel == 'Submitted')
    			dTable2('#'+targetTableId, listtable, '${submittedcompanyvessel}', _companyCode); 
    			else if(_showvessel == 'Ship_Registered')
    			dTable2('#'+targetTableId, listtable, '${registeredcompanyvessel}', _companyCode);
    		*/
    		
    	}
    	else
    	{
    		if(_showvessel == 'Inactive')
    			dTable1('#'+targetTableId, listtable, '${inactivevessel}');
    		else if(_showvessel == 'Active')
    			dTable1('#'+targetTableId, listtable, '${allactivevessel}');
    		else if(_showvessel == 'Sold')
    			dTable1('#'+targetTableId, listtable, '${soldvessel}');
    		else if(_showvessel == 'Scrap')
    			dTable1('#'+targetTableId, listtable, '${scrapvessel}');
    		else 
    			dTable1('#'+targetTableId, listtable, '${getallvessel}');
    		/* else if(_showvessel == 'Active')
    			dTable1('#'+targetTableId, listtable, '${activevessel}');
    		else if(_showvessel == 'Submitted')
    			dTable1('#'+targetTableId, listtable, '${submittedvessel}'); 
    		else if(_showvessel == 'Ship_Registered')
    			dTable1('#'+targetTableId, listtable, '${registeredvessel}');
    		*/
    		
    	}  
    });
	
	/* Zoom out handle for edit button */
	$('body').on('click','#viewvessel-table tbody td>button[name=btnedit]',function() {
		var vesselId = listtable.item.row($(this).parents('tr')).data().vesselId;
		var url = "${editvessel}/"+ vesselId;
			window.location = url;
										});
	
	/* Zoom in handle for edit button */
	$('body').on('click','#viewvessel-table.collapsed tbody li span>button[name=btnedit]',function() {
		var cellIndex = listtable.item.responsive.index($(this).parent().parent());
		var vesselId = listtable.item.data()[cellIndex.row].vesselId;
		var url = "${editvessel}/"+ vesselId;
			window.location = url;
								});
	
	/* Zoom out handle for view button */
	$('body').on('click','#viewvessel-table tbody td>button[name=btnview]',function() {
		var vesselId = listtable.item.row($(this).parents('tr')).data().vesselId;
			 /* alert("Zoom out handle for view button vesselId "+ vesselId); */
		var url = "${viewvessel}/"+ vesselId;
		window.location = url;
	});
	
	/* Zoom in handle for view button */
	$('body').on('click','#viewvessel-table.collapsed tbody li span>button[name=btnbtnview]',function() {
		var cellIndex = listtable.item.responsive.index($(this).parent().parent());
		var vesselId = listtable.item.data()[cellIndex.row].vesselId;
				/* alert("Zoom in handle for view button vesselId "+ vesselId); */
		var url = "${viewvessel}/"+ vesselId;
		window.location = url;
	});
});

	
</script>


</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">
					
					<!-- Show All Ship -->
					
					<div class="tab-pane">
						<div id="showError">
							<c:if test="${error != null }">
									<div class='alert alert-danger colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
										${error }
									allvessel</div>
								</c:if>
								<c:if test="${success != null }">
									<div class='alert alert-success colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
										${success }
									</div>
								</c:if>
						</div>
						<div class="box box-info">
								<!-- <div class="box-header with-border">
									<h3 class="box-title" align="center">Vessel</h3>
								</div> -->
								<input type="hidden" id="userCategory" value="${userCategory}"/>
								<input type="hidden" id="companyCode" value="${companyCode}"/>
								<div class="box-header with-border">
									<h3 class="box-title" align="center">List Of Vessels</h3>
								</div>
									<div class="box-body">
										<!-- FIRST ROW -->
										<div class="row">
											<div class="form-group col-sm-6">
												<label for="showvessel" class="col-sm-4">Show Vessel</label>
												<div class="col-sm-8">
													<select class="form-control" id="showvessel"
														name="showvessel" >
														<option value="All" selected="selected">All</option>
														<option value="Active">Active</option>
														<option value="Inactive">Inactive</option>
														<option value="Sold">Sold</option>
														<option value="Scrap">Scrap</option>
														<!-- <option value="Ship_Registered">Ship_Registered</option> -->
													</select>
												</div>
											</div>
										</div>
									</div>
									<!-- /.box -->
							</div>
							</div>
					<div class="tab-pane">	
						<!-- <div class="box box-info"> -->
							<!-- <div class="container col-md-12"> -->
								 <!-- <div class="box-header with-border">
									<h3 class="box-title" align="center">List Of Vessels</h3>
								</div> --> 
								<!-- <div class="nav-tabs-custom" id="tabs"> -->
									<div class="tab-content">
										<div class="box-body">
											<div>
												<table id="viewvessel-table" class="table table-bordered table-striped " style="width: 100%">
													<thead>
														<tr>		
															<th data-data="vesselId" data-default-content="NA">Vessel Id</th>																	
															<th data-data="imoNo" data-default-content="NA">IMO No.</th>
															<th data-data="mmsiNo" data-default-content="NA">MMSI No</th>
															<th data-data="vesselName" data-default-content="NA">Ship Name</th>
															<th data-data="callSign" data-default-content="NA">Call Sign</th>
															<!-- <th data-data="status" data-default-content="NA">Status</th> -->
															<th data-data="regStatus" data-default-content="NA">Registration Status</th>
																<%-- <sec:authorize access="hasAnyAuthority('viewvessel')"> --%>
															<th data-data="null" data-default-content="<button name='btnview'  class='btn btn-info'> View</button>"></th>
															<%-- </sec:authorize> --%>
															<%-- <sec:authorize access="hasAnyAuthority('editvessel')"> --%>
															<th data-data="null" data-default-content="<button name='btnedit'  class='btn btn-info'> Edit</button>"></th>
														<%--  </sec:authorize> --%>
														 </tr>
													</thead>
												</table>
											</div>
										</div>
									</div>
								<!-- </div> -->
							<!-- </div> -->
							<!-- /.form -->
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
		</div>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<div class="control-sidebar-bg"></div>
	<!-- </div> -->
	<!-- ./wrapper -->
</body>
</html>