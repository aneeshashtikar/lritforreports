<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Vessel Registration Form</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<%-- <spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url> --%>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
<spring:url value="/resources/js/vessel_button.js"
	var="buttonjs" />
<script src="${buttonjs}"></script>
<spring:url value="/resources/js/validateform.js"
	var="validateformjs" />
<script src="${validateformjs}"></script>

<spring:url value="/login" var="Login" />
<spring:url value="/ship/getcsodpalist/" var="getcsodpalist" />
<spring:url value="/ship/getcompanydetails" var="getcompanydetails" />
<spring:url value="/ship/getuserdetails" var="getuserdetails" />
<spring:url value="/ship/persistvessel" var="persistvessel" />
<spring:url value="/ship/errorPage" var="errorPage" />


<style>
.table_wrapper {
	display: block;
	overflow-x: auto;
	white-space: nowrap;
}
</style>

<script type="text/javascript">
var type, loginId, buttonId1, number;

function showTableCompany(targetTableId, buttontype1, buttontype2) {

	console.log("Inside show table");
	tableId = '#' + targetTableId;
	buttonId1 = '#' + buttontype1;
	buttonId2 = '#' + buttontype2;
	
	if ($.fn.dataTable.isDataTable(tableId)) {
		$('#CompanyDetail').DataTable().ajax.reload();
	} else {
		var table = $('#CompanyDetail').DataTable({
			dom : 'lfrtip',
			"ajax" : {
				"url" : "${getcompanydetails}",
				"type" : "GET",
				"dataSrc" : "",
				"error" : function(xhr, error, thrown) {
					window.location = "${errorPage}";
				}
			},
			"columnDefs" : [ {
				"orderable" : false,
				"className" : 'select-checkbox',
				"targets" : 0,
				"defaultContent" : ""
			} ],
			"select" : {
				"style" : 'os',
				"selector" : 'td:first-child'
			},
			"order" : [ [ 1, 'asc' ] ],
			"columns" : [ {
				"data" : ""
			}, {
				"data" : "companyCode"
			}, {
				"data" : "companyName"
			} ]

		});
		$('#selectCompany').on('click', function() {
			$.each(table.rows('.selected').data(), function() {
				companyCode = this["companyCode"];
				companyName = this["companyName"];
				$(buttonId1).val(companyCode);
				/* alert(companyCode); */
				$(buttonId2).val(companyName);
				/* alert(companyName); */
			});
		});
	}
}

function showTableUser(targetTableId, buttontype1, buttontype2) {
	/* var loginId; */
	console.log("Inside show table");
	companyCode = $(buttonId1).val();
	//alert(companyCode);
	tableId = '#' + targetTableId;
	buttonId1 = '#' + buttontype1;
	buttonId2 = '#' + buttontype2;
	//alert(buttonId);
	
	if ($.fn.dataTable.isDataTable(tableId)) {
		$('#UserDetail').DataTable().ajax.reload();
	} else {
		var table = $('#UserDetail').DataTable({
			dom : 'lfrtip',
			"ajax" : {
				"url" : "${getuserdetails}",
				"type" : "GET",
				"dataSrc" : "",
				"data" : function(d) {
					d.companyCode = companyCode;
				},
				"error" : function(xhr, error, thrown) {
					window.location = "${errorPage}";
				}
			},
			"columnDefs" : [ {
				"orderable" : false,
				"className" : 'select-checkbox',
				"targets" : 0,
				"defaultContent" : ""
			} ],
			"select" : {
				"style" : 'os',
				"selector" : 'td:first-child'
			},
			"order" : [ [ 1, 'asc' ] ],
			"columns" : [ {
				"data" : ""
			}, {
				"data" : "loginId"
			}, {
				"data" : "name"
			}, {
				"data" : "telephone"
			}, {
				"data" : "emailid"
			} ]

		});

		$('#selectUser').on('click', function() {
			$.each(table.rows('.selected').data(), function() {
				loginId = this["loginId"];
				name = this["name"];
				//alert(buttonId);
				$(buttonId1).val(loginId);
				$(buttonId2).val(name);
			});
		});
	}
}

function showTableCSODPA(buttontype1, buttontype2, type, loginId, targetTableId, number) {
	if(number==1)
		loginId = $('#userId1').val();
	else
		loginId = $(buttonId1).val();
	
	console.log("Inside show table");
	
	//alert(loginId);
	
	dTable('#' + targetTableId, type, loginId, buttontype1, buttontype2, number);
}

var dTable = function(targetTableId, type, loginId, buttontype1, buttontype2, number) {
	//alert(loginId);
	//alert(csoId);
	var buttonid1 = '#' + buttontype1;
	var buttonid2 = '#' + buttontype2;
	var buttonid3 = '#acsoId' + number;
	//alert(buttonid3);
	//alert("button id " + buttonid);
	//var url = null;
	//alert(url);
	var resultSet = function() {
		this.item = {};
	};

	var listtable = new resultSet();

	if ($.fn.dataTable.isDataTable(targetTableId)) {
		$('#CSODetail').DataTable().clear().destroy();
		dTable(targetTableId, type, loginId, buttontype1, buttontype2, number);

	} else {
		//alert(loginId);
		console.log("in else", targetTableId, type, loginId);
		resultSet.item = $('#CSODetail').DataTable({
			dom : 'lfrtip',
			"ajax" : {
				"url" : "${getcsodpalist}",
				"type" : "GET",
				"dataSrc" : "",
				"data" : function(d) {
					d.type = type;
					d.loginId = loginId;
				},
				"error" : function(xhr, error, thrown) {
					window.location = "${errorPage}";
				}
			},
			"columnDefs" : [ {
				"orderable" : false,
				"className" : 'select-checkbox',
				"targets" : 0,
				"defaultContent" : ""
			}, 
			{
				"targets": [ 5 ],
                "visible": false,
                "searchable": false
			} ],
			"select" : {
				"style" : 'os',
				"selector" : 'td:first-child'
			},
			"order" : [ [ 1, 'asc' ] ],
			"columns" : [ {
				"data" : ""
			}, {
				"data" : "lritCsoIdSeq"
			}, {
				"data" : "name"
			}, {
				"data" : "telephoneOffice"
			}, {
				"data" : "emailId"
			}, {
				"data" : "alternateCsoId"
			} ],
			"destroy" : true
		});

		$('#selectCSO').on('click', function() {
			$.each(resultSet.item.rows('.selected').data(), function() {
				lritCsoIdSeq = this["lritCsoIdSeq"];
				name = this["name"];
				alternateCsoId = this["alternateCsoId"];
				
				$(buttonid1).val(lritCsoIdSeq);
				$(buttonid2).val(name);
				
				if(type == 'cso')
				{
					//alert("inside cso");
					//alert(alternateCsoId);
					$(buttonid3).val(alternateCsoId);
				}
				//csoId = $(buttonid1).val();
			});

		});
	}

}
</script>
</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">
		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">

						<!-- Add New Vessel Code -->

						<div class="tab-pane">
							<!--.box -->
							<div id="showError">
								<c:if test="${error != null }">
									<div class='alert alert-danger colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert'
											aria-label='close'>&times;</a> ${error }
									</div>
								</c:if>
								<c:if test="${success != null }">
									<div class='alert alert-success colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert'
											aria-label='close'>&times;</a> ${success }
									</div>
								</c:if>
							</div>
							<div class="error " id="formerror" style="color: red; font-size: 16px; font-weight: bold;"></div>
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title" align="center">Vessel Registration</h3>
								</div>
								<!-- /.form -->
								<form:form class="form-horizontal" id="shipForm"
									action="${persistvessel}" name="shipborne" method="post"
									modelAttribute="vrdto" onsubmit="return validateForm()"
									enctype="multipart/form-data">
									<div class="box-body">
										<!-- Custom form -->
										<div class="nav-tabs-custom" id="tabs">
											<ul class="nav nav-tabs">
												<!-- <li class="active" id="shipdetfirst"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#shipDet" data-toggle="tab"><b>Ship Details</b></a></li>
												<li id="uploaddocs"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#uploadDocs" data-toggle="tab">Upload Vessel
														Documents</a></li>
												<li id="compdet"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#compDet" data-toggle="tab">Company Details</a></li>
												<li id="shipbornedetlast"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#shipborneDet" data-toggle="tab">Shipbourne
														Equipment Id Detail</a></li>
												<li class="pull-right"><a href="#" class="text-muted"><i
														class="fa fa-gear"></i></a></li> -->
												<li class="active" id="shipdetfirst"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#shipDet" data-toggle="tab"><b>Ship Details</b></a></li>
												<li id="uploaddocs"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#uploadDocs" data-toggle="tab">Upload Vessel
														Documents</a></li>
												<li id="shipbornedet"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#shipborneDet" data-toggle="tab">Shipbourne
														Equipment Id Detail</a></li>
												<li id="compdetlast"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#compDet" data-toggle="tab">Company Details</a></li>
												<li class="pull-right"><a href="#" class="text-muted"><i 
														class="fa fa-gear"></i></a></li>
											</ul>
											<input type="hidden" id="repurchaseflag" name="repurchaseflag" value="${repurchase}"/>
											<div class="tab-content">
												<!-- First Tab -->
												<div class="tab-pane active" id="shipDet">
													<!-- FIRST ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="imoNo" class="col-sm-4 ">IMO Number</label>
															<div class="col-sm-8">
																<form:input type="text" id="imoNo" class="form-control"
																	path="shipEq.vesselDet.imoNo" placeholder="IMO Number" readonly="true"/>
																<span id="imoError" style="color: red; font-size: 15px;"></span>
															</div>

														</div>
														<div class="form-group col-sm-6">
															<label for="mmsiNo" class="col-sm-4">MMSI Number</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="shipEq.vesselDet.mmsiNo" id="mmsiNo"
																	placeholder="MMSI Number" readonly="true"/>
																<span id="mmsiError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>

													<!-- SECOND ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="vesselName" class="col-sm-4 ">Ship
																Name</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="shipEq.vesselDet.vesselName" id="vesselName"
																	placeholder="Ship Name" />
																<span id="vesselNameError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="callSign" class="col-sm-4 ">Call Sign</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	id="callSign" path="shipEq.vesselDet.callSign"
																	placeholder="call sign" />
																<span id="callSignError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>

													<!-- THIRD ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="yearOfBuilt" class="col-sm-4"> Year
																of Built</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="shipEq.vesselDet.yearOfBuilt" id="yearOfBuilt"
																	placeholder="Year of Built" />
																<span id="yearOfBuiltError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="grossTonnage" class="col-sm-4">Gross
																Tonnage</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	id="grossTonnage" path="shipEq.vesselDet.grossTonnage"
																	placeholder="Gross Tonnage" />
																<span id="grossTonnageError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>

													<!-- FOURTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="deadWeight" class="col-sm-4 ">Dead
																Weight(DWT)</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="shipEq.vesselDet.deadWeight" id="deadWeight"
																	placeholder="Dead Weight" />
																<span id="deadWeightError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="registrationNo" class="col-sm-4 ">Registration
																No</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	id="registrationNo"
																	path="shipEq.vesselDet.registrationNo" />
																<span id="registrationNoError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>

													<!-- FIFTH ROW -->
													<div class="row">
														<%--  <div class="form-group col-sm-6">
															<label for="timeDelay" class="col-sm-4 ">Time
																Delay</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="shipEq.vesselDet.timeDelay" id="timeDelay"
																	placeholder="in minutes" />
																<span id="timeDelayError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>  --%>
														<div class="form-group col-sm-6">
															<label for="imoVesselType" class="col-sm-4">IMO
																Vessel Type</label>
															<div class="col-sm-8">
																<%-- <form:input type="text" class="form-control"
															path="shipEq.vesselDet.imoVesselType" id="imoVesselType" readonly /> --%>
																<form:select class="form-control"
																	path="shipEq.vesselDet.imoVesselType"
																	id="imoVesselType">
																	<form:option value="" label="Select"></form:option>
																	<form:option value="0100">0100: Passenger</form:option>
																	<form:option value="0200">0200: Cargo</form:option>
																	<form:option value="0300">0300: Tanker</form:option>
																	<form:option value="0400">0400: Mobile offshore drilling unit</form:option>
																	<form:option value="9900">9900: Other</form:option>
																</form:select>
																
																<span id="imoVesselTypeError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="vesselType" class="col-sm-4">Vessel
																Type</label>
															<div class="col-sm-8">
																<%-- <form:input type="text" class="form-control"
															path="shipEq.vesselDet.vesselType" id="vesselType" readonly /> --%>
																<form:select class="form-control"
																	path="shipEq.vesselDet.vesselType" id="vesselType"
																	onchange="findCategorySet()" >
																	<form:option value="" label="Select"></form:option>
																	<c:forEach var="vesselType" items="${vesselTypeList}">
																		<form:option value="${vesselType}"></form:option>
																	</c:forEach>
																</form:select>
																<span id="vesselTypeError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>

													<!-- SIXTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="vesselCategory" class="col-sm-4">Vessel
																Category</label>
															<div class="col-sm-8">
																<%-- <form:input type="text" class="form-control"
															path="shipEq.vesselDet.vesselCategory" id="vesselCategory" readonly /> --%>
																<form:select class="form-control"
																	path="shipEq.vesselDet.vesselCategory"
																	id="vesselCategory">
																	<form:option value="" label="Select"></form:option>
																</form:select>
																<span id="vesselCategoryError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
														<%-- <form:input type="hidden" id="groupId"
															path="shipEq.vesselDet.portalVesselGroup.groupId" /> --%>
														<div class="form-group col-sm-6">
															<label for="vesselGroup" class="col-sm-4">Vessel
																Group</label>
															<div class="col-sm-8">
																<%-- <form:input type="text" class="form-control"
																	path="shipEq.vesselDet.portalVesselGroup.groupName"
																	id="vesselGroup" readonly="true" /> --%>
																<form:select class="form-control"
																	path="shipEq.vesselDet.portalVesselGroup.groupId"
																	id="vesselGroup">
																	<form:option value="" label="Select" />
																	<c:forEach var="groupname" items="${groupNames}">
																		<form:option value="${groupname.getGroupId()}">${groupname.getGroupName()}</form:option>
																	</c:forEach>
																</form:select>
																<span id="vesselGroupError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>
													
													<!-- SEVENTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="piClub" class="col-sm-4 ">P & I Club</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="shipEq.vesselDet.piClub" id="piClub"
																	placeholder="P & i cLUB" />
																<span id="piClubError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="vesselImage" class="col-sm-4">Vessel
																Image</label>
															<div class="col-sm-8">
																<input type="file" class="form-control file-input"
																	name="shipEq.vesselDet.vesselImage1" id="vesselImage" />
																<span id="vesselImageError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>

													<!-- EIGHTH ROW -->
													<!-- <div class="row">
														
													</div> -->
												</div>
												<!-- First Tab End-->

												<!-- /.tab-pane -->
												<div class="tab-pane" id="uploadDocs">
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="emailDoc" class="col-sm-4 "> Request
																From Owner</label>
															<div class="col-sm-8">
																<input type="file" class="form-control file-input"
																	name="docs.emailDoc1" id="emailDoc" /> <span
																	id="emailDocError" style="color: red; font-size: 15px;"></span>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="tataLritDoc" class="col-sm-4 "> CSP
																Registration Form</label>
															<div class="col-sm-8">
																<input type="file" class="form-control file-input"
																	name="docs.tataLritDoc1" id="tataLritDoc" /> <span
																	id="tataLritDocError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>
													<!-- TENTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="flagRegDoc" class="col-sm-4 "> Flag
																Registration Certificate</label>
															<div class="col-sm-8">
																<input type="file" class="form-control file-input"
																	name="docs.flagRegDoc1" id="flagRegDoc" /> <span
																	id="flagRegDocError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="conformanceTestCertDoc" class="col-sm-4 ">
																Conformance Test Certificate</label>
															<div class="col-sm-8">
																<input type="file" class="form-control file-input"
																	name="docs.conformanceTestCertDoc1"
																	id="conformanceTestCertDoc" /> <span
																	id="conformanceTestCertDocError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>
													<!-- ELEVENTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="additionalDoc" class="col-sm-4 ">
																Additional Documents(If Required)</label>
															<div class="col-sm-8">
																<input type="file" class="form-control file-input"
																	name="docs.additionalDoc1" id="additionalDoc" /> <span
																	id="additionalDocError"
																	style="color: red; font-size: 15px;"></span>
															</div>
														</div>
													</div>
												</div>
												<!-- /.tab-pane -->

												<!-- /.tab-pane -->
												<div class="tab-pane" id="compDet">
													<!-- Owner Company -->
													<table class="table no-margin" id="csoDet">
														<!-- here should go some titles... -->
														<thead>
															<tr>
																<th></th>
																<th>Company Name</th>
																<th>User Name</th>
																<th>Current CSO Name</th>
																<!-- <th>Alternate CSO Name</th> -->
																<th>DPA Name</th>
															</tr>
														<thead>
															<tboby> <c:forEach items="${vrdto.pscvrList}"
																var="pscvr" varStatus="stat">
																<tr>
																	<form:input type="hidden" id="status"
																		path="pscvrList[${stat.index}].status"
																		value="${stat.count}" />
																	<td><c:if test="${stat.count == 1}">
																			<label>Owner Company Details</label>
																		</c:if> <c:if test="${stat.count == 2}">
																			<label>Technical Company Details</label>
																		</c:if> <c:if test="${stat.count == 3}">
																			<label>Commercial Company Details</label>
																		</c:if></td>
																	<td>
																		<%-- <c:if test="${userCategory == 'USER_CATEGORY_SC'}"> --%>
																		<c:if test="${stat.count == 1}">
																			<form:input type="hidden"
																			id="companyCode${stat.count}" value="${requestScope.ownerCompanyCode}"
																			path="pscvrList[${stat.index}].customId.userDet.portalShippingCompany.companyCode" />
																			<input type="text" name="companyName" value=${requestScope.ownerCompanyName}
																			class="form-control" id="companyName${stat.count}" readonly="readonly"/>
																		</c:if>
																		<c:if test="${stat.count == 2 || stat.count == 3}" >
																			<form:input type="hidden"
																			id="companyCode${stat.count}"
																			path="pscvrList[${stat.index}].customId.userDet.portalShippingCompany.companyCode" />
																			<input type="text" name="companyName"
																			class="form-control" id="companyName${stat.count}" />
																			<button type="submit" data-toggle="modal"
																			id="companyCode" data-target="#myModalCompany"
																			onclick="showTableCompany('CompanyDetail', 'companyCode${stat.count}','companyName${stat.count}')">
																			<i class=" fa fa-search"></i>
																			</button>
																		</c:if>
																		<%-- </c:if> --%>
																		<%-- <c:if test="${userCategory == 'USER_CATEGORY_DGS'}">
																			<form:input type="hidden"
																			id="companyCode${stat.count}"
																			path="pscvrList[${stat.index}].customId.userDet.portalShippingCompany.companyCode" />
																			<input type="text" name="companyName"
																			class="form-control" id="companyName${stat.count}" />
																			<button type="submit" data-toggle="modal"
																			id="companyCode" data-target="#myModalCompany"
																			onclick="showTableCompany('CompanyDetail', 'companyCode${stat.count}','companyName${stat.count}')">
																			<i class=" fa fa-search"></i>
																			</button>
																		</c:if> --%>
																		 <span id="companyCode${stat.count}Error" style="color: red; font-size: 15px;"></span>
																	</td>
																		<td>
																		<%-- <c:if test="${userCategory == 'USER_CATEGORY_SC'}"> --%>
																		<c:if test="${stat.count == 1}">
																			<form:input type="hidden"
																			id="userId${stat.count}" value="${requestScope.userId}"
																			path="pscvrList[${stat.index}].customId.userDet.loginId" />
																			<input type="text" name="userName" value="${requestScope.userName}"
																			class="form-control" id="userName${stat.count}" readonly="readonly"/>
																		</c:if>
																		<c:if test="${stat.count == 2 || stat.count == 3}" >
																			<form:input type="hidden"
																			id="userId${stat.count}"
																			path="pscvrList[${stat.index}].customId.userDet.loginId" />
																			<input type="text" name="userName"
																			class="form-control" id="userName${stat.count}" />
																			<button type="submit" data-toggle="modal" id="userId"
																			data-target="#myModalUser"
																			onclick="showTableUser('UserDetail', 'userId${stat.count}', 'userName${stat.count}')">
																			<i class=" fa fa-search"></i>
																			</button>
																		</c:if>
																		<%-- </c:if> --%>
																		<%-- <c:if test="${userCategory == 'USER_CATEGORY_DGS'}">
																			<form:input type="hidden"
																			id="userId${stat.count}"
																			path="pscvrList[${stat.index}].customId.userDet.loginId" />
																			<input type="text" name="userName"
																			class="form-control" id="userName${stat.count}" />
																			<button type="submit" data-toggle="modal" id="userId"
																			data-target="#myModalUser"
																			onclick="showTableUser('UserDetail', 'userId${stat.count}', 'userName${stat.count}')">
																			<i class=" fa fa-search"></i>
																			</button>
																		</c:if> --%>
																	 	<span id="userId${stat.count}Error"
																		style="color: red; font-size: 15px;"></span>
																	</td>
																	<td><form:input type="hidden"
																			id="csoId${stat.count}"
																			path="pscvrList[${stat.index}].ccsoId" /> 
																			<input
																		type="text" name="csoName" class="form-control"
																		id="csoName${stat.count}" />
																		<button type="submit" data-toggle="modal" id="csoId"
																			data-target="#myModalCSO" 
																			onclick="showTableCSODPA('csoId${stat.count}', 'csoName${stat.count}', 'cso', loginId, 'CSODetail', '${stat.count}')">
																			<i class=" fa fa-search" ></i>
																		</button> <span id="csoId${stat.count}Error"
																		style="color: red; font-size: 15px;"></span>
																	</td>

																	<!-- <td> -->
																	<form:input type="hidden"
																			id="acsoId${stat.count}"
																			path="pscvrList[${stat.index}].acsoId" />  
																			<%-- <input type="text" name="acsoName" class="form-control"
																		id="acsoName${stat.count}" /> --%>
																		<%-- <button type="submit" data-toggle="modal" id="acsoId"
																			data-target="#myModalCSO"
																			onclick="showTableCSODPA('acsoId${stat.count}', 'acsoName${stat.count}', 'acso', loginId, 'CSODetail')">
																			<i class=" fa fa-search"></i>
																		</button> --%> 
																		<%-- <span id="acsoId${stat.count}Error"
																		style="color: red; font-size: 15px;"></span> --%> 
																		<!-- </td> -->

																	<td>
																		<form:input type="hidden"
																			id="dpaId${stat.count}"
																			path="pscvrList[${stat.index}].dpaId" /> 
																			<input type="text" class="form-control"
																		id="dpaName${stat.count}" name="dpaName" />
																		<button type="submit" data-toggle="modal" id="dpaId"
																			data-target="#myModalCSO"
																			onclick="showTableCSODPA('dpaId${stat.count}', 'dpaName${stat.count}', 'dpa', loginId, 'CSODetail', '${stat.count}')">
																			<i class=" fa fa-search"></i>
																		</button> 
																		<span id="dpaId${stat.count}Error" style="color: red; font-size: 15px;"></span>
																	</td>
																</tr>
															</c:forEach> </tboby>
													</table>
													<!-- <input type="submit" class="btn btn-success pull-right" value="Submit" form="shipForm"/> -->
												</div>

												<!-- /.tab-pane -->
												<div class="tab-pane table_wrapper" id="shipborneDet">
													
													<!-- Table added for shipbourne equipment details -->
													<!-- <div class="table_wrapper"> -->
													<table class="table no-margin" id="shipborneTable">
														<thead>
															<tr>
																<th>Shipborne Equipment Id</th>
																<th>Manufacturer Name</th>
																<th>Model Type</th>
																<th>Model Name</th>
																<th>DNID Number</th>
																<th>Member Number</th>
																<th>Ocean Region</th>
																<!-- <th>IsActive</th>
																<th>DNID Registration Status</th> -->
																<th>
																	<!-- <button type="button" class="btn btn-info btn-add">Add
																		Row</button> -->
																</th>
															</tr>
														</thead>
														<tbody id="shipborne_table_body">
															<tr class="newRow">
																<td><form:input type="text"
																		path="shipEq.shipborneEquipmentId"
																		id="shipborneEquipmentId" class="form-control" /> <span
																	id="shipborneEquipmentIdError"
																	style="color: red; font-size: 15px;"></span></td>
																<td><form:select id="manufacturerName"
																		path="shipEq.modelDet.manuDet.manufacturerId"
																		onchange="findModelType()" class="form-control">
																		<form:option value="" label="Select" />
																		<%-- <c:forEach var="manuname"
																			items="${List}">
																			<form:option value="${manuname}">${manuname}</form:option>
																		</c:forEach> --%>
																		<c:forEach var="manuname"
																			items="${manufacturerName}">
																			<form:option value="${manuname.getManufacturerId()}">${manuname.getManufacturerName()}</form:option>
																		</c:forEach>
																	</form:select> <span id="manufacturerNameError"
																	style="color: red; font-size: 15px;"></span></td>
																<td><form:select id="modelType"
																		path="shipEq.modelDet.modelType"
																		onchange="findModelName()" class="form-control">
																		<form:option value="">Select</form:option>
																		<%-- <form:option value="Type-1"></form:option>
																			<form:option value="Type-2"></form:option> --%>
																	</form:select> <span id="modelTypeError"
																	style="color: red; font-size: 15px;"></span></td>
																<td><form:select id="modelName"
																		path="shipEq.modelDet.modelId" class="form-control">
																		<form:option value="" label="Select" />
																	</form:select> <span id="modelNameError"
																	style="color: red; font-size: 15px;"></span></td>
																<td><form:input type="text" id="dnidNo"
																		class="form-control" path="shipEq.dnidNo" disabled="true" /> <span
																	id="dnidNoError" style="color: red; font-size: 15px;"></span>
																</td>
																<td><form:input type="text" id="memberNo"
																		class="form-control" path="shipEq.memberNo" disabled="true" /> <span
																	id="memberNoError" style="color: red; font-size: 15px;"></span>
																</td>
																<td><form:select id="oceanRegion"
																		path="shipEq.oceanRegion" class="form-control">
																		<form:option value="">Select</form:option>
																		<form:option value="0">0: Atlantic Ocean West</form:option>
																		<form:option value="1">1: Atlantic Ocean East</form:option>
																		<form:option value="2">2: Pacific Ocean</form:option>
																		<form:option value="3">3: Indian Ocean</form:option>
																		<form:option value="9">9: All Ocean Region</form:option>
																	</form:select> <span id="oceanRegionError"
																	style="color: red; font-size: 15px;"></span></td>
																<%-- <td><form:input type="text" id="seidStatus"
																			name="seidStatus" size="5" path="shipEq.seidStatus" /></td> --%>
																<%-- <td>
																	<!-- <div class="radio"> --> 
																	<form:radiobutton path="shipEq.seidStatus" value="Active"
																		id="seidStatus" disabled="true" /> 
																	<span id="seidStatusError" style="color: red; font-size: 15px;"></span>
																</td>
																<td><form:select id="dnidStatus"
																		path="shipEq.dnidStatus" class="form-control" disabled="true" >
																		<form:option value="">Select</form:option>
																		<form:option value="DNID Download"></form:option>
																		<form:option value="DNID Download Failed"></form:option>
																		<form:option value="Re-Download DNID"></form:option>
																		<form:option value="DNID Deleted"></form:option>
																		<form:option value="SEID Deleted"></form:option>
																		</form:select> <span id="dnidStatusError"
																	style="color: red; font-size: 15px;"></span></td> --%>
																<!-- <td>
																<button type="button"
																		class="btn btn-danger btn-delete">Remove</button></td> -->
															</tr>
														</tbody>
													</table>
													<!-- /.table end -->
												</div>
											</div>
											<!-- Custom form ?End-->
										</div>
										<!-- /.box -->
									</div>
									<div class="box-footer" id="addbtn">
										<button type="button" class="btn btn-info pull-right btn-next">Next</button>
									</div>
								</form:form>
								<!-- /.form -->
							</div>
						</div>

						<!-- Table to display for CSO -->
						<div class="modal fade" id="myModalCompany" role="dialog">
							<div class="modal-dialog" style="width: 95%">

								<div class="modal-content">
									<div class="modal-header">
										<h4>Ship Details</h4>
									</div>
									<div class="modal-body">
										<div class="box-body">
											<table id="CompanyDetail"
												class="table table-bordered table-striped "
												style="width: 100%">
												<thead>
													<tr>
														<th></th>
														<th data-data="companyCode" data-default-content="NA">Company
															Code</th>
														<th data-data="companyName" data-default-content="NA">Company
															Name</th>
													</tr>
												</thead>
											</table>
										</div>

									</div>
									<div class="modal-footer">
										<button id="selectCompany" type="button"
											class="btn btn-default" data-dismiss="modal">Select</button>
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>


						<div class="modal fade" id="myModalCSO" role="dialog">
							<div class="modal-dialog" style="width: 95%">

								<div class="modal-content">
									<div class="modal-header">
										<h4>CSO Details</h4>
									</div>
									<div class="modal-body">
										<div class="box-body">
											<table id="CSODetail"
												class="table table-bordered table-striped "
												style="width: 100%">
												<thead>
													<tr>
														<th></th>
														<th data-data="lritCsoIdSeq" data-default-content="NA">Lrit
															Cso Id</th>
														<th data-data="name" data-default-content="NA">CSO
															Name</th>
														<th data-data="telephoneOffice" data-default-content="NA">Telephone
															No</th>
														<th data-data="emailId" data-default-content="NA">Email
															Id</th>
														<th data-data="alternateCsoId" data-default-content="NA">ACSO Lrit Id</th>
													</tr>
												</thead>
											</table>
										</div>

									</div>
									<div class="modal-footer">
										<button id="selectCSO" type="button" class="btn btn-default"
											data-dismiss="modal">Select</button>
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="myModalUser" role="dialog">
							<div class="modal-dialog" style="width: 95%">

								<div class="modal-content">
									<div class="modal-header">
										<h4>User Details</h4>
									</div>
									<div class="modal-body">
										<div class="box-body">
											<table id="UserDetail"
												class="table table-bordered table-striped "
												style="width: 100%">
												<thead>
													<tr>
														<th></th>
														<th data-data="loginId" data-default-content="NA">User
															Id</th>
														<th data-data="name" data-default-content="NA">User
															Name</th>
														<th data-data="telephone" data-default-content="NA">Telephone
															No</th>
														<th data-data="emailid" data-default-content="NA">Email
															Id</th>
													</tr>
												</thead>
											</table>
										</div>

									</div>
									<div class="modal-footer">
										<button id="selectUser" type="button" class="btn btn-default"
											data-dismiss="modal">Select</button>
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>

						<!-- End Table to display for CSO -->

						<!--/.col (left) -->

					</div>
					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<div class="control-sidebar-bg"></div>

	<!-- ./wrapper -->

</body>
</html>