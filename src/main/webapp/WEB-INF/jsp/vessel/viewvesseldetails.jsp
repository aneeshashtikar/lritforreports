<!-- 
--Created finalform.jsp as the sample form finalized by team for Shipping Company.
--included a shipbourne table in tab_2 
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>View Vessel</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<%-- <spring:url value="/resources/bower_components/Ionicons/css/ionicons.min.css" var="ioniconsmincss" /> --%>
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- Theme style -->
<%-- <spring:url value="/resources/dist/css/AdminLTE.min.css" var="adminltemincss" /> --%>
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<%-- <spring:url value="/resources/dist/css/skins/_all-skins.min.css" var="allskinmincss" /> --%>
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<%-- <spring:url value="/resources/custom.css" var="customcss" /> 
<link rel="stylesheet" href="${customcss}"> --%>

<spring:url value="/login" var="Login" />

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style>
.customform {
	text-align: left;
}
</style>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Vessel Registration Form</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-lg-12">
						<!-- Horizontal Form -->
						<div class="box box-info">
							<!-- /.box-header -->
							<!-- form start -->
							<div class="container col-md-12">
								<!-- <div class="row"> -->
								<!-- <div class="col-md-10"> -->
								<form:form class="form-horizontal" id="editShipForm"
									action="approvevessel" name="editShipForm" method="post" >
									<div class="box-body">
										<!-- Custom form -->
										<div class="nav-tabs-custom" id="tabs">
											<ul class="nav nav-tabs">
												<li class="active" id="shipdetfirst"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#shipDet" data-toggle="tab">Ship Details</a></li>
												<li id="shipbornedet"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#shipborneDet" data-toggle="tab">Shipbourne Equipment Id
														Detail</a></li>
												<li id="compdetlast"
													onclick="tabChange(id, _prev, _next, _submit)"><a
													href="#compDet" data-toggle="tab">Company Details</a></li>
												<li class="pull-right"><a href="#" class="text-muted"><i
														class="fa fa-gear"></i></a></li>
											</ul>
											<div class="tab-content">
												<!-- First Tab -->
												<div class="tab-pane active" id="shipDet">
													<!-- FIRST ROW -->
													<input type="hidden" id="vesselId" path="vesselDet.vesselId" value="${vesselDet.vesselId }"/>
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="imoNo" class="col-sm-4 ">IMO Number</label>
															<div class="col-sm-8">
																<form:input type="text" id="imoNo" class="form-control"
																	name="imoNo" path="vesselDet.imoNo"
																	placeholder="IMO Number" required="" readonly="true"/>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="mmsiNo" class="col-sm-4">MMSI Number</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="vesselDet.mmsiNo" id="mmsiNo"
																	placeholder="MMSI Number" readonly="true"/>
															</div>
														</div>
													</div>
													<!-- SECOND ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="vesselName" class="col-sm-4 ">Ship
																Name</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="vesselDet.vesselName" id="vesselName"
																	placeholder="Ship Name" readonly="true"/>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="callSign" class="col-sm-4 ">Call Sign</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	id="callSign" path="vesselDet.callSign"
																	placeholder="call sign" readonly="true"/>
															</div>
														</div>
													</div>
													<!-- THIRD ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="yearOfBuilt" class="col-sm-4"> Year
																of Built</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="vesselDet.yearOfBuilt" id="yearOfBuilt"
																	placeholder="Year of Built" readonly="true"/>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="grossTonnage" class="col-sm-4">Gross
																Tonnage</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	id="grossTonnage" path="vesselDet.grossTonnage"
																	placeholder="Gross Tonnage" readonly="true"/>
															</div>
														</div>
													</div>
													<!-- FOURTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="deadWeight" class="col-sm-4 ">Dead
																Weight(DWT)</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="vesselDet.deadWeight" id="deadWeight"
																	placeholder="Dead Weight" readonly="true"/>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="registrationNo" class="col-sm-4 ">Registration
																No</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	id="registrationNo" path="vesselDet.registrationNo"
																	placeholder="" readonly="true"/>
															</div>
														</div>
													</div>
													<!-- FIFTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="vesselImage" class="col-sm-4">Vessel
																Image</label>
															<div class="col-sm-8">
																<form:input type="image" class="form-control"
																	path="vesselDet.vesselImage" id="vesselImage" readonly="true"/>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="vesselType" class="col-sm-4">Vessel
																Type</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="vesselDet.vesselType" id="vesselType" readonly="true"/>
																<%-- <form:select class="form-control"
																	path="vesselDet.vesselType" id="vesselType" onchange="findCategorySet()">
																	<form:option value="" label="Select"></form:option>
																	<c:forEach var="vesselType" items="${vesselTypeList }">
																		<form:option value="${vesselType }"></form:option>
																	</c:forEach>
																</form:select> --%>
															</div>
														</div>
													</div>
													<!-- SIXTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="vesselCategory" class="col-sm-4">Vessel
																Category</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="vesselDet.vesselCategory" id="vesselCategory" readonly="true"/>
																<%-- <form:select class="form-control"
																	path="vesselDet.vesselCategory" id="vesselCategory">
																	<form:option value="" label="Select" />
																	<c:forEach var="category" items="${categoryList}">
																		<form:option value="${category }"></form:option>
																	</c:forEach>
																</form:select> --%>
															</div>
														</div>
														<input type="hidden" id="groupId" path="vesselDet.portalVesselGroup.groupId" value="${vesselDet.portalVesselGroup.groupId }" />
														<div class="form-group col-sm-6">
															<label for="vesselGroup" class="col-sm-4">Vessel
																Group</label>
															<div class="col-sm-8">
																	<form:input type="text" class="form-control"
																	path="vesselDet.portalVesselGroup.groupName" id="vesselGroup" readonly="true"/>
																<%-- <form:select class="form-control"
																	path="vesselDet.portalVesselGroup.groupName"
																	id="vesselGroup">
																	<form:option value="" label="Select" />
																	<c:forEach var="groupname" items="${groupNameList}">
																		<form:option value="${groupname }"></form:option>
																	</c:forEach>
																</form:select> --%>
															</div>
														</div>
													</div>
													<!-- SEVENTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="piClub" class="col-sm-4 ">P & I Club</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="vesselDet.piClub" id="piClub"
																	placeholder="P & i cLUB" readonly="true"/>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="timeDelay" class="col-sm-4 ">Time
																Delay</label>
															<div class="col-sm-8">
																<form:input type="text" class="form-control"
																	path="vesselDet.timeDelay" id="timeDelay"
																	placeholder="in minutes" readonly="true"/>
															</div>
														</div>
													</div>
													<!-- EIGHTH ROW -->
													<div class="row">
													<div class="form-group col-sm-6">
														<label for="status" class="col-sm-4 ">Registration Status</label>
														<div class="col-sm-8">
																<form:select id="status" class="form-control" path="vesselDet.status" value="${vesselDet.status }">
																	<form:option value="Pending"></form:option>
																	<form:option value="Submitted"></form:option>
																	<form:option value="Approved"></form:option>
																	<form:option value="Reject"></form:option>
																	<form:option value="Inactive"></form:option>
																</form:select>
														</div>
													</div>
													</div>
												</div>
												<!-- First Tab End-->
												<!-- /.tab-pane -->
												<div class="tab-pane" id="shipborneDet" >
													<div class="box box-info">
														<div class="box-header with-border">
															<h3 class="box-title" align="center">Shipborne
																Equipment Id Detail</h3>
														</div>
														<!-- /.box-header -->
														<div class="box-body">
															<!-- Table added for shipbourne equipment details -->
															<table class="table no-margin" id="shipborneTable">
																<thead>
																	<tr>
																		<th>Shipborne Equipment Id</th>
																		<th>Model Type</th>
																		<th>Manufact Name</th>
																		<th>Model Name</th>
																		<th>DNID No</th>
																		<th>Member No</th>
																		<th>Ocean Region</th>
																		<th>IsActive</th>
																		<th>Reg Status</th>
																		<th>
																			<button type="button" class="btn btn-info"
																				id="addBtn">Add Row</button>
																		</th>
																	</tr>
																</thead>
																<tbody id="shipborne_table_body">
																	<tr id="new_shipborne_row">
																		<td>
																			<form:input type="text"
																				path="shipborneEquipmentId"
																				id="shipborneEquipmentId" size="4" readonly="true"/>
																		</td>
																		<td><%-- <form:select id="modelType"
																				path="modelDet.modelType" onchange="findModelType()">
																				<form:option value="Select"></form:option>
																				<form:option value="Type-1"></form:option>
																				<form:option value="Type-2"></form:option>
																			</form:select> --%>
																			<input type="hidden" id="modelId" path="modelDet.modelId" value="${modelDet.modelId }" />
																			<form:input type="text" path="modelDet.modelType" id="modelType" size="4" readonly="true"/>
																		</td>
																		<td>
																			<%-- <form:select id="manufacturerName" path="modelDet.manufacturerName" onchange="findManuName()">
																				<form:option value="" label="Select" />
																			</form:select> --%>
																			<form:input type="text" path="modelDet.manufacturerName" id="manufacturerName" size="6" readonly="true"/>
																		</td>
																		<td>
																			<%-- <form:select id="modelName" path="modelDet.modelName">
																				<form:option value="" label="Select" />
																			</form:select> --%>
																			<form:input type="text" path="modelDet.modelName" id="modelName" size="7" readonly="true"/>
																		</td>
																		<td>
																			<form:input type="text" id="dnidNo" size="3" path="dnidNo" />
																		</td>
																		<td>
																			<form:input type="text" id="memberNo" size="6" path="memberNo" />
																		</td>
																		<td>
																			<%-- <form:select id="oceanRegion"
																				path="oceanRegion">
																				<form:option value="Region-1"></form:option>
																				<form:option value="Region-2"></form:option>
																				<form:option value="Region-3"></form:option>
																			</form:select> --%>
																			<form:input type="text" id="oceanRegion" size="5" path="oceanRegion" readonly="true"/>
																		</td>
																		<td>
																			<form:input type="text" id="seidStatus" size="3" name="seidStatus" path="seidStatus" /></td>
																		<td>
																			<form:select id="regStatus" path="dnidStatus">
																				<form:option value="DNID Download"></form:option>
																				<form:option value="DNID Download Failed"></form:option>
																				<form:option value="Re-Download DNID"></form:option>
																				<form:option value="DNID Deleted"></form:option>
																			</form:select>
																		</td>
																		<td><button type="button"
																				class="btn btn-danger btn-delete">Remove</button></td>
																	</tr>

																</tbody>
															</table>
															<!-- /.table end -->
														</div>
														<!-- /.box-body -->
														<div class="box-footer clearfix"></div>
														<!-- /.box-footer -->
													</div>
												</div>
												<!-- /.tab-pane -->
												<div class="tab-pane" id="compDet">
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="ownercompanyname" class="col-sm-4">Owner
																Company Name</label>
															<div class="col-sm-8">
																<%-- <form:select id="companyNameOwner" 
																path="vesselDet.ownerLoginId" class="form-control" >
																	<form:option value="" label="Select" />
																	<c:forEach var="company" items="${companyList }">
																		<form:option value="${company[2] }">${company[0] }</form:option>
																	</c:forEach>
																</form:select> --%>
																<form:input type="text" id="ownerLoginId" name="ownerLoginId" path="vesselDet.ownerLoginId" readonly="true"/>															</div>
														</div>
														</div>
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="techmanagername" class="col-sm-4">Technical
																Manager Name</label>
															 <div class="col-sm-7">
																<%-- <form:select id="companyNameTech"
																path="vesselDet.technicalManagerLoginId" class="form-control" >
																	<form:option value="" label="Select" />
																	<c:forEach var="company" items="${companyList }">
																		<form:option value="${company[2] }">${company[0] }</form:option>
																	</c:forEach>
																</form:select> --%>
																<form:input type="text" id="companyNameTech" path="vesselDet.technicalManagerLoginId" readonly="true"/>
															</div> 
															</div>
														</div>
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="commercialmanagername" class="col-sm-4 ">Commercial
																Manager Name</label>
															<div class="col-sm-7">
																<%-- <form:select id="companyNameComm"
																	path="vesselDet.commercialManagerLoginId" class="form-control" >
																	<form:option value="" label="Select" />
																	<c:forEach var="company" items="${companyList }">
																		<form:option value="${company[2] }">${company[0] }</form:option>
																	</c:forEach>
																</form:select> --%>
																<form:input type="text" id="companyNameComm" path="vesselDet.commercialManagerLoginId" readonly="true"/>
																</div>
															</div>
														</div>
													</div>
												<!-- /.tab-pane -->
										</div>
											<!-- Custom form ?End-->
										</div>
										<!-- /.box -->
										<div class="box-footer" id="addbtn">
											<button type="button"
												class="btn btn-info pull-right btn-next">Next</button>
										</div>
									</div>
								</form:form>
							</div>
							<!-- /.form -->
						</div>
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>

		<!-- Control Sidebar -->
		<jsp:include page="../rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<spring:url value="/bower_components/jquery/dist/jquery.min.js"
		var="jqueryminjs" />
	<script src="${jqueryminjs}"></script>
	<!-- Bootstrap 3.3.7 -->
	<spring:url
		value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
		var="bootstrapminjs" />
	<script src="${bootstrapminjs}"></script>
	<!-- FastClick -->
	<spring:url value="/bower_components/fastclick/lib/fastclick.js"
		var="fastclickjs" />
	<script src="${fastclickjs}"></script>
	<!-- AdminLTE App -->

	<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
	<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
	<script src="${adminlteminjs}"></script>
	<!-- AdminLTE for demo purposes -->
	<spring:url value="/dist/js/demo.js" var="demojs" />
	<%-- <spring:url value="/resources/dist/js/demo.js" var="demojs" /> --%>
	<script src="${demojs}"></script>
	<!-- Custom Script -->
	<script src="js/approvedpage.js"></script>
</body>
</html>