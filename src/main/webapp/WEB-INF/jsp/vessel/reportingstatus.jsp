<!-- 
--Created finalform.jsp as the sample form finalized by team for Shipping Company.
--included a shipbourne table in tab_2 
-->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Reporting Status</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<%-- <spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url> --%>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

<style>
.table_wrapper {
	display: block;
	overflow-x: auto;
	white-space: nowrap;
}
</style>

<spring:url value="/resources/js/vessel_button.js"
	var="buttonjs" />
<script src="${buttonjs}"></script>
<spring:url value="/resources/js/validateform.js"
	var="validateformjs" />
<script src="${validateformjs}"></script>
<spring:url value="/resources/js/imovessel.js" var="imovesseljs" />
<script src="${imovesseljs}"></script>

<spring:url value="/ship/updatereportingstatus" var="updatereportingstatus" />
<spring:url value="/ship/imagefile" var="imagefile" />

<!-- <style>
/* .side-box {
	margin-bottom: 0px;
	/* height: 200px;
} */
</style> -->

<style>
.table_wrapper {
	display: block;
	overflow-x: auto;
	white-space: nowrap;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
	$('#changestatus').hide();
	$('#sellship').hide();
	$('#scrapship').hide();
	$('#changestatustoactive').hide();
	$('#changestatustoinactive').hide();
});

function getAction()
{
	var _action = $('#action').val();
	
	if(_action == 'Change Vessel Reporting Status')
	{
		$('#changestatus').show();
		
		$('#changestatus').change(function(){
			
			_status = $('#status').val();
			
			/* if(_status == 'ACTIVE')
			{
				$('#changestatustoactive').show();
				$('#sellship').hide();
				$('#scrapship').hide();
				$('#changestatustoinactive').hide();
			} */
			if(_status == 'INACTIVE')
			{
				$('#changestatustoinactive').show();
				$('#sellship').hide();
				$('#scrapship').hide();
				/* $('#changestatustoactive').hide(); */
			}
			else
			{
				/* $('#changestatustoactive').hide(); */
				$('#changestatustoinactive').hide();
				$('#sellship').hide();
				$('#scrapship').hide();
			}
			
		});
		
	}
	else if(_action == 'Sell Ship')
	{
		$('#changestatus').hide();
		$('#sellship').show();
	 	$('#scrapship').hide();
	 	$('#changestatustoinactive').hide();
	 	/* $('#changestatustoactive').hide(); */
	 }
	else if(_action == 'Scrap Ship')
	{
		$('#changestatus').hide();
		$('#sellship').hide();
	 	$('#scrapship').show();
	 	$('#changestatustoinactive').hide();
	 	/* $('#changestatustoactive').hide(); */
	}
	else if(_action == 'Repurchase')
	{
		$('#changestatus').hide();
		$('#sellship').hide();
		$('#scrapship').hide();
		$('#changestatustoinactive').hide();
		/* $('#changestatustoactive').hide(); */
	}
}

function validateActionForm()
{
	var arr = [];
	
	var _action = $('#action').val();
	
	arr[0] = validateAction();
	
	if(_action == 'Change Vessel Reporting Status')
	{
		_status = $('#status').val();
		
		if(_status == 'INACTIVE')
		{
			arr[1] = validateStatus();
			arr[2] = validateReason();
			arr[3] = validateReportingDocumentForInactive();
			
			arr[4] = validateRemarksForInactive();
		}
		/* else if(_status == 'ACTIVE')
		{
			arr[1] = validateStatus();
			arr[2] = validateReportingDocumentForActive();
			
			arr[3] = validateRemarksForActive();
		} */
		else
		{
			arr[1] = validateStatus();
		}		
	}
	else if(_action == 'Sell Ship')
	{
		arr[1] = validateReportingDocumentForSell(); 
		
		/* arr[1] = validateRemarksForSell(); */
	}
	else if(_action == 'Scrap Ship')
	{
		arr[1] = validateReportingDocumentForScrap(); 
		
		/* arr[1] = validateRemarksForScrap(); */
	}
		
	for(var i = 0 ; i < arr.length ; i++)
	{
		/*alert(arr[i]);*/
		if(!arr[i]){	
			return false;
		}
	}
	return true;
}

</script>

</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">

						<!-- Approve New Vessel Code -->

						<div class="tab-pane">
							<!--.box -->
							<div id="showError">
								<c:if test="${error != null }">
									<div class='alert alert-danger colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert'
											aria-label='close'>&times;</a> ${error }
									</div>
								</c:if>
								<c:if test="${success != null }">
									<div class='alert alert-success colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert'
											aria-label='close'>&times;</a> ${success }
									</div>
								</c:if>
							</div>
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title" align="center">Change Vessel
										Reporting Status</h3>
								</div>
								<form:form class="form-horizontal" id="actionForm"
									action="${updatereportingstatus }" name="actionForm" method="post"
									modelAttribute="vesselDet" enctype="multipart/form-data" onsubmit="return validateActionForm()">
								<div class="box-body">
									<div class="tab-content">
										<!-- First Tab -->
										<div class="tab-pane active" id="shipDet">
											<!-- FIRST ROW -->
											<form:input type="hidden" id="vesselId"
												path="vesselId" />
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="imoNo" class="col-sm-4 ">IMO Number</label>
													<div class="col-sm-8">
														<%-- <div class="form-control">${vesselDet.imoNo }</div> --%>
														<input type="text" id="imoNo" class="form-control"
															name="imoNo" value="${vesselDet.imoNo }"
															readonly />
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="mmsiNo" class="col-sm-4">MMSI Number</label>
													<div class="col-sm-8">
														<%-- <div class="form-control">${vesselDet.mmsiNo }</div> --%>
														<input type="text" class="form-control"
															value="${vesselDet.mmsiNo }" id="mmsiNo"
															readonly />
													</div>
												</div>
											</div>
											<!-- SECOND ROW -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="vesselName" class="col-sm-4 ">Ship Name</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															value="${vesselDet.vesselName }" id="vesselName"
															readonly />
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="callSign" class="col-sm-4 ">Call Sign</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" id="callSign"
															value="${vesselDet.callSign }"
															readonly />
													</div>
												</div>
											</div>
											<!-- THIRD ROW -->
											<%-- <div class="row">
														<div class="form-group col-sm-6">
															<label for="yearOfBuilt" class="col-sm-4"> Year
																of Built</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${vesselDet.yearOfBuilt }" id="yearOfBuilt"
																	placeholder="Year of Built" readonly />
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="grossTonnage" class="col-sm-4">Gross
																Tonnage</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	id="grossTonnage" value="${vesselDet.grossTonnage }"
																	placeholder="Gross Tonnage" readonly />
															</div>
														</div>
													</div> --%>
											<!-- FOURTH ROW -->
											<div class="row">
												<%-- <div class="form-group col-sm-6">
															<label for="deadWeight" class="col-sm-4 ">Dead
																Weight(DWT)</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${vesselDet.deadWeight }" id="deadWeight"
																	placeholder="Dead Weight" readonly />
															</div>
														</div> --%>
												<div class="form-group col-sm-6">
													<label for="registrationNo" class="col-sm-4 ">Registration
														No</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															id="registrationNo" value="${vesselDet.registrationNo }"
															readonly />
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="imoVesselType" class="col-sm-4">IMO
														Vessel Type</label>
													<div class="col-sm-8">
														<%-- <c:if test="${vesselDet.imoVesselType == 0200 }">Cargo</c:if>
													<c:if test="${vesselDet.imoVesselType == 0100 }">Passenger</c:if> --%>
														<input type="text" class="form-control" value="${vesselDet.imoVesselType }"
															id="imoVesselType" readonly />
													</div>
												</div>
											</div>
											<!-- FIFTH ROW -->
											<!-- <div class="row"> -->
												<%-- <div class="form-group col-sm-6">
															<label for="timeDelay" class="col-sm-4 ">Time
																Delay</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${vesselDet.timeDelay }" id="timeDelay"
																	placeholder="in minutes" readonly />
															</div>
														</div> --%>
												<%-- <div class="form-group col-sm-6">
															<label for="imoVesselType" class="col-sm-4">IMO
																Vessel Type</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${vesselDet.imoVesselType }"
																	id="imoVesselType" readonly />
															</div>
														</div> --%>
											<!-- </div> -->
											<!-- SIXTH ROW -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="vesselType" class="col-sm-4">Vessel
														Type</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															value="${vesselDet.vesselType }" id="vesselType" readonly />
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="vesselCategory" class="col-sm-4">Vessel
														Category</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															value="${vesselDet.vesselCategory }" id="vesselCategory"
															readonly />
													</div>
												</div>
											</div>
											<!-- SEVENTH ROW -->
											<div class="row">
												<input type="hidden" id="groupId"
													value="${vesselDet.portalVesselGroup.groupId }" />
												<div class="form-group col-sm-6">
													<label for="vesselGroup" class="col-sm-4">Vessel
														Group</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															value="${vesselDet.portalVesselGroup.groupName }"
															id="vesselGroup" readonly />
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="status" class="col-sm-4 ">Registration
														Status</label>
													<div class="col-sm-8">
														<input type="text" class="form-control"
															value="${vesselDet.regStatus }" id="regStatus" readonly />
													</div>
												</div>
												<%-- <div class="form-group col-sm-6">
															<label for="piClub" class="col-sm-4 ">P & I Club</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${vesselDet.piClub }"
																	placeholder="P & i cLUB" readonly />
															</div>
														</div> --%>
											</div>
											<!-- EIGHTH ROW -->
											<!-- <div class="row"> -->
												<%-- <div class="form-group col-sm-6">
															<label for="statusVessel" class="col-sm-4">Status</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${vesselDet.status }" id="statusVessel"
																	readonly />
															</div>
														</div> --%>
												<%-- <div class="form-group col-sm-6">
															<label for="status" class="col-sm-4 ">Registration
																Status</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${vesselDet.regStatus }" id="regStatus"
																	readonly />
															</div>
														</div> --%>
											<!-- </div> -->
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="acion" class="col-sm-4 ">Vessel
														Action</label>
													<div class="col-sm-8">
														<select id="action" class="form-control" name="action" onchange="getAction()">
															<c:if test="${vesselDet.regStatus != 'SOLD'}">
															<option value="">Select</option>
															<option value="Change Vessel Reporting Status">Change Vessel Reporting Status</option>
															<option value="Sell Ship">Sell Ship</option>
															<option value="Scrap Ship">Scrap Ship</option>
															</c:if>
															<%-- <c:if test="${vesselDet.regStatus == 'SOLD'}">
																<option value="Repurchase">Repurchase</option>
															</c:if> --%>
														</select>
														<span id="actionError" style="color: red; font-size: 15px;"></span>
													</div>
												</div>
											</div>
											
											<div id="changestatus">
												<div class="row">
												<div class="form-group col-sm-6">
													<label for="status" class="col-sm-4 ">Vessel
														Reporting Status</label>
													<div class="col-sm-8">
														<form:select id="status" class="form-control"
															path="status" >
															<form:option value="">Select</form:option>
															<form:option value="ACTIVE"></form:option>
															<form:option value="INACTIVE"></form:option>
														</form:select>
														<span id="statusError" style="color: red; font-size: 15px;"></span>
													</div>
												</div>
												</div>
											</div>
											
											<div id="changestatustoinactive">
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="reason" class="col-sm-4 ">Reason</label>
													<div class="col-sm-8">
														<form:select id="reason" class="form-control"
															path="reportingStatusChangeReason">
															<form:option value="">Select</form:option>
															<form:option value="Hot Lay up"></form:option>
															<form:option value="Cold Lay up"></form:option>
															<form:option value="Change to RSV"></form:option>
															<form:option value="GMDSS Excemption"></form:option>
															<form:option value="Abandoned"></form:option>
															<form:option value="Other"></form:option>
														</form:select>
														<span id="reasonError" style="color: red; font-size: 15px;"></span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="remarks" class="col-sm-4 ">Remarks</label>
													<div class="col-sm-8">
														<input type="text" id="remarksforinactive" class="form-control"
															name="reportingStatusChangeRemarks" />
														<span id="remarksforinactiveError" style="color: red; font-size: 15px;"></span>
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="document" class="col-sm-4 ">Supporting
														Documents </label>
													<div class="col-sm-8">
														<input type="file" id="documentforinactive" class="form-control" 
															name="portalDocs.document1" />
														<span id="documentforinactiveError" style="color: red; font-size: 15px;"></span>
													</div>
													<!-- (Lay up / Laid up form to be filled up, signed and stamped by CSO, Change of vessel certification issued by DGS, 
														Exemption certificate issued by DGS ) -->
												</div>
											</div>
											</div>
											
											<!-- <div id="changestatustoactive">
											<div class="row">
												<div class="form-group col-sm-6">
													<label for="remarks" class="col-sm-4 ">Remarks</label>
													<div class="col-sm-8">
														<input type="text" id="remarksforactive" class="form-control"
															name="reportingStatusChangeRemarks" />
														<span id="remarksforactiveError" style="color: red; font-size: 15px;"></span>
													</div>
												</div>
												<div class="form-group col-sm-6">
													<label for="document" class="col-sm-4 ">Supporting
														Documents </label>
													<div class="col-sm-8">
														<input type="file" id="documentforactive" class="form-control" 
															name="portalDocs.document1" />
														<span id="documentforactiveError" style="color: red; font-size: 15px;"></span>
													</div>
													(Lay up / Laid up form to be filled up, signed and stamped by CSO, Change of vessel certification issued by DGS, 
														Exemption certificate issued by DGS )
												</div>
											</div>
											</div> -->
											
											<div id="sellship">
											<div class="row">
												<!-- <div class="form-group col-sm-6">
													<label for="remarks" class="col-sm-4 ">Remarks</label>
													<div class="col-sm-8">
														<input type="text" id="remarksforsell" class="form-control"
															name="reportingStatusChangeRemarks" />
														<span id="remarksforsellError" style="color: red; font-size: 15px;"></span>
													</div>
												</div> -->
												<div class="form-group col-sm-6">
													<label for="document" class="col-sm-4 ">Supporting
														Documents </label>
													<div class="col-sm-8">
														<input type="file" id="documentforsell" class="form-control" 
															name="portalDocs.document2" />
														<span id="documentforsellError" style="color: red; font-size: 15px;"></span>
													</div>
													<!-- (Lay up / Laid up form to be filled up, signed and stamped by CSO, Change of vessel certification issued by DGS, 
														Exemption certificate issued by DGS ) -->
												</div>
											</div>
											</div>
											
											<div id="scrapship">
											<div class="row">
												<!-- <div class="form-group col-sm-6">
													<label for="remarks" class="col-sm-4 ">Remarks</label>
													<div class="col-sm-8">
														<input type="text" id="remarksforscrap" class="form-control"
															name="reportingStatusChangeRemarks" />
														<span id="remarksforscrapError" style="color: red; font-size: 15px;"></span>
													</div>
												</div> -->
												<div class="form-group col-sm-6">
													<label for="document" class="col-sm-4 ">Supporting
														Documents </label>
													<div class="col-sm-8">
														<input type="file" id="documentforscrap" class="form-control" 
															name="portalDocs.document3" />
														<span id="documentforscrapError" style="color: red; font-size: 15px;"></span>
													</div>
													<!-- (Lay up / Laid up form to be filled up, signed and stamped by CSO, Change of vessel certification issued by DGS, 
														Exemption certificate issued by DGS ) -->
												</div>
											</div>
											</div>
										</div>
										<!-- First Tab End-->
										<!-- /.tab-pane -->

									</div>
									</div>
									
									<!-- Custom form ?End-->
									<!-- </div> -->
									<!-- /.box -->
									<div class="box-footer" id="addbtn">
										<button type="submit" class="btn btn-primary pull-right btn-next">Submit</button>
										<!-- <a href="/lrit/ship/updatereportingstatus" class="btn btn-info pull-right btn-next">Submit</a> -->
									</div>
									</form:form>
								</div>
								<%-- </form:form> --%>
							</div>
							<!-- /.form -->
							<!--/.col (left) -->
						</div>
						<!-- right column -->
						<!--/.col (right) -->
						<div class="col-md-2">
							<jsp:include page="../common.jsp"></jsp:include>
						</div>
					</div>
					<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
</html>