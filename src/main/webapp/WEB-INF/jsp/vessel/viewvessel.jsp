<!-- 
--Created finalform.jsp as the sample form finalized by team for Shipping Company.
--included a shipbourne table in tab_2 
-->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>View Vessel Detail</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<%-- <spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url> --%>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">

<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

<spring:url value="/resources/js/viewvessel.js" var="viewvesseljs" />
<script src="${viewvesseljs}"></script>
<spring:url value="/resources/js/imovessel.js" var="imovesseljs" />
<script src="${imovesseljs}"></script>

<spring:url value="/ship/imagefile" var="imagefile" />

<style>
.table_wrapper {
	display: block;
	overflow-x: auto;
	white-space: nowrap;
}
</style>


</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">
						
						<!-- Approve New Vessel Code -->
						
						<div class="tab-pane">
							<!--.box -->
							<div id="showError">
							<c:if test="${error != null }">
									<div class='alert alert-danger colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
										${error }
									</div>
								</c:if>
								<c:if test="${success != null }">
									<div class='alert alert-info colose' data-dismiss='alert'>
										<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
										${success }
									</div>
								</c:if>
							</div>
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title" align="center">Vessel Details</h3>
								</div>
										<div class="nav-tabs-custom" id="tabs">
											<ul class="nav nav-tabs">
												<li class="active" id="shipdetfirst"
													onclick="tabChange(id, _prev, _next)"><a
													href="#shipDet" data-toggle="tab">Ship Details</a></li>
												<li id="shipbornedet"
													onclick="tabChange(id, _prev, _next)"><a
													href="#shipborneDet" data-toggle="tab">Shipbourne
														Equipment Id Detail</a></li>
												<li id="compdetlast"
													onclick="tabChange(id, _prev, _next)"><a
													href="#compDet" data-toggle="tab">Company Details</a></li>
												<li class="pull-right"><a href="#" class="text-muted"><i
														class="fa fa-gear"></i></a></li>
											</ul>
											<div class="tab-content">
												<!-- First Tab -->
												<div class="tab-pane active" id="shipDet">
													<!-- FIRST ROW -->
													<input type="hidden" id="vesselId"
														path="shipEq.vesselDet.vesselId"
														value="${avrdto.shipEq.vesselDet.vesselId }" />
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="imoNo" class="col-sm-4 ">IMO Number</label>
															<div class="col-sm-8">
															<%--  <div class="form-control">${avrdto.shipEq.vesselDet.imoNo }</div> --%>
																<input type="text" id="imoNo" class="form-control"
																	name="imoNo" value="${avrdto.shipEq.vesselDet.imoNo }"
																	required="" readonly />
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="mmsiNo" class="col-sm-4">MMSI Number</label>
															<div class="col-sm-8">
															 <%-- <div class="form-control">${avrdto.shipEq.vesselDet.mmsiNo }</div> --%>
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.mmsiNo }" id="mmsiNo"
																	readonly />
															</div>
														</div>
													</div>
													<!-- SECOND ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="vesselName" class="col-sm-4 ">Ship
																Name</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.vesselName }" id="vesselName"
																	readonly />
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="callSign" class="col-sm-4 ">Call Sign</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	id="callSign" value="${avrdto.shipEq.vesselDet.callSign }"
																	readonly />
															</div>
														</div>
													</div>
													<!-- THIRD ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="yearOfBuilt" class="col-sm-4"> Year
																of Built</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.yearOfBuilt }" id="yearOfBuilt"
																	readonly />
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="grossTonnage" class="col-sm-4">Gross
																Tonnage</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	id="grossTonnage" value="${avrdto.shipEq.vesselDet.grossTonnage }"
																	readonly />
															</div>
														</div>
													</div>
													<!-- FOURTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="deadWeight" class="col-sm-4 ">Dead
																Weight(DWT)</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.deadWeight }" id="deadWeight"
																	readonly />
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="registrationNo" class="col-sm-4 ">Registration
																No</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	id="registrationNo"
																	value="${avrdto.shipEq.vesselDet.registrationNo }"
																	readonly />
															</div>
														</div>
													</div>
													<!-- FIFTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="timeDelay" class="col-sm-4 ">Time
																Delay</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.timeDelay }" id="timeDelay"
																	readonly />
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="imoVesselType" class="col-sm-4">IMO
																Vessel Type</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.imoVesselType }"
																	id="imoVesselType" readonly />
															</div>
														</div>
													</div>
													<!-- SIXTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="vesselType" class="col-sm-4">Vessel
																Type</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.vesselType }" id="vesselType"
																	readonly />
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="vesselCategory" class="col-sm-4">Vessel
																Category</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.vesselCategory }"
																	id="vesselCategory" readonly />
															</div>
														</div>
													</div>
													<!-- SEVENTH ROW -->
													<div class="row">
														<input type="hidden" id="groupId"
															value="${avrdto.shipEq.vesselDet.portalVesselGroup.groupId }" />
														<div class="form-group col-sm-6">
															<label for="vesselGroup" class="col-sm-4">Vessel
																Group</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.portalVesselGroup.groupName }"
																	id="vesselGroup" readonly />
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="piClub" class="col-sm-4 ">P & I Club</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" 
																	value="${avrdto.shipEq.vesselDet.piClub }"
																	readonly />
															</div>
														</div>
													</div>
													<!-- EIGHTH ROW -->
													<div class="row">
														<div class="form-group col-sm-6">
															<label for="vesselImage" class="col-sm-4">Vessel
																Image</label>
															<div class="col-sm-8">
																<input type="hidden" class="form-control"
																	name="shipEq.vesselDet.vesselImage" id="vesselImage"
																	readonly />
																<a href="${imagefile}?vesselId=${avrdto.shipEq.vesselDet.vesselId }" target="_blank">Vessel Image</a>
															</div>
														</div>
														<div class="form-group col-sm-6">
															<label for="status" class="col-sm-4 ">Registration
																Status</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	value="${avrdto.shipEq.vesselDet.regStatus }" id="regStatus"
																	readonly />
															</div>
														</div>
													</div>
												</div>
												<!-- First Tab End-->
												<!-- /.tab-pane -->
												
												<div class="tab-pane table_wrapper" id="shipborneDet">
															<!-- Table added for shipbourne equipment details -->
															<!-- <div class="table_wrapper"> -->
															<table class="table no-margin" id="shipborneTable">
																<thead>
																	<tr>
																		<th>Shipborne Equipment Id</th>
																		<th>Manufacturer Name</th>
																		<th>Model Type</th>
																		<th>Model Name</th>
																		<th>DNID Number</th>
																		<th>Member Number</th>
																		<th>Ocean Region</th>
																		<!-- <th>IsActive</th>
																		<th>DNID Registration Status</th> -->
																		<th>
																			<!-- <button type="button" class="btn btn-info"
																				id="addBtn">Add Row</button> -->
																		</th>
																	</tr>
																</thead>
																<tbody id="shipborne_table_body">
																	<tr id="new_shipborne_row">
																		<td><input type="text" class="form-control"
																				value="${avrdto.shipEq.shipborneEquipmentId }"
																				id="shipborneEquipmentId" readonly />
																		</td>
																		<td>
																			<input type="hidden" id="manufacturerId"
																			value="${avrdto.shipEq.modelDet.manuDet.manufacturerId }" />
																			<input type="text"
																				value="${avrdto.shipEq.modelDet.manuDet.manufacturerName }"
																				id="manufacturerName" class="form-control" readonly />
																		</td>
																		<td>
																			<input type="hidden" id="modelId"
																			value="${avrdto.shipEq.modelDet.modelId }" /> 
																			<input type="text" value="${avrdto.shipEq.modelDet.modelType }"
																				id="modelType" readonly class="form-control" />
																		</td>
																		<td>
																			 <input type="text"
																				value="${avrdto.shipEq.modelDet.modelName }" id="modelName"
																				readonly class="form-control"/>
																		</td>
																		<td><input type="text" id="dnidNo"
																				value="${avrdto.shipEq.dnidNo }" class="form-control" readonly/></td>
																		<td><input type="text" id="memberNo"
																				 value="${avrdto.shipEq.memberNo }" class="form-control" readonly/></td>
																		<td> <input type="text"
																				id="oceanRegion" value="${avrdto.shipEq.oceanRegion }"
																				readonly class="form-control"/>
																		</td>
																		<%-- <td><input type="text" id="seidStatus" class="form-control" value="${avrdto.shipEq.seidStatus }" readonly /></td>
																		<td>
																			<input type="text" id="dnidStatus"
																				 value="${avrdto.shipEq.dnidStatus }" class="form-control" readonly/>
																			</td> --%>
																		<!-- <td><button type="button"
																				class="btn btn-danger btn-delete">Remove</button></td> -->
																	</tr>
																</tbody>
															</table>
															<!-- /.table end -->
												</div>
												<!-- /.tab-pane -->
												<div class="tab-pane" id="compDet">		
													<!-- approved mapping table -->
													
													<table class="table no-margin" id="csoDet">
														<!-- here should go some titles... -->
														<thead>
															<tr>
																<th></th>
																<th>Company Name</th>
																<th>User Name</th>
																<th>Current CSO Name</th>
																<th>Alternate CSO Name</th>
																<th>DPA Name</th>
															</tr>
														<thead>
															<tboby>	
																<c:forEach items="${avrdto.nameslist}" var="name"
																	varStatus="stat">
																	<tr>
																	<td><c:if test="${stat.count == 1}">
																			<label>Owner Company Details</label>
																		</c:if> <c:if test="${stat.count == 2}">
																			<label>Technical Company Details</label>
																		</c:if> <c:if test="${stat.count == 3}">
																			<label>Commercial Company Details</label>
																		</c:if></td>
																	<td style="color : #6d6464">${name.companyName }
																			</td>
																	<td style="color : #6d6464">${name.userName }
																			</td>
																	<td style="color : #6d6464">${name.csoName }
																			</td>
																	<td style="color : #6d6464">${name.acsoName }
																			</td>
																	<td style="color : #6d6464">${name.dpaName }
																			</td>
																	</tr>
																</c:forEach>
															
														</tboby>
													</table>
													
												</div>
											</div>
											<!-- Custom form ?End-->
										</div>
										<!-- /.box -->
										<div class="box-footer" id="addbtn">
											<button type="button"
												class="btn btn-info pull-right btn-next">Next</button>
										</div>
									</div>
							</div>
							<!-- /.form -->
						<!--/.col (left) -->
					</div>
					<!-- right column -->
					<!--/.col (right) -->
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="../footer.jsp"></jsp:include>
		
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

</body>
</html>