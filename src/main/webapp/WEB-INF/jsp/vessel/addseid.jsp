<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Ship Equipment Detail</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<spring:url var="showPositionReport" value="/report/showPositionReport"></spring:url>
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<spring:url value="/resources/js/vessel_button.js" var="buttonjs" />
<script src="${buttonjs}"></script>
<spring:url value="/resources/js/validateform.js" var="validateformjs" />
<script src="${validateformjs}"></script>

<spring:url value="/ship/persistshipequipment"
	var="persistshipequipment" />


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<script type="text/javascript">
	function validateAddSEIDForm() {
		var _userCategory = '${requestScope.userCategory}';
		//alert(_userCategory);

		var arr = [];

		arr[0] = validateShipEquipId();
		arr[1] = validateOceanRegion();
		arr[2] = validateManufacturerName();
		arr[3] = validateModelType();
		arr[4] = validateModelName();
		arr[5] = validateSEIDDocument();
		//arr[7] = validateSEIDStatusSEID();

		if (_userCategory == 'USER_CATEGORY_DGS') {
			arr[6] = validateDNIDNoSEID();
			arr[7] = validateMemberNoSEID();
		}

		for (var i = 0; i < arr.length; i++) {
			//alert(arr[i]);
			if (!arr[i]) {
				return false;
			}
		}

		return true;
	}
</script>

</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">

		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
						<div id="showError">
							<c:if test="${successMsg != null}">
								<div class='alert alert-success'>
									<a href='#' class='close' data-dismiss='alert'
										aria-label='close'>&times;</a> ${successMsg}
								</div>
							</c:if>
							<c:if test="${errorMsg != null}">
								<div class='alert alert-danger colose' data-dismiss='alert'>
									<a href='#' class='close' data-dismiss='alert'
										aria-label='close'>&times;</a> ${errorMsg}
								</div>
							</c:if>
						</div>

						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Ship Borne Equipment Detail</h3>
							</div>
							<!-- /.box-header -->
							<!-- form start -->
							<form:form id="frmAddShipEquipDet" method="POST"
								action="${persistshipequipment}"
								modelAttribute="newShipEquipDet"
								onsubmit="return validateAddSEIDForm()"
								enctype="multipart/form-data">
								<div class="box-body">
									<table class="table table-bordered table-striped">
										<tr>

											<form:input id="vesselId" class="form-control"
												path="vesselDet.vesselId" type="hidden" />

										</tr>
										<tr>
											<th>IMO Number</th>
											<td><form:input id="imoNo" type="text"
													class="form-control" path="imoNo"
													value="${newShipEquipDet.imoNo }" readonly="true" /></td>
										</tr>
										<tr>
											<th>Shipborne Equipment ID</th>
											<td><form:input type="text" class="form-control"
													path="shipborneEquipmentId" id="shipborneEquipmentId" /> <span
												id="shipborneEquipmentIdError"
												style="color: red; font-size: 15px;"></span></td>
										</tr>
										<c:if
											test="${requestScope.userCategory == 'USER_CATEGORY_SC'}">
											<tr>
												<th>DNID</th>
												<td><form:input id="dnidNo" type="text"
														class="form-control" path="dnidNo" disabled="true" /> <span
													id="dnidNoError" style="color: red; font-size: 15px;"></span>
												</td>
											</tr>
											<tr>
												<th>Member Number</th>
												<td><form:input id="memberNo" type="text"
														class="form-control" path="memberNo" disabled="true" /> <span
													id="memberNoError" style="color: red; font-size: 15px;"></span>
												</td>
											</tr>
										</c:if>
										<c:if
											test="${requestScope.userCategory == 'USER_CATEGORY_DGS'}">
											<tr>
												<th>DNID</th>
												<td><form:input id="dnidNo" type="text"
														class="form-control" path="dnidNo" /> <span
													id="dnidNoError" style="color: red; font-size: 15px;"></span>
												</td>
											</tr>
											<tr>
												<th>Member Number</th>
												<td><form:input id="memberNo" type="text"
														class="form-control" path="memberNo" /> <span
													id="memberNoError" style="color: red; font-size: 15px;"></span>
												</td>
											</tr>
										</c:if>
										<tr>
											<th>Ocean Region</th>
											<td><form:select id="oceanRegion" path="oceanRegion"
													class="form-control">
													<form:option value="">Select</form:option>
													<form:option value="0">0: Atlantic Ocean West</form:option>
													<form:option value="1">1: Atlantic Ocean East</form:option>
													<form:option value="2">2: Pacific Ocean</form:option>
													<form:option value="3">3: Indian Ocean</form:option>
													<form:option value="9">9: All Ocean Region</form:option>

													<%-- <form:option value="">Select</form:option>
													<form:option value="All Oceanregion"></form:option>
													<form:option value="Atlantic OceanregionEast"></form:option>
													<form:option value="Atlantic OceanregionWest"></form:option>
													<form:option value="Indian Oceanregion"></form:option>
													<form:option value="Pacific Oceanregion"></form:option> --%>
												</form:select> 
												<span id="oceanRegionError"
												style="color: red; font-size: 15px;"></span></td>
										</tr>
										<tr>
											<th>Manufacturer Name</th>
											<td><form:select id="manufacturerName"
													path="modelDet.manuDet.manufacturerId"
													onchange="findModelType()" class="form-control">
													<form:option value="" label="Select" />
													<c:forEach var="manuname" items="${manufacturerName}">
														<form:option value="${manuname.getManufacturerId()}">${manuname.getManufacturerName()}</form:option>
													</c:forEach>
												</form:select> <span id="manufacturerNameError"
												style="color: red; font-size: 15px;"></span></td>
										</tr>
										<tr>
											<th>Model Type</th>
											<td><form:select id="modelType"
													path="modelDet.modelType" onchange="findModelName()"
													class="form-control">
													<form:option value="">Select</form:option>
												</form:select> <span id="modelTypeError"
												style="color: red; font-size: 15px;"></span></td>
										</tr>
										<tr>
											<th>Model Name</th>
											<td><form:select id="modelName" path="modelDet.modelId"
													class="form-control">
													<form:option value="" label="Select" />
												</form:select> <span id="modelNameError"
												style="color: red; font-size: 15px;"></span></td>
										</tr>
										<tr>
											<th>Supporting Document</th>
											<td><input type="file" class="form-control file-input"
												name="supportingDocs1" id="supportingDocs" /> <span
												id="supportingDocsError"
												style="color: red; font-size: 15px;"></span></td>
										</tr>
										<%-- <tr>
							<th>IsActive</th>
							<td>
								<form:radiobutton path="seidStatus" value="Active" id="seidStatus" />
								<div id="seidhidden">
									<form:radiobutton path="seidStatus" value="InActive" checked="true" /> 
								</div>
								<span id="seidStatusError" style="color: red; font-size: 15px;"></span>
							</td>
							</tr> --%>
									</table>
								</div>
								<div class="box-footer">
									<input type="submit" class="btn btn-primary" value="Submit" />
								</div>
							</form:form>
						</div>
					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>
				</div>

				<!-- </div> -->
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>

	<!-- <div class="control-sidebar-bg"></div> -->
	<!-- </div> -->
	<!-- ./wrapper -->
</body>
</html>
