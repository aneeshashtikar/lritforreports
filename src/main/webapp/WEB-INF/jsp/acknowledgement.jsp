<!-- 
--Created finalform.jsp as the sample form finalized by team for Shipping Company.
--included a shipbourne table in tab_2 
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Acknowledgement</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawesomemincss" />
<link rel="stylesheet" href="${fontawesomemincss}">
<!-- Ionicons -->
<%-- <spring:url value="/resources/bower_components/Ionicons/css/ionicons.min.css" var="ioniconsmincss" /> --%>
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconsmincss" />
<link rel="stylesheet" href="${ioniconsmincss}">

<!-- Theme style -->
<%-- <spring:url value="/resources/dist/css/AdminLTE.min.css" var="adminltemincss" /> --%>
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<%-- <spring:url value="/resources/dist/css/skins/_all-skins.min.css" var="allskinmincss" /> --%>
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">

<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<spring:url value="../resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">


<spring:url value="/users/forgot" var="forgotpassword" />
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style>
.customform {
	text-align: left;
}
</style>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="sidebar.jsp"></jsp:include>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<!-- left column -->
					<div class="col-md-10">

						<!-- ADD YOUR MESSAGE CONTENTS HERE  -->
						<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger'>
								<a href='#' >&times;</a> ${errorMsg}
							</div>
						</c:if>
						<!-- /#ADD YOUR MESSAGE CONTENTS HERE  -->
					</div>

					<div class="col-md-2">
						<jsp:include page="common.jsp"></jsp:include>
					</div>
					<!--/.col (right) -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="footer.jsp"></jsp:include>

		<!-- Control Sidebar -->
		<jsp:include page="rightbar.jsp"></jsp:include>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<spring:url value="/bower_components/jquery/dist/jquery.min.js"
		var="jqueryminjs" />
	<script src="${jqueryminjs}"></script>
	<!-- Bootstrap 3.3.7 -->
	<spring:url
		value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
		var="bootstrapminjs" />
	<script src="${bootstrapminjs}"></script>
	<!-- FastClick -->
	<spring:url value="/bower_components/fastclick/lib/fastclick.js"
		var="fastclickjs" />
	<script src="${fastclickjs}"></script>
	<!-- AdminLTE App -->

	<%-- <spring:url value="/resources/dist/js/adminlte.min.js" var="adminlteminjs" /> --%>
	<spring:url value="/dist/js/adminlte.min.js" var="adminlteminjs" />
	<script src="${adminlteminjs}"></script>
	<!-- AdminLTE for demo purposes -->
	<spring:url value="/dist/js/demo.js" var="demojs" />
	<%-- <spring:url value="/resources/dist/js/demo.js" var="demojs" /> --%>
	<script src="${demojs}"></script>


</body>
</html>