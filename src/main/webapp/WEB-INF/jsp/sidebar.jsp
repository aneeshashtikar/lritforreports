<!-- Sidebar.jsp Start -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<spring:url value="/requests/gaforideapproval" var="gaForIdeApproval" />
<spring:url value="/requests/gadisapprovefromide"
	var="gadisapprovefromide" />
<spring:url value="/requests/galistsendtoddp" var="galistsendtoddp" />
<spring:url value="/requests/gaapprovedfromddp" var="gaapprovedfromddp" />
<spring:url value="/report/getStandingOrderReportPage" var="getStandingOrderReportPage" />
<spring:url value="/vessel/getVesselDetailPage" var="getVesselDetailPage" />



<spring:url value="/contractinggovernment/registercontractinggovernment"
	var="registercontractinggovernment" />
<spring:url value="/contractinggovernment/viewcontractinggovernment"
	var="viewcontractinggovernment" />


<spring:url value="/requests/gasendtoddp" var="gasendtoddp" />

<spring:url value="/roles/createrole" var="createrole" />
<spring:url value="/roles/viewrole" var="viewrole" />

<spring:url value="/requests/ddprequest" var="ddprequest" />
<spring:url value="/requests/flagrequest" var="flagrequest" />
<spring:url value="/requests/portrequest" var="portrequest" />
<spring:url value="/requests/surpicrequest" var="surpicrequest" />
<spring:url value="/requests/coastalsurpicrequest"
	var="coastalsurpicrequest" />
<spring:url value="/standingOrder" var="standingOrder" />
<spring:url value="/requests/sarrequest" var="sarrequest" />
<spring:url value="/requests/coastalrequest" var="coastalrequest" />
<spring:url value="/users/createuser" var="createuser" />
<spring:url value="/users/viewuser" var="viewuser" />
<spring:url value="/users/csoacsomapping" var="csoacsomapping" />
<spring:url value="/ship/registervessel" var="registervessel" />
<spring:url value="/vessels/submittedvessel" var="submittedvessel" />
<spring:url value="/vessels/allvessel" var="allvessel" />
<spring:url value="/company/allcompanylist" var="allcompanylist" />
<spring:url value="/group/createvesselgroup" var="createvesselgroup" />
<spring:url value="/manufacturer/viewallmanufacturer"
	var="viewallmanufacturer" />
<spring:url value="/ship/vesselaction" var="vesselaction" />


<spring:url value="/report/getSarSurpicReportPage" var="getSarSurpicReport" />
<spring:url value="/report/getShipPositionRequestReportPage" var="getShipPositionRequestReportPage" />
<spring:url value="/report/getShipPositionReportPage" var="getShipPositionReportPage" />	
<spring:url value="/report/getASPShipPositionReportPage" var="getASPShipPositionReportPage" />	
<spring:url value="/report/getGAUpdateToIdePage" var="getGAUpdateToIdePage" />	
<spring:url value="/report/getGAUpdateToDdpPage" var="getGAUpdateToDdpPage" />	
<spring:url value="/report/getOpenSoPage" var="getOpenSoPage" />
<spring:url value="/report/getCspLogReportPage" var="getCspLogReportPage" />
<spring:url value="/report/getDdpLogReportPage" var="getDdpLogReportPage" />
<spring:url value="/report/getAspLogReportPage" var="getAspLogReportPage" />	
<spring:url value="/report/getFlagVesselsAtSeaReportPage" var="getFlagVesselsAtSeaReportPage" />

	
<spring:url value="/users/csoacsomapping" var="csoacsomapping" />
<spring:url value="/report/getSarSurpicReportPage"
	var="getSarSurpicReport" />
<spring:url value="/group/viewvesselgroup" var="viewgroup" />

<!-- BILLING URLS -->
<spring:url value="/aspcontract/aspcontract" var="addaspcontract" />

<spring:url value="/cspcontract/cspcontract" var="addcspcontract" />

<spring:url value="/usacontract/usacontract" var="addusacontract" />

<spring:url value="/shippingcompanycontract/shippingcompanycontract"
	var="addshippingcompanycontract" />


<spring:url value="/contract/viewcontract" var="viewcontract" />

<spring:url value="/invoice/addInvoice" var="addInvoice" />

<spring:url value="/invoice/listInvoice" var="listInvoice" />

<spring:url value="/bill/addBill" var="addBill" />

<spring:url value="/billCSP/addCSPBill" var="addCSPBill" />

<spring:url value="/bill/listBill" var="listBill" />

<spring:url value="/billable/addBillableCategory"
	var="addBillableCategory" />

<spring:url value="/billable/updateBillableCategory"
	var="updateBillableCategory" />

<spring:url value="/billable/viewBillableCategory"
	var="viewBillableCategory" />

<spring:url value="/ide/uploadJournalExtract" var="uploadJournalExtract" />

<spring:url value="/ide/viewJournalExtract" var="viewJournalExtract" />
<spring:url value="/route/createroute" var="createroute" />
<spring:url value="/route/viewroutes" var="viewroutes" />

<spring:url value="/vessels/repurchasevessellist" var="repurchasevessellist" />
<spring:url value="/report/getDataAnalysisReportPage" var="getDataAnalysisReportPage" />

<!-- BILLING URLS ENDS -->

<aside id="sidebar-img" class="main-sidebar main-sidebar-custom">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">

			<!-- CONTRACTING GOVERNMENT MENU STARTS -->
			<sec:authorize
				access="hasAnyAuthority('registercontractinggovernment', 'deregistercontractinggovernment','viewcontractinggovernment','editcontractinggovernment')">
				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Contracting Government </span>
						<span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>


					<ul class="treeview-menu">
						<sec:authorize
							access="hasAuthority('registercontractinggovernment')">
							<li><a href="${registercontractinggovernment}"
								><i class="fa fa-circle-o"></i>Register
									Contracting <br /> Government</a></li>
						</sec:authorize>
						<sec:authorize
							access="hasAnyAuthority( 'deregistercontractinggovernment','viewcontractinggovernment','editcontractinggovernment')">
							<li><a href="${viewcontractinggovernment}" ><i
									class="fa fa-circle-o"></i>View Contracting <br /> Government</a></li>
						</sec:authorize>
					</ul></li>
			</sec:authorize>
			<!-- CONTRACTING GOVERNMENT MENU ENDS -->


			<!-- USER MENU STARTS -->
			<sec:authorize
				access="hasAnyAuthority('createuser', 'edituser','viewuser','deleteuser')">

				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Manage User</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>

					<ul class="treeview-menu">
						<sec:authorize access="hasAuthority('createuser')">
							<li><a href="${createuser} " ><i
									class="fa fa-circle-o"></i>Create User</a></li>
						</sec:authorize>
						<sec:authorize
							access="hasAnyAuthority( 'edituser','viewuser','deleteuser')">
							<li><a href="${viewuser}" ><i
									class="fa fa-circle-o"></i>View User</a></li>
						</sec:authorize>
					</ul></li>
			</sec:authorize>
			<!-- USER MENU ENDS -->
			<!-- ROLE MENU STARTS -->
			<sec:authorize
				access="hasAnyAuthority('createrole', 'viewrole','editrole','deleterole')">

				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Manage Role</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>

					<ul class="treeview-menu">
						<sec:authorize access="hasAuthority('createrole')">
							<li><a href="${createrole} " ><i
									class="fa fa-circle-o"></i>Create Role</a></li>
						</sec:authorize>
						<sec:authorize
							access="hasAnyAuthority('viewrole','editrole','deleterole')">
							<li><a href="${viewrole}" ><i
									class="fa fa-circle-o"></i>View Role</a></li>
						</sec:authorize>
					</ul></li>
			</sec:authorize>
			<!-- ROLE MENU ENDS -->

			<!-- VESSEL MENU STARTS -->

			<sec:authorize
				access="hasAnyAuthority('viewvessel', 'viewvesselcompanywise', 'approvevesselregistration')">
				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Vessel</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<c:if test="${sessionScope.USER.category == 'USER_CATEGORY_SC'}">
						<%-- <sec:authorize access="hasAnyAuthority('registervessel')"> --%>
							<li><a href="${registervessel }" ><i
									class="fa fa-circle-o"></i>Registration</a></li>
						</c:if>
					<sec:authorize access="hasAuthority('approvevesselregistration')">
					<c:if test="${sessionScope.USER.category != 'USER_CATEGORY_SC'}">			
					<li><a href="${submittedvessel }" ><i
							class="fa fa-circle-o"></i>Approve Registration</a></li>
					</c:if>	
					</sec:authorize>	
					<sec:authorize access="hasAuthority('viewvessel')">
						<li><a href="${allvessel }" ><i
								class="fa fa-circle-o"></i>Edit/View Vessel</a></li>
					</sec:authorize>
					<c:if test="${sessionScope.USER.category != 'USER_CATEGORY_SC'}">
					<sec:authorize access="hasAuthority('viewvesselcompanywise')">
						<li><a href="${allcompanylist }" ><i
								class="fa fa-circle-o"></i>View Vessel Company-wise</a></li>
					</sec:authorize>
					</c:if>	
					</ul></li>
			</sec:authorize>

			<!-- <li><a href="/index2.html"><i class="fa fa-circle-o"></i>Vessel
							Master</a></li> -->
			<!-- <li><a href="/index2.html"><i class="fa fa-circle-o"></i>Company
							Details</a></li> -->
			<!-- VESSEL MENU ENDS -->

			<!-- VESSEL GROUP MENU STARTS -->

			<sec:authorize
				access="hasAnyAuthority('viewvesselgroup', 'editvesselgroup')">
				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Vessel Group</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize access="hasAuthority( 'editvesselgroup')">
							<li><a href="${createvesselgroup }"><i
									class="fa fa-circle-o"></i>Create Vessel Group</a></li>

						</sec:authorize>

						<li><a href="${viewgroup }"><i class="fa fa-circle-o"></i>View

								Vessel Group</a></li>

					</ul></li>
			</sec:authorize>
			<!-- VESSEL GROUP MENU ENDS -->

			<!-- Manufacturer MENU STARTS -->
			<sec:authorize
				access="hasAnyAuthority('editmanufacturerandmodel','viewmanufacturerandmodel')">
				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Manufacturer & Model</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<li><a href="${viewallmanufacturer }" ><i
								class="fa fa-circle-o"></i>Manage Manufacturer & Model</a></li>
					</ul></li>
			</sec:authorize>
			<!-- Manufacturer MENU ENDS -->

			<sec:authorize
				access="hasAnyAuthority('vesselaction','shipborneaction','addshipborneequipment')">
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>Vessel Actions</span> <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<sec:authorize access="hasAuthority('vesselaction')">
					<li><a href="${vesselaction }?request=action" ><i
							class="fa fa-circle-o"></i> Vessel Action</a></li>
					</sec:authorize>
					<sec:authorize access="hasAuthority('shipborneaction')">
					<li><a href="${getVesselDetailPage}" ><i
							class="fa fa-circle-o"></i>ShipBorne Equipment Action</a></li>
					</sec:authorize>
					<sec:authorize access="hasAuthority('addshipborneequipment')">
					<li><a href="${vesselaction }?request=addseid" ><i
							class="fa fa-circle-o"></i> Add ShipBorne Equipment</a></li>
					</sec:authorize>
					<c:if test="${sessionScope.USER.category == 'USER_CATEGORY_SC'}">
					<sec:authorize access="hasAuthority('vesselaction')">
					<li><a href="${repurchasevessellist }" ><i
							class="fa fa-circle-o"></i>Repurchase Vessel</a></li>
					</sec:authorize>
					</c:if>
				</ul></li>
				</sec:authorize>

			<!-- GEOGRAPHICAL REQUEST MENU STARTS -->
			<sec:authorize
				access="hasAnyAuthority('viewGeographicalArea','processingGeographicalArea','deleteGeographicalArea')">
				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Geographical Area</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<li><a href="${gaForIdeApproval}" ><i
								class="fa fa-circle-o"></i>Pending for IDE</a></li>
						<li><a href="${galistsendtoddp}" ><i
								class="fa fa-circle-o"></i>Pending for DDP</a></li>
						<li><a href="${gadisapprovefromide}" ><i
								class="fa fa-circle-o"></i>Disapproved from IDE</a></li>
						<li><a href="${gaapprovedfromddp}" ><i
								class="fa fa-circle-o"></i>Approved from DDP</a></li>
					</ul></li>
			</sec:authorize>
			<!-- GEOGRAPHICAL REQUEST MENU ENDS -->


			<!-- REQUEST MENU STARTS -->
			<sec:authorize
				access="hasAnyAuthority('ddprequest','flagrequest','portrequest','sarsurpicrequest','surpicrequest','coastalsurpicrequest','standingOrder')">
				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Requests</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize access="hasAuthority('flagrequest')">
							<li><a href="${flagrequest}" ><i
									class="fa fa-circle-o"></i>Flag Request</a></li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('portrequest')">
							<li><a href="${portrequest}" ><i
									class="fa fa-circle-o"></i>Port Request</a></li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('ddprequest')">
							<li><a href="${ddprequest}" ><i
									class="fa fa-circle-o"></i>DDP Request</a></li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('sarsurpicrequest')">
							<li><a href="${surpicrequest}"  target="_blank"><i
									class="fa fa-circle-o"></i>SURPIC Request</a></li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('sarpollrequest')">
							<li><a href="${sarrequest}" ><i
									class="fa fa-circle-o"></i>SAR Poll Requests</a></li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('coastalrequest')">
							<li><a href="${coastalrequest}" ><i
									class="fa fa-circle-o"></i>Coastal Requests</a></li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('coastalsurpicrequest')">
							<li><a href="${coastalsurpicrequest}"  target="_blank"><i
									class="fa fa-circle-o"></i>Coastal Surpic Request</a></li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('standingOrder')">
							<li><a href="${standingOrder}" ><i
									class="fa fa-circle-o"></i>Standing Order</a></li>
						</sec:authorize>

					</ul></li>
			</sec:authorize>

			<!-- REQUEST MENU ENDS -->

			<!-- REPORTS MENU STARTS -->
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>Reports</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<%-- <li><a href="#"><i class="fa fa-circle-o"></i>Ship Speed
							Position Report</a></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-circle-o"></i> Standing Order Reports <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="${getStandingOrderReportPage}"><i
									class="fa fa-circle-o"></i>Open Standing Order</a></li>
							<li><a href="${getStandingOrderReportPage}"><i
									class="fa fa-circle-o"></i>Close Standing Order</a></li>
							<li><a href="${getStandingOrderReportPage}"><i
									class="fa fa-circle-o"></i>All Standing Order</a></li>
						</ul></li> --%>

					<%-- <li><a href="${getSarSurpicReport}" ><i
							class="fa fa-circle-o"></i>SAR Surpic Report</a></li> --%>
					<li><a href="${getASPShipPositionReportPage }" ><i
							class="fa fa-circle-o"></i>ASP Ship Position Report</a></li>
					<li><a href="${getCspLogReportPage }" ><i
							class="fa fa-circle-o"></i>CSP Log Report</a></li>
					<li><a href="${getDdpLogReportPage }" ><i
							class="fa fa-circle-o"></i>DDP Log Report</a></li>
					<li><a href="${getAspLogReportPage }" ><i
							class="fa fa-circle-o"></i>ASP Log Report</a></li>	
					<li><a href="${getFlagVesselsAtSeaReportPage }" ><i
							class="fa fa-circle-o"></i>Flag Vessels at Sea Report</a></li>					
							
					<li><a href="${getDataAnalysisReportPage }" ><i
							class="fa fa-circle-o"></i>Data Analysis Report</a></li>
				</ul></li>
			<!-- REPORTS MENU ENDS -->



			<!-- ROLE MANAGEMENT MENU STARTS -->
			<%-- <li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>Role Management </span> <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${createrole}"><i class="fa fa-circle-o"></i>Create
							Role</a></li>
					<li><a href="${viewrole}"><i class="fa fa-circle-o"></i>View
							Role</a></li>

				</ul></li> --%>
			<!-- ROLE MANAGEMENT MENU ENDS -->
		</ul>
		
		<ul class="sidebar-menu" data-widget="tree">
		<sec:authorize access="hasAuthority('lritaccount')">
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>Contract</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>


				<ul class="treeview-menu">

					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Add Contract</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>

						<ul class="treeview-menu">
							<li><a href="${addaspcontract}"><i
									class="fa fa-circle-o"></i>ASP/DC Contract </a></li>

							<li><a href="${addcspcontract}"><i
									class="fa fa-circle-o"></i>CSP Contract</a></li>
							<li><a href="${addusacontract}"><i
									class="fa fa-circle-o"></i>USA Contract</a></li>

							<li><a href="${addshippingcompanycontract}"><i
									class="fa fa-circle-o"></i>Shipping Company <br>Contract</a></li>

						</ul></li>
					<li><a href="${viewcontract}"><i class="fa fa-circle-o"></i>View
							Contract</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>Invoice</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${addInvoice}"><i class="fa fa-circle-o"></i>Add
							Invoice</a></li>
					<li><a href="${listInvoice}"><i class="fa fa-circle-o"></i>List
							Invoice</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>Bill</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Add Bill</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="${addBill}"><i class="fa fa-circle-o"></i>ASP/DC
									Bill</a></li>
							<li><a href="${addCSPBill}"><i class="fa fa-circle-o"></i>CSP
									Bill</a></li>
						</ul></li>
					<li><a href="${listBill}"><i class="fa fa-circle-o"></i>List
							Bill</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>BillableItem</span> <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${addBillableCategory}"><i
							class="fa fa-circle-o"></i>Add Billable Item Category</a></li>
					<li><a href="${updateBillableCategory}"><i
							class="fa fa-circle-o"></i>Update Billable Item Category</a></li>
					<li><a href="${viewBillableCategory}"><i
							class="fa fa-circle-o"></i>View Billable Item Category</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>IDE</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					
					<li><a href="${uploadJournalExtract}"><i
							class="fa fa-circle-o"></i>Upload Journal Extract</a></li>
					
					<li><a href="${viewJournalExtract}"><i
							class="fa fa-circle-o"></i>View Journal Extract</a></li>
				</ul></li>
			</sec:authorize>
			<sec:authorize
				access="hasAnyAuthority('createroute', 'viewroute','editroute')">

				<li class="treeview"><a href="#"> <i
						class="fa fa-dashboard"></i> <span>Vessel Route</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize access="hasAuthority('createroute')">
							<li><a href="${createroute}"><i class="fa fa-circle-o"></i>Create
									Route</a></li>
						</sec:authorize>
						<sec:authorize access="hasAnyAuthority('viewroute','editroute')">
							<li><a href="${viewroutes}"><i class="fa fa-circle-o"></i>Route
									List</a></li>
						</sec:authorize>
					</ul></li>
			</sec:authorize>
		</ul>
	</section>
</aside>
<!-- SideBar.jsp End -->