<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Requests Pending Tasks List</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<spring:url value="/dist/css/skins/_all-skins.min.css"
	var="allskinmincss" />
<link rel="stylesheet" href="${allskinmincss}">
<!-- Date Picker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
	var="bootstrapdatepickermincss" />
<link rel="stylesheet" href="${bootstrapdatepickermincss}">
<!-- Daterange picker -->
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.css"
	var="daterangepickercss" />
<link rel="stylesheet" href="${daterangepickercss}">
<!-- bootstrap wysihtml5 - text editor -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	var="wysihtml5mincss" />
<link rel="stylesheet" href="${wysihtml5mincss}">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- Custom theme css -->
<spring:url value="/resources/css/lrit-dashboard.css"
	var="lritcustomcss" />
<link rel="stylesheet" href="${lritcustomcss}">
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<!-- DataTables -->
<spring:url
	value="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
	var="dtbootstrapmincss" />
<link rel="stylesheet" href="${dtbootstrapmincss}">
<spring:url
	value="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"
	var="datatablemincss" />
<link rel="stylesheet" href="${datatablemincss}">
<spring:url
	value="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
	var="buttondatatablemincss" />
<link rel="stylesheet" href="${buttondatatablemincss}">
<spring:url
	value="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"
	var="dataTableCss" />

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">


<link rel="icon" href="../resources/img/dgslogo.ico">

<!-- jss -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery/dist/jquery.min.js"
	var="jqueryminjs" />
<script src="${jqueryminjs}"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<!-- jQuery 3 -->
<spring:url value="/bower_components/jquery-ui/jquery-ui.min.js"
	var="jqueryuiminjs" />
<script src="${jqueryuiminjs}"></script>
<spring:url
	value="/bower_components/datatables.net/js/jquery.dataTables.min.js"
	var="datatablesminjs" />
<script src="${datatablesminjs}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<spring:url value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
	var="bootstrapminjs" />
<script src="${bootstrapminjs}"></script>
<spring:url
	value="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
	var="dtbootstrapminjs" />
<script src="${dtbootstrapminjs}"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- daterangepicker -->
<spring:url value="/bower_components/moment/min/moment.min.js"
	var="momentminjs" />
<script src="${momentminjs}"></script>
<spring:url
	value="/bower_components/bootstrap-daterangepicker/daterangepicker.js"
	var="daterangepickerjs" />
<script src="${daterangepickerjs}"></script>
<!-- datepicker -->
<spring:url
	value="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
	var="bootstrapdaterangepickerminjs" />
<script src="${bootstrapdaterangepickerminjs}"></script>
<!-- Bootstrap WYSIHTML5 -->
<spring:url
	value="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
	var="wysihtml5allminjs" />
<script src="${wysihtml5allminjs}"></script>
<!-- Slimscroll -->
<spring:url
	value="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"
	var="jqueryslimscrollminjs" />
<script src="${jqueryslimscrollminjs}"></script>
<!-- jQuery Knob Chart -->
<spring:url
	value="/bower_components/jquery-knob/dist/jquery.knob.min.js"
	var="jqueryknobminjs" />
<script src="${jqueryknobminjs}"></script>
<!-- FastClick -->
<spring:url value="/bower_components/fastclick/lib/fastclick.js"
	var="fastclickjs" />
<script src="${fastclickjs}"></script>
<!-- AdminLTE App -->
<spring:url value="/dist/js/adminlte.min.js" var="adminlteminsjs" />
<script src="${adminlteminsjs}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<%-- <spring:url value="/dist/js/pages/dashboard.js" var="dashboardjs" />
<script src="${dashboardjs}"></script>
 --%>
<!-- AdminLTE for demo purposes -->
<spring:url value="/dist/js/demo.js" var="demojs" />
<%-- <spring:url value="/requests/currentddpversion" var="currentddpversion" />
 --%>
<script src="${demojs}"></script>
<script
	src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
<!-- <script type="text/javascript"
	src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>

<spring:url value="/pendingtask/pendingtaskrequestdata" var="requestpendingtask" />
<spring:url value="/pendingtask/pendingTaskDecision" var="pendingTaskDecision" />
<spring:url value="/pendingtask/showDocument" var="showDocument" />


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body id="img" class="hold-transition skin-blue sidebar-mini">
	<div id="wrapper-img" class="wrapper">
		<jsp:include page="../header.jsp"></jsp:include>
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="../sidebar.jsp"></jsp:include>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<!-- Small boxe (Stat box) -->
				<div class="row">
					<div class="col-md-10">
					<c:if test="${successMsg != null}">
							<div class='alert alert-success'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${successMsg}
							</div>
						</c:if>
						<c:if test="${errorMsg != null}">
							<div class='alert alert-danger colose' data-dismiss='alert'>
								<a href='#' class='close' data-dismiss='alert'
									aria-label='close'>&times;</a> ${errorMsg}
							</div>
						</c:if>
					 <div id="message">
								</div> 
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">List of All Pending Tasks</h3>
								
							</div>
							<!-- context dropdown -->
							<div class="box-body">
								<div class="form-group">
								 <label>Select Contracting Govt</label>
									<select id="cglist" class="form-control" >  
									<option>Select</option>
															<c:forEach items="${cgList}" var="cg">
        									<option value="${cg.lritId}">${cg.countryName}</option>
    										</c:forEach>

													</select>
								</div>
							</div>

							<!-- Report Table -->
							<div class="box-body">
								<table id="pendingtask-request"
									class="table table-bordered table-striped "
									style="display: none; width: 100%"
									data-access-type="disapprove from ide">
									<thead>
										<tr>
								 <!-- <th data-data="null" data-default-content=""></th> --> 
											<!-- <th data-data="pendingtaskId" data-default-content="NA">PendingTask Id</th> -->
											<th data-data="referenceId" data-default-content="NA" >Reference Id</th>
											<!-- <th data-data="category" data-default-content="NA" >Category</th> -->
											<th data-data="requestDate" data-default-content="NA">Request Date</th>
											<th data-data="message" data-default-content="NA">Message</th>
											<th data-data="null" data-orderable="false" data-default-content="<textarea id='comment' name='comment' rows='3' cols='40' ></textarea>">Comment</th>
											<%-- <th data-data="null" data-default-content="<textarea name='comment' rows='3' cols='40' ></textarea>">Comment</th> --%>
											<th data-data="null" data-default-content="<button name='approve' class='btn btn-success'>Approve</button>"">Approve</th>
											<th data-data="null" data-default-content="<button name='reject' class='btn btn-danger'>Reject</button>">Reject</th>
										
										</tr>
									</thead>
								</table>
							</div>
						</div>

					</div>
					<div class="col-md-2">
						<jsp:include page="../common.jsp"></jsp:include>
					</div>

				</div>
			</section>
		</div>

		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="../footer.jsp"></jsp:include>
	<!-- <div class="control-sidebar-bg"></div> -->
	<!-- </div> -->
	<!-- ./wrapper -->
</body>
<script type="text/javascript">
	
	var resultSet = function() {
		this.item = {};
	}
	var requesttable = new resultSet();

	$(document).ready(function() {

	/*function to load pending task table on select from option*/		
	$("select.form-control").change(function() {
		$('#pendingtask-request').css("display", "");
		$('#pendingtask-request').DataTable().clear().destroy();
		var countryId = $(this).children("option:selected").val();
		//alert("You have selected the country - " + countryId);
		pendingTaskTableById(countryId);
	});

	function pendingTaskTableById(countryId) {
		//alert("in called function" + countryId);
		requesttable.item = $('#pendingtask-request')
				.DataTable(
						{
							"ajax" : {
								"url" : "${requestpendingtask}",
								"data" : {
									'countryId' : countryId
								},
								"dataSrc" : ""
							},
							"responsive" : {
								"details" : {
									"type" : 'column',
									"target" : 'tr'
								}
							},
							"columnDefs" : [{

								"targets" : 0,
								"render" : function(data, type, row, meta) {
									if (row.category == "Vessel_Sold") {
										return '<a href="${showDocument}?pendingTaskId='
												+ row.pendingtaskId
												+ '" target="_blank">'
												+ row.referenceId + '</a>';
									} else
										return data;
								}
							}],
							"order" : [ [ 1, 'asc' ] ]
						});//end datatable function
	}//end pendingTaskTableById function

	/*Function call on accept or reject button click*/
	function makeDecision(id, category, comment, decision, referenceId) {
		$
				.ajax({
					url : "${pendingTaskDecision}",
					type : "POST",
					data : {
						category : category,
						pendingtaskId : id,
						comment : comment,
						decision : decision,
						referenceId : referenceId
					},
					success : function(data) {
						$("#message")
								.append(
										"<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'"
															 +" aria-label='close'>&times;</a>"
												+ data + "</div>")
						requesttable.item.ajax.reload();
						pendingTaskCounter();
					},
					error : function(data) {
						$("#message")
								.append(
										"<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'"
													 +" aria-label='close'>&times;</a>"
												+ data + "</div>")
						requesttable.item.ajax.reload();
					}
				});

	}//end make decision


	
		<%--START : FOR APPROVE BUTTON --%>
		$('body').on('click','#pendingtask-request tbody td>button[name=approve]',function() {
				var id = requesttable.item.row($(this).parents('tr')).data().pendingtaskId;
				var referenceId = requesttable.item.row($(this).parents('tr')).data().referenceId;
				var category = requesttable.item.row($(this).parents('tr')).data().category;
				var comment = $(this).parent().siblings().find("[name=comment]").val();
				var decision = 'Y';
				//alert("Zoom out button "+id + category + comment + decision);
				makeDecision(id, category, comment, decision,referenceId);

		});

		$('body').on('click','#pendingtask-request.collapsed tbody li span>button[name=approve]',function() {
				var id = requesttable.item.row( this ).data().pendingtaskId;
				var referenceId = requesttable.item.row( this ).data().referenceId;
				var category = requesttable.item.row( this ).data().category;
				var cellRow = $(this).parent().parent().closest('tr');
				var comment = cellRow.siblings().find('textarea[name="comment"]').val();
				var decision = 'Y';
				//alert("Zoom in button "+id + category + comment + decision);
				makeDecision(id, category, comment, decision,referenceId);

		});
		<%--END : FOR APPROVE BUTTON --%>
		
		<%--START : FOR REJECT BUTTON --%>
		$('body').on('click','#pendingtask-request tbody td>button[name=reject]',function() {
				var id = requesttable.item.row($(this).parents('tr')).data().pendingtaskId;
				var referenceId = requesttable.item.row($(this).parents('tr')).data().referenceId;
				var category = requesttable.item.row($(this).parents('tr')).data().category;
				var comment = $(this).parent().siblings().find("[name=comment]").val();
				var decision = 'N';
				//alert("Zoom out button "+id + category + comment + decision);
				makeDecision(id, category, comment, decision,referenceId);

		});

		$('body').on('click','#pendingtask-request.collapsed tbody li span>button[name=reject]',function() {
				var cellRow = $(this).parent().parent().closest('tr');
				var id = requesttable.item.row( this ).data().pendingtaskId;
				var category = requesttable.item.row( this ).data().category;
				var referenceId = requesttable.item.row( this ).data().referenceId;
				var comment = cellRow.siblings().find('textarea[name="comment"]').val();
				var decision = 'N';
				//alert("Zoom in button "+id + category + comment + decision);
				makeDecision(id, category, comment, decision,referenceId);
		});
		<%--END : FOR REJECT BUTTON --%>

		}); //end document.ready
</script>
</html>
