<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>LRIT | Log in</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<!-- Bootstrap 3.3.7 -->
<spring:url
	value="/bower_components/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapmincss" />
<link rel="stylesheet" href="${bootstrapmincss}">
<!-- Font Awesome -->
<spring:url
	value="/bower_components/font-awesome/css/font-awesome.min.css"
	var="fontawosmmincss" />
<link rel="stylesheet" href="${fontawosmmincss}">
<!-- Ionicons -->
<spring:url value="/bower_components/Ionicons/css/ionicons.min.css"
	var="ioniconscss" />
<link rel="stylesheet" href="${ioniconscss}">
<!-- Theme style -->
<spring:url value="/dist/css/AdminLTE.min.css" var="adminltemincss" />
<link rel="stylesheet" href="${adminltemincss}">
<!-- iCheck -->
<spring:url value="/plugins/iCheck/square/blue.css" var="bluecss" />
<link rel="stylesheet" href="${bluecss}">

<spring:url value="/resources/img/refresh1.png" var="refresh" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<c:url value="/login" var="loginUrl" />
<spring:url value="/resources/img/header_12.jpg" var="bannerImage" />
<spring:url value="/users/forgotpassword" var="forgotpassword" />


<style>
#wrapper-img {
	background-image: url("resources/img/img2.jpg");
}
</style>
</head>
<body class="hold-transition login-page">
	<div id="wrapper-img" class="wrapper">
		<div id="header-img">
			<header class="main-header main-header-custom ">
				<img class="img-fluid" src="${bannerImage}" height="100px"
					width="100%">


			</header>
		</div>

		<div class="login-box">
			<!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to start your session</p>

				<%-- <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"></c:out>
					<c:out value="${successMsg}"></c:out> --%>
				<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
					<font color="red"> Your login attempt was not successful due to 
				 <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />.
					</font>
				</c:if>


				<form action="${loginUrl}" method="post">
					<div class="form-group has-feedback">
						<input type="text" class="form-control" placeholder="Loginid"
							id="username" name="username"> <span
							class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" class="form-control" placeholder="Password"
							id="password" name="password"> <span
							class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</div>
					<div class="form-group">
						<input class="form-control" name='captcha'
							placeholder="Captcha Here.." type="captcha" autocomplete="off"
							required>
					</div>

					<img id="captcha_id" src="/lrit/CaptchaServlet">
					<td align="left"><a href="javascript:;"
						title="change captcha text"
						onclick="document.getElementById('captcha_id').src = '/lrit/CaptchaServlet?' + Math.random();  return false">
							&nbsp;<img src="${refresh}" height="25" width="25" />
					</a></td> <br>&nbsp;

					<div class="row">
						<!-- /.col -->
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Sign
								In</button>
						</div>
						<!-- /.col -->
					</div>
				</form>
				<a href="${forgotpassword}">I forgot my password</a><br>
			</div>
			<!-- /.login-box-body -->
		</div>
		<!-- /.login-box -->
	</div>
	<!-- /.Wrapper end -->

	<!-- jQuery 3 -->
	<spring:url value="/bower_components/jquery/dist/jquery.min.js"
		var="jqueryminjs" />
	<script src="${jqueryminjs}"></script>
	<!-- Bootstrap 3.3.7 -->
	<spring:url
		value="/bower_components/bootstrap/dist/js/bootstrap.min.js"
		var="bootstrapminjs" />
	<script src="${bootstrapminjs}"></script>
	<!-- iCheck -->
	<spring:url value="/plugins/iCheck/icheck.min.js" var="icheck" />
	<script src="${icheck}"></script>
	<script src=""></script>
	<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
