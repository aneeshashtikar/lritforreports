/**
 * Created a script for the category field to open a new
 * tab while registering user
 */

$(function() {
	$('#category').click(function() {
		var _selected = $('#category').val();
		if(_selected == 'Shipping Company')
			{
				var _newtab = '<li id="tab3"><a href="#tab_3" data-toggle="tab">CSO Details</a></li>';
				$('div.nav-tabs-custom ul').append(_newtab);
				$('li#tab3').addClass('active').siblings().removeClass('active');
			}
		else
			{
				$('li#tab3').hide();
			}
		});
	
	$(document).on('dblclick', '#category', function() {
		if($('#category').val() == 'Shipping Company')
			{
				$('li#tab3').show().addClass('active').siblings().removeClass('active');
			}
		});
	$(document).on('click', '.btn-prev', function() {
  		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
  	});
  	
  	$(document).on('click', '.btn-next', function() {
  		$('.nav-tabs > .active').next('li').find('a').trigger('click');
  	});
	});
			/*var num_tabs = $('div.nav-tabs-custom ul li').length + 1;
			alert(num_tabs);

	        $('div.nav-tabs-custom ul').append(
	            '<li><a href="#tab_' + num_tabs + '" data-toggle="tab">Shipping Company Details</a></li>'
	        );
	        
	        $('div.nav-tabs-custom').tabs("refresh");
			}*/
		
	
	
