/**
 * Created a script for the buttons of the sample form page finalform.jsp
 * buttons included are :
 * 1. AddRow 2.Remove 3. Previous 4. Next
 */

$(function() {
	$('#addBtn').click(function() {
		var _tr = '<tr><td><input type="text" id="equipId" size="5"></td>' + 
		          '<td><select id="manuType"><option>Type-1</option><option>Type-2</option><option>Type-3</option></select></td>' + 
		          '<td><select id="manuName"><option>ANRITSU</option><option>SAILOR</option><option>Manufaturer-3</option></select></td>' +
		          '<td><select id="manuModel"><option>RSS 406 A</option><option>H 2095B</option><option>Model-3</option></select></td>' +
		          '<td><input type="text" id="dnidNo" size="3"></td>' +
		          '<td><input type="text" id="memberNo" size="6"></td>' +
		          '<td><select id="oceanRegion"><option>Region-1</option><option>Region-2</option><option>Region-3</option></select></td>' +
		          '<td><input type="radio" name="selectActive" size="5"></td>' +
		          '<td><select id="regStatus"><option>Completed</option><option>Not Completed</option></select></td>' +
		          '<td><button type="button" class="btn btn-danger btn-delete">Remove</button></td>' +
		          '</tr>';
		          
	  $('tbody').append(_tr);
	});
	
  	$(document).on('click', '.btn-delete', function() {
  		$(this).closest('tr').remove();
  	});
  	
  	$(document).on('click', '.btn-prev', function() {
  		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
  	});
  	
  	$(document).on('click', '.btn-next', function() {
  		$('.nav-tabs > .active').next('li').find('a').trigger('click');
  	});
});