Please provide summery of the code you are commiting with your name and date.
Date : 31-july-2019
Name : Kavita Sharma	
Description : New Theme and Dashboard design.Ship Speed position report code added.
Theme : lrit-dashboard.css added for theme customization.
Classes : ReportController, LritLatestShipPostion, ReportRepository, ReportService, ReportServiceImpl
jsp : shipspeed_report.jsp 
dependency : slf4j  
_____________________________________________________________________________________________________________________________

Date : 18-Sep-2019
Name : Kavita Sharma	
Description : Implemented Geographical Area, Send to IDE, Send to DDP, Delete, Delete from DDP,Disapprove from IDE
Theme : lrit-dashboard.css added for theme customization.
Classes : 1. GeographicalAreaRepository, GeographicalAreaRequestRepository (in.gov.lrit.portal.repository.requests)
	      2. GeographicalAreaService, GeographicalAreaServiceImpl (in.gov.lrit.portal.service.request)
		  3. PortalGeoraphicalArea, PortalGeoraphicalAreaRequest (in.gov.lrit.portal.model.request)
	      4. GeographicalAreaRequestController (in.gov.lrit.portal.model.request)
jsp :       1. ga_approved_from_ddp, 
			2. ga_disapprove_from_ide, 
			3. ga_for_ide_approval, 
			4. ga_send_to_ddp

Dependency : axiom-api for gml file
_____________________________________________________________________________________________________________________________

Date : 22-Oct-2019
Name : Bhavika Ninawe	
Description : Implemented Contracting government Usecase
Classes :
 
1. Controller : CGmanagementController.java  (in in.gov.lrit.portal.controller.contractinggovernment)

2. Services : CGManagementService.java,ICGManagementService.java (in in.gov.lrit.portal.service.contractinggovernment)
			 IUserManagementService.java, UserManagementService.java (in in.gov.lrit.portal.service.user)
			 
3. Pojo : PortalContext.java,PortalContextPK.java, PortalLritIdMaster.java,PortalLritIdMasterHistory.java (in in.gov.lrit.portal.model.contractinggovernment)
		PortalCsoDpaDetail.java, PortalShippingCompany.java, PortalUser.java, PortalUserDTO.java, PortalUserSarAuthority.java (in in.gov.lrit.portal.model.user)
		
4. Repository : ContractingGovernmentRepository.java, PortalContextRepository.java, PortalLritIdMasterHistoryRepository.java (in in.gov.lrit.portal.repository.contractinggovernment)
		PortalUserSarAuthorityRepository.java, UserManagementRepository.java (in in.gov.lrit.portal.repository.user)
		
5. Jsp : registercontractinggovernment.jsp, 
		viewcontractinggov.jsp, 
		viewsar.jsp 
		Changed in sidebar.jsp added spring taglib and list for contracting government.
		
_____________________________________________________________________________________________________________________________


Date : 11-nov-2019
Name : Pooja Katkade	
Description : Implemented requests usecase
Classes :
 
1. Controller : RequestController.java,SoapController.java

2. Services : DDPManagementService.java,RequestManagementservice.java,ShipdeailService.java
			 
3. Pojo : in in.gov.lrit.portal.model.request
4. Repository : DcddpNotification.java,Dcpositionreportrepository.java,Dcpositionrequestrepository.java,DCsarsurpicrepository,LatestShipflagrepository.java,


5. Jsp : ddprequestpage.jsp,flagrequestpage.jsp,portrequestpage.jsp,sarsurpic.jsp,surpicpagerequest.jsp,coastalrequestpage.jsp
		
		
		
		
		
_____________________________________________________________________________________________________________________________

Date : 11-NOV-2019
Name : Bhavika Ninawe	
Description : Implemented RoleManagement Usecase

Classes:
1. Controller :  RolemanagementController.java (in in.gov.lrit.portal.controller.user package).
2. Service : IRoleManagementService.java (in in.gov.lrit.portal.service.user
			 RoleManagementService.java (in in.gov.lrit.portal.service.user package)
3. Pojo : PortalRoles.java, PortalRoleActivitiesMappingPK.java, PortalRoleActivitiesMapping.java added (in in.gov.lrit.portal.model.role)
4. Repository : ActivityManagementRepository.java ,PortalRoleActivitiesMappingRepository.java ,RoleManagementRepository.java is added (in in.gov.lrit.portal.repository.role)
5. Jsp : createrole.jsp, editrole.jsp, viewrole.jsp, viewroledetail.jsp (in role folder) 
Changed in sidebar.jsp added spring taglib and list for contracting government.
		Changed in sidebar.jsp added spring taglib and list for contracting government.
		
_____________________________________________________________________________________________________________________________

Date : 13-NOV-2019
Name : Bhavika Ninawe	
Description : Implemented UserManagement Usecase without shipping company

Classes Added :
1. Controller : UsermanagementController.java (in in.gov.lrit.portal.controller.user)
2. Service : IUserManagementService.java UserManagementService.java (in in.gov.lrit.portal.service.user)
3. Pojo : PortalUsersPassword.java PortalUsersRoleMapping.java PortalUsersRoleMappingPK.java PortalUserTransit.java ChangePassword.java  (in in.gov.lrit.portal.model.user)
4. Repository : PortalUsersRoleMappingRepository.java PortalUserTransitRepository.java (in in.gov.lrit.portal.repository.user)
5 Jsp : createuser.jsp,edituser.jsp,viewusers.jsp,viewusersdetails.jsp (in users folder)
6. Common : Encrypt.java Utility.java UUIDGeneratorUtil.java (in in.gov.lrit.portal.CommonUtil)

added dependency : generate-unique-id, commons-codec, spring-security-crypto.

Classes Changed :
1. Controller : RolemanagementController.java changed persist edit role.
________________________________________________________________________________________________________________________________



Date : 13-NOV-2019
Name : Bhavika Ninawe

Description : Changed in UserController for file download and commented alert in user_tab.js file

________________________________________________________________________________________________________________________________
Date : 13-Nov-2019
Name : Kavita Sharma	
Description : Implemented Geographical Area changes ,Delete DNID, Delete SEID, Re-Download SEID 
packages : in.gov.lrit.portal.webservice.config ASPSoapConnector,ASPWebServiceConfig, one report added,
in.gov.lrit.portal.model.vessel
Classes : 1. ShipEquipmentRepository, VesselDetailRepository (in.gov.lrit.portal.repository.vessel)
	      2. VesselDetailService, VesselDetailServiceImpl (in.gov.lrit.portal.service.vessel)
		  3. GMLAreaCodeMappingRepository,  (in.gov.lrit.portal.gis.lritdb.repository)
		  4. VesselController  (in.gov.lrit.portal.controller.vessel)
		  5. in.gov.lrit.portal.model.vessel(all the vessel related pojo)
jsp :       1. pendingtask_request, 
			2. pendingtask_shipequip, 
			3. shipborne_equipment_detail
			4. vesseldetaillist
			
Dependency ASP 

_____________________________________________________________________________________________________________________________


Date : 14-nov-2019
Name : Pooja Katkade	
Description : Changed Request JSP,merged common model & repository,renamed classes.
Classes :
 
1. Controller : RequestController.java,SoapController.java

2. Services : DDPManagementService.java,RequestManagementservice.java,ShipdeailService.java
			 
3. Pojo : in in.gov.lrit.portal.model.request
4. Repository : DcddpNotification.java,Dcpositionreportrepository.java,Dcpositionrequestrepository.java,DCsarsurpicrepository,LatestShipflagrepository.java,


5. Jsp : ddprequestpage.jsp,flagrequestpage.jsp,portrequestpage.jsp,sarsurpic.jsp,surpicpagerequest.jsp,coastalrequestpage.jsp

________________________________________________________________________________________________________________________________

Date : 15-NOV-2019
Name : Bhavika Ninawe
Description : Changed in role management and user management

Classes edited :
1. Jsp : createrole, viewrole, editrole, viewroledetail, createuser
2. controller RolemanagementController.java UsermanagementController.java
3. Repository : RoleManagementRepository
4. Js : addRow.js


________________________________________________________________________________________________________________________________
Date : 15-Nov-2019
Name : Kavita Sharma	
Description : Implemented spring Security with default login page on https://localhost:8443/lrit/
packages : in.gov.lrit.portal.controller.logging,in.gov.lrit.portal.repository.logging,in.gov.lrit.portal.service.logging--> (login/logout details on DB)
in.gov.lrit.security.config --> Spring security classes

Classes : 1. PortalUserInterface, SecurityUserInterface, UserSessionInterface
	      2. PortalLoginAudit (in.gov.lrit.portal.model.logging)
		  3. PortalLoggingRepository (in.gov.lrit.portal.repository.logging)
		  4. PortalLoggingService, PortalLoggingServiceImpl (in.gov.lrit.portal.service.logging)
		  5. CustomAuthenticationProvider, CustomLogoutHandler, CustomUserService, LoginFailureHandler, LoginSuccessHandler, SecurityConfig
		  6. Utility, Captcha Servlet (in.gov.lrit)
jsp :       1. verticalgeoserver --> Added location access function in javascript
			2. header --> logout link added 
			3. login.jsp --> new jsp for login page
		
			
Dependency --> spring-boot-starter-security 
 
________________________________________________________________________________________________________________________________
Date : 15-Nov-2019
Name : Kavita Sharma	
Description : ADDED footer.jsp 
added the footer in verticalgeoserver.jsp
removed box header from the map
added background in login jsp


________________________________________________________________________________________________________________________________
Date : 18-Nov-2019
Name : Kavita Sharma	
Description : website headers changed
home link added on the header image
geographical area menu added in different list
GIS controller mapped with "/map" 
csrf token disabled
space removed in login page error messsage
Display_Map.js url changed accordingly


________________________________________________________________________________________________________________________________
Date : 18-Nov-2019
Name : Bhavika Ninawe	
Description : changed in shiping company registration and view
________________________________________________________________________________________________________________________________

Date : 14-nov-2019
Name : Pooja Katkade	
Description : updated RequestCOntroller.
added validation code in requestcontroller.

________________________________________________________________________________________________________________________________

Date : 19-nov-2019
Name : Kavita Sharam	
Description : getddpversion changed in geographicalarea request controller according to new schema
footer added in geoarea pages and shipborneequipment table
title added in the pages

Date : 19-Nov-2019
Name : Mayuri Mali
Description : SAR-Surpic report is added. sarsurpic_request_report.jsp is page to view report.

________________________________________________________________________________________________________________________________

Date : 19-nov-2019
Name : Kavita Sharma	
Description : Side bar managed
removed alerts
extra code removed


________________________________________________________________________________________________________________________________

Date : 19-nov-2019
Name : Kavita Sharma	
Description : Geographical Area Reponse update as per DC
				SoapConnector and GeographicalAreaServiceImpl
jquery removed from header.jsp



________________________________________________________________________________________________________________________________

Date : 20-nov-2019
Name : Bhavika Ninawe
Description : Usecase of shiping company is added
editusershippingcompany.jsp  editcsoalternatecsomapping.jsp

Changed in UsermanagementController,  UserManagementService, PortalCsoDpaDetailRepository and ShipingCompanyRepository

________________________________________________________________________________________________________________________________

Date : 20-nov-2019
Name : Deepika
Description : Usecases of Vessel Management, Vessel Action Management, Vessel Group Management, Manufacturer & Model Management are added
Added Vessel's Jsp - allvessels.jsp, vesselregistration.jsp, viewvessel.jsp, editvessel.jsp, approvevessel.jsp, submittedship.jsp
Added Vessel Group's - Jsp = allvesselgroup.jsp, vesselgroup.jsp, viewgroup.jsp
Added Model's jsp - allmodel.jsp
Added Manufacturer's Jsp - allmanufacturer.jsp 
Added Vessel Action's Jsp - reportingstatus.jsp, showrepurchaseship.jsp, showallforaction.jsp

Added PortalShipEquipment, ShipEquipmentController, PortalShipEquipmentService, PortalShipEquipmentServiceImpl and ShipEquipmentRepository
Added PortalVesselDetail, PortalVesselList, VesselController, VesselService, VesselServiceImpl and VesselRepository
Added PortalVesselGroup, GroupList, VesselDetailGroupController, VesselGroupService, VesselGroupServiceImpl and VesselGroupRepository
Added PortalVesselType, VesselTypeService, VesselTypeServiceImpl and VesselTypeRepository
Added PortalVesselCategory, VesselCategoryService, VesselCategoryServiceImpl and VesselCategoryRepository 
Added PortalShippingCompanyVesselRelation, VesselLoginMappingId
Added PortalManufacturerDetail, ManufacturerList, ManufacturerController, ManufacturerService, ManufacturerServiceImpl and ManufacturerRepository
Added PortalModelDetail, ModelList, ModelController, ModelService, ModelServiceImpl and ModelRepository
Added VesselRegistrationDTO, ApprovedVesselRegistrationDTO, DocumentForm, Mapping Names
Added PortalDocuments
Added PortalRelation
Added ShippingCompanyController
_________________________________________________________________________________________________________________________

Date : 20-nov-2019
Name : Deepika
Description : Some comments were left to remove, so removed
edited vesselgroup.jsp

_________________________________________________________________________________________________________________________
Date : 20-nov-2019
Name : Pooja
Description : jsp search button change in all the request from portal

_________________________________________________________________________________________________________________________
Date : 20-nov-2019
Name : Kavita
Description : Final Demo war Purpose 
commented some alerts in header.jsp
header removed from map
shipposition.js --> commented data for the ship
_________________________________________________________________________________________________________________________
Date : 21-nov-2019
Name : Bhavika
Description :
Demo purpose commented hyperlink in view user
Changed in acknowledgement page added common.jsp and footer.jsp
_________________________________________________________________________________________________________________________
Date : 21-nov-2019
Name : Kavita
Description :
commented user profile view option


pooja
no conflict for report added in jsp
_________________________________________________________________________________________________________________________
Date : 21-nov-2019
Name : Deepika
Description :
commented documents on editvessel.jsp
commented error on vesselregsitration.jsp
created two sub menu option on vessel group menu

_________________________________________________________________________________________________________________________
Date : 02-DEC-2019
Name : YOGENDRA YADAV
Description :
BILLING MODEL IS INTEGRATED IN MAIN PROJECT
CHANGES MADE IN CURRENT PROJECT ARE BELOW : 
1.POM.XML HAS BEEN UPDATED DUE TO JASPER DEPENDENCYS.
	**i)jasper report related dependency.
	**ii)postgres dependency scope has been changed.
2.PAKAGES DETAILS:
	**i)billing.dao and ide package included in PortalDataSourceConfig.
	**ii)ddp.model included in DDPDataSourceConfig.
	iii)billing.model.billingservice, billing.model.billingservice.bill and billing.model.billingservice.invoice models
		for invoice and bill and operation based on invoice and bill is billing.service.billingservice.bill.
		and billing.service.billingservice.invoice and common method for both included in billing.service.billingservice
	iv)billing.model.contract included all contract model with child class.
		Contract Related operation in billing.service.contact and custom method in billing.dao.contract.
	v)billing.model.payment have model in corresponds to payment regarding bill or invoice.
	vi)billing.model.payment.billableitem contains rule of message calculation.
	vii)In in.gov.lrit.config package also files FileStorageProperties and MultipartConfig for operation on csv files of ide data.
	viii)ddp.model relation between cgs with asps, dcs.
	ix)in.gov.lrit.mail for mail operation with attachment or without attachment.
	x)ide.model used for journal extract functionality
3.FRONT END:
	i)billing.js and validation.js has been added in resources/js.
	**ii)changes made in lrit-dashboard.css to override datepicker setting.
	iii)in jsp/billing folder contains all billing jsp files.
	iv)finalHeader.jsp and finalFooter.jsp created in main jsp folder bcoz it consist of comman header and footer for all files.

	**v)sidebar has been updated
	

________________________________________________________________________________________________________________________________
Date : 3-dec-2019
Name : Pooja Katkade
Description : Changed soap connector's callwebservice method,added security related policies in requests jsp.,changed msgid method


Classes : 1.RequestController.java
		  2.SoapConnector.java
		  3.commonutilservice.java
jsp :     1.flagrequest.jsp
		  2.ddprequest.jsp
		  3.surpicrequest.jsp
		  4.coastalrequest.jsp
		  5.sarrequest.jsp
		  6.portrequest.jsp
		
			
________________________________________________________________________________________________________________________________	
Date : 03-dec-2019
Name : Deepika
Description : Added Pending Task Code in Edit Vessel Detail and Change Vessel Reporting Status 
Classes : 1. Added Sequence in PortalVesselDetails.java
		  2. Added PortalVesselDetailHistory.java, PortalVesselReportingStatusHistory in in.gov.lrit.portal.model.vessel package
		  3. Added VesselDetailHistoryRepository, VesselReportingStatusHistoryRepository in in.gov.lrit.portal.repository.vessel package
		  4. Added VesselDetailHistoryService, VesselDetailHistoryServiceImpl , VesselReportingStatusHistoryService, VesselReportingStatusHistoryServiceImpl in in.gov.lrit.portal.service.vessel package
		  5. Changed ShipEquipmentController.java 
		     a) editVesselDetailPage() method 
		     b) updatereportingstatusPage() method
		     c) commented getacsomethod() method
		     d) added condition to view vessel with dpa = null in all methods
jsp :     1. editvessel.jsp
		  2. viewvessel.jsp
		  3. approvevessel.jsp
		  4. vesselregistration.jsp (removed acso name column from company detail tab)
js :      1. validateform.js (added condition to check same cso if company name and login id is same for both companies while registering vessel)



Date :4-Dec-2019
Name : Mayuri Mali
Description: Created company details page for gis.
			methods added in following classes :
			class UserManagementService :  getUserDetailsByImo(), getCurrentCsoDetails(String imoNo), getAlternateCsoDetails(String imoNo)
			UserManagementRepository, PortalCsoDpaDetailRepository, UserManagementController.
			jsp page: users/company_cso_details.
			
			Added Vessel Route code :- modified sidebar.jsp. 
			
			Added Alert Code for displaying alerts depending on user roles and categorized by severity.

--------------------------------------------------------------------------------------------------------------------------------------------------------

Date :5-Dec-2019
Name : Pratibha Nikam
Description: GIS module changes (Display_Map.js and mapControl.js changes)
1) Grid Line shown on map with visible on/off functionality.
2) Ship Detail Popup added resize button. (design updated)
3) Animation for search ship
4) Path Prediction function changes for vector length variation according to speed and given time duration.
5) Multiple range bearing tool is added in addition to existing distance tool(distance and bearing tool).
6) MRCC Names are added on Map inside each MRCC.
7) Maximize map(Full screen map) functionality added.
8) lat, long search added.

jsp pages modification:
1) verticalgeoserver.jsp modified for adding mapFullscreen button.
2) mapView.jsp modified for adding lat, long search and map scaleline.

mrccColombo.json file is added in resources.
--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 6-Dec-2019
Name: Shivraj Dongawe 
Description: Billing module changes(Added sequence generator for some of the entity classes,Add and edit contract changes )
1) Added sequence generator for following entity classes (name of the sequence used from the database is also mentioned) 
   a)  Service - billing_billing_service
   b) BillableItem - billing_billable_items
   c) ContractAnnexure - billing_contract_annexures
   d) Contract - billing_contract_id 
   e) ServiceStatus - billing_service_status_seq
   f) ContractStakeholders - billing_contract_stakeholders_seq
   g) PaymentDetailsPerMember - billing_payment_details_per_member_seq
   h) PaymentDetailsForService - billing_payment_details_for_service_seq
   i) ReminderLog - billing_reminder_log_seq
   j) JournalExtract - billing_journal_extract_seq
   k) JournalExtractLog - billing_journal_extract_log_seq
   
2) Retrieving DDP information from lrit_datacentre_info using regular version number(Previously, immediate version number was used). 
3) Above changes are done for add contract functionality of all contract types 
4) Modified edit contract functionality to provide following features: 
   a) New instance of the contract will be created only if contract is slated to be extended(Valid from entered in edit contract is greater than valid to of the contract to be edited). 
   b) For any other modification(which does not lies under previous condition) existing contract will modifed. 
   c) A single contract can have multiple contract annexures by way of multiple edits and annexure uploads.
 
--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 10-Dec-2019
Name: Bhavika Ninawe
Description: User Usecase and UserProfilemanagement usecase  implementation and added role base management for contractinggovernment, role, user and route  
1. Added @PreAuthorize annotation in UsermanagementController, RolemanagementController, CGmanagementController 
2. Added method for userprofile and users usecase in UsermanagementController , UserManagementService.
3. Added role based management in sidebar.jsp and user, role, route and contracting government folder of jsp 

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 10-Dec-2019
Name: Kavita Sharma	
Description: 

Pending task controller and PendingTaskService,PendingTaskServiceImpl classes Code added for pednign task
Request, deletednid, deleteseid,vesseledit completed.

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 10-Dec-2019
Name: Bhavika Ninawe
Description: Changed in sidebar.jsp in goegraphical area controller

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 10-Dec-2019
Name: Bhavika Ninawe
Description: Changed in geographicalAreaRequest Controller and in user, role and contracting government .

Added @PreAuthorize in GeographicalAreaRequest 
Changed color of buttons in user,role, contracting government jsp



--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 11-Dec-2019
Name: Bhavika Ninawe
Description: Applied @PreAuthorize in Manufacturer, Model and in Vessel Group usecase

Applied @PreAuthorize in ManufacturerController, ModelController and VesselGroupController.
Applied sec:authorize tag on sidebar.jsp allmanufacturer.jsp , allmodel.jsp , viewgroup.jsp

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 11-Dec-2019
Name: Shivraj Dongawe
Description: Added entity classes 

I have already added following entity classes:
1. in.gov.lrit.billing.model.billingservice.invoice.ApprovedInvoice for table billing_approved_invoice
2. in.gov.lrit.billing.model.billingservice.invoice.GeneratedInvoice for table billing_generated_invoice
3. in.gov.lrit.billing.model.contract.CurrencyType for table billing_currency_type 

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 11-dec-2019
Name : Deepika
Description : Added AddSEID option in Vessel Action menu
Added ViewVesselMergedDTO in vessel pojo
-----------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 12-dec-2019
Name : Mayuri Mali
Description : Close alert's, playing sound, and flashing alert icon code added in header.jsp. 
			Code to display DDP version on dashboard is added in common,.jsp (url :- /getddpversion in RouteController class).
			modified Query of method getUserDetailsByImo() from UserManagementRepository class.
			modified query of methods getCurrentCsoDetails(String imoNo), getAlternateCsoDetails(String imoNo) from PortalCsoDpaDetailRepository.
			modified query of methods getCurrentCsoDetails(String imoNo), getAlternateCsoDetails(String imoNo) from PortalCsoDpaDetailRepository.
			modified query of methods getCurrentCsoDetails(String imoNo), getAlternateCsoDetails(String imoNo) from PortalCsoDpaDetailRepository.
			
-----------------------------------------------------------------------------------------------------------------------------------------------------------
			
Date : 12-DEC-2019
Name : Pooja Katkade	
Description : Implemented shipborne equipment usecses
Classes :
 
1. Controller : VesselController.java

2. Services : VesselDetailsService.java
			 
3. Pojo : PortalShipEquipementHst.java
4. Repository :  PortalShipEquipementrepository.java,PortalVesseldetailRepository.java

		
--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 12-Dec-2019
Name: Kavita Sharma	
Description: 

Pending Task and Pending task link added on the dahsboard
Dynamic role and context based pending task display

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 12-Dec-2019
Name: Bhavika Ninawe
Description: Changed in user controller issue of appending "" in company name is resolved

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 13-Dec-2019
Name: Kavita Sharma	
Description: 
Dashboard image changed.
portalvasselactionpendingtask removed
sar authority added in the session for category coast guard	

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date: 13-Dec-2019
Name: Kavita Sharma	
Description: 
Pending task handled for null values

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 16-Dec-2019
Name : Deepika
Description : Added shipping company wise list activity in Vessel Management Menu
1. Added shipping company wise list activity 
2. edited allcompany.jsp for showing all company 
3. edited allvessel.jsp for showing all company vessel list and submitted ships without approve option 
4. edited ShipEquipmenController for showing pages as per user category
5. edited VesselController for showing data as per company  selected by admin
6. added supporting document input filed for taking supporting document in case of Add SEID

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 16-Dec-2019
Name : Pratibha
Description : 
1) open Standing Order Added
2) SAr Surpic Request Changes
3) Login Base Access Added
4) Close Sar Status Pending
5) Flag vessel reporting status added in common.jsp (working on all pages in new tab)
6) ships counts of With in 1000 NM  ,Flag vessel world wide added in common.jsp. note: working on home pages only  (issue in new tab)

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 16-Dec-2019
Name : Kavita Sharma
Description : 
1. session data in geographical area request
2. sequence added for portalloginaudit
3. asp jar removed
4. pending task handle for null values
	

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 16-Dec-2019
Name : Nikhil C	
Description :  Standing Order Usecase Implemented
1. New method for Standing Order created in SoapConnector
2. new query created in PolygonRepository
3. new entry for Standing Order in sidebar.jsp
------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Deepika
Description : Updated url for Manufcaturer and Model Mgmt url
Updated url for Group Mgmt
Updated redirection url for vessel registration and edit vessel

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Varunkrishnan T K	
Description :  Add Geographic area Form replaced
1. Geographic Area Form will open in new tab.
2. Degree decimal added to Longitude and Latitude input field of Geographic Area Form.
3. Bounding rectangular coordinate added in Geographic Area Form. 

-----------------------------------------------------------------------------------------------------------------------------------------------------
Date : 12-Dec-2019
Name : Mayuri Mali
Description : Alert page refresh timer interval changed.
			 update button class changed in viewroute.jsp
			 
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Deepika	
Description :  Changed url to acknowledgment for vessel action

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Nikhil	
Description :  Status update message displayed in Standing order usecase
			   Displayed all Standing order on load in standingorder.jsp
----------------------------------------------------------------------------------------------------------------------------------------------------

Date : 19-Dec-2019
Name : Mayuri Mali
Description : Queries for sar-supic report has been modified.
			  Solved issue of repeated response coming against message_id. 
			  Added class CompositeKeyForSarSurpicRequest.java

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Deepika	
Description :  Corrected admin was not able to edit vessel details and approve vessel 		   
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Deepika	
Description :  Corrected approve vessel registration url updated on submittedship.jsp 
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Yogendra Yadav	
Description : 
1)Invoice Model has been Updated, Multiple generated invoice and approved invoice can be processed on invoice as per count.
2)ViewInvoice.jsp created to get overview of invoice, contracting government and payment history.
3)Alignment in BillableItem category.
4)Invoice action removed from DTOs.
5)logger implemeted.
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Mayur Kulkarni	
Description : 
1)Bill Flow change as per Client requirement
2)Views created for Contracts and Bills
3)Alignment in Contracts and Bill pages
4)Bill and Contracts action removed from DTOs (Hyperlinks).
5)logger implemeted.
6)Adding Contracting govts that are not belong or associated with specific asp/dc
7)Annexure Document validation

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Deepika	
Description :  Tested and Updated vessel sold and vessel scrap and added access restriction on vessel action and add seid menu 
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 17-Dec-2019
Name : Deepika	
Description : Updated access for vessel action and vessel management menu option added activities in activity table

--------------------------------------------------------------------------------------------------------

Date: 18-Dec2019
Name: Pratibha
Description:
1) Sarsurpic/ coastal surpice reuest issue solved.
2) foreign request on right click functionality added.


---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 18-Dec-2019
Name : Mayur Kulkarni	
1)Update Contract Change for CSP,USA,Shipping Company

--------------------------------------------------------------------------------------------------------

Date: 18-Dec-2019
Name: Kavita Sharma
Description:
1. Messsage displayed along with message in geographical area.
2. Flag vessel count by frequency set 
Packages added :
in.gov.lrit.portal.controller.common
in.gov.lrit.portal.repository.common
in.gov.lrit.portal.service.common
in.gov.lrit.portal.model.common
3. Commented alert in pending tasks


---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 18-Dec-2019
Name : Bhavika Ninawe
Description : 
1)Update Contract Change for CSP,USA,Shipping Company
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 18-Dec-2019
Name : Shivraj Dongawe 	
Description : 
1) Merged changes of mayur and yogendra 
2) Resolved detached child bug occured while extending the contract with annexures.
3) Renamed package com.gov.lrit.billing to in_.gov.lrit.billing and modified repositories accordingly for DTOs 




Handled Connection timeout error from mail server and commented alerts
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 18-Dec-2019
Name : Bhavika Ninawe
Description : 
MAil server configuration was changed for testing now reverted back.

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 19-Dec-2019
Name : Kavita Sharma
Description : 
Pending task bug resolved.
geographical area schema version and test get from db

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 19-Dec-2019
Name : Kavita Sharma
Description : 
Pending task bug resolved.
Inactive ship count on dashboard added

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 19-Dec-2019
Name : Pratibha Nikam
Description : 
1) History bug fixed
2) sar area view access added
3) surpic from circle added
4) popup bug fixed

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 23-Dec-2019
Name : Shivraj Dongawe 	
Description : 
1) Added sample code to manage transaction at service layer using Transactional annotation and to handle exceptions of service layer at controller layer 
   in ContractCSPController and ContractCSPServiceImpl for adding contract
2) Added access control related to code in required jsp pages(sidebar.jsp) and controllers for billing  
3) All the billing related pages and controller methods will be accessible by a user who has a role who can perform lritaccount activity 
4) Added lritaccount activity in the database. 

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 26-Dec-2019
Name : Pratibha Nikam
Description : 
1) Layer List Bug Solved.
2) Map Shifting issue resolved.
3) previous ship data removed at the time of data refresh.
4) right click for flag vessel issue resolved.
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 27-Dec-2019
Name : Pratibha Nikam
Description : 
1) within 1000NM count and flagwise count function changed.
2) Labeling for DDP Polygon Added
3) Lrit id for counry Name added
4) search Lat Lon button Added.

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 27/12/2019
Name : Kavita Sharma
Description : 
1. message id displayed in geographical area.
2. sar surpic request created for pending tasks.
3. inactive to active pending task implemented.
4. bug resolved for edit vessel pending task.

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 27-Dec-2019
Name : Mayuri Mali
Description: 
	Ship position request report added.(jsp is report/ship_position_request_report)
	Ship position response report added (jsp is report/ship_position_report)
	Asp ship position report added (jsp :- report/asp_ship_position_report)
	Geographical area updated sent to IDE/DDP report added. (jsp:- report/ga_update_to_ide, report/ga_update_to_ddp)
	modified sidebar to add these menu report option.
	
---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 27-Dec-2019
Name : Deepika	
Description : Added Repurchase Vessel Code in updatereportingstatus method of ShipBorneController.
Updated List of Vessels in view/edit vessel detail option.
Updated vessel action list.
---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 27-Dec-2019
Name : Deepika	
Description : Added Admin flow for reporting status change status action and changed eligibility condition too.

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 28-Dec-2019
Name : Deepika	
Description : Resolved Addseid Issue
Resolved Approve Registration Issue
Added status on reporting status jsp


---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 28-Dec-2019
Name : Bhavika	
Description :  Changed in Reporting time stamp from ITC to UTC

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 30-Dec-2019
Name : Deepika	
Description : Resolved Add document issue while sold and scrap vessel
Removed document in case of inactive to active vessel status
Removed status on reporting status jsp

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 30-Dec-2019
Name : Pratibha Nikam
Description :
1) Port Layer Added in Layer List
2) Clear Button Added in Layer List
3) Time changed from IST to UTS
4) Coastal Sea(Yellow Color) changed to black

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 03-Ja-2020
Name : Shivraj
Description :
1) Moved email ids used in code to application.properties
2) Modified messages.propertiesz

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 03-Jan-2020
Name : Yogendra Yadav
Description :
1) Exception handling implemented.
2) Logger Included.
3) Method Desciption.
4) Update Payment paid amount issue solved.

_________________________________________________________________________________________________________________________

Date : 06-Jan-2020
Name : Deepika
Description : Changed all vessel list to show different vessels for owner company and its users

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 06-Jan-2020
Name : Bhavika Ninawe
Description : Changed in users "" are now not seen if shipping company code is already present

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 07-Jan-2020
Name : Kavita Sharma	
Description : 
1. context added in session.
2. pending task get by context.
3. logging for pending tasks.
4. login handle for no password in db.
5. pending task change for redownload dnid.
6. rightbar jsp removed from viewusers.jsp.
7. datatable script and css added in verticlegeoserver.jsp to display the table on click on the number in side bar

-----------------------------------------------------------------------------------------------------------------------------------------------------

Date : 07-Jan-2020
Name : Mayuri Mali
Description :
		Alerts are displayed according to context id of user(changed queries).
_________________________________________________________________________________________________________________________

Date : 07-Jan-2020
Name : Deepika
Description : Validaton issues resolved
Image and Document size while registration validation changed to 2MB
Resolved edit vessel issue.

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 07-Jan-2020
Name : Kavita Sharma	
Description :
1. Pending task bugs resolved for delete SEID


---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 07-Jan-2020
Name : Pratibha Nikam	
Description :
1. Popup issue resolved.
2. Sar Area View issue resolved.
3. Shipposition query changed to show active and inactive ships.
4. custom polygon issue resolved.
5. added form reset in refresh.
6. inside 1000nm validation added for coastal surpic request.
7. gis datatable design  changes to resolved conflict with portal datatable
8. superUser Activity added to view ships according to dc_id instead of lrit_id
( changes made in ship search, history, shippostions, shiping company and popup for superuser)

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 07-Jan-2020
Name : Mayur Kulkarni	
Description :
1. update contract implemented
2. bill, csp bill
3. logger implemented
4. description of methods and classes
---------------------------------------------------------------------------------------------
Date : 08-Jan-2020
Name : Mayur Kulkarni	
Description :
1. update CSP contract issue resolved.
2. update ASP-DC annexure validation removed.

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 08-Jan-2020
Name : Yogendra Yadav
Description :
1) Mail Invoice To Agency CC has been Added.
2) Invoice Related Details in send Reminder.
3) DateFormat in update billable item category. 
----------------------------------------------------------------------------------------------------------------------------------------------------
Date : 08-Jan-2020
Name : Mayur Kulkarni	
Description :
1. Contract Related issues
2. Bill and Payment in Bill related Changes

----------------------------------------------------------------------------------------------------------------------------------------------------
Date : 08-Jan-2020
Name : Shivraj Dongawe	
Description :
1. Changed ide/journal extract related pages to show right sidebar 
2. Added one exception in catch block of upload journal extract controller to check for non selection of files in browse. 

---------------------------------------------------------------------------------------------------------------------------------------------------
Date : 10-Jan-2020
Name : Kavita Sharma	
Description :
1. Empty password and username check in login

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 10-Jan-2020
Name : Yogendra Yadav
Description :
1) View Invoice : Send Reminders and Action Details shown.

----------------------------------------------------------------------------------------------------------------------------------------------------
Date : 10-Jan-2020
Name : Shivraj Dongawe	
Description :
1. Corrected service code for add bill and add csp bill functionality  
------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 13-Jan-2020
Name : Mayuri Mali
Description : Close alert functionality added in viewall alert page.
			  code added to display closed alerts in specific time period.(jsp:-viewclosedalerts.jsp)
			  
------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 13-Jan-2020
Name : Bhavika Ninawe
Description : Change in User validation and added alternate email id in createuser, edituser, editshipingcompany and user profile
1. Changed in addRow.js file (commented alert)
2. Changed in user-tabs.js file for validation 
3. Changed in user Controller , userService , user repository
4. Changed in PortalUser pojo and in portalUserTransist  added alertnateEmailId


------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 13-Jan-2020
Name : Bhavika Ninawe
Description : Changed in UI of shipping company and loginID is taken from session for portalUserTransit.setUpdatedBy column is changed 
1. Changed in createuser.jsp editusershippingcompany.jsp userprofile.jsp
2. Changed in userController

---------------------------------------------------------------------------------------------------------------------------------------------------

Date : 14-Jan-2020
Name : Kavita Sharma	
Description :
1. SEID, DNID and member number shown in shipbournequipment action

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 14-Jan-2020
Name : Mayuri Mali
Description : Code added to display alerts and closed alerts user category wise.

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 14-Jan-2020
Name : Deepika
Description : Removed timedelay feild from vesselregistration page and added at approval page for admin.
Added two more option for active to inactive reporting status change as Abandoned and Other
Make documents as not mandatory at time of registation and dynamic list of document at approval and edit time.

--------------------------------------------------------------------------------------------------------------------------------------------------------
Date : 15-Jan-2020
Name : Pratibha Nikam
Description : 
1) right click on foreign ship issue resolved.
2) Clear Button added in countryboundary, countryships and shipping company model;
3) Sar Area View and History calendar changes.
4) 24 Hours sar visible always.
5) custom polygon modify issue resolved.
6) search Ship issue resolved.
7) ship history Styling changed for start point. 

-----------------------------------------------------------------------------------------------------------------------------------
Date : 15-Jan-2020
Name : Varunkrishnan T K
Description: 
Range Ring Tool modified ( Distance & Bearing integrated with range ring , Ring Distance default set to 1NM )

-----------------------------------------------------------------------------------------------------------------------------------
Date : 15-Jan-2020
Name : Deepika
Description: Added Sold, Scrap, Active Selection option in edit/view vessel list and removed ship_regsistered option.
Added vesselId column in edit/view vessel list menu

-----------------------------------------------------------------------------------------------------------------------------------
Date : 16-Jan-2020
Name : Nikhil
Description: Edited Standing Order usecase as per new requirement
1. Handled some test cases (create SO fails, DC call fails).


-----------------------------------------------------------------------------------------------------------------------------------
Date : 17-Jan-2020
Name : Kavita Sharma
Description: Right side bar data display in "Flag vessels reporting status" and  "Flag vessels by Reporting frequency"
for shipping company and different contracting goverment 

-----------------------------------------------------------------------------------------------------------------------------------
Date : 17-Jan-2020
Name : Deepika
Description: Vessel Action menu's (ship sold, ship scrap, change reporting status(active to inactive and inactive to active) 
made available to only shipping company to perform other user category will get an error message regarding they cannot perform
those action.
Repurchase submenu option has been added in Vessel Action menu and only other company's sold vessel are visible in this option

-----------------------------------------------------------------------------------------------------------------------------------
Date : 17-Jan-2020
Name : Kavita Sharma
Description: Pending task conflict handle if action is already performed for delete dnid, delete seid, forcefull seid delete
standing order conflict resolved

-----------------------------------------------------------------------------------------------------------------------------------

Date : 17-Jan-2020
Name : Yogendra Yadav
Description:
1)New billableitemcategorylog to store the history record of rates 
2)view Billable ITem Category log history view
3)edited billable item category log 
--------------------------------------------------------------------------------------------------------------------------------------------
	
Date : 17-Jan-2020
Name : Mayur Kulkarni
Description:
1)Retrival of rate from BillableItem in Service(Invoice,Bill). 

---------------------------------------------------------------------------------------------------------------------

Date : 20-Jan-2020
Name : Deepika	
Description: Change Repurchase Ship to Repurchase Vessel.
Corrected Error Message for Document not uploaded case. 
---------------------------------------------------------------------------------------------------------------------

Date : 20-Jan-2020
Name : Bhavika	
Description: Added Country name in view users
Added Country name in view users
---------------------------------------------------------------------------------------------------------------------

Date : 20-Jan-2020
Name : pratibha	
Description:
1) display surpic request of 24 hours with hide and view button
2) sar poll request for ship which are in surpic area
-----------------------------------------------------------------------------------------------------------------------------------

Date : 20-Jan-2020
Name : Yogendra Yadav
Description: 
1) Billable Item log has been Resolved

-----------------------------------------------------------------------------------------------------------------------------------

Date : 20-Jan-2020
Name : Shivraj Dongawe
Description: 
1) Now fetching list of payment billable item categories by comparing valid from and valid till of payment billable item category logs 

-----------------------------------------------------------------------------------------------------------------------------------

Date : 22-Jan-2020
Name : Nikhil Chinchole
Description: 
1. Changes in view of datatable(pagination, info, scroll, etc). 

------------------------------------------------------------------------------------------------------------------------------------------
Date : 22-Jan-2020
Name : Mayuri Mali
Description : Asp Ship position report modified.
			CSP Log, ASP Log, DDP Log reports are added.
			
------------------------------------------------------------------------------------------------------------------------------------------
Date : 23-Jan-2020
Name : Bhavika Ninawe
Description : Changed in jsp of viewuserdetailshipingcompany.jsp changed file path for download

------------------------------------------------------------------------------------------------------------------------------------------
Date : 23-Jan-2020
Name : Pratibha Nikam
Description : 
1) Geographical port layer added
2) Ship Status from DB
3) filtered vessel design change
4) Ocean Region labeling
------------------------------------------------------------------------------------------------------------------------------------------
Date : 24-Jan-2020
Name : Mayuri Mali
Description: Added Flag vessels at sea report.
			modified ASP ship position, CSPLog repo.
			
------------------------------------------------------------------------------------------------------------------------------------------
Date : 24-Jan-2020
Name : Deepika
Description: Made Vessel Group field not mandatory for Vessel Usecases(while registration and edit)
             Corrected findbyImoNo() method for showing vessel details on map.
             Corrected document error message if no document present.
				
------------------------------------------------------------------------------------------------------------------------------------------
Date : 24-Jan-2020
Name : Pratibha Nikam
Description: 
1) search ship datatable issue resolved.
2) Show Custom polygon on based on user login.

------------------------------------------------------------------------------------------------------------------------------------------
Date : 24-Jan-2020
Name : Deepika
Description: Corrected message for repurchase vessel if imo exist
Corected vessel group for noseid vessels

------------------------------------------------------------------------------------------------------------------------------------------

Date : 27-Jan-2020
Name : Yogendra Yadav
Description: 
Added Method Descriptions.

------------------------------------------------------------------------------------------------------------------------------------------

Date : 27-Jan-2020
Name : Mayur Kulkarni
Description: 
Added Validation on contract as per billable item category		

------------------------------------------------------------------------------------------------------------------------------------------		
Date : 27-Jan-2020
Name : Pratibha Nikam
Description: 
Show Custom Polygon error after selecting costal Surpic Resolved.

------------------------------------------------------------------------------------------------------------------------------------------		
Date : 28-Jan-2020
Name : Pratibha Nikam
Description: 
functions for finding Ships in Custom Polygon and DDP Polygon are added (for report).
